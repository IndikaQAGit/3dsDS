package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

public class PublicArrayOutputStreamTest {

    @Test
    public void publicArrayOutputStream() {
        String hello = "hello";
        byte[] helloBytes = hello.getBytes(StandardCharsets.UTF_8);
        PublicArrayOutputStream out = new PublicArrayOutputStream(helloBytes);
        Assertions.assertEquals(helloBytes, out.getBuf());
        Assertions.assertEquals(helloBytes.length, out.getCount());
        Assertions.assertEquals(hello, out.toString(StandardCharsets.UTF_8));
    }
}
