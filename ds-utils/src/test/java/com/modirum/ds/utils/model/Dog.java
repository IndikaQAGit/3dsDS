package com.modirum.ds.utils.model;

import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
public class Dog {
    String name2;
    int age;
}