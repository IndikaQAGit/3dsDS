package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

public class HexUtilsTest {

    @Test
    public void stringToHex() {
        String text = "Lorem Ipsum";
        String hexString = String.format("%040x", new BigInteger(1, text.getBytes(StandardCharsets.UTF_8)));
        byte[] hexBytes = HexUtils.fromString(hexString);
        Assertions.assertEquals(hexString, HexUtils.toStringLC(hexBytes));
        Assertions.assertEquals(HexUtils.toString(hexBytes), HexUtils.toStringLC(hexBytes).toUpperCase());
        Assertions.assertNotEquals(HexUtils.toString(hexBytes), HexUtils.toStringLC(hexBytes));
        Assertions.assertThrows(NullPointerException.class, () -> HexUtils.fromString(null));
        Assertions.assertThrows(NullPointerException.class, () -> HexUtils.toString(null));
        Assertions.assertThrows(NullPointerException.class, () -> HexUtils.toStringLC(null));
    }

}
