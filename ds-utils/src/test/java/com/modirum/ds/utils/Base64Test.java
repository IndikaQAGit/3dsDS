package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

public class Base64Test {

    @Test
    public void encodeAndDecodeString() {
        String testString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor " +
                "incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                "ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in " +
                "voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non " +
                "proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        byte[] testStringByteArray = testString.getBytes(StandardCharsets.UTF_8);
        String expectedBase64EncodedString =
                "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwg\n" +
                        "c2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWdu\n" +
                        "YSBhbGlxdWEuIFV0IGVuaW0gYWQgbWluaW0gdmVuaWFtLCBxdWlzIG5vc3RydWQgZXhlcmNpdGF0\n" +
                        "aW9uIHVsbGFtY28gbGFib3JpcyBuaXNpIHV0IGFsaXF1aXAgZXggZWEgY29tbW9kbyBjb25zZXF1\n" +
                        "YXQuIER1aXMgYXV0ZSBpcnVyZSBkb2xvciBpbiByZXByZWhlbmRlcml0IGluIHZvbHVwdGF0ZSB2\n" +
                        "ZWxpdCBlc3NlIGNpbGx1bSBkb2xvcmUgZXUgZnVnaWF0IG51bGxhIHBhcmlhdHVyLiBFeGNlcHRl\n" +
                        "dXIgc2ludCBvY2NhZWNhdCBjdXBpZGF0YXQgbm9uIHByb2lkZW50LCBzdW50IGluIGN1bHBhIHF1\n" +
                        "aSBvZmZpY2lhIGRlc2VydW50IG1vbGxpdCBhbmltIGlkIGVzdCBsYWJvcnVtLg==";

        String base64EncodedString = Base64.encode(testStringByteArray);
        Assertions.assertEquals(expectedBase64EncodedString, base64EncodedString);
        Assertions.assertEquals(testString, new String(Base64.decode(expectedBase64EncodedString)));
    }


    @Test
    public void encodeAndDecodeWithDiffrentWrapSizes() {
        String testString = "Lorem Ipsum";
        byte[] testStringByteArray = testString.getBytes(StandardCharsets.UTF_8);
        String expectedBase64EncodedString = "TG9yZW0gSXBzdW0=";
        String expectedBase64EncodedStringWithWrap4 = "TG9y\nZW0g\nSXBz\ndW0=";
        String expectedBase64EncodedStringWithWrap8 = "TG9yZW0g\nSXBzdW0=";

        String base64EncodedString = Base64.encode(testStringByteArray, 0);
        Assertions.assertEquals(expectedBase64EncodedString, base64EncodedString);
        Assertions.assertEquals(testString, new String(Base64.decode(expectedBase64EncodedString)));

        String base64EncodedStringWithWrap4 = Base64.encode(testStringByteArray, 4);
        Assertions.assertEquals(expectedBase64EncodedStringWithWrap4, base64EncodedStringWithWrap4);
        Assertions.assertEquals(testString, new String(Base64.decode(base64EncodedStringWithWrap4)));

        String base64EncodedStringWithWrap8 = Base64.encode(testStringByteArray, 8);
        Assertions.assertEquals(expectedBase64EncodedStringWithWrap8, base64EncodedStringWithWrap8);
        Assertions.assertEquals(testString, new String(Base64.decode(base64EncodedStringWithWrap8)));
    }

    @Test
    public void encodeAndDecodeURL() {
        String url = "http://google.com?param1=testing&param2=¾a";
        String expectedBase64EncodedUrl = "aHR0cDovL2dvb2dsZS5jb20_cGFyYW0xPXRlc3RpbmcmcGFyYW0yPcK-YQ";
        String encodedUrl = Base64.encodeURL(url.getBytes(StandardCharsets.UTF_8));
        Assertions.assertEquals(expectedBase64EncodedUrl, encodedUrl);
        String decodedUrl = new String(Base64.decodeURL(encodedUrl), StandardCharsets.UTF_8);
        Assertions.assertEquals(url, decodedUrl);
    }

}
