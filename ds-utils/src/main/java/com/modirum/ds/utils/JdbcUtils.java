package com.modirum.ds.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcUtils {

    /**
     * A convenient helper method in extracting column value from ResultSet.
     *
     * @param resultSet
     * @param columnLabel
     * @param valueClazz
     * @param <T>
     * @return
     */
    public static <T> T getColumnValue(ResultSet resultSet, String columnLabel, Class<T> valueClazz) throws SQLException {
        if (isColumnExisting(resultSet, columnLabel)) {
            //This is specially handled since getObject for Short is not internally supported by the jdbc driver
            if (Short.class.equals(valueClazz)) {
                String value = resultSet.getString(columnLabel);
                return resultSet.wasNull() ? null : (T) Short.valueOf(value);
            }
            T value = resultSet.getObject(columnLabel, valueClazz);
            return resultSet.wasNull() ? null : value;
        }
        return null;
    }

    /**
     * Checks if columnLabel is existing in the ResultSet.
     *
     * @param resultSet
     * @param columnLabel
     * @return
     */
    public static boolean isColumnExisting(ResultSet resultSet, String columnLabel) {
        try {
            //Need to separate this check to silently ignore the SQLException when columnLabel is not existing in the query
            //but we don't want to ignore other causes of SQLException.
            return resultSet.findColumn(columnLabel) > 0;
        } catch (SQLException sqlException) {
            //Silently ignore, this means columnLabel is not included in the sql select columns
            return false;
        }
    }
}
