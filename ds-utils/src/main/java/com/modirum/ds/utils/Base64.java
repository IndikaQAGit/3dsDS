/*
 * Copyright (c) 2001, University of Siegen,
 *                     Institute for Data Communications Systems
 *                     http://www.nue.et-inf.uni-siegen.de/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * - Neither the name of the "University of Siegen, Institute for Data
 *   Communications Systems" nor the names of its contributors may be used
 *   to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * -----------------
 *
 * Modified for M3DS by Tuomas Lukka
 */
package com.modirum.ds.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Implementation of MIME's Base64 encoding and decoding conversions. Optimized code. (raw version taken from oreilly.jonathan.util) Modified
 * for M3DS.
 *
 * @author Anli Shundi
 * @author Christian Geuer-Pollmann
 * @see <A HREF="ftp://ftp.isi.edu/in-notes/rfc2045.txt">RFC 2045</A>
 * @see org.xmlsecurity.transforms.implementations.TransformBase64Decode
 */
public class Base64 {

    public interface Mode {
        public int cNormal = 1;
        public int cURL = 2; //RFC 4648
    }

    static class ByteSequence implements CharSequence {
        byte[] buf = null;

        public ByteSequence(byte[] b) {
            this.buf = b;

        }

        @Override
        public int length() {
            return buf != null ? buf.length : 0;
        }

        @Override
        public char charAt(int index) {
            return (char) buf[index];
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            // TODO Auto-generated method stub
            byte[] nbuf = new byte[end - start];
            System.arraycopy(buf, start, nbuf, 0, nbuf.length);

            return new ByteSequence(nbuf);
        }

    }

    /**
     * Field LINE_SEPARATOR
     */
    public static final char LINE_SEPARATOR = '\n';

    /**
     * Method decode
     *
     * @param base64
     * @return
     * @throws java.io.UnsupportedEncodingException
     */
    public static byte[] decode(byte[] base64) {
        return decode(new ByteSequence(base64));
    }

    /**
     * <p>
     * Decode a Base64-encoded string to a byte array
     * </p>
     *
     * @param base64 <code>String</code> encoded string (single line only !!)
     * @return Decoded data in a byte array
     */
    public static byte[] decode(CharSequence base64) {
        return decode(base64, Mode.cNormal);
    }

    public static byte[] decodeURL(CharSequence base64) {
        return decode(base64, Mode.cURL);
    }

    public static byte[] decode(CharSequence base64, int mode) {

        //strip whitespace from anywhere in the string.  Not the most memory
        StringBuilder buf = new StringBuilder(base64.length());

        for (int i = 0; i < base64.length(); i++) {
            char c = base64.charAt(i);
            if (!Character.isWhitespace(c)) {
                buf.append(c);
            }
        }

        int pad = 0;

        // append padding in case url mode.
        while (Mode.cURL == mode && buf.length() % 4 != 0) {
            buf.append('=');
        }

        for (int i = buf.length() - 1; (i > 0) && (buf.charAt(i) == '='); i--) {
            pad++;
        }

        int length = buf.length() * 3 / 4 - pad;
        byte[] raw = new byte[length];

        for (int i = 0, rawIndex = 0; i < buf.length(); i += 4, rawIndex += 3) {

            int block = (getValue(buf.charAt(i), mode) << 18)
                    + (getValue(buf.charAt(i + 1), mode) << 12)
                    + (getValue(buf.charAt(i + 2), mode) << 6)
                    + (getValue(buf.charAt(i + 3), mode));

            for (int j = 2; j >= 0; j--) {
                if (rawIndex + j < raw.length) {
                    raw[rawIndex + j] = (byte) (block & 0xff);
                }

                block >>= 8;
            }
        }

        return raw;
    }

    /**
     * <p>
     * Encode a byte array in Base64 format and return an optionally wrapped line
     * </p>
     *
     * @param raw  <code>byte[]</code> data to be encoded
     * @param wrap <code>int<code> length of wrapped lines; No wrapping if less than 4.
     * @return a <code>String</code> with encoded data
     */
    public static String encode(byte[] raw, int wrap) {
        return new String(encode(raw, wrap, Mode.cNormal), StandardCharsets.ISO_8859_1);
    }

    public static byte[] encode(byte[] raw, int wrap, int mode) {

        //calculate length of encoded string
        int encLen = ((raw.length + 2) / 3) * 4;

        //adjust for newlines
        if (wrap > 3) {
            wrap -= wrap % 4;
            encLen += 2 * (encLen / wrap);
        } else { //disable wrapping
            wrap = Integer.MAX_VALUE;
        }

        java.io.ByteArrayOutputStream encoded = new java.io.ByteArrayOutputStream(encLen);
        int len3 = (raw.length / 3) * 3;
        int outLen = 0; //length of output line
        try {
            for (int i = 0; i < len3; i += 3, outLen += 4) {
                if (outLen + 4 > wrap) {
                    encoded.write((byte) LINE_SEPARATOR);
                    outLen = 0;
                }

                encoded.write(encodeFullBlock(raw, i, mode));
            }

            if (outLen >= wrap) { //this will produce an extra newline if needed !? Sun had it this way...
                encoded.write((byte) LINE_SEPARATOR);
            }

            if (len3 < raw.length) {
                encoded.write(encodeBlock(raw, len3, mode));
            }

        } catch (IOException neverHappens) {
            throw new RuntimeException(neverHappens);
        }

        return encoded.toByteArray();
    }

    /**
     * Encode a byte array and fold lines at the standard 76th character.
     *
     * @param raw <code>byte[]<code> to be base64 encoded
     * @return the <code>String<code> with encoded data
     */
    public static String encode(byte[] raw) {
        return encode(raw, 76);
    }

    public static String encodeURL(byte[] raw) {
        return new String(encode(raw, 0, Mode.cURL), StandardCharsets.ISO_8859_1);
    }

    public static String encodeURL(byte[] raw, int len) {
        return new String(encode(raw, len, Mode.cURL), StandardCharsets.ISO_8859_1);
    }

    /**
     * Method encodeBlock
     *
     * @param raw
     * @param offset
     * @return
     */
    @SuppressWarnings({"unused"})
    protected static byte[] encodeBlock(byte[] raw, int offset, int mode) {

        int block = 0;
        int slack = raw.length - offset - 1;

        //int end = (slack >= 2) ? 2 : slack;

        for (int i = 0; i < 3; i++) {
            byte b = (offset + i < raw.length)
                    ? raw[offset + i]
                    : 0;
            int neuter = (b < 0)
                    ? b + 256
                    : b;

            block <<= 8;
            block += neuter;
        }


        byte[] base64 = new byte[4];

        for (int i = base64.length - 1; i >= 0; i--) {
            int sixBit = block & 0x3f;
            base64[i] = (byte) getChar(sixBit, mode);
            block >>= 6;
        }

        int pdc = 0;
        if (slack < 1) {
            base64[2] = '=';
            pdc++;
        }

        if (slack < 2) {
            base64[3] = '=';
            pdc++;
        }

        if (mode == Mode.cURL && pdc > 0) {
            byte[] wopad = new byte[base64.length - pdc];
            System.arraycopy(base64, 0, wopad, 0, wopad.length);
            base64 = wopad;
        }


        return base64;
    }

    /**
     * Method encodeFullBlock
     *
     * @param raw
     * @param offset
     * @return
     */
    protected static byte[] encodeFullBlock(byte[] raw, int offset, int mode) {
        int block = 0;

        for (int i = 0; i < 3; i++) {
            block <<= 8;
            block += (0xff & raw[offset + i]);
        }

        block = ((raw[offset] & 0xff) << 16) + ((raw[offset + 1] & 0xff) << 8)
                + (raw[offset + 2] & 0xff);

        byte[] base64 = new byte[4];

        for (int i = 3; i >= 0; i--) {
            int sixBit = block & 0x3f;

            base64[i] = (byte) getChar(sixBit, mode);
            block >>= 6;
        }

        return base64;
    }

    /**
     * Method getChar
     *
     * @param sixBit
     * @return
     */
    protected static char getChar(int sixBit, int mode) {

        if ((sixBit >= 0) && (sixBit < 26)) {
            return (char) ('A' + sixBit);
        }

        if ((sixBit >= 26) && (sixBit < 52)) {
            return (char) ('a' + (sixBit - 26));
        }

        if ((sixBit >= 52) && (sixBit < 62)) {
            return (char) ('0' + (sixBit - 52));
        }

        if (sixBit == 62) {
            if (mode == Mode.cURL) {
                return '-';
            }

            return '+';
        }

        if (sixBit == 63) {
            if (mode == Mode.cURL) {
                return '_';
            }

            return '/';
        }

        return '?';
    }

    /**
     * Method getValue
     *
     * @param c
     * @return
     */
    protected static int getValue(char c, int mode) {

        if ((c >= 'A') && (c <= 'Z')) {
            return c - 'A';
        }

        if ((c >= 'a') && (c <= 'z')) {
            return c - 'a' + 26;
        }

        if ((c >= '0') && (c <= '9')) {
            return c - '0' + 52;
        }

        if (mode == Mode.cNormal && c == '+' || mode == Mode.cURL && c == '-') {
            return 62;
        }

        if (mode == Mode.cNormal && c == '/' || mode == Mode.cURL && c == '_') {
            return 63;
        }

        if (c == '=') {
            return 0;
        }

        throw new IllegalArgumentException("Invalid base 64" + (mode == Mode.cURL ? "url" : "") + " char '" + c + "' (" + ((int) (c)) + ")");
    }

    public static void main(String[] arg) {
        String test = "MIIDpTCCAo2gAwIBAgIEch+NzTANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJFRTELMAkGA1UE" +
                "CBMCSE0xEDAOBgNVBAcTB1RhbGxpbm4xGDAWBgNVBAoTD0NhcmQgU2NoZW1lIEluYzELMAkGA1UE" +
                "CxMCRFMxGDAWBgNVBAMTDzNEUyBTY2hlbWUgUm9vdDAeFw0xNjAxMTMxNjA5MzhaFw0xNjA0MTIx" +
                "NjA5MzhaMGMxCzAJBgNVBAYTAkVFMQswCQYDVQQIEwJITTEQMA4GA1UEBxMHVGFsbGlubjETMBEG" +
                "A1UEChMKTWVyY2hhbnQgMTEPMA0GA1UECxMGNDEyMzQ1MQ8wDQYDVQQDEwY3NzExMjIwggEiMA0G" +
                "CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCF6YbnvWe1YACWVX+jPzDVkHgvvqYGH14tz5ZUe7dh" +
                "ETliAtvqkS8rfCI5OFejrTTquGdH7BmAMu57dxsw77PBHNuSxhGjnRt0T4bLdKVwSx9arHu5jcIp" +
                "LoIovmbOoK6hP3yN0Z2cPTF3R88nG4hAULof3WJ18TqngD1OQztlBpxZfQVTtDNv0V83fyrz5BWj" +
                "se1wTW1MDmYuL1jAowZv9XOBqpu7PBEKwLA979Yi63LqDOkBFyKbJ9RmhWK1YYq65daYIoyPWXge" +
                "Qr0D2xOKqp5nvWKV3EuWN1pqIL6dCJbIGHEv7qj2szUlCk4rgL5tfKPCefIfpBJcz1yAhmyRAgMB" +
                "AAGjVzBVMB8GA1UdIwQYMBaAFC7R38knHV4U9Vk/wyw1/gIyTvudMBMGA1UdJQQMMAoGCCsGAQUF" +
                "BwMCMB0GA1UdDgQWBBRXWazzjvqzu725TEJxtLVuA5o8ezANBgkqhkiG9w0BAQsFAAOCAQEAQxQh" +
                "Ajy3VHH/PVpQHYrzxrlmycb8t3zbPorRF+zIE0+LBOnELGW8CATpbBzifSQfAPvcgEuuHgdFU2nP" +
                "ObIBkwgAI1s3uQ+iedoYxjpgvPj0WpnEPwArw0709ZGKgvq3cvVnw0ZGV63H02pNO2YSEs40q8B/" +
                "EOkjl9D4apYPAhXCJid2nU+rf/EsK2o1LeV2ccadfWnumR6xBe/Vv6jDkj0kl6hbB1XA+AkYMAcN" +
                "iAq9XeLt5OY4MD/y+g6XNuLmyR9BdVYu9o4F2gaAPZDu+Ba8EuH1Q9w/wK8X2eoYoIYxYk1Z9PbA" +
                "IHO2YJHIfNf/TNeFL+nVp3ip7pjvI8Hrxw==";

        byte[] dec = Base64.decode(test);
        String testback = Base64.encode(dec, 0);
        System.out.println("test    =" + test);
        System.out.println("testback=" + testback);
        System.out.println("Test 1 " + test.equals(testback));
        System.out.println("Test 1 " + test.equals(testback));
        String test1u = Base64.encodeURL(dec, 0);
        System.out.println("test1u    =" + test1u);
        byte[] test1u1b = Base64.decodeURL(test1u);
        String test1ub = Base64.encodeURL(test1u1b, 0);
        System.out.println("test1uback=" + test1ub);

        String test2 = "Tere tere vana kere õöäü %&219201-_\\===.a// \\!!!!\nContinues!";
        String enc2 = Base64.encode(test2.getBytes(StandardCharsets.UTF_8));
        System.out.println("ENC2=" + enc2);
        byte[] dec2 = Base64.decode(enc2);
        String test2b = new String(dec2, StandardCharsets.UTF_8);
        System.out.println("Back (" + (test2.equals(test2b) ? "matches" : "mismatch") + ")=" + test2b);

        String enc3 = Base64.encodeURL(test2.getBytes(StandardCharsets.UTF_8));
        System.out.println("ENC3=" + enc3);
        byte[] dec3 = Base64.decodeURL(enc3);
        String test3b = new String(dec3, StandardCharsets.UTF_8);
        System.out.println("Back (" + (test2.equals(test3b) ? "matches" : "mismatch") + ")=" + test3b);

    }
}
