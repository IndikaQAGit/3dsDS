/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 05.10.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeFactory;

public class DateUtil {
    public final static long DT24H = 24L * 3600L * 1000L;
    static Map<String, List<SimpleDateFormat>> fmtMap = new java.util.HashMap<String, List<SimpleDateFormat>>();
    static int poolMinSize = 5;
    static int poolMaxSize = 10;

    public final static TimeZone GMTZone = TimeZone.getTimeZone("GMT");

    // safe pooling if pool runs out return new instance
    static SimpleDateFormat getFormatter(String format) {

        List<SimpleDateFormat> fmtPool = fmtMap.get(format);
        if (fmtPool == null) {
            fmtPool = new ArrayList<SimpleDateFormat>();
            for (int i = 0; i < poolMinSize; i++) {
                SimpleDateFormat sf = new SimpleDateFormat(format);
                sf.setLenient(false);
                fmtPool.add(sf);
            }

            synchronized (fmtMap) {
                fmtMap.put(format, fmtPool);
            }

        }

        // transformer not thread safe should be pooled..
        synchronized (fmtPool) {
            if (fmtPool.size() > 0) {
                return fmtPool.remove(fmtPool.size() - 1);
            }
        }

        SimpleDateFormat sf = new SimpleDateFormat(format);
        sf.setLenient(false);

        return sf;

    }

    // safe pool returning until pool size < 10
    static void returnFormattter(String format, SimpleDateFormat formatter) {
        List<SimpleDateFormat> fmtPool = fmtMap.get(format);
        if (fmtPool != null && fmtPool.size() < poolMaxSize) {
            synchronized (fmtPool) {
                fmtPool.add(formatter);
            }
        }
    }

    public static java.util.Calendar normalizeDate(java.util.Calendar dateCal) {
        dateCal.set(java.util.GregorianCalendar.HOUR_OF_DAY, 0);
        dateCal.set(java.util.GregorianCalendar.MINUTE, 0);
        dateCal.set(java.util.GregorianCalendar.SECOND, 0);
        dateCal.set(java.util.GregorianCalendar.MILLISECOND, 0);

        return dateCal;
    }

    public static java.util.Calendar normalizeDateDayEnd(java.util.Calendar dateCal) {
        dateCal.set(java.util.GregorianCalendar.HOUR_OF_DAY, 23);
        dateCal.set(java.util.GregorianCalendar.MINUTE, 59);
        dateCal.set(java.util.GregorianCalendar.SECOND, 59);
        dateCal.set(java.util.GregorianCalendar.MILLISECOND, 999);

        return dateCal;
    }

    public static java.util.Date normalizeDateDayEnd(java.util.Date date) {
        java.util.Calendar dateCal = java.util.Calendar.getInstance();
        dateCal.setTime(date);
        dateCal.set(java.util.GregorianCalendar.HOUR_OF_DAY, 23);
        dateCal.set(java.util.GregorianCalendar.MINUTE, 59);
        dateCal.set(java.util.GregorianCalendar.SECOND, 59);
        dateCal.set(java.util.GregorianCalendar.MILLISECOND, 999);

        return dateCal.getTime();
    }

    /**
     * Returns a date with <code>days</code> added. Does not take daylight saving into consideration,
     * adds exactly the amount of time equal <code>days</code> provided.
     * @param date Date to add days to
     * @param days Amount of days to add, may be negative for subtraction
     * @return <code>date</code> with <code>days</code> added
     */
    public static Date addDays(Date date, int days) {
        // convert to UTC to avoid daylight saving hours
        Calendar dateCal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        dateCal.setTime(date);
        dateCal.add(GregorianCalendar.DATE, days);
        return dateCal.getTime();
    }

    public static java.util.Date addMonths(java.util.Date date, int months) {
        java.util.Calendar dateCal = java.util.Calendar.getInstance();
        dateCal.setTime(date);
        dateCal.add(java.util.GregorianCalendar.MONTH, months);
        return dateCal.getTime();
    }

    public static int calculateDaysDiff(java.util.Date date1, java.util.Date date2) {
        // normalize dates
        java.util.GregorianCalendar date1Cal = new java.util.GregorianCalendar();
        date1Cal.setTime(date1);
        DateUtil.normalizeDate(date1Cal);

        java.util.GregorianCalendar date2Cal = new java.util.GregorianCalendar();
        date2Cal.setTime(date2);
        DateUtil.normalizeDate(date2Cal);

        int diffDays = (int) ((date2Cal.getTimeInMillis() + date2Cal.get(Calendar.ZONE_OFFSET) + date2Cal.get(Calendar.DST_OFFSET) -
                date1Cal.getTimeInMillis() + date1Cal.get(Calendar.ZONE_OFFSET) + date1Cal.get(Calendar.DST_OFFSET)) / DT24H);

        return diffDays;
    }

    public static java.util.Date parseDate(String d, String format, boolean gmt) {
        if (d == null) {
            return null;
        }
        // 20110824 15:12:37
        if (gmt) {
            return parseDate(d, format, GMTZone);
        }
        return parseDate(d, format);
    }


    public static java.util.Date parseDate(String d, String format, TimeZone tz) {
        if (d == null) {
            return null;
        }
        // 20110824 15:12:37
        if (tz != null) {
            SimpleDateFormat sdf = getFormatter(format);
            TimeZone backup = sdf.getTimeZone();
            sdf.setTimeZone(tz);
            try {
                java.util.Date da = sdf.parse(d);
                sdf.setTimeZone(backup);
                returnFormattter(format, sdf);
                return da;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return parseDate(d, format);
    }

    public static String formatDate(java.util.Date d, String format, boolean gmt) {
        if (d == null) {
            return null;
        }
        // 20110824 15:12:37

        if (gmt) {
            return formatDate(d, format, GMTZone);
        }
        return formatDate(d, format);
    }

    public static String formatDate(java.util.Date d, String format, TimeZone tz) {
        if (d == null) {
            return null;
        }
        // 20110824 15:12:37

        if (tz != null) {
            SimpleDateFormat sdf = getFormatter(format);
            TimeZone backup = sdf.getTimeZone();
            sdf.setTimeZone(tz);
            String s = sdf.format(d);
            sdf.setTimeZone(backup);
            returnFormattter(format, sdf);
            return s;
        }
        return formatDate(d, format);
    }

    public static String formatDate(java.util.Date d, String format) {
        return formatDate(d, format, TimeZone.getDefault());
    }

    public static final java.util.Date parseDate(String dv, String format) {
        if (dv == null) {
            return null;
        }
        SimpleDateFormat sdf = getFormatter(format);
        java.util.Date d = null;
        try {
            d = sdf.parse(dv);
            returnFormattter(format, sdf);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return d;
    }

    static DatatypeFactory dtf = null;

    static {
        try {
            dtf = DatatypeFactory.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String formatXMLDateTime(java.util.Date d) {
        if (d == null) {
            return null;
        }

        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTime(d);
        return javax.xml.bind.DatatypeConverter.printDateTime(c);
    }

    public static java.util.Date parseXMLDateTime(String d) {
        if (d == null) {
            return null;
        }

        java.util.Calendar c = javax.xml.bind.DatatypeConverter.parseDateTime(d);
        if (c != null) {
            return c.getTime();
        }

        return null;
    }

    public static String formatXMLDate(java.util.Date d) {
        if (d == null) {
            return null;
        }

        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTime(d);
        return javax.xml.bind.DatatypeConverter.printDate(c);
    }

    public static java.util.Date parseXMLDate(String d) {
        if (d == null) {
            return null;
        }

        java.util.Calendar c = javax.xml.bind.DatatypeConverter.parseDate(d);
        if (c != null) {
            return c.getTime();
        }

        return null;
    }

    public static String formatXMLTime(java.util.Date d) {
        if (d == null) {
            return null;
        }

        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTime(d);
        return javax.xml.bind.DatatypeConverter.printTime(c);
    }

    public static java.util.Date parseXMLTime(String d) {
        if (d == null) {
            return null;
        }

        java.util.Calendar c = javax.xml.bind.DatatypeConverter.parseTime(d);
        if (c != null) {
            return c.getTime();
        }

        return null;
    }

    public static boolean isValidHour(Integer hour) {
        return hour != null && hour >= 0 && hour < 24;
    }

    public static boolean isValidMinute(Integer minute) {
        return minute != null && minute >= 0 && minute < 60;
    }


    public static void main(String args[]) {
        String fmt = "dd/MM/yyyy HH:mm:ss";
        int days1 = DateUtil.calculateDaysDiff(DateUtil.parseDate("24/02/2018 20:14:57", fmt), DateUtil.parseDate("27/03/2018 07:00:09", fmt));
        System.out.println("days1=" + days1);
        int days2 = DateUtil.calculateDaysDiff(DateUtil.parseDate("23/02/2018 16:15:20", fmt), DateUtil.parseDate("25/03/2018 07:00:09", fmt));
        System.out.println("days2=" + days2);
        int days3 = DateUtil.calculateDaysDiff(DateUtil.parseDate("24/02/2018 20:14:57", fmt), DateUtil.parseDate("26/03/2018 07:00:09", fmt));
        System.out.println("days3=" + days3);

        int days4 = DateUtil.calculateDaysDiff(DateUtil.parseDate("23/02/2018 16:15:20", fmt), DateUtil.parseDate("28/10/2018 07:00:09", fmt));
        System.out.println("days4=" + days4);
        int days5 = DateUtil.calculateDaysDiff(DateUtil.parseDate("24/02/2018 20:14:57", fmt), DateUtil.parseDate("29/10/2018 07:00:09", fmt));
        System.out.println("days5=" + days5);

        int days6 = DateUtil.calculateDaysDiff(DateUtil.parseDate("28/03/2018 16:15:20", fmt), DateUtil.parseDate("28/10/2018 07:00:09", fmt));
        System.out.println("days6=" + days6);
        int days7 = DateUtil.calculateDaysDiff(DateUtil.parseDate("29/03/2018 20:14:57", fmt), DateUtil.parseDate("29/10/2018 07:00:09", fmt));
        System.out.println("days7=" + days7);

        for (int i = 10; i < 31; i++) {
            int daysn = DateUtil.calculateDaysDiff(DateUtil.parseDate("29/03/2018 20:14:57", fmt), DateUtil.parseDate(i + "/10/2018 07:00:09", fmt));
            System.out.println("days(" + i + ")" + daysn);
        }


        TimeZone gmt = TimeZone.getTimeZone("GMT");
        TimeZone utc = TimeZone.getTimeZone("UTC");
        final String purchaseDateFormatJuly2017 = "yyyyMMddHHmmss";
        String jd = "20190620151048";
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Moscow"));
        java.util.Date dvgmt = DateUtil.parseDate(jd, purchaseDateFormatJuly2017, true);
        java.util.Date dvgmtu = DateUtil.parseDate(jd, purchaseDateFormatJuly2017, utc);
        System.out.println("JSON=" + jd + " DateTZ=" + dvgmt + "/" + dvgmtu + " tz=" + TimeZone.getDefault() + " DateGMT=" + DateUtil.formatDate(dvgmt, purchaseDateFormatJuly2017, true));
    }

}
