/*
 * Copyright (C) 2014 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 14.01.2014
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.util.concurrent.ThreadFactory;

public class DaemonThreadFactory implements ThreadFactory {
    String dmnName = "";

    public DaemonThreadFactory(String dmnName) {
        this.dmnName = dmnName;
    }

    public Thread newThread(Runnable r) {
        Thread dThread = new Thread(r);
        dThread.setDaemon(true);
        dThread.setName(dmnName + " " + dThread.getName());

        return dThread;
    }
}