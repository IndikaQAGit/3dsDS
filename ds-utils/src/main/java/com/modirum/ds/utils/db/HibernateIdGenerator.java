package com.modirum.ds.utils.db;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

public class HibernateIdGenerator implements IdentifierGenerator, Configurable {
    private String name;
    private String idType;
    private String idTable;
    private boolean usedbid = true;
    DoNextWork work = null;

    public HibernateIdGenerator() {
        work = this.new DoNextWork();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIdType(String type) {
        this.idType = type;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    public void setUsedbid(boolean u) {
        this.usedbid = u;
    }

    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        IdGen generator = IdGen.getInstance(name, idTable, usedbid);
        if ("Integer".equals(idType)) {
            return Long.valueOf(generator.next(session.connection())).intValue();
        } else if ("Short".equals(idType)) {
            return Long.valueOf(generator.next(session.connection())).shortValue();
        } else if ("String".equals(idType)) {
            return Long.valueOf(generator.next(session.connection())).toString();
        }

        return generator.next(session.connection());
    }

    class DoNextWork implements org.hibernate.jdbc.ReturningWork<Long> {
        public Long execute(Connection conn) throws SQLException {
            IdGen generator = IdGen.getInstance(name, idTable, usedbid);
            return generator.next(conn);
        }
    }

    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
        name = params.getProperty("name", "default");
        idType = params.getProperty("idType", "");
        String v = params.getProperty("idTable");
        if (v != null) {
            idTable = v;
        }
    }

}
