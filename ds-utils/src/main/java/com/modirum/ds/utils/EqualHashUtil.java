/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 02.12.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Toolkit for equals and hascode implementations
 *
 * @author andri
 */
public class EqualHashUtil {
    static java.util.Map<String, List<Field>> fieldCache = new java.util.HashMap<>();

    private static List<Field> getList(Class<?> thisclass) {
        List<Field> list = fieldCache.get(thisclass.getName());
        if (list == null) {
            list = new java.util.ArrayList<Field>();
            list.addAll(java.util.Arrays.asList(thisclass.getDeclaredFields()));

            Class<?> currentClass = thisclass;
            while (currentClass.getSuperclass() != null && !currentClass.getSuperclass().equals(Object.class)) {
                currentClass = currentClass.getSuperclass();
                list.addAll(java.util.Arrays.asList(currentClass.getDeclaredFields()));
            }

            synchronized (fieldCache) {
                fieldCache.put(thisclass.getName(), list);
            }
        }
        return list;
    }

    public static int hashCode(Object o) {
        if (o == null) {
            return 0;
        }

        int hcode = 0;
        Class<?> thisclass = o.getClass();
        List<Field> list = getList(thisclass);

        for (int i = 0; list != null && i < list.size(); i++) {
            try {
                Field fx = list.get(i);
                if (!fx.isAccessible()) {
                    fx.setAccessible(true);
                }

                // skip transients
                if (!java.lang.reflect.Modifier.isTransient(fx.getModifiers())) {
                    Object thisValue = fx.get(o);
                    if (thisValue != null) {
                        if (o == thisValue) // prevent cyclic overflow
                        {
                            if (hcode != 0) {
                                return hcode;
                            }
                            hcode += 31 * thisValue.toString().hashCode() + i;
                        } else {
                            hcode += 31 * thisValue.hashCode() + i;
                        }
                    }
                }
            } catch (Exception e) {
                java.util.logging.Logger.getLogger(EqualHashUtil.class.getName()).
                        log(java.util.logging.Level.WARNING, "EqualHashUtil hashcode error", e);
            }
        }

        return hcode;
    }

    public static boolean equals(Object o1, Object o2) {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null && o2 == null) {
            return true;
        }
        if (o1 == null && o2 != null) {
            return false;
        }
        if (o1 != null && o2 == null) {
            return false;
        }

        Class<?> oclass = o2.getClass();
        Class<?> thisclass = o1.getClass();

        if (!oclass.equals(thisclass)) {
            return false;
        }

        List<Field> list = getList(thisclass);

        for (int i = 0; list != null && i < list.size(); i++) {
            try {
                Field fx = list.get(i);
                if (!fx.isAccessible()) {
                    fx.setAccessible(true);
                }

                // skip transients
                if (!java.lang.reflect.Modifier.isTransient(fx.getModifiers())) {
                    Object thisValue = fx.get(o1);
                    Object objectValue = fx.get(o2);
                    if (thisValue == objectValue) {
                        continue;
                    }

                    if (thisValue == null && objectValue != null || thisValue != null && objectValue == null) {
                        return false;
                    }
                    if (thisValue != null && objectValue != null && !thisValue.equals(objectValue)) {
                        return false;
                    }
                }
            } catch (Exception e) {
                java.util.logging.Logger.getLogger(EqualHashUtil.class.getName()).
                        log(java.util.logging.Level.SEVERE, "Error comparing objects", e);
                return false;
            }

        }
        return true;
    }
}
