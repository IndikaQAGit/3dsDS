/*
 * $Id: HttpsJSSEClient.java,v 1.14 2007-08-28 04:43:03 veparkki Exp $
 *
 * Copyright (c) 2002,2003 Modirum.
 * All rights reserved.
 */

package com.modirum.ds.utils.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.logging.Level;

import javax.naming.ConfigurationException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;

import com.modirum.ds.utils.Misc;

/**
 * HTTPS client that can POST data to a given url address. The client utlises both client and server authentication.
 *
 * <p>
 * Uses Sun Microsystem's JSSE (Java Secure Sockets Extension), see <code><a href="http://java.sun.com/products/jsse">
 * http://java.sun.com/products/jsse</a></code>.
 * </p>
 *
 * <hr>
 * <b>Copyright</b> &copy;2002, 2003 <a href="http://www.modirum.com/">Modirum</a> <br>
 * All rights reserved.
 * <p>
 * <i>$Revision: 1.14 $</i>
 */
public class HttpsJSSEClient {
    private static LoggerAdapt log = new LoggerAdapt();

    private static class LoggerAdapt {
        java.util.logging.Logger jdkLog = java.util.logging.Logger.getLogger(HttpsJSSEClient.class.getName());
        Object sfl4j = null;

        public LoggerAdapt() {
            try {
                sfl4j = org.slf4j.LoggerFactory.getLogger(HttpsJSSEClient.class);
            } catch (Throwable t) {
                warn("org.slf4j.LoggerFactory not found using java.util.logging.Logger");
            }
        }

        public void error(String s, Throwable t) {
            if (sfl4j != null) {
                ((org.slf4j.Logger) sfl4j).error(s, t);
            } else {
                jdkLog.log(Level.SEVERE, s, t);
            }
        }

        public void error(String s) {
            if (sfl4j != null) {
                ((org.slf4j.Logger) sfl4j).error(s);
            } else {
                jdkLog.log(Level.SEVERE, s);
            }

        }

        public void debug(String s) {
            if (sfl4j != null) {
                ((org.slf4j.Logger) sfl4j).debug(s);
            } else {
                jdkLog.log(Level.FINE, s);
            }
        }

        public void info(String s) {
            if (sfl4j != null) {
                ((org.slf4j.Logger) sfl4j).info(s);
            } else {
                jdkLog.log(Level.INFO, s);
            }


        }

        public void warn(String s) {
            if (sfl4j != null) {
                ((org.slf4j.Logger) sfl4j).warn(s);
            } else {
                jdkLog.log(Level.WARNING, s);
            }

        }

        public void warn(String s, Throwable t) {
            if (sfl4j != null) {
                ((org.slf4j.Logger) sfl4j).warn(s, t);
            } else {
                jdkLog.log(Level.WARNING, s, t);
            }

        }

        public boolean isDebugEnabled() {

            if (sfl4j != null) {
                return ((org.slf4j.Logger) sfl4j).isDebugEnabled();
            } else {
                return jdkLog.isLoggable(Level.FINE);
            }
        }
    }


    static final ThreadLocal<String> CTL = new ThreadLocal<String>();
    private static volatile int COMNUM = 0;

    protected String clientKeyAlias = "clientauth";
    public static int MINFREE_USEPOOL = 2;
    protected SSLSocketFactory factory;
    protected int connectTimeout = 10000;
    protected int readTimeout = 30000;
    boolean hostNameVerify = false;
    private Proxy proxy;
    ScheduledThreadPoolExecutor execService;
    public static final String DEFAULT_SSL_CONTEXT_PROTOCOL = SSLSocketHelper.SSLProto.DEFAULT;
    String userAgent = "Modirum HTTPClient";
    String urlUsed = null;
    String trustManagerAlg;
    boolean clientKeySet = false;
    String clientKeySetTo;
    String[] ciphers;
    String[] prototocols;

    /**
     * Initializes SSL provider, keystore, truststore and SSL context.
     *
     * @param sslProvider                - normally should be null
     * @param keyManagerFactoryType      - in case of sun jdk SunX509, in case of ibm jdk IbmX509
     * @param sslContext                 - SSL or TLS
     * @param allowUntrustedCertificates - true or false
     * @param clientKey                  - in case client authentication is needed client private key or null if not client auth
     * @param clientCert                 - in case client authentication is needed client certificate or null if not client auth
     * @param truststore                 - the ssl server truststore
     * @param postTimeout                - timeout in ms
     * @throws ConfigurationException
     */

    @SuppressWarnings("rawtypes")
    public void init(String sslProvider, String keyManagerFactoryType, String sslContext, String allowUntrustedCertificates,
                     PrivateKey clientKey, Certificate[] clientCert, KeyStore truststore, int connectTimeout1, int readTimeout1, boolean hostNameVerify)
            throws Exception {

        this.hostNameVerify = hostNameVerify;
        //
        // Initialise SSL provider */
        //
        if (sslProvider != null && sslProvider.length() > 0 && !"com.sun.net.ssl.internal.ssl.Provider".equals(sslProvider)) {
            Class handlerClass = null;
            Provider intern = null;

            try {
                handlerClass = Class.forName(sslProvider);
                intern = (Provider) handlerClass.newInstance();

                if (intern != null && java.security.Security.getProvider(intern.getName()) == null) {
                    java.security.Security.addProvider(intern);
                }

            } catch (Exception ex) {
                log.error("Couldn't initialize SSL provider", ex);

                if (handlerClass == null) {
                    throw new RuntimeException(sslProvider + "not found", ex);
                }
                if (intern == null) {
                    throw new RuntimeException(sslProvider + "instance new failed", ex);
                }

                throw new RuntimeException(sslProvider + "creation failed", ex);
            }

        }

        try { // certificates

            KeyManager[] keyManagers = null;
            // keystore
            // load client key managers only if properties are set up
            // otherwise leave client key/cert out of context
            if (clientKey != null && clientCert != null) {
                KeyStore clientkeystore = KeyStore.getInstance("JKS");
                clientkeystore.load(null);
                clientkeystore.setKeyEntry(clientKeyAlias, clientKey, "password".toCharArray(), clientCert);

                log.info("Client key certificate set (pubkey=" + Misc.cut(clientCert[0].getPublicKey().toString(), 16) + ".." +
                        ", SDN=" + ((X509Certificate) clientCert[0]).getSubjectDN());

                KeyManagerFactory kmf = KeyManagerFactory.getInstance(keyManagerFactoryType);
                kmf.init(clientkeystore, "password".toCharArray());

                if (log.isDebugEnabled()) {
                    log.debug("get key managers");
                }

                KeyManager[] defaultKeyManagers = kmf.getKeyManagers();

                if (defaultKeyManagers.length <= 0) {
                    throw new RuntimeException("no default manager");
                }

                keyManagers = new KeyManager[]{new EnchX509KeyManager(defaultKeyManagers[0])};
                clientKeySet = true;
                clientKeySetTo = ((X509Certificate) clientCert[0]).getSubjectDN().toString();

                if (log.isDebugEnabled()) {
                    for (int i = 0; i < keyManagers.length; i++) {
                        log.debug("keymanager = " + keyManagers[i]);
                    }
                }

            } else if (log.isDebugEnabled()) {
                log.debug("Client ssl key/certificate not specified and not to be used for the DS connection..");
            }

            //
            // Create trustmanagerlist for client authentication
            //

            TrustManager[] trustManagers = null;
            if ("true".equals(allowUntrustedCertificates)) {
                if (log.isDebugEnabled()) {
                    log.debug("allowing untrusted certs");
                }

                trustManagers = new TrustManager[]{new TrustingX509TrustManager(),};
            } else if (truststore != null) {
                if (log.isDebugEnabled()) {
                    log.debug("not allowing untrusted certs, using supplied truststore");
                }

                TrustManagerFactory tmf = TrustManagerFactory.getInstance(trustManagerAlg != null ? trustManagerAlg : TrustManagerFactory.getDefaultAlgorithm());
                tmf.init(truststore);
                trustManagers = tmf.getTrustManagers();

                for (int i = 0; log.isDebugEnabled() && i < trustManagers.length; i++) {
                    log.debug("trustmanager = " + trustManagers[i]);
                }
            }

            // Initialise SSL Context and create sopcket factory
            SSLContext context = sslContext != null ? SSLContext.getInstance(sslContext) : SSLContext.getDefault();
            context.init(keyManagers, trustManagers, null);
            if (ciphers != null && ciphers.length > 0) {
                context.getDefaultSSLParameters().setCipherSuites(ciphers);
            }
            if (prototocols != null && prototocols.length > 0) {
                context.getDefaultSSLParameters().setProtocols(prototocols);
            }

            factory = context.getSocketFactory();

        } catch (KeyStoreException ex) {
            throw new RuntimeException("invalid keystore - ", ex);
        } catch (IOException ex) {
            throw new RuntimeException("IO: invalid keystore - ", ex);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("no algorithm - ", ex);
        } catch (CertificateException ex) {
            throw new RuntimeException("invalid cert- ", ex);
        } catch (KeyManagementException ex) {
            throw new RuntimeException("KeyManagement: invalid cert - ", ex);
        } catch (UnrecoverableKeyException ex) {
            throw new RuntimeException("Unrecoverable key / invalid keystore - ", ex);
        } catch (Exception ex) {
            throw new RuntimeException("Unexpected exception initialization ", ex);
        }

        if (readTimeout1 > 0) {
            readTimeout = readTimeout1;
        }
        if (connectTimeout1 > 0) {
            connectTimeout = connectTimeout1;
        }

        if (log.isDebugEnabled()) {
            log.debug("HttpsJSSEClient: initializing done");
        }
    }

    private URL[] parseURLs(String urlString) throws MalformedURLException {
        String[] urls = urlString != null ? urlString.split(",") : null;
        if (urls == null) {
            return null;
        }
        URL[] ret = new URL[urls.length];
        for (int i = 0; i < urls.length; i++) {
            ret[i] = new java.net.URL(urls[i]);
        }
        return ret;
    }


    public byte[] post(String postUrl, final byte[] bytes, java.util.Map<String, String> extraHeaders) throws Exception {
        return post(postUrl, null, bytes, extraHeaders, null);
    }

    public byte[] post(String postUrl, String contentType, final byte[] bytes) throws Exception {
        return post(postUrl, contentType, bytes, null, null);
    }

    public byte[] post(String postUrl, String contentType, final byte[] bytes, java.util.Map<String, String> extraHeaders,
                       java.util.Map<String, String> responseHeaders) throws Exception {
        return send("POST", postUrl, contentType, bytes, 0, bytes != null ? bytes.length : 0, extraHeaders, responseHeaders);
    }

    public byte[] post(String postUrl, String contentType, final byte[] bytes, int offs, int len, java.util.Map<String, String> extraHeaders,
                       java.util.Map<String, String> responseHeaders) throws Exception {
        return send("POST", postUrl, contentType, bytes, offs, len, extraHeaders, responseHeaders);
    }

    public byte[] get(String postUrl, String contentType, final byte[] bytes, java.util.Map<String, String> extraHeaders,
                      java.util.Map<String, String> responseHeaders) throws Exception {
        return send("GET", postUrl, contentType, bytes, 0, bytes != null ? bytes.length : 0, extraHeaders, responseHeaders);
    }

    public byte[] send(String httpMethod, String postUrl, String contentType, final byte[] bytes, int offs, int len,
                       java.util.Map<String, String> extraHeaders,
                       java.util.Map<String, String> responseHeaders) throws IOException {

        // debug unique connection / send session number
        String sid = "C";
        synchronized (log) {
            COMNUM++;
            sid = sid + COMNUM;
        }

        if (log.isDebugEnabled()) {
            log.debug("[" + sid + "] " + "Sending " + httpMethod + " [url(s)=" + postUrl + "]");
        }

        // final URL httpsUrl = new URL(url);
        // parse url string to multiple urls
        URL[] urls = parseURLs(postUrl);

        if (log.isDebugEnabled()) {
            log.debug("[" + sid + "] " + "Dest urls count:" + (urls != null ? urls.length : 0));
        }
        //
        // Create a new thread for posting
        // and reading response
        ConnectionThread conThread = null;
        // object for sync signalling
        Object synchObject = new Object();

        for (int i = 0; i < urls.length; i++) {
            try {
                log.info("[" + sid + "] " + "Trying url: " + urls[i] + "" + (clientKeySetTo != null ? ", with client cert " + clientKeySetTo : ", no client cert"));
                conThread = new ConnectionThread(urls[i], bytes, offs, len, httpMethod, synchObject, contentType, extraHeaders, responseHeaders, sid);
                // start io thread
                conThread.start();

                try {
                    // wait until timeout or if notified
                    long wuco = System.currentTimeMillis() + connectTimeout;
                    long wucom = wuco + readTimeout;

                    if (log.isDebugEnabled()) {
                        log.debug("[" + sid + "] " + "Waiting connection thread to connect in " + connectTimeout + "ms, finish or up to " + (connectTimeout + readTimeout) + "ms");
                    }
                    while (!conThread.completed) {
                        // check if connected adjust wait if connected else wait only up to connect timeout
                        long wu = conThread.connected ? wucom : wuco;
                        long w = Math.min(wu - System.currentTimeMillis(), 2000);
                        if (w > 0) {
                            synchronized (synchObject) {
                                synchObject.wait(w);
                            }
                        } else {
                            break;
                        }
                    }
                } catch (InterruptedException ie) {
                    log.warn("[" + sid + "] " + "Thread post caller interrupted", ie);
                }

                if (!conThread.completed && conThread.isAlive()) {
                    conThread.interrupt();
                    try {
                        conThread.join(10);
                    } catch (Exception eee) {
                    }
                    if (conThread.exception == null) {
                        if (conThread.is == null && conThread.es == null) {
                            conThread.exception = new java.io.InterruptedIOException("Connection timed out.");
                        } else {
                            conThread.exception = new java.io.InterruptedIOException("Connection read timed out.");
                        }
                    }

                    if (conThread.is != null) {
                        try {
                            conThread.is.close();
                        } catch (Exception dc) {
                        }
                    }
                    if (conThread.os != null) {
                        try {
                            conThread.os.close();
                        } catch (Exception dc) {
                        }
                    }
                    if (conThread.es != null) {
                        try {
                            conThread.es.close();
                        } catch (Exception dc) {
                        }
                    }

                    if (conThread.con != null) {
                        try {
                            conThread.con.disconnect();
                            // second interrupt after close to let thred to die if not dead yet
                            if (conThread.isAlive()) {
                                conThread.interrupt();
                            }
                        } catch (Exception dc) {
                        }
                    }
                } else if (conThread.exception == null) // no exception with
                // directory no need to
                // continue
                {
                    String urlUsed = urls[i] != null ? urls[i].toString() : null;
                    if (responseHeaders != null) {
                        responseHeaders.put("urlUsed", urlUsed);
                    }

                    break;
                }

            } catch (Exception e) {
                conThread.exception = e;
            }

        } // for

        if (conThread.exception != null) {
            String s = "";
            for (int i = 0; i < urls.length; i++) {
                s += "[" + urls[i] + "]";
            }
            //log.error("["+sid+"] "+"Send error " + conThread.exception.getMessage(), conThread.exception);
            throw new IOException("[" + sid + "] Sending failed [" + conThread.exception + "] for urls: " + s, conThread.exception);
        }

        log.info("[" + sid + "] " + "posted/read successfully " + len + "/" +
                (conThread.responseOs != null ? conThread.responseOs.size() : 0));

        return conThread.responseOs != null ? conThread.responseOs.toByteArray() : new byte[0];
    }

    /**
     * Internal connection thread, which peforms the actual POST ' operation.
     */

    static class LastSocketRefFactory extends SSLSocketFactory {
        SSLSocketFactory sslf;
        Socket lastSocket;

        public LastSocketRefFactory(SSLSocketFactory sslf) {
            this.sslf = sslf;
        }

        @Override
        public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
            lastSocket = sslf.createSocket(s, host, port, autoClose);
            return lastSocket;
        }

        @Override
        public String[] getDefaultCipherSuites() {

            return sslf.getDefaultCipherSuites();
        }

        @Override
        public String[] getSupportedCipherSuites() {
            return sslf.getSupportedCipherSuites();
        }

        @Override
        public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
            lastSocket = sslf.createSocket(host, port);
            return lastSocket;
        }

        @Override
        public Socket createSocket(InetAddress host, int port) throws IOException {
            lastSocket = sslf.createSocket(host, port);
            return lastSocket;
        }

        @Override
        public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
            lastSocket = sslf.createSocket(host, port, localHost, localPort);
            return lastSocket;
        }

        @Override
        public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
            lastSocket = sslf.createSocket(address, port, localAddress, localPort);
            return lastSocket;
        }

        public Socket getLastSocket() {
            return lastSocket;
        }

    }

    protected class ConnectionThread implements Runnable {
        public InputStream is = null;
        public InputStream es = null;
        public OutputStream os = null;
        public Exception exception = null;

        public boolean notified = false;

        public boolean isNotified() {
            return notified;
        }

        Object synchObject;
        byte[] bytes;
        int offs;
        int len;
        URL httpsUrl;
        ByteArrayOutputStream responseOs;
        String contentType;
        public boolean completed = false;
        public boolean connected = false;
        HttpURLConnection con = null;
        java.util.Map<String, String> extraHeaders;
        java.util.Map<String, String> respHeaders;
        String requestMethod;
        Thread t = null;
        boolean alive = false;
        Future future = null;
        String sid;

        /**
         * Constructor initialises all the parameters.
         *
         * @param u     URL, where POST is sent
         * @param bytes byte array, which is posted to the URL
         * @param t     main thread
         */
        ConnectionThread(URL u, byte[] bytes, int offs, int len, String requestMethod, Object synchObject, String contentType,
                         java.util.Map<String, String> extraHeaders, java.util.Map<String, String> responseHeaders, String sid) {
            this.sid = sid;
            this.httpsUrl = u;
            this.bytes = bytes;
            this.offs = offs;
            this.len = len;
            this.synchObject = synchObject;
            this.contentType = contentType;

            this.extraHeaders = extraHeaders;
            this.respHeaders = responseHeaders;
            this.requestMethod = requestMethod;
        }

        public void start() {
            // use exec service only if at least 2 free threads
            if (execService != null &&
                    (execService.getMaximumPoolSize() - execService.getActiveCount() >= MINFREE_USEPOOL ||
                            execService.getMaximumPoolSize() - execService.getPoolSize() >= MINFREE_USEPOOL)) {
                future = execService.submit(this);
            } else {
                t = new Thread(this);
                t.setName("HttpsJSSECl.con-" + t.getName() + " " + sid);
                t.start();
            }
        }

        public void interrupt() {
            if (t != null) {
                t.interrupt();
            } else if (future != null) {
                future.cancel(true);
            }
        }

        public boolean isAlive() {
            if (t != null) {
                return t.isAlive();
            }

            return alive;
        }

        public void join(long l) throws InterruptedException {
            if (t != null) {
                t.join(l);
            }
        }

        /**
         * Entry point of the POST thread
         */
        @Override
        public void run() {
            alive = true;
            CTL.set(sid);
            if (log.isDebugEnabled()) {
                log.debug("[" + sid + "] " + "Connection thread started");
            }

            try {
                /*************************************************************/
                /*                                                           */
                /* Create connection and set method to POST */
                /*                                                           */
                /*************************************************************/
                urlUsed = httpsUrl.toString();

                if (proxy != null) {
                    log.info("[" + sid + "] " + "Connect via proxy " + proxy.address() + " to " + httpsUrl);
                    con = (HttpURLConnection) httpsUrl.openConnection(proxy);
                } else {
                    log.info("[" + sid + "] " + "Connect to " + httpsUrl);
                    con = (HttpURLConnection) httpsUrl.openConnection();
                }

                con.setConnectTimeout(connectTimeout);
                con.setReadTimeout(readTimeout);

                if (requestMethod != null && requestMethod.length() > 0) {
                    con.setRequestMethod(requestMethod);
                } else {
                    con.setRequestMethod("POST");
                }

                /*************************************************************/
                /*                                                           */
                /* If HTTPS, set SSL socket factory */
                /*                                                           */
                /*************************************************************/
                LastSocketRefFactory lrf = null;
                if (httpsUrl.getProtocol().equalsIgnoreCase("https")) {
                    lrf = new LastSocketRefFactory(factory != null ? factory : ((HttpsURLConnection) con).getSSLSocketFactory());
                    ((HttpsURLConnection) con).setSSLSocketFactory(lrf);

                    if (!hostNameVerify) {
                        ((HttpsURLConnection) con).setHostnameVerifier(new EnchHostnameVerifier());
                    }
                } else if (httpsUrl.getProtocol().equalsIgnoreCase("http")) {
                    log.warn("[" + sid + "] " + "Url protocol is unsecure HTTP instead of HTTPS");
                } else {
                    log.warn("[" + sid + "] " + "Non http protocol '" + httpsUrl.getProtocol() + "' (not HTTPS or HTTP");
                }

                boolean uaSet = false;
                if (extraHeaders != null && extraHeaders.size() > 0) {
                    java.util.Set<java.util.Map.Entry<String, String>> hSet = extraHeaders.entrySet();
                    for (java.util.Map.Entry<String, String> entry : hSet) {
                        con.setRequestProperty(entry.getKey(), entry.getValue());
                        uaSet = "User-Agent".equalsIgnoreCase(entry.getKey());

                    }
                }

                if (!uaSet) {
                    con.setRequestProperty("User-Agent", userAgent);
                }

                con.setDoInput(true);
                con.setUseCaches(false);

                // Write data to the output stream
                if (bytes != null) {
                    if (this.contentType != null) {
                        con.setRequestProperty("Content-Type", this.contentType);
                    } else {
                        con.setRequestProperty("Content-Type", "application/xml; charset=\"utf-8\"");
                    }

                    con.setRequestProperty("Content-Length", "" + len);
                    con.setDoOutput(true);
                    // establish open connection
                    log.info("[" + sid + "] " + "Connecting (open streams) to " + httpsUrl);
                    os = con.getOutputStream();
                    Socket sx = null;
                    if (lrf != null && (sx = lrf.getLastSocket()) != null) {
                        String p = "";
                        if (sx instanceof SSLSocket && ((SSLSocket) sx).getSession() != null) {
                            p = " proto=" + ((SSLSocket) sx).getSession().getProtocol();
                            if (log.isDebugEnabled()) {
                                p = p + " cip=" + ((SSLSocket) sx).getSession().getCipherSuite();
                            }
                        }
                        log.info("[" + sid + "] " + "Connected from " + sx.getLocalAddress().getHostAddress() + ":" + sx.getLocalPort() + " to " +
                                sx.getInetAddress().getHostAddress() + ":" + sx.getPort() + "" + p);
                    }
                    connected = true;
                    os.write(bytes, offs, len);
                    os.flush();

                    if (log.isDebugEnabled()) {
                        log.debug("[" + sid + "] " + "Content posted " + len + " bytes..");
                    }
                }

                if (log.isDebugEnabled()) {
                    log.debug("[" + sid + "] " + "HTTP response code is " + con.getResponseCode() + " and message is " + con.getResponseMessage());
                }

                int rcode = con.getResponseCode();
                if (respHeaders != null) {
                    respHeaders.put("HTTP_RESPONSE", "" + rcode);
                    respHeaders.put("HTTP_RESPONSE_MSG", con.getResponseMessage());
                    Map<String, List<String>> headers = con.getHeaderFields();
                    if (headers != null) {
                        for (String headerName : headers.keySet()) {
                            List<String> hv = headers.get(headerName);
                            StringBuilder hvs = new StringBuilder();
                            for (int n = 0; hv != null && n < hv.size(); n++) {
                                if (hvs.length() > 0) {
                                    hvs.append(";");
                                }
                                hvs.append(hv.get(n));
                            }
                            respHeaders.put(headerName, hvs.toString());
                        }
                    }
                    respHeaders.put("ContentEncoding", con.getContentEncoding());
                }

                boolean chunked = "chunked".equals(con.getHeaderField("Transfer-Encoding"));
                int ctl = con.getContentLength();
                if (ctl > 0 || chunked) {
                    try {
                        is = con.getInputStream();

                        int bz = 2048;
                        if (ctl > 64 * 1024) {
                            bz = 64 * 1024;

                        } else if (ctl > 16 * 1024) {
                            bz = 16 * 1024;
                        }

                        responseOs = new ByteArrayOutputStream(ctl > 0 ? ctl : 2048);

                        byte[] buff = new byte[bz];
                        int n = 0;

                        if (log.isDebugEnabled()) {
                            log.debug("[" + sid + "] " + "Reading response data..");
                        }
                        while ((n = is.read(buff, 0, buff.length)) >= 0 && (chunked || responseOs.size() < con.getContentLength())) {
                            responseOs.write(buff, 0, n);
                        }

                        if (log.isDebugEnabled()) {
                            log.debug("[" + sid + "] " + "Reading response data completed " + responseOs.size() + " bytes");
                        }

                    } catch (Exception e) {
                        // read error data and populate to errorData
                        if (respHeaders != null) {
                            es = con.getErrorStream();
                            if (es != null) {
                                ByteArrayOutputStream errorOs = new ByteArrayOutputStream(512);
                                byte[] buff = new byte[512];
                                int n = 0;

                                if (log.isDebugEnabled()) {
                                    log.debug("[" + sid + "] " + "Reading response error data..");
                                }
                                while ((n = es.read(buff, 0, buff.length)) >= 0 && (chunked || errorOs.size() < con.getContentLength())) {
                                    errorOs.write(buff, 0, n);
                                }

                                if (log.isDebugEnabled()) {
                                    log.debug("[" + sid + "] " + "Reading error response data completed " + errorOs.size() + " bytes");
                                }
                                respHeaders.put("errorData", new String(errorOs.toByteArray(), "UTF-8"));
                            }
                        }

                        throw e;
                    } // fail

                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("[" + sid + "] " + "No response data not chunked and content-length " + con.getContentLength() + " bytes");
                    }
                }

            } catch (Exception ex) {
                String httpCode = null;
                try {
                    httpCode = con.getResponseCode() + " " + con.getResponseMessage();
                } catch (Exception dc) {
                }

                this.exception = ex;
                log.error("[" + sid + "] " + "Connection '" + urlUsed + "' run got " + ex.getMessage() + " http resp " + httpCode); //, ex);
            } finally {
                completed = true;
                alive = false;

                try {
                    synchronized (synchObject) {
                        notified = true;
                        synchObject.notifyAll();
                    }
                } catch (Exception dc) {
                }

                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception dc) {
                    }
                    is = null;
                }

                if (es != null) {
                    try {
                        es.close();
                    } catch (Exception dc) {
                    }
                    es = null;
                }

                if (os != null) {
                    try {
                        os.close();
                    } catch (Exception dc) {
                    }
                    os = null;
                }

                if (con != null) {
                    try {
                        con.disconnect();
                    } catch (Exception dc) {
                    }
                    con = null;
                }

                CTL.set(sid + "*");
            } //finally
            CTL.set(sid + "#");
        } // run
    } // class

    /**
     * Custom X509 Keymanager to make it possible to use cert req signed SSL certificates with client authorization.
     */
    protected class EnchX509KeyManager implements X509KeyManager {

        protected X509KeyManager km;
        String sid = "C";

        public EnchX509KeyManager(KeyManager km) {
            this.km = (X509KeyManager) km;
        }

        public String chooseClientAlias(String keyType, Principal[] issuers) {
            if (log.isDebugEnabled()) {
                log.debug("[" + CTL.get() + "] " + "chooseClientAlias [" + keyType + "]");
            }

            for (int i = 0; i < issuers.length; i++) {
                if (log.isDebugEnabled()) {
                    log.debug("[" + CTL.get() + "] " + "issuer=" + issuers[i]);
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("[" + CTL.get() + "] " + "returned client alias = " + clientKeyAlias);
            }

            return clientKeyAlias;
        }

        public X509Certificate[] getCertificateChain(String alias) {

            if (log.isDebugEnabled()) {
                log.debug("[" + CTL.get() + "] " + "getCertificateChain [" + clientKeyAlias + "]");
            }

            X509Certificate[] ret = km.getCertificateChain(clientKeyAlias);

            for (int i = 0; ret != null && i < ret.length; i++) {
                if (log.isDebugEnabled()) {
                    log.debug("[" + CTL.get() + "] " + "cert[" + i + "] = " +
                            (ret[i] != null ?
                                    " V=" + ret[i].getVersion() + " SER=" + ret[i].getSerialNumber() + " SDN=" + ret[i].getSubjectDN() + " IDN=" + ret[i].getIssuerDN() + " valid=" + ret[i].getNotAfter()
                                            + " SIGAlg=" + ret[i].getSigAlgName()
                                    : null)
                    );
                }
            }

            return ret;
        }

        public String[] getClientAliases(String keyType, Principal[] issuers) {
            if (log.isDebugEnabled()) {
                log.debug("[" + CTL.get() + "] " + "getClientAliases [" + keyType + "]");
            }

            for (int i = 0; i < issuers.length; i++) {
                if (log.isDebugEnabled()) {
                    log.debug("[" + CTL.get() + "] " + "issuer = " + issuers[i]);
                }
            }
            String[] ret = km.getClientAliases(keyType, issuers);
            for (int i = 0; i < ret.length; i++) {
                if (log.isDebugEnabled()) {
                    log.debug("[" + CTL.get() + "] " + "returning alias[" + i + "] = " + ret[i]);
                }
            }
            return ret;
        }

        public PrivateKey getPrivateKey(String alias) {
            if (log.isDebugEnabled()) {
                log.debug("[" + CTL.get() + "] " + "getPrivateKey [" + alias + "]");
            }

            PrivateKey ret = km.getPrivateKey(alias);

            if (log.isDebugEnabled()) {
                log.debug("[" + CTL.get() + "] " + "private key = " + ret);
            }

            return ret;
        }

        public String[] getServerAliases(String keyType, Principal[] issuers) {
            return null;
        }

        public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {
            return this.chooseClientAlias(keyType[0], issuers);

        }

        public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {

            return this.chooseClientAlias(keyType, issuers);
        }
    }

    /**
     * Modified hostname verifier, which always returns true to verify
     */
    protected class EnchHostnameVerifier implements HostnameVerifier {
        public boolean verify(String urlHostName, String certHostname) {
            return true;
        }

        public boolean verify(String hostname, SSLSession session) {
            // TODO Auto-generated method stub
            return true;
        }
    }

    /**
     * Customized trust manager to communicate with server that uses untrusted certificates.
     */
    public class TrustingX509TrustManager implements X509TrustManager {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            if (log.isDebugEnabled()) {
                log.debug("TrustingX509TrustManager.getAcceptedIssuers()");
            }
            return null;
        }

        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
            // TODO Auto-generated method stub

        }

        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
            // TODO Auto-generated method stub

        }
    }

    public Proxy getProxy() {
        return proxy;
    }

    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    public ScheduledThreadPoolExecutor getExecService() {
        return execService;
    }

    public void setExecService(ScheduledThreadPoolExecutor execService) {
        this.execService = execService;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUrlUsed() {
        return urlUsed;
    }

    public void setUrlUsed(String urlUsed) {
        this.urlUsed = urlUsed;
    }

    public String getTrustManagerAlg() {
        return trustManagerAlg;
    }

    public void setTrustManagerAlg(String trustManagerAlg) {
        this.trustManagerAlg = trustManagerAlg;
    }

    public boolean isClientKeySet() {
        return clientKeySet;
    }

    public void setClientKeySet(boolean clientKeySet) {
        this.clientKeySet = clientKeySet;
    }

    public String[] getCiphers() {
        return ciphers;
    }

    public void setCiphers(String[] ciphers) {
        this.ciphers = ciphers;
    }

    public String[] getPrototocols() {
        return prototocols;
    }

    public void setPrototocols(String[] prototocols) {
        this.prototocols = prototocols;
    }

    public String getClientKeySetTo() {
        return clientKeySetTo;
    }

    public void setClientKeySetTo(String clientKeySetTo) {
        this.clientKeySetTo = clientKeySetTo;
    }

}
