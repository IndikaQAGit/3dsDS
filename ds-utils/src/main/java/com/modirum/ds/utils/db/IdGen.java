/* $Id: IdGen.java,v 1.2 2007-06-25 10:23:39 veparkki Exp $ */
/* Copyright 2003 Modirum */

// class to generate unique transaction id
// loosely based on O'Reilly Java Database Best Practice book
//

package com.modirum.ds.utils.db;

import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * A tool for the automatic generation of unique numbers. This class goes to the database once every <code>MAX_KEYS</code> requests to get a new
 * seed for the numbers it generates. This class is thread-safe, meaning multiple threads can be safely requesting unique numbers from it. It is
 * also multi-process safe. In other words, multiple machines can simultaneously be generating unique values and those values will be guaranteed
 * to be unique across all applications.
 * <p/>
 * The example code for using this class to get next transactionID: IdGen tids = IdGen.getInstance("tID"); tID = tids.next( conn );
 * <p/>
 * <p/>
 * <code>IdGen</code> table. That table should have the following <code>CREATE</code>: <span class="code"> CREATE TABLE MDMIDS ( name
 * VARCHAR(20) NOT NULL, seed BIGINT UNSIGNED NOT NULL, lastUpdate BIGINT UNSIGNED NOT NULL, PRIMARY KEY ( name, lastUpdate ) ); Last modified
 * $Date: 2007-06-25 10:23:39 $
 *
 * @author Jari Heikkinen
 * @version $Revision: 1.2 $
 */
public class IdGen {

    protected transient static Logger log = LoggerFactory.getLogger(IdGen.class);

    /**
     * The maximum number of keys that may be safely generated without going to the database. You should lower this number for client
     * applications and other short-lived programs. The number can be higher for applications with long uptimes. All applications using the same
     * sequencer, however, should have the same value for <code>MAX_KEYS</code>.
     */
    public final static long MAX_KEYS = 100L;
    /**
     * All sequencers currently in memory.
     */
    static private final HashMap<String, IdGen> sequencers = new HashMap<String, IdGen>();

    static private final Object syncObject = new Object();

    String idTable = "md_ids";
    static long idStart = 10;
    static long dbId = 0; // dbid support
    private boolean usedbid;

    static {
        dbIdInit();
    }

    private static void dbIdInit() {
        String dbIdStr = System.getProperty("modirum.dbId");
        if (Misc.isNullOrEmpty(dbIdStr)) {
            try {
                javax.naming.Context env = javax.naming.InitialContext.doLookup("java:comp/env");
                if (env != null) {
                    dbIdStr = (String) env.lookup("modirum.dbId");
                }
            } catch (javax.naming.NameNotFoundException e) {
                log.warn("modirum.dbId was not set in java:comp/env " + e);
            } catch (javax.naming.NoInitialContextException e) {
                log.warn("Error initializing/loading naming context " + e);
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.warn("Warning: Error initializing jndi context", e);
                } else {
                    log.warn("Warning: Error initializing jndi context " + e);
                }
            }
        }

        if (Misc.isInt(dbIdStr)) {
            dbId = Misc.parseInt(dbIdStr);
            log.info("Modirum DB ID initialized as " + dbId);
        }
    }

    /**
     * Looks to see if a sequencer has been generated for the sequence with the specified name. If not, it will instantiate one. Multiple calls
     * to this method with the same name are guaranteed to receive the same sequencer object. For best performance, classes should save a
     * reference to the sequencer once they get it in order to avoid the overhead of a <code>HashMap</code> lookup.
     *
     * @param name the name of the desired sequencer
     * @return the sequencer with the specified name
     */
    static public final IdGen getInstance(String name, String idTable, boolean usedbid) {
        final String key = "" + idTable + "-" + name;
        synchronized (sequencers) {
            if (!sequencers.containsKey(key)) {
                IdGen seq = null;
                if (idTable != null) {
                    seq = new IdGen(name, idTable);
                } else {
                    seq = new IdGen(name);
                }

                seq.usedbid = (usedbid);

                sequencers.put(key, seq);
                return seq;
            } else {
                return sequencers.get(key);
            }
        }
    }

    // The name of this sequencer. e.g. which id's we are creating
    private String name = null;

    // The seed this sequencer will use for generating its ID's.
    private long seed = -1L;

    // The current sequence within this sequencer's seed.
    private long sequence = 0L;


    /**
     * Constructs a new sequencer with the specified name.
     *
     * @param nom the name of the sequencer
     */
    private IdGen(String nom) {
        super();
        name = nom;
    }

    private IdGen(String nom, String tableName) {
        super();
        name = nom;
        idTable = tableName;
    }

    /**
     * Helper function to close statement and resultset
     */
    static private void close(PreparedStatement stmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * Gets the next seed from the database for this sequence. Creates or updates the sequence in database to contain new id Keeps on trying
     * until there are no concurrency problems
     *
     * @param conn the database connection
     * @throws java.sql.SQLException a database error occurred
     */
    private void reseed(Connection conn) throws SQLException {
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            // Keep in this loop as long as we encounter concurrency errors
            sequence = -1L;
            do {
                stmt = conn.prepareStatement("SELECT seed, lastUpdate FROM " + idTable + " WHERE name = ?");
                stmt.setString(1, name);
                rs = stmt.executeQuery();
                if (!rs.next()) {
                    // no such sequence, create it
                    close(stmt, rs);
                    try {
                        stmt = conn.prepareStatement("INSERT INTO " + idTable + " ( name, seed, lastUpdate ) " + "VALUES ( ?, ?, ? )");
                        stmt.setString(1, name);
                        stmt.setLong(2, idStart);
                        stmt.setLong(3, System.currentTimeMillis());
                        if (stmt.executeUpdate() != 1) {
                            throw new SQLException("No row was inserted.");
                        }
                        // need immediate commit as finally commit may be delayed
                        if (!conn.getAutoCommit()) {
                            conn.commit();
                        }
                        seed = idStart;
                        // DONT ALLOW 0..9 id to be generated
                        if (seed == 0) {
                            sequence = 9;
                        }
                    } finally {
                        close(stmt, rs);
                    }
                } else {
                    // fetch current seed & update time, increment and store

                    long ts; // time when last updated

                    seed = rs.getLong(1) + 1L;
                    ts = rs.getLong(2);
                    close(stmt, rs);

                    // increment the seed in the database
                    stmt = conn.prepareStatement("UPDATE " + idTable + " SET seed = ?, lastUpdate = ? " +
                            "WHERE name = ? AND lastUpdate = ?");
                    stmt.setLong(1, seed);
                    stmt.setLong(2, System.currentTimeMillis());
                    stmt.setString(3, name);
                    stmt.setLong(4, ts);
                    if (stmt.executeUpdate() != 1) {
                        // someone changed the database! try again!
                        seed = -1L;
                    }
                    // need immediate commit as finally commit may be delayed
                    if (!conn.getAutoCommit()) {
                        conn.commit();
                    }
                }
            } while (seed == -1L);

        } finally {
            close(stmt, rs);
            if (!conn.getAutoCommit()) {
                conn.commit();
            }
        }
    }

    /**
     * Generates a new unique number. The unique number is based on the following algorithm:<br/>
     * <i>unique number</i> = <i>seed</i> multiple by <i>maximum keys per seed</i> added to <i>seed sequence</i> <br/>
     * The method then increments the seed sequence for the next ID to be generated. If the ID to be generated would exhaust the seed, then a
     * new seed is retrieved from the database.
     *
     * @return a unique number or -1L if error occurred occurred while generating the number
     */
    public long next(Connection conn) {
        synchronized (syncObject) {
            // when seed is -1 or the sequence keys for this seed are exhausted,
            // get a new seed from the database
            if ((seed == -1L) || ((sequence + 1) >= MAX_KEYS)) {
                try {
                    reseed(conn);
                } catch (SQLException e) {
                    log.error("IdGen error", e);
                    throw new RuntimeException(e);
                }
            }
            // otherwise, just increment sequence value and return id
            sequence++;
            //return ((seed * MAX_KEYS) + sequence);
            // db id support
            long nxt = ((seed * MAX_KEYS) + sequence);
            if (usedbid) {
                String id = Long.toString(nxt) + dbId;
                return Long.parseLong(id);
            }
            return nxt;
        }
    }

    public static long getDbId() {
        return dbId;
    }

}
