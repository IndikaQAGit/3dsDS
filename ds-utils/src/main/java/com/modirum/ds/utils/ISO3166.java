package com.modirum.ds.utils;

import java.io.IOException;
import java.text.Collator;
import java.util.Comparator;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Alexander Zinovin Date: 03.03.2010 Time: 16:06:59
 */
public class ISO3166 {

    private static Logger log = LoggerFactory.getLogger(ISO3166.class);

    public static final int ALPHA_2_CODE_INDEX = 0;
    public static final int ALPHA_3_CODE_INDEX = 1;
    public static final int NAME_INDEX = 2;
    public static final String ALPHA_CODES_DELIMITER = "\\|";
    public static final String ISO3166_FILE_PATH = "/ISO3166.xml";

    private static ISO3166[] iso3166WithoutEmptyValues;
    private static ISO3166[] iso3166WithEmptyValues = new ISO3166[999];
    static TreeMap<String, ISO3166> iso3166all = new TreeMap<String, ISO3166>();

    static {
        Properties properties = new Properties();
        TreeSet<ISO3166> iso3166s = new TreeSet<ISO3166>(new Comparator<ISO3166>() {
            private Collator collator = Collator.getInstance();

            @Override
            public int compare(ISO3166 o1, ISO3166 o2) {
                return collator.compare(o1.getCountryName(), o2.getCountryName());
            }
        });

        java.io.InputStream is = null;
        try {
            is = ISO3166.class.getResourceAsStream(ISO3166_FILE_PATH);
            properties.loadFromXML(is);
            iso3166all.clear();
            iso3166WithoutEmptyValues = new ISO3166[properties.size()];
            for (Map.Entry<Object, Object> iso3166Entry : properties.entrySet()) {
                short numericCode = Short.parseShort((String) iso3166Entry.getKey());
                String[] alphaCodes = ((String) iso3166Entry.getValue()).split(ALPHA_CODES_DELIMITER);
                String name = alphaCodes[NAME_INDEX];
                String alpha2Code = alphaCodes[ALPHA_2_CODE_INDEX];
                String alpha3Code = alphaCodes[ALPHA_3_CODE_INDEX];
                // subdivisions
                Map<String, String> sd = null;
                if (alphaCodes.length > 3) {
                    sd = new java.util.LinkedHashMap<String, String>();
                    for (int i = 3; i < alphaCodes.length - 1; i++) {
                        String sdcode = alphaCodes[i].trim();
                        String sdname = sdcode; //alphaCodes[i].trim();
                        sd.put(sdcode, sdname);
                    }
                }

                ISO3166 iso3166 = new ISO3166(name, alpha2Code, alpha3Code, numericCode, sd);
                iso3166WithEmptyValues[numericCode] = iso3166;
                iso3166all.put(iso3166.a2code, iso3166);
                iso3166all.put(iso3166.a3code, iso3166);
                iso3166all.put(String.valueOf(iso3166.numeric), iso3166);
                iso3166s.add(iso3166);
            }
        } catch (IOException e) {
            log.warn("Error loading countries " + e.getMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception doncCare) {
                }
            }
        }

        iso3166WithoutEmptyValues = iso3166s.toArray(iso3166WithoutEmptyValues);
    }

    public final String countryName;
    public final String a2code;
    public final String a3code;
    public final Short numeric;
    public final Map<String, String> subDivisions;

    public String toString() {
        return new StringBuilder(32)
                .append(a2code).append(" ").append(a3code).append(" ").append(countryName).append(":").append(numeric).toString();
    }

    ISO3166(String countryName, String a2code, String a3code, int numeric, Map<String, String> subDivisions) {
        this.countryName = countryName;
        this.a2code = a2code;
        this.a3code = a3code;
        this.numeric = (short) numeric;
        this.subDivisions = subDivisions;
    }

    public static ISO3166 findFromNumeric(Short numericCode) {
        return numericCode > -1 && numericCode < iso3166WithEmptyValues.length ? iso3166WithEmptyValues[numericCode] : null;
    }

    public static ISO3166[] values() {
        return iso3166WithoutEmptyValues;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getA2code() {
        return a2code;
    }

    public String getA3code() {
        return a3code;
    }

    public Short getNumeric() {
        return numeric;
    }

}