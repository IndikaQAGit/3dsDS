/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 19.05.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ObjectDiff implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    static Logger log = LoggerFactory.getLogger(ObjectDiff.class);

    String className;
    String property;
    String method;
    Object value1;
    Object value2;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Object getValue1() {
        return value1;
    }

    public void setValue1(Object value1) {
        this.value1 = value1;
    }

    public Object getValue2() {
        return value2;
    }

    public void setValue2(Object value2) {
        this.value2 = value2;
    }


    public static java.util.List<ObjectDiff> diff(Object o1, Object o2) {
        java.util.List<ObjectDiff> diffList = new java.util.LinkedList<ObjectDiff>();
        Class<?> oclass = o1 != null ? o1.getClass() : null;
        if (oclass == null) {
            oclass = o2 != null ? o2.getClass() : null;
        }
        if (oclass == null) {
            return diffList;
        }

        Method[] methods = oclass.getMethods();
        for (int i = 0; methods != null && i < methods.length; i++) {
            try {
                if (methods[i].getParameterTypes().length == 0 &&
                        !"clone".equals(methods[i].getName()) && !"serialize".equals(methods[i].getName()) && !"hashCode".equals(methods[i].getName()) &&
                        !"toString".equals(methods[i].getName()) && !methods[i].getDeclaringClass().equals(Object.class) &&
                        Modifier.isPublic(methods[i].getModifiers()) && !Modifier.isStatic(methods[i].getModifiers())) {
                    Object v1 = o1 != null ? methods[i].invoke(o1, (Object[]) null) : null;
                    Object v2 = o2 != null ? methods[i].invoke(o2, (Object[]) null) : null;
                    if (v1 == null && v2 != null ||
                            v1 != null && v2 == null ||
                            v1 != null && !v1.equals(v2) ||
                            v2 != null && !v2.equals(v1)
                    ) {
                        // skip equal element collections
                        if (v1 != null && v2 != null && v1 instanceof Collection && v2 instanceof Collection) {
                            if (Arrays.deepEquals(((Collection) v1).toArray(), ((Collection) v2).toArray())) {
                                continue;
                            }
                        }

                        ObjectDiff od = new ObjectDiff();
                        od.setClassName(oclass.getSimpleName());
                        od.setMethod(methods[i].getName());
                        if (methods[i].getName().startsWith("get")) {
                            od.setProperty(java.beans.Introspector.decapitalize(methods[i].getName().substring(3)));
                        } else if (methods[i].getName().startsWith("is")) {
                            od.setProperty(java.beans.Introspector.decapitalize(methods[i].getName().substring(2)));
                        } else {
                            od.setProperty(methods[i].getName());
                        }

                        od.setValue1(v1);
                        od.setValue2(v2);
                        diffList.add(od);
                    }
                }
            } catch (Exception e) {
                log.error("Diff cannot resolved", e);
            }
        }

        return diffList;
    }

    public static java.util.Map<String, Object> properties(Object o1, boolean skipNullAndEmpty) {
        java.util.Map<String, Object> properties = new java.util.HashMap<String, Object>();
        Class<?> oclass = o1 != null ? o1.getClass() : null;
        if (oclass == null) {
            return properties;
        }

        Method[] methods = oclass.getMethods();
        for (int i = 0; methods != null && i < methods.length; i++) {
            try {
                if (methods[i].getParameterTypes().length == 0 &&
                        !"clone".equals(methods[i].getName()) && !"serialize".equals(methods[i].getName()) && !"hashCode".equals(methods[i].getName()) &&
                        !"toString".equals(methods[i].getName()) && !methods[i].getDeclaringClass().equals(Object.class) &&
                        Modifier.isPublic(methods[i].getModifiers()) && !Modifier.isStatic(methods[i].getModifiers())) {
                    Object v1 = o1 != null ? methods[i].invoke(o1, (Object[]) null) : null;
                    String property = "";
                    if (methods[i].getName().startsWith("get")) {
                        property = (java.beans.Introspector.decapitalize(methods[i].getName().substring(3)));
                    } else if (methods[i].getName().startsWith("is")) {
                        property = (java.beans.Introspector.decapitalize(methods[i].getName().substring(2)));
                    } else {
                        property = (methods[i].getName());
                    }

                    if (Misc.isNotNullOrEmpty(v1) || !skipNullAndEmpty) {
                        properties.put(property, v1);
                    }
                }

            } catch (Exception e) {
                log.error("Read properties error", e);
            }
        }

        return properties;
    }


    public static String createSimpleDiffString(java.util.List<ObjectDiff> diffList) {
        return createSimpleDiffString(diffList, null);
    }

    public static String createSimpleDiffString(java.util.List<ObjectDiff> diffList, Set<String> mask) {
        StringBuffer diffs = new StringBuffer();
        for (int i = 0; i < diffList.size(); i++) {
            ObjectDiff od = diffList.get(i);
            if (mask != null && mask.contains(od.getProperty())) {
                diffs.append(od.getProperty() + ": '" + Misc.mask(String.valueOf(od.getValue1()), 1) + "' -> '" + Misc.mask(String.valueOf(od.getValue2()), 1) + "';");
            } else {
                diffs.append(od.getProperty() + ": '" + od.getValue1() + "' -> '" + od.getValue2() + "';");
            }
        }

        return diffs.toString();
    }

    public static ObjectDiff removeDiffByProperty(java.util.List<ObjectDiff> diffList, String propName) {

        if (diffList != null && Misc.isNotNullOrEmpty(propName)) {
            for (int i = 0; i < diffList.size(); i++) {
                ObjectDiff odx = diffList.get(i);
                if (odx != null && propName.equals(odx.getProperty())) {
                    diffList.remove(i);
                    return odx;
                }
            }

        }

        return null;
    }


    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }


}
