/*
 * Copyright (C) 2018 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on Mar 23, 2018
 *
 */
package com.modirum.ds.utils;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;

public class PublicArrayOutputStream extends ByteArrayOutputStream {
    public PublicArrayOutputStream() {
        this(32);
    }

    public PublicArrayOutputStream(int size) {
        super(size);
    }

    public PublicArrayOutputStream(byte[] buf) {
        super.buf = buf;
        super.count = buf.length;
    }

    public String toString(Charset cs) {
        return new String(super.buf, 0, this.count, cs);
    }

    public byte[] getBuf() {
        return super.buf;
    }

    public int getCount() {
        return super.count;
    }
}