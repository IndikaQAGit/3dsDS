/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 22.12.2010
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils.db;

/**
 * Dummy model object, used only that hibernate creates automatically
 * ids table
 *
 * @author andri
 */
public class Id implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private Long seed;
    private Long lastUpdate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSeed() {
        return seed;
    }

    public void setSeed(Long seed) {
        this.seed = seed;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
