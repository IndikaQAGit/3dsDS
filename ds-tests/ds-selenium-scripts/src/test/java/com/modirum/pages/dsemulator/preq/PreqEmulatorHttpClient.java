package com.modirum.pages.dsemulator.preq;

import com.modirum.models.DeviceChannel;
import com.modirum.models.MessageCategory;
import com.modirum.utilities.MessageBuilder;
import com.modirum.utilities.FileUtil;
import com.modirum.utilities.HttpClient;
import com.modirum.utilities.JsonData;
import com.modirum.utilities.SelDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PreqEmulatorHttpClient implements PreqEmulator{
    private static final Logger LOGGER = LogManager.getLogger(PreqEmulatorHttpClient.class.getName());

    private String baseURL;
    private static final String DSERVER_URL = "/ds/DServer";
    private String dsEntrypointUrl;

    // 3DS MESSAGES
    private String threedssPreq;
    private String dsPres;

    // TRANSACTION FLOW FIELDS
    private String messageVersion;
    private MessageCategory messageCategory;
    private DeviceChannel deviceChannel;


    public PreqEmulatorHttpClient(SelDriver selDriver /* unused */, String baseURL){
        this.baseURL = baseURL;
        this.dsEntrypointUrl = baseURL + DSERVER_URL;
        threedssPreq = "";
        dsPres = "";

        messageVersion = "2.2.0";
        messageCategory = MessageCategory.PA;
        deviceChannel = DeviceChannel.BRW;
    }


    /***************************************************
     * Getters
     ***************************************************/
    public String getDsEntrypointUrl(){ return this.dsEntrypointUrl; }
    public String getPreq(){ return this.threedssPreq; }
    public String getDsPres(){ return this.dsPres; }

    /***************************************************
     * Setters / Clickers
     ***************************************************/
    public PreqEmulatorHttpClient setDsEntrypointUrl(String dsEntrypointUrl){
        this.dsEntrypointUrl = dsEntrypointUrl;
        return this;
    }
    public PreqEmulatorHttpClient setPreq(String preq){
        this.threedssPreq = preq;
        MessageBuilder preqBuilder = new MessageBuilder();
        preqBuilder.build(preq);

        if(preqBuilder.getField("messageVersion") != null && preqBuilder.getField("messageVersion") != "") {
            messageVersion = preqBuilder.getField("messageVersion").toString();
        }
        if(preqBuilder.getField("deviceChannel") != null) {
            deviceChannel = DeviceChannel.getDeviceChannelFromValue(preqBuilder.getField("deviceChannel").toString());
        }
        if(preqBuilder.getField("messageCategory") != null) {
            messageCategory = MessageCategory.getMessageCategoryFromValue(preqBuilder.getField("messageCategory").toString());
        }
        return this;
    }
    public void generatePreqPaBrw210(){
        messageVersion = "2.1.0";

        Map<String, Object> preq =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("dsemulator" + File.separator + messageVersion
                                + File.separator,
                        "3dss-preq.json"));
        JsonData.addOrUpdateField(preq, "threeDSServerTransID", UUID.randomUUID().toString());
        threedssPreq = JsonData.getAsString(preq);
    }
    public void generatePreqPaBrw220(){
        messageVersion = "2.2.0";

        Map<String, Object> preq =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("dsemulator" + File.separator + messageVersion
                                + File.separator,
                        "3dss-preq.json"));
        JsonData.addOrUpdateField(preq, "threeDSServerTransID", UUID.randomUUID().toString());
        threedssPreq = JsonData.getAsString(preq);
    }
    public void sendPreq(){
        dsPres = sendMessage(dsEntrypointUrl, threedssPreq);
        if(dsPres != null){
            MessageBuilder presBuilder = new MessageBuilder();
            presBuilder.build(dsPres);
        }
    }

    /***************************************************
     * Common Methods
     ***************************************************/
    public String waitForDsPres(){
        return getDsPres();
    }

    /***************************************************
     * Common Methods
     ***************************************************/
    private String sendMessage(String url, String payload){
        Map<String, Object> httpRespObj;

        // Create request header
        Map<String, Object> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        header.put("Connection", "keep-alive");
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        header.put("Accept-Encoding", "gzip, deflate, br");
        header.put("Accept-Language", "en-US,en;q=0.5");

        httpRespObj = HttpClient.send(header, "POST", url, payload);
        if(Integer.parseInt(httpRespObj.get("responseCode").toString()) == 200) {
            LOGGER.info(httpRespObj.toString());
            return httpRespObj.get("response").toString();
        } else {
            LOGGER.error("RESPONSE CODE: " + httpRespObj.get("responseCode").toString());
            LOGGER.error(httpRespObj.toString());
            return null;
        }
    }
}
