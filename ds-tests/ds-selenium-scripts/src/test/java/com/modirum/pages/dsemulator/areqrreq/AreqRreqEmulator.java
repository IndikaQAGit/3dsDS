package com.modirum.pages.dsemulator.areqrreq;

import com.modirum.models.DeviceChannel;
import com.modirum.models.MessageCategory;
import com.modirum.models.TransactionFlow;

public interface AreqRreqEmulator {
    /***************************************************
     * Getters
     ***************************************************/
    public String getDsEntrypointUrl();
    public String get3dssAreq();
    public String getAcsAres();
    public String getDsAres();
    public String getAcsTransIdForRres();
    public String get3dssRresJson();
    public String getAcsRreqJson();
    public String getDsRres();

    /***************************************************
     * Setters / Clickers
     ***************************************************/
    public AreqRreqEmulator setDsEntrypointUrl(String dsEntrypointUrl);
    public AreqRreqEmulator set3dssAreq(String areq);
    public void send3dssAreq();
    public AreqRreqEmulator setAcsAres(String acsAres);
    public AreqRreqEmulator setDsAres(String dsAres);
    public void generateAreqPaBrw210();
    public void generateAreqPaBrw220();
    public void generateAcsAresFrictionless();
    public void generateAcsAresChallenge();
    public void cacheAcsAres();
    public AreqRreqEmulator setAcsTransIdForRres(String acsTransId);
    public AreqRreqEmulator setRresJson(String rresJson);
    public void generate3dssRres();
    public void cache3dssRres();
    public void generateAcsRreq();
    public AreqRreqEmulator setAcsRreqJson(String rreqJson);
    public void sendAcsRreq();

    public String waitForDsAres();
    public String waitForDsRres();
    public void generate3dssAreq(String version, MessageCategory category, DeviceChannel channel);
    public void generateAcsAres(TransactionFlow transactionFlow);
}
