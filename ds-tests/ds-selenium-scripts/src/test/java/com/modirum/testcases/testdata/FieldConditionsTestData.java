package com.modirum.testcases.testdata;

public class FieldConditionsTestData {
    //---> AREQ
    public static String[] getAReqConditionalFields_browserJavascriptEnabled_True(){
        return new String[]  { "browserJavaEnabled", "browserColorDepth", "browserScreenHeight", "browserScreenWidth", "browserTZ" };
    }
    
    public static String[] getAReqConditionalFields_threeDSRequestorAuthenticationInd_02(){
        return new String[] { "purchaseAmount", "purchaseCurrency", "purchaseExponent", "purchaseDate", "recurringExpiry", "recurringFrequency" };
    }

    public static String[] getAReqConditionalFields_threeDSRequestorAuthenticationInd_03(){
        return new String[] { "purchaseInstalData" , "purchaseAmount", "purchaseCurrency", "purchaseExponent", "purchaseDate", "recurringExpiry", "recurringFrequency" };
    }

    // AReq, ARes, and RReq have this condition
    public static String[] getAReqConditionalFields_whiteListStatus_PRESENT(){
        return new String[] { "whiteListStatusSource" };
    }

    public static String[] getAReqConditionalFields_payTokenInd_true(){
        // (TODO) NOTE: payTokenInd inclusion itself is conditional (Required if there is a de-tokenisation of an Account Number.)
        // For testing protocol-level, assume that payTokenInd is optional since we can't get the condition from the message
        return new String[] { "payTokenSource" };
    }

    public static String[] getAReqConditionalFields_DsSpecificRules(){
        // (TODO) NOTE: DS Specific rules unknown at this time.
        // For testing protocol-level, assume that these are optional unless otherwise specified in resources/threeds/<version>/messages
        return new String[] {
               "threeDSReqAuthMethodInd",
         /*       "threeDSServerOperatorID",   NOTE: disabled since our DS has different handling for this field */
                "broadInfo",
                "cardExpiryDate",
                "messageExtension"
        };
    }

    public static String[] getAReqConditionalFields_RequiredRegionalMandate(){
        // (TODO) NOTE: EMVco inclusion rules for these fields are a bit vague
        //  --> required if available unless regional mandate restricts sending
        //  --> From protocol POV, treat as optional since we can't get regional mandate restrictions from the message

        return new String[] {
                "billAddrCity",
                "billAddrCountry",
                "billAddrLine1",
                "billAddrLine2",
                "billAddrLine3",
                "billAddrPostCode",
                "billAddrState",
                "email",
                "homePhone",
                "mobilePhone",
                "cardholderName",
                "shipAddrCity",
                "shipAddrCountry",
                "shipAddrLine1",
                "shipAddrLine2",
                "shipAddrLine3",
                "shipAddrPostCode",
                "shipAddrState",
                "workPhone"
        };
    }

    //---> ARES
    // AReq, ARes, and RReq have this condition
    public static String[] getAResConditionalFields_whiteListStatus_PRESENT(){
        return new String[] { "whiteListStatusSource" };
    }

    public static String[] getAResConditionalFields_transStatus_C(){
        return new String[] { "acsChallengeMandated", "acsEphemPubKey", "acsRenderingType", "acsSignedContent",
                "acsURL", "authenticationType", "sdkEphemPubKey" };
    }

    public static String[] getAResConditionalFields_transStatus_YA(){
        return new String[] { "authenticationValue" };
    }

    public static String[] getAResConditionalFields_transStatus_N(){
        return new String[] { "authenticationValue", "transStatusReason" };
    }

    public static String[] getAResConditionalFields_transStatus_UR(){
        return new String[] { "transStatusReason" };
    }

    public static String[] getAResConditionalFields_transStatus_D(){ // decoupled
        return new String[] { "acsChallengeMandated", "acsDecConInd", "authenticationType" };
    }

    public static String[] getAResConditionalFields_acsDecConInd_Y(){ // decoupled
        return new String[] { "cardholderInfo" };
    }

    public static String[] getAResConditionalFields_DsSpecificRules(){
        return new String[] { "acsOperatorID", "broadInfo", "eci", "messageExtension" };
    }

    //--> RREQ
    // AReq, ARes, and RReq have this condition
    public static String[] getRReqConditionalFields_whiteListStatus_PRESENT(){
        return new String[] { "whiteListStatusSource" };
    }

    public static String[] getRReqConditionalFields_transStatus_Y(){
        return new String[] {
                "authenticationType",
                "authenticationValue"
        };
    }

    public static String[] getRReqConditionalFields_transStatus_UAR(){
        return new String[] {
                "transStatusReason"
        };
    }

    public static String[] getRReqConditionalFields_transStatus_N(){
        return new String[] {
                "authenticationType",
                "authenticationValue",
                "transStatusReason"
        };
    }

    public static String[] getRReqConditionalFields_DsSpecificRules(){
        // (TODO) NOTE: DS Specific rules unknown at this time.
        // For testing protocol-level, assume that these are optional unless otherwise specified in resources/threeds/<version>/messages
        return new String[] {
                "authenticationValue", // DS specific for NPA
                "eci",
                "messageExtension"
        };
    }

    //--> RRES
    public static String[] getRResConditionalFields_DsSpecificRules(){
        return new String[] { "messageExtension" };
    }

    //--> PREQ
    public static String[] getPReqConditionalFields_DsSpecificRules(){
        return new String[] { "threeDSServerOperatorID", "messageExtension" };
    }
}
