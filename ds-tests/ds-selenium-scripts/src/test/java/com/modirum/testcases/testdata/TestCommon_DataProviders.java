package com.modirum.testcases.testdata;

import com.modirum.models.ErrorCode;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TestCommon_DataProviders {
    @DataProvider(name = "DP_AREQ_REQUIRED_FIELDS")
    private Object[][] Common_AReq_Required(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("requiredAreqFields");
        int numTests;
        if(context.getAttribute("testSet").toString().equalsIgnoreCase("core") && (5 <= fields.size())){
            numTests = 5;
            Collections.shuffle(fields); // shuffle the fields to get a random set of 5
        } else {
            numTests = fields.size();
        }

        Object [][] objArray = new Object[numTests][];

        for(int i=0;i< numTests;i++){
            String testParam = fields.get(i);;

            ErrorCode expectedError = ErrorCode.MISSING_REQUIRED_ELEMENT;
            if(testParam.equals("messageType")){
                expectedError = ErrorCode.INVALID_MESSAGE;
            } else if(testParam.equals("threeDSServerOperatorID")){
                expectedError = ErrorCode.ACCESS_DENIED_INVALID_ENDPOINT;
            }
            objArray[i] = new Object [] { testParam, expectedError };

        }
        return objArray;
    }

    @DataProvider(name = "DP_AREQ_OPTIONAL_FIELDS")
    private Object[][] Common_AReq_Optional(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("optionalAreqFields");
        int numTests;
        if(context.getAttribute("testSet").toString().equalsIgnoreCase("core") && (5 <= fields.size())){
            numTests = 5;
            Collections.shuffle(fields); // shuffle the fields to get a random set of 5
        } else {
            numTests = fields.size();
        }

        Object [][] objArray = new Object[numTests][];

        for(int i=0;i< numTests;i++){
            String testParam = fields.get(i);
            objArray[i] = new Object[1];
            objArray[i][0] = testParam;
        }
        return objArray;
    }

    @DataProvider(name = "DP_ARES_REQUIRED_FIELDS")
    private Object[][] Common_ARes_Required(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("requiredAresFields");
        int numTests;
        if(context.getAttribute("testSet").toString().equalsIgnoreCase("core") && (5 <= fields.size())){
            numTests = 5;
            Collections.shuffle(fields); // shuffle the fields to get a random set of 5
        } else {
            numTests = fields.size();

            if(fields.contains("dsReferenceNumber")) numTests = numTests - 1;
            if(fields.contains("dsTransID")) numTests = numTests - 1;
        }

        Object [][] objArray = new Object[numTests][];

        for(int i=0,j=0; j<numTests; i++){
            String testParam = fields.get(i);;

            ErrorCode expectedError = ErrorCode.MISSING_REQUIRED_ELEMENT;
            if(testParam.equals("messageType")){
                expectedError = ErrorCode.INVALID_MESSAGE;
            } else if(testParam.equals("dsReferenceNumber") || testParam.equals("dsTransID")){
                // Skipped due to emulator limitation (these DS fields are automatically added by emulator to the cached ARes and cannot be tested).
                continue;
            }
            objArray[j] = new Object [] { testParam, expectedError };
            j++;
        }
        return objArray;
    }

    @DataProvider(name = "DP_ARES_OPTIONAL_FIELDS")
    private Object[][] Common_AReS_Optional(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("optionalAresFields");
        int numTests;
        if(context.getAttribute("testSet").toString().equalsIgnoreCase("core") && (5 <= fields.size())){
            numTests = 5;
            Collections.shuffle(fields); // shuffle the fields to get a random set of 5
        } else {
            numTests = fields.size();
        }

        Object [][] objArray = new Object[numTests][];

        for(int i=0;i< numTests;i++){
            String testParam = fields.get(i);
            objArray[i] = new Object[1];
            objArray[i][0] = testParam;
        }
        return objArray;
    }

    @DataProvider(name = "DP_RREQ_REQUIRED_FIELDS")
    private Object[][] Common_RReq_Required(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("requiredRreqFields");
        Object [][] objArray = new Object[fields.size()][];

        for(int i=0;i< fields.size();i++) {
            String testParam = fields.get(i);
            ErrorCode expectedError = ErrorCode.MISSING_REQUIRED_ELEMENT;
            if (testParam.equals("messageType")) {
                expectedError = ErrorCode.INVALID_MESSAGE;
            }
            objArray[i] = new Object[]{testParam, expectedError};
        }
        return objArray;
    }

    @DataProvider(name = "DP_RREQ_OPTIONAL_FIELDS")
    private Object[][] Common_RReq_Optional(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("optionalRreqFields");
        int numTests;
        if(context.getAttribute("testSet").toString().equalsIgnoreCase("core") && (5 <= fields.size())){
            numTests = 5;
            Collections.shuffle(fields); // shuffle the fields to get a random set of 5
        } else {
            numTests = fields.size();
        }

        Object [][] objArray = new Object[numTests][];

        for(int i=0;i< numTests;i++){
            String testParam = fields.get(i);
            objArray[i] = new Object[1];
            objArray[i][0] = testParam;
        }
        return objArray;
    }

    @DataProvider(name = "DP_RRES_REQUIRED_FIELDS")
    private Object[][] Common_RRes_Required(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("requiredRresFields");
        Object [][] objArray = new Object[fields.size()][];

        for(int i=0;i< fields.size();i++) {
            String testParam = fields.get(i);
            ErrorCode expectedError = ErrorCode.MISSING_REQUIRED_ELEMENT;
            if (testParam.equals("messageType")) {
                expectedError = ErrorCode.INVALID_MESSAGE;
            }
            objArray[i] = new Object[]{testParam, expectedError};
        }
        return objArray;
    }

    @DataProvider(name = "DP_RRES_OPTIONAL_FIELDS")
    private Object[][] Common_RRes_Optional(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("optionalRresFields");
        int numTests;
        if(context.getAttribute("testSet").toString().equalsIgnoreCase("core") && (5 <= fields.size())){
            numTests = 5;
            Collections.shuffle(fields); // shuffle the fields to get a random set of 5
        } else {
            numTests = fields.size();
        }

        Object [][] objArray = new Object[numTests][];

        for(int i=0;i< numTests;i++){
            String testParam = fields.get(i);
            objArray[i] = new Object[1];
            objArray[i][0] = testParam;
        }
        return objArray;
    }

    @DataProvider(name = "DP_AREQ_CONDITIONAL_FIELDS_threeDSReqAuthMethodInd")
    private Iterator<Object[]> AReq_Conditional_threeDSReqAuthMethodInd(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("conditionalAreqFields");
        String ds = (String)context.getAttribute("targetDS");

        List<Object[]> data = new ArrayList<>();
        if(fields.contains("threeDSReqAuthMethodInd")){
            switch(ds.toLowerCase()){
                default:
                case "modirum":
                    // EMVco spec: DS-specific rules
                    data.add(new Object[]{ "01", null });
                    data.add(new Object[]{ "02", null });
                    data.add(new Object[]{ "03", null });
                    break;
                case "eftpos":
                    // eftpos spec: Reserved for future use
                    data.add(new Object[]{ "01", ErrorCode.INVALID_FORMAT });
                    data.add(new Object[]{ "02", ErrorCode.INVALID_FORMAT });
                    data.add(new Object[]{ "03", null });
                    break;
            }

            // COMMON FORMAT CHECKS
            // 04–79 = Reserved for EMVCo future use (values invalid until defined by EMVCo)
            data.add(new Object[]{ "04", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "79", ErrorCode.INVALID_FORMAT});
            // 80–99 = Reserved for DS use
            data.add(new Object[]{ "80", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "99", ErrorCode.INVALID_FORMAT});
            // length checks
            data.add(new Object[]{ "1", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "100", ErrorCode.INVALID_FORMAT});
        }

        return data.iterator();
    }

    /* **************************************************
     * PREPARATION FLOW (PREQ)
     ************************************************** */

    @DataProvider(name = "DP_COMMON_PREQ_REQUIRED")
    private Object[][] Common_PReq_Required(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("requiredPreqFields");
        Object [][] objArray = new Object[fields.size()][];

        for(int i=0;i< fields.size();i++){
            String testParam = fields.get(i);
            objArray[i] = new Object[1];
            objArray[i][0] = testParam;
        }
        return objArray;
    }

    @DataProvider(name = "DP_PREQ_ThreeDSServerOperatorID")
    private Object[][] Preq_ThreeDSServerOperatorID(){
        return new Object[][] {
                {"DS-3DS-EMULATOR-OPER-ID", "PRes"},
                {"ACS-EMULATOR-OPER-ID", "Erro"}
        };
    }
}
