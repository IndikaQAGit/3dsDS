package com.modirum.reports;

public enum TestReportType {
    DEFAULT,
    HTML,
    CONSOLE
}
