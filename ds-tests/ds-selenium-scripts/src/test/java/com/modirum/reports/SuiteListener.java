package com.modirum.reports;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ISuiteResult;
import org.testng.ITestContext;

import java.util.Date;
import java.util.Map;

public class SuiteListener implements ISuiteListener {
    private static final Logger LOGGER = LogManager.getLogger(SuiteListener.class.getName());
    private int suitePassedTests;
    private int suiteFailedTests;
    private int suiteSkippedTests;
    private int suiteTotalTests;
    private Date suiteStartTime;
    private Date suiteEndTime;


    @Override
    public void onStart(ISuite iSuite) {

    }

    @Override
    public void onFinish(ISuite iSuite) {
        suitePassedTests = 0;
        suiteFailedTests = 0;
        suiteSkippedTests = 0;
        suiteTotalTests = 0;
        suiteStartTime = null;
        suiteEndTime = null;


        for( Map.Entry entry: iSuite.getResults().entrySet()){
            System.out.println("KEY : " + entry.getKey());
            System.out.println("VALUE : " + entry.getValue());
            ISuiteResult result = (ISuiteResult) entry.getValue();
            onTestFinish(result.getTestContext());
        }

        LOGGER.info("----------------------------------------------");
        LOGGER.info("--------------- Test Suite Summary --------------");
        LOGGER.info("Test Suite started on: " + suiteStartTime.toString());

        LOGGER.info(""); // New Line
        LOGGER.info("Total Tests Ran: [" + suiteTotalTests + "]");
        LOGGER.info("  PASSED TEST CASES: [" + suitePassedTests + "]");
        LOGGER.info("  SKIPPED TEST CASES: [" + suiteSkippedTests + "]");
        LOGGER.info("  FAILED TEST CASES: [" + suiteFailedTests + "]");
        LOGGER.info(""); // New Line

        LOGGER.info("Test Suite completed on: " + suiteEndTime.toString());

        long diff = suiteEndTime.getTime() - suiteStartTime.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        LOGGER.info("Test Suite run duration: "+diffHours+" Hour(s), " + diffMinutes + " Minute(s), "+diffSeconds+" Second(s)");


        LOGGER.info("----------- End of Test Suite Summary -----------");
        LOGGER.info("----------------------------------------------");
    }

    public void onTestFinish(ITestContext iTestContext) {

        LOGGER.info("----------------------------------------------");
        LOGGER.info("--------------- SUITE Testing Summary --------------");
        LOGGER.info("--------------- "+  iTestContext.getName() +" --------------");

        Date testStartTime = iTestContext.getStartDate();

        LOGGER.info("Test started on: " + testStartTime.toString());

        if(suiteStartTime == null || suiteStartTime.after(testStartTime)){
            suiteStartTime = testStartTime;
        }

        int totalTests = iTestContext.getPassedTests().getAllResults().size()
                + iTestContext.getFailedTests().size()
                + iTestContext.getSkippedTests().size();
        LOGGER.info("Total Tests Ran: [" + totalTests + "]");
        suiteTotalTests += totalTests;

        LOGGER.info("  PASSED TEST CASES: [" + iTestContext.getPassedTests().getAllResults().size() + "]");
        suitePassedTests += iTestContext.getPassedTests().getAllResults().size();

        LOGGER.info("  SKIPPED TEST CASES: [" + iTestContext.getSkippedTests().getAllResults().size() + "]");
        suiteSkippedTests += iTestContext.getSkippedTests().getAllResults().size();

        if (iTestContext.getFailedTests().size() == 0)
            LOGGER.info("  FAILED TEST CASES: [0] NONE");
        else {
            LOGGER.info("  FAILED TEST CASES: [" + iTestContext.getFailedTests().getAllResults().size() + "]");
            suiteFailedTests += iTestContext.getFailedTests().getAllResults().size();

            LOGGER.error("  IN DETAIL:");
            iTestContext.getFailedTests().getAllResults()
                    .forEach(result -> {
                        String message = result.getThrowable().getMessage();
                        Object[] params = result.getParameters();
                        if(params.length > 0) {
                            LOGGER.error("    * " + result.getName() + "[" + params[0] + "]" + "." + message);
                        } else {
                            LOGGER.error("    * " + result.getName() + "." + message);
                        }
                    });
        }

        LOGGER.info(""); // New Line
        LOGGER.info("Test completed on: " + iTestContext.getEndDate().toString());

        Date testEndTime = iTestContext.getEndDate();
        if(suiteEndTime == null || suiteEndTime.before(testEndTime)){
            suiteEndTime = testEndTime;
        }


        long diff = iTestContext.getEndDate().getTime() - iTestContext.getStartDate().getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        // long diffDays = diff / (24 * 60 * 60 * 1000);

        LOGGER.info("Test run duration: "+diffHours+" Hour(s), " + diffMinutes + " Minute(s), "+diffSeconds+" Second(s)");

        LOGGER.info("----------- End of SUITE Testing Summary -----------");
        LOGGER.info("----------------------------------------------");
    }
}
