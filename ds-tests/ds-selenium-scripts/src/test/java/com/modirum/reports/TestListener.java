package com.modirum.reports;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class TestListener implements ITestListener {
    private static final Logger LOGGER = LogManager.getLogger(TestListener.class.getName());

    /*
     * For Reports
     */
    protected ITestReporter reporter;

    @Override
    public void onTestStart(ITestResult iTestResult) {
        String testClassName = iTestResult.getMethod().getInstance().getClass().getSimpleName();
        String testName = (String)iTestResult.getTestContext().getAttribute("testName");

        Object[] params = iTestResult.getParameters();
        if(params.length > 0) {
            LOGGER.info("-- " + testClassName + " -- Test started: " + iTestResult.getMethod().getMethodName() + "[" + params[0].toString() + "]");
            reporter.startTest(testName, "Test Method: " + iTestResult.getMethod().getMethodName() + "<br> Description: " + iTestResult.getMethod().getDescription());
        } else {
            LOGGER.info("-- " + testClassName + " -- Test started: " + iTestResult.getMethod().getMethodName());
            reporter.startTest(testName, iTestResult.getMethod().getDescription());
        }
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        String testClassName = iTestResult.getMethod().getInstance().getClass().getSimpleName();

        LOGGER.info("-- " + testClassName + " -- [PASSED TEST METHOD] " + iTestResult.getTestContext().getAttribute("testName"));
        reporter.logPass("[" + testClassName + "] " + iTestResult.getTestContext().getAttribute("testName"));
        reporter.endTest();
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        String testClassName = iTestResult.getMethod().getInstance().getClass().getSimpleName();

        LOGGER.info("-- " + testClassName + " -- [FAILED TEST METHOD] " + iTestResult.getTestContext().getAttribute("testName"));
        reporter.logFail("[" + testClassName + "] " + iTestResult.getTestContext().getAttribute("testName"));
        reporter.logFail("[" + testClassName + "] " + iTestResult.getThrowable().getMessage());
        reporter.endTest();
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        String testClassName = iTestResult.getMethod().getInstance().getClass().getSimpleName();

        LOGGER.info("-- " + testClassName + " -- [SKIPPED TEST METHOD] " + iTestResult.getTestContext().getAttribute("testName"));
        reporter.logSkip("[" + testClassName + "] " + iTestResult.getTestContext().getAttribute("testName"));
        reporter.endTest();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        String testOutPath = System.getProperty("user.dir") + File.separator + "test-reports" + File.separator + "DS_3ds2" + File.separator;
        String testContextName = iTestContext.getName();

        System.out.println("ONSTART: -NAME- " + testContextName);
        System.out.println("testOutPath + testClassName: " + testOutPath + testContextName + "_Report.html");
        reporter =  TestReporterFactory.getTestReporter(TestReportType.DEFAULT, testOutPath + testContextName + "_Report.html", true);
        // Set Test Reporter in context so that we can log information from within test cases
        iTestContext.setAttribute("testReporter", reporter);
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        LOGGER.info("----------------------------------------------");
        LOGGER.info("--------------- " + iTestContext.getName() + " Testing Summary --------------");

        LOGGER.info("Test started on: " + iTestContext.getStartDate().toString());

        int totalTests = iTestContext.getPassedTests().getAllResults().size()
                + iTestContext.getFailedTests().size()
                + iTestContext.getSkippedTests().size();
        LOGGER.info("Total Tests Ran: [" + totalTests + "]");

        LOGGER.info("  PASSED TEST CASES: [" + iTestContext.getPassedTests().getAllResults().size() + "]");

        LOGGER.info("  SKIPPED TEST CASES: [" + iTestContext.getSkippedTests().getAllResults().size() + "]");

        if (iTestContext.getFailedTests().size() == 0)
            LOGGER.info("  FAILED TEST CASES: [0] NONE");
        else {
            LOGGER.info("  FAILED TEST CASES: [" + iTestContext.getFailedTests().getAllResults().size() + "]");
            LOGGER.error("  IN DETAIL:");
            iTestContext.getFailedTests().getAllResults()
                    .forEach(result -> {
                        String message = result.getThrowable().getMessage();
                        Object[] params = result.getParameters();
                        if(params.length > 0) {
                            LOGGER.error("    * " + result.getName() + "[" + params[0] + "]" + "." + message);
                        } else {
                            LOGGER.error("    * " + result.getName() + "." + message);
                        }
                    });
        }

        LOGGER.info(""); // New Line
        LOGGER.info("Test completed on: " + iTestContext.getEndDate().toString());

        long diff = iTestContext.getEndDate().getTime() - iTestContext.getStartDate().getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        // long diffDays = diff / (24 * 60 * 60 * 1000);

        LOGGER.info("Test run duration: "+diffHours+" Hour(s), " + diffMinutes + " Minute(s), "+diffSeconds+" Second(s)");

        LOGGER.info("----------- End of " + iTestContext.getName() + " Testing Summary -----------");
        LOGGER.info("----------------------------------------------");
    }
}

