package com.modirum.reports;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.lang3.StringUtils;

public class ExtentTestReporter implements ITestReporter {
    private ExtentReports m_reporter = null;
    private ExtentTest m_testReporter = null;

    public ExtentTestReporter(String fullPath, Boolean bOverwrite){
        m_reporter = new ExtentReports(fullPath, bOverwrite);
    }

    public void startTest(String testName, String testDescription){
        m_testReporter = m_reporter.startTest(testName, testDescription);
    }

    public void logInfo(String info){
        m_testReporter.log(LogStatus.INFO, StringUtils.replace(info,"\n","<br>"));
    }

    public void logSkip(String info){
        m_testReporter.log(LogStatus.SKIP, StringUtils.replace(info,"\n","<br>"));
    }

    public void logPass(String info){
        m_testReporter.log(LogStatus.PASS, StringUtils.replace(info,"\n","<br>"));
    }

    public void logFail(String info){
        m_testReporter.log(LogStatus.FAIL, StringUtils.replace(info,"\n","<br>"));
    }

    public void logWarning(String info){
        m_testReporter.log(LogStatus.WARNING, StringUtils.replace(info,"\n","<br>"));
    }

    public void endTest(){
        m_reporter.endTest(m_testReporter);
        m_reporter.flush();
    }
}
