package com.modirum.reports;

import com.relevantcodes.extentreports.LogStatus;

public class ConsoleTestReporter implements ITestReporter{
    public ConsoleTestReporter(){
        System.out.println("Starting Test Report\n");
    }

    private void log(LogStatus status, String info){
        System.out.println(status.toString() + info);
    }

    public void startTest(String testName, String testDescription){
        System.out.println("\n[START TEST] " + testName + "\n\tDESCRIPTION: " + testDescription );
    }

    public void logInfo(String info){
        //System.out.println("[INFO]" + info);
        log(LogStatus.INFO, info);
    }

    public void logSkip(String info){
        //System.out.println("[SKIP]" + info);
        log(LogStatus.SKIP, info);
    }

    public void logPass(String info){
        //System.out.println("[PASS]" + info);
        log(LogStatus.PASS, info);
    }

    public void logFail(String info){
        //System.out.println("[FAIL]" + info);
        log(LogStatus.FAIL, info);
    }

    public void logWarning(String info){
        //System.out.println("[WARNING]" + info);
        log(LogStatus.WARNING, info);
    }

    public void endTest(){
        System.out.println("[END TEST]");
    }
}
