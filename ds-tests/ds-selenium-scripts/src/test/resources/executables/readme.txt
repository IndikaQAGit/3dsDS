Place geckodriver here.
Currently, only Firefox is supported in scripts.

* Reference : https://github.com/Modirum/ACS/wiki/QA-Test-Scripts-Guide

* Quick reference when running scripts from testDrivers directory

To start hub:
java -jar -Dwebdriver.gecko.driver=geckodriver selenium-server-standalone-3.141.59.jar -role hub -port 5557 -hubConfig DefaultHub.json

To start node:
java -jar -Dwebdriver.gecko.driver=geckodriver selenium-server-standalone-3.141.59.jar -role node -port 5558 -nodeConfig DefaultNodeWebDriver.json

