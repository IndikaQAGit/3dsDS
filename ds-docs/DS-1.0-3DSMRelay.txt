DS FSS data collection for 3DS Method relay and TACS.
DS 1.0.8 Date: 2018-01-07
(C) Modirum 2019

This short guide is supplemental information how to deploy 
external modules to support 3DS Method relay or TACS data collection functionality.

If DS is configured to work with 3DS Method relay
then this additionally depends on 3rd party modules like
	FingerprintJS2 and Evercookie.
	
Current DS version 1.0.8 has been developed with fingerprintjs2-2.0.3
and evercookie from master .

To make deployment straight forward and easy DS build combines live soures
from As fingerprintjs2 downloadable from vendor:
	https://github.com/Valve/fingerprintjs2/archive/2.0.3.zip
and from everkookie vendor
	https://github.com/samyk/evercookie/archive/master.zip

Evercookie however in addition to static scripts and files contains dynamic server code such as php
files or equivalent servlet substitutes. To be useable on tomcat/java based platform a deployable
war need to be created and java source code compiled (and optionally online patched some issues)
current project uses master distribution of evercookie and finally
packages them all together into a stand alone war module ddc.war (device data collection)
internally saparating
evercookie public files to evercookie folder and fingreprint files to fingerprintjs2 folder.
so if depoyed as is  dcc.war the URI locations on server will be
https:/mydomain.com/dcc/evercookie/*
https:/mydomain.com/dcc/fingreprintjs2/* 

present in this DS package as evercookie.war. 

For deployment on tomcat just copy the file ddc.war to /tomcat/webapps
no other actions or configuration not needed.

If You want to deploy those modules to non default locations
check DS 3ds method relay applicable settings from DS-1.0-Manager.pdf document
or appropriate TACS documentation.

Installation of FSS hmac key
use menu item 
pls Install a 256 bit secret key (for plugins)
if prompted for key alias "Enter key alias for this key:" then enter 
hmacKeyExt

