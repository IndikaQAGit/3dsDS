echo "Pushing Azure Images with DS_VERSION=${DS_VERSION}";

docker push modirum.azurecr.io/mdpayds:${DS_VERSION} & \
docker push modirum.azurecr.io/mdpayds-admin:${DS_VERSION}