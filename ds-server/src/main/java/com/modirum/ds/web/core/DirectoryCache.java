package com.modirum.ds.web.core;

import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.model.CacheObject;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.utils.http.HttpsJSSEClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DirectoryCache {

    private static final Logger log = LoggerFactory.getLogger(DirectoryCache.class);

    public static final int C_CACHE_PERIOD = 60 * 1000; // 1 min simple entities (wo ranges reload flags set)

    protected Map<Long, CacheObject<TDSServerProfile>> tdsServerCache;
    protected Map<String, CacheObject<TDSServerProfile>> tdsServerCacheRID;
    protected Map<String, CacheObject<Merchant>> merchantCache;
    protected Map<Integer, CacheObject<Issuer>> issuerCache;
    protected Map<String, CacheObject<Acquirer>> acquirerCache;
    protected Map<String, CacheObject<HttpsJSSEClient>> httpClientCache;
    protected Map<String, String> tableA6;
    protected Map<Integer, PresCardRangeCache> presCardRangeCacheMap;

    public DirectoryCache() {
        tdsServerCache = new HashMap<>();
        tdsServerCacheRID = new HashMap<>();
        merchantCache = new HashMap<>();
        issuerCache = new HashMap<>();
        acquirerCache = new HashMap<>();
        httpClientCache = new HashMap<>();
        presCardRangeCacheMap = new HashMap<>();

        tableA6 = new HashMap<>();
        for (TDSModel.TableA6Currencies ecu : TDSModel.TableA6Currencies.values()) {
            tableA6.put(ecu.value(), ecu.value());
        }
    }

    public void cleanCaches() {
        long currentTimeMillis = System.currentTimeMillis();
        long loadTime = currentTimeMillis - 10 * C_CACHE_PERIOD;
        int mir = 0, mer = 0, issr = 0, acqr = 0, clcn = 0;

        Iterator<CacheObject<TDSServerProfile>> tdsServerCacheIterator = getTDSServerCache().values().iterator();
        while (tdsServerCacheIterator.hasNext()) {
            CacheObject<TDSServerProfile> entry = tdsServerCacheIterator.next();
            if (entry.loaded < loadTime) {
                synchronized (getTDSServerCache()) {
                    tdsServerCacheIterator.remove();
                    mir++;
                }
            }
        }

        Iterator<CacheObject<TDSServerProfile>> tdsServerCacheRIDIterator = getTDSServerCacheRID().values().iterator();
        while (tdsServerCacheRIDIterator.hasNext()) {
            CacheObject<TDSServerProfile> entry = tdsServerCacheRIDIterator.next();
            if (entry.loaded < loadTime) {
                synchronized (getTDSServerCacheRID()) {
                    tdsServerCacheRIDIterator.remove();
                    mir++;
                }
            }
        }

        Iterator<Map.Entry<String, CacheObject<Merchant>>> merchantCacheIterator = getMerchantCache().entrySet().iterator();
        while (merchantCacheIterator.hasNext()) {
            Map.Entry<String, CacheObject<Merchant>> entry = merchantCacheIterator.next();
            if (entry.getValue().loaded < loadTime) {
                synchronized (getMerchantCache()) {
                    merchantCacheIterator.remove();
                    mer++;
                }
            }
        }

        Iterator<Map.Entry<Integer, CacheObject<Issuer>>> issuerCacheIterator = getIssuerCache().entrySet().iterator();
        while (issuerCacheIterator.hasNext()) {
            Map.Entry<Integer, CacheObject<Issuer>> entry = issuerCacheIterator.next();
            if (entry.getValue().loaded < loadTime) {
                synchronized (getIssuerCache()) {
                    issuerCacheIterator.remove();
                    issr++;
                }
            }
        }

        Iterator<Map.Entry<String, CacheObject<Acquirer>>> acquirerCacheIterator = getAcquirerCache().entrySet().iterator();
        while (acquirerCacheIterator.hasNext()) {
            Map.Entry<String, CacheObject<Acquirer>> entry = acquirerCacheIterator.next();
            if (entry.getValue().loaded < loadTime) {
                synchronized (getAcquirerCache()) {
                    acquirerCacheIterator.remove();
                    acqr++;
                }
            }
        }

        Iterator<Map.Entry<String, CacheObject<HttpsJSSEClient>>> httpClientCacheIterator = getHttpClientCache().entrySet().iterator();
        while (httpClientCacheIterator.hasNext()) {
            Map.Entry<String, CacheObject<HttpsJSSEClient>> entry = httpClientCacheIterator.next();
            if (entry.getValue().loaded < loadTime) {
                synchronized (getHttpClientCache()) {
                    httpClientCacheIterator.remove();
                    clcn++;
                }
            }
        }

        log.info(String.format("Caches cleaned in %s ms removed MI: %s, Mer: %s, Iss: %s , Acq: %s, HtCl: %s",
                (System.currentTimeMillis() - currentTimeMillis), mir, mer, issr, acqr, clcn));

    }

    public synchronized void clearCachesFast() {
        tdsServerCache.clear();
        tdsServerCacheRID.clear();
        merchantCache.clear();
        issuerCache.clear();
        acquirerCache.clear();
        httpClientCache.clear();
    }

    public Map<Long, CacheObject<TDSServerProfile>> getTDSServerCache() {
        return tdsServerCache;
    }

    public Map<String, CacheObject<TDSServerProfile>> getTDSServerCacheRID() {
        return tdsServerCacheRID;
    }

    public Map<String, CacheObject<Merchant>> getMerchantCache() {
        return merchantCache;
    }

    public Map<Integer, CacheObject<Issuer>> getIssuerCache() {
        return issuerCache;
    }

    public Map<String, CacheObject<Acquirer>> getAcquirerCache() {
        return acquirerCache;
    }

    public Map<String, CacheObject<HttpsJSSEClient>> getHttpClientCache() {
        return httpClientCache;
    }

    public Map<String, String> getTableA6() {
        return tableA6;
    }

    public Map<Integer, PresCardRangeCache> getPresCardRangeCacheMap() {
        return presCardRangeCacheMap;
    }

    public void setPresCardRangeCacheMap(Map<Integer, PresCardRangeCache> crCache) {
        this.presCardRangeCacheMap = crCache;
    }
}
