/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, https://www.modirum.com
 *
 * Created on 11. jaan 2016
 *
 */
package com.modirum.ds.web;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.ext.fss.ThreeDSMethodRelayService;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.CertificateData;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.model.ProductInfo;
import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.ErrorCode;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.model.Timings;
import com.modirum.ds.services.JsonMessageService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.MessageService.JsonAndDucplicates;
import com.modirum.ds.services.MessageService.PublicBOS;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.web.core.DSConfigurableSettings;
import com.modirum.ds.web.services.AbstractDirectoryService;
import com.modirum.ds.web.services.AbstractDirectoryService.Results;
import com.modirum.ds.web.services.Directory;
import com.modirum.ds.web.services.DirectoryService210;
import com.modirum.ds.web.services.DirectoryService220;
import com.modirum.ds.web.services.DirectoryService230;
import com.modirum.ds.web.services.MaintenanceDaemon;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import com.modirum.ds.web.context.WebContext;
import com.modirum.ds.web.servlets.BaseHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.json.JsonObject;
import javax.security.auth.x500.X500Principal;
import javax.servlet.HttpConstraintElement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletSecurityElement;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPOutputStream;

/**
 * Servlet that handles all DS requests and gives valid messages for DirectoryService for processing
 * additionally serves public keys to public (SDK key and scheme roots).
 */
public class DServer extends BaseHandler {

    public final static String cRemoteCertificateAttr = "remoteCertificate";
    public final static String cRemoteCertificateSDNAttr = "remoteCertificateSDN";
    public final static String cLocalCertificateDataAttr = "localCertificateData";
    public final static String cSSLAuthenticatedRole = "sslauthenticated";
    private static final Logger log = LoggerFactory.getLogger(DServer.class);
    private static final int MAX_REASONABLE_CONTENT_LENGTH = 1024 * 512; // acs html 100 KB alone!!!
    private static final long serialVersionUID = 1L;
    protected Charset charSet = StandardCharsets.UTF_8;
    private Directory directory;
    private DSConfigurableSettings dsSettings;
    private PersistenceService persistenceService;
    private MaintenanceDaemon maintenanceDaemon;
    private KeyService keyService;
    private JsonMessageService jsonMessageService;
    private boolean isDSService;

    private String sdkKeyURI = "/DSSDKKey";
    private String sdkECKeyURI = "/DSSDKECKey";
    private String rootCertURI = "/DSRoots";
    private ApplicationContext applicationContext;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        log.info(this.getServletName() + ": " + this.getClass().getSimpleName() + " " + ProductInfo.getBuildVersion() +
                 " " + DateUtil.formatDate(new Date(ProductInfo.getBuildDate()), "yyyy-MM-dd HH:mm") +
                 (ProductInfo.getCommitId() != null ? " commit-id: " + ProductInfo.getCommitId() : "") +
                 " Servlet init, logging started");

        WebContext.reset();
        WebContext webContext = new WebContext();
        webContext.setServlet(this);
        webContext.setServletContext(this.getServletContext());
        webContext.setServerIntanceId(Utils.getLocalNonLoIp());
        persistenceService = ServiceLocator.getInstance().getPersistenceService();
        dsSettings = new DSConfigurableSettings(persistenceService);
        webContext.setSettingService(persistenceService);
        keyService = ServiceLocator.getInstance().getKeyService();
        jsonMessageService = ServiceLocator.getInstance().getJsonMessageService();
        setApplicationContext();

        String charSetStr = this.getInitParameter("requestCharSet");
        if (Misc.isNotNullOrEmpty(charSetStr)) {
            charSet = Charset.forName(charSetStr);
        }

        boolean viaProxy = dsSettings.isViaProxyEnabled();
        log.info("Via Proxy: " + viaProxy);
        isDSService = "true".equals(this.getInitParameter("service"));

        if (!viaProxy && isDSService) {
            log.info("Setting up Tomcat TLS Auth");
            ServletRegistration.Dynamic registration = (ServletRegistration.Dynamic) this.getServletContext().getServletRegistration(
                    this.getServletName());
            HttpConstraintElement sc = new HttpConstraintElement(ServletSecurity.EmptyRoleSemantic.PERMIT,
                                                                 ServletSecurity.TransportGuarantee.CONFIDENTIAL,
                                                                 "sslauthenticated");
            ServletSecurityElement sel = new ServletSecurityElement(sc);
            registration.setServletSecurity(sel);
        }

        directory = getApplicationContext().getBean(Directory.class);

        log.info("DSServer is running as a service: " + isDSService);
        if (isDSService) {
            int maxPoolThreads = webContext.getIntSetting(DsSetting.DS_MAX_POOL_THREADS.getKey());
            if (maxPoolThreads > 10 && maxPoolThreads < 1001) {
                ServiceLocator.maxSchedulerThreads = maxPoolThreads;
            }

            directory.init(webContext);
            directory.checkLicensing();
            maintenanceDaemon = new MaintenanceDaemon(directory, webContext);
            maintenanceDaemon.start();

            String versionEnabled = webContext.getStringSetting(DsSetting.DS_VERSIONS_ENABLED.getKey());
            // This property is a space separated versions.
            String[] versionsStr = versionEnabled.split(" ");

            for (String versionStr : versionsStr) {
                TDSModel.MessageVersion version = TDSModel.MessageVersion.fromValue(versionStr);
                AbstractDirectoryService directoryService = null;
                switch (version) {
                    case V2_1_0:
                        directoryService = getApplicationContext().getBean("directoryService210",
                                                                           DirectoryService210.class);
                        break;
                    case V2_2_0:
                        directoryService = getApplicationContext().getBean("directoryService220",
                                                                           DirectoryService220.class);
                        break;
                    case V2_3_0:
                        directoryService = getApplicationContext().getBean("directoryService230",
                                                                           DirectoryService230.class);
                        break;
                    default:
                        log.warn("Directory service version = " + versionStr + " is not supported.");
                        break;
                }
                if (directoryService != null) {
                    directory.addDirectoryService(directoryService, version);
                }
            }

            // start card range caching/reloading thread job
            directory.initRangeLoading();

            // Start second and map public servlet dynamically
            ServletRegistration.Dynamic registration = webContext.getServletContext().addServlet("DSPublic",
                                                                                                 DServer.class.getName());
            log.info("Created Servlet " + registration.getName());

            String cSDKKeyURI = getServletContext().getInitParameter("SDKKeyURI");
            if (cSDKKeyURI != null) {
                sdkKeyURI = cSDKKeyURI;
            }
            if (Misc.isNotNullOrEmpty(sdkKeyURI)) {
                log.info("Registering " + registration.getName() + " mapping to uri: " + sdkKeyURI);
                registration.addMapping(sdkKeyURI);
            }

            String cSDKECKeyURI = getServletContext().getInitParameter("SDKECKeyURI");
            if (cSDKECKeyURI != null) {
                sdkECKeyURI = cSDKECKeyURI;
            }
            if (Misc.isNotNullOrEmpty(sdkECKeyURI)) {
                log.info("Registering " + registration.getName() + " mapping to uri: " + sdkECKeyURI);
                registration.addMapping(sdkECKeyURI);
            }

            String cRootCertURI = getServletContext().getInitParameter("SDKKeyURI");
            if (cRootCertURI != null) {
                rootCertURI = cRootCertURI;
            }

            if (Misc.isNotNullOrEmpty(rootCertURI)) {
                log.info("Registering " + registration.getName() + " mapping to uri: " + rootCertURI);
                registration.addMapping(rootCertURI);
            }

            if (dsSettings.isDs3DSMRelayEnabled()) {
                try {
                    if (directory.checkExtensionLicensing(ProductInfo.Extensions.tdsmrelay.name())) {
                        directory.setTdsmRelayService(new ThreeDSMethodRelayService(directory, webContext));
                        String uri = directory.getTdsmRelayService().getForwardURI();
                        if (Misc.isNotNullOrEmpty(uri)) {
                            log.info("Registering " + registration.getName() +
                                     " for 3DSM relay service to mapping to uri: " + uri + "/*");
                            registration.addMapping(uri + "/*");
                        }
                    } else {
                        log.warn("DS.3DSMRelayEnabled true but not licensed, so 3DSMRelay not activated");
                    }
                } catch (Throwable t) {
                    log.error("DS.3DSMRelay init failed with error ", t);
                }
            }
            registration.setInitParameter("service", "false");
            registration.setAsyncSupported(true);
            registration.setLoadOnStartup(2);
        }
        log.info(
                this.getServletName() + ": " + this.getClass().getSimpleName() + " " + webContext.getServerIntanceId() +
                " initializing done");
    }

    @Override
    public void destroy() {
        log.info("Servlet stopping - (" + getServletInfo() + ")");
        if (maintenanceDaemon != null) {
            maintenanceDaemon.stop();
        }
        super.destroy();
        log.info(this.getServletName() + " stopped, logging stoped as well.");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long now = System.currentTimeMillis();
        Timings timings = Timings.getInstance();
        directory.countRequest();
        timings.reqStart = now;
        req.setCharacterEncoding(charSet.name());
        String ct = req.getHeader("Content-type");
        int ctLen = req.getContentLength();

        String chunkedLogPrefix = "chunked".equals(req.getHeader("Transfer-Encoding")) ? " (chunked)" :  "";
        log.info("New {}: {} port={} content: {} {} {} bytes from {}", req.getMethod(), req.getRequestURI(),
                req.getServerPort(), ct, ctLen, chunkedLogPrefix, req.getRemoteAddr());

        req.setAttribute("requestlogged", Boolean.TRUE);
        debugRequest(req);
        try {
            if (isDSService) {
                handle3DS(req, resp, timings);
            } else {
                handlePublic(req, resp, timings);
            }
            long e = System.currentTimeMillis();
            timings.resEnd = (int) (e - now);
            directory.processingTime(e - now);
        } finally {
            if (timings.resEnd == 0) {
                timings.resEnd = (int) (System.currentTimeMillis() - now);
            }
            log.info("End total in " + timings.resEnd + (timings.reqEnd > 0 && timings.reqEnd < timings.resStart ?
                    "(self " + (timings.resEnd - (timings.resStart - timings.reqEnd)) + ")" : "") + " ms ");
            MDC.getMDCAdapter().clear();
        }
    }

    /**
     * Handle non authenticated other requests like servicing public keys.
     */
    public void handlePublic(HttpServletRequest req, HttpServletResponse resp, Timings timings) throws ServletException, IOException {
        // process 3DSM relay service if enabled
        if (directory.getTdsmRelayService() != null && Misc.isNotNullOrEmpty(directory.getTdsmRelayService().getForwardURI()) &&
            req.getRequestURI().contains(directory.getTdsmRelayService().getForwardURI())) {
            directory.getTdsmRelayService().handlePublic(req, resp, timings);
            return;
        } else if (Misc.isNotNullOrEmpty(sdkKeyURI) && req.getRequestURI().endsWith(sdkKeyURI)) {
            try {
                String ks = keyService.getSDKRSACertifcateString();
                if (ks != null) {
                    resp.setContentType("application/pkix-cert");
                    resp.getWriter().print(ks);
                    log.info("Sent SDK pub key cert.");
                } else {
                    resp.setContentType("text/plain");
                    resp.getWriter().print(
                            "SDK Key not installed " + DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm"));
                    log.warn("Send SDK pub key cert failed (SDK seems not exist, install with dstools).");
                }
            } catch (Exception e) {
                String eid = Context.getUnique();
                log.error("SDK key send error id " + eid, e);
                resp.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Internal failure, error id " + eid);
            }
            return;
        } else if (Misc.isNotNullOrEmpty(sdkECKeyURI) && req.getRequestURI().endsWith(sdkECKeyURI)) {
            try {
                String ks = keyService.getSDKECCertifcateString();
                if (ks != null) {
                    resp.setContentType("application/pkix-cert");
                    resp.getWriter().print(ks);
                    log.info("Sent SDK EC pub key cert.");
                } else {
                    resp.setContentType("text/plain");
                    resp.getWriter().print("SDK EC Key not installed " +
                                           DateUtil.formatDate(new java.util.Date(), "yyyy-MM-dd HH:mm"));
                    log.warn("Send SDK EC pub key cert failed (SDK seems not exist, install with dstools).");
                }
            } catch (Exception e) {
                String eid = Context.getUnique();
                log.error("SDK EC key send error id " + eid, e);
                resp.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Internal failure, error id " + eid);
            }
            return;
        } else if (Misc.isNotNullOrEmpty(rootCertURI) && req.getRequestURI().endsWith(rootCertURI)) {
            try {
                List<X509Certificate> roots = keyService.getSchemeRoots();
                if (roots != null && roots.size() > 0) {
                    resp.setContentType("application/x-x509-ca-cert");
                    for (X509Certificate rx : roots) {
                        resp.getWriter().println(KeyService.BEGIN_CERTIFICATE);
                        resp.getWriter().println(Base64.encode(rx.getEncoded()));
                        resp.getWriter().println(KeyService.END_CERTIFICATE);
                    }
                    log.info("Sent " + roots.size() + " Scheme root cert(s).");
                } else {
                    resp.setContentType("text/plain");
                    resp.getWriter().print("Scheme roots not installed " +
                                           DateUtil.formatDate(new java.util.Date(), "yyyy-MM-dd HH:mm"));
                    log.warn("Send Scheme roots failed (seems not exist, install with dstools).");
                }
            } catch (Exception e) {
                String eid = Context.getUnique();
                log.error("Roots send error id " + eid, e);
                resp.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Internal failure, error id " + eid);
            }
            return;
        }
        resp.sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    /**
     * Handle 3DS protocol messages with authentication
     */
    public void handle3DS(HttpServletRequest request, HttpServletResponse response, Timings timings) throws IOException {
        Date now = new Date();
        String contentType = request.getHeader("Content-type");
        int contentLength = request.getContentLength();
        boolean isChunked = "chunked".equals(request.getHeader("Transfer-Encoding"));
        boolean isGzipResponse = StringUtils.contains(request.getHeader("Accept-Encoding"), "gzip");

        if (contentType != null && contentType.toLowerCase().contains(MessageService.CT_JSON) && (contentLength >= 0 || isChunked)) {
            OutputStream responseOutputStream = response.getOutputStream();
            Results results = null;

            WebContext webContext = new WebContext();
            webContext.setServlet(this);
            webContext.setServletContext(this.getServletContext());
            webContext.setRequest(request);
            webContext.setResponse(response);
            webContext.setSettingService(persistenceService);

            AbstractDirectoryService activeDirectoryService = directory.getDirectoryService(
                    TDSModel.MessageVersion.V2_1_0);

            try {
                byte[] jsonContent = readContentFromRequest(request, contentLength, isChunked);

                if (isAuthenticated(request, webContext)) {
                    String[] json = new String[]{new String(jsonContent, charSet)};
                    TDSMessageBase tdsMessage = null;
                    String messageVersion = null;
                    try {
                        JsonAndDucplicates jsd = MessageService.parseJsonAndDups(json);
                        JsonObject jsonObj = jsd.json;

                        messageVersion = jsonObj != null && jsonObj.containsKey("messageVersion") ? jsonObj.getString(
                                "messageVersion") : null;

                        activeDirectoryService = directory.getDirectoryService(
                                TDSModel.MessageVersion.fromValue(messageVersion));
                        if (activeDirectoryService == null) {
                            // Load default version
                            activeDirectoryService = directory.getDirectoryService(
                                    getDsDefaultVersion(webContext));
                        }

                        log.info("Message version = " + messageVersion + ", service: " +
                                 activeDirectoryService.getClass().getSimpleName());
                        tdsMessage = activeDirectoryService.getMessageService().fromJSON(jsonObj);
                        tdsMessage.setDuplicateCounts(jsd.dups);
                        tdsMessage.setJsonObject(jsd.json);
                        tdsMessage.setIncomingJsonMessage(jsonMessageService.parse(jsd.json.toString()));

                        logTdsMessageInfo(request, tdsMessage);

                    } catch (Exception em) {

                        if (activeDirectoryService == null) {
                            activeDirectoryService = directory.getDirectoryService(getDsDefaultVersion(webContext));
                        }
                        if (em instanceof NullPointerException &&
                            activeDirectoryService.getMessageService().isInvalidStructureNPE(em)) {
                            results = activeDirectoryService.createErrorResults(
                                    activeDirectoryService.getMessageService().getSupportedVersion(),
                                    ErrorCode.cInvalidFieldFormat203, null, null,
                                    "Mismatching JSON structure failed to parse", null);
                        } else {
                            results = activeDirectoryService.createErrorResults(
                                    activeDirectoryService.getMessageService().getSupportedVersion(),
                                    ErrorCode.cInvalidMessageType101, null, null, "Message JSON syntax error", null);
                        }
                        StringBuilder jsn = new StringBuilder(json[0]);
                        MessageService.maskJSONStrValue(jsn, "acctNumber", 3);
                        log.warn("Error parsing tdsMessageBase: '{" + jsn + "}'", em);
                    }

                    if (tdsMessage != null) {
                        if (activeDirectoryService.getMessageService().isErrorMessage(tdsMessage)) {
                            if (tdsMessage.getAcctNumber() != null && tdsMessage.getAcctNumber().length() > 10) {
                                json[0] = Misc.replace(json[0], tdsMessage.getAcctNumber(),
                                                       Misc.mask(tdsMessage.getAcctNumber(), 6, 3));
                            }
                            log.warn("Got ERROR tdsMessageBase: {}", new Object[]{json[0]});
                            response.setStatus(HttpServletResponse.SC_OK);
                            TDSRecord rec = null;
                            if (Misc.isNotNullOrEmpty(tdsMessage.getDsTransID())) {
                                rec = ServiceLocator.getInstance().getPersistenceService().getTDSRecordByDSTransId(
                                        tdsMessage.getDsTransID());
                            }
                            if (rec == null) {
                                rec = activeDirectoryService.createNewTDSRecord(tdsMessage, now, webContext);
                                rec.setLocalStatus(DSModel.TSDRecord.Status.COMPLETED);
                                rec.setLocalDateEnd(new Date());
                                activeDirectoryService.safeUpdate(rec, -1, webContext);
                            }
                            activeDirectoryService.createNewTDSMessageData(tdsMessage, rec, json, now,
                                                                           request.getRemoteAddr(),
                                                                           "DS:" + activeDirectoryService.getDSID(),
                                                                           webContext);
                            return;
                        } else if (isAuthenticatedStage2(request, tdsMessage, webContext)) {
                            // DS received request from MPI and ACS and also sending messages to both.
                            results = activeDirectoryService.processRequest(tdsMessage, jsonContent, json, now,
                                                                            timings, webContext);
                        } else {
                            results = activeDirectoryService.createErrorResults(
                                    activeDirectoryService.getMessageService().getSupportedVersion(),
                                    ErrorCode.cAccessDenied303, null, null, "Authentication data or location mismatch",
                                    new String[]{tdsMessage.getSdkTransID(), tdsMessage.getTdsServerTransID(), null, tdsMessage.getAcsTransID()});
                        }
                    }
                } else {
                    results = activeDirectoryService.createErrorResults(
                            activeDirectoryService.getMessageService().getSupportedVersion(),
                            ErrorCode.cAccessDenied303, null, null, "Authentication failed", null);
                }

                PublicBOS emb = null;
                PublicBOS emb2 = null;
                if (results.jsonRaw != null) {
                    emb = results.jsonRaw;
                    if (results.jsonRaw2 != null) {
                        emb2 = results.jsonRaw2;
                    }
                }

                int total = (emb != null ? emb.getCount() : 0) + (emb2 != null ? emb2.getCount() : 0);

                logSendReplyInfo(results, total);

                response.setContentType(MessageService.CT_JSON_UTF8);

                if (isGzipResponse && TDSModel.MessageVersion.V2_3_0.value().equals(results.tdsMessageBase.getMessageVersion()) &&
                        TDSModel.XmessageType.P_RES.value().equals(results.tdsMessageBase.getMessageType())) {
                    response.setHeader("Content-Encoding", "gzip");
                    responseOutputStream = new GZIPOutputStream(responseOutputStream);
                }
                else {
                    isGzipResponse = false;
                    response.setContentLength(total);
                }

                responseOutputStream.write(emb.getBuf(), 0, emb.getCount());
                if (emb2 != null) {
                    responseOutputStream.write(emb2.getBuf(), 0, emb2.getCount());
                }
                response.flushBuffer();

                if (isGzipResponse) {
                    // client won't know all data is transferred unless outputStream is closed
                    responseOutputStream.close();
                }

            } catch (Exception e) {
                String eid = Context.getUnique();
                if (e instanceof IOException) {
                    log.error("Sending response error id " + eid, e);
                    if (results != null && results.tdsRecord != null) {
                        if (Misc.isNullOrEmpty(results.tdsRecord.getErrorDetail())) {
                            results.tdsRecord.setErrorDetail("Warning response may have failed, check log for " + eid);
                            activeDirectoryService.safeUpdate(results.tdsRecord, -1, webContext);
                        }
                    }
                } else {
                    log.error("DS Processing error id " + eid, e);
                    if (results != null && results.tdsRecord != null) {
                        if (Misc.isNullOrEmpty(results.tdsRecord.getErrorDetail())) {
                            results.tdsRecord.setErrorDetail(
                                    "DS Procssing Error id+" + eid + "," + e + " check log for more");
                            results.tdsRecord.setLocalStatus(DSModel.TSDRecord.Status.ERROR);
                            activeDirectoryService.safeUpdate(results.tdsRecord, -1, webContext);
                        }
                    }

                    try {
                        if (!response.isCommitted()) {
                            response.setContentType(MessageService.CT_JSON_UTF8);

                            if (activeDirectoryService == null) {
                                activeDirectoryService = directory.getDirectoryService(getDsDefaultVersion(webContext));
                            }
                            Results results2 = activeDirectoryService.createErrorResults(
                                    activeDirectoryService.getMessageService().getSupportedVersion(),
                                    ErrorCode.cPermanentSysFailure404, null, null, "System error, ref id " + eid, null);
                            if (results != null && results.tdsRecord != null) {
                                activeDirectoryService.createNewTDSMessageData(results2.tdsMessageBase,
                                                                               results.tdsRecord, results2.jsonRaw,
                                                                               new Date(),
                                                                               "DS:" + activeDirectoryService.getDSID(),
                                                                               request.getRemoteAddr(), webContext);
                            }

                            int total = (results2.jsonRaw != null ? results2.jsonRaw.getCount() : 0);
                            log.info("Sending reply on DS error " + results2.tdsMessageBase.getMessageVersion() + ":" +
                                     results2.tdsMessageBase.getMessageType() + " 3DSID: " +
                                     results2.tdsMessageBase.getTdsServerTransID() + ", DSID:" +
                                     results2.tdsMessageBase.getDsTransID() + ", ACSID: " +
                                     results2.tdsMessageBase.getAcsTransID() + " " + total + " bytes" +
                                     (results2.tdsMessageBase.getErrorCode() != null ?
                                             " Error: " + results2.tdsMessageBase.getErrorCode() + "/" +
                                             results2.tdsMessageBase.getErrorDetail() + " " +
                                             results2.tdsMessageBase.getErrorMessageType() + "/" +
                                             results2.tdsMessageBase.getErrorComponent() : "") +
                                     (results2.tdsMessageBase.getTransStatus() != null ?
                                             " transStatus: " + results2.tdsMessageBase.getTransStatus() + "/" +
                                             results2.tdsMessageBase.getTransStatusReason() : ""));

                            response.setContentType(MessageService.CT_JSON_UTF8);
                            response.setContentLength(total);
                            responseOutputStream.write(results2.jsonRaw.getBuf(), 0, results2.jsonRaw.getCount());
                        }
                    } catch (Exception ee) {
                        log.error("Errror sending error id " + eid, ee);
                        if (!response.isCommitted()) {
                            response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Internal failure");
                        }
                    }
                }
            }
        } else {
            String eid = Context.getUnique();

            if (contentLength >= 0 || isChunked) {
                log.error("Send HTTP " + HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE + " Invalid content type , id " +
                          eid + " ct=" + contentType + " ctl=" + contentLength + " chunked=" + isChunked);
                response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE,
                                   "Invalid content type, error id " + eid);
            } else {
                log.error(
                        "Send HTTP " + HttpServletResponse.SC_LENGTH_REQUIRED + " Invalid content length , id " + eid +
                        " ct=" + contentType + " ctl=" + contentLength + " chunked=" + isChunked);
                response.sendError(HttpServletResponse.SC_LENGTH_REQUIRED, "Invalid content length, error id " + eid);
            }
        }
    }

    private TDSModel.MessageVersion getDsDefaultVersion(WebContext webContext) {
        TDSModel.MessageVersion dsDefaultVersion = TDSModel.MessageVersion.fromValue(webContext.getStringSetting(DsSetting.DS_VERSION_DEFAULT.getKey()));
        return dsDefaultVersion != null ? dsDefaultVersion : TDSModel.MessageVersion.V2_1_0;
    }

    private void logSendReplyInfo(Results results, int total) {
        log.info("Sending reply " + results.tdsMessageBase.getMessageVersion() + ":" +
                 results.tdsMessageBase.getMessageType() + " 3DSID: " + results.tdsMessageBase.getTdsServerTransID() +
                 ", DSID:" + results.tdsMessageBase.getDsTransID() + ", ACSID: " +
                 results.tdsMessageBase.getAcsTransID() + " " + total + " bytes" +
                 (results.tdsMessageBase.getErrorCode() != null ?
                         " Error: " + results.tdsMessageBase.getErrorCode() + "/" +
                         results.tdsMessageBase.getErrorDetail() + " " + results.tdsMessageBase.getErrorMessageType() +
                         "/" + results.tdsMessageBase.getErrorComponent() : "") +
                 (results.tdsMessageBase.getTransStatus() != null ?
                         " transStatus: " + results.tdsMessageBase.getTransStatus() + "/" +
                         results.tdsMessageBase.getTransStatusReason() : ""));
    }

    private void logTdsMessageInfo(HttpServletRequest request, TDSMessageBase tdsMessage) {
        log.info("Received new tdsMessageBase: " + tdsMessage.getMessageType() + ", 3DSID = " +
                 tdsMessage.getTdsServerTransID() +
                 (tdsMessage.getDsTransID() != null ? ", DSID =" + tdsMessage.getDsTransID() : "") +
                 (tdsMessage.getAcsTransID() != null ? ", ACSID =" + tdsMessage.getAcsTransID() : "") + " from " +
                 request.getRemoteHost() + " user " + request.getRemoteUser());

        String ulTestCase = request.getHeader("x-ul-testcase-id");
        if (Misc.isNotNullOrEmpty(ulTestCase)) {
            log.info("UL Test Case id: " + ulTestCase + ", run id: " + request.getHeader("x-ul-testcaserun-id"));
        }
    }

    private byte[] readContentFromRequest(HttpServletRequest request, int contentLength, boolean chunked) throws IOException {
        byte[] jsonContent;
        int read = 0;
        ServletInputStream requestInputStream = request.getInputStream();
        if (chunked && contentLength <= 0) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream(8192);
            byte[] buf = new byte[4096];
            while (true) {
                int rdx = requestInputStream.read(buf);
                if (rdx < 0) {
                    break;
                }
                if (rdx > 0) {
                    read += rdx;
                    bos.write(buf, 0, rdx);
                }

                if (read >= MAX_REASONABLE_CONTENT_LENGTH) {
                    log.warn("Chunked content read " + read +
                             "bytes, actual content seems too long not reading any further, may be partial content!!!");
                    break;
                }
            }
            jsonContent = bos.toByteArray();
        } else {
            if (contentLength > MAX_REASONABLE_CONTENT_LENGTH) {
                // limit contentLength to maximum reasonable
                contentLength = MAX_REASONABLE_CONTENT_LENGTH;
                log.warn("Too long content lenght will read only first " + MAX_REASONABLE_CONTENT_LENGTH + " bytes..");
            }
            jsonContent = new byte[contentLength];

            int lastRead;
            do {
                lastRead = requestInputStream.read(jsonContent, read, contentLength - read);
                if (lastRead < 0) {
                    break;
                }
                read += lastRead;

            } while (read < contentLength);

            if (read != contentLength && read < MAX_REASONABLE_CONTENT_LENGTH) {
                log.warn("Content length was " + contentLength + " actual content read " + read +
                         " bytes, may be partial content!!!");
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Content read in " + read + " bytes");
        }
        return jsonContent;
    }

    /**
     * Check if it was authenticated.
     */
    protected boolean isAuthenticated(HttpServletRequest req, WebContext webContext) throws Exception {
        // below works with tomcat direct ssl authentication or via apache with ajp and
        //<Location /ds/DServer* >
        // SSLVerifyClient require
        // SSLVerifyDepth 2
        // SSLOptions +StdEnvVars +ExportCertData
        // only differnce is that in case of tomcat certificate chain is in javax.servlet.request.X509Certificate
        // in case of apache only client cetitficate is in javax.servlet.request.X509Certificate

        boolean viaProxy = webContext.isTrueSetting(DsSetting.VIA_PROXY_ENABLED.getKey());
        boolean sslCertsManagedExt = webContext.isTrueSetting(DsSetting.SSL_CERTS_MANAGED_EXTERNALLY.getKey());
        boolean containerAuth = false;
        String sdn = req.getRemoteUser();
        String clientAuthType = req.getAuthType();
        Object o = req.getAttribute("javax.servlet.request.X509Certificate");
        if (o != null) {
            X509Certificate[] certs = (X509Certificate[]) o;
            X509Certificate cert = certs[0];
            boolean acceptContainerCertAsAuth = "true".equals(webContext.getStringSetting(DsSetting.ACCEPT_CONTAINER_CERT_AS_AUTH.getKey()));
            if (acceptContainerCertAsAuth) {
                log.info("Accepting container certificate as successful auth auth (acceptContainerCertAsAuth=true)");
                clientAuthType = "CLIENT_CERT";
                containerAuth = true;
            }
            sdn = cert.getSubjectX500Principal().getName(X500Principal.RFC2253);
            log.info("Remote client cert SDN (tomcat): '" + sdn + "'");
            if (log.isDebugEnabled()) {
                log.debug("Remote client cert (tomcat): " + Base64.encode(certs[0].getEncoded()) + "");
            }
            req.setAttribute(cRemoteCertificateAttr, cert);
        } else if (viaProxy) {
            // trusting proxy variables
            String proxyType = webContext.getStringSetting(DsSetting.PROXY_TYPE.getKey());
            String clientAuthTLScert = null;
            if ("pound".equals(proxyType)) {
                clientAuthTLScert = req.getHeader("X-SSL-certificate");
                if (clientAuthTLScert != null) {
                    clientAuthType = "CLIENT_CERT";
                }
            } else if ("nginx".equals(proxyType)) {
                String rawCert = req.getHeader("X-SSL-Client-Cert");
                if (rawCert != null) {
                    clientAuthTLScert = URLDecoder.decode(rawCert, StandardCharsets.UTF_8.toString());
                    clientAuthType = "CLIENT_CERT";
                }
            } else {
                // apache
                clientAuthTLScert = req.getHeader("SSL_CLIENT_CERT");
                if (sdn != null && "SUCCESS".equals(req.getHeader("SSL_CLIENT_VERIFY"))) {
                    clientAuthType = "CLIENT_CERT";
                }
            }

            if (clientAuthTLScert != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Remote client cert: '" + clientAuthTLScert + "'");
                }
                clientAuthTLScert = Misc.replace(clientAuthTLScert, "-----BEGIN CERTIFICATE-----", "");
                clientAuthTLScert = Misc.replace(clientAuthTLScert, "-----END CERTIFICATE-----", "").trim();

                X509Certificate[] certs = KeyService.parseX509Certificates(Base64.decode(clientAuthTLScert), "X509");
                if (certs != null) {
                    req.setAttribute(cRemoteCertificateAttr, certs[0]);
                    sdn = certs[0].getSubjectX500Principal().getName(X500Principal.RFC2253);
                    log.info("Remote client cert SDN (proxy " + Misc.defaultString(proxyType) + ") '" + sdn + "'");
                }
            } else {
                log.warn("Remote client cert data was not present (via proxy " + Misc.defaultString(proxyType) + ")");
            }
        }

        req.setAttribute(cRemoteCertificateSDNAttr, sdn);

        if (sslCertsManagedExt) {
            // if TLS client authentication is performed outside of DS application (container),
            // then there is nothing to check anymore in DS.
            log.info("SSL certificates managed elsewhere (sslCertsManagedExt={}), skipping app level checks (stage1)",
                    sslCertsManagedExt);
            return true;
        }

        if ("CLIENT_CERT".equals(clientAuthType) && Misc.isNotNullOrEmpty(sdn)) {
            CertificateData cdLocal = persistenceService.getCertificateDataBySDN(sdn);
            if (cdLocal != null && CertificateData.Status.cValid.equals(cdLocal.getStatus()) &&
                cdLocal.getExpires() != null && cdLocal.getExpires().after(new java.util.Date()) &&
                (viaProxy || containerAuth ||
                 req.isUserInRole(cSSLAuthenticatedRole))) // role is from SSLRealm config and implementation
            {
                req.setAttribute("localCertificateData", cdLocal);
                return true;
            }
            if (cdLocal == null) {
                log.warn("Stage1 authentication failed locally stored certificate not found by sdn=" + sdn);
            } else if (!CertificateData.Status.cValid.equals(cdLocal.getStatus())) {
                log.warn("Stage1 authentication failed certificate sdn=" + sdn + " id=" + cdLocal.getId() +
                         " local status is=" + cdLocal.getStatus());
            } else if (!cdLocal.getExpires().after(new java.util.Date())) {
                log.warn("Stage1 authentication failed certificate sdn=" + sdn + " id=" + cdLocal.getId() +
                         " has been expired on =" + cdLocal.getExpires());
            } else if (!containerAuth && !req.isUserInRole(cSSLAuthenticatedRole)) // unlikely
            {
                log.warn("Stage1 authentication failed certificate sdn=" + sdn + " id=" + cdLocal.getId() +
                         " user is not in role " + cSSLAuthenticatedRole);
            }

            return false;
        }

        log.warn("Stage1 authentication failed sdn is missing sdn={} or not cert authenticated at={}",
                sdn, clientAuthType);
        return false;
    }

    public boolean checkDNSCN(HttpServletRequest req, String remCertSDN) {
        boolean dnsmatch = false;
        StringBuilder dnsnLookupIPs = new StringBuilder();
        StringBuilder logStr = new StringBuilder();
        String cn;
        String dnsn = req.getRemoteHost();
        if (remCertSDN != null) {
            // method 1  check if remotehost is in dnsn (if container resolves remote hosts)
            dnsmatch = dnsn != null && remCertSDN.contains(dnsn);
            if (!dnsmatch) {
                logStr.append(" 1: Hostname: '" + dnsn + "' not in sdn '" + remCertSDN + "'");
            } else {
                logStr.append(" 1: Hostname: '" + dnsn + "' is in sdn '" + remCertSDN + "'");
            }

            // method 2 if method 1 not match lookup cn
            if (!dnsmatch) {

                int cnstart = remCertSDN.indexOf("CN=");
                if (cnstart > -1) {
                    int cnend = remCertSDN.indexOf(",", cnstart);
                    cn = remCertSDN.substring(cnstart + 3, cnend > cnstart + 3 ? cnend : remCertSDN.length());

                    // 3 range
                    int[] iprange = Misc.parseIpV4AddressRange(cn);
                    if (iprange != null && iprange.length == 2) {
                        int remotevalue = Misc.parseIpV4Value(req.getRemoteAddr());
                        if (remotevalue >= iprange[0] && remotevalue <= iprange[1]) {
                            dnsmatch = true;
                        }

                        if (!dnsmatch) {
                            logStr.append(", 3: remote ip: " + req.getRemoteAddr() + " no in cn ip range(" + cn + ")");
                        } else {
                            logStr.append(", 3: remote ip: " + req.getRemoteAddr() + " is in cn ip range(" + cn + ")");
                        }
                    } else {
                        try {
                            InetAddress[] resovled = InetAddress.getAllByName(cn);
                            for (InetAddress rx : resovled) {
                                if (dnsnLookupIPs.length() > 0) {
                                    dnsnLookupIPs.append(";");
                                }
                                dnsnLookupIPs.append(rx.getHostAddress());

                                if (rx.getHostAddress().equals(req.getRemoteAddr())) {
                                    dnsmatch = true;
                                    break;
                                }
                            }
                        } catch (UnknownHostException e) {
                            log.warn("Sorry CN hostname: '" + cn + "' produced UnknownHostException", e);
                            dnsnLookupIPs.append(" " + cn + "=" + e);
                        }

                        if (!dnsmatch) {
                            logStr.append(", 2: nslookup(" + cn + ")=" + dnsnLookupIPs + "!=" + req.getRemoteAddr());
                        } else {
                            logStr.append(
                                    ", 2: nslookup(" + cn + ")=" + dnsnLookupIPs + " contains " + req.getRemoteAddr());
                        }
                    }

                } else {
                    logStr.append(" !!!NO CN IN SDN!!!");
                }
            }

        }
        if (!dnsmatch) {
            log.warn("Stage2 auth failed remote cert sdn/cn/hostname mismatch: " + logStr);
        } else {
            log.info("Stage2 auth passed remote cert sdn/cn/hostname match: " + logStr);
        }

        return dnsmatch;
    }

    /**
     * check if was authenticated and is valid compared to message contents.
     */
    protected boolean isAuthenticatedStage2(HttpServletRequest req, TDSMessageBase messageBase, WebContext webContext) throws Exception {
        String remoteCertificateSDN = (String) req.getAttribute(cRemoteCertificateSDNAttr);
        if (TDSModel.XmessageType.A_REQ.value().equals(messageBase.getMessageType()) ||
            TDSModel.XmessageType.P_REQ.value().equals(messageBase.getMessageType())) {
            String checkIpAcquirer = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_IP_MATCH_ACQUIRER.getKey());
            if ("true".equals(checkIpAcquirer)) {
                boolean ipmatch;
                String ip = req.getRemoteAddr();
                ipmatch = remoteCertificateSDN != null && ip != null && remoteCertificateSDN.contains(ip);
                if (!ipmatch) {
                    log.warn("Stage2 authentication failed remote ip mismatch=" + ip + " with sdn " +
                             remoteCertificateSDN);
                    return false;
                }
            }

            String checkDNSAcquirer = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_CN_DNS_MATCH_ACQUIRER.getKey());
            if ("true".equals(checkDNSAcquirer)) {
                if (!checkDNSCN(req, remoteCertificateSDN)) {
                    return false;
                }
            }
        }

        if (TDSModel.XmessageType.A_REQ.value().equals(messageBase.getMessageType())) {
            String checkBin = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_BIN_MATCH.getKey());
            String checkMid = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_IP_MATCH_ISSUER.getKey());
            String checkReqId = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_REQUESTOR_ID_MATCH.getKey());
            String checkOperId = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_OPERATOR_ID_MATCH.getKey());
            String bin;
            String mid;
            String requestorId;
            String operatorId;

            bin = messageBase.getAcquirerBIN();
            mid = messageBase.getAcquirerMerchantID();
            requestorId = messageBase.getTdsRequestorID();
            operatorId = messageBase.getThreeDSServerOperatorID();

            boolean binmatch = true;
            boolean midmatch = true;
            boolean rqidmatch = true;
            boolean opermatch = true;
            if ("true".equals(checkBin)) {
                binmatch = remoteCertificateSDN != null && bin != null && remoteCertificateSDN.contains(bin);
                if (!binmatch) {
                    log.warn("Stage2 authentication failed acquirer bin mismatch=" + bin + " with sdn " +
                             remoteCertificateSDN);
                }
            }

            if ("true".equals(checkMid) && mid != null) {
                midmatch = remoteCertificateSDN != null && remoteCertificateSDN.contains(mid);
                if (!midmatch) {
                    log.warn("Stage2 authentication failed mid mismatch=" + mid + " with sdn " + remoteCertificateSDN);
                }
            }

            if ("true".equals(checkReqId) && requestorId != null) {
                rqidmatch = remoteCertificateSDN != null && remoteCertificateSDN.contains(requestorId);
                if (!rqidmatch) {
                    log.warn("Stage2 authentication failed requestor id mismatch=" + requestorId + " with sdn " +
                             remoteCertificateSDN);
                }
            }

            if ("true".equals(checkOperId) && operatorId != null) {
                opermatch = remoteCertificateSDN != null && remoteCertificateSDN.contains(operatorId);
                if (!opermatch) {
                    log.warn("Stage2 authentication failed operator id mismatch=" + operatorId + " with sdn " +
                             remoteCertificateSDN);
                }
            }

            boolean sslCertsManagedExt = webContext.isTrueSetting(DsSetting.SSL_CERTS_MANAGED_EXTERNALLY.getKey());
            X509Certificate remoteCert = (X509Certificate) req.getAttribute(cRemoteCertificateAttr);

            boolean family;
            if (sslCertsManagedExt && remoteCert != null) {
                String misCerIssuerSDNs = webContext.getStringSetting(DsSetting.SSL_ACQUIRER_SDNS.getKey());
                if (Misc.isNullOrEmpty(misCerIssuerSDNs)) {
                    log.info("Stage2 success, sslCertsManagedExt=" + sslCertsManagedExt +
                             " setting 'sslAcquirerSDNs' not set " +
                             " certificate Acquirer/3DS family check skipped, all assumed ok");
                    family = true;
                } else if (("," + misCerIssuerSDNs + ",").contains(
                        remoteCert.getIssuerX500Principal().getName(X500Principal.RFC2253))) {
                    log.info("Stage2 success, sslCertsManagedExt=" + sslCertsManagedExt + " certificate issuerDN '" +
                             remoteCert.getIssuerX500Principal().getName(X500Principal.RFC2253) +
                             "' belongs to Acquirer/3DS family");
                    family = true;
                } else {
                    log.warn("Stage2 failure, sslCertsManagedExt=" + sslCertsManagedExt + " certificate issuerDN '" +
                             remoteCert.getIssuerX500Principal().getName(X500Principal.RFC2253) +
                             "' does not belong to Acquirer/3DS family defined in sslAcquirerSDNs='" +
                             misCerIssuerSDNs + "'");
                    family = false;
                }

                if (family) {
                    directory.certifcateExpirationChecks(remoteCert, remoteCertificateSDN);
                }
            } else {
                // no family checks, exact checks in
                family = true;
            }

            return binmatch && midmatch && rqidmatch && opermatch && family;
        } else if (TDSModel.XmessageType.R_REQ.value().equals(messageBase.getMessageType())) {
            String checkIpIssuer = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_IP_MATCH_ISSUER.getKey());
            if ("true".equals(checkIpIssuer)) {
                boolean ipmatch;
                String ip = req.getRemoteAddr();
                ipmatch = remoteCertificateSDN != null && ip != null && remoteCertificateSDN.contains(ip);
                if (!ipmatch) {
                    log.warn("Stage2 issuer authentication failed remote ip mismatch=" + ip + " with sdn " +
                             remoteCertificateSDN);
                    return false;
                }
            }

            String checkDNSIssuer = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_CN_DNS_MATCH_ISSUER.getKey());
            if ("true".equals(checkDNSIssuer)) {
                if (!checkDNSCN(req, remoteCertificateSDN)) {
                    return false;
                }
            }

            String checkACSRerNo = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_ACS_REFNO.getKey());
            if ("true".equals(checkACSRerNo)) {
                TDSRecord record = persistenceService.getTDSRecordByDSTransId(messageBase.getDsTransID());
                if (record != null && Misc.isNotNullOrEmpty(record.getACSRefNo())) {
                    boolean acsrefmatch =
                            remoteCertificateSDN != null && remoteCertificateSDN.contains(record.getACSRefNo());
                    if (!acsrefmatch) {
                        log.warn("Stage2 authentication failed tdsrecord issuer ACSRefNo '" + record.getACSRefNo() +
                                 "'" + " mismatch with issuer presented cert sdn " + remoteCertificateSDN);
                        return false;
                    } else {
                        log.info("Stage2 authentication validation tdsrecord issuer ACSRefNo '" + record.getACSRefNo() +
                                 "'" + " match with issuer presented cert sdn " + remoteCertificateSDN);
                    }
                } else {
                    log.warn("Stage2 authentication cert ACSRef validation skipped due " +
                             (record == null ? " tds record missing" : " tds record ACSRefNo missing"));
                }
            }

            String checkACSOpertorID = webContext.getStringSetting(DsSetting.STAGE2AUTHENTICATION_CHECK_SDN_ACS_OPERATORID.getKey());
            if ("true".equals(checkACSOpertorID)) {
                TDSRecord record = persistenceService.getTDSRecordByDSTransId(messageBase.getDsTransID());
                if (record != null && Misc.isNotNullOrEmpty(record.getACSOperatorID())) {
                    boolean acsoperidmatch =
                            remoteCertificateSDN != null && remoteCertificateSDN.contains(record.getACSOperatorID());
                    if (!acsoperidmatch) {
                        log.warn("Stage2 authentication failed tdsrecord issuer ACSOperatorID '" +
                                 record.getACSOperatorID() + "'" + " mismatch with issuer presented cert sdn " +
                                 remoteCertificateSDN);
                        return false;
                    } else {
                        log.info("Stage2 authentication validation tdsrecord issuer ACSOperatorID '" +
                                 record.getACSOperatorID() + "'" + " match with issuer presented cert sdn " +
                                 remoteCertificateSDN);
                    }
                } else {
                    log.warn("Stage2 authentication cert ACSOperatorID validation skipped due " +
                             (record == null ? " tds record missing" : " tds record ACSOperatorID missing"));
                }
            }

            CertificateData cd = (CertificateData) req.getAttribute(cLocalCertificateDataAttr);
            boolean sslCertsManagedExt = webContext.isTrueSetting(DsSetting.SSL_CERTS_MANAGED_EXTERNALLY.getKey());
            X509Certificate remoteCert = (X509Certificate) req.getAttribute(cRemoteCertificateAttr);

            if (sslCertsManagedExt && remoteCert != null) {
                String acsCerIssuerSDNs = webContext.getStringSetting(DsSetting.SSL_ISSUER_SDNS.getKey());
                if (Misc.isNullOrEmpty(acsCerIssuerSDNs)) {
                    log.info("Stage2 success, sslCertsManagedExt=" + sslCertsManagedExt +
                             " setting 'sslIssuerSDNs' is not set " +
                             " sslIssuerSDNs Issuer/ACS family check skipped, all assumed ok");
                    directory.certifcateExpirationChecks(remoteCert, remoteCertificateSDN);
                    return true;
                } else if (("," + acsCerIssuerSDNs + ",").contains(
                        remoteCert.getIssuerX500Principal().getName(X500Principal.RFC2253))) {
                    log.info("Stage2 success, sslCertsManagedExt=" + sslCertsManagedExt + " certificate issuerDN '" +
                             remoteCert.getIssuerX500Principal().getName(X500Principal.RFC2253) +
                             "' belongs to Issuer/ACS family");
                    directory.certifcateExpirationChecks(remoteCert, remoteCertificateSDN);
                    return true;
                } else {
                    log.warn("Stage2 failure, sslCertsManagedExt=" + sslCertsManagedExt + " certificate issuerDN '" +
                             remoteCert.getIssuerX500Principal().getName(X500Principal.RFC2253) +
                             "' does not belong to Issuer/ACS family as defined in sslIssuerSDNs='" + acsCerIssuerSDNs +
                             "'");
                    return false;
                }
            }

            if (sslCertsManagedExt) {
                // if TLS client authentication is performed outside of DS application (container),
                // then there is nothing to check anymore in DS.
                log.info("SSL certificates managed elsewhere (sslCertsManagedExt={}), skipping app level checks (stage2)",
                        sslCertsManagedExt);
                return true;
            }

            if (cd == null) {
                log.warn("Stage2 authentication failed, local certicate not found and sslCertsManagedExt=" +
                         sslCertsManagedExt);
                return false;
            }

            //find if valid acs by cert id
            List<ACSProfile> acsList = ServiceLocator.getInstance().getIssuerService().getACSListByCertId(cd.getId(),
                                                                                                          DSModel.ACSProfile.Status.ACTIVE,
                                                                                                          true);
            if (acsList.size() > 0) {
                log.info("Stage2 success, active issuer ACS conf with cert id=" + cd.getId() + "/" + cd.getSubjectDN() +
                         " found " + acsList.size());
                return true;
            } else {
                log.warn("Stage2 authentication failed, active issuer ACS conf with cert id=" + cd.getId() + "/" +
                         cd.getSubjectDN() + " was not found");
                return false;
            }
        } else {
            log.info("Stage2 contents match authentication skipped (not RReq or AReq)");
            return true;
        }
    }

    private void setApplicationContext() {
        applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    }

    private ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
