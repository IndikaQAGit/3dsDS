package com.modirum.ds.web.core;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.dao.PersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DSConfigurableSettings {

    private static final Logger log = LoggerFactory.getLogger(DSConfigurableSettings.class);

    private Date lastSettingsUpdated;
    private int lastSettingsCount;

    private final PersistenceService persistenceService;

    /**
     * Constructor.
     */
    public DSConfigurableSettings(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public boolean isSslCertsManagedExternally() {
        return getProperty(DsSetting.SSL_CERTS_MANAGED_EXTERNALLY.getKey());
    }

    public boolean isDSSupportRangeDiffUpdates() {
        return getProperty(DsSetting.DS_SUPPORT_RANGE_DIFF_UPDATES.getKey());
    }

    public boolean isViaProxyEnabled() {
        return getProperty(DsSetting.VIA_PROXY_ENABLED.getKey());
    }

    public boolean isDs3DSMRelayEnabled() {
        return getProperty(DsSetting.DS_3DSM_RELAY_ENABLED.getKey());
    }

    public boolean isTreatEmtpyValuesAsMissing() {
        return getProperty(DsSetting.TREAT_EMPTY_VALUE_AS_MISSING.getKey());
    }

    public boolean isVirtualACSEnabled() {
        return getProperty(DsSetting.VIRTUAL_ACS_SERVICE.getKey());
    }

    public boolean isAcceptedSDKListCheckSkipped() {
        return getProperty(DsSetting.SKIP_ACCEPTED_SDK_LIST_CHECK.getKey(), false);
    }

    public boolean isSettingsChanged() throws Exception {
        Map<String, Object> query = new HashMap<>();
        query.put("key!%", new String[]{"DS.expCerts", "DS.instanceinfo", "DS.timings"});

        List<Setting> sl = persistenceService.getPersitableList(Setting.class, "lastModified", "desc", true, 0, 1,
                                                                query);

        Long count = (Long) query.get("total");
        boolean countchanged = false;
        boolean lmfchanged = false;
        if (count != null && lastSettingsCount != count.intValue()) {
            countchanged = true;
            lastSettingsCount = count.intValue();
        }

        Setting lms = sl != null && sl.size() > 0 ? sl.get(0) : null;
        Date nowLastSettingsUpdated = lms != null ? lms.getLastModified() : null;
        if (lastSettingsUpdated == null ||
            nowLastSettingsUpdated != null && nowLastSettingsUpdated.after(lastSettingsUpdated)) {
            lastSettingsUpdated = nowLastSettingsUpdated;
            lmfchanged = true;
        }
        return countchanged || lmfchanged;
    }

    private boolean getProperty(String propertyName) {
        try {
            return "true".equals(persistenceService.getStringSetting(propertyName));
        } catch (Exception e) {
            log.warn("Setting " + propertyName + " is missing from databse.");
            return false;
        }
    }

    private boolean getProperty(String propertyName, boolean defaultValue) {
        try {
            return "true".equals(persistenceService.getStringSetting(propertyName));
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
