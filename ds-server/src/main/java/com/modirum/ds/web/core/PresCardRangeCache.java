package com.modirum.ds.web.core;

import com.modirum.ds.db.model.CardRange;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Cache Map for PRes Card Ranges where the key is the concatenated
 * start and end range of the card bin.
 */
public class PresCardRangeCache extends HashMap<String, CardRange> implements Serializable {
    private static final long serialVersionUID = 1L;
}
