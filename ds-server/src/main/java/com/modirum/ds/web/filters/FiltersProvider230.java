package com.modirum.ds.web.filters;

import com.modirum.ds.model.TDSModel;

import java.util.ArrayList;
import java.util.List;

public class FiltersProvider230 {

    /**
     * For forwarded messsages
     * MSGType + fieldname + Device + category
     * MSGType + fieldname + Device + category + status
     * MSGType + fieldname  (for all)
     */
    public static List<String> getFilters() {
        List<String> filters = new ArrayList<>();

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSMethodId." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSMethodId." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorChallengeInd." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorChallengeInd." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".acceptLanguage." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acceptLanguage." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCode." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCode." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCode." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCode." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCode." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCode." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCodeSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCodeSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCodeSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCodeSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCodeSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".acquirerCountryCodeSource." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".appIp." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".appIp." + "01.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationMethod." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationMethod." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationMethod." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationMethod." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationMethod." + "03.01.D");
        filters.add(TDSModel.XmessageType.A_RES.value + ".authenticationMethod." + "03.02.D");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatus." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatus." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatus." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatus." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatus." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatus." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatus." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatus." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatus." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatus." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatus." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatus." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".deviceBindingStatusSource." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceBindingStatusSource." + "03.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceInfoRecognisedVersion." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".deviceInfoRecognisedVersion." + "01.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".multiTransaction." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".multiTransaction." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".multiTransaction." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".multiTransaction." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkServerSignedContent." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkServerSignedContent." + "01.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkType." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".sdkType." + "01.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".taxId." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".taxId." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".taxId." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".taxId." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".taxId." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".taxId." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatus." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatus." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatus." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatus." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatus." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatus." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatus." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatus." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatus." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatus." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatus." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatus." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".trustListStatusSource." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".trustListStatusSource." + "03.02");

        return filters;
    }

}
