/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 01.12.2010
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.services;

import com.modirum.ds.utils.Misc;
import com.modirum.ds.web.context.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Maintenance Daemon to clean up transactions whose life in system needs to be terminated.
 */
public class MaintenanceDaemon implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(MaintenanceDaemon.class);

    private final WebContext ctx;
    private Thread daemonThread;
    private boolean keepRunning;
    private final long loopInterval = 60 * 1000;
    private final Directory directory;

    public MaintenanceDaemon(Directory dir, WebContext ctx) {
        this.ctx = ctx;
        this.directory = dir;
    }

    public synchronized void start() {
        daemonThread = new Thread(this);
        daemonThread.setDaemon(true);
        daemonThread.setName("DSMDaemon " + Misc.replace(daemonThread.getName(), "Thread-", "T-"));
        keepRunning = true;
        daemonThread.start();
    }

    public synchronized void stop() {
        keepRunning = false;
        if (daemonThread != null) {
            this.notifyAll();
            // wait some time to allow thread to terminate
            try {
                Thread.sleep(300);
            } catch (Exception dc) {
            }
            // if still running
            if (daemonThread.isAlive()) {
                daemonThread.interrupt();
                try {
                    Thread.sleep(100);
                } catch (Exception dc) {
                }
            }
        }
    }

    @Override
    public void run() {
        log.info("MaintenanceDaemon started");
        do {
            try {
                directory.periodicMaintenance(ctx);
            } catch (Throwable t) {
                log.error("Maintenance failed", t);
            } finally {
                if (keepRunning) {
                    synchronized (this) {
                        try {
                            this.wait(loopInterval);
                        } catch (InterruptedException dc) {
                        }
                    }
                }
            }
        } while (keepRunning);

        log.info("MaintenanceDaemon stopped");
    }

    protected void finalize() throws Throwable {
        stop();
        super.finalize();
    }
}
