package com.modirum.ds.web.job;

import com.modirum.ds.web.core.RequestsTiming;

public class TimingStoreJob implements Runnable {

    private final RequestsTiming timing;

    /**
     * Constructor.
     */
    public TimingStoreJob(RequestsTiming timing) {
        this.timing = timing;
    }

    @Override
    public void run() {
        timing.storeTimings();
    }
}