/*
 * Copyright (C) 2018 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 6. Oct 2018
 *
 */

package com.modirum.ds.web.services;

import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.model.ProductInfo;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.web.services.AbstractDirectoryService.Results;
import com.modirum.ds.web.context.WebContext;

import java.util.List;

/**
 * A interface to prepare custom extensions that can be added to messages
 * its an generic abstraction and actual implementations may do whatever nesessary
 * creating extensions itself may be optional
 * method shall block maximum to maxWit milliseconds and then return
 * so any processing taking longer shall be carried out asynchronously in other trhead.
 */
public interface ExtensionService {

    void createAReqExtensions(TDSRecord record, Results results, Merchant mer, Acquirer acq, CardRange bin, Issuer is, List<ACSProfile> acsList, TDSMessage aReq, DirectoryService210 dss, WebContext wctx);

    void createAResExtensions(TDSRecord record, Results results, Merchant mer, TDSMessage aRes, Long maxWait, DirectoryService210 dss, WebContext wctx);

    void createRReqExtensions(TDSRecord record, Results results, Merchant mer, TDSMessage aRes, Long maxWait, DirectoryService210 dss, WebContext wctx);

    void periodic(WebContext wctx);

    ProductInfo.Extensions getExtensionId();
}
