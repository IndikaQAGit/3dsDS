package com.modirum.ds.web.filters;

import com.modirum.ds.model.TDSModel;

import java.util.ArrayList;
import java.util.List;

public class FiltersProvider220 {

    /**
     * For forwarded messsages: MSGType + Fieldname + Category + Status
     */
    public static List<String> getFilters() {
        List<String> filters = new ArrayList<>();
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSReqAuthMethodInd." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSReqAuthMethodInd." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSReqAuthMethodInd." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSReqAuthMethodInd." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecMaxTime." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecMaxTime." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecMaxTime." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecMaxTime." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecMaxTime." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecMaxTime." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecReqInd." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecReqInd." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecReqInd." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecReqInd." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecReqInd." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".threeDSRequestorDecReqInd." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserJavascriptEnabled." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".browserJavascriptEnabled." + "02.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".payTokenSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".payTokenSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".payTokenSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".payTokenSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".payTokenSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".payTokenSource." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatus." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatus." + "03.02");
        filters.add(TDSModel.XmessageType.R_RES.value + ".whiteListStatus." + "01.01");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatus." + "01.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatus." + "01.02");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatus." + "02.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatus." + "02.02");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatus." + "03.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatus." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".whiteListStatusSource." + "03.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".whiteListStatusSource." + "03.02");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatusSource." + "01.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatusSource." + "01.02");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatusSource." + "02.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatusSource." + "02.02");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatusSource." + "03.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".whiteListStatusSource." + "03.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".acsDecConInd." + "01.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsDecConInd." + "01.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsDecConInd." + "02.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsDecConInd." + "02.02");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsDecConInd." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".acsDecConInd." + "03.02");

        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseInstalData." + "03.01");
        filters.add(TDSModel.XmessageType.A_REQ.value + ".purchaseInstalData." + "03.02");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".sdkTransID." + "01.01");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".sdkTransID." + "01.02");
        filters.add(TDSModel.XmessageType.R_RES.value + ".sdkTransID." + "01.01");
        filters.add(TDSModel.XmessageType.R_RES.value + ".sdkTransID." + "01.02");

        filters.add(TDSModel.XmessageType.A_RES.value +
                    ".authenticationType");//just for some cases to pass, needs refinement
        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType");

        filters.add(TDSModel.XmessageType.R_RES.value + ".resultsStatus." +
                    "03.01");//in 2.2 results status must be included in 3RI
        filters.add(TDSModel.XmessageType.R_RES.value + ".resultsStatus." + "03.02");


        filters.add(TDSModel.XmessageType.A_RES.value +
                    ".acsChallengeMandated");//just for some cases to pass, needs refinement

        filters.add(TDSModel.XmessageType.R_REQ.value + ".authenticationType." + "03.01.N");

        filters.add(TDSModel.XmessageType.A_RES.value + ".cardholderInfo." + "03.01");
        filters.add(TDSModel.XmessageType.A_RES.value + ".cardholderInfo." + "03.02");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.02.I");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "01.02.D");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.02.I");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "02.02.D");

        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.02.I");
        filters.add(TDSModel.XmessageType.A_RES.value + ".transStatusReason." + "03.02.D");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.02.I");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "01.02.D");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.02.I");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "02.02.D");

        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.02.I");
        filters.add(TDSModel.XmessageType.R_REQ.value + ".transStatusReason." + "03.02.D");
        return filters;
    }
}
