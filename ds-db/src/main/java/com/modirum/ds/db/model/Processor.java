/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 14.02.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import com.modirum.ds.db.model.Persistable;
import com.modirum.ds.db.model.PersistableClass;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class Processor extends PersistableClass implements Serializable, Persistable {

    private static final long serialVersionUID = 1L;

    private Short id;
    private String name;
    private String contactInfo;
    private String email;
    private String webSite;
    private String serviceDomain;
    private String logoImage;
    private String styleSheet;
    private String paymentXSLTemplate;
    private Boolean active;
    private Boolean master;

    private Long loaded; // transient for caching

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogoImage() {
        return logoImage;
    }

    public void setLogoImage(String logoImage) {
        this.logoImage = logoImage;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getStyleSheet() {
        return styleSheet;
    }

    public void setStyleSheet(String styleSheet) {
        this.styleSheet = styleSheet;
    }

    public String getPaymentXSLTemplate() {
        return paymentXSLTemplate;
    }

    public void setPaymentXSLTemplate(String paymentXSLTemplate) {
        this.paymentXSLTemplate = paymentXSLTemplate;
    }

    public Boolean getMaster() {
        return master;
    }

    public void setMaster(Boolean master) {
        this.master = master;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getServiceDomain() {
        return serviceDomain;
    }

    public void setServiceDomain(String serviceDomain) {
        this.serviceDomain = serviceDomain;
    }

    public Long getLoaded() {
        return loaded;
    }

    public void setLoaded(Long loaded) {
        this.loaded = loaded;
    }
}
