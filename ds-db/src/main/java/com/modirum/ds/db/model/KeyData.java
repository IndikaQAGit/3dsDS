/*
 * Copyright (C) 2014 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 27.01.2014
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import java.util.Date;

public class KeyData extends PersistableClass implements Persistable {

    public interface KeyStatus {
        String working = "working";
        String pending ="pending";
        String migrating = "migrating";
        String retired = "retired";
        String obsolete = "obsolete";
    }

    private Long id;
    private String keyAlias;
    private String keyData;
    private Long hsmDeviceId;
    private Long wrapkeyId;
    private String keyCheckValue;
    private Date keyDate;
    private Integer cryptoPeriodDays;
    private String status;
    private String signedBy1;
    private String signature1;
    private String signedBy2;
    private String signature2;

    public String getKeyAlias() {
        return keyAlias;
    }

    public void setKeyAlias(String keyAlias) {
        this.keyAlias = keyAlias;
    }

    public String getKeyData() {
        return keyData;
    }

    public void setKeyData(String keyData) {
        this.keyData = keyData;
    }

    public Long getHsmDeviceId() {
        return hsmDeviceId;
    }

    public void setHsmDeviceId(Long hsmDeviceId) {
        this.hsmDeviceId = hsmDeviceId;
    }

    public Long getWrapkeyId() {
        return wrapkeyId;
    }

    public void setWrapkeyId(Long wrapkeyId) {
        this.wrapkeyId = wrapkeyId;
    }

    public String getKeyCheckValue() {
        return keyCheckValue;
    }

    public void setKeyCheckValue(String keyCheckValue) {
        this.keyCheckValue = keyCheckValue;
    }

    public Date getKeyDate() {
        return keyDate;
    }

    public void setKeyDate(Date keyDate) {
        this.keyDate = keyDate;
    }

    public Integer getCryptoPeriodDays() {
        return cryptoPeriodDays;
    }

    public void setCryptoPeriodDays(Integer cryptoPeriodDays) {
        this.cryptoPeriodDays = cryptoPeriodDays;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSignedBy1() {
        return signedBy1;
    }

    public void setSignedBy1(String signedBy1) {
        this.signedBy1 = signedBy1;
    }

    /*
     * public String getSignCert1() { return signCert1; } public void setSignCert1(String signCert1) { this.signCert1 = signCert1; }
     */
    public String getSignature1() {
        return signature1;
    }

    public void setSignature1(String signature1) {
        this.signature1 = signature1;
    }

    public String getSignedBy2() {
        return signedBy2;
    }

    public void setSignedBy2(String signedBy2) {
        this.signedBy2 = signedBy2;
    }

    public String getSignature2() {
        return signature2;
    }

    public void setSignature2(String signature2) {
        this.signature2 = signature2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
