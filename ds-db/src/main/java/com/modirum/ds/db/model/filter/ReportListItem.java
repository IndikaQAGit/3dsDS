package com.modirum.ds.db.model.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportListItem {

    private Integer paymentSystemId;
    private Long total;
    private Integer start;
    private Integer limit;

}
