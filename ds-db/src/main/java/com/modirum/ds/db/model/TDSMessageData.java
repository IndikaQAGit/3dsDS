/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 04.04.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TDSMessageData extends PersistableClass implements Serializable {

    private static final long serialVersionUID = 1L;

    protected Long id;
    protected Long tdsrId;
    protected String messageType;
    protected String messageVersion;
    protected String sourceIP;
    protected String destIP;
    protected Date messageDate;
    protected String contents;

}
