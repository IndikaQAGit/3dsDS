package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TDSRecord extends PersistableClass implements PaymentSystemResource, Serializable {

    private static final long serialVersionUID = 1L;

    protected Long id;

    protected String acquirerBIN;
    protected String acquirerMerchantID;
    protected String ACSOperatorID;
    protected String ACSRefNo;
    protected String ACSTransID;
    protected String ACSURL;
    protected String acctNumber;
    protected String acctNumberEnc;
    protected Long acctNumberKid;
    protected String acctNumberHMAC;
    protected Long acctNumberHMKid;
    protected String authenticationMethod;
    protected String authenticationType;
    protected String authenticationValue;

    protected String browserIP;
    protected String browserJavaEnabled;
    protected String browserLanguage;
    protected String browserUserAgent;
    protected String cardExpiryDate;
    protected String deviceChannel;
    protected String dsRef;
    protected String DSTransID;
    protected String errorCode;
    protected String errorDetail;
    private String errorDescription;
    private String errorResolution;
    protected String ECI;
    protected String issuerBIN;
    protected Date localDateStart;
    protected Date localDateEnd;
    protected Short localStatus;
    protected Integer localIssuerId;
    protected Integer localAcquirerId;
    protected String requestorName;
    protected String requestorID;
    protected String requestorURL;
    protected String MCC;
    protected Short merchantCountry;
    protected String merchantName;
    protected String merchantRiskIndicator;

    protected String messageCategory;
    protected String tdsTransID;
    protected String MIReferenceNumber;
    protected String tdsServerURL;
    protected Long MIProfileId;

    protected String protocol;

    protected Long purchaseAmount;
    protected Short purchaseCurrency;
    protected Short purchaseExponent;
    protected Date purchaseDate;
    protected Short purchaseInstalData;

    protected String recurringExpiry;
    protected Short recurringFrequency;
    protected String resultsStatus;
    protected String SDKAppID;
    protected String SDKReferenceNumber;
    protected String SDKTransID;
    protected String transType;
    protected String transStatus;
    protected String transStatusReason;
    protected String issuerOptions;
    protected String acquirerOptions;
    private List<TDSMessageData> tdsMessageDataList;
    private Integer paymentSystemId;

    public String getIssuerOptions() {
        return issuerOptions;
    }

    public void setIssuerOptions(String issuerOptions) {
        this.issuerOptions = issuerOptions;
    }

    public String getAcquirerOptions() {
        return acquirerOptions;
    }

    public void setAcquirerOptions(String acquirerOptions) {
        this.acquirerOptions = acquirerOptions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDsRef() {
        return dsRef;
    }

    public void setDsRef(String dsRef) {
        this.dsRef = dsRef;
    }

    public String getAcquirerBIN() {
        return acquirerBIN;
    }

    public void setAcquirerBIN(String acquirerBIN) {
        this.acquirerBIN = acquirerBIN;
    }

    public String getAcquirerMerchantID() {
        return acquirerMerchantID;
    }

    public void setAcquirerMerchantID(String acquirerMerchantID) {
        this.acquirerMerchantID = acquirerMerchantID;
    }

    public String getACSRefNo() {
        return ACSRefNo;
    }

    public void setACSRefNo(String acsRef) {
        this.ACSRefNo = acsRef;
    }

    public String getACSURL() {
        return ACSURL;
    }

    public void setACSURL(String acsUrl) {
        this.ACSURL = acsUrl;
    }

    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }

    public String getAuthenticationValue() {
        return authenticationValue;
    }

    public void setAuthenticationValue(String authenticationValue) {
        this.authenticationValue = authenticationValue;
    }

    public String getBrowserJavaEnabled() {
        return browserJavaEnabled;
    }

    public void setBrowserJavaEnabled(String browserJavaEnabled) {
        this.browserJavaEnabled = browserJavaEnabled;
    }

    public String getBrowserLanguage() {
        return browserLanguage;
    }

    public void setBrowserLanguage(String browserLanguage) {
        this.browserLanguage = browserLanguage;
    }

    public String getBrowserUserAgent() {
        return browserUserAgent;
    }

    public void setBrowserUserAgent(String browserUserAgent) {
        this.browserUserAgent = browserUserAgent;
    }

    public String getDeviceChannel() {
        return deviceChannel;
    }

    public void setDeviceChannel(String deviceChannel) {
        this.deviceChannel = deviceChannel;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorResolution() {
        return errorResolution;
    }

    public void setErrorResolution(String errorResolution) {
        this.errorResolution = errorResolution;
    }

    public String getECI() {
        return ECI;
    }

    public void setECI(String eCI) {
        ECI = eCI;
    }

    public String getIssuerBIN() {
        return issuerBIN;
    }

    public void setIssuerBIN(String issuerBIN) {
        this.issuerBIN = issuerBIN;
    }

    /*
        public String getIreqCode()
        {
            return ireqCode;
        }
        public void setIreqCode(String ireqCode)
        {
            this.ireqCode = ireqCode;
        }
        public String getIreqDetail()
        {
            return ireqDetail;
        }
        public void setIreqDetail(String ireqDetail)
        {
            this.ireqDetail = ireqDetail;
        }
        */
    public Short getMerchantCountry() {
        return merchantCountry;
    }

    public void setMerchantCountry(Short merchantCountry) {
        this.merchantCountry = merchantCountry;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantRiskIndicator() {
        return merchantRiskIndicator;
    }

    public void setMerchantRiskIndicator(String merchantRiskIndicator) {
        this.merchantRiskIndicator = merchantRiskIndicator;
    }

    public Long getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(Long purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    public Short getPurchaseCurrency() {
        return purchaseCurrency;
    }

    public void setPurchaseCurrency(Short purchaseCurrency) {
        this.purchaseCurrency = purchaseCurrency;
    }

    public Short getPurchaseExponent() {
        return purchaseExponent;
    }

    public void setPurchaseExponent(Short purchaseExponent) {
        this.purchaseExponent = purchaseExponent;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Short getPurchaseInstalData() {
        return purchaseInstalData;
    }

    public void setPurchaseInstalData(Short purchaseInstalData) {
        this.purchaseInstalData = purchaseInstalData;
    }

    public String getRecurringExpiry() {
        return recurringExpiry;
    }

    public void setRecurringExpiry(String recurringExpiry) {
        this.recurringExpiry = recurringExpiry;
    }

    public Short getRecurringFrequency() {
        return recurringFrequency;
    }

    public void setRecurringFrequency(Short recurringFrequency) {
        this.recurringFrequency = recurringFrequency;
    }

    public String getResultsStatus() {
        return resultsStatus;
    }

    public void setResultsStatus(String resultsStatus) {
        this.resultsStatus = resultsStatus;
    }

    public String getSDKAppID() {
        return SDKAppID;
    }

    public void setSDKAppID(String sdkAppID) {
        this.SDKAppID = sdkAppID;
    }

    public String getSDKReferenceNumber() {
        return SDKReferenceNumber;
    }

    public void setSDKReferenceNumber(String sdkReferenceNumber) {
        this.SDKReferenceNumber = sdkReferenceNumber;
    }

    public String getSDKTransID() {
        return SDKTransID;
    }

    public void setSDKTransID(String sdkTransID) {
        this.SDKTransID = sdkTransID;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getAuthenticationMethod() {
        return authenticationMethod;
    }

    public void setAuthenticationMethod(String authenticationMethod) {
        this.authenticationMethod = authenticationMethod;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    public String getCardExpiryDate() {
        return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
        this.cardExpiryDate = cardExpiryDate;
    }

    public String getMCC() {
        return MCC;
    }

    public void setMCC(String mCC) {
        MCC = mCC;
    }

    public String getTdsTransID() {
        return tdsTransID;
    }

    public void setTdsTransID(String tdsTransID) {
        this.tdsTransID = tdsTransID;
    }

    public String getMIReferenceNumber() {
        return MIReferenceNumber;
    }

    public void setMIReferenceNumber(String mIReferenceNumber) {
        MIReferenceNumber = mIReferenceNumber;
    }

    public String getTdsServerURL() {
        return tdsServerURL;
    }

    public void setTdsServerURL(String tdsServerURL) {
        this.tdsServerURL = tdsServerURL;
    }

    public String getTransStatusReason() {
        return transStatusReason;
    }

    public void setTransStatusReason(String transStatusReason) {
        this.transStatusReason = transStatusReason;
    }

    public String getAcctNumberEnc() {
        return acctNumberEnc;
    }

    public void setAcctNumberEnc(String acctNumberEnc) {
        this.acctNumberEnc = acctNumberEnc;
    }

    public String getAcctNumberHMAC() {
        return acctNumberHMAC;
    }

    public void setAcctNumberHMAC(String acctNumberHmac) {
        this.acctNumberHMAC = acctNumberHmac;
    }

    public String getMessageCategory() {
        return messageCategory;
    }

    public void setMessageCategory(String messageCategory) {
        this.messageCategory = messageCategory;
    }

    public Date getLocalDateStart() {
        return localDateStart;
    }

    public void setLocalDateStart(Date localDateStart) {
        this.localDateStart = localDateStart;
    }

    public Date getLocalDateEnd() {
        return localDateEnd;
    }

    public void setLocalDateEnd(Date localDateEnd) {
        this.localDateEnd = localDateEnd;
    }

    public Short getLocalStatus() {
        return localStatus;
    }

    public void setLocalStatus(Short localStatus) {
        this.localStatus = localStatus;
    }

    public String getACSTransID() {
        return ACSTransID;
    }

    public void setACSTransID(String aCSAccountID) {
        ACSTransID = aCSAccountID;
    }

    public Long getMIProfileId() {
        return MIProfileId;
    }

    public void setMIProfileId(Long mIProfileId) {
        MIProfileId = mIProfileId;
    }

    public String getDSTransID() {
        return DSTransID;
    }

    public void setDSTransID(String dSTransID) {
        DSTransID = dSTransID;
    }

    public Integer getLocalIssuerId() {
        return localIssuerId;
    }

    public void setLocalIssuerId(Integer localIssuerId) {
        this.localIssuerId = localIssuerId;
    }

    public Integer getLocalAcquirerId() {
        return localAcquirerId;
    }

    public void setLocalAcquirerId(Integer localAcquirerId) {
        this.localAcquirerId = localAcquirerId;
    }

    public Long getAcctNumberKid() {
        return acctNumberKid;
    }

    public void setAcctNumberKid(Long acctNumberKid) {
        this.acctNumberKid = acctNumberKid;
    }

    public Long getAcctNumberHMKid() {
        return acctNumberHMKid;
    }

    public void setAcctNumberHMKid(Long acctNumberHMKid) {
        this.acctNumberHMKid = acctNumberHMKid;
    }

    public String getBrowserIP() {
        return browserIP;
    }

    public void setBrowserIP(String browserIP) {
        this.browserIP = browserIP;
    }

    public String getRequestorName() {
        return requestorName;
    }

    public void setRequestorName(String requestorName) {
        this.requestorName = requestorName;
    }

    public String getRequestorID() {
        return requestorID;
    }

    public void setRequestorID(String requestorID) {
        this.requestorID = requestorID;
    }

    public String getRequestorURL() {
        return requestorURL;
    }

    public void setRequestorURL(String requestorURL) {
        this.requestorURL = requestorURL;
    }

    public String getACSOperatorID() {
        return ACSOperatorID;
    }

    public void setACSOperatorID(String aCSOperatorID) {
        ACSOperatorID = aCSOperatorID;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public List<TDSMessageData> getTdsMessageDataList() {
        return tdsMessageDataList;
    }

    public void setTdsMessageDataList(List<TDSMessageData> tdsMessageDataList) {
        this.tdsMessageDataList = tdsMessageDataList;
    }

    @Override
    public Integer getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }
}
