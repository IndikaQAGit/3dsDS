package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.time.LocalDateTime;

@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class Issuer extends SchemeSubject implements PaymentSystemResource {

    Integer attemptsACSProId;
    private Integer paymentSystemId;
    @Getter @Setter private LocalDateTime createdDate;
    @Getter @Setter private LocalDateTime lastModifiedDate;

    public Integer getAttemptsACSProId() {
        return attemptsACSProId;
    }

    public void setAttemptsACSProId(Integer attemptsACSProId) {
        this.attemptsACSProId = attemptsACSProId;
    }

    @Override
    public Integer getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }
}
