/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 18.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.dao;

import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.AuditLog;
import com.modirum.ds.db.model.AuditLogSearcher;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.CertificateData;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.HsmDevice;
import com.modirum.ds.db.model.HsmDeviceConf;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.License;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.Persistable;
import com.modirum.ds.db.model.Report;
import com.modirum.ds.db.model.Searcher;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.model.SettingSearcher;
import com.modirum.ds.db.model.TDSMessageData;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.db.model.TDSRecordSearcher;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.db.model.Text;
import com.modirum.ds.db.model.TextSearcher;
import com.modirum.ds.db.model.User;
import com.modirum.ds.db.model.UserSearchFilter;
import com.modirum.ds.db.model.filter.ReportListItem;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.SettingService;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.dialect.Dialect;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.query.Query;
import org.hibernate.type.TimestampType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

/**
 * Service to handle persistence of VPOS model objects
 *
 * @author andri
 * @version $Revision: 1.0 $ $Date$
 */
public class PersistenceService implements SettingService, Serializable {

    public static final String IS_NULL_OPERATOR = "NULL";
    protected transient static Logger log = LoggerFactory.getLogger(PersistenceService.class);

    private static final String[] mappings = {"AuditLog.hbm.xml", "CardRange.hbm.xml", "CertificateData.hbm.xml",
                                              "Id.hbm.xml", "Issuer.hbm.xml", "Acquirer.hbm.xml", "KeyData.hbm.xml", "Setting.hbm.xml",
                                              "TDSRecord.hbm.xml", "TDSMessageData.hbm.xml", "Text.hbm.xml", "User.hbm.xml", "TDSServerProfile.hbm.xml",
                                              "ACSProfile.hbm.xml", "Merchant.hbm.xml", "HsmDevice.hbm.xml", "HsmDeviceConf.hbm.xml", "PaymentSystem.hbm.xml",
                                              "License.hbm.xml", "Report.hbm.xml"};

    private static final long serialVersionUID = 1L;

    private static SessionFactory sessionFactory;

    protected int transactionTimeOut = 30;
    protected transient Configuration cfg = null;

    public PersistenceService() {
    }

    private SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = buildSessionFactory();
        }
        return sessionFactory;
    }

    public StatelessSession getStatelessSession() {
        return getSessionFactory().openStatelessSession();
    }

    private Session getCurrentSession() {
        return getSessionFactory().getCurrentSession();
    }

    public void configureFromProperties(Properties properties) {
        try {
            if (cfg == null) {
                Configuration cfgtemp = new Configuration();
                for (String res : mappings) {
                    cfgtemp.addResource(res);
                }

                if (properties != null) {
                    cfgtemp.addProperties(properties);
                }

                cfgtemp.configure();
                cfg = cfgtemp;
            } else if (properties != null) {
                cfg.addProperties(properties);
                cfg.configure();
            }
        } catch (Throwable ex) {
            log.error("Exception during configuring hibernate" + (properties != null ? " from properties" : ""), ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    private SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            configureFromProperties(null);
            return cfg.buildSessionFactory();
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            log.error("Initial SessionFactory creation failed.", ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * @param id setting key
     * @return setting if found
     */
    public Setting getSettingById(String id) {
        if (id != null) {
            Setting s = null;
            Session session = getCurrentSession();
            Transaction tx = session.getTransaction();
            tx.setTimeout(this.transactionTimeOut);
            try {
                SessionUtils.txBegin(session);
                Query<Setting> findSettByIdAndProcId = session.createQuery("from Setting s where s.key=:key" + "",
                                                                           Setting.class);
                findSettByIdAndProcId.setParameter("key", id);
                List<Setting> sl = findSettByIdAndProcId.list();
                tx.commit();

                if (sl.size() > 0) {
                    s = sl.get(0);
                }
                return s;
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    public <T extends Persistable> T getPersistableById(Serializable id, Class<T> persistable) throws Exception {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);

        T tr = null;
        try {
            SessionUtils.txBegin(session);
            tr = session.get(persistable, id);
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return tr;
    }

    // Only use this method if you think the client won't be
    // able to recover with DB error otherwise use getPersistableById
    public <T extends Persistable> T getPersistableByIdUnchecked(Serializable id, Class<T> persistable) {
        T tr = null;
        try {
            tr = getPersistableById(id, persistable);
        } catch (Exception e) {
            log.error("Error encountered while invoking getPersistableById for {} with id {}",
                      persistable.getSimpleName(), id.toString());
            log.error("Error: ", e);
            throw new RuntimeException(e);
        }
        return tr;
    }

    public String getStringSetting(String key) throws Exception {
        Setting s = getSettingById(key);

        return s != null ? s.getValue() : null;
    }

    public boolean isSetting(String key) {
        Setting s = getSettingById(key);

        return s != null ? Boolean.parseBoolean(s.getValue()) : false;
    }

    @SuppressWarnings("unchecked")
    public List<Setting> getSettingsByIdStart(String idStart, String order, Integer limit) throws Exception {
        List<Setting> settings = null;
        if (idStart != null && idStart.length() > 0) {
            Session session = getCurrentSession();
            Transaction tx = session.getTransaction();
            tx.setTimeout(this.transactionTimeOut);

            String orderHQL = "";
            if ("keyasc".equals(order)) {
                orderHQL = " order by s.key";
            } else if ("keydesc".equals(order)) {
                orderHQL = " order by s.key desc";
            } else if ("lastModifiedasc".equals(order)) {
                orderHQL = " order by s.lastModified";
            } else if ("lastModifieddesc".equals(order)) {
                orderHQL = " order by s.lastModified desc";
            }


            try {
                SessionUtils.txBegin(session);
                Query findSettByIdAndProcId = session.createQuery("from Setting s where s.key like :key " + orderHQL);
                findSettByIdAndProcId.setString("key", idStart);
                if (limit != null) {
                    findSettByIdAndProcId.setMaxResults(limit);
                }

                settings = findSettByIdAndProcId.list();
                tx.commit();
                return settings;
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }

        return null;
    }

    public void delete(Persistable p) throws Exception {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);
        try {
            SessionUtils.txBegin(session);
            session.delete(p);
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

    }

    /**
     * save or update as needed
     *
     * @param udb
     * @throws Exception
     */
    public void saveOrUpdate(Persistable udb) throws Exception {
        saveOrUpdate(udb, this.transactionTimeOut);
    }

    public final void saveOrUpdate(Persistable udb, int transactionTimeOut) throws Exception {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);
        try {
            SessionUtils.txBegin(session);
            session.saveOrUpdate(udb);
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            try {
                session.evict(udb);
            } catch (Exception dc) {
            }

            throw e;
        }
    }

    public void setIssuerStatus(final String name, final Integer paymentSystemId, final String status) {
        Issuer issuer = getIssuerByNameAndPaymentSystem(name, paymentSystemId);
        if (issuer != null) {
            Session session = getSessionFactory().getCurrentSession();
            try {
                Transaction transaction = SessionUtils.txBegin(session);
                issuer.setStatus(status);
                session.update(issuer);
                transaction.commit();
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
    }

    public void deleteMerchantByNameAndPaymentSystemId(final String name, final Integer paymentSystemId) {
        Merchant merchant = getMerchantByNameAndPaymentSystemId(name, paymentSystemId);
        if (merchant != null) {
            Session session = getSessionFactory().getCurrentSession();
            try {
                Transaction transaction = SessionUtils.txBegin(session);
                session.delete(merchant);
                transaction.commit();
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
    }

    public void setMerchantStatus(final String name, final Integer paymentSystemId, final String status) {
        Merchant merchant = getMerchantByNameAndPaymentSystemId(name, paymentSystemId);
        if (merchant != null) {
            Session session = getSessionFactory().getCurrentSession();
            try {
                Transaction transaction = SessionUtils.txBegin(session);
                merchant.setStatus(status);
                session.update(merchant);
                transaction.commit();
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
    }

    public void deleteIssuerByNameAndPaymentSystemId(final String name, final Integer paymentSystemId) {
        Issuer issuer = getIssuerByNameAndPaymentSystem(name, paymentSystemId);
        if (issuer != null) {
            Session session = getSessionFactory().getCurrentSession();
            try {
                Transaction transaction = SessionUtils.txBegin(session);
                session.delete(issuer);
                transaction.commit();
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
    }

    public Issuer getIssuerByNameAndPaymentSystemID(final String name, final Integer paymentSystemId) {
        QueryBuilder queryBuilder = (Session session) -> {
            Query<Issuer> findQuery = session.createQuery("select i from Issuer i where i.name = :name " +
                "and i.paymentSystemId = :paymentSystemId");
            findQuery.setParameter("name", name);
            findQuery.setParameter("paymentSystemId", paymentSystemId);
            return findQuery;
        };
        return getPersistable(queryBuilder, Issuer.class);
    }

    /**
     * save, object should not be persisted before
     */
    public void save(Persistable udb) throws Exception {
        save(udb, this.transactionTimeOut);
    }


    public final void save(Persistable udb, int transactionTimeOut) throws Exception {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(transactionTimeOut);
        try {
            SessionUtils.txBegin(session);
            session.save(udb);
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public int saveOrUpdate(List<? extends Persistable> list) throws Exception {
        return saveOrUpdate(list, 0, list.size(), false);
    }

    public int saveOrUpdate(List<? extends Persistable> list, int offs, int limit, boolean flushEvery) throws Exception {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut * ((limit / 200) + 1));
        int cnt = 0;
        int end = offs + limit;
        if (end > list.size()) {
            end = list.size();
        }
        try {
            SessionUtils.txBegin(session);
            for (int i = offs; i < end; i++) {
                session.saveOrUpdate(list.get(i));
                if (flushEvery) {
                    session.flush();
                }
                cnt++;
            }
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return cnt;
    }

    public CardRange getCardRangeByPaymentSystemId(final String start, final String end, final String paymentSystemId) {
        QueryBuilder queryBuilder = (Session session) -> {
            Query<CardRange> findQuery = session.createQuery(
                "select cr from CardRange cr where " +
                    "cr.binSt like :start and cr.end like :end " +
                    "and exists (select 1 from Issuer i where i.id = cr.issuerId and i.paymentSystemId = :paymentSystemId)");
            findQuery.setParameter("start", start);
            findQuery.setParameter("end", end);
            findQuery.setParameter("paymentSystemId", paymentSystemId);

            return findQuery;
        };
        return getPersistable(queryBuilder, CardRange.class);
    }

    public void setCardRangeStatus(final String start, final String end, final String paymentSystemId, final String status) {
        CardRange cardRange = getCardRangeByPaymentSystemId(start, end, paymentSystemId);
        if (cardRange != null) {
            Session session = getSessionFactory().getCurrentSession();
            try {
                Transaction transaction = SessionUtils.txBegin(session);
                cardRange.setStatus(status);
                session.update(cardRange);
                transaction.commit();
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
    }

    public int delete(List<? extends Persistable> list) throws Exception {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut * ((list.size() / 200) + 1));
        int cnt = 0;
        try {
            SessionUtils.txBegin(session);
            for (int i = 0; i < list.size(); i++) {
                session.delete(list.get(i));
                cnt++;
            }
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return cnt;
    }

    /**
     * @param sdn subject distinguished name
     * @return CertificateData if found
     */
    @SuppressWarnings("rawtypes")
    public CertificateData getCertificateDataBySDN(String sdn) throws Exception {
        if (sdn != null) {
            CertificateData s = null;
            Session session = getCurrentSession();
            try {
                SessionUtils.txBegin(session);
                Query findCertificateDataBySdn = session.createQuery(
                        "from CertificateData s where s.subjectDN=:sdn" + "");
                findCertificateDataBySdn.setString("sdn", sdn);

                List sl = findCertificateDataBySdn.list();
                if (sl.size() > 0) {
                    s = (CertificateData) sl.get(0);
                }

                session.getTransaction().commit();
                return s;
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }

        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<TDSRecord> getTDSRecordsByQuery(TDSRecordSearcher searcher, boolean count) throws Exception {
        List<TDSRecord> found = null;
        Session session = getCurrentSession();
        try {
            if (searcher.getQueryTimeout() != null) {
                session.getTransaction().setTimeout(searcher.getQueryTimeout());
            }

            SessionUtils.txBegin(session);;
            Criteria trCrit = createCriteriaTransactionsByQuery(searcher, session);

            Number counted;

            if (searcher.getQueryTimeout() != null) {
                trCrit.setTimeout(searcher.getQueryTimeout());
            }

            if (count) {
                // as there is no such thing as count limit but we dont want to count millions here..
                // lets test if there is record beyond limit
                if (searcher.getSearchLimit() != null && searcher.getSearchLimit() > 0) {
                    trCrit.setProjection(Projections.id());
                    trCrit.setResultTransformer(Criteria.PROJECTION);
                    trCrit.setFirstResult(searcher.getSearchLimit());
                    trCrit.setMaxResults(1);
                    List<?> test = trCrit.list();
                    if (test.size() > 0) {
                        searcher.setOverLimit(true);
                        searcher.setTotal(searcher.getSearchLimit().longValue());
                    } else
                    // if less count exactly
                    {
                        trCrit.setFirstResult(0);
                        trCrit.setMaxResults(1);
                        trCrit.setProjection(Projections.rowCount());
                        counted = (Number) trCrit.uniqueResult();
                        if (counted != null) {
                            searcher.setOverLimit(false);
                            searcher.setTotal(counted.longValue());
                        }
                    }
                } else {
                    // trCrit.setFirstResult(0);
                    trCrit.setProjection(Projections.rowCount());
                    counted = (Number) trCrit.uniqueResult();
                    if (counted != null) {
                        searcher.setTotal(counted.longValue());
                        searcher.setOverLimit(false);
                    }
                }
            }

            if (searcher.getLimit() == null || searcher.getLimit() > 0) {
                trCrit.setProjection(null);
                trCrit.setResultTransformer(Criteria.ROOT_ENTITY);
                if (Misc.isNotNullOrEmpty(searcher.getOrder())) {
                    if ("desc".equals(searcher.getOrderDirection())) {
                        trCrit.addOrder(Order.desc(searcher.getOrder()));
                    } else {
                        trCrit.addOrder(Order.asc(searcher.getOrder()));
                    }
                } else {
                    trCrit.addOrder(Order.asc("localDateStart"));
                }

                if (searcher.getStart() != null) {
                    trCrit.setFirstResult(searcher.getStart());
                } else {
                    trCrit.setFirstResult(0);
                }
                if (searcher.getLimit() != null) {
                    trCrit.setMaxResults(searcher.getLimit());
                } else if (count) {
                    trCrit.setMaxResults(100);
                }

                found = trCrit.list();
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return found;

    }

    /**
     * This query is intended for TdsRecords CSV export functionality using the same search criteria builder used in the
     * TdsRecords search page. The difference from the TdsRecords search functionality, this expects a limit and
     * also query the TdsMessagesData by batch within the same hibernate transaction.
     * @param searcher
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<Long> getTDSRecordsIdForExport(TDSRecordSearcher searcher) throws Exception {
        List<Long> tdsRecordList = null;
        Session session = getCurrentSession();
        try {

            SessionUtils.txBegin(session);;

            Criteria tdsRecordCriteria = createCriteriaTransactionsByQuery(searcher, session);

            if (Misc.isNotNullOrEmpty(searcher.getOrder())) {
                if ("desc".equals(searcher.getOrderDirection())) {
                    tdsRecordCriteria.addOrder(Order.desc(searcher.getOrder()));
                } else {
                    tdsRecordCriteria.addOrder(Order.asc(searcher.getOrder()));
                }
            } else {
                tdsRecordCriteria.addOrder(Order.asc("localDateStart"));
            }

            tdsRecordCriteria.setFirstResult(searcher.getStart());
            tdsRecordCriteria.setMaxResults(searcher.getLimit());
            tdsRecordCriteria.setProjection(Projections.id());
            tdsRecordList = tdsRecordCriteria.list();

            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return tdsRecordList;

    }

    protected Criteria createCriteriaTransactionsByQuery(TDSRecordSearcher searcher, Session session) {

        Criteria trCrit = session.createCriteria(TDSRecord.class, "t");

        if (searcher.getId() != null) {
            trCrit.add(Restrictions.eq("id", searcher.getId()));
        }

        if (searcher.getLastId() != null) {
            trCrit.add(Restrictions.le("id", searcher.getLastId()));
        }
        if (searcher.getStartId() != null) {
            trCrit.add(Restrictions.ge("id", searcher.getStartId()));
        }
        if (searcher.getPaymentSystemId() != null) {
            trCrit.add(Restrictions.eq("paymentSystemId", searcher.getPaymentSystemId()));
        }
        if (Misc.isNotNullOrEmpty(searcher.getAcquirerMerchantID())) {
            trCrit.add(Restrictions.like("acquirerMerchantID", searcher.getAcquirerMerchantID(), MatchMode.END));
        }
        // merchant name only if id not set
        else if (Misc.isNotNullOrEmpty(searcher.getMerchantName())) {
            trCrit.add(Restrictions.like("merchantName", searcher.getMerchantName(), MatchMode.ANYWHERE));
        }

        if (searcher.getTrDateFrom() != null) {
            trCrit.add(Restrictions.ge("localDateStart", searcher.getTrDateFrom()));
        }
        if (searcher.getTrDateTo() != null) {
            trCrit.add(Restrictions.le("localDateStart", searcher.getTrDateTo()));
        }

        // for complete incomplete
        if (Boolean.FALSE.equals(searcher.getComplete())) {
            trCrit.add(Restrictions.isNull("localDateEnd"));
        } else if (Boolean.TRUE.equals(searcher.getComplete())) {
            trCrit.add(Restrictions.isNotNull("localDateEnd"));
        }
        if (Misc.isNotNullOrEmpty(searcher.getLocalStatus())) {
            trCrit.add(Restrictions.eq("localStatus", searcher.getLocalStatus()));
        } else if (searcher.getLocalStatuses() != null && searcher.getLocalStatuses().length == 1) {
            trCrit.add(Restrictions.eq("localStatus", searcher.getLocalStatuses()[0]));
        } else if (searcher.getLocalStatuses() != null && searcher.getLocalStatuses().length > 1) {
            Criterion statusesCrit = null;
            for (int r = 0; r < searcher.getLocalStatuses().length - 1; r++) {
                if (statusesCrit == null) {
                    statusesCrit = Restrictions.or(Restrictions.eq("localStatus", searcher.getLocalStatuses()[r]),
                                                   Restrictions.eq("localStatus", searcher.getLocalStatuses()[r + 1]));
                } else {
                    statusesCrit = Restrictions.or(statusesCrit,
                                                   Restrictions.eq("localStatus", searcher.getLocalStatuses()[r + 1]));
                }
            }

            if (statusesCrit != null) {
                trCrit.add(statusesCrit);
            }
        }

        if (Misc.isNotNullOrEmpty(searcher.getProtocol())) {
            trCrit.add(Restrictions.eq("protocol", searcher.getProtocol()));
        }

        if (Misc.isNotNullOrEmpty(searcher.getRequestorID())) {
            trCrit.add(Restrictions.eq("requestorID", searcher.getRequestorID()));
        }

        if (Misc.isNotNullOrEmpty(searcher.getTransStatus())) {
            trCrit.add(Restrictions.eq("transStatus", searcher.getTransStatus()));
        }
        if (Misc.isNotNullOrEmpty(searcher.getTransStatusReason())) {
            trCrit.add(Restrictions.eq("transStatusReason", searcher.getTransStatusReason()));
        }

        if (searcher.getPurchaseAmountLow() != null) {
            trCrit.add(Restrictions.ge("purchaseAmount", searcher.getPurchaseAmountLow()));
        }
        if (searcher.getPurchaseAmountHigh() != null) {
            trCrit.add(Restrictions.le("purchaseAmount", searcher.getPurchaseAmountHigh()));
        }
        if (searcher.getPurchaseCurrency() != null) {
            trCrit.add(Restrictions.eq("purchaseCurrency", searcher.getPurchaseCurrency()));
        }

        if (Misc.isNotNullOrEmpty(searcher.getDeviceChannel())) {
            trCrit.add(Restrictions.eq("deviceChannel", searcher.getDeviceChannel()));
        }

        if (Misc.isNotNullOrEmpty(searcher.getLocalAcquirerId())) {
            trCrit.add(Restrictions.eq("localAcquirerId", searcher.getLocalAcquirerId()));
        }

        if (Misc.isNotNullOrEmpty(searcher.getLocalIssuerId())) {
            trCrit.add(Restrictions.eq("localIssuerId", searcher.getLocalIssuerId()));
        }

        if (Misc.isNotNullOrEmpty(searcher.getFirst6()) && Misc.isNotNullOrEmpty(searcher.getLast4())) {
            trCrit.add(Restrictions.like("acctNumber", searcher.getFirst6(), MatchMode.START));
            trCrit.add(Restrictions.like("acctNumber", searcher.getLast4(), MatchMode.END));
        } else if (Misc.isNotNullOrEmpty(searcher.getFirst6()) && Misc.isNullOrEmpty(searcher.getLast4())) {
            trCrit.add(Restrictions.like("acctNumber", searcher.getFirst6(), MatchMode.START));
        } else if (Misc.isNullOrEmpty(searcher.getFirst6()) && Misc.isNotNullOrEmpty(searcher.getLast4())) {
            trCrit.add(Restrictions.like("acctNumber", searcher.getLast4(), MatchMode.END));
        }

        if (Misc.isNotNullOrEmpty(searcher.getAcquirerBIN())) {
            trCrit.add(Restrictions.like("acquirerBIN", searcher.getAcquirerBIN(), MatchMode.START));
        }
        if (Misc.isNotNullOrEmpty(searcher.getIssuerBIN())) {
            trCrit.add(Restrictions.like("issuerBIN", searcher.getIssuerBIN(), MatchMode.START));
        }
        if (Misc.isNotNullOrEmpty(searcher.getErrorCode())) {
            trCrit.add(Restrictions.ilike("errorCode", searcher.getErrorCode(), MatchMode.START));
        }
        if (Misc.isNotNullOrEmpty(searcher.getErrorDetail())) {
            trCrit.add(Restrictions.ilike("errorDetail", searcher.getErrorDetail(), MatchMode.ANYWHERE));
        }

        if (Misc.isNotNullOrEmpty(searcher.getECI())) {
            trCrit.add(Restrictions.eq("ECI", searcher.getECI()));
        }
        if (Misc.isNotNullOrEmpty(searcher.getMCC())) {
            trCrit.add(Restrictions.eq("MCC", searcher.getMCC()));
        }
        if (searcher.getMerchantCountry() != null) {
            trCrit.add(Restrictions.eq("merchantCountry", searcher.getMerchantCountry()));
        }

        if (Misc.isNotNullOrEmpty(searcher.getTdsTransID())) {
            trCrit.add(Restrictions.ilike("tdsTransID", searcher.getTdsTransID(), MatchMode.START));
        }
        if (Misc.isNotNullOrEmpty(searcher.getACSTransID())) {
            trCrit.add(Restrictions.eq("ACSTransID", searcher.getACSTransID()));
        }
        if (Misc.isNotNullOrEmpty(searcher.getDSTransID())) {
            trCrit.add(Restrictions.eq("DSTransID", searcher.getDSTransID()));
        }
        if (Misc.isNotNullOrEmpty(searcher.getSDKTransID())) {
            trCrit.add(Restrictions.ilike("SDKTransID", searcher.getSDKTransID(), MatchMode.START));
        }

        return trCrit;
    }


    @Override
    public com.modirum.ds.utils.Setting getSettingByKey(String arg0) {
        return getSettingById(arg0);
    }

    @SuppressWarnings("unchecked")
    public KeyData getKeyDataByAlias(String alias, String status) throws Exception {
        if (alias != null) {
            KeyData s = null;
            Session session = getCurrentSession();
            session.getTransaction().setTimeout(transactionTimeOut);
            try {
                SessionUtils.txBegin(session);
                Query findSettByIdAndProcId = session.createQuery(
                        "from KeyData s where s.keyAlias=:keyAlias" + "" + " and s.status=:status");
                findSettByIdAndProcId.setString("keyAlias", alias);
                findSettByIdAndProcId.setString("status", status);
                findSettByIdAndProcId.setReadOnly(true);
                List<KeyData> sl = findSettByIdAndProcId.list();
                if (sl.size() > 0) {
                    s = sl.get(0);
                }

                session.getTransaction().commit();
                return s;
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }

        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<KeyData> getKeyDatasByAlias(String alias, String status, String order, String od, Integer start, Integer limit, int[] count) throws Exception {
        if (!("id".equals(order) || "keyAlias".equals(order) || "keyDate".equals(order) || "status".equals(order))) {
            order = null;
        }

        List<KeyData> sl = null;
        Session session = getCurrentSession();
        session.getTransaction().setTimeout(transactionTimeOut);
        try {
            SessionUtils.txBegin(session);
            Criteria keyCrit = session.createCriteria(KeyData.class, "kd");
            keyCrit.setReadOnly(true);

            if (Misc.isNotNullOrEmpty(alias)) {
                Criterion aliasCrit = Restrictions.ilike("keyAlias", alias, org.hibernate.criterion.MatchMode.START);
                keyCrit.add(aliasCrit);
            }
            if (Misc.isNotNullOrEmpty(status)) {
                Criterion statusCrit = Restrictions.eq("status", status);
                keyCrit.add(statusCrit);
            }

            if (count != null) {
                keyCrit.setProjection(Projections.rowCount());
                Number counted = (Number) keyCrit.uniqueResult();
                count[0] = counted.intValue();
            }

            keyCrit.setProjection(null);
            keyCrit.setResultTransformer(Criteria.ROOT_ENTITY);
            if (Misc.isNotNullOrEmpty(order)) {
                if ("desc".equals(od)) {
                    keyCrit.addOrder(Order.desc(order));
                } else {
                    keyCrit.addOrder(Order.asc(order));
                }
            } else {
                keyCrit.addOrder(Order.asc("id"));
            }

            if (start != null) {
                keyCrit.setFirstResult(start);
            }
            if (limit != null) {
                keyCrit.setMaxResults(limit);
            }

            if (!(limit != null && limit == 0)) {
                sl = keyCrit.list();
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return sl;
    }

    @SuppressWarnings("unchecked")
    public List<KeyData> getKeyDataByStatus(String status) throws Exception {
        List<KeyData> l = null;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);;

            Query findSettByIdAndProcId = session.createQuery("from KeyData s where s.id is not null" + (status !=
                                                                                                         null ? " and s.status=:status" : "")).setReadOnly(
                    true);

            if (status != null) {
                findSettByIdAndProcId.setParameter("status", status);
            }

            l = findSettByIdAndProcId.list();

            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return l;
    }

    public KeyData getKeyDataById(Long id) {
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);
            KeyData kd = session.get(KeyData.class, id);
            session.getTransaction().commit();
            return kd;
        } catch (RuntimeException e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public Acquirer getAcquirerByBINAndPaymentSystem(String bin, Integer paymentSystemId) throws Exception {
        List<Acquirer> l = null;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);

            Query q = session.createQuery(
                    "from Acquirer a where a.BIN=:bin and a.paymentSystemId=:paymentSystemId").setReadOnly(true);
            q.setParameter("bin", bin);
            q.setParameter("paymentSystemId", paymentSystemId);

            l = q.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return l != null && l.size() > 0 ? l.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public Merchant getAquirerMerchant(Integer acquirerId, String acquirerMerchantId) throws Exception {
        List<Merchant> l = null;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);

            Query q = session.createQuery(
                    "from Merchant a where a.acquirerId=:aid and a.identifier=:merId").setReadOnly(true);
            q.setInteger("aid", acquirerId);
            q.setString("merId", acquirerMerchantId);

            l = q.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return l != null && l.size() > 0 ? l.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public Merchant getRequestorMerchant(String requestorId) throws Exception {
        List<Merchant> l = null;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);

            Query q = session.createQuery("from Merchant a where a.requestorID=:rid").setReadOnly(true);
            q.setString("rid", requestorId);

            l = q.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return l != null && l.size() > 0 ? l.get(0) : null;
    }


    final static String getTDSRecordByDSTransId = "select ar from TDSRecord ar where ar.DSTransID=:tstid";

    public TDSRecord getTDSRecordByDSTransId(String uuid) throws Exception {
        TDSRecord rec = null;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            Query findQuery = session.createQuery(getTDSRecordByDSTransId).setString("tstid", uuid).setReadOnly(true);
            @SuppressWarnings("unchecked") List<TDSRecord> found = findQuery.list();
            if (found.size() > 0) {
                rec = found.get(0);
            }
            session.getTransaction().commit();

        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return rec;
    }

    public List<Setting> getAllSettings() throws Exception {
        List<Setting> settings = null;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);;

            Criteria settingCriteria = session.createCriteria(Setting.class, "setting");
            settingCriteria.setReadOnly(true);
            settings = settingCriteria.list();

            session.getTransaction().commit();

        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return settings;
    }

    public List<Setting> findSettings(SettingSearcher searcher) throws Exception {
        List<Setting> l = null;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);;

            Criteria textCrit = session.createCriteria(Setting.class, "setting");
            textCrit.setReadOnly(true);

            if (Misc.isNotNullOrEmpty(searcher.getKey())) {
                textCrit.add(Restrictions.eq("key", searcher.getKey()));
            }

            if (Misc.isNotNullOrEmpty(searcher.getKeyword())) {
                Criterion keyWordCrit = Restrictions.or(
                        Restrictions.ilike("key", searcher.getKeyword(), org.hibernate.criterion.MatchMode.ANYWHERE),
                        Restrictions.ilike("comment", searcher.getKeyword(),
                                           org.hibernate.criterion.MatchMode.ANYWHERE),
                        Restrictions.ilike("value", searcher.getKeyword(), org.hibernate.criterion.MatchMode.ANYWHERE));
                textCrit.add(keyWordCrit);
            }
            if (Misc.isNotNullOrEmpty(searcher.getExclude())) {
                Criterion exclCrit = Restrictions.not(
                        Restrictions.ilike("key", searcher.getExclude(), org.hibernate.criterion.MatchMode.ANYWHERE));
                textCrit.add(exclCrit);
            }

            if (Misc.isNotNullOrEmpty(searcher.getValue())) {
                textCrit.add(Restrictions.like("value", searcher.getValue(), org.hibernate.criterion.MatchMode.START));
            }

            Number counted = null;
            boolean count = true;
            if (count) {
                textCrit.setProjection(Projections.rowCount());
                counted = (Number) textCrit.uniqueResult();
                searcher.setTotal(counted.intValue());
            }
            if (searcher.getLimit() == null || searcher.getLimit() > 0) {
                textCrit.setProjection(null);
                textCrit.setResultTransformer(Criteria.ROOT_ENTITY);
                if (Misc.isNotNullOrEmpty(searcher.getOrder())) {
                    if ("desc".equals(searcher.getOrderDirection())) {
                        textCrit.addOrder(Order.desc(searcher.getOrder()));
                    } else {
                        textCrit.addOrder(Order.asc(searcher.getOrder()));
                    }
                } else {
                    textCrit.addOrder(Order.asc("key"));
                }

                if (searcher.getStart() != null) {
                    textCrit.setFirstResult(searcher.getStart());
                }
                if (searcher.getLimit() != null) {
                    textCrit.setMaxResults(searcher.getLimit());
                }

                l = textCrit.list();
            }
            session.getTransaction().commit();

        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return l;
    }

    public int countActiveMerchants() throws Exception {
        Session session = getCurrentSession();
        Number counted = null;
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);;

            Criteria trCrit = session.createCriteria(Merchant.class, "m");
            trCrit.add(Restrictions.eq("status", DSModel.Merchant.Status.ACTIVE));
            trCrit.setProjection(Projections.rowCount());
            counted = (Number) trCrit.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return counted.intValue();
    }

    @SuppressWarnings("unchecked")
    public <T extends Persistable> List<T> getPersitableList(Class<T> clazz, String order, String status) throws Exception {
        List<T> found = null;
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);
        try {

            SessionUtils.txBegin(session);
            Criteria trCrit = session.createCriteria(clazz, "pers");
            if (status != null) {
                trCrit.add(Restrictions.eq("status", status));
            }
            trCrit.setProjection(null);
            trCrit.setResultTransformer(Criteria.ROOT_ENTITY);
            if (Misc.isNotNullOrEmpty(order)) {
                trCrit.addOrder(Order.asc(order));
            }

            found = trCrit.list();
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return found;
    }

    @SuppressWarnings("unchecked")
    public <T extends Persistable> List<T> getPersitableList(Class<T> clazz, int idStart, int limit) {
        List<T> found = null;
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);
        SessionUtils.txBegin(session);
        try {
            Criteria cr = session.createCriteria(clazz);
            cr.addOrder((Order.asc("id")));
            cr.setFirstResult(idStart);
            cr.setMaxResults(limit);
            cr.setReadOnly(true);
            found = cr.list();
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return found;
    }

    public <T extends Persistable> List<T> searchList(Class<T> clazz, Searcher searcher, Map<String, Object> query) {
        try {
            List<T> resultList = getPersitableList(clazz, searcher.getOrder(), searcher.getOrderDirection(), true, searcher.getStart(), searcher.getLimit(), query);
            if (query.get("total") != null) {
                searcher.setTotal(((Number) query.get("total")).intValue());
            }
            return resultList;
        } catch (Exception e) {
            //this error simply give hint to which entity the error occurred
            log.error("Error encountered while querying for {}.", clazz.getSimpleName());
            //This error prints the stacktrace via log stream.
            //Currently there is no way to log both arguments and exception the proper way
            log.error("", e);
        }
        return Collections.emptyList();
    }
    @SuppressWarnings("unchecked")
    public <T extends Persistable> List<T> getPersitableList(Class<T> clazz, String order, String od, boolean count, Integer start, Integer limit, Map<String, Object> query) throws Exception {
        List<T> found = null;

        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);
        try {

            SessionUtils.txBegin(session);
            Criteria trCrit = session.createCriteria(clazz, "pers");
            for (String k : query.keySet()) {
                String prop = k;
                String op = null;
                if (k.endsWith("!=")) {
                    op = "!=";
                    prop = k.substring(0, k.length() - 2);
                } else if (k.endsWith(">=")) {
                    op = ">=";
                    prop = k.substring(0, k.length() - 2);
                } else if (k.endsWith("<=")) {
                    op = "<=";
                    prop = k.substring(0, k.length() - 2);
                } else if (k.endsWith("<")) {
                    op = "<";
                    prop = k.substring(0, k.length() - 1);
                } else if (k.endsWith(">")) {
                    op = ">";
                    prop = k.substring(0, k.length() - 1);
                } else if (k.endsWith(":")) // in
                {
                    op = ":";
                    prop = k.substring(0, k.length() - 1);
                } else if (k.endsWith("!%")) // not like
                {
                    op = "!%";
                    prop = k.substring(0, k.length() - 2);
                }

                Object v = query.get(k);

                if (Misc.isNotNullOrEmpty(v)) {
                    if (op != null) {
                        if (">".equals(op)) {
                            trCrit.add(Restrictions.gt(prop, v));
                        } else if (">=".equals(op)) {
                            trCrit.add(Restrictions.ge(prop, v));
                        } else if ("<".equals(op)) {
                            trCrit.add(Restrictions.lt(prop, v));
                        } else if ("<=".equals(op)) {
                            trCrit.add(Restrictions.le(prop, v));
                        } else if ("!=".equals(op)) {
                            trCrit.add(Restrictions.ne(prop, v));
                        } else if ("!%".equals(op)) {
                            if (v instanceof String[]) {
                                for (String vx : (String[]) v) {
                                    trCrit.add(Restrictions.not(Restrictions.ilike(prop, vx, MatchMode.ANYWHERE)));
                                }
                            } else {
                                trCrit.add(Restrictions.not(Restrictions.ilike(prop, (String) v, MatchMode.ANYWHERE)));
                            }
                        } else if (":".equals(op)) {
                            Object[] va = (Object[]) query.get(k);
                            if (va != null && va.length > 0) {
                                trCrit.add(Restrictions.in(prop, va));
                            }
                        }
                    } else if (v instanceof String && ((String) v).contains("%")) {
                        trCrit.add(Restrictions.ilike(prop, (String) v, MatchMode.ANYWHERE));
                    } else if (v instanceof String && IS_NULL_OPERATOR.equals(v)) {
                        trCrit.add(Restrictions.isNull(prop));
                    } else {
                        trCrit.add(Restrictions.eq(prop, v));
                    }
                }
            }
            trCrit.setReadOnly(true);
            if (count) {
                trCrit.setFirstResult(0);
                trCrit.setMaxResults(1);
                trCrit.setProjection(Projections.rowCount());
                Number counted = (Number) trCrit.uniqueResult();
                if (counted != null) {
                    query.put("total", counted.longValue());
                }
                trCrit.setMaxResults(Integer.MAX_VALUE);
            }

            if (limit == null || limit > 0) {
                trCrit.setProjection(null);
                trCrit.setResultTransformer(Criteria.ROOT_ENTITY);
                if (Misc.isNotNullOrEmpty(order)) {
                    if ("desc".equals(od)) {
                        trCrit.addOrder(Order.desc(order));
                    } else {
                        trCrit.addOrder(Order.asc(order));
                    }
                }

                if (start != null) {
                    trCrit.setFirstResult(start);
                } else {
                    trCrit.setFirstResult(0);
                }
                if (limit != null) {
                    trCrit.setMaxResults(limit);
                }
                //else if (count)
                //{
                //	trCrit.setMaxResults(100);
                //}
                found = trCrit.list();
            }

            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return found;
    }

    final static String getTDSMessagesByRecordId = "select msg from TDSMessageData msg where msg.tdsrId=:recId order by msg.messageDate,msg.id";

    public List<TDSMessageData> getTDSMessagesByRecordId(Long recId) {
        List<TDSMessageData> found;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            Query findQuery = session.createQuery(getTDSMessagesByRecordId).setLong("recId", recId).setReadOnly(true);
            found = findQuery.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return found;
    }

    final static String setChdNull =
            "update TDSRecord t set t.acctNumberEnc=NULL, t.acctNumberKid=NULL, t.acctNumberHMAC=NULL, t.acctNumberHMKid=NULL" +
            " where  t.localDateStart BETWEEN :d1 AND :d2 AND t.acctNumberEnc IS NOT NULL";

    public int setChdNull(int months, int hours) throws Exception {
        int count = 0;
        Session session = getCurrentSession();
        Date now = new Date();
        Date d2 = DateUtil.addMonths(now, -months);
        Date d1 = new Date(d2.getTime() - hours * 3600L * 1000L);
        try {
            session.getTransaction().setTimeout(transactionTimeOut * 2);
            SessionUtils.txBegin(session);;
            Query uq = session.createQuery(setChdNull);
            uq.setParameter("d1", d1, TemporalType.TIMESTAMP);
            uq.setParameter("d2", d2, TemporalType.TIMESTAMP);
            count = uq.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return count;
    }


    public int updateTDSPropertyOnly(Long id, String property, Object value) throws Exception {
        int count = 0;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut * 2);
            SessionUtils.txBegin(session);;
            String updatePropertyOnly = "update TDSRecord t set t." + property + "=:value where t.id=:id";
            Query uq = session.createQuery(updatePropertyOnly);
            uq.setParameter("value", value);
            uq.setParameter("id", id);
            count = uq.executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return count;
    }


    final static String deleteTDS = "delete from TDSRecord t where t.localDateStart<:d1";
    final static String deleteMsgs = "delete from TDSMessageData m where m.messageDate<:d1";

    public int deleteTDSrecordsAndMsgs(int months) throws Exception {
        int count = 0;
        Session session = getCurrentSession();
        Date now = new Date();
        Date d1 = DateUtil.addMonths(now, -months);
        try {
            session.getTransaction().setTimeout(transactionTimeOut * 2);
            SessionUtils.txBegin(session);;
            Query uq = session.createQuery(deleteTDS);
            uq.setTimestamp("d1", d1);
            count += uq.executeUpdate();

            Query uq2 = session.createQuery(deleteMsgs);
            uq2.setTimestamp("d1", d1);
            count += uq2.executeUpdate();

            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return count;
    }
    
    @SuppressWarnings("unchecked")
    public List<CertificateData> getCertificatesByExpiryDateRange(Date lowerLimit, Date upperLimit) {
        Session session = getCurrentSession();
        List<CertificateData> result = null;

        try {
            session.getTransaction().setTimeout(transactionTimeOut * 2);
            SessionUtils.txBegin(session);;
            Query q = sessionFactory.getCurrentSession().createQuery(
                    "from CertificateData cd where cd.expires< :upperLimit and cd.expires>:lowerLimit");
            q.setDate("upperLimit", upperLimit);
            q.setDate("lowerLimit", lowerLimit);
            result = q.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return result;

    }

    public HsmDevice getHsmDevice(Long id) throws Exception {
        HsmDevice hsmDevice;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);
            hsmDevice =  session.get(HsmDevice.class, id);
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return hsmDevice;
    }

    public List<HsmDevice> listActiveHsmDevices() throws Exception {
        return getHsmDevicesByStatus(DSModel.HsmDevice.Status.ACTIVE);
    }

    /**
     * Returns a list containing all HSM devices with a specified status
     * @return    list of HSM devices
     */
    public List<HsmDevice> getHsmDevicesByStatus(String status) {
        QueryBuilder queryBuilder = (Session session) -> {
            Query<HsmDevice> findQuery = session.createQuery(
                    "select s from HsmDevice s where status = :status");
            findQuery.setParameter("status", status);
            return findQuery;
        };
        return getPersistableList(queryBuilder, HsmDevice.class);
    }

    /**
     * Get HSM Device Config given HSM Device ID and DS ID = 0 by default
     * @param hsmDeviceId      HSM Device ID
     * @return HsmDeviceConf   HSM Device Config
     * @throws Exception
     */
    public HsmDeviceConf getHsmDeviceConfig(Long hsmDeviceId) throws Exception {
        return getHsmDeviceConfig(hsmDeviceId, DSModel.HsmDeviceConf.dsId.DEFAULT_DS);
    }

    public HsmDeviceConf getHsmDeviceConfig(Long hsmDeviceId, int dsId) throws Exception {
        HsmDeviceConf config;
        Session session = getCurrentSession();
        try {
            session.getTransaction().setTimeout(transactionTimeOut);
            SessionUtils.txBegin(session);
            Query<HsmDeviceConf> query = session.createQuery("FROM HsmDeviceConf WHERE hsmDeviceId=:hsmDeviceId AND dsId=:dsId",
                    HsmDeviceConf.class);

            query.setParameter("hsmDeviceId", hsmDeviceId);
            query.setParameter("dsId", dsId);

            query.setMaxResults(1); // Avoiding ugly NoResultException for getSingleResult.
            config = query.getResultList().stream().findFirst().orElse(null);
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return config;
    }

    public Optional<Integer> getPaymentSystemIdByPort(Short port) throws Exception {
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);;
            Criteria criteria = session.createCriteria(PaymentSystem.class, "ps");
            criteria.add(Restrictions.eq("port", port));
            criteria.setProjection(Projections.id());

            Integer psIdResult = (Integer) criteria.uniqueResult();
            session.getTransaction().commit();

            return Optional.ofNullable(psIdResult);
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public int getTransactionTimeOut() {
        return transactionTimeOut;
    }

    public void setTransactionTimeOut(int transactionTimeOut) {
        this.transactionTimeOut = transactionTimeOut;
    }

    @Override
    public com.modirum.ds.utils.Setting getSettingByKey(String arg0, Short arg1) {
        return getSettingByKey(arg0);
    }

    public <T extends Persistable> List<T> getPersistableList(QueryBuilder qb, Class<T> clazz) {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);
        try {
            SessionUtils.txBegin(session);
            Query query = qb.build(session);
            List<T> result = query.getResultList();
            tx.commit();
            return result;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            log.error("Error encountered while invoking getPersistableList(QueryBuilder qb) for {}",
                      clazz.getSimpleName());
            log.error("Error: ", e);
            throw new RuntimeException(e);
        }
    }

    public <T extends Persistable> T getPersistable(QueryBuilder qb, Class<T> clazz) {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);
        try {
            SessionUtils.txBegin(session);
            Query query = qb.build(session);
            T result = (T) query.getResultList().stream().findFirst().orElse(null);
            tx.commit();
            return result;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            log.error("Error encountered while invoking getPersistable(QueryBuilder qb) for {}", clazz.getSimpleName());
            log.error("Error: ", e);
            throw new RuntimeException(e);
        }
    }

    public boolean isIssuerExists(String issuerName, Integer paymentSystemId, Integer issuerId) {
        Session session = getCurrentSession();
        boolean found;
        try {
            SessionUtils.txBegin(session);
            String hqlQuery = "select 1 from Issuer iss where iss.name = :issuerName" +
                              " and iss.paymentSystemId = :paymentSystemId" +
                              (issuerId != null ? " and iss.id <> :issuerId" : "");
            Query<Number> findQuery = session.createQuery(hqlQuery);
            findQuery.setParameter("paymentSystemId", paymentSystemId);
            findQuery.setParameter("issuerName", issuerName);

            if (issuerId != null) {
                findQuery.setParameter("issuerId", issuerId);
            }

            found = findQuery.stream().findAny().isPresent();
            session.getTransaction().commit();
            return found;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public List<CardRange> getCardRangesByIssuerId(Integer issuerId) {
        final String getIssuerCardBins = "select cb from CardRange cb where cb.issuerId=:iid order by cb.binSt";

        List<CardRange> cardRanges = null;
        if (issuerId != null) {
            Session session = getCurrentSession();
            try {
                SessionUtils.txBegin(session);
                Query findQuery = session.createQuery(getIssuerCardBins).setReadOnly(true);
                findQuery.setParameter("iid", issuerId);
                cardRanges = findQuery.list();
                session.getTransaction().commit();
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
        return cardRanges;
    }

    public List<CardRange> getCardRangesPerPaymentSystem(Long cardRangeId, String bin, Integer paymentSystemId) {
        List<CardRange> found = null;
        if (bin != null) {
            Session session = getCurrentSession();
            try {
                SessionUtils.txBegin(session);
                String qstr = "select cb from CardRange cb" + " where cb.binSt like :bin" +
                              (cardRangeId != null ? " and cb.id <> :cardRangeId" : "") +
                              " and exists (select id from Issuer iss where iss.id = cb.issuerId and iss.paymentSystemId = :paymentSystemId)";
                Query findQuery = session.createQuery(qstr);
                findQuery.setParameter("bin", bin + "%");
                if (cardRangeId != null) {
                    findQuery.setParameter("cardRangeId", cardRangeId);
                }
                findQuery.setParameter("paymentSystemId", paymentSystemId);
                found = findQuery.list();
                session.getTransaction().commit();
            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
        return found;
    }

    public boolean isCardRangeNameExistingUnderPaymentSystem(String name, Long cardRangeId, Integer paymentSystemId) {
        boolean found;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            String qstr = "select 1 from CardRange cb" + " where cb.name = :name" +
                          (cardRangeId != null ? " and cb.id <> :cardRangeId" : "") +
                          " and exists (select id from Issuer iss where iss.id = cb.issuerId and iss.paymentSystemId = :paymentSystemId)";
            Query findQuery = session.createQuery(qstr);
            findQuery.setParameter("name", name);
            findQuery.setParameter("paymentSystemId", paymentSystemId);
            if (cardRangeId != null) {
                findQuery.setParameter("cardRangeId", cardRangeId);
            }
            found = findQuery.stream().findAny().isPresent();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return found;
    }

    public void getIssuerCardBins(List<String> check, Map<String, CardRange> results) {
        Session session = getCurrentSession();
        try {
            Transaction tx = session.getTransaction();
            tx.setTimeout(getTransactionTimeOut());
            SessionUtils.txBegin(session);

            Criteria crit = session.createCriteria(CardRange.class);
            crit.add(Restrictions.in("binSt", check));

            List<CardRange> foundl = crit.list();
            tx.commit();
            for (CardRange cr : foundl) {
                results.put(cr.getBinSt(), cr);
            }
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public CardRange getBestMatchingIssuerCardBin(String maxBin, Integer paymentSystemId, int minBinLength, int maxBinLength) {
        Session session = getCurrentSession();
        try {
            Transaction tx = session.getTransaction();
            tx.setTimeout(getTransactionTimeOut());
            SessionUtils.txBegin(session);
            Criteria rangeCriteria = session.createCriteria(CardRange.class, "cr");
            Disjunction disjunction = Restrictions.disjunction();
            int todo = maxBinLength - minBinLength;

            for (int i = 0; i < todo && maxBin.length() - i >= minBinLength; i++) {
                String xbin = maxBin.substring(0, maxBin.length() - i);
                Criterion binnxc = Restrictions.eq("cr.binSt", xbin);
                disjunction.add(binnxc);
            }
            rangeCriteria.add(disjunction);

            DetachedCriteria issuerCriteria = DetachedCriteria.forClass(Issuer.class, "is");
            issuerCriteria.setProjection(Property.forName("is.id")).add(
                    Restrictions.eq("paymentSystemId", paymentSystemId)).add(
                    Property.forName("is.id").eqProperty("cr.issuerId"));

            rangeCriteria.add(Subqueries.exists(issuerCriteria));
            rangeCriteria.addOrder(Order.desc("cr.binSt"));
            List<CardRange> rangeList = rangeCriteria.list();
            tx.commit();

            if (rangeList.size() > 0) {
                return rangeList.get(0);
            }
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return null;
    }

    public CardRange getBestMatchingIssuerCardBin(String maxBin, int minBinLength, int maxBinLength) {
        Session session = getCurrentSession();
        try {
            Transaction tx = session.getTransaction();
            tx.setTimeout(getTransactionTimeOut());
            SessionUtils.txBegin(session);
            Criteria rangeCriteria = session.createCriteria(CardRange.class);
            Disjunction disjunction = Restrictions.disjunction();
            int todo = maxBinLength - minBinLength;

            for (int i = 0; i < todo && maxBin.length() - i >= minBinLength; i++) {
                String xbin = maxBin.substring(0, maxBin.length() - i);
                Criterion binnxc = Restrictions.eq("binSt", xbin);
                disjunction.add(binnxc);
            }

            rangeCriteria.add(disjunction);
            rangeCriteria.addOrder(Order.desc("binSt"));
            List<CardRange> rangeList = rangeCriteria.list();
            tx.commit();

            if (rangeList.size() > 0) {
                return rangeList.get(0);
            }
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return null;
    }

    public CardRange getBestMatchingIssuerCardBinByRange(String pan) {
        Session session = getCurrentSession();
        try {
            Transaction tx = session.getTransaction();
            tx.setTimeout(getTransactionTimeOut());
            SessionUtils.txBegin(session);

            Query rangeQuery = session.createQuery(
                    "select cr from CardRange cr where cr.binSt<=:pan and cr.end>=:pan and length(cr.binSt)=:len and length(cr.end)=:len" +
                    " order by cr.binSt desc, cr.end asc");

            rangeQuery.setString("pan", pan);
            rangeQuery.setInteger("len", pan.length());
            List<CardRange> rangeList = rangeQuery.list();
            tx.commit();

            if (rangeList.size() > 0) {
                return rangeList.get(0);
            }
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return null;
    }

    public CardRange getBestMatchingIssuerCardBinByRange(String pan, Integer paymentSystemId) {
        Session session = getCurrentSession();
        try {
            Transaction tx = session.getTransaction();
            tx.setTimeout(getTransactionTimeOut());
            SessionUtils.txBegin(session);

            Query rangeQuery = session.createQuery("select cr from CardRange cr" +
                                                   " where cr.binSt<=:pan and cr.end>=:pan and length(cr.binSt)=:len and length(cr.end)=:len" +
                                                   "       and exists (select id from Issuer i where i.id=cr.issuerId and i.paymentSystemId=:paymentSystemId)" +
                                                   " order by cr.binSt desc, cr.end asc");

            rangeQuery.setString("pan", pan);
            rangeQuery.setInteger("len", pan.length());
            rangeQuery.setInteger("paymentSystemId", paymentSystemId);
            List<CardRange> rangeList = rangeQuery.list();
            tx.commit();

            if (rangeList.size() > 0) {
                return rangeList.get(0);
            }
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return null;
    }

    public List<ACSProfile> getAcsProfiles(Integer issuerId, Short set, String status) {
        List<ACSProfile> list;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            Criteria acpCrit = session.createCriteria(ACSProfile.class);
            acpCrit.add(Restrictions.eq("issuerId", issuerId));

            if (set != null) {
                acpCrit.add(Restrictions.eq("set", set));
            }
            if (status != null) {
                acpCrit.add(Restrictions.eq("status", status));
            }

            acpCrit.addOrder(Order.asc("id"));


            list = acpCrit.list();
            session.getTransaction().commit();

        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return list;
    }

    public List<ACSProfile> getAcsProfiles(Long certId, String status) {
        List<ACSProfile> list;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            String qstr = "select cb from ACSProfile cb where cb.inClientCert=:certId " +
                          (status != null ? " and cb.status=:status" : "");
            Query findQuery = session.createQuery(qstr);
            findQuery.setParameter("certId", certId);
            if (status != null) {
                findQuery.setParameter("status", status);
            }

            list = findQuery.list();
            session.getTransaction().commit();

        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return list;
    }

    public List<ACSProfile> getAcsProfiles(String refNo, Integer issid) {
        List<ACSProfile> result;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            String qstr = "select profile from ACSProfile profile where profile.refNo=:refNo " +
                          (issid != null ? " and profile.issuerId=:issid" : " and profile.issuerId is null");
            Query findQuery = session.createQuery(qstr);
            findQuery.setParameter("refNo", refNo);
            if (issid != null) {
                findQuery.setParameter("issid", issid);
            }
            findQuery.setReadOnly(true);

            result = findQuery.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return result;
    }

    public List<ACSProfile> getAttemptsACSList(String status, Integer paymentSystemId) {
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            String qstr = "select cb from ACSProfile cb where cb.issuerId is null " +
                          (status != null ? " and cb.status=:status" : "") +
                          (paymentSystemId != null ? " and cb.paymentSystemId=:paymentSystemId" : "");
            Query findQuery = session.createQuery(qstr);
            if (status != null) {
                findQuery.setParameter("status", status);
            }
            if (paymentSystemId != null) {
                findQuery.setParameter("paymentSystemId", paymentSystemId);
            }

            List<ACSProfile> list = findQuery.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public List<ACSProfile> getAcsProfileByNameAndPaymentSystem(String name, Integer paymentSystemId) {
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            String queryStr = "select m from ACSProfile m where m.paymentSystemId=:paymentSystemId AND m.name=:name";
            Query<ACSProfile> query = session.createQuery(queryStr);
            query.setParameter("name", name);
            query.setParameter("paymentSystemId", paymentSystemId);
            List<ACSProfile> list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            e.printStackTrace();
            throw e;
        }
    }

    public ACSProfile getAttemptsACSById(Integer aacsId) {
        QueryBuilder queryBuilder = (Session session) -> {
            String query = "select acs from ACSProfile acs where acs.id=:id " + " and acs.issuerId is null";
            Query<ACSProfile> findQuery = session.createQuery(query);
            findQuery.setParameter("id", aacsId);
            return findQuery;
        };

        return getPersistable(queryBuilder, ACSProfile.class);
    }

    public Text loadText(String key, String language) {
        Text foundText = null;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);

            org.hibernate.Query findText2 = session.createQuery(
                    "from Text t where t.key=:key and t.locale=:loc");// or (t.key=:key and t.locale=:loc2)");
            findText2.setString("key", key);
            findText2.setString("loc", language);
            findText2.setCacheable(true);
            List matches = findText2.list();
            for (int i = 0; matches != null && i < matches.size(); i++) {
                foundText = (Text) matches.get(i);
                if (foundText.getLocale().equals(language)) {
                    break;
                }
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return foundText;
    }

    public List<Text> findTexts(TextSearcher searcher) {
        List<Text> texts = null;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            Criteria textCrit = session.createCriteria(Text.class, "text");

            if (Misc.isNotNullOrEmpty(searcher.getLocale())) {
                textCrit.add(Restrictions.eq("locale", searcher.getLocale()));
            }
            if (Misc.isNotNullOrEmpty(searcher.getKey())) {
                textCrit.add(Restrictions.eq("key", searcher.getKey()));
            }

            if (Misc.isNotNullOrEmpty(searcher.getKeyword())) {
                Criterion keyWordCrit = Restrictions.or(
                        Restrictions.ilike("key", searcher.getKeyword(), org.hibernate.criterion.MatchMode.ANYWHERE),
                        Restrictions.ilike("message", searcher.getKeyword(),
                                           org.hibernate.criterion.MatchMode.ANYWHERE));
                textCrit.add(keyWordCrit);
            }


            Number counted = null;
            boolean count = true;
            if (count) {
                textCrit.setProjection(Projections.rowCount());
                counted = (Number) textCrit.uniqueResult();
                searcher.setTotal(counted.intValue());
            }
            if (searcher.getLimit() == null || searcher.getLimit() > 0) {
                textCrit.setProjection(null);
                textCrit.setResultTransformer(Criteria.ROOT_ENTITY);
                if (Misc.isNotNullOrEmpty(searcher.getOrder())) {
                    if ("desc".equals(searcher.getOrderDirection())) {
                        textCrit.addOrder(Order.desc(searcher.getOrder()));
                    } else {
                        textCrit.addOrder(Order.asc(searcher.getOrder()));
                    }
                } else {
                    textCrit.addOrder(Order.asc("key"));
                }

                if (searcher.getStart() != null) {
                    textCrit.setFirstResult(searcher.getStart());
                }
                if (searcher.getLimit() != null) {
                    textCrit.setMaxResults(searcher.getLimit());
                }

                texts = textCrit.list();
            }
            session.getTransaction().commit();

        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return texts;
    }

    public List<User> listUsers(UserSearchFilter filter) {

        // filter paymentsystem is based on paymentSystemDropDown
        final Boolean isSuperUserFilter = PaymentSystem.UNLIMITED.equals(filter.getPaymentSystemId());
        final Boolean isAllUserFilter = PaymentSystem.ALL.equals(filter.getPaymentSystemId());

        List<User> users = null;
        if (filter != null) {
            StringBuilder where = new StringBuilder();

            if (isSuperUserFilter) {
                where.append(" AND u.paymentSystemId IS NULL");
            } else if (!isAllUserFilter) {
                where.append(" AND u.paymentSystemId=:paymentSystemId");
            }
            if (Misc.isNotNullOrEmpty(filter.getLoginname())) {
                where.append(" and u.loginname like :loginname");
            }
            if (Misc.isNotNullOrEmpty(filter.getFirstName())) {
                where.append(" and u.firstName like :firstName");
            }
            if (Misc.isNotNullOrEmpty(filter.getLastName())) {
                where.append(" and u.lastName like :lastName");
            }
            if (Misc.isNotNullOrEmpty(filter.getEmail())) {
                where.append(" and u.email like :email");
            }
            if (Misc.isNotNullOrEmpty(filter.getIssuerRole())) {
                where.append(" and :issuerrole in elements(u.roles)");
            }
            if (Misc.isNotNullOrEmpty(filter.getStatus())) {
                where.append(" and u.status=:status");
            }

            // orm protection
            if (Misc.isNotNullOrEmpty(filter.getOrder()) && !User.VALID__SEARCH_ORDERS.contains(filter.getOrder())) {
                filter.setOrder(null);
            }

            Session session = getCurrentSession();
            try {

                SessionUtils.txBegin(session);

                String qstr = "select u from User u where u.id is not null " + where.toString() +
                              (Misc.isNotNullOrEmpty(filter.getOrder()) ? " order by u." + filter.getOrder() + ("desc".equals(
                                      filter.getOrderDirection()) ? " desc" : "") : "");
                org.hibernate.Query findQuery = session.createQuery(qstr);
                org.hibernate.Query countQuery = session.createQuery(
                        "select count(*) from User as u where u.id is not null " + where.toString());

                if (!isSuperUserFilter && !isAllUserFilter) {
                    findQuery.setParameter("paymentSystemId", filter.getPaymentSystemId());
                    countQuery.setParameter("paymentSystemId", filter.getPaymentSystemId());
                }

                if (Misc.isNotNullOrEmpty(filter.getLoginname())) {
                    findQuery.setParameter("loginname", "%" + filter.getLoginname() + "%");
                    countQuery.setParameter("loginname", "%" + filter.getLoginname() + "%");
                }
                if (Misc.isNotNullOrEmpty(filter.getEmail())) {
                    findQuery.setParameter("email", "%" + filter.getEmail() + "%");
                    countQuery.setParameter("email", "%" + filter.getEmail() + "%");
                }
                if (Misc.isNotNullOrEmpty(filter.getFirstName())) {
                    findQuery.setParameter("firstName", "%" + filter.getFirstName() + "%");
                    countQuery.setParameter("firstName", "%" + filter.getFirstName() + "%");
                }
                if (Misc.isNotNullOrEmpty(filter.getLastName())) {
                    findQuery.setParameter("lastName", "%" + filter.getLastName() + "%");
                    countQuery.setParameter("lastName", "%" + filter.getLastName() + "%");
                }
                if (Misc.isNotNullOrEmpty(filter.getIssuerRole())) {
                    findQuery.setParameter("issuerrole", filter.getIssuerRole());
                    countQuery.setParameter("issuerrole", filter.getIssuerRole());
                }
                if (Misc.isNotNullOrEmpty(filter.getStatus())) {
                    findQuery.setParameter("status", filter.getStatus());
                    countQuery.setParameter("status", filter.getStatus());
                }

                Number count = (Number) countQuery.uniqueResult();
                filter.setTotal(count.intValue());

                if (filter.getStart() != null) {
                    findQuery.setFirstResult(filter.getStart());
                }
                if (filter.getLimit() != null) {
                    findQuery.setMaxResults(filter.getLimit());
                }

                users = findQuery.list();
                session.getTransaction().commit();

            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
        return users;
    }

    public boolean deleteCardRange(String start, String end, String paymentSystemId) {
        Session currentSession = getCurrentSession();
        boolean removed = false;
        try {
            Transaction tx = SessionUtils.txBegin(currentSession);
            Query query = currentSession.createQuery(
                    "delete from CardRange cr where cr.binSt like :start and cr.end like :end and exists (select 1 from Issuer i where i.id = cr.issuerId and i.paymentSystemId = :paymentSystemId)");
            query.setParameter("start", start);
            query.setParameter("end", end);
            query.setParameter("paymentSystemId", paymentSystemId);
            removed = query.executeUpdate() > 0;
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(currentSession);
        }
        return removed;
    }

    public List<AuditLog> getLogRecordsByQuery(AuditLogSearcher searcher) {
        List<AuditLog> auditLogRecords = null;
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            StringBuffer where = new StringBuffer();

            if (searcher.getById() != null) {
                where.append(" and al.byId=:byId");
            }

            if (searcher.getLogDateFrom() != null) {
                where.append(" and al.when>=:logDateFrom");
            }
            if (searcher.getLogDateTo() != null) {
                where.append(" and al.when<=:logDateTo");
            }

            if (searcher.getPaymentSystemId() != null) {
                where.append(" and al.paymentSystemId=:paymentSystemId");
            }

            if (Misc.isNotNullOrEmpty(searcher.getAction())) {
                where.append(" and al.action=:action");
            } else if (searcher.getActions() != null && searcher.getActions().length > 0) {
                where.append(" and (");
                String[] aa = searcher.getActions();
                for (int i = 0; i < aa.length; i++) {
                    if (i > 0) {
                        where.append(" or ");
                    }
                    where.append(" al.action=:action" + i);

                }
                where.append(")");
            }

            if (Misc.isNotNullOrEmpty(searcher.getBy())) {
                where.append(" and lower(al.by) like :by");
            }
            if (searcher.getObjectId() != null) {
                where.append(" and al.objectId=:objectId");
            }
            if (Misc.isNotNullOrEmpty(searcher.getObjectClass())) {
                where.append(" and al.objectClass=:objectClass");
            }

            String qstr = "select al from AuditLog al where al.id is not null " + where.toString() +
                          (Misc.isNotNullOrEmpty(searcher.getOrder()) ? " order by al." + searcher.getOrder() + ("desc".equals(
                                  searcher.getOrderDirection()) ? " desc" : "") : "");
            org.hibernate.Query findQuery = session.createQuery(qstr);
            org.hibernate.Query countQuery = session.createQuery(
                    "select count(*) from AuditLog al where al.id is not null " + where.toString());

            if (searcher.getById() != null) {
                findQuery.setParameter("byId", searcher.getById());
                countQuery.setParameter("byId", searcher.getById());
            }
            if (searcher.getLogDateFrom() != null) {
                findQuery.setParameter("logDateFrom", searcher.getLogDateFrom(), new TimestampType());
                countQuery.setParameter("logDateFrom", searcher.getLogDateFrom(), new TimestampType());
            }
            if (searcher.getLogDateTo() != null) {
                findQuery.setParameter("logDateTo", searcher.getLogDateTo(), new TimestampType());
                countQuery.setParameter("logDateTo", searcher.getLogDateTo(), new TimestampType());
            }

            if (Misc.isNotNullOrEmpty(searcher.getAction())) {
                findQuery.setParameter("action", searcher.getAction());
                countQuery.setParameter("action", searcher.getAction());
            } else if (searcher.getActions() != null && searcher.getActions().length > 0) {
                String[] aa = searcher.getActions();
                for (int i = 0; i < aa.length; i++) {
                    findQuery.setParameter("action" + i, aa[i]);
                    countQuery.setParameter("action" + i, aa[i]);
                }
            }

            if (searcher.getPaymentSystemId() != null) {
                findQuery.setParameter("paymentSystemId", searcher.getPaymentSystemId());
                countQuery.setParameter("paymentSystemId", searcher.getPaymentSystemId());
            }

            if (Misc.isNotNullOrEmpty(searcher.getBy())) {
                findQuery.setParameter("by", searcher.getBy().toLowerCase() + "%");
                countQuery.setParameter("by", searcher.getBy().toLowerCase() + "%");
            }

            if (searcher.getObjectId() != null) {
                findQuery.setParameter("objectId", searcher.getObjectId());
                countQuery.setParameter("objectId", searcher.getObjectId());
            }
            if (Misc.isNotNullOrEmpty(searcher.getObjectClass())) {
                findQuery.setParameter("objectClass", searcher.getObjectClass());
                countQuery.setParameter("objectClass", searcher.getObjectClass());
            }

            Number count = (Number) countQuery.uniqueResult();
            searcher.setTotal(count.intValue());

            if (searcher.getStart() != null) {
                findQuery.setFirstResult(searcher.getStart());
            }
            if (searcher.getLimit() != null) {
                findQuery.setMaxResults(searcher.getLimit());
            }

            auditLogRecords = findQuery.list();
            session.getTransaction().commit();

        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }

        return auditLogRecords;
    }

    public Boolean isExistingDuplicateAcquirerName(Integer acquirerId, String acquirerName, Integer paymentSystemId) {
        Session session = getCurrentSession();
        boolean found;
        try {
            Transaction tx = SessionUtils.txBegin(session);
            String query = "select 1 from Acquirer acq " +
                           "where acq.name = :acquirerName and acq.paymentSystemId = :paymentSystemId " +
                           (acquirerId != null ? "and acq.id <> :acquirerId" : "");
            Query<Number> findQuery = session.createQuery(query);
            findQuery.setParameter("paymentSystemId", paymentSystemId).setParameter("acquirerName", acquirerName);
            if (acquirerId != null) {
                findQuery.setParameter("acquirerId", acquirerId);
            }
            found = findQuery.stream().findAny().isPresent();
            tx.commit();
            return found;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public boolean isExistingDuplicateAcquirerBIN(Integer acquirerId, String acquirerBIN, Integer paymentSystemId) {
        Session session = getCurrentSession();
        boolean found;
        try {
            Transaction tx = SessionUtils.txBegin(session);
            String query = "select 1 from Acquirer acq " +
                           "where acq.BIN = :acquirerBIN and acq.paymentSystemId = :paymentSystemId " +
                           (acquirerId != null ? "and acq.id <> :acquirerId" : "");
            Query<Number> findQuery = session.createQuery(query);
            findQuery.setParameter("paymentSystemId", paymentSystemId).setParameter("acquirerBIN", acquirerBIN);
            if (acquirerId != null) {
                findQuery.setParameter("acquirerId", acquirerId);
            }
            found = findQuery.stream().findAny().isPresent();
            tx.commit();
            return found;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public List<Acquirer> getAcquirers(Integer paymentSystemId) {
        List<Acquirer> acquirerList = Collections.emptyList();
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            String hqlQuery = "select acq from Acquirer acq" +
                              (paymentSystemId != null ? " where acq.paymentSystemId = :paymentSystemId" : "") +
                              " order by acq.name";
            Query<Acquirer> findQuery = session.createQuery(hqlQuery);
            if (paymentSystemId != null) {
                findQuery.setParameter("paymentSystemId", paymentSystemId);
            }
            acquirerList = Optional.ofNullable(findQuery.list()).orElse(Collections.emptyList());
            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
            SessionUtils.txRollback(session);
        }
        return acquirerList;
    }

    public boolean isExistingDuplicateOperatorId(String operatorId, Integer paymentSystemId, Long tdsServerProfileId) {
        Session session = getCurrentSession();
        boolean found;
        try {
            SessionUtils.txBegin(session);
            String hqlQuery = "select 1 from TDSServerProfile tsp where tsp.operatorID = :operatorId" +
                              " and tsp.paymentSystemId = :paymentSystemId" +
                              (tdsServerProfileId != null ? " and tsp.id <> :tdsServerProfileId" : "");
            Query<Number> findQuery = session.createQuery(hqlQuery);
            findQuery.setParameter("paymentSystemId", paymentSystemId);
            findQuery.setParameter("operatorId", operatorId);

            if (tdsServerProfileId != null) {
                findQuery.setParameter("tdsServerProfileId", tdsServerProfileId);
            }

            found = findQuery.stream().findAny().isPresent();
            session.getTransaction().commit();
            return found;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public List<Merchant> getMerchantsByIdentifierAndAcquirerId(List<String> idCheck, List<Integer> aidCheck) {
        QueryBuilder queryBuilder = (Session session) -> {
            // TODO: Refactor this either find a more simple query or do the  filtering in Java
            StringBuilder qs = new StringBuilder(64 + idCheck.size() * 20);
            qs.append("select cb from Merchant cb where");
            for (int i = 0; i < idCheck.size(); i++) {
                qs.append(" (cb.identifier=:id").append(i).append(" and cb.acquirerId=:aid").append(i).append(")");
                if (i < idCheck.size() - 1) {
                    qs.append(" or");
                }
            }

            Query<Merchant> findQuery = session.createQuery(qs.toString());
            for (int i = 0; i < idCheck.size(); i++) {
                findQuery.setParameter("id" + i, idCheck.get(i));
                findQuery.setParameter("aid" + i, aidCheck.get(i));
            }
            return findQuery;
        };

        List<Merchant> foundMerchants = getPersistableList(queryBuilder, Merchant.class);
        return foundMerchants;
    }

    public List<TDSServerProfile> getTdsServerProfilesByRequestorIdOrOperatorId(String requestorID, String operatorID) {
        List<TDSServerProfile> result;
        QueryBuilder queryBuilder = (Session session) -> {
            String queryStr = "select m from TDSServerProfile m where ";

            StringBuilder where = new StringBuilder();
            if (requestorID != null) {
                where.append(" m.requestorID=:requestorID");
            }

            if (operatorID != null) {
                if (where.length() > 0) {
                    where.append(" and ");
                }
                where.append(" m.operatorID=:operatorID");
            }

            Query<TDSServerProfile> query = session.createQuery(queryStr + where);
            if (requestorID != null) {
                query.setParameter("requestorID", requestorID);
            }
            if (operatorID != null) {
                query.setParameter("operatorID", operatorID);
            }
            return query;
        };


        result = getPersistableList(queryBuilder, TDSServerProfile.class);
        return result;
    }

    public List<TDSServerProfile> getTdsServerProfilesByNameAndPaymentSystem(String name, Integer paymentSystemId) {
        List<TDSServerProfile> result;
        QueryBuilder queryBuilder = (Session session) -> {
            String queryStr = "select m from TDSServerProfile m where m.paymentSystemId=:paymentSystemId AND m.name=:name";
            Query<TDSServerProfile> query = session.createQuery(queryStr);
            query.setParameter("name", name);
            query.setParameter("paymentSystemId", paymentSystemId);
            return query;
        };

        result = getPersistableList(queryBuilder, TDSServerProfile.class);
        return result;
    }

    public TDSServerProfile getActiveTdsServerProfileByOperatorId(Integer paymentSystemId, String operatorID) {
        TDSServerProfile tdsServerProfile;
        QueryBuilder queryBuilder = (Session session) -> {
            String queryStr = "select m from TDSServerProfile m where m.paymentSystemId=:paymentSystemId AND m.operatorID=:operatorID";

            Query<TDSServerProfile> query = session.createQuery(queryStr);
            query.setParameter("paymentSystemId", paymentSystemId);
            query.setParameter("operatorID", operatorID);
            return query;
        };
        tdsServerProfile = getPersistable(queryBuilder, TDSServerProfile.class);
        return tdsServerProfile;
    }

    public ACSProfile getACSProfileByUrlAndIssuerId(String url, Integer issuerId) {
        QueryBuilder queryBuilder = (Session session) -> {
            Query<ACSProfile> findQuery = session.createQuery(
                    "select a from ACSProfile a where a.URL=:url AND a.issuerId=:issuerId");
            findQuery.setParameter("url", url);
            findQuery.setParameter("issuerId", issuerId);
            return findQuery;
        };

        List<ACSProfile> acsProfiles = getPersistableList(queryBuilder, ACSProfile.class);
        return acsProfiles.size() == 1 ? acsProfiles.get(0) : null;
    }

    public Issuer getIssuerByNameAndPaymentSystem(String name, Integer paymentSystemId) {
        QueryBuilder queryBuilder = (Session session) -> {
            Query<Issuer> findQuery = session.createQuery(
                    "select i from Issuer i where i.name = :name " + "and i.paymentSystemId = :paymentSystemId");
            findQuery.setParameter("name", name);
            findQuery.setParameter("paymentSystemId", paymentSystemId);
            return findQuery;
        };
        return getPersistable(queryBuilder, Issuer.class);
    }

    public Merchant getMerchantByNameAndPaymentSystemId(String name, Integer paymentSystemId) {
        QueryBuilder queryBuilder = (Session session) -> {
            Query<Merchant> findQuery = session.createQuery(
                    "select m from Merchant m where m.name=:name and m.paymentSystemId=:paymentSystemId");
            findQuery.setParameter("name", name);
            findQuery.setParameter("paymentSystemId", paymentSystemId);

            return findQuery;
        };
        return getPersistable(queryBuilder, Merchant.class);
    }

    public TDSServerProfile getTdsServerProfile(String requestorId, Integer paymentSystemId, Long tdsProfileId) {
        QueryBuilder queryBuilder = (Session session) -> {
            String queryStr = "select p from TDSServerProfile p " + "where p.requestorID = :requestorID " +
                              "and p.paymentSystemId = :paymentSystemId " +
                              (tdsProfileId != null ? "and p.id <> :tdsProfileId" : "");
            Query<TDSServerProfile> query = session.createQuery(queryStr);
            query.setParameter("requestorID", requestorId);
            query.setParameter("paymentSystemId", paymentSystemId);
            if (tdsProfileId != null) {
                query.setParameter("tdsProfileId", tdsProfileId);
            }
            return query;
        };
        TDSServerProfile tdsServerProfile = getPersistable(queryBuilder, TDSServerProfile.class);
        return tdsServerProfile;
    }

    public boolean isExistingDuplicateIssuersBin(Integer issuerId, String issuerBin, Integer paymentSystemId) {
        Session session = getCurrentSession();
        try {
            SessionUtils.txBegin(session);
            String hqlQuery = "select count(*) from Issuer iss" +
                              " where iss.BIN = :issuerBin and iss.paymentSystemId = :paymentSystemId" +
                              (issuerId != null ? " and iss.id <> :issuerId" : "");
            Query<Number> findQuery = session.createQuery(hqlQuery);
            findQuery.setParameter("issuerBin", issuerBin);
            findQuery.setParameter("paymentSystemId", paymentSystemId);
            if (issuerId != null) {
                findQuery.setParameter("issuerId", issuerId);
            }
            Number number = findQuery.uniqueResult();
            session.getTransaction().commit();
            return number.intValue() > 0;
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
    }

    public User getUserByLoginName(String loginname) {
        Session session = getCurrentSession();
        User found = null;
        try {
            SessionUtils.txBegin(session);

            org.hibernate.Query findQuery = session.createQuery("select u from User u where u.loginname=:lgn");
            findQuery.setString("lgn", loginname);

            List<User> foundByLgn = findQuery.list();
            if (foundByLgn.size() > 0) {
                found = foundByLgn.get(0);
            }

            session.getTransaction().commit();

        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return found;
    }

    public final void update(Persistable udb) throws HibernateException {
        Session session = getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.setTimeout(this.transactionTimeOut);
        try {
            SessionUtils.txBegin(session);
            session.update(udb);
            tx.commit();
        } catch (HibernateException e) {
            SessionUtils.txRollback(session);
            try {
                session.evict(udb);
            } catch (Exception dc) {
            }

            throw e;
        }
    }

    public List<CardRange> getCardBins(String[] dsURL, String forwardURI) {
        ScrollableResults scrollableResults = null;
        Session session = getCurrentSession();
        List<CardRange> cardRanges = new ArrayList<>();
        try {
            Transaction tx = session.getTransaction();
            tx.setTimeout(getTransactionTimeOut() * 3);
            log.info("PReq load ranges scroll");
            SessionUtils.txBegin(session);
            String query = createGetCardBinsByStatusSQL();
            SQLQuery findQuery = session.createSQLQuery(query);

            findQuery.setReadOnly(true);
            scrollableResults = findQuery.scroll(ScrollMode.FORWARD_ONLY);
            int count = 0;

            while (scrollableResults.next()) {

                CardRange cardRange = new CardRange();
                Object[] row = scrollableResults.get();
                if (row[0] instanceof Object[]) {
                    row = (Object[]) row[0];
                }
                cardRange.setBinSt((String) row[0]);
                cardRange.setEnd((String) row[1]);
                cardRange.setMethodURL(Misc.defaultToNull((String) row[2]));
                cardRange.setStartProtocolVersion((String) row[3]);
                cardRange.setEndProtocolVersion((String) row[4]);
                cardRange.setIssuerId((Integer) row[5]);
                cardRange.setAcsInfoInd(Misc.defaultToNull((String) row[6]));
                Boolean includeThreeDSMethodURL = (Boolean) row[7];
                if (includeThreeDSMethodURL == null) {
                    includeThreeDSMethodURL = true;
                }
                cardRange.setIncludeThreeDSMethodURL(includeThreeDSMethodURL);

                Integer acpId = (Integer) row[8];

                if (forwardURI != null) {
                    cardRange.setMethodURL(getThreeDMethodRelayURL(acpId, dsURL, forwardURI));
                }

                cardRange.setPaymentSystemId((Integer) row[9]);
                cardRange.setAcsInfoInd2_3_0((String)row[10]);
                cardRanges.add(cardRange);
                count++;
                if (count % 5000 == 0) {
                    log.info("PReq fill Ranges scroll " + count);
                }
            }

            scrollableResults.close();
            tx.commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            if (scrollableResults != null) {
                try {
                    scrollableResults.close();
                } catch (Exception dc) {
                }
            }
            throw e;
        }
        return cardRanges;
    }

    private CharSequence getThreeDMethodRelayURL(Integer acpId, String[] dsURL, String forwardURI) {
        int ls = dsURL[0].lastIndexOf('/');
        if (ls > 0) {
            return new StringBuilder(dsURL[0].substring(0, ls)).append(forwardURI).append('/').append(acpId);
        }
        return null;
    }

    protected String createGetCardBinsByStatusSQL() {
        String getCardBinsByStatusNativeSql = null;
        if (getCardBinsByStatusNativeSql == null) {
            // Selects all ranges from all issuers which have existing acsProfiles (or attempt ACS profiles)
            // All entities have participating/active status.
            // In case of multiple acsProfiles and aacs,
            // will use the issuer's first acsProfile if present, otherwise will use the AACS.
            getCardBinsByStatusNativeSql =
                    "SELECT DISTINCT " +
                    "cRange.bin, " +
                    "cRange.end, " +
                    "acsProfile.threeDSMethodURL, " +
                    "acsProfile.startProtocolVersion, " +
                    "acsProfile.endProtocolVersion, " +
                    "cRange.issuerId, " +
                    "cRange.acsInfoInd, " +
                    "cRange.includeThreeDSMethodURL, " +
                    "acsProfile.id, " +
                    "issuer.paymentSystemId, " +
                    "acsProfile.acsInfoInd AS acsInfoInd_2_3_0 " +
                    "FROM ds_card_ranges AS cRange " +
                    "JOIN ds_issuers issuer ON cRange.issuerId=issuer.id " +
                    "JOIN ds_acsprofiles acsProfile ON (cRange.issuerId=acsProfile.issuerId OR issuer.attemptsACSPro=acsProfile.id) " +
                    "WHERE cRange.status='" + DSModel.CardRange.Status.PARTICIPATING + "' " +
                    "AND acsProfile.id=(SELECT id FROM ds_acsprofiles " +
                    "WHERE (issuerId=cRange.issuerId OR id=issuer.attemptsACSPro) AND status='" +
                    DSModel.ACSProfile.Status.ACTIVE + "' AND issuer.status='" + DSModel.Issuer.Status.ACTIVE +
                    "' AND (setv=cRange.setv OR (issuer.attemptsACSPro=id AND setv IS NULL)) " +
                    "ORDER BY issuerId DESC, id ASC LIMIT 1) ORDER BY cRange.issuerId, cRange.bin, acsProfile.id";
        }

        log.info("Retrieving card range data for PRes using SQL: {}", getCardBinsByStatusNativeSql);
        return getCardBinsByStatusNativeSql;
    }

    public List<Report> listReports(ReportListItem listItem) {

       List<Report> reports = null;
        if (listItem != null) {
            StringBuilder where = new StringBuilder();
            Integer paymentSystemId = listItem.getPaymentSystemId();
            boolean paymentSystemIdPresent = false;
            if (paymentSystemId != null) {
                if (PaymentSystem.UNLIMITED.equals(paymentSystemId)) {
                    where.append(" AND r.paymentSystemId IS NULL");
                } else if (!PaymentSystem.ALL.equals(paymentSystemId)) {
                    paymentSystemIdPresent = true;
                    where.append(" AND r.paymentSystemId=:paymentSystemId");
                }
            }

            Session session = getCurrentSession();
            try {

                SessionUtils.txBegin(session);

                String qstr = "select r from Report r where r.status!=:deletedStatus " + where.toString() +
                        " order by r.createdDate desc";
                Query findQuery = session.createQuery(qstr);
                Query countQuery = session.createQuery(
                        "select count(*) from Report r where r.status!=:deletedStatus  " + where.toString());

                findQuery.setParameter("deletedStatus", DSModel.Report.Status.DELETED);
                countQuery.setParameter("deletedStatus", DSModel.Report.Status.DELETED);

                if (paymentSystemIdPresent) {
                    findQuery.setParameter("paymentSystemId", listItem.getPaymentSystemId());
                    countQuery.setParameter("paymentSystemId", listItem.getPaymentSystemId());
                }

                Number count = (Number) countQuery.uniqueResult();
                listItem.setTotal(count.longValue());

                if (listItem.getStart() != null) {
                    findQuery.setFirstResult(listItem.getStart());
                }
                if (listItem.getLimit() != null) {
                    findQuery.setMaxResults(listItem.getLimit());
                }

                reports = findQuery.list();
                session.getTransaction().commit();

            } catch (Exception e) {
                SessionUtils.txRollback(session);
                throw e;
            }
        }
        return reports;
    }

    protected interface QueryBuilder {
        Query build(Session session);
    }

    public License getLicense() {
        List<License> licenseList;
        Session session = getCurrentSession();
        try {
            String queryStr = "select l from License l";
            SessionUtils.txBegin(session);
            Query findQuery = session.createQuery(queryStr);
            licenseList = findQuery.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            SessionUtils.txRollback(session);
            throw e;
        }
        return licenseList.get(0);
    }
}
