/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 04.04.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import java.util.Date;

public class TDSRecordSearcher extends TDSRecord {

    private static final long serialVersionUID = 1L;

    private Integer start;
    private Integer limit;
    private Integer totalResults;
    private String order;
    private String orderDirection;
    private String keyword;
    private Integer queryTimeout;
    private Integer searchLimit;
    private boolean overLimit;
    private Long total;
    private Long lastId;
    private Long startId;
    private Boolean complete;

    private Date trDateFrom;
    private Date trDateTo;
    private String[] localStatuses;
    private Long purchaseAmountLow;
    private Long purchaseAmountHigh;
    private String first6;
    private String last4;

	/*
	public void setMerchantCountryAsInt(Integer i)
	{
		super.setMerchantCountry(i!=null ? i.shortValue() : null);
	}

	public Integer getMerchantCountryAsInt()
	{
		Short v=super.getMerchantCountry();
		return v!=null ? v.intValue() : null;
	}


	public void setLocalStatusAsInt(Integer i)
	{
		super.setLocalStatus(i!=null ? i.shortValue() : null);
	}
	public Integer getLocalStatusAsInt()
	{
		Short v=super.getLocalStatus();
		return v!=null ? v.intValue() : null;
	}
	*/


    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer total) {
        this.totalResults = total;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Date getTrDateFrom() {
        return trDateFrom;
    }

    public void setTrDateFrom(Date trDateFrom) {
        this.trDateFrom = trDateFrom;
    }

    public Date getTrDateTo() {
        return trDateTo;
    }

    public void setTrDateTo(Date trDateTo) {
        this.trDateTo = trDateTo;
    }

    public Integer getQueryTimeout() {
        return queryTimeout;
    }

    public void setQueryTimeout(Integer queryTimeout) {
        this.queryTimeout = queryTimeout;
    }

    public Integer getSearchLimit() {
        return searchLimit;
    }

    public void setSearchLimit(Integer searchLimit) {
        this.searchLimit = searchLimit;
    }

    public boolean isOverLimit() {
        return overLimit;
    }

    public void setOverLimit(boolean overLimit) {
        this.overLimit = overLimit;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getLastId() {
        return lastId;
    }

    public void setLastId(Long lastId) {
        this.lastId = lastId;
    }

    public Long getStartId() {
        return startId;
    }

    public void setStartId(Long startId) {
        this.startId = startId;
    }

    public Boolean getComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    public String[] getLocalStatuses() {
        return localStatuses;
    }

    public void setLocalStatuses(String[] localStatuses) {
        this.localStatuses = localStatuses;
    }

    public Long getPurchaseAmountLow() {
        return purchaseAmountLow;
    }

    public void setPurchaseAmountLow(Long purchaseAmountLow) {
        this.purchaseAmountLow = purchaseAmountLow;
    }

    public Long getPurchaseAmountHigh() {
        return purchaseAmountHigh;
    }

    public void setPurchaseAmountHigh(Long purchaseAmountHigh) {
        this.purchaseAmountHigh = purchaseAmountHigh;
    }

    public String getFirst6() {
        return first6;
    }

    public void setFirst6(String first6) {
        this.first6 = first6;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }
}
