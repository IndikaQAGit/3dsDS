package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;

public class PaymentSystem extends PersistableClass implements PaymentSystemResource, Comparable<PaymentSystem>, Persistable {

    public static Integer UNLIMITED = -1;
    public static Integer ALL = -2;

    private Integer id;
    private String name;
    private String type;
    private Short port;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Short getPort() {
        return port;
    }

    public void setPort(Short port) {
        this.port = port;
    }

    @Override
    public int compareTo(PaymentSystem o) {
        if (o == null || o.getName() == null) return -1;
        if (this.getName() == null) return 1;
        return this.getName().compareToIgnoreCase(o.getName());
    }

    @Override
    public Integer getPaymentSystemId() {
        return id;
    }
}
