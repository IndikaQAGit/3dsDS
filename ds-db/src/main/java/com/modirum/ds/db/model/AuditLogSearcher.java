package com.modirum.ds.db.model;

import java.util.Date;

public class AuditLogSearcher extends AuditLog implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private Integer start;
    private Integer limit;
    private Integer totalResults;
    private String order;
    private String orderDirection;
    private String keyword;
    private String[] actions;
    private Integer paymentSystemId;
    private Date logDateFrom;
    private Date logDateTo;

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTotal() {
        return totalResults;
    }

    public void setTotal(Integer total) {
        this.totalResults = total;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Date getLogDateFrom() {
        return logDateFrom;
    }

    public void setLogDateFrom(Date trDateFrom) {
        this.logDateFrom = trDateFrom;
    }

    public Date getLogDateTo() {
        return logDateTo;
    }

    public void setLogDateTo(Date trDateTo) {
        this.logDateTo = trDateTo;
    }

    public String[] getActions() {
        return actions;
    }

    public void setActions(String[] actions) {
        this.actions = actions;
    }

    public Integer getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }
}
