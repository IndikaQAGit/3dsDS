/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 24.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

public interface DSModel {

    Short DEFAULT_SET = (short) 1;

    interface CardRange {
        interface Status {
            String PARTICIPATING = "P";
            String NOTPARTICIPATING = "N";
            String UNKNOWN = "U";
            //String ENDED = "E";
        }
    }

    interface Address {
        interface Type {
            String SHIPPING = "S"; // user added shipping addresses
            String BILLING = "B"; // fixed primary address / billing address (address that use has supplied to issuer by other means)
        }
    }

    interface User {
        interface Status {
            String ACTIVE = "A";
            String DISABLED = "D";
            String TEMP_LOCKED = "L";
            String TEMPLATE = "T";
            String ENDED = "E";
        }
    }

    interface Issuer {
        interface Status {
            String ACTIVE = "A";
            String DISABLED = "D";
            String ENDED = "E";
        }
    }

    interface Acquirer {
        interface Status {
            String ACTIVE = "A";
            String DISABLED = "D";
            String ENDED = "E";
        }
    }

    interface Merchant {
        interface Status {
            String ACTIVE = "A";
            String DISABLED = "D";
            //String TEMPLATE="T";
            String ENDED = "E";
        }
    }


    interface ACSProfile {
        interface Status {
            String ACTIVE = "A";
            String DISABLED = "D";
            //String TEMPLATE="T";
            String ENDED = "E";
        }
    }

    interface TDSServerProfile {
        interface Status {
            String ACTIVE = "A";
            String DISABLED = "D";
            //String TEMPLATE="T";
            String ENDED = "E";
        }
    }

	/*
	interface CardInfo
	{
		interface Status
		{
			String ACTIVE="A";
			String BLOCKED="B";
			String DELETED="D";
		}

		interface ThreeDStatus // enrollment status
		{
			String ENROLLED="Y";
			String NOT_ENROLLED="N";
			String UNABLE="U";
		}

	}
	*/

    interface TSDRecord {
        interface Status {
            Short INPROCESS = 10;
            Short CHANLLENGE_INPROCESS = 20;
            Short CHANLLENGE_COMPLETED = 25; // challenge completed but processing not yet complete (RReq not forwarded)
            Short COMPLETED = 50;
            Short ERROR = 100;
            Short ACSERROR = 110;
            Short MIERROR = 112;
            //Short ACSIREQ=120;
            //Short MIIREQ=122;
            Short ACSCOMERROR = 130;
            Short MICOMERROR = 132;
            Short ACSTOOLATE = 140;
        }

    }


    interface AuditLog {
        interface Action {
            String INSERT = "ins";
            String UPDATE = "upd";
            String DELETE = "del";
            String VIEW = "view";
            String LOGIN = "login";
            String LOGINFAIL = "loginfail";
            String LOGOFF = "logoff";
            String UNAUTH = "unauthorized";
            String SEARCH = "search";
            String APPSTART = "appStart";
            String APPSTOP = "appStop";
        }
    }

    interface HsmDevice {
        interface Status {
            String ACTIVE = "A";
            String DISABLED = "D";
        }
    }

    interface HsmDeviceConf {
        interface dsId {
            Integer DEFAULT_DS = 0;
        }
    }

    interface CertificateData {
        interface Status extends com.modirum.ds.db.model.CertificateData.Status {
        }
    }

    interface Report {
        interface Status {
            String QUEUED = "Q";
            String PROCESSING = "P";
            String COMPLETED = "C";
            String ERROR = "E";
            String DELETED = "D";
            String UNKNOWN = "U";
        }
    }
}
