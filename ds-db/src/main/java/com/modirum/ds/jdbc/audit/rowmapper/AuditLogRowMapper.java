package com.modirum.ds.jdbc.audit.rowmapper;

import com.modirum.ds.jdbc.SqlUtil;
import com.modirum.ds.jdbc.audit.model.AuditLog;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuditLogRowMapper implements RowMapper<AuditLog> {
    @Override
    public AuditLog mapRow(ResultSet rs, int i) throws SQLException {
        AuditLog auditLog = new AuditLog();
        auditLog.setId(SqlUtil.getLong(rs, "id"));
        auditLog.setWhendate(rs.getTimestamp("whendate"));
        auditLog.setByuser(rs.getString("byuser"));
        auditLog.setByuserId(SqlUtil.getLong(rs, "byuserId"));
        auditLog.setAction(rs.getString("action"));
        auditLog.setObjectId(rs.getLong("objectId"));
        auditLog.setObjectClass(rs.getString("objectClass"));
        auditLog.setDetails(rs.getString("details"));
        auditLog.setPaymentSystemId(SqlUtil.getInteger(rs, "paymentSystemId"));
        return  auditLog;
    }
}
