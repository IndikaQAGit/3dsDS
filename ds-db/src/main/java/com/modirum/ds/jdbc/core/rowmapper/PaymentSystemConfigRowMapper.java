package com.modirum.ds.jdbc.core.rowmapper;

import com.modirum.ds.jdbc.core.model.PaymentSystemConfig;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class PaymentSystemConfigRowMapper implements RowMapper<PaymentSystemConfig> {
    @Override
    public PaymentSystemConfig mapRow(ResultSet resultSet, int i) throws SQLException {
        return PaymentSystemConfig.builder()
                                  .dsId(resultSet.getInt("dsId"))
                                  .paymentSystemId(resultSet.getInt("paymentSystemId"))
                                  .key(resultSet.getString("skey"))
                                  .value(resultSet.getString("value"))
                                  .comment(resultSet.getString("comment"))
                                  .createdDate(resultSet.getObject("createdDate", LocalDateTime.class))
                                  .createdUser(resultSet.getString("createdUser"))
                                  .lastModifiedDate(resultSet.getObject("lastModifiedDate", LocalDateTime.class))
                                  .lastModifiedBy(resultSet.getString("lastModifiedBy"))
                                  .build();
    }
}
