package com.modirum.ds.jdbc.core.dao;

import com.modirum.ds.jdbc.BaseDAO;
import com.modirum.ds.jdbc.core.model.TdsRecordAttribute;
import com.modirum.ds.jdbc.core.rowmapper.TdsRecordAttributeRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class TdsRecordAttributeDAO extends BaseDAO {

    private final TdsRecordAttributeRowMapper tdsRecordAttributeRowMapper = new TdsRecordAttributeRowMapper();

    private final static List<String> columnNames = Arrays.asList("id", "tdsRecordId", "attr", "value",
            "asciiValue", "createdDate", "createdUser");

    private final static List<String> columnNamesAssignment = columnNames.stream().map(columnName -> ":" + columnName)
            .collect(Collectors.toList());

    /**
     * Insert a TDS Record Attribute
     * @param tdsRecordAttribute
     */
    public void insert(TdsRecordAttribute tdsRecordAttribute) {
        String sql = String.format("INSERT INTO ds_tdsrecord_attributes (%s) VALUES (%s)",
                            String.join(",", columnNames),
                            String.join(",", columnNamesAssignment));

        SqlParameterSource params = new MapSqlParameterSource("id", tdsRecordAttribute.getId())
                .addValue("tdsRecordId", tdsRecordAttribute.getTdsRecordId())
                .addValue("attr", tdsRecordAttribute.getKey())
                .addValue("value", tdsRecordAttribute.getValue())
                .addValue("asciiValue", tdsRecordAttribute.getAsciiValue())
                .addValue("createdDate", tdsRecordAttribute.getCreatedDate())
                .addValue("createdUser", tdsRecordAttribute.getCreatedUser());
        getNamedParameterJdbcTemplate().update(sql, params);
    }

    /**
     * Returns the specified key/attribute of tdsRecordId
     * @param tdsRecordId
     * @param key
     * @return
     */
    public TdsRecordAttribute getTDSSRecordAttributeByRecordIDAndKey(Long tdsRecordId, String key) {
        String sql = String.format("SELECT %s " +
                                    "FROM ds_tdsrecord_attributes " +
                                    "WHERE tdsRecordId = :tdsRecordId AND attr = :attr",
                            String.join(",", columnNames));
        SqlParameterSource params = new MapSqlParameterSource("tdsRecordId", tdsRecordId)
                .addValue("attr", key);
        List<TdsRecordAttribute> result =  getNamedParameterJdbcTemplate().query(sql, params, tdsRecordAttributeRowMapper);
        return CollectionUtils.isEmpty(result) ? null : result.get(0);
    }

    /**
     * returns a list of tdsrecordattribute rows from the db
     *
     * @param tdsRecordId
     * @return
     */
    public List<TdsRecordAttribute> getRecordAttributesByTDSRecordId(Long tdsRecordId) {
        String sql = String.format("SELECT %s " +
                                    "FROM ds_tdsrecord_attributes " +
                                    "WHERE tdsRecordId = :tdsRecordId",
                            String.join(",", columnNames));
        SqlParameterSource params = new MapSqlParameterSource("tdsRecordId", tdsRecordId);
        List<TdsRecordAttribute> result =  getNamedParameterJdbcTemplate().query(sql, params, tdsRecordAttributeRowMapper);
        return result;
    }

}
