package com.modirum.ds.jdbc.core.dao;

import com.modirum.ds.jdbc.BaseDAO;
import com.modirum.ds.jdbc.SqlUtil;
import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import com.modirum.ds.jdbc.core.rowmapper.HsmDeviceConfRowMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class HsmDeviceConfDAO extends BaseDAO {

    private final HsmDeviceConfRowMapper hsmDeviceConfRowMapper = new HsmDeviceConfRowMapper();

    /**
     * Retrieve an HSM Device Conf given its hsmDeviceId and dsId
     * @param hsmDeviceId
     * @param dsId
     * @return
     */
    public HsmDeviceConf get(Long hsmDeviceId, Integer dsId) {
        String sql = "SELECT hsmdevice_id, dsId, config FROM ds_hsmdevice_conf WHERE hsmdevice_id = ? AND dsId = ?";
        return getJdbcTemplate().query(sql, hsmDeviceConfRowMapper, hsmDeviceId, dsId)
            .stream().findFirst().orElse(null);
    }

    /**
     * Check if an HSM Device Conf exists given its hsmDeviceId and dsId
     * @param hsmDeviceId
     * @param dsId
     * @return
     */
    public boolean exists(Long hsmDeviceId, Integer dsId) {
        String sql = "SELECT 1 FROM ds_hsmdevice_conf WHERE hsmdevice_id = ? AND dsId = ? LIMIT 1";
        return getJdbcTemplate().queryForList(sql, Long.class, hsmDeviceId, dsId)
            .stream().findFirst().isPresent();
    }

    /**
     * Insert an HSM Device Conf
     * @param hsmDeviceConf
     */
    public void insert(HsmDeviceConf hsmDeviceConf) {
        String sql = "INSERT INTO ds_hsmdevice_conf (hsmdevice_id, dsId, config) " +
            "VALUES (:hsmdevice_id, :dsId, :config)";

        Map<String, Object> params = new HashMap<>();
        params.put("hsmdevice_id", hsmDeviceConf.getHsmDeviceId());
        params.put("dsId", hsmDeviceConf.getDsId());
        params.put("config", hsmDeviceConf.getConfig());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    /**
     * Update an HSM Device Conf
     * @param hsmDeviceConf
     */
    public void update(HsmDeviceConf hsmDeviceConf) {
        String sql = "UPDATE ds_hsmdevice_conf SET config = :config WHERE hsmdevice_id = :hsmdevice_id AND dsId = :dsId";

        Map<String, Object> params = new HashMap<>();
        params.put("hsmdevice_id", hsmDeviceConf.getHsmDeviceId());
        params.put("dsId", hsmDeviceConf.getDsId());
        params.put("config", hsmDeviceConf.getConfig());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    /**
     * Delete an HSM Device Conf
     * @param hsmDeviceConf
     */
    public void delete(HsmDeviceConf hsmDeviceConf) {
        String sql = "DELETE FROM ds_hsmdevice_conf WHERE hsmdevice_id = :hsmdevice_id AND dsId = :dsId";

        Map<String, Object> params = new HashMap<>();
        params.put("hsmdevice_id", hsmDeviceConf.getHsmDeviceId());
        params.put("dsId", hsmDeviceConf.getDsId());

        getNamedParameterJdbcTemplate().update(sql, params);
    }
}
