package com.modirum.ds.jdbc.core.rowmapper;

import com.modirum.ds.jdbc.core.model.HsmDevice;
import com.modirum.ds.utils.JdbcUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HsmDeviceRowMapper implements RowMapper<HsmDevice> {

    @Override
    public HsmDevice mapRow(ResultSet rs, int i) throws SQLException {
        return HsmDevice.builder()
            .id(JdbcUtils.getColumnValue(rs, "id", Long.class))
            .status(JdbcUtils.getColumnValue(rs, "status", String.class))
            .className(JdbcUtils.getColumnValue(rs, "className", String.class))
            .name(JdbcUtils.getColumnValue(rs, "name", String.class))
            .build();
    }

}
