package com.modirum.ds.jdbc.core.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Represents ds_tdsrecord_attributes table entry.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TdsRecordAttribute {

    private String id;
    private Long tdsRecordId;
    private String key;
    private String value;
    private String asciiValue;
    private Date createdDate;
    private String createdUser;

}
