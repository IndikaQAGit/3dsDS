package com.modirum.ds.jdbc.core.dao;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import com.modirum.ds.jdbc.test.support.RunWithDB;
import com.modirum.ds.jdbc.test.support.TestEmbeddedDbUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

@RunWithDB
public class HsmDeviceConfDAOTest {

    private static final DataSource dataSource = TestEmbeddedDbUtil.dataSource();

    private static HsmDeviceConfDAO hsmDeviceConfDAO;

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
        hsmDeviceConfDAO = new HsmDeviceConfDAO();
        hsmDeviceConfDAO.setDataSource(dataSource);
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice_conf;"})
    public void insert_success() {
        HsmDeviceConf expectedHsmDeviceConf = new HsmDeviceConf();
        expectedHsmDeviceConf.setHsmDeviceId(1L);
        expectedHsmDeviceConf.setDsId(0);
        expectedHsmDeviceConf.setConfig("host=127.0.0.1");
        hsmDeviceConfDAO.insert(expectedHsmDeviceConf);

        HsmDeviceConf actualHsmDeviceConf = hsmDeviceConfDAO.get(1L, 0);
        Assertions.assertNotNull(actualHsmDeviceConf);
        Assertions.assertEquals("host=127.0.0.1", actualHsmDeviceConf.getConfig());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice_conf;"})
    public void get_success() {
        HsmDeviceConf expectedHsmDeviceConf = new HsmDeviceConf();
        expectedHsmDeviceConf.setHsmDeviceId(1L);
        expectedHsmDeviceConf.setDsId(0);
        expectedHsmDeviceConf.setConfig("host=127.0.0.1");
        hsmDeviceConfDAO.insert(expectedHsmDeviceConf);

        HsmDeviceConf actualHsmDeviceConf = hsmDeviceConfDAO.get(1L, 0);
        Assertions.assertNotNull(actualHsmDeviceConf);
        Assertions.assertEquals("host=127.0.0.1", actualHsmDeviceConf.getConfig());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice_conf;"})
    public void update_success() {
        HsmDeviceConf expectedHsmDeviceConf = new HsmDeviceConf();
        expectedHsmDeviceConf.setHsmDeviceId(1L);
        expectedHsmDeviceConf.setDsId(0);
        expectedHsmDeviceConf.setConfig("host=127.0.0.1");
        hsmDeviceConfDAO.insert(expectedHsmDeviceConf);

        expectedHsmDeviceConf.setConfig("host=127.0.0.1;timeout=30");
        hsmDeviceConfDAO.update(expectedHsmDeviceConf);

        HsmDeviceConf actualHsmDeviceConf = hsmDeviceConfDAO.get(1L, 0);
        Assertions.assertNotNull(actualHsmDeviceConf);
        Assertions.assertEquals("host=127.0.0.1;timeout=30", actualHsmDeviceConf.getConfig());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice_conf;"})
    public void exists_success() {
        HsmDeviceConf expectedHsmDeviceConf = new HsmDeviceConf();
        expectedHsmDeviceConf.setHsmDeviceId(1L);
        expectedHsmDeviceConf.setDsId(0);
        expectedHsmDeviceConf.setConfig("host=127.0.0.1");
        hsmDeviceConfDAO.insert(expectedHsmDeviceConf);

        boolean isExists = hsmDeviceConfDAO.exists(1L, 0);
        Assertions.assertTrue(isExists);
    }
}
