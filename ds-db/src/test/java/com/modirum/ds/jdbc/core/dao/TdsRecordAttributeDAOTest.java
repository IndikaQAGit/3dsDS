package com.modirum.ds.jdbc.core.dao;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.jdbc.core.model.TdsRecordAttribute;
import com.modirum.ds.jdbc.test.support.TestEmbeddedDbUtil;
import com.modirum.ds.utils.ObjectDiff;
import org.apache.commons.collections.CollectionUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TdsRecordAttributeDAOTest {

    private static final DataSource dataSource = TestEmbeddedDbUtil.dataSource();

    private static TdsRecordAttributeDAO dao;

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
        dao = new TdsRecordAttributeDAO();
        dao.setDataSource(dataSource);
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_tdsrecord_attributes;"})
    public void insert_success() {
        Date date = new Date();
        String id = UUID.randomUUID().toString();
        dao.insert(TdsRecordAttribute.builder()
                .tdsRecordId(12345L)
                .createdUser("admin")
                .createdDate(date)
                .key("threeDSServerURL")
                .asciiValue("http://localhost")
                .id(id)
                .build());

        List<TdsRecordAttribute> attributes = dao.getRecordAttributesByTDSRecordId(12345L);
        Assertions.assertTrue(CollectionUtils.isNotEmpty(attributes));
        Assertions.assertEquals(attributes.size(), 1);
        TdsRecordAttribute attribute = attributes.get(0);
        Assertions.assertEquals(12345L, attribute.getTdsRecordId());
        Assertions.assertEquals("admin", attribute.getCreatedUser());
        Assertions.assertEquals(date, attribute.getCreatedDate());
        Assertions.assertEquals("threeDSServerURL", attribute.getKey());
        Assertions.assertNull(attribute.getValue());
        Assertions.assertEquals("http://localhost", attribute.getAsciiValue());
        Assertions.assertEquals(id, attribute.getId());

        TdsRecordAttribute attribute2 = dao.getTDSSRecordAttributeByRecordIDAndKey(12345L, "threeDSServerURL");
        Assertions.assertNotNull(attribute2);
        Assertions.assertTrue(CollectionUtils.isEmpty(ObjectDiff.diff(attribute, attribute2)));
    }

}
