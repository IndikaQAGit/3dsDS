package com.modirum.ds.reporting.services;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.reporting.RunWithDB;
import com.modirum.ds.reporting.model.HsmDevice;
import com.modirum.ds.reporting.model.HsmDeviceConf;
import com.modirum.ds.reporting.testutil.TestEmbeddedDbUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

@RunWithDB
public class HsmDeviceTest {
    private static DataSource dataSource = TestEmbeddedDbUtil.dataSource();
    private static HsmDeviceReportService hsmDeviceReportService = ServiceLocator.initInstance(dataSource).getHsmDeviceReportService();

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeScriptsBefore = {"/services/hsm-device.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice;"})
    public void listActiveHsmDevices() throws Exception {
        List<HsmDevice> hsmDevices = hsmDeviceReportService.listActiveHsmDevices();
        Assertions.assertNotNull(hsmDevices);
        Assertions.assertEquals(1, hsmDevices.size());
    }

    @Test
    @DataSet(executeScriptsBefore = {"/services/hsm-device-conf.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice_conf;", "TRUNCATE TABLE ds_hsmdevice;"})
    public void getHsmDeviceConfig() throws Exception {
        HsmDeviceConf hsmDeviceConfig = hsmDeviceReportService.getHsmDeviceConfig(1L, 0);
        Assertions.assertNotNull(hsmDeviceConfig);
        Assertions.assertEquals("timeout=30", hsmDeviceConfig.getConfig());
    }
}
