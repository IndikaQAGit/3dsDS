package com.modirum.ds.reporting.services;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.reporting.RunWithDB;
import com.modirum.ds.reporting.model.Id;
import com.modirum.ds.reporting.model.rowmapper.IdResultSetMapper;
import com.modirum.ds.reporting.testutil.TestEmbeddedDbUtil;
import com.modirum.ds.reporting.util.JdbcUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

@RunWithDB
public class IdServiceTest {
    private static DataSource dataSource = TestEmbeddedDbUtil.dataSource();
    private static IdService idService = ServiceLocator.initInstance(dataSource).getIdService();

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeScriptsBefore = {"/services/id-service.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_ids;"})
    public void logAction() throws Exception {
        Id id = idService.getNextId(AuditLogJdbcService.AUDIT_ID_SEQUENCE_NAME);
        Assertions.assertNotNull(id);
        Assertions.assertEquals(Long.valueOf(2L), id.getSeed());

        List<Id> ids = JdbcUtils.query(dataSource, "SELECT * FROM ds_ids WHERE name=?", new Object[]{AuditLogJdbcService.AUDIT_ID_SEQUENCE_NAME}, new IdResultSetMapper());
        Assertions.assertNotNull(ids);
        Assertions.assertEquals(1, ids.size());
        Assertions.assertEquals(id.getSeed(), ids.get(0).getSeed());
        Assertions.assertEquals(id.getLastUpdate(), ids.get(0).getLastUpdate());
    }
}
