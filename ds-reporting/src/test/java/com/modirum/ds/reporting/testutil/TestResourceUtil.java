package com.modirum.ds.reporting.testutil;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

/**
 * Helper methods related on java test resources.
 */
public class TestResourceUtil {
    /**
     * Gets the base path of the test resource directory
     * @return
     * @throws URISyntaxException
     */
    public static String getBasePath() throws URISyntaxException {
        return getPath("");
    }

    /**
     * Retrieves the absolute path based on the relative path under test resource.
     *
     * @param resourceRelativePath The relative path under test resource
     * @return
     */
    public static String getPath(String resourceRelativePath) throws URISyntaxException {
        return TestResourceUtil.class.getClassLoader().getResource(resourceRelativePath).toURI().getPath();
    }

    /**
     * This creates a directory/folder under test resource path.
     *
     * @param tempDirectory
     * @return
     */
    public static String createTempDirectory(String tempDirectory) throws IOException, URISyntaxException {
        String resourcePath = getPath("");
        File dir = new File(resourcePath + "/" + tempDirectory);
        if (!dir.exists()) {
            dir.mkdir();
        } else {
            FileUtils.cleanDirectory(dir);
        }
        return dir.getAbsolutePath();
    }

    /**
     * Cleans a directory without deleting it. The directory is relative under test resources.
     *
     * @param tempDirectory the relative directory under test resources.
     * @throws IOException
     */
    public static void cleanDirectory(String tempDirectory) throws IOException, URISyntaxException {
        cleanDirectory(tempDirectory, true);
    }

    /**
     * Cleans a directory without deleting it. The directory is relative under test resources.
     *
     * @param tempDirectory the relative directory under test resources.
     * @param resourceRelativePath if true then the directory is relative to test resource, otherwise it is an absolute url
     * @throws IOException
     */
    public static void cleanDirectory(String tempDirectory, boolean resourceRelativePath) throws IOException, URISyntaxException {
        String directoryPath = tempDirectory;
        if (resourceRelativePath) {
            directoryPath = getPath("") + tempDirectory;
        }
        File dir = new File(directoryPath);
        if (dir.exists()) {
            FileUtils.cleanDirectory(dir);
        }
    }

    /**
     * Reads the file referred by this resourceFilePath and return as String using UTF_8
     * charset. This resourceFilePath is relative path under test resource classpath.
     *
     * @param resourceFilePath
     * @return
     * @throws IOException
     */
    public static String readFileAsString(String resourceFilePath) throws IOException, URISyntaxException {
        String resourcePath = getPath("");
        File dir = new File(resourcePath + "/" + resourceFilePath);
        return FileUtils.readFileToString(dir, StandardCharsets.UTF_8);
    }

    /**
     * Reads the first file found under this resourceFolder and return as String using UTF_8
     * charset. This resourceFolder is relative path under test resource classpath.
     *
     * @param resourceFolder
     * @return
     * @throws IOException
     */
    public static String readFirstReportFileAsString(String resourceFolder) throws IOException, URISyntaxException {
        File firstFile = getFirstReportFile(resourceFolder);
        return FileUtils.readFileToString(firstFile, StandardCharsets.UTF_8);
    }

    /**
     * Gets the first file found under this resourceFolder. If not found, returns null.
     * This resourceFolder is relative path under test resource classpath.
     *
     * @param resourceFolder
     * @return
     * @throws IOException
     */
    public static File getFirstReportFile(String resourceFolder) throws URISyntaxException {
        String absolutePath = getPath(resourceFolder);
        File basedDir = new File(absolutePath);
        File[] dirs = basedDir.listFiles();
        if (ArrayUtils.isEmpty(dirs)) {
            return null;
        }
        File firstDir = dirs[0];
        File[] files = firstDir.listFiles();
        return ArrayUtils.isEmpty(files) ? null : files[0];
    }
}
