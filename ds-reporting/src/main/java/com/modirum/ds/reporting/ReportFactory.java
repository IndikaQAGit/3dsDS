package com.modirum.ds.reporting;

import com.modirum.ds.enums.ReportStatus;

public interface ReportFactory {
    public void processQueuedReports();
    public boolean updateReportStatus(Long id, ReportStatus status);
    public boolean updateReportStatus(Long id, ReportStatus newStatus, ReportStatus currentStatus);
    public void completeReport(Long id, String filename, String secret);
}
