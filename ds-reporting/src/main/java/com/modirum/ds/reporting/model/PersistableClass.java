package com.modirum.ds.reporting.model;

import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class PersistableClass implements Importable, Persistable, Cloneable {

    transient Byte isId;

    @XmlTransient
    public Byte getIsId() {
        return isId;
    }

    public void setIsId(Byte isid) {
        this.isId = isid;
    }

    @Override
    public int hashCode() {
        return com.modirum.ds.utils.EqualHashUtil.hashCode(this);
    }

    @Override
    public boolean equals(Object o) {
        return com.modirum.ds.utils.EqualHashUtil.equals(this, o);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
