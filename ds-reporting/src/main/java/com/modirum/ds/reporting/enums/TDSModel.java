/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 24.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.reporting.enums;

import com.modirum.ds.utils.Misc;

// const and enum for 3DS Messages
public interface TDSModel {

    enum MessageVersion {

        V2_1_0("2.1.0", false),
        V2_2_0("2.2.0", false);

        private final String value;
        private boolean support;

        MessageVersion(String value, boolean support) {
            this.value = value;
            this.support = support;
        }

        public String value() {
            return value;
        }

        public boolean isEqual(String messageVersion) {
            return value.equals(messageVersion);
        }

        public boolean isSupported() {
            return support;
        }

        public void setSupport() {
            support = true;
        }

        public static MessageVersion fromValue(String v) {
            for (MessageVersion c : MessageVersion.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            return null;//if you throw an exception here it breaks the flow if version is missing/invalid. also breaks cases
        }

        public static MessageVersion minSupported() {
            for (MessageVersion c : MessageVersion.values()) {
                if (c.support) {
                    return c;
                }
            }
            return null;
        }

        public static MessageVersion maxSupported() {
            MessageVersion m = null;
            for (MessageVersion c : MessageVersion.values()) {
                if (c.support) {
                    m = c;
                }
            }
            return m;
        }
    }

    /**
     * Table A.4: Error Code, Error Description, and Error Detail
     *
     * @author andri
     */
    enum ErrorCode {
        cInvalidMessageType101("101", "Invalid Message Type"),
        cMessageVersionNotSupported102("102", "Message version not supported"),
        cSentMessagesLimitExceeded103("103", "Sent Messages limit exceeded"),

        cRequiredFieldMissing201("201", "Required element missing"),
        cCriticalElemNotRecognized202("202", "Data element not recognised"),
        cInvalidFieldFormat203("203", "Format of one or more elements is invalid"),
        cDuplicateDataElem204("204", "Duplicate Data Element"),

        cInvalidTransactionID301("301", "Transaction ID Not Recognized"),
        cDateDecryptionFailed302("302", "Data decryption failure"),
        cAccessDenied303("303", "Access denied, invalid endpoint"),
        cInvalidISOCode304("304", "ISO code not valid"),
        cInvalidTxData305("305", "Transaction data not valid"),
        cInvalidMCC306("306", "Invalid MCC"),
        cInvalidSerialNum307("307", "Invalid serial number"),

        cUnsupportedDevice401("401", "Unsupported device"),
        cTransTimedOut402("402", "Transaction Timed Out"),
        cTransientSysFailure403("403", "Transient system failure"),
        cPermanentSysFailure404("404", "Permanent system failure"),
        cSysConnectionFailure405("405", "System connection failure");

        public final String value;
        public final String desc;

        ErrorCode(String v, String d) {
            value = v;
            desc = d;
        }

        public String value() {
            return value;
        }

        public String desc() {
            return desc;
        }

        public static ErrorCode fromValue(String v) {
            for (ErrorCode c : ErrorCode.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            return null;
        }

    }

    enum XMessageCategory {
        C01("01"), C02("02"), C80("80"), C81("81"), C82("82"), C83("83"), C84("84"), C85("85"), C86("86"), C87(
                "87"), C88("88"), C89("89"), C90("90"), C91("91"), C92("92"), C93("93"), C94("94"), C95("95"), C96(
                "96"), C97("97"), C98("98"), C99("99");
        public final String value;

        XMessageCategory(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public boolean isEqual(String value) {
            return this.value.equals(value);
        }
        public static XMessageCategory fromValue(String v) {
            for (XMessageCategory c : XMessageCategory.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }


        public static boolean contains(String v) {
            for (XMessageCategory c : XMessageCategory.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }
    }


    // 3ds message enums
    enum XtransStatus {
        //2.1
        Y("Authentication Successful"),
        N("Not Authenticated"),
        U("Authentication Could Not Be Performed"),
        A("Attempts Processing Performed"),
        C("Challenge Required; Additional authentication is required"),
        R("Authentication Rejected"),
        //2.2
        D("Challenge Required; Decoupled Authentication confirmed"),
        I("Informational Only; 3DS Requestor challenge preference acknowledged");

        private final String text;

        XtransStatus(String text) {
            this.text = text;
        }

        public String value() {
            return name();
        }

        public String getText() {
            return text;
        }

        public static String getTextFromValue(String value) {
            if (Misc.isNotNullOrEmpty(value)) {
                return fromValue(value).getText();
            }
            return value;
        }

        public boolean isEqual(String value) {
            return this.value().equals(value);
        }

        public static XtransStatus fromValue(String v) {
            return valueOf(v);
        }

    }

    //FIXME: Change names to smth looking like 01, 02.. . Text written as camel case put to the String description field of the enum. Don't forget to fix it in the MPI
    enum XtransStatusReason {
        C01AuthFailed("01", "Card authentication failed"),
        C02UnknDevice("02", "Unknown Device"),
        C03UnsupDevice("03", "Unsupported Device"),
        C04ExceedsAuthFreq("04", "Exceeds authentication frequency limit"),
        C05ExpiredCard("05", "Expired card"),
        C06InvalidPan("06", "Invalid card number"),
        C07InvalidTx("07", "Invalid transaction"),
        C08NoCardrecord("08", "No Card record"),
        C09SecurityFailure("09", "Security failure"),
        C10StolenCard("10", "Stolen card"),
        C11SupectFraud("11", "Suspected fraud"),
        C12TxNotPermitted("12", "Transaction not permitted to cardholder"),
        C13CardNotEnrolled("13", "Cardholder not enrolled in service"),
        C14TxTimedOutACS("14", "Transaction timed out at the ACS"),
        C15LowConfidence("15", "Low confidence"),
        C16MedConfidence("16", "Medium confidence"),
        C17HighConfidence("17", "High confidence"),
        C18VeryHighConfidence("18", "Very High confidence"),
        C19ExceedsACSMaxChallenges("19", "Exceeds ACS maximum challenges"),
        C20NonPaymentTransactionNotSupported("20", "Non-Payment transaction not supported"),
        C213RITransactionNotSupported("21", "3RI transaction not supported"),
        C22ACSTechnicalIssue("22", "ACS technical issue"),
        C23DecoupledAuthenticationRequired("23", "Decoupled Authentication required by ACS but not requested by 3DS Requestor"),
        C24DecoupledAuthMaxExpiryTimeExceeded("24", "3DS Requestor Decoupled Max Expiry Time exceeded"),
        C25DecoupledAuthWasProvidedInsufficientTime("25", "Decoupled Authentication was provided insufficient time to authenticate cardholder. ACS will not make attempt."),
        C26AuthenticationAttemptedButNotPerformed("26", "Authentication attempted but not performed by the cardholder");

        public final String value;
        private final String text;

        XtransStatusReason(String v, String text) {
            this.value = v;
            this.text = text;
        }

        public String value() {
            return value;
        }

        public String getText() {
            return text;
        }

        public static String getTextFromValue(String value) {
            if (Misc.isNotNullOrEmpty(value)) {
                return fromValue(value).getText();
            }
            return null;
        }

        public static XtransStatusReason fromValue(String v) {
            for (XtransStatusReason c : XtransStatusReason.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (XtransStatusReason c : XtransStatusReason.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }


    enum XYesNo {

        Y, N;

        public String value() {
            return name();
        }

        public static XYesNo fromValue(String v) {
            return valueOf(v);
        }

    }

    enum XmessageType {
        A_REQ("AReq"), A_RES("ARes"), C_REQ("CReq"), C_RES("CRes"), R_REQ("RReq"), R_RES("RRes"), P_REQ("PReq"), P_RES(
                "PRes"), ERRO("Erro");
        public final String value;

        XmessageType(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static XmessageType fromValue(String v) {
            for (XmessageType c : XmessageType.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

    }

    // emvco Table A6
    enum TableA6Currencies {
        // excluded currencies
        C955("955"), C956("956"), C957("957"), C958("958"), C959("959"), C960("960"), C961("961"), C962("962"), C963(
                "963"), C964("964"), C999("999"), C901("901");

        public final String value;

        TableA6Currencies(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public Integer intValue() {
            return Integer.valueOf(value);
        }

        public static TableA6Currencies fromValue(String v) {
            for (TableA6Currencies c : TableA6Currencies.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

    }

    enum DeviceChannel {
        cChannelUnk00("00"), cChannelApp01("01"), cChannelBrow02("02"), cChannel3RI03("03");

        private final String value;


        DeviceChannel(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static DeviceChannel fromValue(String v) {
            for (DeviceChannel c : DeviceChannel.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }
    }

    enum ErrorComponent {
        cSDK("C"), c3DSServer("S"), cDS("D"), cACS("A");

        public final String value;


        ErrorComponent(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static ErrorComponent fromValue(String v) {
            for (ErrorComponent c : ErrorComponent.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }
    }


    enum ChallengeCancelInd2_0_0 {
        C1_ChnOtherPm("1"), C2_ChnCancelAndContShop("2"), C3_RequestorAuthCancel("3"), C4_TxTimedOutAtACS(
                "4"), C6_TxError("6"), C7_Unknown("7");

        public final String value;

        ChallengeCancelInd2_0_0(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static ChallengeCancelInd2_0_0 fromValue(String v) {
            for (ChallengeCancelInd2_0_0 c : ChallengeCancelInd2_0_0.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (ChallengeCancelInd2_0_0 c : ChallengeCancelInd2_0_0.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }

    enum ChallengeCancelInd2_0_1 {
        // 2.0.1
        C01_ChnCanel("01"), C02_RequestorAuthCancel("02"), C03_TxAbandoned("03"), CO4_TxTimedOutAtACS(
                "04"), CO5_TxError("05"), C06_Unknown("06")
        // 07-79 EMVCo future use
        // 08-89 DS future use
        ;

        public final String value;

        ChallengeCancelInd2_0_1(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static ChallengeCancelInd2_0_1 fromValue(String v) {
            for (ChallengeCancelInd2_0_1 c : ChallengeCancelInd2_0_1.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (ChallengeCancelInd2_0_1 c : ChallengeCancelInd2_0_1.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }

    enum UIType2_0_1 {
        // 2.0.1
        C01_Text("01"), C02_SingleSelect("02"), C03_MultiSelect("03"), CO4_OOB("04"), C05_HTML("05")
        // 07-79 EMVCo future use
        // 08-89 DS future use
        ;

        public final String value;

        UIType2_0_1(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static UIType2_0_1 fromValue(String v) {
            for (UIType2_0_1 c : UIType2_0_1.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (UIType2_0_1 c : UIType2_0_1.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }

    enum ACSUIType2_1_0 {
        // 2.0.1
        C01_Text("01"), C02_SingleSelect("02"), C03_MultiSelect("03"), CO4_OOB("04"), C05_HTML("05")
        // 07-79 EMVCo future use
        // 08-89 DS future use
        ;

        public final String value;

        ACSUIType2_1_0(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static ACSUIType2_1_0 fromValue(String v) {
            for (ACSUIType2_1_0 c : ACSUIType2_1_0.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (ACSUIType2_1_0 c : ACSUIType2_1_0.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }
    }

    enum ACSUIType2_2_0 {
        // 2.0.1
        C01_Text("01"), C02_SingleSelect("02"), C03_MultiSelect("03"), CO4_OOB("04"), C05_HTML("05")
        // 07-79 EMVCo future use
        // 08-89 DS future use
        ;

        public final String value;

        ACSUIType2_2_0(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static ACSUIType2_1_0 fromValue(String v) {
            for (ACSUIType2_1_0 c : ACSUIType2_1_0.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (ACSUIType2_1_0 c : ACSUIType2_1_0.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }
    }


    enum XauthenticationType {
        C01Static("01"), C02Dynamic("02"), C03OOB("03"), C04DEC(
                "04")// 3ds 2.2 only. this shouldnt ve here but the other enum is not beeing
        ;

        public final String value;

        XauthenticationType(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static XauthenticationType fromValue(String v) {
            for (XauthenticationType c : XauthenticationType.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (XauthenticationType c : XauthenticationType.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }


    enum XauthenticationType22 {
        C01Static("01"), C02Dynamic("02"), C03OOB("03"), C04Decupled("04");

        public final String value;

        XauthenticationType22(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static XauthenticationType22 fromValue(String v) {
            for (XauthenticationType22 c : XauthenticationType22.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (XauthenticationType22 c : XauthenticationType22.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }
    }

    String[] threeDSRequestorChallengeInds21 = {"01", "02", "03", "04", "08", "09"};

    enum XResultsStatus {
        C01RRes("01"), C02NoCRes("02"), C03ARes("03");

        public final String value;

        XResultsStatus(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static XResultsStatus fromValue(String v) {
            for (XResultsStatus c : XResultsStatus.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (XResultsStatus c : XResultsStatus.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }


    enum X3RIInd {
        C01Recurring("01"), C02Instalment("02"), C03AddCard("03"), C04MaintainCard("04"), C05AccountVerification("05");

        public final String value;

        X3RIInd(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static X3RIInd fromValue(String v) {
            for (X3RIInd c : X3RIInd.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (X3RIInd c : X3RIInd.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }

    enum X3RIInd22 {
        C01Recurring("01"), C02Instalment("02"), C03AddCard("03"), C04MaintainCard("04"), C05AccountVerification(
                "05"), C06SplitDelayedShipment("06"), C07TopUp("07"), C08MailOrder("08"), C09TelphoneOrder(
                "09"), C10WhiteListStatusCheck("10"), C11OtherPayment("11"), C12Reserved("12");

        public final String value;

        X3RIInd22(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static X3RIInd22 fromValue(String v) {
            for (X3RIInd22 c : X3RIInd22.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (X3RIInd22 c : X3RIInd22.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }


    enum XauthenticationMethod {
        C01Static("01"), C02SMSOTP("02"), C03EMVOTP("03"), C04APPOTP("04"), C05OTHOTP("05"), C06KBA("06"), C07OOBBIO(
                "07"), C08OOBLGN("08"), C09OOBOTH("09"), C10OTH("10"),
        ;

        public final String value;

        XauthenticationMethod(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static XauthenticationMethod fromValue(String v) {
            for (XauthenticationMethod c : XauthenticationMethod.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (XauthenticationMethod c : XauthenticationMethod.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }

    enum XDSRequestorAuthenticationInd {

        C01Payment("01"),
        C02Recurring("02"),
        C03Instalment("03"),
        C04AddCard("04"),
        C05MaintainCard("05"),
        C06ChVerEMVTokenID("06");

        public final String value;

        XDSRequestorAuthenticationInd(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static XDSRequestorAuthenticationInd fromValue(String v) {
            for (XDSRequestorAuthenticationInd c : XDSRequestorAuthenticationInd.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (XDSRequestorAuthenticationInd c : XDSRequestorAuthenticationInd.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }

    enum XDSRequestor3RIInd {
        C01Recurring("01"), C02Instalment("02"), C03AddCard("03"), C04MaintainCard("04");

        public final String value;

        XDSRequestor3RIInd(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static XDSRequestor3RIInd fromValue(String v) {
            for (XDSRequestor3RIInd c : XDSRequestor3RIInd.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

        public static boolean contains(String v) {
            for (XDSRequestor3RIInd c : XDSRequestor3RIInd.values()) {
                if (c.value.equals(v)) {
                    return true;
                }
            }
            return false;
        }

    }


    enum XthreeDSCompInd {
        Y, N, U;

        public String value() {
            return name();
        }

        public static XthreeDSCompInd fromValue(String v) {
            return valueOf(v);
        }
    }

    enum XwhiteListStatus {
        Y, N, E, P, R, U;

        public static boolean contains(String testValue) {
            for (XwhiteListStatus validStatus : XwhiteListStatus.values()) {
                if (validStatus.name().equals(testValue)) {
                    return true;
                }
            }
            return false;
        }
    }

}
