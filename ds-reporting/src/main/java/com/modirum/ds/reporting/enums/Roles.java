/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 11.04.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.reporting.enums;

public interface Roles {
    String merchantsView = "merchantsView";
    String merchantsEdit = "merchantsEdit";
    String userView = "userView";
    String userEdit = "userEdit";
    String issuerView = "issuerView";
    String issuerEdit = "issuerEdit";
    String acquirerView = "acquirerView";
    String acquirerEdit = "acquirerEdit";
    String recordsView = "recordsView";
    String panView = "panView";
    String textView = "textView";
    String textEdit = "textEdit";
    String caView = "caView";
    String caEdit = "caEdit";
    String auditLogView = "auditLogView";
    String adminSetup = "adminSetup";
    String keyManage = "keyManage";
    String reports = "reports";
}