package com.modirum.ds.reporting.util;

import com.modirum.ds.reporting.enums.DateTimeFormat;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

/**
 * Helper methods for reports.
 */
public final class ReportUtil {
    private final static Logger log = LoggerFactory.getLogger(ReportUtil.class);
    private ReportUtil(){
    }

    /**
     * Converts a report parameter to Long if it is a valid number, otherwise it specifically
     * logs and throws IllegalArgumentException for an invalid number format of the property reportParamKey.
     * @param reportParamKey the report parameter key
     * @param reportParamValue the report parameter value
     * @return
     * @throws IllegalArgumentException
     */
    public static Long convertToLong(String reportParamKey, String reportParamValue) throws IllegalArgumentException {
        try {
            return Long.valueOf(reportParamValue);
        } catch (NumberFormatException e) {
            log.error("Invalid number format for parameter [{}]", reportParamKey);
            throw new IllegalArgumentException("Invalid number format for parameter [" + reportParamKey + "]");
        }
    }

    /**
     * Converts a report parameter to Date if it is in valid format, otherwise it specifically
     * logs and throws IllegalArgumentException for an invalid date format of the property reportParamKey.
     * @param reportParamKey
     * @param reportParamValue
     * @param timeZone
     * @return
     * @throws IllegalArgumentException
     */
    public static Date convertToDate(String reportParamKey, String reportParamValue, TimeZone timeZone) throws IllegalArgumentException {
        try {
            LocalDateTime localDateTime = LocalDateTime.parse(reportParamValue, DateTimeFormatter.ofPattern(DateTimeFormat.DATETIME_FORMAT));
            Instant instant = localDateTime.atZone(timeZone.toZoneId()).toInstant();
            return Date.from(instant);
        } catch (Exception e) {
            log.error("Invalid date format for parameter [{}]. Please follow this format[{}]", reportParamKey, DateTimeFormat.DATETIME_FORMAT);
            throw new IllegalArgumentException(
                    "Invalid date format for parameter [" + reportParamKey + "]. Please follow this format[" +
                    DateTimeFormat.DATETIME_FORMAT + "]");
        }
    }

    /**
     * Common way for Reports to read and validate a required report parameter from the given Properties.
     *
     * @param properties
     * @param key
     * @return
     */
    public static String getRequiredProperty(Properties properties, String key) {
        String value = properties.getProperty(key);
        if (StringUtils.isEmpty(value)) {
            throw new IllegalArgumentException(
                    "Required parameter [" + key + "] is missing. Please supply this as cli parameters.");
        }
        return value;
    }
}
