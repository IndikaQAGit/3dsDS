package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.Id;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ResultSetMapper for Id entity
 */
public class IdResultSetMapper implements ResultSetMapper<Id> {
    @Override
    public Id map(ResultSet resultSet) throws SQLException {
        return Id
                .builder()
                .name(JdbcUtils.getColumnValue(resultSet, "name", String.class))
                .seed(JdbcUtils.getColumnValue(resultSet, "seed", Long.class))
                .lastUpdate(JdbcUtils.getColumnValue(resultSet, "lastUpdate", Long.class))
                .build();
    }
}
