package com.modirum.ds.reporting.util;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.Optional;

/**
 * Common utility for objects
 */
public final class ObjectUtils {
    private ObjectUtils() {
    }

    /**
     * This prevents NPE on String by returning empty string on null object.
     *
     * @param value String value
     * @return If object is null, this returns an empty String instead, otherwise, it returns the original object.
     */
    public static String safeNull(String value) {
        return value == null ? "" : value;
    }

    /**
     * This prevents NPE on Object[] by returning empty object array when null.
     *
     * @param value
     * @return
     */
    public static Object[] safeNull(Object[] value) {
        return Optional.ofNullable(value)
                       .orElse(ArrayUtils.EMPTY_OBJECT_ARRAY);
    }

    /**
     * Checks if the given item is included within the array of items.
     *
     * @param item The Object in check
     * @param list
     * @param <T>
     * @return Returns true if the given item is included in the list, otherwise returns false
     */
    public static <T> boolean isIn(T item, T... list) {
        return Arrays.asList(list).contains(item);
    }

    /**
     * Checks if the given item is NOT included within the array of items.
     *
     * @param item The Object in check
     * @param list
     * @param <T>
     * @return Returns true if the given item is NOT included in the list, otherwise returns false
     */
    public static <T> boolean isNotIn(T item, T... list) {
        return !isIn(item, list);
    }
}
