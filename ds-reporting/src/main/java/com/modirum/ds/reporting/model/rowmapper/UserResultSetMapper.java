package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.User;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * ResultSetMapper for User entity
 */
public class UserResultSetMapper implements ResultSetMapper<User> {
    @Override
    public User map(ResultSet resultSet) throws SQLException {
        return User
                .builder()
                .id(JdbcUtils.getColumnValue(resultSet, "id", Long.class))
                .loginName(JdbcUtils.getColumnValue(resultSet, "loginname", String.class))
                .password(JdbcUtils.getColumnValue(resultSet, "password", String.class))
                .lastPassChangeDate(JdbcUtils.getColumnValue(resultSet, "lastPassChangeDate", Timestamp.class))
                .firstName(JdbcUtils.getColumnValue(resultSet, "firstName", String.class))
                .lastName(JdbcUtils.getColumnValue(resultSet, "lastName", String.class))
                .status(JdbcUtils.getColumnValue(resultSet, "status", String.class))
                .email(JdbcUtils.getColumnValue(resultSet, "email", String.class))
                .paymentSystemId(JdbcUtils.getColumnValue(resultSet, "paymentSystemId", Integer.class))
                .phone(JdbcUtils.getColumnValue(resultSet, "phone", String.class))
                .locale(JdbcUtils.getColumnValue(resultSet, "locale", String.class))
                .build();
    }
}
