package com.modirum.ds.reporting.enums;

/**
 * Enumeration for core report config keys. This does not include report-specific parameters.
 */
public class ReportConfigKeys {
    //csv configuration
    public static final String CSV_FILTERING_ENABLED = "csv.filtering.enabled";
    public static final String CSV_PROHIBITED_CHARACTERS_REGEX = "csv.prohibitedCharacters.regex";
    public static final String CSV_SANITIZING_ENABLED = "csv.csvSanitizing.enabled";
    public static final String CSV_UNSAFE_STARTING_CHARACTERS = "csv.unsafeStartingCharacters";
    public static final String CSV_INJECTION_SANITIZER = "csv.injectionSanitizer";

    //reporting db configuration
    public static final String REPORTING_DB_DRIVER = "reporting.dbDriver";
    public static final String REPORTING_DB_URL = "reporting.dbUrl";
    public static final String REPORTING_DB_USER = "reporting.dbUser";
    public static final String REPORTING_DB_PASS = "reporting.dbPass";
    public static final String REPORTING_VALIDATION_QUERY = "reporting.validationQuery";
    //auditlog db configuration
    public static final String AUDIT_DB_DRIVER = "audit.dbDriver";
    public static final String AUDIT_DB_URL = "audit.dbUrl";
    public static final String AUDIT_DB_USER = "audit.dbUser";
    public static final String AUDIT_DB_PASS = "audit.dbPass";
    public static final String AUDIT_VALIDATION_QUERY = "audit.validationQuery";
    //reporting base directory configuration
    public static final String REPORTING_DIR = "reporting.dir";

    //core report parameters
    public static final String LOCALE = "locale";
    public static final String TIMEZONE = "timezone";
    public static final String REPORT_CLASS_NAME = "reportClassName";
    public static final String REPORT_FILE_NAME = "reportFileName";
    public static final String REPORT_ID = "report.id";
    public static final String DELETE_REPORTS_DAYS_OLD = "deleteReportsDaysOld";
    public static final String SLEEP_TIME_SECONDS = "sleepTimeSeconds";

    public static final String SINGLE_THREADED = "single.threaded";
    public static final String REPORTS_POOL_SIZE = "reports.pool.size";

}
