package com.modirum.ds.reporting.services;

import com.modirum.ds.reporting.model.Id;
import com.modirum.ds.reporting.model.rowmapper.IdResultSetMapper;
import com.modirum.ds.reporting.util.JdbcUtils;
import org.apache.commons.collections.CollectionUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Jdbc Service for Id entity
 */
public class IdService {
    private final DataSource dataSource;

    IdService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Gets the next sequence id for the given idName and updates the sequence table as well.
     *
     * @param idName
     * @return
     * @throws SQLException
     */
    public Id getNextId(String idName) throws SQLException {
        try (Connection connection = JdbcUtils.startTransaction(dataSource)) {
            String query = "SELECT name,seed,lastUpdate FROM ds_ids WHERE name=? FOR UPDATE";
            List<Id> ids = JdbcUtils.query(connection, query, new Object[]{idName}, new IdResultSetMapper());
            Id id;
            if (CollectionUtils.isNotEmpty(ids)) {
                id = ids.get(0);
                id.setSeed(id.getSeed() + 1);
                id.setLastUpdate(System.currentTimeMillis());
                JdbcUtils.execute(connection,
                                  "UPDATE ds_ids SET seed=?, lastUpdate=? WHERE name=?",
                                  new Object[]{id.getSeed(), id.getLastUpdate(), id.getName()});
            } else {
                id = Id.builder()
                       .name(idName)
                       .seed(1L)
                       .lastUpdate(System.currentTimeMillis())
                       .build();
                JdbcUtils.execute(connection,
                                  "INSERT INTO ds_ids(name, seed, lastUpdate) VALUES (?,?,?)",
                                  new Object[]{id.getName(), id.getSeed(), id.getLastUpdate()});
            }
            JdbcUtils.txCommitAndClose(connection);
            return id;
        }
    }
}
