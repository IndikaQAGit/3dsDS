package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.Text;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ResultSetMapper for Text
 */
public class TextResultSetMapper implements ResultSetMapper<Text> {
    @Override
    public Text map(ResultSet resultSet) throws SQLException {
        Text text = new Text();
        text.setId(JdbcUtils.getColumnValue(resultSet, "id", Integer.class));
        text.setKey(JdbcUtils.getColumnValue(resultSet, "tkey", String.class));
        text.setLocale(JdbcUtils.getColumnValue(resultSet, "locale", String.class));
        text.setMessage(JdbcUtils.getColumnValue(resultSet, "message", String.class));
        return text;
    }
}
