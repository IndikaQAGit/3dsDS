package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.HsmDevice;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ResultSetMapper for HsmDevice entity
 */
public class HsmDeviceResultSetMapper implements ResultSetMapper<HsmDevice> {
    @Override
    public HsmDevice map(ResultSet resultSet) throws SQLException {
        return HsmDevice
                .builder()
                .id(JdbcUtils.getColumnValue(resultSet, "id", Long.class))
                .className(JdbcUtils.getColumnValue(resultSet, "className", String.class))
                .name(JdbcUtils.getColumnValue(resultSet, "name", String.class))
                .status(JdbcUtils.getColumnValue(resultSet, "status", String.class))
                .build();
    }
}
