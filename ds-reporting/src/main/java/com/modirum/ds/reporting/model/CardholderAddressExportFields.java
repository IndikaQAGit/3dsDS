package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Encapsulates error columns needed in CSV Export of transactions.
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CardholderAddressExportFields {
    private String addrCity;
    private String addrCountry;
    private String addrLine1;
    private String addrLine2;
    private String addrLine3;
    private String addrPostCode;
    private String addrState;
}