package com.modirum.ds.reporting.reports;

import java.io.Writer;

/**
 * Main interface for DS Reports
 */
public interface DsReport {
    /**
     * This primarily executes the actual report content generation normally
     * by querying against the database using the reportParameters.
     *
     * @param writer           The implementation where the content is to be written.
     * @param reportParameters Contains the needed parameters to execute report query and content generation.
     */
    void writeReport(Writer writer, ReportParameters reportParameters) throws Exception;
}
