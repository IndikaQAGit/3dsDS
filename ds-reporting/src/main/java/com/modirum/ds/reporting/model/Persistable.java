/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 24.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.reporting.model;

import java.io.Serializable;

public interface Persistable {

    Serializable getId();

    int hashCode();

    boolean equals(Object o);
}
