package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.reporting.model.AuditLog;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * ResultSetMapper for AuditLog entity
 */
public class AuditLogResultSetMapper implements ResultSetMapper<AuditLog> {
    @Override
    public AuditLog map(ResultSet resultSet) throws SQLException {
        return AuditLog
                .builder()
                .id(JdbcUtils.getColumnValue(resultSet, "id", Long.class))
                .action(JdbcUtils.getColumnValue(resultSet, "action", String.class))
                .by(JdbcUtils.getColumnValue(resultSet, "byuser", String.class))
                .byId(JdbcUtils.getColumnValue(resultSet, "byuserId", Long.class))
                .details(JdbcUtils.getColumnValue(resultSet, "details", String.class))
                .objectClass(JdbcUtils.getColumnValue(resultSet, "objectClass", String.class))
                .objectId(JdbcUtils.getColumnValue(resultSet, "objectId", Long.class))
                .when(JdbcUtils.getColumnValue(resultSet, "whendate", Timestamp.class))
                .build();
    }
}
