package com.modirum.ds.reporting.reports;

import com.modirum.ds.reporting.enums.AuditLogAction;
import com.modirum.ds.reporting.enums.DateTimeFormat;
import com.modirum.ds.reporting.enums.Roles;
import com.modirum.ds.reporting.enums.UserStatus;
import com.modirum.ds.reporting.model.User;
import com.modirum.ds.reporting.model.rowmapper.UserResultSetMapper;
import com.modirum.ds.reporting.services.CsvFilteringService;
import com.modirum.ds.reporting.services.ServiceLocator;
import com.modirum.ds.reporting.util.JdbcUtils;
import com.modirum.ds.reporting.util.ReportUtil;
import com.modirum.ds.utils.DateUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

/**
 * Report generator to audit changes on DS users.
 */
public class UserAuditReport implements DsReport {
    private final static Logger log = LoggerFactory.getLogger(UserAuditReport.class);
    public static final String PAYMENT_SYSTEM_ID_KEY = "paymentSystemId";
    private final CsvFilteringService csvFilteringService = ServiceLocator.getInstance().getCsvFilteringService();
    final static String[] HEADERS = new String[]{
            "USERID",
            "FIRSTNAME",
            "LASTNAME",
            "EMAIL",
            "VIEWUSERS",
            "EDITUSERS",
            "VIEWISSR",
            "EDITISSR",
            "VIEWACQR",
            "EDITACQR",
            "VIEWREC",
            "VIEWTXT",
            "EDITTXT",
            "ADMINSETUP",
            "VIEWCERT",
            "EDITCERT",
            "KEYMANAGE",
            "REPORTS",
            "STATUS",
            "STATUS LAST UPDATE",
            "CREATED",
            "LASTLOGIN"
    };

    @Override
    public void writeReport(Writer writer, ReportParameters reportParameters) throws Exception {
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                .withRecordSeparator(System.lineSeparator())
                .withHeader(HEADERS));
        log.debug("UserAuditReport report query starting...");

        Properties criteriaProperties = reportParameters.getCriteriaProperties();
        Object[] queryParameters = ArrayUtils.EMPTY_OBJECT_ARRAY;
        if (StringUtils.isNotEmpty(criteriaProperties.getProperty(PAYMENT_SYSTEM_ID_KEY))) {
            Long paymentSystemId = ReportUtil.convertToLong(PAYMENT_SYSTEM_ID_KEY, criteriaProperties.getProperty(PAYMENT_SYSTEM_ID_KEY));
            queryParameters = new Object[]{paymentSystemId};
        }
        try (Connection connection = reportParameters.getReportingDataSource().getConnection();
             PreparedStatement ps = buildSqlQuery(connection, queryParameters);
             ResultSet resultSet = ps.executeQuery()) {

            UserResultSetMapper userResultSetMapper = new UserResultSetMapper();
            while (resultSet.next()) {
                User user = userResultSetMapper.map(resultSet);
                log.debug("UserAuditReport processing record for username={}", user.getLoginName());

                Set<String> roles = new HashSet<>();
                String userRoleSql = "SELECT role FROM ds_userroles WHERE user_id=?";
                try (Connection userRoleConnection = reportParameters.getReportingDataSource().getConnection();
                     PreparedStatement userRolePreparedStatement =
                             JdbcUtils.getPreparedStatement(userRoleConnection, userRoleSql, new Object[]{user.getId()});
                     ResultSet userRoleResultSet = userRolePreparedStatement.executeQuery()) {
                    while (userRoleResultSet.next()) {
                        roles.add(userRoleResultSet.getString("role"));
                    }
                }

                UserStatus userStatus = UserStatus.fromValue(user.getStatus());
                String statusLabel = userStatus != null ? userStatus.getText() : null;

                String statusLastUpdatedSql = "SELECT whendate FROM ds_auditlog" +
                                              " WHERE ((action=? and details like ?) or (action=? and details like ?)) and objectClass=?" +
                                              " ORDER BY whendate desc";
                Object[] statusUpdateSqlParamValues = {AuditLogAction.LOGINFAIL,
                                                       "Login fail user " + user.getLoginName() + "%",
                                                       AuditLogAction.UPDATE, user.getLoginName() + " status:%",
                                                       User.class.getSimpleName()};
                Timestamp statusLastUpdatedDate = JdbcUtils.findFirst(reportParameters.getReportingDataSource(), statusLastUpdatedSql, statusUpdateSqlParamValues,
                                                                      auditLogResultSet -> auditLogResultSet.getTimestamp("whendate"));

                String userCreatedDateSql = "SELECT whendate FROM ds_auditlog where action=? and objectClass=? and details like ? ORDER BY whendate desc";
                Object[] userCreatedDateSqlParamValues = {AuditLogAction.INSERT, User.class.getSimpleName(),
                                                          user.getLoginName() + " created%"};
                Timestamp userCreatedDate = JdbcUtils.findFirst(reportParameters.getReportingDataSource(), userCreatedDateSql, userCreatedDateSqlParamValues,
                                                                auditLogResultSet -> auditLogResultSet.getTimestamp("whendate"));

                String lastLoginDateSql = "SELECT whendate FROM ds_auditlog where byuserId=? and action=? and objectClass=? ORDER BY whendate desc";
                Object[] lastLoginDateSqlParamValues = {user.getId(), AuditLogAction.LOGIN, User.class.getSimpleName()};
                Timestamp lastLoginDate = JdbcUtils.findFirst(reportParameters.getReportingDataSource(), lastLoginDateSql, lastLoginDateSqlParamValues,
                                                              auditLogResultSet -> auditLogResultSet.getTimestamp("whendate"));

                TimeZone timeZone = reportParameters.getTimeZone();

                Object[] values = {
                        csvFilteringService.replaceProhibitedCharsWithSpace(user.getLoginName()),
                        csvFilteringService.replaceProhibitedCharsWithSpace(user.getFirstName()),
                        csvFilteringService.replaceProhibitedCharsWithSpace(user.getLastName()),
                        csvFilteringService.replaceProhibitedCharsWithSpace(user.getEmail()),
                        roles.contains(Roles.userView) ? "YES" : "NO",
                        roles.contains(Roles.userEdit) ? "YES" : "NO",
                        roles.contains(Roles.issuerView) ? "YES" : "NO",
                        roles.contains(Roles.issuerEdit) ? "YES" : "NO",
                        roles.contains(Roles.acquirerView) ? "YES" : "NO",
                        roles.contains(Roles.acquirerEdit) ? "YES" : "NO",
                        roles.contains(Roles.recordsView) ? "YES" : "NO",
                        roles.contains(Roles.textView) ? "YES" : "NO",
                        roles.contains(Roles.textEdit) ? "YES" : "NO",
                        roles.contains(Roles.adminSetup) ? "YES" : "NO",
                        roles.contains(Roles.caView) ? "YES" : "NO",
                        roles.contains(Roles.caEdit) ? "YES" : "NO",
                        roles.contains(Roles.keyManage) ? "YES" : "NO",
                        roles.contains(Roles.reports) ? "YES" : "NO",
                        statusLabel,
                        DateUtil.formatDate(statusLastUpdatedDate, DateTimeFormat.DATE_FORMAT, timeZone),
                        DateUtil.formatDate(userCreatedDate, DateTimeFormat.DATE_FORMAT, timeZone),
                        DateUtil.formatDate(lastLoginDate, DateTimeFormat.DATETIME_FORMAT, timeZone)
                };
                log.debug("UserAuditReport writing a record detail for username={}", user.getLoginName());
                csvPrinter.printRecord(csvFilteringService.nullSafeAndSanitizeArrayValues(values));
            }
        }
        csvPrinter.flush();
    }

    private PreparedStatement buildSqlQuery(Connection connection, Object[] queryParameters) throws SQLException {
        String sql = "SELECT id,loginname,firstName,lastName,email,status" +
                     " FROM ds_users";
        if (queryParameters.length == 1) {
            sql += " WHERE paymentSystemId=?";
        }
        return JdbcUtils.getPreparedStatement(connection, sql, queryParameters);
    }
}
