package com.modirum.ds.reporting;

import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.enums.ReportStatus;
import com.modirum.ds.json.JsonUtil;
import com.modirum.ds.reporting.enums.AuditLogAction;
import com.modirum.ds.reporting.enums.ReportConfigKeys;
import com.modirum.ds.reporting.model.User;
import com.modirum.ds.reporting.reports.DsReport;
import com.modirum.ds.reporting.reports.ReportParameters;
import com.modirum.ds.reporting.reports.TransactionReport;
import com.modirum.ds.reporting.services.AuditLogJdbcService;
import com.modirum.ds.reporting.util.JdbcUtils;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import lombok.AllArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.json.JsonObject;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

@AllArgsConstructor
public class ReportFactoryImpl implements ReportFactory, Runnable {

    private Logger logger;
    private DataSource reportingDS;
    private AuditLogJdbcService auditLogJdbcService;
    private Integer deleteOldReportsDays;
    private String reportBaseDir;

    private final static String REPORT_USER = "report";

    private String getFormattedDateFilename(String date) {
        date = StringUtils.remove(date, " ");
        date = StringUtils.remove(date, "-");
        return StringUtils.substring(date, 0, 8);
    }

    private String getReportFilename(Properties reportQueryProps) {
        StringBuilder filename = new StringBuilder();
        filename.append(reportQueryProps.getProperty(ReportConfigKeys.REPORT_CLASS_NAME)).append("_");

        String paymentSystemId = reportQueryProps.getProperty(TransactionReport.PAYMENT_SYSTEM_ID_KEY);
        if (Misc.isNullOrEmpty(paymentSystemId) || PaymentSystem.UNLIMITED.toString().equals(paymentSystemId)) {
            filename.append("All_");
        } else {
            filename.append(paymentSystemId).append("_");
        }

        String startDate = reportQueryProps.getProperty(TransactionReport.START_DATE_KEY);
        String endDate = reportQueryProps.getProperty(TransactionReport.END_DATE_KEY);
        if (Misc.isNotNullOrEmpty(startDate) && Misc.isNotNullOrEmpty(endDate)) {
            filename.append(getFormattedDateFilename(startDate))
                    .append("-")
                    .append(getFormattedDateFilename(endDate));
        }
        else {
            filename.append(DateUtil.formatDate(new Date(), "yyyyMMddHH"));
        }

        filename.append(".csv");
        return filename.toString();
    }

    public void processQueuedReports() {
        String query = "SELECT id, paymentSystemId, name, dateFrom, dateTo, criteria"
                + " FROM ds_reports WHERE status = ? ORDER BY createdDate LIMIT 1";

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = reportingDS.getConnection();
            ps = con.prepareStatement(query);

            ps.setString(1, ReportStatus.QUEUED.getLabel());

            rs = ps.executeQuery();
            while (rs.next()) {
                String reportName = rs.getString(4);
                Long id = rs.getLong(1);

                // Mark report as processed immediately to avoid crossprocessing
                boolean processedByAnotherThread = !updateReportStatus(id, ReportStatus.PROCESSING, ReportStatus.QUEUED);
                if (processedByAnotherThread) {
                    continue;
                }

                try {
                    ReportParameters reportParameters = new ReportParameters();
                    reportParameters.setReportingDataSource(reportingDS);
                    JsonObject reportCriteria = JsonUtil.stringToJsonObject(rs.getString(6));

                    Locale reportLocale = Locale.ENGLISH;
                    TimeZone reportTimezone = TimeZone.getTimeZone("UTC");
                    Set<String> criteriaKeys = reportCriteria.keySet();

                    if (criteriaKeys.contains(ReportConfigKeys.LOCALE)) {
                        String locale = reportCriteria.getString(ReportConfigKeys.LOCALE);
                        if (StringUtils.isNotEmpty(locale)) {
                            reportLocale = Locale.forLanguageTag(locale);
                        }
                    }

                    if (criteriaKeys.contains(ReportConfigKeys.TIMEZONE)) {
                        String timezone = reportCriteria.getString(ReportConfigKeys.TIMEZONE);
                        if (StringUtils.isNotEmpty(timezone)) {
                            reportTimezone = TimeZone.getTimeZone(timezone);
                        }
                    }

                    reportParameters.setLocale(reportLocale);
                    reportParameters.setTimeZone(reportTimezone);

                    Properties reportQueryProps = new Properties();
                    reportQueryProps.put(ReportConfigKeys.REPORT_CLASS_NAME, rs.getString(3));
                    reportQueryProps.put(ReportConfigKeys.REPORT_ID, rs.getString(1));

                    String startDate = rs.getString(4);
                    if (Misc.isNotNullOrEmpty(startDate)) {
                        reportQueryProps.put(TransactionReport.START_DATE_KEY, startDate);
                    }

                    String endDate = rs.getString(5);
                    if (Misc.isNotNullOrEmpty(endDate)) {
                        reportQueryProps.put(TransactionReport.END_DATE_KEY, endDate);
                    }

                    String paymentSystemId = rs.getString(2);
                    if (!PaymentSystem.UNLIMITED.toString().equals(paymentSystemId)) {
                        reportQueryProps.put(TransactionReport.PAYMENT_SYSTEM_ID_KEY, paymentSystemId);
                    }

                    reportQueryProps.put(ReportConfigKeys.REPORT_FILE_NAME, getReportFilename(reportQueryProps));
                    reportParameters.setCriteriaProperties(reportQueryProps);

                    String reportClassName = reportQueryProps.getProperty(ReportConfigKeys.REPORT_CLASS_NAME);
                    Class<? extends DsReport> clazz = (Class<? extends DsReport>) Class.forName("com.modirum.ds.reporting.reports." + reportClassName);
                    logger.debug("DsReport class={} will be instantiated.", clazz);
                    DsReport dsReport = clazz.newInstance();

                    String reportFileName = reportQueryProps.getProperty(ReportConfigKeys.REPORT_FILE_NAME);
                    String reportId = reportQueryProps.getProperty(ReportConfigKeys.REPORT_ID);
                    String hashedSubFolder = DigestUtils.md5Hex(reportId + System.currentTimeMillis() + reportFileName);
                    File dir = new File(reportBaseDir + "/" + hashedSubFolder);
                    logger.debug("Creating report subdirectory[{}]...", dir.toURI().getPath());
                    if (dir.exists()) {
                        updateReportStatus(id, ReportStatus.ERROR);
                        String existingReportDirErrorMessage =
                                MessageFormat.format("Report directory[{0}] for the given report.id[{1}] and reportFileName[{2}] is already existing!",
                                        dir.toURI().getPath(),
                                        reportId,
                                        reportFileName);
                        throw new IllegalArgumentException(existingReportDirErrorMessage);
                    }
                    if (dir.mkdir()) {
                        logger.debug("Successfully created report subdirectory.");
                    }
                    File reportFile = new File(reportBaseDir + "/" + hashedSubFolder + "/" + reportFileName);

                    logger.debug("{}'s report generation is starting...", reportClassName);
                    try (OutputStreamWriter fileOutputStream = new OutputStreamWriter(new FileOutputStream(reportFile), StandardCharsets.UTF_8);
                         PrintWriter printWriter = new PrintWriter(fileOutputStream)) {

                        long startTime = System.currentTimeMillis();
                        logger.debug("{}.writeReport started to execute", getClass().getSimpleName());

                        dsReport.writeReport(printWriter, reportParameters);

                        logger.debug("{}.writeReport is returning after {} second(s) ",
                                dsReport.getClass().getSimpleName(),
                                (System.currentTimeMillis() - startTime) / 1000);

                        completeReport(id, reportFileName, hashedSubFolder);

                        auditLogJdbcService.logAction(new User(0L, REPORT_USER),
                                null,
                                AuditLogAction.CREATE_REPORT,
                                MessageFormat.format("ReportClassName[{0}] has been generated with the reportFile={1}", reportClassName, reportFile.toURI().getPath()));
                        logger.info("/{}/{}", hashedSubFolder, reportFileName);
                    } catch (Exception e) {
                        logger.error("Report generation failed!");
                        logger.error("Cleaning up...");
                        FileUtils.deleteQuietly(reportFile);
                        FileUtils.deleteQuietly(dir);
                        logger.error("Failed report clean up done!");
                        throw e;
                    }
                    logger.debug("Completed report generation!");

                } catch (Exception e) {
                    logger.error("Caught exception while processing queued report", e);
                    updateReportStatus(id, ReportStatus.ERROR);
                }
            }
        } catch (Exception e) {
            logger.error("Caught exception while processing queued reports", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
            DbUtils.closeQuietly(con);
        }

    }

    public boolean updateReportStatus(Long id, ReportStatus newStatus) {
        return updateReportStatus(id, newStatus, null);
    }

    public boolean updateReportStatus(Long id, ReportStatus newStatus, ReportStatus currentStatus) {
        StringBuilder query = new StringBuilder("UPDATE ds_reports SET status = ?, lastModifiedBy = ?, lastModifiedDate = ? WHERE id = ?");

        Object[] params = new Object[] {
            newStatus.getLabel(),
            REPORT_USER,
            new Timestamp(System.currentTimeMillis()),
            id
        };

        if (currentStatus != null) {
            query.append(" AND status = ?");
            params = ArrayUtils.add(params, currentStatus.getLabel());
        }

        try (Connection conn = JdbcUtils.startTransaction(reportingDS)) {
            boolean success = JdbcUtils.executeUpdate(conn, query.toString(), params) > 0;
            JdbcUtils.txCommitAndClose(conn);
            return success;
        } catch (Exception e) {
            logger.error("Could not update report", e);
        }
        return false;
    }

    public void completeReport(Long id, String filename, String secret) {
        String query = "UPDATE ds_reports SET status = ?, filename = ?, secret = ?,"
                + " lastModifiedBy = ?, lastModifiedDate = ? WHERE id = ?";

        Object[] params = new Object[] {
                ReportStatus.COMPLETED.getLabel(),
                filename,
                secret,
                REPORT_USER,
                new Timestamp(System.currentTimeMillis()),
                id
        };

        try (Connection conn = JdbcUtils.startTransaction(reportingDS)) {
            JdbcUtils.execute(conn, query, params);
            JdbcUtils.txCommitAndClose(conn);
        } catch (Exception e) {
            logger.error("Could not complete report", e);
        }

    }

    public void markOldReportsAsDeleted() {
        if (deleteOldReportsDays != null) {
            logger.debug("Purging reports older than {} days", deleteOldReportsDays);

            try (Connection conn = JdbcUtils.startTransaction(reportingDS)) {

                boolean isH2 = conn.getMetaData().getDriverName().toLowerCase().contains("h2");
                String query = isH2 ? "UPDATE ds_reports SET status = ?, lastModifiedBy = ?, lastModifiedDate = ? WHERE createdDate < TIMESTAMPADD(DAY, ?, CURRENT_DATE)"
                        : "UPDATE ds_reports SET status = ?, lastModifiedBy = ?, lastModifiedDate = ?  WHERE createdDate < DATE_SUB(NOW(), INTERVAL ? DAY)";

                Object[] params = new Object[] {
                        ReportStatus.DELETED.getLabel(),
                        REPORT_USER,
                        new Timestamp(System.currentTimeMillis()),
                        isH2 ? -deleteOldReportsDays : deleteOldReportsDays
                };

                JdbcUtils.execute(conn, query, params);
                JdbcUtils.txCommitAndClose(conn);
            } catch (Exception e) {
                logger.error("Could not mark old reports as deleted", e);
            }
        }
    }

    @Override
    public void run() {
        try {
            long startTime = System.currentTimeMillis();
            this.processQueuedReports();
            this.markOldReportsAsDeleted();
            long elapsedTime = System.currentTimeMillis() - startTime;
            logger.debug("Elapsed time: {}", elapsedTime);
        } catch (Throwable e) {
            logger.error("Error while processing reports", e);
            throw e;
        }
    }

}