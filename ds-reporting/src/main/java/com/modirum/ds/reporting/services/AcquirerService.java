package com.modirum.ds.reporting.services;

import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.reporting.model.rowmapper.AcquirerResultSetMapper;
import com.modirum.ds.reporting.util.JdbcUtils;

import javax.sql.DataSource;

public class AcquirerService {

    private final DataSource dataSource;

    AcquirerService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Acquirer getAcquirerByBINAndPaymentSystem(String bin, Integer paymentSystemId) throws Exception {
        String sql = "SELECT id, paymentSystemId, BIN, name, phone, email, address, city, country, status, isid " +
            "FROM ds_acquirers " +
            "WHERE BIN = ? AND paymentSystemId = ?";
        return JdbcUtils.query(dataSource, sql, new Object[]{bin, paymentSystemId}, new AcquirerResultSetMapper())
            .stream().findFirst().orElse(null);
    }
}