package com.modirum.ds.reporting.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Utilities for Properties Object
 */
public final class PropertiesUtil {
    private PropertiesUtil() {
    }

    /**
     * Load properties from a given property file.
     *
     * @param configFile file path of the property file.
     * @return
     * @throws IOException
     */
    public static Properties loadFromFile(String configFile) throws IOException {
        Properties properties = new Properties();
        try (FileInputStream fis = new FileInputStream(configFile)) {
            properties.load(fis);
        }
        return properties;
    }

}
