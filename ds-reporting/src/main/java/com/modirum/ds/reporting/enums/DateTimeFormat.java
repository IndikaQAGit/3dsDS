package com.modirum.ds.reporting.enums;

/**
 * Standard date and time format used in DS.
 */
public final class DateTimeFormat {
    private DateTimeFormat() {
    }
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String TIME_FORMAT = "HH:mm:ss";
}
