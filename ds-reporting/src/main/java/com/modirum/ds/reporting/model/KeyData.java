/*
 * Copyright (C) 2014 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 27.01.2014
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KeyData extends PersistableClass implements Persistable {
    private Long id;
    private String keyAlias;
    private String keyData;
    private Long hsmDeviceId;
    private Long wrapkeyId;
    private String keyCheckValue;
    private Date keyDate;
    private Integer cryptoPeriodDays;
    private String status;
    private String signedBy1;
    private String signature1;
    private String signedBy2;
    private String signature2;
}
