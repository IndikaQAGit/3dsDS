package com.modirum.ds.reporting.model;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentSystemConfig implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Long paymentSystemId;
    private Integer dsId;
    private String key;
    private String value;
    private String comment;
    private Date createdDate;
    private String createdUser;
    private Date lastModifiedDate;
    private String lastModifiedBy;

}
