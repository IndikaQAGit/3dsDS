package com.modirum.ds.services.elo.papi;

import com.modirum.ds.services.elo.auth.EloAuthenticationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import java.io.Console;
import java.util.regex.Pattern;

/**
 * Cmd tool for Elo PAPI Services. Currently this tool provides interface in creating user
 * against ELO API and if successful, stores the credentials in the database. The stored
 * credentials will be used by the DS Server for authentication during Detokenization flow.
 */
@Service
public class EloPapiTools {
    private final static Logger log = LoggerFactory.getLogger(EloPapiTools.class);
    static {
        //set the default logback.xml location if not specified
        if (StringUtils.isEmpty(System.getProperty("logback.configurationFile"))) {
            System.setProperty("logback.configurationFile", "conf/logback.xml");
        }
    }

    @Autowired
    private EloAuthenticationService eloAuthenticationService;

    public static void main(String[] args) {
        log.debug("Initializing ApplicationContext...");
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:application-context.xml");
        log.debug("Initializing ApplicationContext complete!");
        context.getBean(EloPapiTools.class).start();
    }

    /**
     * Starts the interactive cmd line tool
     */
    public void start() {
        Console console = System.console();
        System.out.println("Starting interactive cmd tool for Elo Papi...");
        while (true) {
            System.out.println("Creating an Elo API user...");
            Integer eloPsId = readInputForEloPsId(console);
            String username = console.readLine("Please enter Elo username: ");
            String password = String.valueOf(console.readPassword("Please enter Elo password: "));
            String cpfId = readInputForCpfId(console);

            System.out.println("Attempting now to create the Elo API user...");

            Boolean success = eloAuthenticationService.createUser(eloPsId, username, password, cpfId);

            if (success) {
                System.out.println("Successfully created the Elo API user!");
                System.out.println("Closing the application.");
                break;
            }

            System.out.println("Failed in creating the Elo API user!");
            String yOrN =  console.readLine("Do you want to try again (Y)?");
            if (!"y".equalsIgnoreCase(yOrN.trim())) {
                break;
            }
        }
    }

    /**
     * Accepts and validate input for ELO PaymentSystemId
     *
     * @param console
     * @return
     */
    private int readInputForEloPsId(Console console) {
        while (true) {
            String eloPsId = console.readLine("Please enter Elo paymentSystemId: ");
            if (!NumberUtils.isDigits(eloPsId)) {
                System.out.println("PaymentSystemId should be a number!");
                continue;
            }
            return Integer.parseInt(eloPsId);
        }
    }

    /**
     * Accepts and validate CPF id
     * @param console
     * @return
     */
    private String readInputForCpfId(Console console) {
        while (true) {
            String cpfId = console.readLine("Please enter Elo CPF id (###.###.###-##): ");
            Pattern cpfFormat = Pattern.compile("\\d{3}\\.\\d{3}.\\d{3}\\-\\d{2}");
            if (!cpfFormat.matcher(cpfId).matches()) {
                System.out.println("Invalid cpf format. It should follow this format '###.###.###-##' where # is a number");
                continue;
            }
            return cpfId;
        }
    }
}
