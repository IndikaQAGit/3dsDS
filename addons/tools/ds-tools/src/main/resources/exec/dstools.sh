#!/bin/sh
# Note you need to run this application with java that has 
# installed the Java Cryptography Extension (JCE) Unlimited Strength
# jurisdiction Policy Files (local_policy.jar)
# for example this java has it..
java="java -Xmx512M  -Ddstools.home=.."
# -Djava.security.manager -Djava.security.policy=../conf/dstools.policy
umask 077
LIBS="../conf"
LIBS="$LIBS:../lib/*"
$java -cp $LIBS com.modirum.ds.tools.KeysInstaller $*