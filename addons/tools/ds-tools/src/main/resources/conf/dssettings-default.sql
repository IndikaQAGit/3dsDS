
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('acceptedMIList', 'Poster form', 'csv list', '2016-03-24 11:35:38', 'admin2 (1100)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('acceptedSDKList', 'SDKEmu V1', 'csv list', '2016-03-24 11:35:26', 'admin2 (1100)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('acs.ssl.client.certId', '15', 'Default ACS SSL auth keypair/certificate id', NULL, NULL);
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('cardType.01.visa', '1', '', '2016-03-16 12:49:46', 'admin (1000)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('cardType.02.mastercard', '2', '', '2016-03-16 12:49:57', 'admin (1000)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('cardType.03.amex', '3', '', '2016-03-16 12:50:27', 'admin (1000)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('cardType.04.jcb', '4', '', '2016-03-24 11:40:47', 'admin2 (1100)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('cardType.05.discover', '5', '', '2016-03-16 12:50:27', 'admin (1000)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('cardType.06.diners', '6', '', '2016-03-16 12:50:27', 'admin (1000)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('cardType.07.bmc', '7', '', '2016-03-16 12:50:27', 'admin (1000)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('cardType.08.nspkmir', '8', '', '2016-03-16 12:50:27', 'admin (1000)');


-- INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('mi.ssl.client.certId', '15', 'Default MI SSL auth keypair/certificate id', NULL, NULL);
-- INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('proxyType', 'pound', NULL, NULL, NULL);
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('reset.settings.cache', 'false', '', '2016-03-24 10:54:09', 'admin (1000)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('scheme.name', 'Scheme Inc.', NULL, NULL, NULL);
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('scheme.website', 'https://schemeinc.com', NULL, NULL, NULL);
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('settingsAccessList', '10.2.5.111', 'Space seprated list of ip adresses eligible for settings edit', NULL, NULL);
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('stage2authentication.checksdnbinmatch', 'true', NULL, NULL, NULL);
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('supportedLocales', 'en_GB,en_US,en_IE,et_EE', NULL, NULL, NULL);
-- INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('viaProxyEnabled', 'true', NULL, NULL, NULL);

