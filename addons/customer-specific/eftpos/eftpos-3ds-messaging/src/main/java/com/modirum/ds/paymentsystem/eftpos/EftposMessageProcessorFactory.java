package com.modirum.ds.paymentsystem.eftpos;

import com.modirum.ds.messageprocessor.EmvMessageProcessorFactory;
import com.modirum.ds.messageprocessor.PaymentSystemMessageProcessor;
import com.modirum.ds.paymentsystem.eftpos.messageprocessor.EftposMessageProcessor;

/**
 * Eftpos processor builder.
 */
public class EftposMessageProcessorFactory implements EmvMessageProcessorFactory {

    @Override
    public PaymentSystemMessageProcessor create() {
        return new EftposMessageProcessor();
    }
}
