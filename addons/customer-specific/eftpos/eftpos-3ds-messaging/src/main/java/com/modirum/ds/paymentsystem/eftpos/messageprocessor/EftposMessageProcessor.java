package com.modirum.ds.paymentsystem.eftpos.messageprocessor;

import com.modirum.ds.enums.FieldName;
import com.modirum.ds.messageprocessor.PaymentSystemMessageProcessor;
import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.XMessageCategory;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.model.threeds.MessageExtension;
import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.services.JsonMessageService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.util.ErrorUtils;
import com.modirum.ds.util.ObjectUtils;
import com.modirum.ds.utils.Misc;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Custom 3DS message processing of Eftpos PaymentSystems.
 */
public class EftposMessageProcessor implements PaymentSystemMessageProcessor {

    private static final Logger log = LoggerFactory.getLogger(EftposMessageProcessor.class);
    public static final String DS_AREQ_MSG_EXTENSION_ID = "A000000384*DSID";
    public static final String DIRECTORY_SERVER_ID_MESSAGE_EXT_VERSION = "1.0";
    private final MessageExtension directoryServerMessageExtensionForAReq = new MessageExtension()
            .setCriticalityIndicator(Boolean.FALSE)
            .setId(DS_AREQ_MSG_EXTENSION_ID)
            .setName("Directory Server ID")
            .addData("directoryServerID", "A000000384")
            .addData("version", DIRECTORY_SERVER_ID_MESSAGE_EXT_VERSION);

    private static final String SUPPORTED_PURCHASE_CURRENCY = "036";

    //transStatus values that makes field `eci` required in ARes and RReq
    private static final List<String> TRANS_STATUS_VALUES_FOR_REQUIRED_ECI = Arrays.asList("Y", "N", "U", "A", "R");

    private final JsonMessageService jsonMessageService;

    public EftposMessageProcessor() {
        this.jsonMessageService = ServiceLocator.getInstance().getJsonMessageService();
    }

    @Override
    public List<Error> validateAResIncoming(JsonMessage aResJsonMessage, JsonMessage aReqJsonMessage) {
        log.info("EftposMessageProcessor.validateAResIncoming is called.");

        List<Error> errors = new ArrayList<>();

        String messageCategory = aReqJsonMessage.getMessageCategory();
        validateIncomingTransStatusReason(aResJsonMessage, errors, messageCategory);
        validateIncomingEci(aResJsonMessage, errors, messageCategory);
        validateAcsOperatorID(aResJsonMessage, errors);

        boolean isNpa = XMessageCategory.C02.isEqual(aReqJsonMessage.getMessageCategory());
        boolean isBrowserDeviceChannel = TDSModel.DeviceChannel.cChannelBrow02.value().equals(aReqJsonMessage.getDeviceChannel());
        boolean isAppDeviceChannel = TDSModel.DeviceChannel.cChannelApp01.value().equals(aReqJsonMessage.getDeviceChannel());
        boolean is3RIDeviceChannel = TDSModel.DeviceChannel.cChannel3RI03.value().equals(aReqJsonMessage.getDeviceChannel());

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isNpa &&
            StringUtils.isEmpty(aResJsonMessage.getTransStatus())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.transStatus));
        }

        return errors;
    }

    private void validateAcsOperatorID(JsonMessage aResJsonMessage, List<Error> errors) {
        if (Misc.isNullOrEmpty(aResJsonMessage.getAcsOperatorID())) {
            errors.add(new Error(TDSModel.ErrorCode.cRequiredFieldMissing201, "acsOperatorID", "is missing"));
        }
    }

    @Override
    public List<Error> validatePReq(JsonMessage pReqJsonMessage) {
        log.info("EftposMessageProcessor.validatePReq is called.");
        List<Error> errors = new ArrayList<>();

        validateTdsServerOperatorId(pReqJsonMessage, errors);

        return errors;
    }

    @Override
    public List<Error> validateAReqIncoming(JsonMessage aReqJsonMessage) {
        final List<Error> errors = new ArrayList<>();

        validatePurchaseCurrency(aReqJsonMessage, errors);
        validateMessageExtensions(aReqJsonMessage, errors);

        boolean npa = XMessageCategory.C02.isEqual(aReqJsonMessage.getMessageCategory());
        boolean pa = XMessageCategory.C01.isEqual(aReqJsonMessage.getMessageCategory());
        boolean isBrowser = TDSModel.DeviceChannel.cChannelBrow02.value().equals(aReqJsonMessage.getDeviceChannel());
        boolean isAppDeviceChannel = TDSModel.DeviceChannel.cChannelApp01.value().equals(aReqJsonMessage.getDeviceChannel());
        boolean is3RIDeviceChannel = TDSModel.DeviceChannel.cChannel3RI03.value().equals(aReqJsonMessage.getDeviceChannel());

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) &&
            (pa || npa)) {
            validateTdsServerOperatorId(aReqJsonMessage, errors);
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) &&
            (pa || npa) &&
            StringUtils.isEmpty(aReqJsonMessage.getCardExpiryDate())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.cardExpiryDate));
        }

        return errors;
    }

    /**
     * As required by Eftpos Secure Authentication Messaging Specification
     * for validating transStatusReason in ARes and RReq messages.
     *
     * @param jsonMessage  ARes or RReq JsonMessage
     */
    private void validateIncomingTransStatusReason(JsonMessage jsonMessage, List<Error> errors, String messageCategory) {
        boolean npa = XMessageCategory.C02.isEqual(messageCategory);
        String transStatusReason = jsonMessage.getTransStatusReason();
        String transStatus = jsonMessage.getTransStatus();
        if (npa && ObjectUtils.isIn(transStatus, "N", "U", "R") && StringUtils.isEmpty(transStatusReason)) {
            errors.add(new Error(TDSModel.ErrorCode.cRequiredFieldMissing201, "transStatusReason", "is missing"));
        } else if (jsonMessageService.isFieldPresent(jsonMessage, FieldName.transStatusReason) && StringUtils.isEmpty(transStatusReason)) {
            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "transStatusReason", "invalid value"));
        }
    }

    /**
     * As required by Eftpos Secure Authentication Messaging Specification
     * for validating eci in ARes and RReq messages.
     *
     * @param jsonMessage ARes or RReq JsonMessage
     */
    private void validateIncomingEci(JsonMessage jsonMessage, List<Error> errors, String messageCategory) {
        boolean npa = XMessageCategory.C02.isEqual(messageCategory);
        boolean pa = XMessageCategory.C01.isEqual(messageCategory);
        String transStatus = jsonMessage.getTransStatus();
        String eci = jsonMessage.getEci();

        if ((pa || npa) && TRANS_STATUS_VALUES_FOR_REQUIRED_ECI.contains(transStatus)) {
            if (StringUtils.isEmpty(eci)) {
                errors.add(ErrorUtils.fieldMissingError(FieldName.eci));
            } else if ("Y".equals(transStatus) && !"05".equals(eci)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "eci", "invalid value"));
            } else if (("N".equals(transStatus) || "U".equals(transStatus) || "R".equals(transStatus)) &&
                       !"07".equals(eci)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "eci", "invalid value"));
            } else if ("A".equals(transStatus) && !"06".equals(eci)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "eci", "invalid value"));
            }
        } else if (StringUtils.isNotEmpty(eci) && ObjectUtils.isNotIn(eci, "05", "06", "07")) {
            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "eci", "invalid value"));
        }
    }

    @Override
    public List<Error> validateRReqIncoming(JsonMessage rReqJsonMessage, TDSRecord record) {
        log.info("EftposMessageProcessor.validateRReqIncoming is called.");

        List<Error> errors = new ArrayList<>();

        String messageCategory = rReqJsonMessage.getMessageCategory();
        validateIncomingTransStatusReason(rReqJsonMessage, errors, messageCategory);
        validateIncomingEci(rReqJsonMessage, errors, messageCategory);

        boolean isNpa = XMessageCategory.C02.isEqual(record.getMessageCategory());
        boolean isBrowserDeviceChannel = TDSModel.DeviceChannel.cChannelBrow02.value().equals(record.getDeviceChannel());
        boolean isAppDeviceChannel = TDSModel.DeviceChannel.cChannelApp01.value().equals(record.getDeviceChannel());
        boolean is3RIDeviceChannel = TDSModel.DeviceChannel.cChannel3RI03.value().equals(record.getDeviceChannel());

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isNpa &&
            StringUtils.isEmpty(rReqJsonMessage.getTransStatus())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.transStatus));
        }
        return errors;
    }

    @Override
    public JsonMessage processAReqOutgoing(JsonMessage aReqJsonMessage) {
        log.info("EftposMessageProcessor.processAReqOutgoing is called.");
        List<MessageExtension> messageExtensions = Optional.ofNullable(aReqJsonMessage.getMessageExtension())
                                                           .orElse(new ArrayList<>());
        if (CollectionUtils.isNotEmpty(messageExtensions)) {
            Iterator<MessageExtension> iterator = messageExtensions.iterator();
            while (iterator.hasNext()) {
                MessageExtension messageExtension = iterator.next();
                if (directoryServerMessageExtensionForAReq.getId().equals(messageExtension.getId())) {
                    iterator.remove();
                    break;
                }
            }
        }

        if (messageExtensions.size() < 10) {
            messageExtensions.add(directoryServerMessageExtensionForAReq);
        }

        aReqJsonMessage.setMessageExtension(messageExtensions);
        return aReqJsonMessage;
    }

    @Override
    public JsonMessage processAResOutgoing(JsonMessage aResJsonMessage, JsonMessage aReqJsonMessage) {
        log.info("EftposMessageProcessor.processAResOutgoing is called.");
        boolean npa = XMessageCategory.C02.isEqual(aReqJsonMessage.getMessageCategory());

        if (npa) {
            aResJsonMessage.setAuthenticationValue(null);
        }

        boolean pa = XMessageCategory.C01.isEqual(aReqJsonMessage.getMessageCategory());
        String transStatus = aResJsonMessage.getTransStatus();
        if ((pa || npa) && TRANS_STATUS_VALUES_FOR_REQUIRED_ECI.contains(transStatus)) {
            if ("Y".equals(transStatus)) {
                aResJsonMessage.setEci("05");
            } else if ("N".equals(transStatus) || "U".equals(transStatus) || "R".equals(transStatus)) {
                aResJsonMessage.setEci("07");
            } else if ("A".equals(transStatus)) {
                aResJsonMessage.setEci("06");
            }
        } else {
            aResJsonMessage.setEci(null);
        }
        return aResJsonMessage;
    }

    @Override
    public JsonMessage processRReqOutgoing(JsonMessage rReqJsonMessage) {
        log.info("EftposMessageProcessor.processRReqOutgoing is called.");
        boolean npa = XMessageCategory.C02.isEqual(rReqJsonMessage.getMessageCategory());

        if (npa) {
            rReqJsonMessage.setAuthenticationValue(null);
        }

        boolean pa = XMessageCategory.C01.isEqual(rReqJsonMessage.getMessageCategory());
        if (!pa && !npa || !TRANS_STATUS_VALUES_FOR_REQUIRED_ECI.contains(rReqJsonMessage.getTransStatus())) {
            rReqJsonMessage.setEci(null);
        }
        return rReqJsonMessage;
    }

    private void validateTdsServerOperatorId(JsonMessage jsonMessage, List<Error> errors) {
        if (Misc.isNullOrEmpty(jsonMessage.getThreeDSServerOperatorID())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.threeDSServerOperatorID));
        }
    }

    private void validateMessageExtensions(JsonMessage aReqJsonMessage, List<Error> errors) {
        List<MessageExtension> messageExtensions = Optional.ofNullable(aReqJsonMessage.getMessageExtension())
            .orElse(Collections.emptyList());
        //messageExtension size with more than 10 wont reach here since it would be validated in DS Core already
        if (messageExtensions.size() == 10 && !messageExtensions.contains(directoryServerMessageExtensionForAReq)) {
            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.messageExtension,
                "too many extensions (" + messageExtensions.size() +
                    " is more than 9)"));
        }
    }

    private void validatePurchaseCurrency(final JsonMessage jsonMessage, final List<Error> errors) {
        String purchaseCurrency = jsonMessage.getPurchaseCurrency();
        if (Misc.isNumber(purchaseCurrency) && purchaseCurrency.length() == 3) {
            if (!SUPPORTED_PURCHASE_CURRENCY.equals(jsonMessage.getPurchaseCurrency())) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.purchaseCurrency,
                    String.format("purchase currency must be %s (Australian Dollar) for Eftpos Secure", SUPPORTED_PURCHASE_CURRENCY)));
            }
        }
    }
}
