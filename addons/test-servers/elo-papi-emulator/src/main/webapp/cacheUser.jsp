<%@ page import="com.modirum.ds.services.elo.authserver.EloAuthenticationServerStub" %>
<%@ page import="org.apache.commons.io.IOUtils" %>
<%@ page import="com.modirum.ds.services.elo.authserver.Response" %><%

    String queryRequest = IOUtils.toString(request.getReader());
    System.out.println("Request: " + queryRequest);
    Response papiResponse = EloAuthenticationServerStub
            .getInstance()
            .cacheUserCredentials(queryRequest);

    System.out.println("Json Response Returned = " + papiResponse.getBody());
    response.setStatus(papiResponse.getStatusCode());
    out.print(papiResponse.getBody());
    return;
%>