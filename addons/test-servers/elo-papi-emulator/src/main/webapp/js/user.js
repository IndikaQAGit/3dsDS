function cacheUser() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var clientId = document.getElementById("clientId").value;
    var clientSecret = document.getElementById("clientSecret").value;

    var credentialJson = JSON.stringify({
        username: username,
        password: password,
        clientId: clientId,
        clientSecret: clientSecret
    });
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                document.getElementById("cacheUserResult").value = xmlhttp.responseText;
            } else {
                document.getElementById("cacheUserResult").value = xmlhttp.status + " (" + xmlhttp.statusText + ")";
            }
        }
    }

    var userCacheUrl = window.location.protocol + "//" + window.location.host +
        "/elo-papi-emulator/cacheUser.jsp";

    xmlhttp.open("POST", userCacheUrl, true);
    xmlhttp.send(credentialJson);
}

function resetCacheUserFields() {
    document.getElementById("username").value = '';
    document.getElementById("password").value = '';
    document.getElementById("clientId").value = '';
    document.getElementById("clientSecret").value = '';
    document.getElementById("cacheUserResult").value = '';

}

function setAccessTokenExpiredFlag(isExpired) {
    document.getElementById("accessTokenResult").innerHTML = '';
    document.getElementById("refreshTokenResult").innerHTML = '';
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                document.getElementById("accessTokenResult").innerHTML = 'Successfully ' + (isExpired ? 'expired' : 'reset') + ' accessToken';
            } else {
                document.getElementById("accessTokenResult").innerHTML = xmlhttp.status + " (" + xmlhttp.statusText + ")";
            }
        }
    }

    var expireAccessTokenUrl = window.location.protocol + "//" + window.location.host +
        "/elo-papi-emulator/expireAccessToken.jsp?expired=" + isExpired;

    xmlhttp.open("POST", expireAccessTokenUrl, true);
    xmlhttp.send("");
}

function setRefreshTokenExpiredFlag(isExpired) {
    document.getElementById("accessTokenResult").innerHTML = '';
    document.getElementById("refreshTokenResult").innerHTML = '';
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                document.getElementById("refreshTokenResult").innerHTML = 'Successfully ' + (isExpired ? 'expired' : 'reset') + ' refreshToken';
            } else {
                document.getElementById("refreshTokenResult").innerHTML = xmlhttp.status + " (" + xmlhttp.statusText + ")";
            }
        }
    }

    var expireRefreshTokenUrl = window.location.protocol + "//" + window.location.host +
        "/elo-papi-emulator/expireRefreshToken.jsp?expired=" + isExpired;

    xmlhttp.open("POST", expireRefreshTokenUrl, true);
    xmlhttp.send("");
}