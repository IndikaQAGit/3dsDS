package com.modirum.ds.services.elo.authserver;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import elo.api.bcrypter.BcryptBase64;
import elo.api.bcrypter.EloBcrypter;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.DataFetcherResult;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import kotlin.TypeCastException;
import kotlin.text.Charsets;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Mock ELO Authentication Server
 */
public class EloAuthenticationServerStub {
    private final static EloAuthenticationServerStub instance = new EloAuthenticationServerStub();
    private static final String defaultClientId = "dsClientId";
    private static final String defaultClientSecret = "dsClientSecret";
    private static final String defaultUsername = "dsUser";
    private static final String defaultPassword = "password";
    private static String salt;
    private static final String dsRefreshToken = "dsRefreshToken";
    private static final String dsAccessToken = "dsAccessToken";
    private static final UserSession userSession = new UserSession()
            .setName("Modirum DS")
            .setId("1")
            .setClientMutationId("1")
            .setOauthToken(new OAuthToken()
                                   .setAccessToken(dsAccessToken)
                                   .setRefreshToken(dsRefreshToken));
    public static final String CLIENT_ID_KEY = "clientId";
    public static final String CLIENT_SECRET_KEY = "clientSecret";
    public static final String USERNAME_KEY = "username";
    public static final String PASSWORD_KEY = "password";
    private static boolean isExpiredAccessToken = false;
    private static boolean isExpiredRefreshToken = false;

    private final ObjectMapper jsonObjectMapper = new ObjectMapper();
    private final static Map<String, String> credentialsCache = initCredentialsCache();

    private final GraphQL graphQL;

    private EloAuthenticationServerStub() {
        String schema = readGraphQLTemplate("schema.graphqls");

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                                                   .type("Query", builder -> builder.dataFetcher("users", environment -> {
                                                       List<String> usersResponse = Arrays.asList(defaultUsername);
                                                       return DataFetcherResult.newResult()
                                                                               .data(usersResponse)
                                                                               .build();
                                                   }))
                                                   .type("Mutation", builder -> builder
                                                           .dataFetcher("createUser", environment -> {
                                                               isExpiredRefreshToken = false;
                                                               isExpiredAccessToken = false;
                                                               return DataFetcherResult.newResult()
                                                                                       .data(userSession)
                                                                                       .build();
                                                           }).dataFetcher("createLoginSalt", environment -> {
                                                               Map<String, Object> input = environment.getArgument("input");
                                                               String username = (String)input.get("username");

                                                               salt = getSalt(username);
                                                               LoginSalt loginSalt = new LoginSalt()
                                                                       .setSalt(salt)
                                                                       .setUsername(username);
                                                               return DataFetcherResult.newResult()
                                                                                       .data(loginSalt)
                                                                                       .build();
                                                           }).dataFetcher("login", environment -> {
                                                               Map<String, Object> input = environment.getArgument("input");
                                                               if (!credentialsCache.get(USERNAME_KEY).equals(input.get("username"))) {
                                                                   throw new IllegalArgumentException("Invalid username");
                                                               }
                                                               if (salt == null) {
                                                                   throw new IllegalArgumentException("Invalid login salt");
                                                               }

                                                               String bcryptPassword = EloBcrypter.encryptPassword(credentialsCache.get(USERNAME_KEY),
                                                                                                                   credentialsCache.get(PASSWORD_KEY));
                                                               String challenge = EloBcrypter.createLoginChallenge(bcryptPassword, salt);
                                                               if (!challenge.equals(input.get("challenge"))) {
                                                                   throw new IllegalArgumentException("Invalid challenge");
                                                               }
                                                               isExpiredRefreshToken = false;
                                                               isExpiredAccessToken = false;
                                                               salt = null;
                                                               return DataFetcherResult.newResult()
                                                                                       .data(userSession)
                                                                                       .build();
                                                           }).dataFetcher("refreshAccessToken", environment -> {
                                                               Map<String, Object> input = environment.getArgument("input");
                                                               if (!dsRefreshToken.equals(input.get("refreshToken")) || isExpiredRefreshToken) {
                                                                   throw new IllegalArgumentException("Invalid refreshToken");
                                                               }
                                                               isExpiredRefreshToken = false;
                                                               isExpiredAccessToken = false;
                                                               return DataFetcherResult.newResult()
                                                                                       .data(userSession)
                                                                                       .build();
                                                           }))
                                                   .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    public static EloAuthenticationServerStub getInstance() {
        return instance;
    }

    private static String getSalt(String username) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");

            Charset var4 = Charsets.UTF_8;
            byte[] usernameSha256Hash;
            if (username == null) {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            } else {
                usernameSha256Hash = username.getBytes(var4);
                byte[] var6 = usernameSha256Hash;
                usernameSha256Hash = digest.digest(var6);
            }
            String base64hash = BcryptBase64.encodeBcryptBase64(usernameSha256Hash, 16);
            return "$2a$12$" + base64hash;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static Map<String, String> initCredentialsCache() {
        Map<String, String> credentialsMap = new HashMap<>();
        credentialsMap.put(USERNAME_KEY, defaultUsername);
        credentialsMap.put(PASSWORD_KEY, defaultPassword);
        credentialsMap.put(CLIENT_ID_KEY, defaultClientId);
        credentialsMap.put(CLIENT_SECRET_KEY, defaultClientSecret);
        return credentialsMap;
    }

    public Response processGraphQL(String queryRequest, Map<String, String> headers) {
        //initial implementation for clientId and Authorization header validations, maybe changed later
        String clientId = credentialsCache.get(CLIENT_ID_KEY);
        if (!clientId.equals(headers.get("client_id"))) {
            return new Response(HttpURLConnection.HTTP_UNAUTHORIZED, "Could not find a required APP in the request, identified by HEADER client_id");
        }

        String clientSecret = credentialsCache.get(CLIENT_SECRET_KEY);
        String authorizationHeaderValue = "Basic " +
                                          Base64.getEncoder()
                                                .encodeToString((clientId + ":" +
                                                                 clientSecret).getBytes(StandardCharsets.UTF_8));
        if (!authorizationHeaderValue.equals(headers.get("Authorization"))) {
            return new Response(HttpURLConnection.HTTP_OK, "{\"errors\":[{\"message\":\"[{\\\"code\\\":\\\"422.005\\\",\\\"description\\\":\\\"Error process login.\\\"}]\",\"locations\":[{\"line\":2,\"column\":3}],\"path\":[\"createUser\"],\"extensions\":{\"code\":\"422\"}}],\"data\":null}");
        }

        try {
            JsonNode jsonRequest = jsonObjectMapper.readTree(queryRequest);
            if (!jsonRequest.has("query")) {
                return new Response(HttpURLConnection.HTTP_BAD_REQUEST, "Must provide query string.");
            }
            String query = jsonRequest.at("/query").asText();
            ExecutionResult executionResult = this.graphQL.execute(query);
            String jsonResponse = jsonObjectMapper.writeValueAsString(executionResult.toSpecification());
            return new Response(HttpURLConnection.HTTP_OK, jsonResponse);
        } catch (Exception e) {
            //temporary error for now, while specific errors are not yet laid out
            return new Response(HttpURLConnection.HTTP_BAD_REQUEST, "{\"error\" : \"Encounter error while parsing the query.\"}");
        }

    }

    /**
     * Validates API access such as AReq Detokenization API.
     * Checks if client_id and access_token headers are valid.
     * @param headers
     * @return
     */
    public Response validateApiAccess(Map<String, String> headers) {
        String clientId = credentialsCache.get(CLIENT_ID_KEY);

        //initial implementation for clientId and Authorization header validations, maybe changed later
        if (!clientId.equals(headers.get("client_id"))) {
            return new Response(HttpURLConnection.HTTP_UNAUTHORIZED, false, "Could not find a required APP in the request, identified by HEADER client_id");
        }

        String accesstoken = headers.get("access_token");
        if (isExpiredAccessToken || !dsAccessToken.equals(accesstoken)) {
            return new Response(HttpURLConnection.HTTP_OK, false, "{\"errors\":[{\"message\":\"{\\\"code\\\":\\\"401.001\\\",\\\"description\\\":\\\"Could not find a required Access Token in the request (identified by HEADER access_token) or the Access Token is expired.\\\"}\",\"locations\":[{\"line\":1,\"column\":12}],\"path\":[\"user\"],\"extensions\":{\"code\":\"401\"}}],\"data\":{\"user\":null}}");
        }

        return new Response(HttpURLConnection.HTTP_OK, null);
    }

    private String readGraphQLTemplate(String graphQLFile) {
        try {
            //This is needed to make it work with classpath /WEB-INF/classes at runtime
            InputStream resourceAsStream = Thread.currentThread()
                                                 .getContextClassLoader()
                                                 .getResourceAsStream(graphQLFile);
            return IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Response cacheUserCredentials(String credentialJson) {
        try {
            JsonNode requestMap = jsonObjectMapper.readTree(credentialJson);
            String username = requestMap.at("/username").asText();
            String password = requestMap.at("/password").asText();
            String clientId = requestMap.at("/clientId").asText();
            String clientSecret = requestMap.at("/clientSecret").asText();
            credentialsCache.clear();
            credentialsCache.put(USERNAME_KEY, isEmpty(username) ? defaultUsername : username);
            credentialsCache.put(PASSWORD_KEY, isEmpty(password) ? defaultPassword : password);
            credentialsCache.put(CLIENT_ID_KEY, isEmpty(clientId) ? defaultClientId : clientId);
            credentialsCache.put(CLIENT_SECRET_KEY, isEmpty(clientSecret) ? defaultClientSecret : clientSecret);
            salt = null;
            String jsonResponse = jsonObjectMapper.writeValueAsString(credentialsCache);
            return new Response(HttpURLConnection.HTTP_OK, jsonResponse);
        } catch (Exception e) {
            e.printStackTrace();
            return new Response(HttpURLConnection.HTTP_INTERNAL_ERROR, e.getMessage());
        }
    }

    public void setAccessTokenExpiredFlag(boolean expired) {
        isExpiredAccessToken = expired;
    }

    public void setRefreshTokenExpiredFlag(boolean expired) {
        isExpiredRefreshToken = expired;
    }

    private boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

}
