package com.modirum.ds.services.elo.authserver;

import java.net.HttpURLConnection;

/**
 * Graphql server response
 */
public class Response {
    private final int statusCode;
    private final String body;
    private final boolean success;

    public int getStatusCode() {
        return statusCode;
    }

    public String getBody() {
        return body;
    }

    public Response(int statusCode, String body) {
        this(statusCode, true, body);
    }

    public Response(int statusCode, boolean success, String body) {
        this.statusCode = statusCode;
        this.body = body;
        this.success = success;
    }

    public boolean isSuccessfulResponse() {
        //most of the time, even for ELO authorization errors, status code is 200, hence needed another flag `success`
        return success && (statusCode == HttpURLConnection.HTTP_OK || statusCode == HttpURLConnection.HTTP_CREATED);
    }
}