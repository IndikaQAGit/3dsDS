var ZLIB
(function (factory) {
    factory(ZLIB = {});
}(function (ZLIB) {
    'use strict';

    var DEFAULT_LEVEL = 6;

    function putByte(n, arr) {
        arr.push(n & 0xFF);
    }

    // LSB first
    function putShort(n, arr) {
        arr.push(n & 0xFF);
        arr.push(n >>> 8);
    }

    // LSB first
    function putLong(n, arr) {
        putShort(n & 0xffff, arr);
        putShort(n >>> 16, arr);
    }

    // MSB first
    function putShortMSB(n, arr) {
        arr.push(n >>> 8);
        arr.push(n & 0xFF);
    }

    // MSB (most significant bit) first
    function putLongMSB(n, arr) {
        putShortMSB(n >>> 16, arr);
        putShortMSB(n & 0xffff, arr);
    }

    function putString(s, arr) {
        var i, len = s.length;
        for (i = 0; i < len; i += 1) {
            putByte(s.charCodeAt(i), arr);
        }
    }

    function readByte(arr) {
        return arr.shift();
    }

    function readShort(arr) {
        return arr.shift() | (arr.shift() << 8);
    }

    function readLong(arr) {
        var n1 = readShort(arr),
            n2 = readShort(arr);

        // JavaScript can't handle bits in the position 32
        // we'll emulate this by removing the left-most bit (if it exists)
        // and add it back in via multiplication, which does work
        if (n2 > 32768) {
            n2 -= 32768;

            return ((n2 << 16) | n1) + 32768 * Math.pow(2, 16);
        }

        return (n2 << 16) | n1;
    }

    function readString(arr) {
        var charArr = [];

        // turn all bytes into chars until the terminating null
        while (arr[0] !== 0) {
            charArr.push(String.fromCharCode(arr.shift()));
        }

        // throw away terminating null
        arr.shift();

        // join all characters into a cohesive string
        return charArr.join('');
    }

    /*
     * Reads n number of bytes and return as an array.
     *
     * @param arr- Array of bytes to read from
     * @param n- Number of bytes to read
     */
    function readBytes(arr, n) {
        var i, ret = [];
        for (i = 0; i < n; i += 1) {
            ret.push(arr.shift());
        }

        return ret;
    }

    /*
     * Data compression according to https://www.ietf.org/rfc/rfc1950.txt
     */
    function compress(data) {
        var out = [];

        if (typeof data === 'string') {
            data = Array.prototype.map.call(data, function (char) {
                return char.charCodeAt(0);
            });
        }

        // CMF , FLG bytes. For some reason data is inverted, but seems to work with JAVA deflate impl
        putByte(120, out); // 0111 1000
        putByte(156, out); // 100 11100

        RAWDEFLATE.deflate(data, DEFAULT_LEVEL).forEach(function (byte) {
            putByte(byte, out);
        });

        putLongMSB(ADLER32.buf(data), out);

        return out;
    }

    function decompress(data) {
        // start with a copy of the array
        var arr = Array.prototype.slice.call(data, 0);

        // throw away 2 first flag bytes
        arr.shift();
        arr.shift();

        // give inflate everything but the last 4 bytes (adler32 checksum).
        var inflatedBytes = RAWINFLATE.inflate(arr.splice(0, arr.length - 4));

        var inflatedString = Array.prototype.map.call(inflatedBytes, function (byte) {
                return String.fromCharCode(byte);
            }).join('');

        //var adler32 = readLong(arr);
        //if (adler32 !== parseInt(ADLER32.buf(inflatedString), 16)) {
        //    throw 'Adler32 checksum does not match';
        //}

        return inflatedString;
    }

    ZLIB.compress = compress;
    ZLIB.decompress = decompress;

}));