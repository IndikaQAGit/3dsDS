function arrayToBase64(bytes) {
    var unsigedIntArr = new Uint8Array(bytes);

    var charString = '';
    for (var i = 0; i < unsigedIntArr.byteLength; i++) {
        charString += String.fromCharCode(unsigedIntArr[i]);
    }

    return window.btoa(charString);
}

function base64ToArray(base64) {
    var charString = window.atob(base64);
    var bytes = Array.prototype.map.call(charString, function (char) {
        return char.charCodeAt(0);
    });

    return bytes;
}

function toBase64URL(str) {
    return btoa(str)
        .replace(/=/g, "")
        .replace(/\+/g, "-")
        .replace(/\//g, "_");
}

function fromBase64URL(base64URL) {
    var str = base64URL + "===";
    str = str.slice(0, Math.floor(str.length / 4) * 4);
    return atob(str
        .replace(/-/g, "+")
        .replace(/_/g, "/"));
}

function stringToArray(str) {
    return Array.prototype.map.call(str, function (char) {
        return char.charCodeAt(0);
    });
}

function arrayBufferToString(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}
function hexToString(hex) {
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function getPublic(base64) {
    var acsPublicAsnOne = atob(base64);
    var asnOneKeyBytesStart = acsPublicAsnOne.lastIndexOf(String.fromCharCode(0)) + 2;
    var asnOneKeyBytesLength = acsPublicAsnOne.length - asnOneKeyBytesStart;
    var coordinateBytesLength = asnOneKeyBytesLength / 2;
    var xStart = asnOneKeyBytesStart;
    var xEnd = xStart + coordinateBytesLength;
    var yEnd = xEnd + coordinateBytesLength;
    var x = acsPublicAsnOne.slice(xStart, xEnd);
    var y = acsPublicAsnOne.slice(xEnd, yEnd);

    return {
        x: x,
        y: y
    }
}

function encodeStringWithLength(str) {
    return getBigEndian(str.length, 4).concat(stringToArray(str));
}

function getBigEndian(number, arrayLength) {
    var array = Array.apply(null, {length: arrayLength});
    array.fill(0);
    for (var i = 0; i < arrayLength; i++) {
        if (number == 0) {
            break;
        }
        array[arrayLength - i - 1] = number % 256;
        number = Math.floor(number / 256);
    }

    return array;
}

function jweCompactSerialize(header, encryptedKey, iv, cipherText, authTag) {
    return toBase64URL(header) + "." +
        toBase64URL(encryptedKey) + "." +
        toBase64URL(iv) + "." +
        toBase64URL(cipherText) + "." +
        toBase64URL(authTag)
}

function prettyPrint(str){
    return JSON.stringify(JSON.parse(str), null, 2);
}

function prettyPrintInputElement(elementId){
    var original = document.getElementById(elementId).value;
    document.getElementById(elementId).value = prettyPrint(original);
}