/**
 * Retrieves acsTransID field from ARes json
 */
function getAcsTransIDFromAres(aresString) {
    var ares = JSON.parse(aresString);
    return ares.acsTransID;
}

function getMessageVersionFromAres(aresString) {
    var ares = JSON.parse(aresString);
    return ares.messageVersion;
}

/**
 * Retrieves a field from a JSON object
 */
function getFieldFromJsonObj(jsonString, fieldName) {
    var jsonObj = JSON.parse(jsonString);
    return jsonObj[fieldName];
}

/**
 * Retrieves dsTransID field from ARes json
 */
function getDsTransIDFromAres(aresString) {
    var ares = JSON.parse(aresString);
    return ares.dsTransID;
}

/**
 * Retrieves sdkReferenceNumber field from AReq json
 */
function getDsReferenceNumberFromAres(aresString) {
    var ares = JSON.parse(aresString);
    return ares.dsReferenceNumber;
}

/**
 * Retrieves threeDSServerTransID field from ARes json
 */
function getThreeDSServerTransIDFromAres(aresString) {
    var ares = JSON.parse(aresString);
    return ares.threeDSServerTransID;
}

/**
 * Retrieves sdkReferenceNumber field from AReq json
 */
function getSdkReferenceNumberFromAreq(areqString) {
    var areq = JSON.parse(areqString);
    return areq.sdkReferenceNumber;
}

/**
 * Retrieves sdkTransID field from ARes json
 */
function getSdkTransIDFromAres(aresString) {
    var ares = JSON.parse(aresString);
    return ares.sdkTransID;
}

function isVersion220(messageVersion) {
    return messageVersion == '2.2.0';
}

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function convertToSpecValue(fieldName, value) {
    if (SPEC_VALUE[fieldName] && SPEC_VALUE[fieldName][value]) {
        return SPEC_VALUE[fieldName][value];
    }

    return null;
}

/**
 * Generates a new areq message
 */
function getNewAreq(params) {
    const messageVersion = params["messageVersion"];
    const messageCategory = params["messageCategory"];
    const deviceChannel = params["deviceChannel"];

    let threeDSServerTransId  = getUrlVars()["threeDSServerTransId"];
    if (threeDSServerTransId == undefined || threeDSServerTransId == '') {
        threeDSServerTransId = generateUUID();
    }

    params["threeDSServerTransID"] = threeDSServerTransId;

    // check if device channel = APP
    if (deviceChannel == '01') {
        params["sdkTransID"] = generateUUID();
    }

    let fields = getJsonFields(deviceChannel, messageCategory, messageVersion);

    if (fields.length == 0) {
        return;
    }
    return generateAreqJson(fields, params);
}

/**
 * Returns an array of string which contains the data element name
 * for generating an areq message
 * @param deviceChannel device channel field of Areq Message
 * @param messageCategory message category field for Areq Message
 * @param messageVersion message version field for Areq Message
 */
function getJsonFields(deviceChannel, messageCategory, messageVersion) {
    let fields = [];
    let PA = '01', NPA = '02';
    let APP = '01', BRW = '02', TRI = '03';
    switch (messageVersion) {
        case '2.1.0':
            switch (messageCategory) {
                // Message Category is PA
                case PA:
                    switch (deviceChannel) {
                        // Device channel APP
                        case APP: fields = AREQ_PA_APP_210; break;
                        // Device channel BRW
                        case BRW: fields = AREQ_PA_BRW_210; break;
                        // Device channel 3RI
                        case TRI: fields = AREQ_PA_3RI_210; break;
                    }
                    break;
                // Message Category is NPA
                case NPA:
                    switch (deviceChannel) {
                        // Device channel APP
                        case APP: fields = AREQ_NPA_APP_210;  break;
                        // Device channel BRW
                        case BRW: fields = AREQ_NPA_BRW_210;  break;
                        // Device channel 3RI
                        case TRI: fields = AREQ_NPA_3RI_210; break;
                    }
                    break;
            }
            break;
        case '2.2.0':
            switch (messageCategory) {
                // Message Category is PA
                case PA:
                    switch (deviceChannel) {
                        // Device channel APP
                        case APP: fields = AREQ_PA_APP_220; break;
                        // Device channel BRW
                        case BRW: fields = AREQ_PA_BRW_220; break;
                        // Device channel 3RI
                        case TRI: fields = AREQ_PA_3RI_220; break;
                    }
                    break;
                // Message Category is NPA
                case NPA:
                    switch (deviceChannel) {
                        // Device channel APP
                        case APP: fields = AREQ_NPA_APP_220; break;
                        // Device channel BRW
                        case BRW: fields = AREQ_NPA_BRW_220;  break;
                        // Device channel 3RI
                        case TRI: fields = AREQ_NPA_3RI_220; break;
                    }
                    break;
            }
            break;
        case '2.3.0':
            switch (messageCategory) {
                // Message Category is PA
                case PA:
                    switch (deviceChannel) {
                        // Device channel APP
                        case APP: fields = AREQ_PA_APP_230; break;
                        // Device channel BRW
                        case BRW: fields = AREQ_PA_BRW_230; break;
                        // Device channel 3RI
                        case TRI: fields = AREQ_PA_3RI_230; break;
                    }
                    break;
                // Message Category is NPA
                case NPA:
                    switch (deviceChannel) {
                        // Device channel APP
                        case APP: fields = AREQ_NPA_APP_230; break;
                        // Device channel BRW
                        case BRW: fields = AREQ_NPA_BRW_230;  break;
                        // Device channel 3RI
                        case TRI: fields = AREQ_NPA_3RI_230; break;
                    }
                    break;
            }
            break;
    }
    return fields;
}

/**
 * Generates an Areq json formatted string based on the fields parameter
 *
 * @param fields data elements/field names of the areq message
 * @param params pre-generated values for some data elements
 * @returns {string} formatted Areq message json
 */
function generateAreqJson(fields, params) {
    let areq = '';
    fields.forEach(field => {
        if (areq.length > 0) {
            areq = areq.concat(",\n");
        }
        areq = areq.concat(params[field]?
            generateJsonAttr(field, "\"" + params[field] + "\"") :
            generateJsonAttr(field, AREQ_FIELDS[field]));
    });

    areq = "{\n" + areq + "\n}";

    return areq;
}

/**
 * Generates a json attribute
 * @param fieldName json attribute name
 * @param fieldValue json attribute value
 * @returns {string} formatted json string which contains name and value
 */
function generateJsonAttr(fieldName, fieldValue) {
    return "\"" + fieldName + "\" : " + fieldValue;
}

/**
 *
 * @param json json data
 * @param fieldName field name to be added to json
 * @returns {string|*}
 */
function appendElementToJson(json, fieldName, elementId) {
    const fieldData = getElementValueInDocument(elementId);
    if (fieldData == null) {
        return json;
    }

    return appendFieldDataToJson(json, fieldName, fieldData);
}

function appendFieldDataToJson(json, fieldName, fieldData) {
    return (json + ", \n\"" + fieldName + "\": \"" + fieldData + "\"");
}

/**
 * Get element value in document
 */
function getElementValueInDocument(name) {
    const elem = document.getElementById(name);
    if (elem) {
        if (elem.value && elem.value != '') {
            return elem.value;
        }
    }
    return null;
}

/**
 * Generates a new creq message for the BRW flow.
 */
function getNewCreq(threeDSServerTransID, acsTransID, messageVersion) {
    return "{\n" +
        "\"threeDSServerTransID\" : \"" + threeDSServerTransID + "\",\n" +
        "\"acsTransID\" : \"" + acsTransID + "\",\n" +
        "\"challengeWindowSize\" : \"02\",\n" +
        "\"messageType\" : \"CReq\",\n" +
        "\"messageVersion\" : \"" + messageVersion + "\"\n" +
        "}";
}

/**
 * Generates a new rreq message for the BRW flow.
 */
function getNewRreq() {
    return "{\n" +
        "    \"authenticationMethod\": \"01\",\n" +
        "    \"authenticationType\": \"01\",\n" +
        "    \"authenticationValue\": \"QUNTRU1VdSZPIWtMeXtjWVNyJT4=\",\n" +
        "    \"eci\": \"05\",\n" +
        "    \"interactionCounter\": \"01\",\n" +
        "    \"messageType\": \"RReq\",\n" +
        "    \"transStatus\": \"Y\"\n" +
        "}";
}

/**
 * Generates a new creq message for the APP flow.
 */
function getNewCreqApp(threeDSServerTransID, acsTransID, sdkTransID, messageVersion) {

    let creq = "\"threeDSServerTransID\" : \"" + threeDSServerTransID + "\",\n" +
        "\"acsTransID\" : \"" + acsTransID + "\",\n" +
        "\"messageType\" : \"CReq\",\n" +
        "\"messageVersion\" : \""+ messageVersion +"\",\n" +
        "\"sdkCounterStoA\" : \"000\",\n" +
        "\"sdkTransID\" : \"" + sdkTransID + "\"";

    if (isVersion220(messageVersion)) {
        // define new fields for messageVersion 220
        creq = this.append220CreqFields(creq, 'APP');
    }

    creq = "{\n" + creq + "\n}";

    return creq;
}

/**
 * Appends Creq fields for Message version 2.2.0
 */
function append220CreqFields(creq, channel) {

    let creq220 = "";
    if (channel == 'APP') {
        creq220 = appendElementToJson(creq220, 'threeDSRequestorAppURL', 'threeDSRequestorAppURL_220');
        creq220 = appendElementToJson(creq220, 'challengeNoEntry', 'challengeNoEntry_220');
        creq220 = appendElementToJson(creq220, 'whitelistingDataEntry', 'whitelistingDataEntry_220');
    }
    // append the 220 version to the old json default data
    return creq.concat(creq220);
}

/**
 * Generates a new rres message.
 */
function getNewRres(acsTransID, dsTransID, threeDSServerTransID, messageVersion, deviceChannel, sdkTransId) {
    let rres = {
        "acsTransID": acsTransID,
        "dsTransID": dsTransID,
        "messageType": "RRes",
        "messageVersion": messageVersion,
        "resultsStatus": "01",
        "threeDSServerTransID": threeDSServerTransID
    };

    if (deviceChannel == '01' && messageVersion == '2.2.0') {
        rres["sdkTransID"] = sdkTransId;
    }

    return rres;
}

function getNewAcsEmulatedAresChallenge(acsTransId, areq) {
    return {
        "acsChallengeMandated": "Y",
        "authenticationType": "02",
        "acsOperatorID": "ACS-EMULATOR-OPER-ID",
        "acsReferenceNumber": "ACS-EMULATOR-REF-NO",
        "acsTransID": acsTransId,
        "acsURL": "http://localhost:8080/ds-3ds-emulator/randomUnusedCreqPage.jsp",
        "messageType": "ARes",
        "messageVersion": areq.messageVersion,
        "threeDSServerTransID": areq.threeDSServerTransID,
        "sdkTransID": areq.sdkTransID,
        "acsRenderingType": {
            "acsInterface": "02",
            "acsUiTemplate": "03"
        },
        "acsSignedContent": "eyJ4NWMiOlsiTUlJRUd6Q0NBd09nQXdJQkFnSUpBTklRZVZCRVwvNzE1TUEwR0NTcUdTSWIzRFFFQkN3VUFNSUdqTVFzd0NRWURWUVFHRXdKVlV6RVJNQThHQTFVRUNBd0lTV3hzYVc1dmFYTXhFekFSQmdOVkJBY01DbEpwZG1WeWQyOXZaSE14SkRBaUJnTlZCQW9NRzBScGMyTnZkbVZ5SUVacGJtRnVZMmxoYkNCVFpYSjJhV05sY3pFWk1CY0dBMVVFQ3d3UVVHRjViV1Z1ZENCVFpYSjJhV05sY3pFck1Da0dBMVVFQXd3aU0yUnpMV1JwYzJOdmRtVnlMblJsYzNRdWJXOWthWEoxYldOc2IzVmtMbU52YlRBZUZ3MHlNREEzTURreE16RTRNRGhhRncweU16QTBNRFV4TXpFNE1EaGFNSUdqTVFzd0NRWURWUVFHRXdKVlV6RVJNQThHQTFVRUNBd0lTV3hzYVc1dmFYTXhFekFSQmdOVkJBY01DbEpwZG1WeWQyOXZaSE14SkRBaUJnTlZCQW9NRzBScGMyTnZkbVZ5SUVacGJtRnVZMmxoYkNCVFpYSjJhV05sY3pFWk1CY0dBMVVFQ3d3UVVHRjViV1Z1ZENCVFpYSjJhV05sY3pFck1Da0dBMVVFQXd3aU0yUnpMV1JwYzJOdmRtVnlMblJsYzNRdWJXOWthWEoxYldOc2IzVmtMbU52YlRDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTFwvTXFBQVZTbDNiMldic1Bhb2NaQlBpczAxdk1SazhIYzlvMGpHTGo2OWUrVmEyc21Eakp4dWNvd0pYelR4bkl6RHNIVXlGazVPbzMzeGg2NXNMeEhMVDVtZnFPK3k3UExVbENHVHVcL3lSUHgrSlNrN2tIZXpNR1hyR2pxWmc2cHJlSWcrRmdDNVU3dm1hK0tJa0JTQTRFdlNJVDlBMWlQOHN3UW1hT1A1UUNwNHdmWVI3NW1xZ2FLU3JlbDFxclNWSVJJVlROajBkXC9KRFwvNUswM3d3UHhmaW9TKytlZU1wbUUwZ0JjMFlPeVVWNEppd3FDNXRXd2M4Q055TmcwMTlra0xFVW5YOUV6aytqWHNSMStzbkZjd3RSdGFrWW9iaW1GMzIxNTRBUm9SUlJSWDNMcm03ZnR1emxtcmVUa2JpQ0lXR3N4SjhvR25tdWRDSjhYTFZrRUNBd0VBQWFOUU1FNHdIUVlEVlIwT0JCWUVGQ2l4RGdtRkMxbENwM01oNTdod2M5czJ1WEdRTUI4R0ExVWRJd1FZTUJhQUZDaXhEZ21GQzFsQ3AzTWg1N2h3YzlzMnVYR1FNQXdHQTFVZEV3UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFLYiszNm84NVRxWEJRVnBzeFpmSjlkY0xOZE1LaHZHbTkxN2RoWmFkOVRIdnFLbElHOXVzd1lTVDZyUTl1akZFaVVFNVI0b2xhbTFHbEZvN2RqTmZkdkFEd3hjTlJcL2pVTXVTTkhRa1FjckN6d2QrSXo4SDJNeUlobDhWSnkrWnc3S21TS3BUamoyeHJtQ1hZUGhcLzVSWGVLeEZVSk1aS1wvdFJkZTRIMG9FS0ZtUyszWHUyZlRLM2JpQ1QrYlhWM2RMNkR0VHQxUFBHYjdDZklmOXFCbFJXekZBTXh3THE4dUN3QU1rYVZuMmtmeDZOMjFWQ1wvZUVZcjJJTE1LU2pjR2prYmpWRjNxRE5LUHVCMndJcE5TS3FNMmFlMFNxZVVSWE1mMEU4dFhHWURQdXc5b1F5dEVmVUIxWjhVdkZLS1wvblZhS2xESVQ5aTIzWEtJQWptM0Y3ND0iXSwiYWxnIjoiUFMyNTYifQ.eyJhY3NVUkwiOiJodHRwczovLzNkcy1kaXNjb3Zlci50ZXN0Lm1vZGlydW1jbG91ZC5jb20vbWRwYXlhY3MvY3JlcWFwcCIsImFjc0VwaGVtUHViS2V5Ijp7Imt0eSI6IioqKiIsImNydiI6IioqKiIsIngiOiIqKioiLCJ5IjoiKioqIn0sInNka0VwaGVtUHViS2V5Ijp7Imt0eSI6IioqKiIsImNydiI6IioqKiIsIngiOiIqKioiLCJ5IjoiKioqIn19.RZB3MvNq4frw8T6XJ7ebB5ozIB5a3FVRg40x310r9XhIEaj8DUTg4mmoQ72EsaIaggBJf5q4OvBexDCLOe1a8gMnNwfd4qmIdgMABjLzXIa4Cn8BAwhEU1OvIhx8NJTCs5k7w5JAlsBl_GcZFgphKpytfR_GUIIMHgAaC4NvlDYxRaXVj-j7OZtlO71StJmf5VBMCrlm6gfd3CsE-zODCfxm54gqveTIKnCMQdYF0fp8Quz0u7JoEV2Nnz_ns92G6nCV_8MV2LqKLGsoeR5sInRC4rNnwd7LrUTyQ3mqyDrfg4tvxjCvvnQKKnlsCK6FFbGEfAUeqeHNbJrtrWEIbw",
        "transStatus": "C",
        "authenticationMethod" : "01"
    };
}

function getNewAcsEmulatedAresFrictionless(acsTransId, areq) {
    return {
        "acsOperatorID": "ACS-EMULATOR-OPER-ID",
        "acsReferenceNumber": "ACS-EMULATOR-REF-NO",
        "acsTransID": acsTransId,
        "acsURL": "http://localhost:8080/ds-3ds-emulator/randomUnusedCreqPage.jsp",
        "eci": "05",
        "messageType": "ARes",
        "messageVersion": areq.messageVersion,
        "threeDSServerTransID": areq.threeDSServerTransID,
        "sdkTransID": areq.sdkTransID,
        "authenticationValue": "AJkBB4YJgQAAAACElgmBAAAAAAA=",
        "transStatus": "Y"
    };
}

//https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
function generateUUID() {
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}