<%@ page import="java.io.BufferedReader, org.json.JSONObject, org.json.JSONException, java.io.StringWriter, java.io.PrintWriter, java.util.Map" %><%

    // parse request body into a json object
    StringBuffer sb = new StringBuffer();
    BufferedReader br = request.getReader();
    String line;
    while ((line = br.readLine()) != null) {
        sb.append(line);
        sb.append("\n");
    }

    String rreqJsonString = sb.toString();

    JSONObject rreqObject;
    try {
        rreqObject = new JSONObject(rreqJsonString);
    } catch (JSONException jsonEx) {
        StringWriter sw = new StringWriter();
        jsonEx.printStackTrace(new PrintWriter(sw));
        out.print(sw.toString());
        return;
    }

    // retrive essential data fields
    String acsTransId = (String) rreqObject.get("acsTransID");
    String dsTransID = (String) rreqObject.get("dsTransID");
    String threeDSServerTransID = (String) rreqObject.get("threeDSServerTransID");

    Map<String, String> rresCache = null;
    if (application.getAttribute("rresCache") != null) {
        rresCache = (Map<String, String>) application.getAttribute("rresCache");

    }

    // check cache for prepared RRes by acsTransId from RReq, if present then use it and remove from cache
    if (rresCache.containsKey(acsTransId)) {
        String cachedRres = rresCache.get(acsTransId);
        rresCache.remove(acsTransId);
        out.print(cachedRres);
        return;

    } else {
        String messageVersion = (String) rreqObject.get("messageVersion");

        // use auto-generated successful RRes
        // try to reassemble parameters into successful RRes, based on received RReq.
        String rresString = "{\n" +
                "\"acsTransID\" : \"" + acsTransId + "\",\n" +
                "\"dsTransID\" : \"" + dsTransID + "\",\n" +
                "\"messageType\" : \"RRes\",\n" +
                "\"messageVersion\" : \"" + messageVersion + "\",\n" +
                "\"resultsStatus\" : \"01\",\n" +
                "\"threeDSServerTransID\" : \"" + threeDSServerTransID + "\"\n" +
                "}";

        out.print(rresString);
        return;
    }
%>