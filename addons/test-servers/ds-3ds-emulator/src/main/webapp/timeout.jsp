<%@page %>
<%
    long timeout = 10;
    String timeoutParam = request.getParameter("seconds");
    if (timeoutParam != null && !timeoutParam.isEmpty()) {
        try {
            timeout = Long.parseLong(timeoutParam);
        } catch (NumberFormatException e) {
            System.err.println("Invalid seconds parameter value. Expecting number format.");
        }
    }
    System.out.println("timeout.jsp is called. Starting to sleep for " + timeout + " seconds...");
    try {
        Thread.sleep(timeout * 1000);
    } catch(InterruptedException e) {
        System.err.println("timeout.jsp thread exits sleep!");
    }
%>