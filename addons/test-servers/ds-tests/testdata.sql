
INSERT INTO ds_miprofiles (id, `acquirerId`, name, `URL`, `outClientCert`, `inClientCert`, status, `SSLProto`)
VALUES (1, 1, 'MI Server 1', NULL, NULL, 1, 'A', NULL);

INSERT INTO ds_settings (skey, svalue, comments) VALUES ('acceptedMIList', 'Poster form', NULL);

INSERT INTO ds_settings (skey, svalue, comments) VALUES ('stage2authentication.checksdnbinmatch', 'true', NULL);

INSERT INTO ds_merchants (id, `MIProfileId`, `acquirerId`, identifier, name, phone, email, country, status)
VALUES (1, 1, 1, '771177', 'Merchant 1', NULL, NULL, '', 'A');

INSERT INTO ds_certdata (id, name, `issuerDN`, `subjectDN`, x509data, expires, status, `pkId`)
VALUES
(1, 'Merchant 1', '', 'CN=771122, OU=412345, O=Merchant 1, L=Tallinn, ST=HM, C=EE', '-----BEGIN CERTIFICATE-----
MIIDpTCCAo2gAwIBAgIEch+NzTANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJFRTELMAkGA1UE
CBMCSE0xEDAOBgNVBAcTB1RhbGxpbm4xGDAWBgNVBAoTD0NhcmQgU2NoZW1lIEluYzELMAkGA1UE
CxMCRFMxGDAWBgNVBAMTDzNEUyBTY2hlbWUgUm9vdDAeFw0xNjAxMTMxNjA5MzhaFw0xNjA0MTIx
NjA5MzhaMGMxCzAJBgNVBAYTAkVFMQswCQYDVQQIEwJITTEQMA4GA1UEBxMHVGFsbGlubjETMBEG
A1UEChMKTWVyY2hhbnQgMTEPMA0GA1UECxMGNDEyMzQ1MQ8wDQYDVQQDEwY3NzExMjIwggEiMA0G
CSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCF6YbnvWe1YACWVX+jPzDVkHgvvqYGH14tz5ZUe7dh
ETliAtvqkS8rfCI5OFejrTTquGdH7BmAMu57dxsw77PBHNuSxhGjnRt0T4bLdKVwSx9arHu5jcIp
LoIovmbOoK6hP3yN0Z2cPTF3R88nG4hAULof3WJ18TqngD1OQztlBpxZfQVTtDNv0V83fyrz5BWj
se1wTW1MDmYuL1jAowZv9XOBqpu7PBEKwLA979Yi63LqDOkBFyKbJ9RmhWK1YYq65daYIoyPWXge
Qr0D2xOKqp5nvWKV3EuWN1pqIL6dCJbIGHEv7qj2szUlCk4rgL5tfKPCefIfpBJcz1yAhmyRAgMB
AAGjVzBVMB8GA1UdIwQYMBaAFC7R38knHV4U9Vk/wyw1/gIyTvudMBMGA1UdJQQMMAoGCCsGAQUF
BwMCMB0GA1UdDgQWBBRXWazzjvqzu725TEJxtLVuA5o8ezANBgkqhkiG9w0BAQsFAAOCAQEAQxQh
Ajy3VHH/PVpQHYrzxrlmycb8t3zbPorRF+zIE0+LBOnELGW8CATpbBzifSQfAPvcgEuuHgdFU2nP
ObIBkwgAI1s3uQ+iedoYxjpgvPj0WpnEPwArw0709ZGKgvq3cvVnw0ZGV63H02pNO2YSEs40q8B/
EOkjl9D4apYPAhXCJid2nU+rf/EsK2o1LeV2ccadfWnumR6xBe/Vv6jDkj0kl6hbB1XA+AkYMAcN
iAq9XeLt5OY4MD/y+g6XNuLmyR9BdVYu9o4F2gaAPZDu+Ba8EuH1Q9w/wK8X2eoYoIYxYk1Z9PbA
IHO2YJHIfNf/TNeFL+nVp3ip7pjvI8Hrxw==
-----END CERTIFICATE-----
', '2026-02-09 14:27:00', 'V', NULL);

INSERT INTO ds_certdata (id, name, `issuerDN`, `subjectDN`, x509data, expires, status, `pkId`)
VALUES
(2, 'Issuer 1 ACS 1', ' ', 'CN=401600, OU=401600, O=Issuer 1, L=Tallinn, ST=HM, C=EE', '-----BEGIN CERTIFICATE-----
MIIDozCCAougAwIBAgIEZq3vrDANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJFRTELMAkGA1UE
CBMCSE0xEDAOBgNVBAcTB1RhbGxpbm4xGDAWBgNVBAoTD0NhcmQgU2NoZW1lIEluYzELMAkGA1UE
CxMCRFMxGDAWBgNVBAMTDzNEUyBTY2hlbWUgUm9vdDAeFw0xNjAyMTIxMjI2NDlaFw0yNjAyMDkx
MjI2NDlaMGExCzAJBgNVBAYTAkVFMQswCQYDVQQIEwJITTEQMA4GA1UEBxMHVGFsbGlubjERMA8G
A1UEChMISXNzdWVyIDExDzANBgNVBAsTBjQwMTYwMDEPMA0GA1UEAxMGNDAxNjAwMIIBIjANBgkq
hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAujAU6jSn1iHSreFNnYwBS7YA2R5jnjJ1ibipcf9MgZFZ
OwWMLWMW+uc/hpFWxCIlBqcsraGNvq722E0iw+xfbocL2Z/1de8kxlp1B93vVVnHTd1M36KUlDTe
l0sBT5M3XBcLMXqxAxMPxHb34exv3hKG1egLbMtBuQQV1As902ujjGwYD7FVb/Z+VcLTeduR9pDM
18R3pqSV64LIjAUOnZmZ/jZ6Krhf8d97IdKj/NdzU7dEFTJf4oSzhfSKxb8eO63o05dgsVPyuwET
7pPF7nYEXlUQ+sJW/KCu1/bG5eHgY2xVsL3wliVshWVLVS04AbWXvAXKR6P0B4G41BJdtwIDAQAB
o1cwVTAfBgNVHSMEGDAWgBQu0d/JJx1eFPVZP8MsNf4CMk77nTATBgNVHSUEDDAKBggrBgEFBQcD
AjAdBgNVHQ4EFgQUSKY3rlW7aykx/HYrsKgmYLSGjJkwDQYJKoZIhvcNAQELBQADggEBAI0bkr3B
0LH/wmqsGiLcsV6KkU4pBXI2S+nWPEsaJ44XmQQfrxlWOxVIwMYZC3DzzvGiEwr73FGQCjZMf4xZ
+olSFs4ztbEoBx07vvvHXBH8xAqOPlvVEp+li3RhWhSnvMyL6ALRTKOo9a/l15MwAXI4m0rptx53
xEgRc6Sdk7Tk5e5/c8MmFpB9JD4HAhSUdHZEXgJGtm6Vevjo1bed8Fo7DiIx3nFIm+7syDUiVy+t
RhJWO+2RPM0LwFJYvqHkI5+fJ6vhoMxW9ezLdWGaRbxtpu8GIDZ2mn+GCWniPhN0gTGKivnW4FI5
WoKs1VDFuPrqyCdmun+Fw7yLkZrfLeo=
-----END CERTIFICATE-----', '2026-02-09 14:26:00', 'V', NULL);

 
INSERT INTO ds_issuers (id, `BIN`, name, phone, email, address, city, country, status)
VALUES (1, '554433', 'Issuer 1', NULL, NULL, NULL, NULL, '233', 'A');

INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (1, '554433', 'MasterCard1', 2, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (2, '401600', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (3, '401610', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (4, '401611', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (5, '401612', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (6, '401613', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (7, '401614', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (8, '401615', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (9, '401616', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (10, '401617', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (11, '401618', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (12, '401619', 'VISA 1', 1, NULL, 1, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (13, '401620', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (14, '401621', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (15, '401622', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (16, '401623', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (17, '401624', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (18, '401625', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (19, '401626', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (20, '401627', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (21, '401628', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (22, '401629', 'VISA 2', 1, NULL, 2, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (23, '401630', 'VISA 3', 1, NULL, 3, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (24, '401631', 'VISA 3', 1, NULL, 3, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (25, '401632', 'VISA 3', 1, NULL, 3, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (26, '401633', 'VISA 3', 1, NULL, 3, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (27, '401634', 'VISA 3', 1, NULL, 3, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (28, '401635', 'VISA 3', 1, NULL, 3, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (29, '401636', 'VISA 3', 1, NULL, 3, 'P');
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (30, '401637', 'VISA 3', 1, NULL, 3, 'P'); 
INSERT INTO ds_card_ranges (id, bin, name, cardtype, subtype, `issuerId`, status) VALUES (31, '401638', 'VISA 3', 1, NULL, 3, 'N');
 
INSERT INTO ds_acsprofiles (id, `issuerId`, name, `URL`, `outClientCert`, `inClientCert`, status, `SSLProto`, `threeDSMethodURL`)
VALUES (1, 1, 'ACS 1', 'http://bank2.modirum.com:8080/dstests/ACSEmu2', NULL, 2, 'A', NULL, NULL);

INSERT INTO ds_acquirers (id, `MIProfileId`, `BIN`, name, phone, email, address, city, country, status)
VALUES (1, 1, '412345', 'Acquirer 1', NULL, NULL, NULL, NULL, NULL, 'A'); 