@rem Note you need to run this application with java that has
@rem installed the Java Cryptography Extension (JCE) Unlimited Strength
@rem jurisdiction Policy Files (local_policy.jar)
@rem for example this java has it..
@rem set java="C:\Program Files\IBM\WebSphere\AppServer\java\bin\java"
@set java=java -Xmx512M
@set LIBS=./classes
@set LIBS=%LIBS%;./lib/*

%java% -cp %LIBS% com.modirum.ds.tests.MessageBurster %*