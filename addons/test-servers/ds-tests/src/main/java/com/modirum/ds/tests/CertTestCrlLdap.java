/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 6. jaan 2016
 *
 */
package com.modirum.ds.tests;

import com.modirum.ds.services.KeyService;
import com.modirum.ds.hsm.KeyServiceBase;
import com.modirum.ds.utils.http.HttpsJSSEClient;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManagerFactory;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class CertTestCrlLdap {
    static Logger log = LoggerFactory.getLogger(CertTestCrlLdap.class);


    public static void main(String[] args) throws Exception {

	/*	String cert=
				"-----BEGIN CERTIFICATE-----\r\n"+
				"MIIEYDCCA0igAwIBAgILBAAAAAABL07hRQwwDQYJKoZIhvcNAQEFBQAwVzELMAkG\r\n"+
				"A1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExEDAOBgNVBAsTB1Jv\r\n"+
				"b3QgQ0ExGzAZBgNVBAMTEkdsb2JhbFNpZ24gUm9vdCBDQTAeFw0xMTA0MTMxMDAw\r\n"+
				"MDBaFw0yMjA0MTMxMDAwMDBaMF0xCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9i\r\n"+
				"YWxTaWduIG52LXNhMTMwMQYDVQQDEypHbG9iYWxTaWduIE9yZ2FuaXphdGlvbiBW\r\n"+
				"YWxpZGF0aW9uIENBIC0gRzIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB\r\n"+
				"AQDdNR3yIFQmGtDvpW+Bdllw3Of01AMkHyQOnSKf1Ccyeit87ovjYWI4F6+0S3qf\r\n"+
				"ZyEcLZVUunm6tsTyDSF0F2d04rFkCJlgePtnwkv3J41vNnbPMYzl8QbX3FcOW6zu\r\n"+
				"zi2rqqlwLwKGyLHQCAeV6irs0Z7kNlw7pja1Q4ur944+ABv/hVlrYgGNguhKujiz\r\n"+
				"4MP0bRmn6gXdhGfCZsckAnNate6kGdn8AM62pI3ffr1fsjqdhDFPyGMM5NgNUqN+\r\n"+
				"ARvUZ6UYKOsBp4I82Y4d5UcNuotZFKMfH0vq4idGhs6dOcRmQafiFSNrVkfB7cVT\r\n"+
				"5NSAH2v6gEaYsgmmD5W+ZoiTAgMBAAGjggElMIIBITAOBgNVHQ8BAf8EBAMCAQYw\r\n"+
				"EgYDVR0TAQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQUXUayjcRLdBy77fVztjq3OI91\r\n"+
				"nn4wRwYDVR0gBEAwPjA8BgRVHSAAMDQwMgYIKwYBBQUHAgEWJmh0dHBzOi8vd3d3\r\n"+
				"Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMDMGA1UdHwQsMCowKKAmoCSGImh0\r\n"+
				"dHA6Ly9jcmwuZ2xvYmFsc2lnbi5uZXQvcm9vdC5jcmwwPQYIKwYBBQUHAQEEMTAv\r\n"+
				"MC0GCCsGAQUFBzABhiFodHRwOi8vb2NzcC5nbG9iYWxzaWduLmNvbS9yb290cjEw\r\n"+
				"HwYDVR0jBBgwFoAUYHtmGkUNl8qJUC99BM00qP/8/UswDQYJKoZIhvcNAQEFBQAD\r\n"+
				"ggEBABvgiADHBREc/6stSEJSzSBo53xBjcEnxSxZZ6CaNduzUKcbYumlO/q2IQen\r\n"+
				"fPMOK25+Lk2TnLryhj5jiBDYW2FQEtuHrhm70t8ylgCoXtwtI7yw07VKoI5lkS/Z\r\n"+
				"9oL2dLLffCbvGSuXL+Ch7rkXIkg/pfcNYNUNUUflWP63n41edTzGQfDPgVRJEcYX\r\n"+
				"pOBWYdw9P91nbHZF2krqrhqkYE/Ho9aqp9nNgSvBZnWygI/1h01fwlr1kMbawb30\r\n"+
				"hag8IyrhFHvBN91i0ZJsumB9iOQct+R2UTjEqUdOqCsukNK1OFHrwZyKarXMsh3o\r\n"+
				"wFZUTKiL8IkyhtyTMr5NGvo1dbU=\r\n"+
				"-----END CERTIFICATE-----";
	*/
/*
		String cert2="-----BEGIN CERTIFICATE-----  MIIDpTCCAo2gAwIBAgIEfY8SpDANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJF        RTELMAkGA1UECBMCSE0xEDAOBgNVBAcTB1RhbGxpbm4xGDAWBgNVBAoTD0NhcmQg        U2NoZW1lIEluYzELMAkGA1UECxMCRFMxGDAWBgNVBAMTDzNEUyBTY2hlbWUgUm9v        dDAeFw0xNjAyMTIxMjI3MzZaFw0yNjAyMDkxMjI3MzZaMGMxCzAJBgNVBAYTAkVF        MQswCQYDVQQIEwJITTEQMA4GA1UEBxMHVGFsbGlubjETMBEGA1UEChMKTWVyY2hh        bnQgMTEPMA0GA1UECxMGNDEyMzQ1MQ8wDQYDVQQDEwY3NzExMjIwggEiMA0GCSqG        SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCF6YbnvWe1YACWVX+jPzDVkHgvvqYGH14t        z5ZUe7dhETliAtvqkS8rfCI5OFejrTTquGdH7BmAMu57dxsw77PBHNuSxhGjnRt0        T4bLdKVwSx9arHu5jcIpLoIovmbOoK6hP3yN0Z2cPTF3R88nG4hAULof3WJ18Tqn        gD1OQztlBpxZfQVTtDNv0V83fyrz5BWjse1wTW1MDmYuL1jAowZv9XOBqpu7PBEK       wLA979Yi63LqDOkBFyKbJ9RmhWK1YYq65daYIoyPWXgeQr0D2xOKqp5nvWKV3EuW N1pqIL6dCJbIGHEv7qj2szUlCk4rgL5tfKPCefIfpBJcz1yAhmyRAgMBAAGjVzBV       MB8GA1UdIwQYMBaAFC7R38knHV4U9Vk/wyw1/gIyTvudMBMGA1UdJQQMMAoGCCsG AQUFBwMCMB0GA1UdDgQWBBRXWazzjvqzu725TEJxtLVuA5o8ezANBgkqhkiG9w0B       AQsFAAOCAQEAtY+TT5AiGXQbkkqcxoxEKumWWSKF8A+nGvy9Vus5Xo0UrAOlvM+G +pokzdLgAnjoc3asnM/fvzGe09YR60n6/CtFgSfhoYgiop6QQ8Ka8S5kGKR7fdUV       tFWaYb+L8fv+rM/muNmF3pP3s28Au2EHfWbfl4T7/MtSWFYJsYXmFvBOZHcUeD+W Lya0kLxpY+zDPPO3/vkpfwmt8T/oBcf53Gskd1y915Q4gq0b+kjsvkdgOBTai5i/       cQCdl6FujbdUXJD1jbcHNLobULwoAWxFdvlYuMn3QcS/cSQfb28+Y+dPRce6+klf ZKR9k0Ngpv6huuPQvHMwZU8AQCMiQMwt7g==    -----END CERTIFICATE-----";
		cert2=Misc.replace(cert2, "-----BEGIN CERTIFICATE----- ","");
		cert2=Misc.replace(cert2, " -----END CERTIFICATE-----","");
			X509Certificate[] certs=KeyService.parseX509Certificates(cert.getBytes(), null);

			System.out.println("SDN="+certs[0].getSubjectDN());
			System.out.println("SDN.name="+certs[0].getSubjectDN().getName());
			System.out.println("SDN.principal="+certs[0].getSubjectX500Principal());
			System.out.println("SDN.principal.name="+certs[0].getSubjectX500Principal().getName());

			X509Certificate[] certs2=KeyService.parseX509Certificates(Base64.decode(cert2), "X509");

			System.out.println("SDN="+certs2[0].getSubjectDN());
			System.out.println("SDN.name="+certs2[0].getSubjectDN().getName());
			System.out.println("SDN.principal="+certs2[0].getSubjectX500Principal());
			System.out.println("SDN.principal.name="+certs2[0].getSubjectX500Principal().getName());

			String subject="CN=Scheme Inc 3DS2 CA 2016-4, OU=3DS2 CA, O=Scheme Inc, L=Tallinn, ST=HM, C=EE";
			String flip=KeyService.flipX509SubjectV3(subject).toString();
			System.out.println("SO="+subject);
			System.out.println("SF="+flip);
			System.out.println("SEN="+new String (certs2[0].getSubjectX500Principal().getEncoded(), StandardCharsets.UTF_8));
			System.out.println("S_N="+certs2[0].getSubjectX500Principal().getName());
			System.out.println("S_N="+certs2[0].getSubjectDN().getName());
			System.out.println("SDN="+certs2[0].getSubjectDN());
			System.out.println("IDN="+certs2[0].getIssuerDN());
			org.bouncycastle.asn1.x500.X500Name xissuer = org.bouncycastle.asn1.x500.X500Name.getInstance(certs2[0].getIssuerX500Principal().getEncoded());
					//org.bouncycastle.asn1.x500.style.BCStyle.INSTANCE, certs2[0].getIssuerX500Principal().getName());
			System.out.println("XI2="+xissuer.toString());
			System.out.println("XIE="+new String (xissuer.getEncoded(), StandardCharsets.UTF_8));
			//KeyStore ks=KeyService.loadKeyStore("m:/certs/keystore20170420", "VAEJeHjNqkDuhz@4IO86W0oXGgrRKtTP", "JCEKS", "SunJCE");
			//RSAPrivateKey pk=(RSAPrivateKey)ks.getKey("bank1mpi2", "VAEJeHjNqkDuhz@4IO86W0oXGgrRKtTP".toCharArray());
			//System.out.println(""+Base64.encode(pk.getEncoded())); */

        String cacertStr = "-----BEGIN CERTIFICATE-----\n" +
                           "MIID7zCCAtegAwIBAgIFIsVyByYwDQYJKoZIhvcNAQELBQAwdTELMAkGA1UEBhMCRUUxCzAJBgNV\n" +
                           "BAgTAkhNMRAwDgYDVQQHEwdUYWxsaW5uMRMwEQYDVQQKEwpTY2hlbWUgSW5jMRAwDgYDVQQLEwdS\n" +
                           "b290IENBMSAwHgYDVQQDExdTY2hlbWUgSW5jIFJvb3QgQ0EgMjAyNzAeFw0xNzA0MjgyMTI1MjVa\n" +
                           "Fw0yNzA0MjYyMzU5NTlaMGUxCzAJBgNVBAYTAkVFMQswCQYDVQQIEwJITTEMMAoGA1UEBxMDVExO\n" +
                           "MRMwEQYDVQQKEwpTY2hlbWUgSW5jMQswCQYDVQQLEwJDQTEZMBcGA1UEAxMQSXNzdWVyIENBIDIw\n" +
                           "MjctMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJw7QqPW0dgQgVLMIAFsLooW7bSP\n" +
                           "DP0JInRkIjgZs/1yoCnyWYnnVzcJEqSZpX4MpiF7Ixc3T533zi303rGZdB8WvJNVhU5tMjdTajQZ\n" +
                           "oWKBCVbulTuD1vHm3CpyF3wwiE9IJhjfPgjhOvYt9G43C+b59i0/9j8wbnRTSSWZmeGJn7yFoCVX\n" +
                           "V02Sz4X795eRz/dJlqU5wglThjUl8p3aCjRvJxVjh0T4/zI+AVlzx3wycX8fHZopDuCwJl92siqR\n" +
                           "/qj5unaGcfxtQHhSyWJMH5QVIJRCs/7wf53X8wHITne9e6XK1z3juTVCCGpl3yKkaDL7SxN3BQiM\n" +
                           "KCRfVFmwhZ0CAwEAAaOBlTCBkjAPBgNVHRMECDAGAQH/AgEAMAsGA1UdDwQEAwIBBjAdBgNVHQ4E\n" +
                           "FgQUFmovZoQHXUcCLSnrflriTMqpup8wHwYDVR0jBBgwFoAUS8OlfzOZuKzB1sHTNaRJsBwho3Iw\n" +
                           "MgYDVR0fBCswKTAnoCWgI4YhaHR0cDovL2RzeC5tb2RpcnVtLmNvbS9kc3Jvb3QuY3JsMA0GCSqG\n" +
                           "SIb3DQEBCwUAA4IBAQBE+tFAJAHT7Bl5TFwOOZmx6JyrfZjqG0Z5EKH0ILuoatXpTGREsqnjhSkL\n" +
                           "oRxxt44AnVjKtyWnb+d1B4DQPGAn545gDNx2q+UkQ+6ROhW4okHwFDF0xvaskCqKURU6RxuBxoeu\n" +
                           "1YfyaUxTthUxHc42hUKs6feSqNB6tO2b7SVFDfsai6wKvmXl/zVicQtDklTyRHBQZ/icLqVQQ8Fc\n" +
                           "+SXG0IHy5R2AavL7/2IXJM1JDn4gkzTsq9+9PHzzdmIsFsJg63zo36oV5pIpcJhbyaTfPvVWsjjq\n" +
                           "29U10FkwbPAzLgPkxMFZZctD4MqNp6V6IMakQbSi4upFLxvFcxTzUIpt\n" + "-----END CERTIFICATE-----";


        String endCetStr = "-----BEGIN CERTIFICATE-----\n" +
                           "MIIEBDCCAuygAwIBAgIFIsVyR4MwDQYJKoZIhvcNAQELBQAwZTELMAkGA1UEBhMCRUUxCzAJBgNV\n" +
                           "BAgTAkhNMQwwCgYDVQQHEwNUTE4xEzARBgNVBAoTClNjaGVtZSBJbmMxCzAJBgNVBAsTAkNBMRkw\n" +
                           "FwYDVQQDExBJc3N1ZXIgQ0EgMjAyNy0zMB4XDTE3MDQyODIxMjgxMFoXDTIwMDQyOTIwNTk1OVow\n" +
                           "cjELMAkGA1UEBhMCRUUxCzAJBgNVBAgMAkhNMRAwDgYDVQQHDAdUYWxsaW5uMRMwEQYDVQQKDApT\n" +
                           "Y2hlbWUgSW5jMRUwEwYDVQQLDAxBQ1MgU2VydmVyIDIxGDAWBgNVBAMMD2RzeC5tb2RpcnVtLmNv\n" +
                           "bTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMEnAcVdjae8RC/+lLDEcUjP7prLHGL2\n" +
                           "WSXSxZmRfU6IaY9nxK+us+waKSxrEDv2dZoPMCbItwkUY6J3EKKtEgzgxUnjOK7GO2Q3l0x151O6\n" +
                           "m1yQ21HyByKgGXf9JgI85u+15HujPVoiYjQhFaO1ZrHMNxqvfTcGzL4XTLVaoPnzSklPPFxR1RZC\n" +
                           "Lv0YDYhnk5dfbu3agdJ0lryvQIVeM29DNV3apYOY0/OLiG/vVH0T0smNKt3DDWnpYALJjG0IT9Jm\n" +
                           "trZW//rCPcm0hpn4djjM6Zf2Txy+wi84B08mv9WhkoQWdHVOeCIVrwhAcmDYc7pSSqo5dA6TdNbv\n" +
                           "QIoWGk0CAwEAAaOBrTCBqjAJBgNVHRMEAjAAMAsGA1UdDwQEAwIEsDAdBgNVHSUEFjAUBggrBgEF\n" +
                           "BQcDAgYIKwYBBQUHAwEwHQYDVR0OBBYEFKsSJ8ezn8A0DTcERYEzl/rEAMRiMB8GA1UdIwQYMBaA\n" +
                           "FBZqL2aEB11HAi0p635a4kzKqbqfMDEGA1UdHwQqMCgwJqAkoCKGIGh0dHA6Ly9kc3gubW9kaXJ1\n" +
                           "bS5jb20vZHNjcmwuY3JsMA0GCSqGSIb3DQEBCwUAA4IBAQB+0/MYcgy9MKN4xHacfJc8KYDknnje\n" +
                           "fpG/ka8eQo3hSZi+9GNxnRnBg0HN4vjYTnTKVKp9ri0XP1zMoGa6i6xSPqNKdrAoaXpd2KcxSfyE\n" +
                           "+WetW8hoPQcOysk4odk8A1LUGWHQK26xg8gNAybaCYFbikIpsluHThCA11wk/S2oO8+AXHabDnzr\n" +
                           "hy6AivPUVW3ozAgIkpmnhjr6vCsOjIbAlZePbbS2+j98VuH8iElzOJFZs9lO49jvO1/QRNeyaFOf\n" +
                           "TgFgKXToTXpCjJLANldMhwfjPPGqSvwkIfBHbjOI751Bf7Lc5AOv2k4FQNnGzHTv/bdOcBTRXm7r\n" +
                           "vlXCTs/m\n" + "-----END CERTIFICATE-----";

        X509Certificate cacert = KeyService.parseX509Certificates(cacertStr.getBytes(), null)[0];
        X509Certificate[] endcerts = KeyService.parseX509Certificates(endCetStr.getBytes(), null);
        X509Certificate endcert = endcerts[0];

        // NOt relevant System.setProperty("com.sun.security.enableCRLDP", "false");
        //TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        //rustManager tm=tmf.getTrustManagers()[0];
        // root
        String trustedCerts = "-----BEGIN CERTIFICATE-----\n" +
                              "MIIDtjCCAp6gAwIBAgIFIsVYDqQwDQYJKoZIhvcNAQELBQAwdTELMAkGA1UEBhMCRUUxCzAJBgNV\n" +
                              "BAgTAkhNMRAwDgYDVQQHEwdUYWxsaW5uMRMwEQYDVQQKEwpTY2hlbWUgSW5jMRAwDgYDVQQLEwdS\n" +
                              "b290IENBMSAwHgYDVQQDExdTY2hlbWUgSW5jIFJvb3QgQ0EgMjAyNzAeFw0xNzA0MjgxNjQxNDVa\n" +
                              "Fw0yNzA0MjYyMzU5NTlaMHUxCzAJBgNVBAYTAkVFMQswCQYDVQQIEwJITTEQMA4GA1UEBxMHVGFs\n" +
                              "bGlubjETMBEGA1UEChMKU2NoZW1lIEluYzEQMA4GA1UECxMHUm9vdCBDQTEgMB4GA1UEAxMXU2No\n" +
                              "ZW1lIEluYyBSb290IENBIDIwMjcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCQ8gSu\n" +
                              "ReMXMyyhKF24nhzGmq4+EFI+r78lRatbyQkDLysYk0ROSz3qYSyK05ku4F4OlG69CWQaXNHx2w2m\n" +
                              "RcRwP8bJoy8mBkVJmAjvmKdtL8gbJWYMVE5rMRdZZltNIpQujjKmpFrHMbJNdmM7Et1Z1sCT5xgT\n" +
                              "z+53lCe9h5dNNF5xiv5jrZn5OQTOP58fRePExBXUV1hPEdRnPTrTVS03ARQ2/K9tTdpxuXIrFxo9\n" +
                              "TvfoijHgTdDNCaGPGHqB0uu3lGEFGBllboe0Si88SFjGtGXPRytVCmgHRhf+Vxf08a9ekGtUjVUQ\n" +
                              "+45vrS0IWNvpOpdbaFydt7anNdccTHrjAgMBAAGjTTBLMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQD\n" +
                              "AgH2MA8GA1UdJQQIMAYGBFUdJQAwHQYDVR0OBBYEFEvDpX8zmbiswdbB0zWkSbAcIaNyMA0GCSqG\n" +
                              "SIb3DQEBCwUAA4IBAQAdS7mGMQIw3fDWPkovvkmrQjchMs67+JGqnUbMP9lqDMXDIN7YrdJe6Cq1\n" +
                              "vIPvvSCYRF645GvSIXlZiO0R9b5nNtF66mBTk4Q5RaCdISoWTlBB9wJ5KKlih7BUNTbSBQ/XPp8t\n" +
                              "SEvtnG6HumDv+NlPVtFtTw23Y3zdLrrDi0I1RlJ5KbH1yKJRwHTI6wy60YmHxJtkn4VJ4iJtglzJ\n" +
                              "Wl0fC95PjdORYt4cWyNG7Y7J30T7n//lIr6U9kS1HYWWgui+wfUIk2YsolhW2+cydgqUGietPuG+\n" +
                              "C/7Gsnks52FK8nO+BKldiXSUaslMgCmSR7BJeqWj/KG3DEIstlDYwEA4\n" +
                              "-----END CERTIFICATE-----";

        trustedCerts =
                "-----BEGIN CERTIFICATE-----\n" + "MIIDojCCAoqgAwIBAgIQE4Y1TR0/BvLB+WUF1ZAcYjANBgkqhkiG9w0BAQUFADBr\n" +
                "MQswCQYDVQQGEwJVUzENMAsGA1UEChMEVklTQTEvMC0GA1UECxMmVmlzYSBJbnRl\n" +
                "cm5hdGlvbmFsIFNlcnZpY2UgQXNzb2NpYXRpb24xHDAaBgNVBAMTE1Zpc2EgZUNv\n" +
                "bW1lcmNlIFJvb3QwHhcNMDIwNjI2MDIxODM2WhcNMjIwNjI0MDAxNjEyWjBrMQsw\n" +
                "CQYDVQQGEwJVUzENMAsGA1UEChMEVklTQTEvMC0GA1UECxMmVmlzYSBJbnRlcm5h\n" +
                "dGlvbmFsIFNlcnZpY2UgQXNzb2NpYXRpb24xHDAaBgNVBAMTE1Zpc2EgZUNvbW1l\n" +
                "cmNlIFJvb3QwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvV95WHm6h\n" +
                "2mCxlCfLF9sHP4CFT8icttD0b0/Pmdjh28JIXDqsOTPHH2qLJj0rNfVIsZHBAk4E\n" +
                "lpF7sDPwsRROEW+1QK8bRaVK7362rPKgH1g/EkZgPI2h4H3PVz4zHvtH8aoVlwdV\n" +
                "ZqW1LS7YgFmypw23RuwhY/81q6UCzyr0TP579ZRdhE2o8mCP2w4lPJ9zcc+U30rq\n" +
                "299yOIzzlr3xF7zSujtFWsan9sYXiwGd/BmoKoMWuDpI/k4+oKsGGelT84ATB+0t\n" +
                "vz8KPFUgOSwsAGl0lUq8ILKpeeUYiZGo3BxN77t+Nwtd/jmliFKMAGzsGHxBvfaL\n" +
                "dXe6YJ2E5/4tAgMBAAGjQjBAMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQD\n" +
                "AgEGMB0GA1UdDgQWBBQVOIMPPyw/cDMezUb+B4wg4NfDtzANBgkqhkiG9w0BAQUF\n" +
                "AAOCAQEAX/FBfXxcCLkr4NWSR/pnXKUTwwMhmytMiUbPWU3J/qVAtmPN3XEolWcR\n" +
                "zCSs00Rsca4BIGsDoo8Ytyk6feUWYFN4PMCvFYP3j1IzJL1kk5fui/fbGKhtcbP3\n" +
                "LBfQdCVp9/5rPJS+TUtBjE7ic9DjkCJzQ83z7+pzzkWKsKZJ/0x9nXGIxHYdkFsd\n" +
                "7v3M9+79YKWxehZx0RbQfBI8bGmX265fOZpwLwU8GUYEmSA20GBuYQa7FkKMcPcw\n" +
                "++DbZqMAAb3mLNqRX6BGi01qnD093QVG/na/oAo85ADmJ7f/hC3euiInlhBx6yLt\n" +
                "398znM/jra6O1I7mT1GvFpLgXPYHDw==\n" + "-----END CERTIFICATE-----\n";

        //String ldap="ldap://Enroll.visaca.com:389/cn=Visa_eCommerce_Issuing_CA,c=US,ou=Visa_International_Service_Association,o=VISA?certificateRevocationList";
        //java.net.URI.create(ldap);


        System.setProperty("java.security.debug", "certpath ocsp");
        System.setProperty("com.sun.net.ssl.checkRevocation", "true");
        //String url="https://dsx.modirum.com:4443/jvm.jsp";
        String url = "https://dswc.visa3dsecure.com/DSMsgServlet";
        //String url="https://google.ee/";
        KeyStore trustStore = KeyStore.getInstance("JKS");
        trustStore.load(null, null);
        X509Certificate[] certs3 = KeyServiceBase.parseX509PemHeadersSafeCertificates(trustedCerts.getBytes(), "X509");
        for (X509Certificate c : certs3) {
            trustStore.setCertificateEntry("c" + c.getSerialNumber(), c);
        }

        //		String pk="48, -126, 4, -66, 2, 1, 0, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 4, -126, 4, -88, 48, -126, 4, -92, 2, 1, 0, 2, -126, 1, 1, 0, -90, 9, 41, 109, -75, -36, -22, 4, 123, -26, -125, 104, 36, -54, 126, 78, 77, 126, 89, -30, 33, -48, -38, -97, -90, 106, -98, 72, 127, -102, -26, -18, -88, 116, -24, 95, 90, 56, 61, -102, 34, -91, 4, -87, -101, 75, 45, -52, 54, 100, -97, 23, -53, -93, -44, 124, -88, 33, -62, -16, -54, 104, -5, -45, -30, 33, -128, -45, 123, 26, -28, 110, 25, -14, 60, -84, 87, 80, -113, -116, -35, 22, -38, 7, 115, -68, -76, -66, -59, 7, 111, -39, 106, 114, 53, 68, -29, -117, 120, -127, 37, 97, -56, -12, 28, 65, 85, 113, -53, -30, -21, 7, -16, 63, 18, -56, 110, -4, 110, -75, 100, 122, -13, -55, 113, 80, -8, -125, 26, -47, 15, -30, -70, -127, 106, -36, 87, 123, 17, -15, 45, 90, 31, -59, 88, -14, -69, -21, 34, 57, -109, -86, 63, 91, -26, -72, -45, 85, 16, -92, -40, 5, 65, -104, -33, 110, 37, -40, 115, -107, -78, 93, -89, -6, -79, 113, 13, 117, -121, 74, -7, 41, 114, -74, -114, 117, 85, -126, -38, -87, 111, 36, -116, 55, 112, -18, -1, 118, 122, -124, -70, -61, 112, -117, 95, -53, -110, -23, -17, -19, -33, -50, -94, 13, 96, -114, -23, -47, -9, 62, -26, 76, -59, -112, -9, 126, -125, 9, -126, -50, -128, 98, 119, -18, -83, 100, -119, 117, 67, 49, 7, -32, -47, 26, -28, -24, -75, -49, 43, -53, -75, 121, -71, 58, -66, -63, 2, 3, 1, 0, 1, 2, -126, 1, 0, 11, 31, -36, 5, -68, -60, -93, 109, 79, 123, -33, 92, 15, -14, -3, 73, 115, -55, -66, 109, -9, 68, 14, 17, -38, 77, -108, 115, 89, 17, 96, -35, -90, -83, -58, -113, -12, -12, -123, -76, 25, 20, 35, 94, -43, -57, -48, -74, -71, 2, 62, -30, -110, 67, 84, -50, 81, 89, -119, -67, 38, 98, -88, 84, -58, -10, -27, -79, -59, -78, -60, -111, 87, -19, -78, 0, -2, 12, -88, -63, -112, 98, -94, -109, 19, 63, 22, -127, 29, -92, 100, -21, 21, 69, -73, -87, -8, -16, -12, 78, -6, -39, 53, 100, -99, -84, 6, -12, -117, -21, -107, 65, -65, -24, 9, -13, 33, -12, -119, -38, 95, 116, 25, 119, 106, -65, 57, 23, 59, -86, -25, 13, -49, 6, 6, 83, 88, -122, 79, 6, -101, 40, 87, -76, -29, -79, -35, 123, 42, -35, -56, 111, -89, -95, 39, 30, 107, -19, 37, 99, 69, 69, 120, 80, 85, 116, 40, -12, 86, 20, -25, -92, -75, 67, -98, 22, -38, 29, -66, 45, 33, -125, -78, 62, -19, 13, -99, -37, 91, 27, -120, 82, -45, 16, 19, 7, -101, 89, 75, -75, 94, -71, 2, -15, -79, 65, 106, 79, -27, -86, -103, 20, -7, -62, 62, 25, -110, 85, 102, 104, 62, 43, 90, 13, 0, -56, -79, -127, 115, 112, -102, 27, 121, -66, -42, -24, 69, -98, 59, -93, 81, -37, -97, -77, 123, 125, 78, 49, 9, 120, 108, 41, -90, 12, -106, 29, 2, -127, -127, 0, -14, -78, -80, 32, -125, 27, 43, -78, 85, 21, -97, -92, -72, -27, 100, 13, -22, 29, -66, -58, 63, 57, -55, -107, -43, -89, -73, 54, 79, 82, 72, 84, -63, 124, -71, 75, -8, -75, -68, 24, 78, -70, -91, 108, 117, -1, 52, -97, -85, -111, -13, -74, 5, -68, -125, -124, 119, -114, 65, 37, -99, 20, 16, 69, -16, -124, 54, -119, 55, -75, 56, 121, 49, -11, 36, 43, -61, -40, -42, -49, -9, 67, 92, -33, 79, 101, 40, 7, -34, -50, 112, -100, -79, 97, -107, -110, -4, -6, -122, -31, -49, 125, 6, -36, 124, 92, -18, -93, -115, -13, -31, 23, -93, 69, 47, -72, 76, -102, -96, 59, -52, -9, -7, -26, 94, -62, -107, -49, 2, -127, -127, 0, -81, 34, -46, 56, 114, 75, 14, -34, -124, -87, 55, -15, -70, 77, -68, -34, 120, -92, 16, 97, -84, -86, 92, 84, -29, -37, 22, 74, 79, -112, -85, 65, 46, -51, 60, 58, -101, -73, 101, -18, -101, -103, -45, -29, 83, 49, -21, -112, 3, -48, -115, -92, 122, 28, 111, 63, 48, -108, -16, 71, 7, -87, 87, -68, 107, -41, 64, -74, 114, 104, 103, 96, -110, 13, -80, -26, -37, 35, -24, 100, 93, 97, -90, 89, 52, -8, 25, -3, 19, -47, -94, -93, -110, -125, -5, 83, 87, 74, 78, -17, 38, -49, 65, -86, -102, -74, 126, -69, -106, -114, -117, 88, 24, -106, 45, 2, -113, -35, 29, -118, -2, -87, 109, 114, -79, 68, 22, 111, 2, -127, -127, 0, -83, 68, 44, -63, -55, 87, -24, -122, 84, -128, 47, -115, 38, 3, 126, -65, 0, 89, 100, 13, -100, -83, -32, -32, -5, -69, -44, -19, 13, 12, 45, 95, 117, 19, 90, 38, -122, -43, 24, 8, 50, -116, -59, -12, -4, -8, -55, -43, 4, 100, 26, -99, -28, -46, -123, 2, 89, 104, 81, -46, 76, -90, 58, 71, -12, 66, 47, -67, 30, -20, 93, -7, 8, 85, 127, 17, 79, -84, -53, 15, -123, -13, -83, -90, 47, -60, -124, 35, -70, 73, -97, -45, -2, 63, 48, -51, -45, -107, -8, 29, -42, 71, -24, 82, 24, -25, 47, -25, -52, 60, 68, -126, -33, -87, 81, 60, 62, 84, 85, -8, -37, -120, 93, -4, -67, -62, 37, -71, 2, -127, -128, 69, -21, 44, -22, 4, 23, 27, 20, -110, -7, -53, -81, -31, -92, -17, 102, 17, -37, -51, -2, 112, -92, -126, -64, 17, 71, -126, 26, -96, -79, 44, -19, -108, -11, -78, -71, -29, 56, -48, -92, -11, -47, -17, -39, -83, 121, 28, -122, 30, 63, -64, -116, -115, -125, -108, -106, 49, -101, 4, 98, 23, 24, -16, 65, -12, -5, -74, -122, 32, 30, -74, 70, -61, -120, 91, -69, -114, 121, 67, 105, -18, 45, -15, -17, 99, 74, 57, 117, -29, -41, -87, -118, -116, 15, -52, -30, 116, -46, -31, -72, 112, 82, 5, -15, 74, 90, -39, 53, -94, -31, -67, -105, -86, 25, 42, 86, -1, -128, 83, 47, 14, -124, 45, -127, -64, 107, 98, 89, 2, -127, -127, 0, -15, -90, 14, -5, 71, 72, -86, 11, 75, 16, -48, 102, -97, 102, 83, 123, -20, -64, 122, 14, -107, -68, -59, 78, 124, 45, -41, -15, 22, -79, 61, -80, 107, -33, -2, 10, -109, 27, -93, -60, -84, -15, -103, 27, 117, -47, 73, 11, -108, 123, 111, 78, 2, 45, 3, -107, 12, -38, 55, 114, -121, 115, -34, 46, -77, 96, -3, -51, 74, 12, 65, 104, -126, -117, 85, 45, -72, 118, 31, 28, -127, -1, -75, 111, 23, -80, 56, -33, 71, -115, 43, 67, 79, -124, 117, 44, -30, -84, 20, 121, -105, 29, -3, -30, 30, 57, -104, 89, -60, 28, -12, -104, 13, -43, -29, -111, 3, -126, -4, 44, -74, -85, -84, 118, 98, 86, -18, -14";
        //String pk="48, -126, 4, -67, 2, 1, 0, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 4, -126, 4, -89, 48, -126, 4, -93, 2, 1, 0, 2, -126, 1, 1, 0, -47, -19, -82, -9, 74, -66, 115, -36, 109, 52, -44, -7, -45, -53, 53, 79, -27, -106, -37, 55, 55, -58, 63, -24, 8, 95, 96, 58, -94, -3, 4, -63, -105, -125, -26, 72, -26, -56, 100, -73, 71, -13, -81, 21, 91, -123, -41, 59, -79, 32, 75, 82, 33, -72, 103, -127, 7, 104, 59, 23, -33, 11, 41, -89, 82, -68, -87, -114, -103, 72, 67, -10, 22, -45, -127, -128, -58, 75, -109, -83, 81, 22, 66, -128, 89, -83, 80, 66, 36, -36, 103, 85, 57, 81, -122, 127, -121, 0, -75, -62, -28, -91, 124, -107, 116, -58, 79, 84, -56, -88, 108, -41, -123, -14, -62, 98, -88, -15, 47, -18, 46, 16, -101, 30, 113, -95, 65, -12, -6, 17, -124, -23, 66, -90, -125, 24, -44, 122, -21, 56, -69, 23, -33, -88, 99, 4, -53, -60, 17, -27, -102, -9, 113, 5, 48, 82, 63, -92, -98, 103, 8, -106, 104, 75, -70, 39, -46, -87, 45, 103, 24, 38, -85, 93, -22, 11, 116, -123, -73, 88, -110, -99, -91, -78, 51, -86, 92, 56, -38, 95, -37, -6, 35, 118, -90, -68, 1, -99, -90, 89, 41, 43, 103, 110, -20, -28, 56, -73, 46, 90, -58, 86, -104, 20, 52, -43, -106, 49, -50, 47, 17, 95, 62, 28, 37, 20, -16, 61, 38, -42, -91, -16, -122, -103, 120, 29, 74, -43, 59, -66, -56, -64, -29, -3, 77, -124, -104, -118, -16, 6, -15, 105, 36, -5, 21, -83, 2, 3, 1, 0, 1, 2, -126, 1, 0, 2, 17, -69, -75, -56, 80, -44, 71, -119, -12, 9, 93, 101, 15, 13, -71, 121, -29, -32, -55, 76, -115, 113, -73, -47, -51, 78, -33, -123, 23, -59, 98, 59, 34, 27, -75, 0, 65, 93, 108, -55, 18, -57, -39, 8, 11, -21, -79, 52, -17, -62, -114, 78, -78, -56, -94, 102, -10, 74, 14, -44, 46, 1, -3, 26, 61, -83, -42, 48, 0, -100, 90, -30, -94, -73, 22, 19, -34, 78, -40, 88, -126, 69, -87, -118, 56, -63, 31, -59, -55, 62, 99, -126, -85, 33, 14, 68, -64, 103, 10, 66, -62, -74, -43, -86, 79, 2, 29, 45, -43, 43, 45, 124, -82, -123, -25, -1, 14, 81, -112, -95, 119, -100, 111, -46, 98, -22, -119, -127, 19, -125, -100, -66, -85, -107, 53, 81, 29, -71, 111, 7, 107, 49, -122, 103, -9, -45, -43, 15, 77, -11, -28, -2, 34, 122, -67, -18, 22, -124, 37, -48, -106, -37, -92, 101, 28, -102, 2, 121, -34, -62, 87, 19, 94, 15, -39, -128, -75, -56, -37, -35, 3, 74, -39, 45, -103, -18, 83, -65, 1, 28, -90, 94, 0, 102, -98, -72, 56, -14, -18, 116, -106, 127, 104, 59, -48, 94, -23, 75, -111, 0, 95, 26, -117, -10, 123, -29, 7, -48, 45, -32, -50, -76, -97, 71, -40, -73, -29, -116, 52, -74, 118, 102, -36, 122, 93, 37, 76, -30, -11, -2, -90, 45, -45, -75, -19, 66, 55, 123, 28, 92, 119, -18, -4, -125, 41, 2, -127, -127, 0, -23, -101, -83, 124, 4, 93, -116, 68, -46, 42, 52, -54, 71, 31, 99, -104, 92, 74, 76, -25, 94, 48, 73, 37, 89, 7, 64, 70, -13, 44, 121, -60, -40, 113, -123, 41, 122, -69, 103, -114, 113, -56, -92, 101, -126, 113, -91, -8, 28, -73, 65, -41, 105, 79, 50, 16, -64, 34, 117, 107, -70, 71, 64, -68, -50, 35, -48, -88, -41, 20, 43, 113, 113, 82, -59, -9, 0, -34, -126, 69, 121, 84, 81, -2, 15, -66, 18, -79, -93, -35, -11, -37, -8, -64, -45, -124, 34, -58, -33, 122, 29, 71, 5, 104, 56, 55, 54, 78, -14, 44, 11, -120, 71, 78, -121, -71, 86, 0, 19, 89, 57, 23, 34, -117, 33, -50, -44, -77, 2, -127, -127, 0, -26, 12, -13, 16, 102, -109, -125, -116, -116, -102, 52, 38, -56, -81, -4, -98, 24, -35, 114, -97, 57, 63, -117, 76, -18, 49, -26, 94, -5, 115, 75, 86, -102, 20, -15, -78, 50, 88, -13, -54, -69, 33, 36, -112, 34, -20, -109, -127, 73, -46, -105, -71, 124, 17, 122, -109, -52, -90, -41, 19, -19, -46, -1, 79, 77, -30, -42, 108, 100, -122, -91, 37, -70, -93, -121, -48, -1, -104, -41, -35, 78, -86, -118, 127, 1, 45, 77, -54, -108, 33, 111, 8, -1, 6, 120, 18, -85, -22, 78, 122, 21, 88, -84, -59, -102, 31, 50, 26, -112, -21, -87, 32, 119, -35, -10, 17, 87, 8, -46, -110, -84, -78, 39, -124, -85, 5, 92, 31, 2, -127, -128, 10, 59, -42, -86, -61, 42, 49, -11, -27, -37, 17, 122, -9, 8, -87, -3, 71, -75, -67, -114, -71, 50, -106, 81, -69, -121, -76, -54, 38, -25, 0, -30, -99, -76, -61, 103, -96, -57, -54, -18, -109, -113, 125, -100, 48, -51, -73, -66, -103, -53, 113, 103, -11, 102, 17, -60, 104, 22, 90, 92, -128, -55, -108, 77, 113, 30, 98, -118, -78, -7, 96, 57, -43, 4, 18, 70, 18, -44, -84, -110, 76, -111, 100, 18, 36, 48, 104, 115, 61, -63, -128, 126, -59, 115, -11, 16, 41, -47, -38, -126, 103, -5, -17, -127, -76, 76, -66, -27, 80, 120, -101, 124, -71, -38, -9, -39, 39, -86, 83, 46, -86, 112, -104, 3, -50, 84, 88, 63, 2, -127, -127, 0, -126, -80, -110, -103, 27, -100, -21, -37, -71, -72, -108, -83, -1, -50, -78, -107, 98, 12, -45, -28, 14, -110, -63, -31, 55, 100, 83, -70, 91, -32, -8, -56, -65, 9, -20, -16, -51, -41, 104, -48, 32, 20, -56, -1, -75, 71, 110, -40, -63, 6, -119, 57, 79, 121, 98, -120, 122, 119, -63, -79, -55, -90, -41, 113, -57, 14, 121, -65, -28, 47, -11, -68, 4, 86, -120, 126, -69, -27, -87, -92, 72, -33, 107, -124, -111, 55, -102, 124, -34, 64, 76, 34, -15, 54, 76, -102, 86, -35, -22, 45, 66, -39, 56, 117, -99, -125, -31, 99, -28, 54, 108, -61, 76, 53, 101, 104, 30, 104, -99, -81, -61, -70, 5, -25, 65, 124, -17, 3, 2, -127, -128, 51, -108, 55, 83, -100, 54, -13, 72, -102, 13, -66, -51, 78, -121, -96, 33, -55, -85, -122, 120, 68, 55, -40, 106, 104, -74, -25, -56, 113, -105, -65, 72, 79, -109, -6, 79, 104, 118, 41, -24, 62, -105, 27, 52, 17, -62, -87, -73, -62, -118, 60, -85, -71, -95, -81, -46, 16, -99, -96, -81, -11, 35, -68, -44, -98, 104, 80, -74, -45, 44, -41, -79, 118, -100, -111, 120, -119, 101, 65, -77, -121, 21, -7, -79, 106, 22, -84, 21, 33, -52, -42, 0, 61, -68, -17, 109, -62, -46, -70, 25, -124, -56, 91, 110, 77, -32, 98, 14, 105, 126, -47, -108, -59, 127, -14, 16, -69, 88, 78, -112, 18, -3, 2, -122, 98, -100, -78, 107";
        String pk = "48, -126, 4, -66, 2, 1, 0, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 4, -126, 4, -88, 48, -126, 4, -92, 2, 1, 0, 2, -126, 1, 1, 0, -100, 59, 66, -93, -42, -47, -40, 16, -127, 82, -52, 32, 1, 108, 46, -118, 22, -19, -76, -113, 12, -3, 9, 34, 116, 100, 34, 56, 25, -77, -3, 114, -96, 41, -14, 89, -119, -25, 87, 55, 9, 18, -92, -103, -91, 126, 12, -90, 33, 123, 35, 23, 55, 79, -99, -9, -50, 45, -12, -34, -79, -103, 116, 31, 22, -68, -109, 85, -123, 78, 109, 50, 55, 83, 106, 52, 25, -95, 98, -127, 9, 86, -18, -107, 59, -125, -42, -15, -26, -36, 42, 114, 23, 124, 48, -120, 79, 72, 38, 24, -33, 62, 8, -31, 58, -10, 45, -12, 110, 55, 11, -26, -7, -10, 45, 63, -10, 63, 48, 110, 116, 83, 73, 37, -103, -103, -31, -119, -97, -68, -123, -96, 37, 87, 87, 77, -110, -49, -123, -5, -9, -105, -111, -49, -9, 73, -106, -91, 57, -62, 9, 83, -122, 53, 37, -14, -99, -38, 10, 52, 111, 39, 21, 99, -121, 68, -8, -1, 50, 62, 1, 89, 115, -57, 124, 50, 113, 127, 31, 29, -102, 41, 14, -32, -80, 38, 95, 118, -78, 42, -111, -2, -88, -7, -70, 118, -122, 113, -4, 109, 64, 120, 82, -55, 98, 76, 31, -108, 21, 32, -108, 66, -77, -2, -16, 127, -99, -41, -13, 1, -56, 78, 119, -67, 123, -91, -54, -41, 61, -29, -71, 53, 66, 8, 106, 101, -33, 34, -92, 104, 50, -5, 75, 19, 119, 5, 8, -116, 40, 36, 95, 84, 89, -80, -123, -99, 2, 3, 1, 0, 1, 2, -126, 1, 0, 86, -48, 51, 50, -103, -113, 87, -111, -84, 91, 22, -28, 126, 5, -32, 66, -119, -123, 34, -3, 14, -119, -104, -58, -43, -120, 66, -33, -117, -96, -47, -88, 29, 73, -47, -54, 97, 111, -12, -83, -73, 83, -19, -75, 23, 25, -40, -21, -125, -43, 73, -27, 127, -83, -75, 40, -37, 31, -4, 94, -74, -81, 127, -80, 58, -47, 36, -55, -77, -34, -91, -100, -18, -45, 98, 67, 123, 46, -117, 111, 10, -126, -116, -26, -50, 0, 111, -77, 81, -58, 107, -85, -92, 83, 53, 32, -71, 117, 108, -2, 11, 39, 12, -122, 35, 94, 98, -96, -35, -111, -89, 2, 125, 73, -121, 81, 112, -125, 34, 101, 110, 44, 77, -116, 32, 51, 58, -8, -127, 67, 108, 57, -121, 80, -128, -40, 114, -47, -97, -30, -115, 127, 11, -113, 41, -49, -120, 68, 61, 92, 94, 78, 77, -52, 48, 117, 84, -109, -78, -30, 8, 53, -81, 2, -44, -79, 85, -120, -36, -123, -76, 41, -92, 30, -54, -57, -81, 98, -39, -88, -33, -58, 92, -38, -36, -23, 37, 73, -83, -105, 125, -92, -37, 72, -83, 32, 106, 19, 56, 25, 24, 89, 91, -36, 116, 22, -111, 77, -87, 79, 19, 48, 69, 105, -34, 49, -83, 74, -32, 105, -81, -42, 113, 77, -53, -102, 18, -68, 24, -62, 100, 16, 39, -11, 104, 99, -6, -123, -81, 122, -102, 53, 116, 122, -55, -71, 17, 14, 30, 96, 67, 108, -63, -127, 118, 65, 2, -127, -127, 0, -52, 121, -58, 27, -44, -112, 40, -106, 1, 98, 102, -20, -55, 81, -52, 59, 111, 27, -37, 111, -43, -65, 24, 73, -94, -53, -37, 110, -115, 4, 72, -39, -108, -106, 46, -83, 43, -119, 0, 90, -90, -24, -125, -57, -20, 108, 74, -26, 15, -54, -50, -74, -31, -42, -105, -68, 48, -1, 116, -48, -72, -48, 89, 82, -10, 24, -69, -51, -66, -44, 61, 43, 63, -9, 15, -44, -126, -66, 94, -111, -75, -125, 94, 64, 9, -44, -34, 73, -57, -61, 60, 18, 127, -73, 9, 126, -45, -56, 25, -63, 35, 4, -118, -122, 120, -2, -68, -24, -128, -12, 61, -128, -15, -123, -5, -7, 95, 97, -98, -61, -107, -19, -77, -71, 65, -52, 50, -83, 2, -127, -127, 0, -61, -103, 95, 25, 26, 31, 62, 1, -56, 101, -38, -50, -115, -33, 62, 83, -34, -52, -16, 99, -60, -93, 34, -25, 8, -48, 17, -2, 90, -104, -27, 101, -31, -8, 100, 3, 73, 71, -70, 16, 41, 75, 67, -72, 1, -38, 58, 8, 108, -82, 83, 15, 118, -22, -111, -69, -7, 85, -69, -98, -7, 43, 32, -69, 112, 79, -95, -49, -125, 11, 81, 76, 27, -122, 26, -128, -124, -82, 76, 114, -49, 25, -121, 6, 107, -35, -58, 17, 101, -30, 29, -13, 100, -25, -5, -104, -112, -24, 112, 17, 78, -89, -13, -115, 95, -127, 20, -39, -6, 65, -76, -103, -76, -41, -45, 62, -124, -53, -39, 122, 17, 80, -120, -80, 115, 50, -20, -79, 2, -127, -128, 96, -125, -94, 37, 4, -62, 62, -59, -1, 69, 73, 59, -65, 18, -29, 9, -78, -70, 61, 107, -98, 66, -94, -34, 46, -1, 118, 69, -54, 62, 25, 124, -54, 11, -47, 96, 95, -54, 60, -26, -115, 29, -9, 5, -24, -20, 124, -75, -9, 2, 77, 108, 57, 81, 39, 49, -25, 22, -51, 54, -81, -27, 120, 36, 22, -111, 58, 36, 53, 76, -52, -54, -101, 88, -84, -102, 44, 84, 91, 34, -1, -64, 80, 62, 125, -27, -126, 62, -27, 17, -100, 75, 71, 9, 72, 108, 26, -34, -70, 121, 85, 100, 17, 86, -61, -55, -55, -75, 29, 25, 43, -26, -81, 26, 5, 3, -126, 39, -5, -34, 105, -24, 87, 10, -68, -87, -5, -55, 2, -127, -127, 0, -65, 87, 19, 110, 76, -35, 111, 28, -13, 90, -96, 23, 27, 53, -35, -108, -46, 9, 12, -57, -84, 30, 34, 16, 123, -48, 127, 41, -96, -102, -99, 29, 115, 86, 55, 15, -9, -23, -112, -100, 42, -87, 19, -49, -73, 16, 49, 96, -8, 95, -9, -92, -45, 33, 87, -6, -40, 72, 3, -57, -39, -37, 84, -93, -57, -108, 62, -89, -18, 123, 109, -59, -45, -50, -11, -79, -17, 25, 74, 115, -83, 32, 61, -76, 12, 100, -32, 83, 56, -91, 95, -61, 47, 92, -72, 4, 97, 12, -32, 68, 45, -5, -94, 7, 19, -4, 3, -41, -48, 53, -30, 99, -46, 66, -90, -59, 43, -58, -10, 49, -79, 42, 100, -20, -89, 100, -109, -95, 2, -127, -127, 0, -64, 18, -45, -15, 95, -75, -128, -107, 81, -4, -68, 80, -32, 27, -124, 20, -107, 61, 91, 25, -57, 9, -73, -19, -53, -23, 89, 49, 92, 48, 122, -27, 15, -7, -113, -89, 94, -39, 16, 54, -24, 14, 52, -123, 9, -114, -78, 97, -96, -51, 68, 21, -66, -71, 91, -3, 5, 111, 1, 4, 78, -70, -17, -30, -10, 41, 105, 99, -47, -13, -127, -31, 8, -5, -66, 90, 6, -14, -95, 116, -81, 127, 32, 120, -43, -24, 9, -75, -114, -42, 87, -115, 86, 107, -31, 4, 81, 88, -97, 110, 98, -68, -3, -116, -54, -75, 44, -104, -87, -73, -128, 57, -5, 95, -52, -38, -108, 67, 93, 50, -17, -11, 43, -82, -116, 28, 14, 47";
        String[] bytes = Misc.split(pk, ",");
        byte[] bb = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            bb[i] = (byte) Misc.parseInt(bytes[i].trim());
        }
        //System.out.println("pk="+Base64.encode(bb));
        java.security.spec.EncodedKeySpec keySpec = new java.security.spec.PKCS8EncodedKeySpec(bb);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey bobPriKey = keyFactory.generatePrivate(keySpec);

        //g.addCRLEntry(endcert.getSerialNumber(), new java.util.Date(), CRLReason.keyCompromise);
        //String pkr="48, -126, 4, -68, 2, 1, 0, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 4, -126, 4, -90, 48, -126, 4, -94, 2, 1, 0, 2, -126, 1, 1, 0, -115, -47, -28, 22, -29, 114, -49, -49, 99, -17, -109, 96, 7, 105, -8, -87, 98, 45, 19, 51, 106, 91, -119, 55, -109, 54, -102, -37, 25, 98, -97, 66, 39, -1, -81, 34, 14, 9, -108, -116, -103, -88, 41, 67, -48, 6, 105, 79, -4, 23, 41, -33, 46, -33, -8, -7, 4, 11, -122, -61, 71, 33, 119, 38, -73, -91, 46, 61, 123, 102, 88, -85, -65, 4, 122, 72, -61, -112, -60, -52, 59, -10, 2, 81, 40, -76, -63, 31, -37, 114, 96, 93, 14, -77, 39, -20, -37, 104, -104, 106, 83, -94, 122, 91, -71, -8, 52, -27, -69, 100, -66, 107, -96, -103, -7, -91, -88, 88, 12, -45, -18, 73, 58, 119, -35, 113, -16, -71, 37, 78, -50, 42, -52, -20, 94, 62, 102, 74, 24, 40, -116, -111, -6, -3, -13, -40, -68, -39, 89, -116, 15, -117, 44, 60, -91, -89, 98, -15, -45, -56, -128, -82, 89, 97, -83, -54, -20, 81, -27, -124, 56, 21, -100, 5, -69, -117, -74, -15, -19, -124, 93, -28, -81, -119, -6, 58, -45, 88, 85, 39, -66, -32, -110, -125, -56, -43, 10, 122, 86, -15, -61, 54, -39, -111, -89, 62, -121, -104, 9, 70, -79, 86, 117, -11, -81, 71, -110, 62, 7, 71, -70, -83, 15, -92, -33, 97, -24, 122, 11, 52, -52, 95, 124, 10, -52, -83, -57, -9, -55, 37, 121, 107, -14, 103, -58, -72, 43, 92, -94, 53, 72, -101, -63, -24, -18, -3, 2, 3, 1, 0, 1, 2, -126, 1, 0, 11, 108, 111, -62, 4, 58, 56, 81, 13, 76, -68, 3, 77, 53, -107, 9, 23, -42, 103, -88, -119, -107, -85, -4, -27, 7, -51, -5, 126, 20, -6, -99, 26, -103, -74, -30, -72, 92, -65, 49, -128, 35, 18, -121, -38, -42, -39, 94, -49, 0, -110, -89, -34, -97, -67, -17, 89, 11, 16, -92, -28, 50, -26, -27, 32, 127, -42, -76, -81, 30, -12, 95, -119, 56, 116, 29, 46, 91, -105, -18, -105, -103, -34, 95, -23, 54, 126, 35, -15, -54, 73, -58, -43, 43, 75, 57, 2, 7, 111, -41, -120, -69, 26, -66, 14, -63, -42, -120, -10, -54, 75, -88, 97, -68, 101, 102, -103, -93, -42, 12, -80, 50, 7, -38, -91, -28, -54, -82, 16, 115, 46, -86, 21, -77, -120, -29, -92, -58, -53, -21, 120, -20, 68, -7, -127, 116, -37, 28, 63, 81, -81, 119, 115, -27, 26, -45, -119, -114, 111, -61, -107, -50, 58, -41, 90, -100, -119, 91, -73, -50, 105, 53, 61, -119, 101, 40, 49, 74, -44, -109, 68, 3, -63, 17, -106, -43, -13, 6, 27, -79, 103, -35, 74, 23, -58, -70, -35, -67, 126, -96, -63, -47, 91, -81, -52, -80, -89, -94, 67, 57, -127, 86, -63, 54, 78, -95, -36, 85, -29, -60, -57, 79, -34, 18, 70, -107, 126, 18, -46, -119, -38, -85, -45, -28, -89, 71, -87, -105, 64, -63, 102, 1, -90, 105, 18, 72, -108, 47, -78, -4, -98, -76, -122, -128, -35, -31, 2, -127, -127, 0, -52, -26, -100, 103, 117, 16, 39, -104, 42, 46, -125, -123, -111, 73, 10, 69, -111, -57, 34, -67, -68, 69, 112, -10, -23, -126, 37, 65, 120, 45, -59, 11, -50, -47, -92, -51, 56, 110, -59, -35, -26, -8, 80, 39, 28, -4, 13, -72, -44, -126, 113, -115, 35, -106, 79, 94, -34, 3, 19, -17, 110, -109, 127, -7, -85, 51, 65, -105, 110, 62, -56, 127, -127, 106, -93, -59, -23, -80, 54, 68, 33, 53, 8, 42, -51, -56, -71, 63, 123, -68, -37, -6, 125, 92, -33, 53, 64, -81, 36, -56, -48, 81, -25, -71, -34, -70, -23, -77, -5, 58, -42, -52, 116, -24, -67, -19, 12, 89, -63, 120, 62, -12, 59, 112, -83, 79, 33, 53, 2, -127, -127, 0, -79, 48, 8, 98, 123, -98, 103, 71, -68, 115, 57, 91, -101, 34, 63, 62, 52, -87, 42, -74, 74, 28, -94, 78, 117, -48, 111, 51, 56, -28, 24, -71, -50, 86, 76, 12, 5, -44, 57, 15, -3, -36, -128, -101, -107, 117, -80, -88, 10, -32, -34, -91, 124, -104, -74, -123, 5, -43, -3, -109, -118, 106, 94, 74, -38, 18, -113, -66, -108, -28, 125, -27, -53, -13, -100, 127, -94, -110, 58, -31, 7, -16, -39, -3, -51, -88, -108, 116, 21, -17, 75, 70, 126, -77, 74, -37, -70, -42, 9, 68, 22, -71, -74, 40, 21, -90, -94, 81, 98, -31, 52, 18, -12, -114, 61, -51, -61, -66, -65, -99, -71, 55, -13, 18, 74, 55, 87, -87, 2, -127, -128, 59, -12, 30, 63, -121, 89, 58, -68, -93, 91, 8, -48, -60, 58, 2, -17, 104, 64, 68, 66, 16, 28, -95, 121, 29, -54, 30, -20, -111, 37, -109, 74, 105, 49, -98, 111, 13, -36, 27, -71, 90, 35, -66, 16, 51, -62, -20, -96, -16, 77, 90, -68, 89, 117, 89, -76, 0, 114, 22, 27, 48, -41, -89, -46, 125, -19, 121, 123, -67, -98, 81, 60, 61, -71, -34, 65, -62, -90, -12, -71, -22, -99, -127, -50, 68, 120, 62, -5, -4, 80, -66, 109, 4, -47, 111, -34, 61, 23, 2, 68, 95, -16, 23, 69, -93, -114, 61, 99, -108, 87, 27, 18, -88, 2, -111, -48, 18, -107, 16, -81, 6, 124, -96, 7, -48, 46, 31, 33, 2, -127, -128, 40, -91, 118, 38, -15, 104, -61, 20, -121, -68, 126, -7, -18, -70, -118, 123, -128, -91, 21, -15, 63, 75, 22, 72, -99, 106, 53, 56, 48, -30, -46, 113, 61, 78, 58, -97, -65, 110, -111, -80, -125, 113, -29, -26, -51, 0, 116, -121, -8, 6, -31, -52, 96, -86, -65, -92, 0, 84, -72, -104, -15, -11, -72, -81, -65, 26, -26, -14, -73, 65, 59, 72, 77, -29, 18, -111, -116, -1, -78, -106, 16, -74, 69, -85, -99, 84, 0, -54, 40, -123, -2, 100, -85, -120, 1, -78, 42, 72, 19, 84, 21, 74, -128, -24, -19, 30, -114, -71, -48, -49, 12, -99, 20, -112, -44, -56, -79, 16, -66, -16, 53, -97, 10, 25, -74, 0, -64, 17, 2, -127, -128, 29, 87, 45, 108, 105, -73, -29, 61, -88, -68, -12, 38, -1, -99, 126, 92, -116, -96, -25, -124, 78, 122, -97, -24, 4, 41, -97, -109, -45, -29, 127, -66, 48, -57, -2, -77, 63, 109, -125, -108, 64, -20, -96, 10, 30, -49, 20, 17, 11, -66, -51, 40, 51, 111, -12, 68, 7, 45, -84, -71, 48, -56, -87, 115, -128, 77, -15, 96, -103, -105, -73, -101, -96, -32, -23, 53, -47, -80, -2, -101, -56, 66, 113, 68, -122, -105, 114, -102, 109, -107, 102, 119, 79, -6, 33, 8, 79, 14, -127, -114, -9, 68, -77, 3, 61, -84, 106, -29, 85, 11, 81, -101, -35, 93, 69, 126, -96, 99, 14, -105, -86, 76, -122, -4, 2, -92, 4, 12";
        String pkr = "48, -126, 4, -66, 2, 1, 0, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 4, -126, 4, -88, 48, -126, 4, -92, 2, 1, 0, 2, -126, 1, 1, 0, -112, -14, 4, -82, 69, -29, 23, 51, 44, -95, 40, 93, -72, -98, 28, -58, -102, -82, 62, 16, 82, 62, -81, -65, 37, 69, -85, 91, -55, 9, 3, 47, 43, 24, -109, 68, 78, 75, 61, -22, 97, 44, -118, -45, -103, 46, -32, 94, 14, -108, 110, -67, 9, 100, 26, 92, -47, -15, -37, 13, -90, 69, -60, 112, 63, -58, -55, -93, 47, 38, 6, 69, 73, -104, 8, -17, -104, -89, 109, 47, -56, 27, 37, 102, 12, 84, 78, 107, 49, 23, 89, 102, 91, 77, 34, -108, 46, -114, 50, -90, -92, 90, -57, 49, -78, 77, 118, 99, 59, 18, -35, 89, -42, -64, -109, -25, 24, 19, -49, -18, 119, -108, 39, -67, -121, -105, 77, 52, 94, 113, -118, -2, 99, -83, -103, -7, 57, 4, -50, 63, -97, 31, 69, -29, -60, -60, 21, -44, 87, 88, 79, 17, -44, 103, 61, 58, -45, 85, 45, 55, 1, 20, 54, -4, -81, 109, 77, -38, 113, -71, 114, 43, 23, 26, 61, 78, -9, -24, -118, 49, -32, 77, -48, -51, 9, -95, -113, 24, 122, -127, -46, -21, -73, -108, 97, 5, 24, 25, 101, 110, -121, -76, 74, 47, 60, 72, 88, -58, -76, 101, -49, 71, 43, 85, 10, 104, 7, 70, 23, -2, 87, 23, -12, -15, -81, 94, -112, 107, 84, -115, 85, 16, -5, -114, 111, -83, 45, 8, 88, -37, -23, 58, -105, 91, 104, 92, -99, -73, -74, -89, 53, -41, 28, 76, 122, -29, 2, 3, 1, 0, 1, 2, -126, 1, 0, 72, -64, -95, -90, -45, -65, 99, -126, -75, 49, 2, -16, -33, -36, -60, -10, -50, -25, 12, -111, -26, -90, 62, 82, -124, 35, -49, 26, 23, -13, 37, 29, 4, -43, -91, -3, -112, 10, -26, 62, 1, -98, 120, -28, 50, -84, 59, -60, 26, 47, 88, -46, 27, -89, -102, 47, -30, -43, 49, -83, 77, 59, 17, -103, -127, -7, -122, 118, 25, 17, 77, -18, 115, 61, 19, -89, 39, -119, 74, 29, -60, -92, 19, -122, -128, 67, -60, -60, -70, -12, 76, 84, 59, 25, -95, 116, -99, 85, -108, -120, -34, 80, 14, 81, -100, 124, -109, 34, -59, -96, -9, 103, -61, -118, 15, -14, -37, -69, -2, 15, 101, 8, 1, 86, 121, 123, 113, 45, 70, -20, 43, -34, 55, 120, -110, 49, 58, -48, -83, 79, 121, -114, 99, -50, 68, 117, 35, 46, 116, 8, 87, 32, 65, -39, 26, -126, -72, -5, -92, -107, -79, -83, -102, 117, 53, -57, 12, -128, -26, 8, 82, 40, 21, -4, -60, 47, -2, -41, 107, -124, 117, -32, 92, 62, -54, -97, 32, 118, 96, -32, -99, -33, 71, 28, -128, -34, 64, 96, -55, -113, -23, -106, 15, 78, 101, 65, 0, 27, 73, 45, -46, 39, -89, 25, -33, -101, 113, 102, -35, 51, -111, -6, 32, -56, -9, -128, 53, 0, 80, 104, -82, -28, -102, 14, -50, 32, -95, 89, -51, -115, 16, 19, -117, -109, -15, -42, -22, -45, 94, -1, -5, 120, -20, 22, 117, 97, 2, -127, -127, 0, -29, -50, 85, -22, 89, -17, -123, 24, 39, -6, 114, 54, 127, 111, 104, 8, -98, -123, -34, 87, 89, 87, -51, -123, -73, 54, 127, -92, -106, 123, 80, 127, -102, 35, -3, -36, 107, -62, 63, -113, -77, -34, 74, 50, -18, 81, -42, -9, -91, -1, 117, -99, 19, -45, 45, -47, -39, -21, 24, 5, -43, 102, 1, -105, 57, -82, 106, -84, -60, 35, 118, 3, 102, -14, 76, -67, -8, 16, -19, 39, 5, -70, -82, -43, -61, 125, -103, -61, -125, -102, -67, -28, -78, 34, 121, 118, 75, -81, 64, 113, -100, -96, -20, -30, -33, -65, 36, 101, 43, -79, -64, 107, -86, -7, 74, 14, -64, 105, -34, -102, -120, 37, 85, 59, -65, -68, 119, -47, 2, -127, -127, 0, -94, -30, 96, -47, -36, -71, 44, 108, 27, 18, -105, -99, -76, -120, 116, 60, -96, -1, -100, 83, -62, 123, -8, -70, -85, 13, 68, 17, 39, -25, -23, -93, -25, -63, -48, -26, 12, -12, -46, -118, 33, -29, 119, 88, -106, -47, -124, -15, 110, -15, 113, -112, -57, -83, -62, 4, 23, 37, 1, 58, -114, -45, -17, 39, 28, -12, -107, -32, -120, 83, -26, 78, -60, -103, 123, -40, -16, 59, -12, 103, 101, -15, 18, 96, 106, 48, 15, 76, 119, 3, 75, -93, 32, 49, -48, 25, 62, 125, -34, 7, -69, 51, 41, 95, -38, -115, 58, 117, -46, -21, -56, -100, -74, -32, 104, 33, 14, 95, 101, -5, 10, -70, -85, 80, 109, -54, 40, 115, 2, -127, -127, 0, -35, 111, 59, -28, 72, -9, -94, 16, 17, 12, 72, -24, -32, -122, -110, 65, -114, -107, -31, -62, 86, 126, 26, -80, -41, -98, -118, 4, -102, -95, -45, 101, -75, -105, 54, -76, -125, -86, -107, -50, 65, -80, -39, -65, 24, 25, 69, 52, 4, -113, -124, -21, -30, -47, 107, -64, -120, 107, 21, 54, -42, 112, 9, 43, -50, 77, -44, 51, -117, -108, 76, -86, -111, -17, 4, -39, -61, -65, -16, 69, -10, 51, -72, -82, -31, 15, -67, -33, 102, -107, -117, -16, 57, -105, 121, 41, -101, -8, 6, 84, 105, 67, -92, 0, 4, 45, -16, 90, -57, 86, 73, 12, 95, 109, 98, -89, 100, 49, -124, -56, 1, 127, 8, -110, -61, -107, -105, -127, 2, -127, -128, 119, 53, 110, -34, -88, -57, -19, 55, 1, 105, 78, 5, 97, -121, 44, 106, -30, -7, -80, 19, -83, 22, -58, 38, -9, -31, 49, -101, -52, -76, -14, 77, -66, -28, 30, 90, -1, -56, 125, -2, 18, 103, 67, -123, 86, 123, -24, -20, 57, -30, 2, -41, -113, 12, 32, 30, -106, 65, -62, -16, 116, 111, -81, 107, -44, -55, 60, -105, -93, -82, -93, -77, -86, 55, 26, 104, -33, 9, 66, 10, 26, -107, -96, 121, 103, -67, -36, -76, 125, -75, -81, -71, -89, 106, 77, 126, -20, 57, 122, 49, -10, 83, -81, 72, 51, -49, -128, -91, -110, -60, 117, -126, 28, 101, -120, 80, 9, 16, -48, -74, 54, 87, -1, -31, 81, 56, -50, -7, 2, -127, -127, 0, -122, -43, -40, -1, -13, -20, 80, 22, -53, -67, 108, 83, -54, 54, 116, -110, 9, 87, 113, 110, 88, -110, -97, 11, 101, 19, -35, 2, -59, -101, -58, -14, -24, 14, 1, -124, 80, 54, 100, 61, 52, 85, 97, -12, 84, 94, -49, -53, 52, 50, -111, -47, -125, -14, 49, -48, 119, -113, -69, -43, 124, 50, 108, 62, 35, 34, 31, -125, 47, -75, 15, -83, -87, 48, -120, -66, 45, 103, -100, 54, 123, -126, -50, -40, -79, 115, 40, 82, 42, 94, -29, -47, 17, 42, -54, 13, -10, 84, 98, 32, 10, -105, -7, 116, -110, -44, 73, 84, 13, 37, -119, -41, -16, -1, -3, -23, -19, 110, 126, -62, 61, -62, -63, -3, 4, -64, -88, -13";
        String[] bytesr = Misc.split(pkr, ",");
        byte[] bbr = new byte[bytesr.length];
        for (int i = 0; i < bytesr.length; i++) {
            bbr[i] = (byte) Misc.parseInt(bytesr[i].trim());
        }
        //System.out.println("pk="+Base64.encode(bb));
        java.security.spec.EncodedKeySpec keySpecr = new java.security.spec.PKCS8EncodedKeySpec(bbr);
        KeyFactory keyFactoryr = KeyFactory.getInstance("RSA");
        PrivateKey bobPriKeyr = keyFactoryr.generatePrivate(keySpecr);


        System.out.println("CRL test...");


        //trustStore.setCertificateEntry("ca"+cacert.getSerialNumber(), cacert);
        boolean tse = true;
        if (tse) {
            java.io.File ts = new java.io.File("tmptrusstore.jks");
            OutputStream os = new FileOutputStream(ts);
            trustStore.store(os, "changeit".toCharArray());
            os.close();
            System.setProperty("javax.net.ssl.trustStore", ts.getAbsolutePath());
        }

        String clientKey = "48, -126, 4, -68, 2, 1, 0, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 4, -126, 4, -90, 48, -126, 4, -94, 2, 1, 0, 2, -126, 1, 1, 0, -128, 110, 77, 63, 33, -69, -101, 116, 88, 76, 48, -46, 110, -107, -51, 0, 27, -44, -78, -80, 25, -103, -120, -76, -87, -97, 14, -127, -119, -48, 105, -110, -124, 55, -102, -72, -29, 27, 0, -41, -119, 49, 101, -10, 4, 99, -92, -60, -38, 125, 122, -24, 57, -7, -21, 59, 102, 40, -43, -21, 10, -61, 2, -123, -82, -94, -22, 63, -82, -36, 0, 107, -112, -24, -44, -120, 46, 20, 46, -80, -56, 51, 14, 118, 16, -106, -89, 36, -11, -11, -126, 90, 57, 104, -62, 20, 74, 53, 91, -108, 113, 113, -9, 30, 23, -105, 115, 80, 62, 62, 11, -77, -78, -77, -127, 8, 19, 107, -2, 108, -100, -18, 46, 52, 100, 69, -96, 68, -119, -111, -7, 10, 5, 13, 77, 49, 23, 51, 100, 113, 64, 54, -95, 86, 18, 33, -15, 33, -17, 38, -54, 28, 68, 91, 22, -84, 109, 116, -36, 104, -104, 99, -2, 16, -26, -62, 24, -18, -72, -120, -119, -48, 11, 95, -52, 70, -17, -116, -107, 15, 45, -86, -79, 35, 68, -65, -103, -50, -21, -18, -33, 115, 1, -60, -85, -112, -127, -94, -37, 35, -105, -122, -6, 49, 126, 6, -71, 30, -4, 98, -38, -75, 97, -85, 119, 93, 95, -105, -82, 95, -81, -46, -33, -60, 88, 53, 91, 29, 7, 0, 26, -23, -68, -88, -108, -44, -105, -55, -25, -56, 40, -58, -4, -92, -119, 0, 4, -6, -107, 84, 46, 99, -82, -84, -38, 121, 2, 3, 1, 0, 1, 2, -126, 1, 0, 96, -45, 71, -41, -99, -43, -12, 24, 93, -19, 107, 45, 107, 3, -13, 30, -18, -36, -121, -82, -18, 77, -21, 91, -47, 57, -62, 84, -11, 18, -76, 85, 42, -21, 68, -15, -72, -2, -123, -66, -7, -122, -51, -119, 17, -68, 17, -33, -5, -99, 120, -119, 22, 20, -114, 61, 37, 3, 10, 107, -2, -100, 17, 13, -70, 32, -57, 28, 37, -9, 101, 53, -77, 117, 101, -82, 25, -105, 75, -80, 41, -17, 28, 42, -12, -52, -81, -52, -44, -58, 56, 117, 79, 44, -28, -115, 41, 121, 90, -58, -22, 84, 114, 107, 52, 124, 17, 10, -31, 50, 35, -34, 97, -114, -91, 107, -14, 64, -117, -5, 84, -64, -39, 2, -44, -54, 13, 78, 39, 18, -79, -88, -40, -108, -93, -66, -61, -74, 82, 118, -46, 24, -83, -118, 29, -38, -78, -57, 120, 124, 22, 73, 13, 48, 99, 116, 43, 89, -35, -64, -27, -54, -29, 68, -19, 5, 14, 83, 90, 36, 127, -59, -38, -79, 119, -42, 73, -52, 70, 110, -105, -89, 104, 50, 41, 22, -59, 79, -120, 80, 63, 98, 35, -90, -104, 74, -40, -39, 89, 37, 29, -44, 112, -27, -81, -88, -122, -7, 72, -24, -127, -53, -8, 97, -29, -125, -62, 56, -102, 57, 102, -21, 27, -113, -89, 16, 46, -75, 105, 19, 54, -29, -52, -74, -40, -25, 19, -9, -28, -55, 53, -10, -96, -33, 117, 48, -65, -91, 59, -47, 72, -4, -120, -127, -16, 117, 2, -127, -127, 0, -32, 65, -78, -61, 66, -39, -49, 14, 115, -56, 83, 35, -88, -27, -51, 73, 49, -111, -72, 111, 111, -32, 63, 14, -47, -42, -92, -80, -103, 34, -21, -69, 57, -107, 81, 85, 76, -56, 25, -82, -104, -42, -67, -10, -23, -38, 116, -59, -24, -114, -48, -3, 9, 50, -22, 64, -99, 41, -39, 26, 3, 72, -4, 91, -24, -82, -97, -119, 68, 98, -32, -114, -68, -89, -42, -124, -102, -61, 4, -14, -116, 30, -52, -51, 50, -57, -95, 75, -105, 113, -113, 96, -80, 50, 29, -86, 98, -29, 30, 12, -86, -50, 104, -59, -52, 19, 114, 114, -53, 52, -7, -24, -128, -119, -50, -58, -56, 118, 81, 36, -67, -69, -53, -94, -46, 18, -60, 47, 2, -127, -127, 0, -110, -100, 51, -86, -14, 112, -16, -56, -93, 71, 35, -45, 70, 75, 71, -11, -1, -2, 101, -74, 4, -32, 77, -117, -105, -77, 107, -108, -4, -25, -111, -106, 76, -11, -124, -88, 40, -14, -91, -11, -61, -17, 124, 14, 33, -12, 84, -26, 44, 23, 90, -40, -128, 45, 5, -48, 85, 16, 77, 120, 22, -113, 100, 69, -86, -76, -48, -21, 26, -72, -102, 118, 58, -112, -8, -85, -99, -113, 99, 91, -120, 13, -46, -29, -32, 106, 102, 22, -49, 26, -2, -96, -5, -14, 86, -19, -69, -81, -101, 15, 22, 47, -25, 64, -45, -58, -44, 95, -90, 76, 54, 52, 8, -10, -105, 60, -104, 43, 118, 51, 8, -25, 7, 21, 36, 118, -103, -41, 2, -127, -128, 86, 108, -37, 96, -25, 29, -101, -17, -100, -19, -126, 37, 7, 13, 113, 61, 25, 14, 104, -65, 1, -91, -79, -121, -101, -107, 22, 41, 61, 11, 57, 64, 17, 100, 27, 81, 61, 99, 12, -118, -66, 58, 92, 65, -111, 68, 112, -90, 105, 6, -55, 66, 11, 78, -124, -6, -37, -42, -23, -27, 93, -120, -66, 65, 9, 126, 106, -37, 114, -16, -116, 76, 86, 38, 116, -60, -20, 3, -26, -97, 55, 35, 19, -104, -83, 78, 68, 58, 38, -90, 76, 52, 94, 19, 34, 93, -84, -8, -116, 119, 35, 72, -93, 22, 63, -100, -21, 9, -108, -120, -39, 27, 34, -106, 121, -117, 32, 108, -128, -65, 33, 36, 3, -55, -65, 30, 30, 95, 2, -127, -128, 11, 113, 75, -117, 13, 15, 9, -117, -17, 74, 111, 30, 95, 63, 34, -25, -83, 74, 50, -80, 9, 35, 100, -119, -1, -106, 14, 80, 123, -120, -85, 81, 58, -62, 114, -49, 25, 27, 15, 30, -26, 98, -5, -96, -78, -65, -70, 28, -104, -42, 93, -51, -59, -5, 61, -105, -21, -5, -80, -64, 45, -86, -107, 93, -26, -101, -28, 46, 101, 80, -127, 37, -73, 30, 119, 122, 21, 123, 10, 123, 43, 92, 64, -40, 110, 60, 127, -87, 74, 18, -58, 74, -44, 63, 46, -49, -79, 53, 92, 121, -55, -10, -123, 43, 7, 28, 94, -104, -12, -122, -64, 56, 65, 118, -43, -120, -14, 43, 122, 55, 51, 123, -84, -114, -42, -32, -74, -45, 2, -127, -128, 48, -76, 33, 61, 13, -5, -69, 68, 30, -125, 20, 50, 46, -10, 116, -27, -81, -2, 39, -14, -33, 121, 17, 104, 9, 65, 122, 86, -24, -40, 81, -31, -55, 10, 64, -78, -82, 11, 1, -47, -98, -52, 38, 94, 30, -11, -17, -23, 98, 15, -19, -108, -81, -44, 3, -104, -57, -25, 84, -84, 71, -104, -48, -68, -68, 11, 107, 100, -17, 26, 65, 113, 94, -62, -124, -20, -24, -1, 36, -35, -87, 17, -97, 24, -104, 41, -124, -96, 9, 10, 67, 103, 39, 126, -22, 2, -69, -52, 98, 116, 34, -15, 23, 7, 127, -117, -119, 104, -17, 81, -69, 18, 26, -31, 69, -69, 70, -127, -126, -48, 113, -57, -99, 13, 1, 23, -102, 83";
        String[] bytesc = Misc.split(clientKey, ",");
        byte[] bbc = new byte[bytesc.length];
        for (int i = 0; i < bytesc.length; i++) {
            bbc[i] = (byte) Misc.parseInt(bytesc[i].trim());
        }
        //System.out.println("pk="+Base64.encode(bb));
        java.security.spec.EncodedKeySpec keySpecc = new java.security.spec.PKCS8EncodedKeySpec(bbc);
        PrivateKey bobPriKeyc = keyFactory.generatePrivate(keySpecc);


        String clientChain = "-----BEGIN CERTIFICATE-----\n" +
                             "MIIEFjCCAv6gAwIBAgIFIstwHrowDQYJKoZIhvcNAQELBQAwdTELMAkGA1UEBhMCRUUxCzAJBgNVBAgTAkhNMRAwDgYDVQQHEwdUYWxsaW5uMRMwEQYDVQQKEwpTY2hlbWUgSW5jMRAwDgYDVQQLEwdSb290IENBMSAwHgYDVQQDExdTY2hlbWUgSW5jIFJvb3QgQ0EgMjAyNzAeFw0xNzA1MTAxMjQxNDhaFw0yMDA4MjIyMzU5NTlaMH4xCzAJBgNVBAYTAkVFMQswCQYDVQQIEwJITTEQMA4GA1UEBxMHVGFsbGlubjETMBEGA1UEChMKU2NoZW1lIEluYzEYMBYGA1UECxMPQ2xpZW50IEF1dGggM0RTMSEwHwYDVQQDExhTY2hlbWUgSW5jIEF1dGggM0RTIDIwMTcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCAbk0/IbubdFhMMNJulc0AG9SysBmZiLSpnw6BidBpkoQ3mrjjGwDXiTFl9gRjpMTafXroOfnrO2Yo1esKwwKFrqLqP67cAGuQ6NSILhQusMgzDnYQlqck9fWCWjlowhRKNVuUcXH3HheXc1A+PguzsrOBCBNr/myc7i40ZEWgRImR+QoFDU0xFzNkcUA2oVYSIfEh7ybKHERbFqxtdNxomGP+EObCGO64iInQC1/MRu+MlQ8tqrEjRL+Zzuvu33MBxKuQgaLbI5eG+jF+Brke/GLatWGrd11fl65fr9LfxFg1Wx0HABrpvKiU1JfJ58goxvykiQAE+pVULmOurNp5AgMBAAGjgaMwgaAwCQYDVR0TBAIwADALBgNVHQ8EBAMCBLAwEwYDVR0lBAwwCgYIKwYBBQUHAwIwHQYDVR0OBBYEFHMsBzl0m9OhGMI76AYnBBRExSeCMB8GA1UdIwQYMBaAFEvDpX8zmbiswdbB0zWkSbAcIaNyMDEGA1UdHwQqMCgwJqAkoCKGIGh0dHA6Ly9kc3gubW9kaXJ1bS5jb20vZHNjcmwuY3JsMA0GCSqGSIb3DQEBCwUAA4IBAQCNtzqgQ2v+QOXLM2Jw1n7lPiAH6xf/qkHGiSuwxqRzhdIs98E7RWxzgb5bxjTgcclg5kKUcygJvyinz/l7W0oYYp/Th+aQ0HMa6mufqwNa/qF0nj+lFE+BLph8IoIFFSurbKpz5jxPyFakrzejIVpvOHAzD7unQN7EpEsAgxgFOJiisSo28VIj4tDyKYFBYtf/5chOBQzWrw8TmMK3zNlrA7hSiw7YwfzHnQyD0OhmGF41P6X/A0aPp83vEBFpK6Zc3sa2/s4I+tsJqgumbLAcpt30WVbaIc3YBVfgNvlLGE0akzrVqWX74T3OHtgmHY4Pawx0gGmtCCif1EZVyppH" +
                             "\n-----END CERTIFICATE-----\n" + "-----BEGIN CERTIFICATE-----\n" +
                             "MIIDtjCCAp6gAwIBAgIFIsVYDqQwDQYJKoZIhvcNAQELBQAwdTELMAkGA1UEBhMCRUUxCzAJBgNVBAgTAkhNMRAwDgYDVQQHEwdUYWxsaW5uMRMwEQYDVQQKEwpTY2hlbWUgSW5jMRAwDgYDVQQLEwdSb290IENBMSAwHgYDVQQDExdTY2hlbWUgSW5jIFJvb3QgQ0EgMjAyNzAeFw0xNzA0MjgxNjQxNDVaFw0yNzA0MjYyMzU5NTlaMHUxCzAJBgNVBAYTAkVFMQswCQYDVQQIEwJITTEQMA4GA1UEBxMHVGFsbGlubjETMBEGA1UEChMKU2NoZW1lIEluYzEQMA4GA1UECxMHUm9vdCBDQTEgMB4GA1UEAxMXU2NoZW1lIEluYyBSb290IENBIDIwMjcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCQ8gSuReMXMyyhKF24nhzGmq4+EFI+r78lRatbyQkDLysYk0ROSz3qYSyK05ku4F4OlG69CWQaXNHx2w2mRcRwP8bJoy8mBkVJmAjvmKdtL8gbJWYMVE5rMRdZZltNIpQujjKmpFrHMbJNdmM7Et1Z1sCT5xgTz+53lCe9h5dNNF5xiv5jrZn5OQTOP58fRePExBXUV1hPEdRnPTrTVS03ARQ2/K9tTdpxuXIrFxo9TvfoijHgTdDNCaGPGHqB0uu3lGEFGBllboe0Si88SFjGtGXPRytVCmgHRhf+Vxf08a9ekGtUjVUQ+45vrS0IWNvpOpdbaFydt7anNdccTHrjAgMBAAGjTTBLMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgH2MA8GA1UdJQQIMAYGBFUdJQAwHQYDVR0OBBYEFEvDpX8zmbiswdbB0zWkSbAcIaNyMA0GCSqGSIb3DQEBCwUAA4IBAQAdS7mGMQIw3fDWPkovvkmrQjchMs67+JGqnUbMP9lqDMXDIN7YrdJe6Cq1vIPvvSCYRF645GvSIXlZiO0R9b5nNtF66mBTk4Q5RaCdISoWTlBB9wJ5KKlih7BUNTbSBQ/XPp8tSEvtnG6HumDv+NlPVtFtTw23Y3zdLrrDi0I1RlJ5KbH1yKJRwHTI6wy60YmHxJtkn4VJ4iJtglzJWl0fC95PjdORYt4cWyNG7Y7J30T7n//lIr6U9kS1HYWWgui+wfUIk2YsolhW2+cydgqUGietPuG+C/7Gsnks52FK8nO+BKldiXSUaslMgCmSR7BJeqWj/KG3DEIstlDYwEA4" +
                             "\n-----END CERTIFICATE-----";

        X509Certificate[] clc = KeyServiceBase.parseX509PemHeadersSafeCertificates(clientChain.getBytes(), "X509");

        HttpsJSSEClient client = new HttpsJSSEClient();
        //client.setTrustManagerAlg("X509");
        //(String sslProvider, String keyManagerFactoryType, String sslContext, String allowUntrustedCertificates,
        //			PrivateKey clientKey, Certificate[] clientCert, KeyStore truststore, int connectTimeout1, int readTimeout1, boolean hostNameVerify)
        client.init(null, KeyManagerFactory.getDefaultAlgorithm(), "TLS", "false", bobPriKeyc, clc, trustStore, 10000,
                    10000, true);
        // client.init(null, null, "TLS", "false", null, null, null, 10000, 10000, true);

        //MyPKIXMasterCertPathValidator
        System.out.println("Connecting " + url);
        Thread.sleep(2000);
        System.out.println("Post..");
        byte[] res = client.post(url, "Test".getBytes(), null);
        System.out.println("Res=" + new String(res));

        System.out.println("Connecting2 " + url);
        //System.setProperty("com.sun.net.ssl.checkRevocation", "false");
        Thread.sleep(1000);
        byte[] res2 = client.post(url, "Test".getBytes(), null);
        System.out.println("Res2=" + new String(res2));


    }


}
