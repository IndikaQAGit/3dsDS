/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 13. jaan 2016
 *
 */
package com.modirum.ds.tests;

import com.modirum.ds.utils.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;


public class Helper {
    protected transient static Logger log = LoggerFactory.getLogger(Helper.class);

    public static java.security.KeyStore loadKeyStore(String ksFile, String ksPassword, String ksType) throws Exception {

        java.security.KeyStore keyStore = null;
        java.io.File f = new java.io.File(ksFile);
        //		log.info("'{}' '{}' '{}' exists '{}' canRead '{}' ", ksFile, ksPassword, ksType, f.exists(), f.canRead());
        if (f.exists() && f.canRead()) {
            java.io.FileInputStream fis = null;
            try {
                keyStore = java.security.KeyStore.getInstance(ksType);
                fis = new java.io.FileInputStream(f);
                keyStore.load(fis, ksPassword.toCharArray());
                fis.close();
                fis = null;
            } catch (Exception e) {
                throw e;
            } finally {
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (Exception dc) { /* nothing to catch here */
                        fis = null;
                    }
                }
            }
        } else {
            log.warn("Keystore file '" + f.getAbsolutePath() + "' not found or not readable");
        }

        return keyStore;
    }

    static public byte[] deflateMessage(byte[] bytes) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DeflaterOutputStream d = new DeflaterOutputStream(bos);
        try {
            d.write(bytes);
            d.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException("Should never happen");
        }
        byte[] ret = Base64.encode(bos.toByteArray()).getBytes(StandardCharsets.UTF_8);
        if (log.isDebugEnabled()) {
            log.debug("Encoded length: " + ret.length);
        }
        return ret;
    }


    /**
     * Core, p.76: Decode base64 and inflate.
     */
    static public byte[] inflateMessage(byte[] bytes) throws RuntimeException {
        try {
            byte[] raw = Base64.decode(bytes);
            InputStream is = new InflaterInputStream(new ByteArrayInputStream(raw));

            int c = -1;
            ByteArrayOutputStream bos = new ByteArrayOutputStream(128);
            while ((c = is.read()) >= 0) {
                bos.write(c);
            }
            return bos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException("Couldn't decode request", e);
        }
    }

}
