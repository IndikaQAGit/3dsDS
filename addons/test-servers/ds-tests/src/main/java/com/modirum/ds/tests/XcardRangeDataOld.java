/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 31. mrts 2016
 *
 */
package com.modirum.ds.tests;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.List;

public class XcardRangeDataOld extends XmlAdapter<String, JAXBElement<List<String>>> {
    public String marshal(JAXBElement<List<String>> object) {
        if (object != null) {

            if (object.getValue() != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                for (String s : object.getValue()) {
                    if (sb.length() > 1) {
                        sb.append(',');
                    }

                    sb.append("\"");
                    sb.append(s);
                    sb.append("\"");

                }

                sb.append("}");


                return sb.toString();
            }


        }

        return null;
    }

    public JAXBElement<List<String>> unmarshal(String object) {
        return null;
    }

}
