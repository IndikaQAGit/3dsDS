/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 6. jaan 2016
 *
 */
package com.modirum.ds.tests;

import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.MessageService.JsonAndMessage;
import com.modirum.ds.services.MessageService210;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XImage;
import com.modirum.ds.tds21msgs.XJWE;
import com.modirum.ds.tds21msgs.XacsSignedContentPayload;
import com.modirum.ds.tds21msgs.XcardRangeData;
import com.modirum.ds.tds21msgs.XmessageExtension;
import com.modirum.ds.tds21msgs.XrenderObj1;
import com.modirum.ds.tds21msgs.XrenderObj2;
import com.modirum.ds.utils.Base64;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.eclipse.persistence.jaxb.MarshallerProperties;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import static com.modirum.ds.util.DSUtils.createUUID;

public class BindingTest21 {


    public static void main(String[] args) throws Exception {

        //Class c=Class.forName(DSModel.Issuer.Status.class.getName());

        MessageService210 mse = MessageService210.getInstance();
        TDSMessage me = new TDSMessage();
        me.setMerchantName("Vesa Parkinen\nAbout Exaping\tto happen inside json");
        System.out.println("RAW Vesa: " + new String(mse.toJSON(me), StandardCharsets.UTF_8));
        System.out.println("Client Vesa.MerchantName: " + mse.fromJSON(mse.toJSON(me)).getMerchantName());

        StringBuilder json1 = new StringBuilder(
                "{\"messageCategory\":\"02\",\"threeDSServerRefNumber\":\"3DS\",\"billAddrState\":\"AZ\",\"acctID\":\"EMVCo 3DS Test Account 000000001\",\"sdkEphemPubKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEMor9GAxY8eZoEEeB51VLegqN9h2ihN5y9I6nQYS2VIsCvcL3/43vNaDMF3Gc0EpHGmb4Y3/S6UzN97f5kPSf9A==\",\"cardExpiryDate\":\"2212\",\"merchantRiskIndicator\":{\"shipIndicator\":\"01\",\"deliveryTimeframe\":\"2\",\"deliveryEmailAddress\":\"example@example.com\",\"reorderItemsInd\":\"1\",\"preOrderPurchaseInd\":\"1\",\"preOrderDate\":\"20300101\",\"giftCardAmount\":\"1\",\"giftCardCurr\":\"840\",\"giftCardCount\":\"01\"},\"shipAddrState\":\"AZ\",\"shipAddrLine3\":\"Address Line 3\",\"shipAddrLine2\":\"Address Line 2\",\"threeDSRequestorURL\":\"http://ul.com/fd2d3d20-b2b3-4ec0-ab27-9f1315fa84ae\",\"acctNumber\":\"0000000040044943\",\"shipAddrLine1\":\"Address Line 1\",\"messageType\":\"AReq\",\"sdkReferenceNumber\":\"Participating UL Test 3DS SDK\",\"threeDSRequestorName\":\"EMVCo 3DS Test Requestor\",\"sdkAppID\":\"e948b38f-2632-45bc-8dd9-e7cc37c4d5e3\",\"shipAddrPostCode\":\"Postal Code\",\"messageVersion\":\"2.0.1\",\"sdkTransID\":\"7b0ce533-30ce-4d2b-959f-024e995b5f38\",\"billAddrPostCode\":\"Postal Code\",\"email\":\"example@example.com\",\"billAddrCountry\":\"840\",\"sdkEncData\":\"eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkExMjhDQkMtSFMyNTYifQ.J12H-XJ3PTqp9ASMHhPT5W42may-SsSMKDsSWwrju0gZodJGreOHij8437RWYgBi2JfjfOxEyRArDXuC8Kn5LrJtdioGORANlxgBaDdDJPGC3QxHrVRITJ9Ze6nka1NV89XOjO47rJqaGXlT45qeYfgr8sH4ox9U6AdIhzBvNU1ElbOhUb8x7ePYCHb4EIpprtCsgKs1_v_2RQ6m0fPuN0u4BeQ_5oD6vBpTUdZszY3BsTxeQvjcQT7cLaKb50_PkhWdAk7HR8NOaiKzM3h9mhJi3BGrMlhRNhniQPB4hW1s5g5hyGjOQPzOCuD1vSWGoRCRJTUl3KILU7HK0Pm3DQ.pBJAEDFIR4-1TTYsmAz2fg.E2Gtup_WDOJZR_i9dL1WcozDcABxOOuGLp0FeV_spP-4RfU8X9Wt5uwt-odLGZj5liIg1HB55p1ptQf3503wro-hTiAjFffgY3PuSffLl1Pxcp8epYnQYF2dqzWLE7lQ-EQoZJXnkmDFrslZrRy7dGyc7E-1R0cPtn0U4tlMCThhtK7JBa6MfRjIqG8UYhnFZZNy1pzOwh9H7IZUN8ZqaPUwz7Ckn_xBa7NO9tazJoLZHo4vqeuZQyPh7MG_WRVWkDIqFo6e3NtcCcgIVZH_lBLrpKo1yg7UmjFiqoo3sh7sI2UxG80nJBm4cIsEVUw6EBr0FjJc_sTSYHk6kqJMfNYr_rizCSs2cbFatVB8j5UIRF0Cay2EvGbDno4tbusR5W2OhTzmJAcLzMsNErFZqsyfHC-gvv68kYG8i-C38dtbHJI05hYPigg-TlJcKQ0Jvs7uUwXFMHrHUzkkJMd4Sui-4s0amLFs44tRGnVomMYaWKUQkx6EnhvtzJRodQ6OjF6hiWN9BsTWeHsDQ4Qx-WQek6r5u_nr1vF8iB1y1Trt818KhR7Je3nDpuAfzhNdesBKEZeQpqBk3CdODIMoRo_31PtmRoubm-yUGuGS6ewQ-fol5yrCvAZ6VPng_TGm.Tfa0szRpY-8PvtOgrPS_kA\",\"homePhone\":{\"cc\":\"123\",\"subscriber\":\"123456789\"},\"threeDSRequestorID\":\"199\",\"billAddrLine2\":\"Address Line 2\",\"billAddrCity\":\"City Name\",\"deviceChannel\":\"01\",\"deviceRenderOptions\":{},\"billAddrLine3\":\"Address Line 3\",\"billAddrLine1\":\"Address Line 1\",\"acctInfo\":{\"chAccAgeInd\":\"5\",\"chAccDate\":\"20170101\",\"chAccChangeInd\":\"4\",\"chAccChange\":\"20170101\",\"chAccPwChangeInd\":\"5\",\"chAccPwChange\":\"20170101\",\"shipAddressUsageInd\":\"4\",\"shipAddressUsage\":\"20170101\",\"txnActivityDay\":\"1\",\"txnActivityYear\":\"1\",\"provisionAttemptsDay\":\"0\",\"nbPurchaseAccount\":\"1\",\"suspiciousAccActivity\":\"1\",\"shipNameIndicator\":\"1\",\"paymentAccInd\":\"5\",\"paymentAccAge\":\"20170101\"},\"threeDSRequestorChallengeInd\":\"01\",\"threeDSRequestorAuthenticationInfo\":{\"threeDSReqAuthMethod\":\"01\",\"threeDSReqAuthTimestamp\":\"201712060909\",\"threeDSReqAuthData\":\"00\"},\"transType\":\"01\",\"shipAddrCity\":\"City Name\",\"mobilePhone\":{\"cc\":\"123\",\"subscriber\":\"123456789\"},\"threeDSRequestorNPAInd\":\"01\",\"cardholderName\":\"Frictionless One\",\"acctType\":\"02\",\"workPhone\":{\"cc\":\"123\",\"subscriber\":\"123456789\"},\"shipAddrCountry\":\"840\",\"threeDSServerURL\":\"http://localhost:8080/v2.0.1/3dsServer/199/\",\"threeDSServerTransID\":\"80c8fc00-899b-4153-9cf3-4369332cad87\"}");
        MessageService.maskJSONStrValue(json1, "acctNumber", 3);
        System.out.println(json1);

        String emptyStruct =
                "{\"messageCategory\":\"02\",\"threeDSServerRefNumber\":\"3DS\",\"threeDSServerRefNumber\":\"3DS\",\"billAddrState\":\"AZ\",\"acctID\":\"EMVCo 3DS Test Account 000000001\",\"sdkEphemPubKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE1WorGD+ITpwkzKDBOOAfbXX22l/NYlCi3EoCF3Kreo/12S+n4naHSPYK+2hL1gFq8GrF3FH3DFDQeck7JG1iEg==\",\"cardExpiryDate\":\"2212\",\"merchantRiskIndicator\":{\"shipIndicator\":\"01\",\"deliveryTimeframe\":\"2\",\"deliveryEmailAddress\":\"example@example.com\",\"reorderItemsInd\":\"1\",\"preOrderPurchaseInd\":\"1\",\"preOrderDate\":\"20300101\",\"giftCardAmount\":\"1\",\"giftCardCurr\":\"840\",\"giftCardCount\":\"01\"},\"shipAddrState\":\"AZ\",\"shipAddrLine3\":\"Address Line 3\",\"shipAddrLine2\":\"Address Line 2\",\"threeDSRequestorURL\":\"http://ul.com/f53816e0-4830-40de-840c-f3e44055568a\",\"acctNumber\":\"000##########943\",\"shipAddrLine1\":\"Address Line 1\",\"messageType\":\"AReq\",\"sdkReferenceNumber\":\"Participating UL Test 3DS SDK\",\"threeDSRequestorName\":\"EMVCo 3DS Test Requestor\",\"sdkAppID\":\"e0564926-8e77-4992-a545-87c41cc0f7ae\",\"shipAddrPostCode\":\"Postal Code\",\"messageVersion\":\"2.0.1\",\"sdkTransID\":\"e703440a-1c0d-44bc-92ae-1190d8a6ea47\",\"billAddrPostCode\":\"Postal Code\",\"email\":\"example@example.com\",\"billAddrCountry\":\"840\"," +
                "\"sdkEncData\":\"eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkExMjhDQkMtSFMyNTYifQ.iOSUn7M-NjPLN9LVRKYCTjEkSQFnI7tTl4jpJ7DEu_8hz2y7LIPA92qjd-h37SEFJtZw3soEhvdSumWJUZ2_zfve1UxHX0b-tF-r0OCwv8_ARxbJusVP1zr3ZKcsQ5gqI3hOK5NoRtaPUqhCvH_KAw0WEo4efjf4ETM7wFBtmpIwZUS5HdVxKYq13Uj8RnwJSpst5AALTM5LLBAQtkeX5ffit519HiBWKM7wFo-xrSnW4moyOwK782wJLjMohy5YMBST_PDS7kWU_V2cbD6tRn6WvF2Yc9QlCVNeDUCpxZDW1XLG5U9X7H4jznXG8AfqbdKWYWiqOxeA6bEtR7uPZw.ToWbVBuaoPR9tktTPFmrbw.S-ZnpWC14jMW2nwmC-LEAjwpXWwbIWtHfguPVJyjL8fvqD1V778MHC5r1m6IheR0TKktA9gJZTmNEj31SgCaMYH5cZcVKRFOrqy28AgsnHMioUS9_R6oWmtxtfEkRXF_9chBLIyzqYJ0oVmZ2WtBdrZpoyDLjYjbdOBL8__xv4brRdCf-q4nMMdGlJCZvp-lfZwgtPIRIlskyXBmymsUk8MIhCUP1R9b6EzULgjuA82B1vc71pdpRMYpZ4jonBL0EQ2ZkPlxUajpE6tB_Vsk2SeB64MBUVeo846vpgu_uaFPX5rpyD5j7T8gLYAtHqVwaQeLu1neREMxodlcsb8Y2RQf8VDWgujLeJuquJ62QpsGyjxNn1rzjVtLDiRaPHfZhOs2lD9oyqTJlrck_XJIckBZ61qNBGhSYBpjnNwdhupxPbDkTuMOuTvhDh3wEXHyi23Ceuq1rkAOQZ2p1PTaZOrCQZjoc_lYG8sMRieo-8h46sW6B7tNDMhYGJbyfs1ZEx2WMdxA2GkgUqsRexPyK0-sODPRZtRpt5iIjCmpTCeaTcALZ9EHu334X6OEpY0IyypNfi3yEhENWTL8orVqPnw2Oe8biF0mr9o1eKbzdly3bt8C2FXMQTY-p8O8XRrK.1DQYCWAXc4pwfd-UA491pw\",\"homePhone\":{\"cc\":\"123\",\"subscriber\":\"123456789\"},\"threeDSRequestorID\":\"199\",\"billAddrCity\":\"City Name\"," +
                "\"deviceRenderOptions\" : {\"sdkInterface\" : \"03\", \"sdkUiType\": [\"01\",\"02\",\"03\",\"04\"]}," +
                "\"sdkEphemPubKey\": { \"kty\": \"EC\", \"crv\": \"P-256\"," +
                "\"x\": \"ICx18uCQCh-UzddkGmmQw0rsBlQA4ZS3GKtYzZYv5IQ\", \"y\": \"DIgC1jg0eIW8Sv7THW_dfXNc4E5bvZSASeF3VYmsCiw\"  }," +
                "\"messageExtension\": [" + "{" + "\"name\": \"testExtensionNonCriticalField\"," + "\"id\": \"ID3\"," +
                "\"criticalityIndicator\": true," + "\"data\": {" + "\"text\": \"Value1\"" + "} } ] }," +
                "\"broadInfo\": {\"field1\" : \"value1\", \"field2\" : \"value2\", \"values\" : [1,2,3,4,5], \"sub\" : { \"sub1\" :\"subval1\", \"num1\" : 22 } }}," +
                "\"billAddrLine2\":\"Address Line 2\",\"deviceChannel\":\"01\",\"deviceRenderOptions\":{},\"billAddrLine3\":\"Address Line 3\",\"billAddrLine1\":\"Address Line 1\",\"acctInfo\":{\"chAccAgeInd\":\"5\",\"chAccDate\":\"20170101\",\"chAccChangeInd\":\"4\",\"chAccChange\":\"20170101\",\"chAccPwChangeInd\":\"5\",\"chAccPwChange\":\"20170101\"," +
                "\"shipAddressUsageInd\":\"4\",\"shipAddressUsage\":\"20170101\",\"txnActivityDay\":\"1\",\"txnActivityYear\":\"1\",\"provisionAttemptsDay\":\"0\",\"nbPurchaseAccount\":\"1\",\"suspiciousAccActivity\":\"1\",\"shipNameIndicator\":\"1\",\"paymentAccInd\":\"5\",\"paymentAccAge\":\"20170101\"},\"threeDSRequestorChallengeInd\":\"01\",\"threeDSRequestorAuthenticationInfo\":{\"threeDSReqAuthMethod\":\"01\",\"threeDSReqAuthTimestamp\":\"201712070525\",\"threeDSReqAuthData\":\"00\"}," +
                "\"shipAddrCity\":\"City Name\",\"mobilePhone\":{\"cc\":\"123\",\"subscriber\":\"123456789\"},\"transType\":\"01\",\"threeDSRequestorNPAInd\":\"01\",\"cardholderName\":\"Frictionless One\",\"acctType\":\"02\",\"shipAddrCountry\":\"840\",\"workPhone\":{\"cc\":\"123\",\"subscriber\":\"123456789\"},\"threeDSServerURL\":\"http://localhost:8080/v2.0.1/3dsServer/199/\",\"threeDSServerTransID\":\"cd657e3f-fd17-4d4b-b018-781c31337ab5\"}";


        JsonAndMessage jsonObj = mse.parseJsonAndMsg(new String[]{emptyStruct});
        TDSMessage mex = (TDSMessage) jsonObj.message;
        mex.setBroadInfo(jsonObj.json.get("broadInfo"));
        //TDSMessage mex=(TDSMessage)mse.fromJSON(jsonObj);
        //TDSMessage mex= (TDSMessage)mse.fromJSON(new StringReader(emptyStruct));
        System.out.println("Duplicates " + mex.getDuplicateCounts());
        System.out.println("Dro " + mex.getDeviceRenderOptions().getSdkInterface() + " " +
                           mex.getDeviceRenderOptions().getSdkUiType());
        System.out.println("SdkEph " + mex.getSdkEphemPubKey());
        System.out.println("Met " + mex.getMessageType());
        if (mex.getMessageExtension() != null && mex.getMessageExtension().size() > 0) {
            System.out.println("Mext '" + mex.getMessageExtension().get(0).getCriticalityIndicator() + "' '" +
                               mex.getMessageExtension().get(0).getId() + "'");
        }

        System.out.println("BroadInfo " + mex.getBroadInfo());
        System.out.println("Back " + new String(mse.toJSON(mex), StandardCharsets.UTF_8));

        JsonObjectBuilder job = Json.createObjectBuilder();
        for (Entry<String, JsonValue> e : jsonObj.json.entrySet()) {
            job.add(e.getKey(), e.getValue());
        }

        //JsonString js = job.add("MyProperty", "MyValue").build().getJsonString("MyProperty");
        //jsonObj.json.put("MyProperty", js);

        job.add("MyProperty", "MyValye");

        System.out.println("Back2 " + new String(mse.toJSON(job.build()), StandardCharsets.UTF_8));


        if (true) {
            return;
        }

        Map<String, AtomicInteger> dups = new java.util.HashMap<>();
        //org.glassfish.json.JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        //  \"acquirerBIN\" : \"4123457\",  \"acquirerBIN\" : \"4123457\",
        byte[] jsonbde = "{\"TDSMessage\": {\"acquirerBIN\" : \"412345\",  \"acquirerMerchantID\" : \"771133\", \"browserAcceptHeader\" : \"ssss\", \"messageType\" : \"AReq\" , \"messageVersion\" : \"2.0.0\" }}".getBytes(
                StandardCharsets.UTF_8);
        byte[] jsonbde2 = "{\"acctNumber\" : \"401600010120112345\", \"acquirerBIN\" : \"412341\",\"acquirerBIN\" : \"412345\", \"acquirerBIN\" : \"4123457\", \"acquirerMerchantID\" : \"771133\", \"browserAcceptHeader\" : \"ssss\", \"messageType\" : \"AReq\" , \"messageVersion\" : \"2.0.0\", \"threeDSRequestorID\":\"1221212\" }".getBytes(
                StandardCharsets.UTF_8);
        System.out.println("Dup el checks");
        TDSMessageBase tdxx = mse.fromJSON(jsonbde2); //WrapRoot
        System.out.println("ACQMID=" + tdxx.getAcquirerMerchantID());
        System.out.println("ACQBIN=" + tdxx.getAcquirerBIN());
        System.out.println("REQUESTOR=" + tdxx.getTdsRequestorID());
        System.out.println("DUPS=" + tdxx.getDuplicateCounts());

        //String si="aKW-siPDLKH4f84CqWiyVhSfqqxxkjEURdAS6q3-gQx9J9jaM-GYk4eLsjAImYRvt4ybJdUVF1PxYGT8V_HrfTSfLX2_vyBl23GrL5ntJhzKiUFJNlEfjN3XTXSNWO4jWd464wi44dpIqKkcZTCkrAFUZpGU3hRVfxORFQSkMq5pvNsXS4db0v3GxVSJGdWckoX0HAdzPTlF3FvMCvbe1Q1Ae2BMJvokqIyGKvNEkBpQxMIUMT36yX-NzLk4rtB0pznxkcYxJEFTsyfMnsvjMOmgOSLTHfg798FaJ2EFkw4A7fFaLkY63B2AwH1PT6IZ7aR0Nbas5Z-iRil4mx2EOw";
        //System.out.println("SI="+new String(Base64.decodeURL(si), StandardCharsets.UTF_8));
        String pl = "eyJhY3NVUkwiOiJodHRwczpcL1wvM2RzMi1hY3MuY2kubW9kaXJ1bS5jb21cL2Fjc1wvYnJvd3NlciIsImFjc0VwaGVtUHViS2V5IjoiTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFYjZWREhNV1hlTWdVU09oRUYxVnllcm5UcXBOcVNrT1czMVRja3V6U0hOaFRJcjhpRHBNeHlIVkI1Sng4bEFMa2tCQXZRak5UMTQ3Rlk0Z3JueE02SFE9PSIsImF1dGhlbnRpY2F0aW9uVHlwZSI6bnVsbCwic2RrRXBoZW1QdWJLZXkiOm51bGx9";
        System.out.println("PL=" + new String(Base64.decodeURL(pl), StandardCharsets.UTF_8));


        TDSMessage m = new TDSMessage();
        //m.setProtocol("ThreeDSecurev2");
        m.setMessageType(TDSModel.XmessageType.A_REQ.value());
        m.setTdsServerTransID(createUUID(Long.MAX_VALUE - 9999999999L, 123));
        m.setTdsServerRefNumber("BindTest");
        m.setTdsServerURL("https://miserver.com/service");
        m.setDsTransID(createUUID(-1, 123456));
        m.setMessageVersion("2.0.0");
        m.setAcquirerBIN("412345");
        m.setMcc("5544");
        m.setMerchantName("Merchant 1");
        m.setTdsRequestorID("771177");
        m.setTdsRequestorName("Merchant 1");
        m.setAcquirerMerchantID("771177AAAAAAAAAAAAAAAAAAAAAAAAAA"); //AAAAAAAA
        m.setMessageCategory("01");
        //m.setIssuerBIN("554433A");
        m.setAcctNumber("5544331221281928");
        m.setPurchaseDate("20160223 23:15:56");
        m.setRecurringExpiry("20171231");

        XrenderObj1 ifo = new XrenderObj1();
        ifo.setSdkInterface("1");
        ifo.getSdkUiType().add("1");
        ifo.getSdkUiType().add("2");
        ifo.getSdkUiType().add("3");

        m.setDeviceRenderOptions(ifo);

        XrenderObj2 acr = new XrenderObj2();
        acr.setAcsInterface("01");
        //acr.setAcsInterface(value);.setSdkUiType("01");
        m.setAcsRenderingType(acr);

        XImage xim = new XImage();
        xim.setHigh("https://www.bank1.com/image1.jpg");
        xim.setMedium("https://www.bank1.com/image2.jpg");
        xim.setExtraHigh("https://www.bank1.com/image3.jpg");
        m.setIssuerImage(xim);
        XImage xim2 = new XImage();
        xim2.setHigh("https://www.bank3.com/image1.jpg");
        xim2.setMedium("https://www.bank3.com/image2.jpg");
        xim2.setExtraHigh("https://www.bank3.com/image3.jpg");
        m.setPsImage(xim2);


        m.getChallengeSelectInfo().add("select1");
        m.getChallengeSelectInfo().add("select2");
        m.getChallengeSelectInfo().add("select3");

        String certData = "-----BEGIN CERTIFICATE-----\r\n" +
                          "MIICgzCCAWsCBFbUk5kwDQYJKoZIhvcNAQELBQAwbTELMAkGA1UEBhMCRUUxCzAJBgNVBAgTAkhN\r\n" +
                          "MRAwDgYDVQQHEwdUYWxsaW5uMRgwFgYDVQQKEw9DYXJkIFNjaGVtZSBJbmMxCzAJBgNVBAsTAkRT\r\n" +
                          "MRgwFgYDVQQDEw8zRFMgU2NoZW1lIFJvb3QwHhcNMTYwMjI5MTg1MzEzWhcNMjAwMjA4MTg1MzEz\r\n" +
                          "WjBqMRUwEwYDVQQDEwxTREsgS2V5IDIwMTYxEDAOBgNVBAsTBzNEUyBTREsxEzARBgNVBAoTClNj\r\n" +
                          "aGVtZSBJbmMxEDAOBgNVBAcTB1RhbGxpbm4xCzAJBgNVBAgTAkhNMQswCQYDVQQGEwJFRTBZMBMG\r\n" +
                          "ByqGSM49AgEGCCqGSM49AwEHA0IABO2urd1zHsabt4C0pfKhJ/TQbMLFHRCWrXQr4CDOStGbBltW\r\n" +
                          "lzCFhPL0HFlaq3k5FMZZafnr1Lf15jG69+YWs1QwDQYJKoZIhvcNAQELBQADggEBAACDKJvEHDf9\r\n" +
                          "mTqjtZTWZ0W8zE5O22NpX1ty/aykAGZarUTXQF2KXWz+6ldpLewgCDp/osM1O+OFzr+nKlzUH0Qt\r\n" +
                          "bS67GSsJb3pdgfbNeUXY/P3BizHEOw89/HIGCuoBMWfelVk0Ca5aph+vFxfVvagYKQ+PnveDO4HI\r\n" +
                          "htaSIUgvmNrLYhh/j7KJtSjiK8yLuNR1T99QD3wVMphfJ9BpHo/5zv9nsnF2QDqmnp4gr8hcROJt\r\n" +
                          "oEj3N+ElGO7ZXfNOzV2c3AqOjLPeL52YL0m0jkGwoPzSfIDl7eJGGLpY6thhNGtL8wTpKZYr7NKa\r\n" +
                          "WFxjbS7dKHb1E/B8f/oj2Qqovrk=\r\n" + "-----END CERTIFICATE-----";

        X509Certificate sdkKeyCert = KeyService.parseX509Certificates(certData.getBytes(), "X509")[0];

        String rsaCertData = "-----BEGIN CERTIFICATE-----\r\n" +
                             "MIIDmTCCAoGgAwIBAgIFIexlo9owDQYJKoZIhvcNAQELBQAwdjELMAkGA1UEBhMCRUUxCzAJBgNV\r\n" +
                             "BAgMAkhNMRAwDgYDVQQHDAdUYWxsaW5uMRMwEQYDVQQKDApTY2hlbWUgSW5jMRAwDgYDVQQLDAcz\r\n" +
                             "RFMyIENBMSEwHwYDVQQDDBhTY2hlbWUgSW5jIDNEUyBDQSAyMDE2LTUwHhcNMTYwMzAzMTAxNDA4\r\n" +
                             "WhcNMjYwMzAxMjMyNTU5WjB2MQswCQYDVQQGEwJFRTELMAkGA1UECAwCSE0xEDAOBgNVBAcMB1Rh\r\n" +
                             "bGxpbm4xEzARBgNVBAoMClNjaGVtZSBJbmMxEDAOBgNVBAsMBzNEUzIgQ0ExITAfBgNVBAMMGFNj\r\n" +
                             "aGVtZSBJbmMgM0RTIENBIDIwMTYtNTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMrS\r\n" +
                             "nrHXyLJ5jhqdjnb0+YClKmQyeXGSJnG4YrYWdoQFPcjptEoG+Fh0fncGrSU601/uLs+8keaHfl/i\r\n" +
                             "Q48wnPtLb7I4GGNisGcsLCObePbXJhc1Qw2lHmBf5YVHx3eL+vZpf7+lSrF/Mm1uV4CqbYkhlwXn\r\n" +
                             "v3x1VtRmpiYjeWWNFjrtbS7PGe/1+OhEXn/ftpwtXvCP/HHiRtoi/zu44TV8zTu9zBIrW3iZXe1c\r\n" +
                             "SrxZ5yWekfkVZJVJuXxubOxkzgEVIKlOn3JhfbkJYILbziJujmFFGMbITlXqBdoPrgPl5JkJzfPP\r\n" +
                             "9vI1LEC8tDmbOMa9+R4hlJQ92qel1vTzHJMCAwEAAaMuMCwwDAYDVR0TBAUwAwEB/zALBgNVHQ8E\r\n" +
                             "BAMCAfYwDwYDVR0lBAgwBgYEVR0lADANBgkqhkiG9w0BAQsFAAOCAQEAgX86tasoswvAiIov81yb\r\n" +
                             "7QC76Gss+SworMlJ55bLLGR3MGmA2FoK+tPuzOTfia8WEOmA6OPSEV+HixB3oVe7JWCeFE9ZruTm\r\n" +
                             "kQYrplSlz7zJDeDa30JhvHdrvujUDHLfio8c6Lh6IEYUarrrfvxWXVVpTtFVHQXSCbHwLgi7VX/j\r\n" +
                             "JkJUz9jGWZ6X0C3h+el+E2HraGLowQ5tq+C7nibeZ3+WJtAHlkN3SFObjzuuaQ8C4SU5U3PgjBGf\r\n" +
                             "/yEBxiUpPqU25TeMi4Pd8vXzVZSL0IApjb8kVeKBNn9tUYcFUjHcR/suuIDfpkcXZSfHw1Ia/53G\r\n" +
                             "2azr5Y2xpSNJGj5o1A==\r\n" + "-----END CERTIFICATE-----";

        X509Certificate sdkRSAKeyCert = KeyService.parseX509Certificates(rsaCertData.getBytes(), "X509")[0];

        String juPublic = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwiFnN/l5cydGq1lCodlKybQywKZhGpJn" +
                          "nCvr3GOsFnl4bSwbERGbhyn21syyKrpLw4pbotwdgGlBZIe8qeZ7oDlM5daCTh+RhMRd+/LUClkS" +
                          "1K/brcl0kUOiWEm2s1/zPr6zKb3npexNOftM0eSaJ5/yaOk88I2Byo2hMHY+Xf+pDF1jIPRamc2t" +
                          "03eElbzuYGrP4TG10qZbn46RQTdewZ98ptYZThR5QDWz5MvFChPXN7vKKETwyWsrAfEoiPm6DWjR" +
                          "RcMgDV/KIOkXlJ4OfepIxVmxUTsZkycWGGXeUe9vzfmHOZdnLmyaBM0aHlNH17VxgEUOzzE5Sa5l" +
                          "n5q3qwIDAQAB";
        String juPrivate = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDCIWc3+XlzJ0arWUKh2UrJtDLA" +
                           "pmEakmecK+vcY6wWeXhtLBsREZuHKfbWzLIqukvDilui3B2AaUFkh7yp5nugOUzl1oJOH5GExF37" +
                           "8tQKWRLUr9utyXSRQ6JYSbazX/M+vrMpveel7E05+0zR5Jonn/Jo6TzwjYHKjaEwdj5d/6kMXWMg" +
                           "9FqZza3Td4SVvO5gas/hMbXSplufjpFBN17Bn3ym1hlOFHlANbPky8UKE9c3u8ooRPDJaysB8SiI" +
                           "+boNaNFFwyANX8og6ReUng596kjFWbFROxmTJxYYZd5R72/N+Yc5l2cubJoEzRoeU0fXtXGARQ7P" +
                           "MTlJrmWfmrerAgMBAAECggEAO1vJWuUuTJaa1jPh2uk9I/W0exGkqWJQygDoCqhzax/TQdZ1pD91" +
                           "Bdw//Kf0Zno2RmEc6exSHPySITIb7vtpm7FJoRF5IHgon6cHcc1rJorb5iqrijUKLoy2rDZF5g0V" +
                           "rH7U+5N0rFhXuL3tXFSBClKu5YNDve4W3J93xQCyyzyjSu1lGQuj31LvfJv91/B6GWj3FVQmWsFw" +
                           "f19/z461MrK7aLdg2azrpVezIbyiyBMQn2bJ51/2iOIWBjMbtxOBMUMzAXz46g0asHQ2FjR/R77b" +
                           "bjPnDy5zsKE1RgJY8VIzHrbe/71nvDhpahll7FsLJWPl5XGcTyU0GcRNkXRaAQKBgQDz7TIDP1Hy" +
                           "9QRXRlhYSr3ApIM1aoU3dkLFbvqVxkw3KbCql9pThAsVOvG9knJAnXJCSQB6tvScK7X5/6/HBFA/" +
                           "5wGfhzTww05Ew5W/ERBGl/W/E6qQRbrR6Wwi3rSLZc00QVxpkQqTGWwvP12SBQ7ZyRYq2unHJW9N" +
                           "56DI9Ju/AQKBgQDLvT1WA22HogrTW16VMKnKGj920TvB6rF/q2+lzAIaEn97+Wi5XwKKBemfnJAp" +
                           "mN+OvlJVYYaGC3nnR7Y8WmYzOcclAvdeoi2Qyoc+eRIf49UyxYhG1SL1pVmHTHOLUN0MxJtoW/vd" +
                           "CsreWoyEnT9Gb59zfrFSxI2W7WLKwDQiqwKBgEXbi3Q0oTKUiy63fbujhB50vwMAQ7CzlMHYW7mj" +
                           "NdaigO8KWEOM0GO/ItZo54Gr1bo4EAQU+47yUcaMPHohuR3j+aoHXdkKBhNZdDxpFaEyCfY0GqgA" +
                           "4FUXpZmC8G/vssohYeb12/Ju2HXbjt+vxSYIGeoiEcuv4+WKzZhfvW8BAoGBAIPHCeYZX33L5H6i" +
                           "aYHLsPZklcnvOVyKxXXTV+qAEpf0gPqZ7g+AEW9etaYkKOaO14JHPY+1/cGzqIxcSNWujs3xQAqD" +
                           "aaaO9TPimylgO8XeoiYtt6BA+VSzNz4Yx99DVUaN4ArW/AgTk/XP0XVxE90mE5crSdxD/2SwdCa5" +
                           "vL7zAoGAMpriZMXZs2c+EjDMky8wsYuRdLCcayjcFKe8l803nyMIqEf6oDuV4e/3gGsEcjpTFUSh" +
                           "/8E5crBn0uk7CHUR/i0oajTBf1hpoppzC8XYHkXaWhyfJr3P4Sgo3djtIq5ZUOrdBg/+fRsudQty" +
                           "tnWKtrxUkqrfHesVPOgplWHV9TU=";


        //m.setCardRangeData("\"10000000000000000\",\"10000000000000500\",\"2.0.0\", \"www.acsurl.com/script\"");
		/*
		List<String> crd = new java.util.ArrayList<String>(4);
		crd.add("10000000000000000");
		crd.add("10000000000000500");
		crd.add("2.0.0");
		crd.add("www.acsurl.com/script");

		//StringArray sa=new StringArray();
		//sa.getValue().addAll(crd);
		//m.getCardRangeData().add(new ObjectFactory().createTDSMessageCardRangeData(crd));
		m.getCardRangeData().addAll(crd);
		List<String> crd2 = new java.util.ArrayList<String>(4);
		crd2.add("20000000000000000");
		crd2.add("20000000000000500");
		crd2.add("2.0.0");
		crd2.add("www.acsurl.com/script2");
		m.getCardRangeData().addAll(crd2);
		*/
        XcardRangeData crd = new XcardRangeData();
        crd.setStartRange("20000000000000000");
        crd.setEndRange("20000000000000500");
        crd.setDsStartProtocolVersion("2.0.1");
        crd.setDsEndProtocolVersion("2.0.1");
        crd.setThreeDSMethodURL("www.acsurl.com/script2");
        m.getCardRangeData().add(crd);

        XcardRangeData crd2 = new XcardRangeData();
        crd2.setStartRange("10000000000000000");
        crd2.setEndRange("10000000000000500");
        crd2.setDsStartProtocolVersion("2.0.1");
        crd2.setDsEndProtocolVersion("2.1.0");
        crd2.setThreeDSMethodURL("www.acsurl.com/script");
        m.getCardRangeData().add(crd2);

        //StringArray sa2=new StringArray();
        //sa2.getValue().addAll(crd);


        String devda = ("{" + "\"DeviceData\" : {" + "\"Android_ID\" : \"12c123e12321f3a12d\"," +
                        "\"Build_Release\" : \"4.4\"," + "\"Build_Serial\" : \"123e12ab\"," +
                        "\"Build_SDK\" : \"KITKAT\"," + "\"Wifi_MAC\" : \"01:02:03:04:05:06\"," +
                        "\"TeleMan_GetID\" : \"123456789123456789\"," + "\"SIM_Serial\" : \"123456789123456789\"" +
                        "}" + "}");

        CryptoService crs = new CryptoService();
        boolean juha = true;
        if (juha) {
            PKCS8EncodedKeySpec priKeySpec = new PKCS8EncodedKeySpec(Base64.decode(juPrivate));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey juPriKey = keyFactory.generatePrivate(priKeySpec);
            X509EncodedKeySpec puKeySpec = new X509EncodedKeySpec(Base64.decode(juPublic));
            PublicKey juPubKey = keyFactory.generatePublic(puKeySpec);


            String sdkdj = "{\"C01\":\"Android\",\"C02\":\"LGE Nexus 5X\",\"C03\":\"N\",\"C04\":\"7.0\",\"C05\":\"en_US\",\"C06\":\"Europe/Helsinki\",\"C07\":\"RE03\",\"C08\":\"1080x1794\",\"C09\":\"Nexus 5X\",\"C10\":\"10.0.1.10\",\"C11\":\"60.41722\",\"C12\":\"24.3219295\",\"SW\":[\"SW04\"]}";
            //String localJWE=mse.toCompactJWE(crs.encryptJWE(juPubKey, CryptoService.JWE.enc.A128CBC_HS256, CryptoService.JWE.alg.RSA_OAEP.javaAlg, null, sdkdj.getBytes(StandardCharsets.UTF_8)));
            String localJWE = crs.encryptJWEComp(juPubKey, CryptoService.JWE.enc.A128CBC_HS256,
                                                 CryptoService.JWE.alg.RSA_OAEP.javaAlg, null,
                                                 sdkdj.getBytes(StandardCharsets.UTF_8), true);
            System.out.println("compact jwe " + localJWE);
            XJWE luwe = mse.fromCompactJWE(localJWE.getBytes(StandardCharsets.UTF_8));
            System.out.println("aad=" + luwe.getAad());
            System.out.println("ekey=" + luwe.getEncryptedKey());
            System.out.println("head=" + luwe.getHeader());
            System.out.println("tag=" + luwe.getTag());
            System.out.println("cipte=" + luwe.getCiphertext());
            System.out.println("pro=" + luwe.getProtected().getAlg() + " " + luwe.getProtected().getEnc());

            //System.out.println("UNCOMP=" + new String(mse.toJSON(mse.fromCompactJWE(localJWE.getBytes(StandardCharsets.UTF_8))), StandardCharsets.UTF_8));


//            byte[] plainDataL = crs.decryptSdkEncData(juPriKey, null, localJWE, false);
//            System.out.println("LJWE decrypted=" + new String(plainDataL, StandardCharsets.UTF_8));
            System.out.println("==================");
		/*
		JWTClaimsSet.Builder sdkEncryptData = new JWTClaimsSet.Builder();
		//sdkEncryptData.audience(sdkdj);
		//sdkEncryptData..
		JWTClaimsSet cset=JWTClaimsSet.parse(sdkdj); ////sdkEncryptData.build();
		EncryptedJWT jwt =new EncryptedJWT(new JWEHeader(JWEAlgorithm.RSA_OAEP, EncryptionMethod.A128CBC_HS256), cset);
		//EncryptedJWT jwt =new EncryptedJWT(new JWEHeader(JWEAlgorithm.RSA_OAEP, EncryptionMethod.A128CBC_HS256).toString(), null, null,sdkdj, null);
		jwt.encrypt(new RSAEncrypter((RSAPublicKey)juPubKey));
		localJWE=jwt.serialize();
		System.out.println("jwt2full=" + localJWE);
		String[] px=Misc.split(localJWE, ".");
		for(int i=0; i<px.length; i++)
		{
			System.out.println("p" + i+" len="+px[i].length()+ "decoded len="+Base64.decodeURL(px[i]).length);
		}
//eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkExMjhDQkMtSFMyNTYifQ.
//aSGgbBvrz47UORBON53VjZ2HQNsnEox9KtMT4WGbMg5HUVUfs-I_LVC_i8lQQHmjF8BKTFe6u1isY4SptOylsxas5BPMIRc6EPM7EiFFKKwWepSrPAoiq_dnTxFqZ-3Uwx9pu55HikIJyeqAdv0X2COc2Fpx4-51-n3Js1F5CpcLx2JwnGazF9I9RmYm7SRMdTGEYDFYNBSujHLBQrdvPBGqNCSjtZFhw24DyojPMvEJnDmo84HtzQy0FGanmIdyMc1nQ0GnrSlzJpH-ca_32JWJ7yUUdd5ifQgL082kX0S-oDLTl-LJzi-qbHsZj5dRDQmMasboiwYtLte6VUXH_Q.
//_2eH18X6BZV451zPifAzoA.
//HyIDOcur42Ad7tj8Xw7pa5SaTqy5P2wNvq7D61y-kJFH_8wU4ei-c-siCK5ZITpVEkuLLgCu9YbrGpkZLKLy6sQpVKIfCMNsixMM1esmbowByrAPYMjfMIeqsTTqoIU_ytaTocdlBGYTLHa-fNEEQ1pyfzWkOvs2_BSeV5kwI2iR7ONUtRoMwxzVf_-eB4iJCFeLeCklBUrFyi7AXu8GCTrYoz0ljSypVR-6NB9AqNHKAfpGkXLyYTrdlYw6hiBSfGO_svdljRC2kwkwpHoXfML7jHB5Ln3m7_3iMgTHO04.
//Z1eDfSZ7bZbYtWmfa7gi7w
		String[] parts=Misc.split(localJWE,".");
		//EncryptedJWT jwt2 =new EncryptedJWT(parts[0],parts[1],parts[2],parts[3],parts[4]);
		EncryptedJWT jwt2=EncryptedJWT.parse(localJWE);
		jwt2.decrypt(new RSADecrypter(juPriKey));
		System.out.println("jwt2ek=" + jwt2.getEncryptedKey());
		System.out.println("jwt2ct=" + jwt2.getPayload().toString());

		luwe=mse.fromCompactJWE(localJWE.getBytes(StandardCharsets.UTF_8));
		System.out.println("aad=" + luwe.getAad());
		System.out.println("ekey=" + luwe.getEncryptedKey());
		System.out.println("head=" + luwe.getHeader());
		System.out.println("tag=" + luwe.getTag());
		System.out.println("cipte=" + luwe.getCiphertext()+ " dec len="+Base64.decodeURL(luwe.getCiphertext()).length);
		System.out.println("pro=" + luwe.getProtected().getAlg()+" "+luwe.getProtected().getEnc());

		byte[] plainDataL1 = crs.decryptJWE(juPriKey, luwe);
		System.out.println("ldec=" + new String(plainDataL1));
*/
            String juhaJWE = "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiUlNBLU9BRVAifQ.WbK5MOMMUAubZTIEb1wlFVG8jzhuwg9Yex8U5t3ZV6T17aT_RDFKzmwjqcgTd4a4M1tN1DkWQEeXIaHYuKAWkwSCBdL2jHrn0VnsW_LGoyMtUSRBCSMgq-yV4ZAPYDsujzWaN8w2xdLzRwWS474cN7I1Aie1gV0UP-f50Tzi4DFDBnt05kYWFRuuqzLWWyrdJXyZmLLXrkIDbsnvVLVJviffVLwud1Gz8-7tbPHzazRVErxK8u1v83g-YuxvfNDu1tRPrUmSKJfYxWApTlOwQ8tFuDmoGjsLUZwGrlZ-PGMS7P_XjK9QB5LSJdccGuYwCWaz-om3KZRySzx3hvWoLQ.DYdgZgJRof9yT9OI8LG7aA.lmGYLgwqspJcXbIlyBzv4UqL7slVd0vL9nhD5vqO052AN8V85B1fOA3KpCHHSa9vVXiVrBjzlPBFxGsXqktE-vyV49SYNcJO0eDCQUa8U_S_TgDivzZc-d-kGmtRDW0qNoikC65fHT6hY9uBs5nUuor5YgJvJ94SeVoj7OUEatTfw39tvvUXF_oTR8WRtF6dY5zqH-Ek1hsJ-ZrvjuBaPqp3cP9bfqlv644RGZYILyNPPDqvjqXftwcMCQL7IMvcY5GhnD9lhTx_EE5LSa0JBD3CntO-EnHumJHhMJfZ0LNwun2WlR-uICgejNzYGtFV8FzXfRTb_qmBUNnhKoufwOzn6hqY78pX4pa-Y1J4pGlzbITXzrsg2_o3VzMyeeke.w15pH-icik6kd53QtR_W8w";

            XJWE juwe = mse.fromCompactJWE(juhaJWE.getBytes(StandardCharsets.UTF_8));
            System.out.println("aad=" + juwe.getAad());
            System.out.println("ekey=" + juwe.getEncryptedKey());
            System.out.println("head=" + juwe.getHeader());
            System.out.println("tag=" + juwe.getTag());
            System.out.println("cipte=" + juwe.getCiphertext());
            System.out.println("pro=" + juwe.getProtected().getAlg() + " " + juwe.getProtected().getEnc());

//            byte[] plainDataJuha = crs.decryptSdkEncData(juPriKey, null, juhaJWE, false);
//            System.out.println("JWE decrypted=" + new String(plainDataJuha, StandardCharsets.UTF_8));

            String jwe = crs.encryptJWEComp(sdkRSAKeyCert.getPublicKey(), CryptoService.JWE.enc.A256GCM,
                                            CryptoService.JWE.alg.RSA_OAEP.javaAlg, null,
                                            devda.getBytes(StandardCharsets.UTF_8), false);
            m.setSdkEncData(jwe);
            //byte[] xjwebuf = mse.toJSON(jwe);
            //System.out.println("JWE to json=" + new String(xjwebuf));
            //System.out.println("JWE compact=" + m.getSdkEncData()+"\n\n");

            XJWE jwe2 = mse.fromCompactJWE(m.getSdkEncData().getBytes());
            System.out.println("JWE back header=" + jwe2.getProtected() + " " + jwe2.getHeader() + "\n\n");

        }

        java.security.SecureRandom sr = new java.security.SecureRandom(); //.getInstance("SHA1PRNG");
        sr.setSeed(("sejrhui4h" + new java.util.Date() + "s5NNSG((Jssdjskdj" + Math.random() + "3xa2" +
                    Runtime.getRuntime().totalMemory() + "Zsld9Bsl" + System.currentTimeMillis()).getBytes());
        java.security.KeyPairGenerator kpg = null;

        kpg = java.security.KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048, sr);
        java.security.KeyPair keyPair = kpg.genKeyPair();

        KeyService.getBouncyCastleProvider();
        java.security.KeyPairGenerator kpgec = java.security.KeyPairGenerator.getInstance("ECDSA", "BC"); //"ECDH"
        AlgorithmParameterSpec params = ECNamedCurveTable.getParameterSpec("prime256v1");
        kpgec.initialize(params, sr);

        java.security.KeyPair keyPairEC = kpgec.genKeyPair();


        //System.out.println("Public "+Base64.encode(keyPair.getPublic().getEncoded()));
        //System.out.println("Private "+Base64.encode(keyPair.getPrivate().getEncoded()));
        //String s1="eyJlcGsiOnsia3R5IjoiRUMiLCJjcnYiOiJQLTI1NiIsIngiOiJwSUthZy1sWTNfRl9sLVRuWVZoZFNQUTh2NW0zSy12QVByRC1yX3V4QWd3IiwieSI6IlVzMlBlTzR3RkZkMzhtWTRxT19rUW5abm1zLVBESXV2WUJOUUN4WG5SVDAifSwiYXB2IjoiUkZNeCIsImVuYyI6IkExMjhDQkMtSFMyNTYiLCJhbGciOiJFQ0RILUVTIn0";
        //String s2="ew0KICAgImFsZyIgOiAiRUNESC1FUyIsDQogICAiYXB2IiA6ICJSRk14IiwNCiAgICJlbmMiIDogIkExMjhDQkMtSFMyNTYiLA0KICAgImVwayIgOiB7DQogICAgICAia3R5IiA6ICJFQyIsDQogICAgICAiY3J2IiA6ICJQLTI1NiIsDQogICAgICAieCIgOiAicElLYWctbFkzX0ZfbC1UbllWaGRTUFE4djVtM0stdkFQckQtcl91eEFndyIsDQogICAgICAieSIgOiAiVXMyUGVPNHdGRmQzOG1ZNHFPX2tRblpubXMtUERJdXZZQk5RQ3hYblJUMCINCiAgIH0NCn0";
        //System.out.println("s1="+new String(Base64.decodeURL(s1)));
        //System.out.println("s2="+new String(Base64.decodeURL(s2)));


        String[] jweencs = {CryptoService.JWE.enc.A128GCM, CryptoService.JWE.enc.A192GCM, CryptoService.JWE.enc.A256GCM, CryptoService.JWE.enc.A128CBC_HS256, CryptoService.JWE.enc.A192CBC_HS384, CryptoService.JWE.enc.A256CBC_HS512};
        //new CryptoService.JWE.alg[] { CryptoService.JWE.alg.ECDH_ES };
        CryptoService.JWE.alg[] algs = CryptoService.JWE.alg.values();
        String apv = "DS1";
        for (boolean alllib : new boolean[]{false, true}) {
            for (String jwealg : jweencs) {
                for (CryptoService.JWE.alg algx : algs) {
                    System.out.println("JWE enc=" + jwealg + " alg " + algx.algv + " allib=" + alllib);

                    if (algx == CryptoService.JWE.alg.ECDH_ES && !(CryptoService.JWE.enc.A128CBC_HS256.equals(jwealg) ||
                                                                   CryptoService.JWE.enc.A128GCM.equals(jwealg))) {
                        continue;
                    }

                    java.security.KeyPair kpx = (algx == CryptoService.JWE.alg.ECDH_ES ? keyPairEC : keyPair);

                    if (algx == CryptoService.JWE.alg.ECDH_ES) {
                        apv = "DS1";
                    } else {
                        apv = null;
                    }

                    //XJWE jwe1 = crs.encryptJWE(kpx.getPublic(), jwealg, algx.javaAlg, apv, devda.getBytes(StandardCharsets.UTF_8));
                    //String jwec=mse.toCompactJWE(jwe1);
                    String jwec = crs.encryptJWEComp(kpx.getPublic(), jwealg, algx.javaAlg, apv,
                                                     devda.getBytes(StandardCharsets.UTF_8), alllib);

                    //System.out.println("JWE plain=" + new String(mse.toJSON(jwe1)));
                    System.out.println("JWE compact=" + jwec);
                    //System.out.println("Uncompact=" + new String(mse.toJSON(mse.fromCompactJWE(jwec.getBytes(StandardCharsets.UTF_8))), StandardCharsets.UTF_8));
                    // decypt..
//                    byte[] plainData = crs.decryptSdkEncData(keyPair.getPrivate(), keyPairEC.getPrivate(), jwec, alllib);
//                    System.out.println("JWE decrypted=" + new String(plainData));
                }
                //byte[] plainData2 = crs.decryptJWE(keyPair.getPrivate(), mse.fromCompactJWE(jwec.getBytes(StandardCharsets.ISO_8859_1)));
                //System.out.println("JWE compact decrypted=" + new String(plainData2));
            }
        }

        if (!juha) {
            return;
        }

        //List<JAXBElement<List<String>>>  me=m.getMessageExtension();
        List<XmessageExtension> el = m.getMessageExtension(); //new java.util.ArrayList<String>();
        XmessageExtension e1 = new XmessageExtension();
        e1.setName("Extension 1");
        e1.setId("1");
        e1.setCriticalityIndicator("true");
        //Xany8059 a=new Xany8059();
        //a..setAny("value1");
        //e1.setData(a);
        el.add(e1);

        XmessageExtension e2 = new XmessageExtension();
        e2.setName("Extension 2");
        e2.setId("2");
        e2.setCriticalityIndicator("false");
        //e2.setData("a");
        el.add(e2);

        //me.add(new JAXBElement(e1));
        //m.setMessageExtension("[ \"id\", \"criticality indicator\", \"data\" ]");

        Map<String, Object> pm = new java.util.HashMap<String, Object>();
        pm.put("eclipselink.media-type", "application/json");
        pm.put("eclipselink-oxm-xml", "tds20special.xml");
        pm.put("eclipselink.json.include-root", false);
        pm.put(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);
        pm.put(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        JAXBContext jc = org.eclipse.persistence.jaxb.JAXBContextFactory.createContext(new Class[]{TDSMessage.class},
                                                                                       pm);

        Marshaller marshaller = jc.createMarshaller();
		/* 	marshaller.setProperty("eclipselink.media-type", "application/json");
			marshaller.setProperty("eclipselink.json.include-root", true);
		 */
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        marshaller.marshal(m, sw);
        String json = sw.toString();
        System.out.println("JSON:\n" + json);

        String dv = null;
        String[] jsona = new String[]{json};
        long sv = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++) {
            dv = MessageService.getMessageVersion(jsona);
        }
        long ev = System.currentTimeMillis();
        System.out.println("Version detected 10000x " + sv + " in " + (ev - sv) + " ms");

        Unmarshaller unmarshaller = jc.createUnmarshaller();

        //unmarshaller.setProperty("eclipselink.media-type", "application/json");
        //unmarshaller.setProperty("eclipselink.json.include-root", true);
        //unmarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        //TDSMessage tds2=(TDSMessage)unmarshaller.unmarshal(new StringReader(json));
        TDSMessage tds2 = unmarshaller.unmarshal(new javax.xml.transform.stream.StreamSource(new StringReader(json)),
                                                 TDSMessage.class).getValue();
        System.out.println("TdsServerTransID: " + tds2.getTdsServerTransID());
        System.out.println("OBJ: " + tds2.getDsTransID());
        System.out.println("OBJ ABIN: " + tds2.getAcquirerBIN());
        System.out.println("OBJ: " + tds2.getCardRangeData());
        if (tds2.getDeviceInfo() != null) {
            System.out.println("OBJ: " + new String(Base64.decode(tds2.getDeviceInfo()), "UTF-8"));
        }

        if (m.getDeviceRenderOptions() != null) {
            System.out.println("dro1: " + m.getDeviceRenderOptions().getSdkInterface() + " " +
                               m.getDeviceRenderOptions().getSdkUiType());
            System.out.println("acr1: " + m.getAcsRenderingType().getAcsInterface() + " " +
                               m.getAcsRenderingType().getAcsUiTemplate());
        }

        byte[] jsonb1 = mse.toJSON(m);
        System.out.println("JSON=" + new String(jsonb1));
        TDSMessage tdx1 = (TDSMessage) mse.fromJSON(jsonb1);
        List<Error> errors = new LinkedList<>();
        mse.validateFieldSyntax(tdx1, errors);

        if (m.getDeviceRenderOptions() != null) {
            System.out.println("dro2: " + tdx1.getDeviceRenderOptions().getSdkInterface() + " " +
                               tdx1.getDeviceRenderOptions().getSdkUiType());
            System.out.println("acr2: " + tdx1.getAcsRenderingType().getAcsInterface() + " " +
                               tdx1.getAcsRenderingType().getAcsUiTemplate());
        }

        errors.clear();
        mse.validateFieldSyntax(tdx1, errors);
        for (Error e : errors) {
            System.out.println("e: " + e.getFieldName() + ": " + e.getDescription());
        }

        System.out.println("ab: " + tdx1.getAcquirerBIN());

        String acsSignedContent = "eyJhbGciOiJSUzI1NiIsIng1YyI6WyJNSUlFM2pDQ0E4YWdBd0lCQWdJVUdRcFAzcW9MV1JNRHBSY3RnVFwvbGFTTk9cL0VFd0RRWUpLb1pJaHZjTkFRRUxCUUF3WGpFTE1Ba0dBMVVFQmhNQ1ZWTXhJVEFmQmdOVkJBb1RHRUZ0WlhKcFkyRnVJRVY0Y0hKbGMzTWdRMjl0Y0dGdWVURXNNQ29HQTFVRUF4TWpRVzFsY21sallXNGdSWGh3Y21WemN5QlRZV1psYTJWNUlFbHpjM1ZwYm1jZ1EwRXdIaGNOTVRjd05qSXdNak0wT0RJMFdoY05NakF3TmpJd01qTTBPREl6V2pDQnBqRUxNQWtHQTFVRUJoTUNWVk14RURBT0JnTlZCQWdUQjBGeWFYcHZibUV4RURBT0JnTlZCQWNUQjFCb2IyVnVhWGd4SVRBZkJnTlZCQW9UR0VGdFpYSnBZMkZ1SUVWNGNISmxjM01nUTI5dGNHRnVlVEVaTUJjR0ExVUVDeE1RVjJWaUlFaHZjM1JwYm1jZ1ZXNXBkREUxTURNR0ExVUVBeE1zWVdOekxtTnNhV1Z1ZEM1ellXWmxhMlY1TG5SbGMzUXlMbUZ0WlhKcFkyRnVaWGh3Y21WemN5NWpiMjB3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQzFHNzdVa21WNkdqeDZTTVN3NkYyVDZibkhJN05yTXRXdGFZKzlYdWpwMEF5eEpTUjFRY2F0OTlyVHFqYklmOG52b2lGSFVlWW1tRmNHQ2E1SnV0eXZUOUpuYkcxTGw0SzY3ZUx0TkJwK1lQSGxpV0RyNm5rM1BHMmhYVXFRREx4YWhOVVNIbkgya2V3WFI1cUxHSlFxUTRKeThaUVBnbGVtenVqZ21iYXZrbW0zeUpXWnpCVzZJYys0dWJ0TGlzUFJCc3ZkQlBmR2pHXC9kNkdPZGRjbUEzWlwvb1N1blVsa3Y3cHY2YW05b0tzUUs5d0lqMGIxd3VOc09lMU1sQitYd2JKK1B2VFp0RmNXN0o4Rm5lNFwvM24rdFc2RytHRHc5RGVycmhcL0lLc2tURG51QWJRV3k4amxSR25FWHhUeERra3RyWGZoemo2dWZiczMrYmNRb3BQM0FnTUJBQUdqZ2dGSk1JSUJSVEFNQmdOVkhSTUJBZjhFQWpBQU1EY0dBMVVkRVFRd01DNkNMR0ZqY3k1amJHbGxiblF1YzJGbVpXdGxlUzUwWlhOME1pNWhiV1Z5YVdOaGJtVjRjSEpsYzNNdVkyOXRNRkVHQ1NzR0FRUUJnamNVQWdSRUhrSUFRUUJOQUVVQVdBQmZBRk1BUVFCR0FFVUFTd0JGQUZrQVh3Qk5BRkFBU1FCZkFFTUFUQUJKQUVVQVRnQlVBRjhBUVFCVkFGUUFTQUJmQUZNQVNBQkJBREl3RGdZRFZSMFBBUUhcL0JBUURBZ1dnTUJNR0ExVWRKUVFNTUFvR0NDc0dBUVVGQndNQ01COEdBMVVkSXdRWU1CYUFGTzVQNjE3bFRJVXdjUWRjMG43VDRKaXhiZ3lJTUVRR0ExVWRId1E5TURzd09hQTNvRFdHTTJoMGRIQTZMeTloYldWNGMyc3VZM0pzTG1OdmJTMXpkSEp2Ym1jdGFXUXVibVYwTDJGdFpYaHpZV1psYTJWNUxtTnliREFkQmdOVkhRNEVGZ1FVSzJNRTJRSVBpOVZkcHZpZFNYOG9kaDNVQ0VVd0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFLZldCdzJ1RlQyWWplcDlZb1g5OCtySWJVMm0zbXhqZVMrczA0eHhTYjZocHB1R1lkQStaajlLZGF6ellzdTY2K3VEWURYcmRxU090RnJuUEV4VTlKek5hd0JzVEJ5eVFxR3k1bXJuUmlMc0oxcms0anpEOUdYdGRaSFU5Y29EMHFFWDJBcEZpMTNPOEZvVkhTWG10OE5XTDJlcFhEd2tyYk5RS0JoUEROQm5cLzdvUmt5MnhVTE42WUJvYjQxS3hYQzZSVnFpTm5zUjF1K3pQb3pkXC9XV0hcL3JnZFRMd3NFOWNxc1FVYVwvbjBvTnV0dHMwWW9pd2oxSTBETFVkOWkrK0RYWWZoU0VLRkFhamc0bVRqN3VZcVc3Zk1uemFmdnZua01tbXIxbUNockkxT0JnekFwNE5uT29ZYjBCYW84QU9ZZllwbXhzWVV0RklCUG5HdjRkWnJnPSIsIk1JSUVCRENDQXV5Z0F3SUJBZ0lVTjFDWGNGdExvWjIwWDZqZnk1bTRjZGdLZUFnd0RRWUpLb1pJaHZjTkFRRUxCUUF3YXpFTE1Ba0dBMVVFQmhNQ1ZWTXhJVEFmQmdOVkJBb1RHRUZ0WlhKcFkyRnVJRVY0Y0hKbGMzTWdRMjl0Y0dGdWVURTVNRGNHQTFVRUF4TXdRVzFsY21sallXNGdSWGh3Y21WemN5QlFjbWwyWVhSbElFTmxjblJwWm1sallYUnBiMjRnUVhWMGFHOXlhWFI1TUI0WERURTBNRGd4TVRFMU1EZ3lObG9YRFRJNU1EZ3hNVEUwTlRNd04xb3dYakVMTUFrR0ExVUVCaE1DVlZNeElUQWZCZ05WQkFvVEdFRnRaWEpwWTJGdUlFVjRjSEpsYzNNZ1EyOXRjR0Z1ZVRFc01Db0dBMVVFQXhNalFXMWxjbWxqWVc0Z1JYaHdjbVZ6Y3lCVFlXWmxhMlY1SUVsemMzVnBibWNnUTBFd2dnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0SUJEd0F3Z2dFS0FvSUJBUUNzWGJzNEdMcE9ERFJ2eEVlYVJWVzFaMWNOd3UwdEx1cHRLd0JSN0NKenJKbG5ibUxBejB1VVo5ZmFoZVRKMFVnUWJGMUhMaWtMMTJidU12ZU5LcEdMNndoNmkzQlN3cnJmMG9mTXY2cGFSdXFkUkU5ZmlTaFhCZWMrTFwvXC9KaDVSKzFDd3orbUQ4MjJQcHVjMklZWEZqMlFrM0pzdXcyUGZcLzk1MlJaNndPT2I3aTFhQmdPSzN0ZkUzVkFXdFZyUTBPeVBIRU03SFRHTGNJdTBcL2E3aTlXeTB4ZWR4Wk5rQXVGNFlYRkJyRVRmbHJyZUpXZG05REZVTUMrbnpwY2MrQjFORTRtejlXb3FuSE5qNW1Ldkt0VGpXeWxxeFdoRlA1T25hUXQ1MXEyb2E2bDc4cHBPTUplaFpEcnBVQjZ2UjBCcTU4SXJoRjBpelJiTUtjb3M3WlZBZ01CQUFHamdhd3dnYWt3RWdZRFZSMFRBUUhcL0JBZ3dCZ0VCXC93SUJBREFPQmdOVkhROEJBZjhFQkFNQ0FjWXdId1lEVlIwakJCZ3dGb0FVcGVvVERBSkFwQ0RiUzFveDVDVDZtcXRKKzJvd1F3WURWUjBmQkR3d09qQTRvRGFnTklZeWFIUjBjRG92TDJGdFpYaHpheTVqY213dVkyOXRMWE4wY205dVp5MXBaQzV1WlhRdllXMWxlSE5yY205dmRDNWpjbXd3SFFZRFZSME9CQllFRk81UDYxN2xUSVV3Y1FkYzBuN1Q0Sml4Ymd5SU1BMEdDU3FHU0liM0RRRUJDd1VBQTRJQkFRQjdOMVwvdUFHS090RUh3ZTFnZGplRTQwUnhGc1NVQjRyS0RYQ1Bla0E3XC82bUZkNHNJbHhhdWRnemljZ3lnK1BoaThGTFRyN3BJUjJvMlwvQ0x6dGVoZEdzVmVpUytVUGZqXC9OeWcwWW8yS21QZEFLbHQ2VDB0UTlSVzlQNk1BU1h2a0VVM1lOOFNrNW03RGZFNUp6N1ZaWTg2RUc2QnQ4ME5ZM2pxeUpXd0lwKzBEbGxhendzQ1wvSVhcLzZ2cjRHNXU5a2FHS1VaXC9nZVpraTdndGpLREVkRmhacnE0MDRFenRxZVF6OG1WWFdWRWMwU0lZc1VcLytpUDdrcVpRM2ZBVmduZnhBT0k5ajJjMmNING9KdTh5QTN6azlEWmdKM0JJYUtwQTZ3dzd1eFJ2dWpZa3BuUUFLSFJqalA5OUV6ckdxYUtoT1lsVG04ZFRoaGxoczI1dUJua1EiLCJNSUlEcGpDQ0FvNmdBd0lCQWdJVUtJVUVubzh1d2s2b3VzVkFhWkRuRVZSWWdUQXdEUVlKS29aSWh2Y05BUUVMQlFBd2F6RUxNQWtHQTFVRUJoTUNWVk14SVRBZkJnTlZCQW9UR0VGdFpYSnBZMkZ1SUVWNGNISmxjM01nUTI5dGNHRnVlVEU1TURjR0ExVUVBeE13UVcxbGNtbGpZVzRnUlhod2NtVnpjeUJRY21sMllYUmxJRU5sY25ScFptbGpZWFJwYjI0Z1FYVjBhRzl5YVhSNU1CNFhEVEUwTURneE1URTBOVE13TjFvWERUSTVNRGd4TVRFME5UTXdOMW93YXpFTE1Ba0dBMVVFQmhNQ1ZWTXhJVEFmQmdOVkJBb1RHRUZ0WlhKcFkyRnVJRVY0Y0hKbGMzTWdRMjl0Y0dGdWVURTVNRGNHQTFVRUF4TXdRVzFsY21sallXNGdSWGh3Y21WemN5QlFjbWwyWVhSbElFTmxjblJwWm1sallYUnBiMjRnUVhWMGFHOXlhWFI1TUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUF2RUVmOHZcL0hhWWJhNFBleU1jb0hQNHlScFJpN0djVlZrUWxIbHRiNDBcL0t1UjFcL2R6QXR2UzNneFZPMENPZXRPMXpzZnBBdmZ5SjhNSmwySEErKzI2NFhxVVo4eVwvT1VqeXVwMFd1OUJQS2dlUm4yRDdBT0ZhM3liWTR0WHZKalVvbTQwQTRHXC9WWldwS0hLem4rbHRMczJQT1l5TjlNRFBQSGI4R0puVnROQktlY1VkSkg0UlI5dnVuVlJ5eHFxalluUFhcL1dmWWRyRUlrVjhiWmI1RDB0bkZYSllYQXFuU2xZbVkyU0pzMnJWV2Z2cHNJZnd2QVF2ZmEwellUczMyRnNqQW15U043R05TV3lUQW9TQmZHeGlYY1IyOURtMGpndmVBdndpSzQ2elwvdE5WRFwvdUxBUmY0UDNKY2drRGNDSUJRXC9XQWZBZHFsaHFGXC9BWTRiaml3SURBUUFCbzBJd1FEQVBCZ05WSFJNQkFmOEVCVEFEQVFIXC9NQTRHQTFVZER3RUJcL3dRRUF3SUJ4akFkQmdOVkhRNEVGZ1FVcGVvVERBSkFwQ0RiUzFveDVDVDZtcXRKKzJvd0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFFMUdIN1dJMjNzTFBIQVBXXC9ZZTlUSXNnMXpFeU96b2dWZFdXRDV6NDVrUHp6TnpKdlwvN0lEZllwSWZ3d2YxSzdkS1R2a1RyOTFiRWVhcit2c2dBWGFiS0RwcGFYak9xeFFJVjRxOXdob1p6eTB4Y3Uzd0FEODBzbHpsQU15VXFtS3FJNUpSSGtxOTAwbll3NVZGMjdtMmdldmVmUVJvbzI0QnRTQzV1QmdLcTVOUzkrdm5Za1RcL1VTZ3QwXC9iaGMwelc5eXRja0lcL0U1b08wY3UzbDJ5REpCSG1zYmc3T0xwS1RuVmFjQlNpUjI1Z2szZnpDb3VjZGtLRU1ub2NaOFlUS1U2MjNvRXM0NDBOWVltR2hSaDZTMFJCcVJFSEVMUHpvS3B4WmEyMEZMUnBiV2RUQ3BpU3VocE1KSmtQY3RFQnhCNk9zb3ArTStad1NsR3JsK28xND0iXX0.eyJhY3NFcGhlbVB1YktleSI6Ik1Ga3dFd1lIS29aSXpqMENBUVlJS29aSXpqMERBUWNEUWdBRTBoQndwYk02SXRENFR6V2hNV0djNWVUYVAwanpFQkdTMVBkUDdpS3FyblltVVVMZ2ZWQkxWVFRoOEtJazlJdTYyenZVUGl3d3FrQ0RMd296YXlFTElBPT0iLCJzZGtFcGhlbVB1YktleSI6IlRVWnJkMFYzV1VoTGIxcEplbW93UTBGUldVbExiMXBKZW1vd1JFRlJZMFJSWjBGRmVuQXpORWRNVW5aSVV6UnNaVkk1YVVKU0sxRkdXSGM0VWtvMWVtWlZZVmh5YVVJNEswZ3JVVGN3ZEZCUlYwSjJaVTVNVms1ek4yRjZWVzU2WjNZdkwxQk5Ua2h2ZVU5U0wwbE1OVzEzV1RaQ2ExcDNOVkU5UFE9PSIsImFjc1VSTCI6Imh0dHBzOi8vYWNzLnNhZmVrZXlxYS5hbWVyaWNhbmV4cHJlc3MuY29tL0dyYXZpdHlBQ1MvM0RTQ2hhbGxlbmdlRmxvd0FwcCJ9.OzbRdWxnFgJTyl4a-32K8lSFp4d0Pd6jdjP7JrCUbYdNZw0CMYeWumVWZU14HLT82qHWBUsUtQ5uGUOUuAB5NqjH_RD_wFYsP2kAWN73SY08EimNolZp4bKOmcM29QCBz77JOL7SkZDJok9AInxqP-Px_dYw-k59G1ihnL0WE3_-rzLAx8m0tM61IcC_v8UgBjlHFaEhvu-eYuT9O0tsorevgu1VBJMskwAL8IxPDUUSybKdguwzm8-DSoxk8jwIKXbzqniF2Mr_YwiaWlrmYnexolGrluJQux8V8kvJ74G-kC4wzDs9lWTZrOWvAyeYeN8TetZ8m6ffKin6Qm5h0A";
        XacsSignedContentPayload scp = mse.getPayloadFromACSSignedContent(acsSignedContent);
        System.out.println("XacsSignedContentPayload : " + scp.getAcsURL() + " " + scp.getAcsEphemPubKey() + " " +
                           scp.getSdkEphemPubKey());


        int iters = 1000;
        long s = System.currentTimeMillis();
        m.setDeviceRenderOptions(null);// unknown issue with wraproot  not complete. One of '{uiType}' is expected
        for (int i = 0; i < iters; i++) {
            byte[] jsonb = mse.toJSON(m);
            TDSMessageBase tdx = mse.fromJSONWrapRoot(jsonb);

        }
        long e = System.currentTimeMillis();
        System.out.println(iters + " from/to JSON in " + (e - s) + "ms");
        s = System.currentTimeMillis();

        for (int i = 0; i < iters; i++) {
            //byte[] jsonb = mse.toJSON(m);
            char[] jsonb = mse.toJSONCharArray(m);
            TDSMessageBase tdx = mse.fromJSON(jsonb);
        }
        e = System.currentTimeMillis();
        System.out.println(iters + " warm from/to JSON in " + (e - s) + "ms");
        s = System.currentTimeMillis();
        for (int i = 0; i < iters; i++) {
            //byte[] jsonb = mse.toJSON(m);
            char[] jsonb = mse.toJSONCharArray(m);
        }
        e = System.currentTimeMillis();
        System.out.println(iters + " to JSON in " + (e - s) + "ms");
        char[] jsoncb = mse.toJSONCharArray(m);
        s = System.currentTimeMillis();
        for (int i = 0; i < iters; i++) {
            TDSMessage tdx = (TDSMessage) mse.fromJSON(jsoncb);
        }
        e = System.currentTimeMillis();
        System.out.println(iters + " from JSON in " + (e - s) + "ms");


        s = System.currentTimeMillis();
        for (int i = 0; i < iters; i++) {
            errors.clear();
            mse.validateFieldSyntax(m, errors);
        }
        e = System.currentTimeMillis();
        System.out.println(iters + " validate fields in " + (e - s) + "ms");


        String jwex = "eyJhbGciOiJSU0FFUy1PQUVQIiwiZW5jIjoiQTI1NkdDTSJ9.drQ1qRgi0rXMCYGfVRzLUa5gJgaFShZe1HW11bNAyu33LBN6WgAiK6_GIN8U1rWs0yZS7PF8LCwFOZAE1vmUB7WNzalM792ZollQqeAvaCJqguX_vtbl7UIS4c5wQlUTOau0_3NyuZObqgxLLFG5-d74VyRtaM6_KyTg3ADYDUe5q_70a1eEUVZQ67tOlLFXUmMF5aRAdWIbTFs4EUmo5IUIn7Grsr0n3jW-QEwK_b4WluSmVPLHXlmiSmN6O50BFDkg7iQeswUJHeI1_Qxw5FIrWovDsR_3KrEwpp3bJ63vMLi0cknS_LyT4fJBCJCqUi4jr7VfnAmO22y4B43f1w.D1tX184mp0De7rMqUfW9-w.qqzISjxyZNzFVmAk-JEwrwmJPTvlUyOCmq0-9aagKf8--2hMUk_ntVUgsT9W2jdL9Kg_VYRSbfI-3gmB6Xzd5ml-Wu0aPs53OzDq04W1MUKUtN1vnZbyPmpP5vpQ1AQio-ZH92itd9JfNhSvUOMWenuCp1UmEBmK9lcMoip0eX97HS_9Yk73VhhPcB2tDey6R4ZYICYvF1YICbpyEJBE0RexwGLHTljROTYqo2uP68aQnWx4LEeZWsCCx8YwFks_gl0Ozu2TNSF8QLjPcQEgqsPxNrDO8KTrZOdoGyl3bT44eTX_gow8Yk9kcj8Z2p-0BqOkb2bjw02WvF_as9dl.KasETsl0yXATRfUHcuUC_Q";
        XJWE luwe = mse.fromCompactJWE(jwex.getBytes(StandardCharsets.UTF_8));
        System.out.println("IV=" + luwe.getIv());
        System.out.println("H=" + luwe.getHeader());
        System.out.println("ek=" + luwe.getEncryptedKey());

        System.out.println("TEST ENDED");
        // dupciate leemet

    }

}
