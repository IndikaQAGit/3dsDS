/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 6. jaan 2016
 *
 */
package com.modirum.ds.tests;

import com.modirum.ds.services.KeyService;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Utils;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;

import java.security.cert.X509Certificate;

public class P7Test {

    public static void main(String[] args) throws Exception {

        //KeyFactory fact = KeyFactory.getInstance("ECDH", "SC");
        java.security.SecureRandom sr = java.security.SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(("sejrhui4h" + new java.util.Date() + "s5NNSG((Jssdjskdj" + Runtime.getRuntime().totalMemory() +
                    "Zsld9Bsl" + Math.random() + System.currentTimeMillis()).getBytes());

        getBouncyCastleProvider();

        String cert = "-----BEGIN CERTIFICATE-----\r\n" +
                      "MIIEYDCCA0igAwIBAgILBAAAAAABL07hRQwwDQYJKoZIhvcNAQEFBQAwVzELMAkG\r\n" +
                      "A1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExEDAOBgNVBAsTB1Jv\r\n" +
                      "b3QgQ0ExGzAZBgNVBAMTEkdsb2JhbFNpZ24gUm9vdCBDQTAeFw0xMTA0MTMxMDAw\r\n" +
                      "MDBaFw0yMjA0MTMxMDAwMDBaMF0xCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9i\r\n" +
                      "YWxTaWduIG52LXNhMTMwMQYDVQQDEypHbG9iYWxTaWduIE9yZ2FuaXphdGlvbiBW\r\n" +
                      "YWxpZGF0aW9uIENBIC0gRzIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB\r\n" +
                      "AQDdNR3yIFQmGtDvpW+Bdllw3Of01AMkHyQOnSKf1Ccyeit87ovjYWI4F6+0S3qf\r\n" +
                      "ZyEcLZVUunm6tsTyDSF0F2d04rFkCJlgePtnwkv3J41vNnbPMYzl8QbX3FcOW6zu\r\n" +
                      "zi2rqqlwLwKGyLHQCAeV6irs0Z7kNlw7pja1Q4ur944+ABv/hVlrYgGNguhKujiz\r\n" +
                      "4MP0bRmn6gXdhGfCZsckAnNate6kGdn8AM62pI3ffr1fsjqdhDFPyGMM5NgNUqN+\r\n" +
                      "ARvUZ6UYKOsBp4I82Y4d5UcNuotZFKMfH0vq4idGhs6dOcRmQafiFSNrVkfB7cVT\r\n" +
                      "5NSAH2v6gEaYsgmmD5W+ZoiTAgMBAAGjggElMIIBITAOBgNVHQ8BAf8EBAMCAQYw\r\n" +
                      "EgYDVR0TAQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQUXUayjcRLdBy77fVztjq3OI91\r\n" +
                      "nn4wRwYDVR0gBEAwPjA8BgRVHSAAMDQwMgYIKwYBBQUHAgEWJmh0dHBzOi8vd3d3\r\n" +
                      "Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMDMGA1UdHwQsMCowKKAmoCSGImh0\r\n" +
                      "dHA6Ly9jcmwuZ2xvYmFsc2lnbi5uZXQvcm9vdC5jcmwwPQYIKwYBBQUHAQEEMTAv\r\n" +
                      "MC0GCCsGAQUFBzABhiFodHRwOi8vb2NzcC5nbG9iYWxzaWduLmNvbS9yb290cjEw\r\n" +
                      "HwYDVR0jBBgwFoAUYHtmGkUNl8qJUC99BM00qP/8/UswDQYJKoZIhvcNAQEFBQAD\r\n" +
                      "ggEBABvgiADHBREc/6stSEJSzSBo53xBjcEnxSxZZ6CaNduzUKcbYumlO/q2IQen\r\n" +
                      "fPMOK25+Lk2TnLryhj5jiBDYW2FQEtuHrhm70t8ylgCoXtwtI7yw07VKoI5lkS/Z\r\n" +
                      "9oL2dLLffCbvGSuXL+Ch7rkXIkg/pfcNYNUNUUflWP63n41edTzGQfDPgVRJEcYX\r\n" +
                      "pOBWYdw9P91nbHZF2krqrhqkYE/Ho9aqp9nNgSvBZnWygI/1h01fwlr1kMbawb30\r\n" +
                      "hag8IyrhFHvBN91i0ZJsumB9iOQct+R2UTjEqUdOqCsukNK1OFHrwZyKarXMsh3o\r\n" +
                      "wFZUTKiL8IkyhtyTMr5NGvo1dbU=\r\n" + "-----END CERTIFICATE-----";

        String cert2 = "-----BEGIN CERTIFICATE-----\r\n" +
                       "MIICHzCCAYigAwIBAgIULLsSuNb5wJ0AJ3MkEqaVJe4ayrYwDQYJKoZIhvcNAQEF\r\n" +
                       "BQAwQDELMAkGA1UEBhMCVVMxEDAOBgNVBAoTB0NhcmFkYXMxDDAKBgNVBAsTA1BJ\r\n" +
                       "VDERMA8GA1UEAxMIcGl0LXJvb3QwHhcNMDQwNTA3MTYyMzUwWhcNMTQwNTA1MTYy\r\n" +
                       "MzUwWjBAMQswCQYDVQQGEwJVUzEQMA4GA1UEChMHQ2FyYWRhczEMMAoGA1UECxMD\r\n" +
                       "UElUMREwDwYDVQQDEwhwaXQtcm9vdDCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkC\r\n" +
                       "gYEAskqbW7oBwM1lCWNwC1obkgj4VV58G1AX7ERMWEIrQQlZ8uFdQ3FNkgMdtmx/\r\n" +
                       "XUjNF+zXTDmxe+K/lne+0KDwLWskqhS6gnkQmxZoR4FUovqRngoqU6bnnn0pM9gF\r\n" +
                       "/AI/vcdu7aowbF9S7TVlSw7IpxIQVjevEfohDpn/+oxljm0CAwEAAaMWMBQwEgYD\r\n" +
                       "VR0TAQH/BAgwBgEB/wIBATANBgkqhkiG9w0BAQUFAAOBgQBHTNIuf2yS8yDMreO3\r\n" +
                       "Ohr1qvTK/jBQkxZdfZbZiba7ItozfAu92tY35iblmElyMgduqmx1XSlbyfuQwXxR\r\n" +
                       "1a6Sb3pEN/fFUEyWXqmQuFXEe2KUu5J74tA8SX1fkcI0SNkxbQI4O3pBnmuyIrWA\r\n" +
                       "dIRzbM/4QV7yBxXh7g66koit9g==\r\n" + "-----END CERTIFICATE-----";

        X509Certificate[] certs = KeyService.parseX509Certificates(cert.getBytes(), null);

        X509Certificate[] certs2 = KeyService.parseX509Certificates(cert2.getBytes(), null);

        CMSTypedData msg = new CMSProcessableByteArray("".getBytes());
        CMSSignedDataGenerator edGen = new CMSSignedDataGenerator();
        java.util.List<X509Certificate> cl = new java.util.ArrayList();
        cl.add(certs2[0]);
        cl.add(certs[0]);


        edGen.addCertificates(new JcaCertStore(cl));
        CMSSignedData ed = edGen.generate(msg, false);

        //System.out.print(new String(Base64.encode(ed.getEncoded())));
        StringBuilder sb = new StringBuilder();
        sb.append("-----BEGIN PKCS7-----\r\n");
        sb.append(Base64.encode(ed.getEncoded(), 64));
        sb.append("\r\n----END PKCS7-----\r\n");
        System.out.print(sb);

        X509Certificate[] certs3 = KeyService.parseX509Certificates(ed.getEncoded(), null);
        System.out.println("certs " + certs3.length);
        Utils.saveFile("m:/p7test1.p7b", KeyService.createP7B(cl));
        for (X509Certificate xc : certs3) {
            System.out.println("Subj " + xc.getSubjectDN() + " " + xc.getNotAfter());
        }

    }

    public static java.security.Provider getBouncyCastleProvider() {
        java.security.Provider bcpe = java.security.Security.getProvider(
                org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME);
        if (bcpe == null) {
            bcpe = new org.bouncycastle.jce.provider.BouncyCastleProvider();
            java.security.Security.addProvider(bcpe);
        }

        return bcpe;
    }

}
