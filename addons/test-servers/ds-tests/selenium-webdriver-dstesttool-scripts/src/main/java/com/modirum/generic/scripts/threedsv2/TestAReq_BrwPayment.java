package com.modirum.generic.scripts.threedsv2;

import com.modirum.generic.utility.JsonData;
import com.modirum.generic.utility.TestSummary;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.List;

/**
 * TestAReq_BrwPayment - This class contains tests for AReq Json messages in the Browser - Payment flow.
 */
public class TestAReq_BrwPayment extends DSAReqTestSuite {

    protected void initTestClassFields() {
        m_messageType = "AReq";
        m_deviceChannel = "02"; // Browser (02)
        m_messageCategory = "01"; // Payment (01)
    }

    protected void initElemFormatInfo() {
        // not needed for this test suite.
    }

    protected void initTestCaseSummary() {
        m_testSummary = new TestSummary(new String[]{"INCLUSION", "Present", "Absent", "Empty", "Duplicate"});
    }

    /***************************************************
     * Start of Test Cases
     ***************************************************/

    /*
     * Default Json Test Cases
     */
    @Test(enabled = true, description = "Test default AReq Json Data - REQUIRED fields", priority = 0)
    public void testRequired_DefaultJson() {
        _testDefaultJson("testRequired_DefaultJson", m_required);
    }

    @Test(enabled = true, description = "Test default AReq Json Data - OPTIONAL fields", priority = 0)
    public void testOptional_DefaultJson() {
        _testDefaultJson("testOptional_DefaultJson", m_optional);
    }

    @Test(enabled = true, description = "Test default AReq Json Data - OPTIONAL (PRIOR AUTH) fields", priority = 0)
    public void testOptional_PriorAuth_DefaultJson() {
        _testDefaultJson("testOptional_PriorAuth_DefaultJson", m_optional_priorauth);
    }

    @Test(enabled = true, description = "Test default AReq Json Data - CONDITIONAL (OPT) fields", priority = 0)
    public void testConditional_Opt_DefaultJson() {
        _testDefaultJson("testConditional_Opt_DefaultJson", m_conditional_opt);
    }

    @Test(enabled = true, description = "Test default AReq Json Data - CONDITIONAL (INSTALL) fields", priority = 0)
    public void testConditional_Install_DefaultJson() {
        _testDefaultJson("testConditional_Install_DefaultJson", m_conditional_install);
    }

    @Test(enabled = true, description = "Test default AReq Json Data - CONDITIONAL (BRAZIL) fields", priority = 0)
    public void testConditional_Brazil_DefaultJson() {
        _testDefaultJson("testConditional_Brazil_DefaultJson", m_conditional_brazil);
    }

    @Test(enabled = true, description = "Test default AReq Json Data - CONDITIONAL (ADDR) fields", priority = 0)
    public void testConditional_Addr_DefaultJson() {
        _testDefaultJson("testConditional_Addr_DefaultJson", m_conditional_addr);
    }

    /*
     * Test Behavior for Required Elems
     */
    @Test(enabled = true, description = "Test behavior of REQUIRED Elements (ABSENT)", dataProvider = "AReqData_Elems", priority = 1)
    public void testRequired_Absent(List<String> testFields) {
        _testRequired_Absent("testRequired_Absent", m_required, testFields);
    }

    @Test(enabled = false, // Disable pending clarifications
            description = "Test behavior of REQUIRED Elements (EMPTY)", dataProvider = "AReqData_Elems", priority = 1)
    public void testRequired_Empty(List<String> testFields) {
        _testEmpty("testRequired_Empty", m_required, testFields);
    }

    @Test(enabled = true, description = "Test behavior of REQUIRED Elements (DUPLICATE)", dataProvider = "AReqData_Elems", priority = 1)
    public void testRequired_Duplicate(List<String> testFields) {
        _testDuplicate("testRequired_Duplicate", m_required, testFields);
    }

    /*
     * Test Behavior for Optional Elems
     */
    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of OPTIONAL Elements (EMPTY)", dataProvider = "AReqData_Elems", priority = 1)
    public void testOptional_Empty(List<String> testFields) {
        if (testFields.contains("threeDSRequestorPriorAuthenticationInfo")) {
            JsonData inJson = new JsonData(m_optional);
            inJson.addElement("threeDSRequestorPriorAuthenticationInfo",
                              m_optional_priorauth.getJsonValue("threeDSRequestorPriorAuthenticationInfo"));
            _testEmpty("testOptional_Empty", inJson, testFields);
        } else {
            _testEmpty("testOptional_Empty", m_optional, testFields);
        }
    }

    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of OPTIONAL Elements (DUPLICATE)", dataProvider = "AReqData_Elems", priority = 1)
    public void testOptional_Duplicate(List<String> testFields) {
        if (testFields.contains("threeDSRequestorPriorAuthenticationInfo")) {
            JsonData inJson = new JsonData(m_optional);
            inJson.addElement("threeDSRequestorPriorAuthenticationInfo",
                              m_optional_priorauth.getJsonValue("threeDSRequestorPriorAuthenticationInfo"));
            _testDuplicate("testOptional_Duplicate", inJson, testFields);
        } else {
            _testDuplicate("testOptional_Duplicate", m_optional, testFields);
        }
    }

    /*
     * Test Behavior for Conditional Elems (Address-related sub-tests)
     */
    @Test(enabled = true, description = "Test behavior of CONDITIONAL (ADDR) Elements (ABSENT)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Addr_Absent(String testField) {
        _testConditional_Addr_Absent("testConditional_Addr_Absent", m_conditional_addr, testField);
    }

    @Test(enabled = false, // Disable pending clarifications
            description = "Test behavior of CONDITIONAL (ADDR) Elements (EMPTY)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Addr_Empty(String testField) {
        _testConditional_Addr_Empty("testConditional_Addr_Absent", m_conditional_addr, testField);
    }

    @Test(enabled = true, description = "Test behavior of CONDITIONAL (ADDR) Elements (DUPLICATE)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Addr_Duplicate(List<String> testFields) {
        _testDuplicate("testConditional_Addr_Duplicate", m_conditional_addr, testFields);
    }

    /*
     * Test Behavior for Conditional Elems (Brazil-related sub-tests)
     */
    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of CONDITIONAL (BRAZIL) Elements (ABSENT)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Brazil_Absent(List<String> testFields) {
        _testRequired_Absent("testConditional_Brazil_Absent", m_conditional_brazil, testFields);
    }

    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of CONDITIONAL (BRAZIL) Elements (EMPTY)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Brazil_Empty(List<String> testFields) {
        _testEmpty("testConditional_Brazil_Empty", m_conditional_brazil, testFields);
    }

    @Test(enabled = true, description = "Test behavior of CONDITIONAL (BRAZIL) Elements (DUPLICATE)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Brazil_Duplicate(List<String> testFields) {
        _testDuplicate("testConditional_Brazil_Duplicate", m_conditional_brazil, testFields);
    }

    /*
     * Test Behavior for Conditional Elems (Install-related sub-tests)
     */
    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of CONDITIONAL (INSTALL) Elements (ABSENT)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Install_Absent(String testField) {
        _testConditional_Install_Absent("testConditional_Install_Absent", m_conditional_install, testField);

    }

    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of CONDITIONAL (INSTALL) Elements (EMPTY)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Install_Empty(String testField) {
        _testConditional_Install_Empty("testConditional_Install_Empty", m_conditional_install, testField);
    }

    @Test(enabled = true, description = "Test behavior of CONDITIONAL (INSTALL) Elements (DUPLICATE)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Install_Duplicate(List<String> testFields) {
        _testDuplicate("testConditional_Install_Duplicate", m_conditional_install, testFields);
    }

    /*
     * Test Behavior for Conditional Elems (Treated as Optional sub-tests)
     */
    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of CONDITIONAL (OPTIONAL) Elements (EMPTY)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Opt_Empty(List<String> testFields) {
        _testEmpty("testConditional_Optional_Empty", m_conditional_opt, testFields);
    }

    @Test(enabled = true, description = "Test behavior of CONDITIONAL (OPTIONAL) Elements (DUPLICATE)", dataProvider = "AReqData_Elems", priority = 1)
    public void testConditional_Opt_Duplicate(List<String> testFields) {
        _testDuplicate("testConditional_Opt_Duplicate", m_conditional_opt, testFields);
    }

    /***************************************************
     * Data Provider
     ***************************************************/
    @DataProvider(name = "AReqData_Elems")
    private Object[][] aReqData_Elems(Method m) {
        return getAReqDataProvider(m.getName());
    }
}