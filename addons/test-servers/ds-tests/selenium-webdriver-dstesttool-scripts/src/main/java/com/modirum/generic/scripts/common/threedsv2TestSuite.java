package com.modirum.generic.scripts.common;

import com.modirum.generic.utility.JsonData;
import com.modirum.generic.utility.SelDriver;
import com.modirum.generic.utility.TestSummary;
import com.modirum.generic.utility.ThreeDS2Messages;
import com.modirum.generic.utility.reports.ITestReporter;
import com.modirum.generic.utility.reports.TestReporterFactory;
import org.apache.commons.io.FileUtils;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * threedsv2TestSuite - Class that contains the common variables and methods used for 3DS2.0 Test Suites
 */
public abstract class threedsv2TestSuite extends AbstractTestSuite {
    /*
     * General member variables
     */
    protected boolean m_basicErrorCheckOnly = true; // flag used to switch between full Error message verification (all fields)
    //      and basic Error message fields verification (errorComponent,
    //      errorCode, errorDescription, errorDetail only)
    protected boolean m_basicResCheckOnly = false;   // flag used to switch between full Response message verification (all fields)
    //      and basic Response message field verification (messageType only)

    protected SelDriver m_driver;
    protected boolean m_bRestart;
    protected boolean m_bScreenshot;
    protected String m_screenshotDir;

    protected String m_testClassName;
    protected int m_numFieldsPerTest;

    protected String m_testFilesPath;

    /*
     * Test Class specific Json message fields
     */
    protected String m_errorComponent;
    protected String m_messageType;
    protected String m_deviceChannel;
    protected String m_messageCategory;
    protected String m_testClassColumn;

    /*
     * CSV Records data
     */
    protected Object[][] m_erroInclusionInfo;   // Stores inclusion requirements for Erro fields [expected results for error cases]

    /*
     * Test Group data
     */
    protected HashMap<String, String> m_testGroupMap;

    /*
     * AReq Json data
     */
    protected String m_json; // Json file containing ALL 3ds2.0 message fields with default values

    /*
     * For Reports
     */
    protected ITestReporter m_reporter;
    protected TestSummary m_testSummary;

    /*
     * Abstract Methods
     */
    protected abstract void setTestClassColumn();

    protected abstract void initTestClassFields();

    protected abstract void initElemFormatInfo();

    protected abstract void initTestCaseSummary();

    protected abstract JsonData getTestGroupJson(String field);

    /*
     * Main
     */
    @Parameters({"browser", "ip_add", "port", "screenshots", "startURL", "alljson", "numFieldsPerTest", "report_type"})
    @BeforeClass(alwaysRun = true)
    public void setUp(String browser, String ip_add, String port, String screenshots, String startURL, String alljson, int numFieldsPerTest, int report_type) {
        // Set global parameters
        m_testFilesPath = System.getProperty("user.dir") + File.separator + "test-files" + File.separator;
        m_testClassName = this.getClass().getSimpleName();
        m_startURL = startURL;
        m_json = alljson;
        m_numFieldsPerTest = numFieldsPerTest;

        // Start webdriver
        String node = "http://" + ip_add + ":" + port + "/wd/hub";
        m_driver = new SelDriver(browser, node);
        m_driver.openUrl(startURL);
        m_bRestart = false; // No need to refresh page for first test (always set initial value to false here)

        // Prepare screenshots folder
        m_bScreenshot = Boolean.parseBoolean(screenshots);
        if (m_bScreenshot) {
            try {
                createScreenshotsDir();
            } catch (IOException e) {
                System.out.println("[WARNING] Cannot create screenshot directory. Screenshots will not be saved.");
                this.m_screenshotDir = null;
                e.printStackTrace();
            }
        } else {
            m_screenshotDir = null;
        }

        // Prepare test result reports file
        String testOutPath =
                System.getProperty("user.dir") + File.separator + "test-reports" + File.separator + "DS_3ds2" +
                File.separator;
        try {
            FileUtils.forceMkdir(new File(testOutPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        m_reporter = TestReporterFactory.getTestReporter(report_type, testOutPath + m_testClassName + "_Report.html",
                                                         true);
        initTestCaseSummary();

        // Initialize test class specific parameters
        initTestClassFields();
        setTestClassColumn();

        // Initialize expected results info
        initExpectedData();

        // Initialize element format info
        initElemFormatInfo();
    }

    /**
     * Method to close the browser and generate a test summary file after execution of all test cases in the class
     */
    @AfterClass(alwaysRun = true)
    public void shutDown() {
        m_driver.closeBrowser();
        if (expectedData != null) {
            expectedData.clear();
        }

        m_testSummary.generateTestSummaryFile(
                System.getProperty("user.dir") + File.separator + "test-reports" + File.separator + "DS_3ds2" +
                File.separator + m_testClassName + "_Summary.csv");
    }

    /**
     * Method to set up the test case before each test case is executed
     */
    @BeforeMethod(alwaysRun = true)
    public void setupTestCase() {
        if (m_bRestart) {
            System.out.println(" Returning to starting URL...");
            m_driver.openUrl(m_startURL);
        } else {
            System.out.println(" Browser is at starting URL.");
        }
        m_bRestart = true;
    }

    /**
     * Method to save the test result after each test case
     *
     * @param result
     */
    @AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result) {
        int stat = result.getStatus();
        String methodName = result.getMethod().getMethodName();
        Object[] testParams = result.getParameters();

        switch (stat) {
            case ITestResult.SUCCESS:
                m_reporter.logPass("Test PASSED");
                setResultFromMethod(methodName, testParams, "PASSED");
                break;
            case ITestResult.FAILURE:
                String logFailedTests = result.getThrowable().toString();
                m_reporter.logFail("Test FAILED\n" + logFailedTests);
                setResultFromMethod(methodName, testParams, "FAILED");
                break;
            case ITestResult.SKIP:
                m_reporter.logSkip("Test SKIPPED");
                setResultFromMethod(methodName, testParams, "SKIPPED");
                break;
            default:
                m_reporter.logWarning("TEST STATUS UNKNOWN!");
                break;
        }
        m_reporter.endTest();
    }

    /*****************************************
     * Common utility functions
     *****************************************/

    /*
     * This function creates the test description for a given test method and starts the TestReport log
     */
    protected void logStartTestList(String testName, List<String> testFields) {

        StringBuilder paramLog = new StringBuilder();
        if (testFields == null) {
            paramLog.append("none");
        } else {
            for (String field : testFields) {
                paramLog.append(field + ",");
            }
            paramLog.deleteCharAt(paramLog.length() - 1);
        }

        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", "");
        }
        m_reporter.logInfo("Starting test with params [" + paramLog + "]");
    }

    /*
     * This function sets the test field result in the TestSummary
     */
    private void setTestFieldResult(Object param, String inclusion, String resColumn, String result) {
        List<String> testFields;
        if (param instanceof String) {
            testFields = new LinkedList<String>();
            testFields.add((String) param);
        } else {
            testFields = (List<String>) param;
        }

        for (String field : testFields) {
            m_testSummary.setResult(field, "INCLUSION", inclusion);
            m_testSummary.setResult(field, resColumn, result);
        }
    }

    /*
     * This function gets the inclusion information from the parsed method name
     */
    private String getInclusion(String inc, String subGroup) {
        if (inc.contains("Required")) {
            return "R";
        }
        if (inc.contains("Optional")) {
            if (subGroup.contains("Default") || subGroup.contains("Absent") || subGroup.contains("Empty") ||
                subGroup.contains("Duplicate")) {
                return "O";
            } else {
                return "O (" + subGroup + ")";
            }
        }
        if (inc.contains("Conditional")) {
            return "C (" + subGroup + ")";
        }
        return "";
    }

    /*
     * This function sets the result of the test case in the TestSummary object
     */
    public void setResultFromMethod(String methodName, Object[] params, String result) {
        if (methodName.contains("ElemFormat")) {
            String[] parsed = methodName.split("_");
            String field = parsed[1];
            m_testSummary.setResult(field, "TEST FORMAT ACTION", (String) params[0]);

            String tc = (String) params[1];
            tc = tc.substring(1, tc.length() - 1);

            if (tc.equals("VALID VALUE") || tc.equals("INVALID VALUE")) {
                m_testSummary.mergeResult(field, tc + " (VAL)", (String) params[2]);
                m_testSummary.mergeResult(field, tc + " (RES)", result);
            } else {
                m_testSummary.setResult(field, tc, result);
            }
        } else { /* inclusion test cases */
            // methodName format: test<INCLUSION><_SUBGROUP>_<MAINTESTGROUP>
            String[] parsed = methodName.split("_");
            StringBuilder defaultTestName = new StringBuilder();

            String inclusion = getInclusion(parsed[0], parsed[1]);
            defaultTestName.append(inclusion);

            int lastIndex = parsed.length - 1;
            if (parsed[lastIndex].contains("Default")) {
                // default checks all present fields for a particular test group - just log the main result
                defaultTestName.append(" DEFAULT FIELDS");

                m_testSummary.setResult(defaultTestName.toString(), "INCLUSION", inclusion);
                m_testSummary.setResult(defaultTestName.toString(), "Present", result);
            } else {
                setTestFieldResult(params[0], inclusion, parsed[lastIndex], result);
            }
        }
    }

    /*******************************
     * COMMON VERIFICATION METHODS
     ********************************/
    /*
     * This function verifies the specified field in the response Json, given the expected value
     * and both input Json data and reply Json data.
     * Notes:
     * 1. val == "" -> assert that field exists, then log actual value for manual checking in the logs.
     * 2. val begins with "REQ." -> assert that the field's value is the same as the specified field's value in the input Json data.
     * 3. Default verification -> assert that the value of the field == val.
     */
    protected void verifyResponseField(SoftAssert softAssert, String val, String field, JsonData inJson, JsonData replyJson) {
        if (val.equals("")) {
            // value is generated by component at runtime
            m_reporter.logInfo("[" + field + "] " + replyJson.getValueAsString(field));
            softAssert.assertTrue(replyJson.containsField(field));
        } else if (val.length() > 4 && val.substring(0, 4).equals("REQ.")) {
            // value is taken from Request
            softAssert.assertEquals(replyJson.getValueAsString(field), inJson.getValueAsString(val.substring(4)),
                                    "\n[FAILED TEST] Incorrect " + field);
        } else {
            // expected value column has a specific value
            softAssert.assertEquals(replyJson.getValueAsString(field), val, "\n[FAILED TEST] Incorrect " + field);
        }
    }

    /*
     * This function verifies only the basic Erro message fields.
     * Use this to verify that the correct category and details are being returned by the component
     * and to isolate Erro message fields that cause the failures for multiple test cases.
     */
    protected void verifyBasicErrorResponse(SoftAssert softAssert, JsonData inJson, JsonData replyJson, String errorCategory, List<String> testFields) {
        String[] expData = expectedData.get(errorCategory);
        verifyResponseField(softAssert, m_errorComponent, "errorComponent", inJson, replyJson);
        verifyResponseField(softAssert, expData[0], "errorCode", inJson, replyJson);
        verifyResponseField(softAssert, expData[1], "errorDescription", inJson, replyJson);
        if (errorCategory.equals("error102")) {
            String errDet = "messageVersion invalid value '" + inJson.getValueAsString("messageVersion") +
                            "' supported versions: 2.0.1";
            verifyResponseField(softAssert, errDet, "errorDetail", inJson, replyJson);
        } else if (errorCategory.equals("error101") || errorCategory.equals("error301") ||
                   errorCategory.equals("error302") || errorCategory.equals("error303") ||
                   errorCategory.equals("error306") || errorCategory.equals("error307") ||
                   errorCategory.equals("error401") || errorCategory.equals("error402") ||
                   errorCategory.equals("error403") || errorCategory.equals("error404") ||
                   errorCategory.equals("error405")) {
            verifyResponseField(softAssert, "", "errorDetail", inJson, replyJson);
        } else {
            ThreeDS2Messages.verifyErrorDetail(softAssert, replyJson.getValueAsString("errorDetail"), testFields);
        }
    }

    /*
     * This function verifies the full Erro message fields
     */
    protected void verifyFullErrorResponse(SoftAssert softAssert, JsonData inJson, JsonData replyJson, String errorCategory, List<String> testFields) {
        String[] expData = expectedData.get(errorCategory);
        for (Object[] erroField : m_erroInclusionInfo) {
            String field = (String) erroField[0];
            String inclusion = (String) erroField[1];
            String condition = (String) erroField[2];
            String val = "";

            boolean isExist = replyJson.containsField(field);
            boolean isRequired = false;

            if (inclusion.equals("R")) {  // required field
                isRequired = true;
            } else if (inclusion.equals("O")) { // optional field
                isRequired = false;
            } else if (inclusion.equals("C")) { // conditional field
                if (condition.equals("available")) {
                    if (inJson.containsField(field)) {
                        isRequired = true;
                    }
                }
            }
            if (isRequired) {
                softAssert.assertTrue(isExist, "\n[FAILED TEST] Missing required field [" + field + "]");
            }
            if (isExist) {
                if (field.equals("errorDetail")) {
                    if (errorCategory.equals("error102")) {
                        String errDet = "messageVersion invalid value '" + inJson.getValueAsString("messageVersion") +
                                        "' supported versions: 2.0.1";
                        verifyResponseField(softAssert, errDet, "errorDetail", inJson, replyJson);
                    } else if (errorCategory.equals("error101") || errorCategory.equals("error301") ||
                               errorCategory.equals("error302") || errorCategory.equals("error303") ||
                               errorCategory.equals("error306") || errorCategory.equals("error307") ||
                               errorCategory.equals("error401") || errorCategory.equals("error402") ||
                               errorCategory.equals("error403") || errorCategory.equals("error404") ||
                               errorCategory.equals("error405")) {
                        verifyResponseField(softAssert, "", "errorDetail", inJson, replyJson);
                    } else {
                        ThreeDS2Messages.verifyErrorDetail(softAssert, replyJson.getValueAsString("errorDetail"),
                                                           testFields);
                    }
                } else {
                    if (field.equals("errorComponent")) {
                        val = m_errorComponent;
                    } else if (field.equals("errorCode")) {
                        val = expData[0];
                    } else if (field.equals("errorDescription")) {
                        val = expData[1];
                    } else {
                        val = (String) erroField[3];
                    }
                    verifyResponseField(softAssert, val, field, inJson, replyJson);
                }
            }
        }
    }

    /*
     * Verify the Erro message. This function uses the global flag m_basicErrorCheckOnly to determine if the basic or full
     * Erro message should be checked.
     */
    protected void verifyErrorResponse(SoftAssert softAssert, JsonData inJson, JsonData replyJson, String errorCategory, List<String> testFields) {
        if (m_basicErrorCheckOnly) {
            verifyBasicErrorResponse(softAssert, inJson, replyJson, errorCategory, testFields);
        } else {
            verifyFullErrorResponse(softAssert, inJson, replyJson, errorCategory, testFields);
        }
    }
}
