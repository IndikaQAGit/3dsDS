package com.modirum.generic.utility.reports;

public class TestReporterFactory {
    public static ITestReporter getTestReporter(int type, String fullPath, Boolean bOverwrite) {
        switch (type) {
            case 1:
            default:
                return new ExtentTestReporter(fullPath, bOverwrite);
            case 2:
                return new AltExtentTestReporter(fullPath);
            case 3:
                return new ConsoleTestReporter();
        }
    }
}
