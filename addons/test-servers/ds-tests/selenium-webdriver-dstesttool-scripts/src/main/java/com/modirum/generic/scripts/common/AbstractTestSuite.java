package com.modirum.generic.scripts.common;

import com.modirum.generic.utility.SelDriver;
import com.modirum.generic.utility.SelServer;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

/**
 * AbstractTestSuite - Class that contains common variables and methods used by all Test Suites
 */
public abstract class AbstractTestSuite {
    protected String m_startURL;
    protected String screenshotPath;
    protected boolean bAutoServer;
    protected Map<String, String[]> expectedData;

    /**
     * Sets up the Selenium Server before executing the test suite
     *
     * @param autoserver - Setting from testng.xml file that indicates if Server should be started by the script or not
     * @param port       - Setting from testng.xml file that indicates the Port used by the Server
     */
    @Parameters({"autoserver", "port"})
    @BeforeSuite(alwaysRun = true)
    public void setUpServer(String autoserver, String port) {
        bAutoServer = Boolean.parseBoolean(autoserver);
        if (bAutoServer) {
            // automatically start the local Selenium server
            SelServer.setPort(port);
            SelServer.start();
            // wait a few seconds for the server to start
            Thread thread = Thread.currentThread();
            try {
                thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Shuts down the Selenium Server after execution of the entire test suite
     */
    @AfterSuite(alwaysRun = true)
    public void shutDownServer() {
        if (SelServer.getServer() != null) {
            SelServer.stop();
        }
    }

    /*
     *
     * Other Common Functions
     *
     * */

    /**
     * Create a screenshot directory
     *
     * @return Full path where the directory was created
     * @throws IOException
     */
    protected String createScreenshotsDir() throws IOException {
        String currentDir = System.getProperty("user.dir");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

        screenshotPath = currentDir + "/screenshots/run_" + this.getClass().getSimpleName() + "_" + timeStamp;

        FileUtils.forceMkdir(new File(screenshotPath));

        return screenshotPath;
    }

    /**
     * Takes a screenshot and saves it in the screenshot directory
     *
     * @param driver        - Selenium driver used to manipulate the webpage
     * @param screenshotDir - Base path where screenshot will be saved
     * @param testName      - Test Name used in creating the screenshot file name
     */
    public void getScreenshot(SelDriver driver, String screenshotDir, String testName) {
        //System.out.println("getScreenshot");

        String fileName = getScreenshotPath(screenshotDir, testName);
        File scrFile = ((TakesScreenshot) (driver.getDriver())).getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(scrFile, new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the full path and name of the screenshot file
     *
     * @param screenshotDir - Base path where the screenshot will be saved
     * @param testName      - Test Name used in creating the screenshot file name
     * @return
     */
    protected String getScreenshotPath(String screenshotDir, String testName) {
        String current = null;

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

        try {
            current = new File(screenshotDir + "/" + testName + "_" + timeStamp + ".jpg").getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return current;
    }

    /*
     * This abstract function initializes the expectedData Map. It must be implemented by the child class.
     * */
    protected abstract void initExpectedData();

}

