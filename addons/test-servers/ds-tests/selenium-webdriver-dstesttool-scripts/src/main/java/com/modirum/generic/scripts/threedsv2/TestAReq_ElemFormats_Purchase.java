package com.modirum.generic.scripts.threedsv2;

import com.modirum.generic.utility.ElemFormatsTestCaseGenerator;
import com.modirum.generic.utility.TestSummary;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * TestAReq_ElemFormats_Purchase - This class contains element format tests for AReq Json messages in the APP - Payment flow.
 */
public class TestAReq_ElemFormats_Purchase extends DSAReqTestSuite {
    private ElemFormatsTestCaseGenerator m_tcGen;

    protected void initTestClassFields() {
        m_messageType = "AReq";
        m_deviceChannel = "01"; // APP (01)
        m_messageCategory = "01"; // Payment (01)
    }

    protected void initElemFormatInfo() {
        m_tcGen = new ElemFormatsTestCaseGenerator();
        m_tcGen.initElemFormatInfo();
    }

    protected void initTestCaseSummary() {
        m_testSummary = new TestSummary(
                new String[]{"TEST FORMAT ACTION", "LENGTH < FIXED SIZE", "LENGTH > FIXED SIZE", "LENGTH = MIN", "LENGTH < MIN", "LENGTH = MAX", "LENGTH > MAX", "VALID VALUE (VAL)", "VALID VALUE (RES)", "INVALID VALUE (VAL)", "INVALID VALUE (RES)", "SPACE AT START", "SPACE AT MID", "SPACE AT END", "NUMBERS", "ALPHA", "SYMBOLS", "CYRILLIC", "CHINESE", "GREEK"});
    }

    /***************************************************
     * Start of Test Cases
     ***************************************************/
    @Test(enabled = true, description = "Test element format verification for purchaseInstalData", dataProvider = "ElemFormats")
    public void testElemFormat_purchaseInstalData(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_purchaseInstalData" + testName, "purchaseInstalData", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for purchaseAmount", dataProvider = "ElemFormats")
    public void testElemFormat_purchaseAmount(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_purchaseAmount" + testName, "purchaseAmount", testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for purchaseCurrency", dataProvider = "ElemFormats")
    public void testElemFormat_purchaseCurrency(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_purchaseCurrency" + testName, "purchaseCurrency", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for purchaseExponent", dataProvider = "ElemFormats")
    public void testElemFormat_purchaseExponent(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_purchaseExponent" + testName, "purchaseExponent", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for purchaseDate", dataProvider = "ElemFormats")
    public void testElemFormat_purchaseDate(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_purchaseDate" + testName, "purchaseDate", testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for recurringExpiry", dataProvider = "ElemFormats")
    public void testElemFormat_recurringExpiry(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_recurringExpiry" + testName, "recurringExpiry", testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for recurringFrequency", dataProvider = "ElemFormats")
    public void testElemFormat_recurringFrequency(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_recurringFrequency" + testName, "recurringFrequency", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for transType", dataProvider = "ElemFormats")
    public void testElemFormat_transType(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_transType" + testName, "transType", testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    /***************************************************
     * Data Provider
     ***************************************************/
    @DataProvider(name = "ElemFormats")
    protected Iterator<Object[]> elemFormatsDP(Method m) {
        String[] parseMethodName = m.getName().split("_");
        if (parseMethodName.length == 2 && parseMethodName[0].equals("testElemFormat")) {
            return m_tcGen.getElemFormatDataProvider(parseMethodName[1], getTestGroupJson(parseMethodName[1]));
        }
        return null;
    }
}
