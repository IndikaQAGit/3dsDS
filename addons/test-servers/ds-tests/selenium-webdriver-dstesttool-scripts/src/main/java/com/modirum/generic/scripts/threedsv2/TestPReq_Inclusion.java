package com.modirum.generic.scripts.threedsv2;

import com.modirum.generic.utility.TestSummary;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.List;

/**
 * TestPReq_Inclusion - This class contains tests for PReq Json messages.
 */
public class TestPReq_Inclusion extends DSPReqTestSuite {
    protected void initElemFormatInfo() {
        // not needed for this test suite.
    }

    protected void initTestCaseSummary() {
        m_testSummary = new TestSummary(new String[]{"INCLUSION", "Present", "Absent", "Empty", "Duplicate"});
    }

    /***************************************************
     * Start of Test Cases
     ***************************************************/

    /*
     * Default Json Test Cases
     */
    @Test(enabled = true, description = "Test default PReq Json Data - REQUIRED fields", priority = 0)
    public void testRequired_DefaultJson() {
        _testDefaultJson("testRequired_DefaultJson", m_required);
    }

    @Test(enabled = true, description = "Test default PReq Json Data - OPTIONAL fields", priority = 0)
    public void testOptional_DefaultJson() {
        _testDefaultJson("testOptional_DefaultJson", m_optional);
    }


    @Test(enabled = true, description = "Test default PReq Json Data - CONDITIONAL (OPT) fields", priority = 0)
    public void testConditional_Opt_DefaultJson() {
        _testDefaultJson("testConditional_Opt_DefaultJson", m_conditional_opt);
    }

    /*
     * Test Behavior for Required Elems
     */
    @Test(enabled = true, description = "Test behavior of REQUIRED Elements (ABSENT)", dataProvider = "PReqData_Elems", priority = 1)
    public void testRequired_Absent(List<String> testFields) {
        _testRequired_Absent("testRequired_Absent", m_required, testFields);
    }

    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of REQUIRED Elements (EMPTY)", dataProvider = "PReqData_Elems", priority = 1)
    public void testRequired_Empty(List<String> testFields) {
        _testEmpty("testRequired_Empty", m_required, testFields);
    }

    @Test(enabled = true, description = "Test behavior of REQUIRED Elements (DUPLICATE)", dataProvider = "PReqData_Elems", priority = 1)
    public void testRequired_Duplicate(List<String> testFields) {
        _testDuplicate("testRequired_Duplicate", m_required, testFields);
    }

    /*
     * Test Behavior for Optional Elems
     */
    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of OPTIONAL Elements (EMPTY)", dataProvider = "PReqData_Elems", priority = 1)
    public void testOptional_Empty(List<String> testFields) {
        _testEmpty("testOptional_Empty", m_optional, testFields);
    }

    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of OPTIONAL Elements (DUPLICATE)", dataProvider = "PReqData_Elems", priority = 1)
    public void testOptional_Duplicate(List<String> testFields) {
        _testDuplicate("testOptional_Duplicate", m_optional, testFields);
    }

    /*
     * Test Behavior for Conditional Elems (Treated as Optional sub-tests)
     */
    @Test(enabled = false, // Disable pending clarification
            description = "Test behavior of CONDITIONAL (OPTIONAL) Elements (EMPTY)", dataProvider = "PReqData_Elems", priority = 1)
    public void testConditional_Opt_Empty(List<String> testFields) {
        _testEmpty("testConditional_Optional_Empty", m_conditional_opt, testFields);
    }

    @Test(enabled = true, description = "Test behavior of CONDITIONAL (OPTIONAL) Elements (DUPLICATE)", dataProvider = "PReqData_Elems", priority = 1)
    public void testConditional_Opt_Duplicate(List<String> testFields) {
        _testDuplicate("testConditional_Opt_Duplicate", m_conditional_opt, testFields);
    }

    /***************************************************
     * Data Provider
     ***************************************************/
    @DataProvider(name = "PReqData_Elems")
    private Object[][] pReqData_Elems(Method m) {
        return getPReqDataProvider(m.getName());
    }
}