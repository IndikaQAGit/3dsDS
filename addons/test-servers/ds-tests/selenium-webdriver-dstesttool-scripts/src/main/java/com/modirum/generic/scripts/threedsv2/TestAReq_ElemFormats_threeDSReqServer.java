package com.modirum.generic.scripts.threedsv2;

import com.modirum.generic.utility.ElemFormatsTestCaseGenerator;
import com.modirum.generic.utility.TestSummary;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * TestAReq_ElemFormats_threeDSReqServer - This class contains element format tests for ...
 */
public class TestAReq_ElemFormats_threeDSReqServer extends DSAReqTestSuite {
    private ElemFormatsTestCaseGenerator m_tcGen;

    protected void initTestClassFields() {
        m_messageType = "AReq";
        m_deviceChannel = "01"; // APP (01)
        m_messageCategory = "01"; // Payment (01)
    }

    protected void initElemFormatInfo() {
        m_tcGen = new ElemFormatsTestCaseGenerator();
        m_tcGen.initElemFormatInfo();
    }

    protected void initTestCaseSummary() {
        m_testSummary = new TestSummary(
                new String[]{"TEST FORMAT ACTION", "LENGTH < FIXED SIZE", "LENGTH > FIXED SIZE", "LENGTH = MIN", "LENGTH < MIN", "LENGTH = MAX", "LENGTH > MAX", "VALID VALUE (VAL)", "VALID VALUE (RES)", "INVALID VALUE (VAL)", "INVALID VALUE (RES)", "SPACE AT START", "SPACE AT MID", "SPACE AT END", "NUMBERS", "ALPHA", "SYMBOLS", "CYRILLIC", "CHINESE", "GREEK"});
    }

    /***************************************************
     * Start of Test Cases
     ***************************************************/
    @Test(enabled = true, description = "Test element format verification for threeDSRequestorChallengeInd", dataProvider = "ElemFormats")
    public void testElemFormat_threeDSRequestorChallengeInd(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_threeDSRequestorChallengeInd" + testName, "threeDSRequestorChallengeInd",
                               testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for threeDSRequestorID", dataProvider = "ElemFormats")
    public void testElemFormat_threeDSRequestorID(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_threeDSRequestorID" + testName, "threeDSRequestorID", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for threeDSRequestorName", dataProvider = "ElemFormats")
    public void testElemFormat_threeDSRequestorName(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_threeDSRequestorName" + testName, "threeDSRequestorName", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for threeDSRequestorURL", dataProvider = "ElemFormats")
    public void testElemFormat_threeDSRequestorURL(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_threeDSRequestorURL" + testName, "threeDSRequestorURL", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for threeDSServerOperatorID", dataProvider = "ElemFormats")
    public void testElemFormat_threeDSServerOperatorID(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_threeDSServerOperatorID" + testName, "threeDSServerOperatorID",
                               testValue, resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for threeDSServerTransID", dataProvider = "ElemFormats")
    public void testElemFormat_threeDSServerTransID(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_threeDSServerTransID" + testName, "threeDSServerTransID", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    @Test(enabled = true, description = "Test element format verification for threeDSServerURL", dataProvider = "ElemFormats")
    public void testElemFormat_threeDSServerURL(String testAction, String testName, String testValue, String resCategory) {
        if (testAction.equals("RUN")) {
            _testElementFormat("testElemFormat_threeDSServerURL" + testName, "threeDSServerURL", testValue,
                               resCategory);
        } else {
            throw new SkipException("Test case is SKIPPED.");
        }
    }

    /***************************************************
     * Data Provider
     ***************************************************/
    @DataProvider(name = "ElemFormats")
    protected Iterator<Object[]> elemFormatsDP(Method m) {
        String[] parseMethodName = m.getName().split("_");
        if (parseMethodName.length == 2 && parseMethodName[0].equals("testElemFormat")) {
            return m_tcGen.getElemFormatDataProvider(parseMethodName[1], getTestGroupJson(parseMethodName[1]));
        }
        return null;
    }
}
