package com.modirum.generic.utility;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class JsonData {
    private List<Pair<String, JsonValue>> jsonElements;
    private String jsonString; // just used for debugging

    /**
     * JsonData default constructor
     */
    public JsonData() {
        jsonElements = new ArrayList<Pair<String, JsonValue>>();
        jsonString = "";
    }

    /**
     * JsonData constructor with path of json file
     *
     * @param fullpath - full path of json file to be read
     */
    public JsonData(String fullpath) {
        jsonElements = new ArrayList<Pair<String, JsonValue>>();
        jsonString = "";
        loadFromFile(fullpath);
    }

    /**
     * JsonData copy constructor
     *
     * @param copy
     */
    public JsonData(JsonData copy) {
        jsonElements = new ArrayList<Pair<String, JsonValue>>();
        jsonString = "";

        int count = copy.size();
        for (int i = 0; i < count; i++) {
            Pair<String, JsonValue> element = copy.getElement(i);
            this.addElement(element.getKey(), element.getValue());
        }
    }

    /**
     * Clears the JsonData structure
     */
    public void clear() {
        jsonElements.clear();
        jsonString = "";
    }

    /**
     * Add a key-value pair
     *
     * @param key   - The key of the field to add
     * @param value - The value of the field (type String)
     */
    public void addElement(String key, String value) {
        jsonElements.add(new MutablePair<String, JsonValue>(key, Json.value(value)));
    }

    /**
     * Add a key-value pair
     *
     * @param key   - The key of the field to add
     * @param value - The value of the field (type JsonValue)
     */
    public void addElement(String key, JsonValue value) {
        jsonElements.add(new MutablePair<String, JsonValue>(key, value));
    }

    /**
     * Add a child key-value pair
     *
     * @param key      - The key of the parent field (value is assumed to be a Json Object)
     * @param childKey - The key of the child field to add
     * @param value    - The value of the child field (type String)
     */
    public void addChildElement(String key, String childKey, String value) {
        JsonValue val = getJsonValue(key);
        if (val != null && val.isObject()) {
            JsonObject obj = val.asObject();
            obj.add(childKey, value);
            setValue(key, obj);
        } else {
            JsonObject obj = new JsonObject();
            obj.add(childKey, value);
            addElement(key, obj);
        }
    }

    /**
     * Add a child key-value pair
     *
     * @param key      - The key of the parent field (value is assumed to be a Json Object)
     * @param childKey - The key of the child field to add
     * @param value    - The value of the child field (type JsonValue)
     */
    public void addChildElement(String key, String childKey, JsonValue value) {
        JsonValue val = getJsonValue(key);
        if (val != null && val.isObject()) {
            JsonObject obj = val.asObject();
            obj.add(childKey, value);
            setValue(key, obj);
        } else {
            JsonObject obj = new JsonObject();
            obj.add(childKey, value);
            addElement(key, obj);
        }
    }

    public void removeElement(String key) {
        ListIterator<Pair<String, JsonValue>> elemIter = jsonElements.listIterator();
        while (elemIter.hasNext()) {
            Pair<String, JsonValue> elem = elemIter.next();
            if (elem.getKey().equals(key)) {
                jsonElements.remove(elem);
                return;
            }
        }
    }

    public void removeChildElement(String key, String childKey) {
        ListIterator<Pair<String, JsonValue>> elemIter = jsonElements.listIterator();
        while (elemIter.hasNext()) {
            Pair<String, JsonValue> elem = elemIter.next();
            if (elem.getKey().equals(key)) {
                JsonValue parent = elem.getValue();
                parent.asObject().remove(childKey);
                return;
            }
        }
    }

    public Pair<String, JsonValue> getElement(int i) {
        if (i < jsonElements.size()) {
            return jsonElements.get(i);
        }
        return null;
    }

    public int size() {
        return jsonElements.size();
    }

    public String getValueAsString(String key) {
        ListIterator<Pair<String, JsonValue>> elemIter = jsonElements.listIterator();
        while (elemIter.hasNext()) {
            Pair<String, JsonValue> elem = elemIter.next();
            if (elem.getKey().equals(key)) {
                JsonValue val = elem.getValue();
                if (val.isString()) {
                    return val.asString();
                }
                return val.toString();
            }
        }
        return null;
    }

    public JsonValue getJsonValue(String key) {
        ListIterator<Pair<String, JsonValue>> elemIter = jsonElements.listIterator();
        while (elemIter.hasNext()) {
            Pair<String, JsonValue> elem = elemIter.next();
            if (elem.getKey().equals(key)) {
                return elem.getValue();
            }
        }
        return null;
    }

    public boolean setValue(String key, String value) {
        return setValue(key, Json.value(value));
    }

    public boolean setValue(String key, JsonValue value) {
        ListIterator<Pair<String, JsonValue>> elemIter = jsonElements.listIterator();
        while (elemIter.hasNext()) {
            Pair<String, JsonValue> elem = elemIter.next();
            if (elem.getKey().equals(key)) {
                elem.setValue(value);
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the value a child key-value pair
     *
     * @param key      - The key of the parent field (value is assumed to be a Json Object)
     * @param childKey - The key of the child field to add
     * @param value    - The value of the child field (type String)
     */
    public void setChildElement(String key, String childKey, String value) {
        JsonValue val = getJsonValue(key);
        if (val != null && val.isObject()) {
            JsonObject obj = val.asObject();
            obj.set(childKey, value);
        }
    }

    public JsonValue getChildElement(String key, String childKey) {
        JsonValue child = null;
        JsonValue val = getJsonValue(key);
        if (val != null && val.isObject()) {
            JsonObject obj = val.asObject();
            child = obj.get(childKey);
        }
        return child;
    }


    public boolean containsField(String key) {
        ListIterator<Pair<String, JsonValue>> elemIter = jsonElements.listIterator();
        while (elemIter.hasNext()) {
            Pair<String, JsonValue> elem = elemIter.next();
            if (elem.getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    public int convertToJson(String data) {
        jsonString = data;
        if (data.length() < 2) {
            return 0;
        }

        // remove trailing whitespaces and the enclosing brackets "{}" from the data
        data = data.trim().substring(1, data.length() - 1);

        String[] temp = data.split("(,\n)");
        for (String elem : temp) {
            String[] tempPair = elem.split(":");
            String key = tempPair[0].trim();
            String value = tempPair[1].trim();
            for (int i = 2; i < tempPair.length; i++) {
                value = value + ":" + tempPair[i];
            }

            if (value.startsWith("\"") && value.endsWith("\"")) {
                addElement(key.substring(1, key.length() - 1), value.substring(1, value.length() - 1));
            } else {
                addElement(key.substring(1, key.length() - 1), value);
            }
        }

        return jsonElements.size();
    }

    /* types:
     * 0 = null
     * 1 = String
     * 2 = Number
     * 3 = Boolean
     * 4 = Array
     * 5 = Json Object
     * */
    public int getType(JsonValue value) {
        if (value.isNull()) {
            return 0;
        }
        if (value.isString()) {
            return 1;
        }
        if (value.isNumber()) {
            return 2;
        }
        if (value.isBoolean()) {
            return 3;
        }
        if (value.isArray()) {
            return 4;
        }
        if (value.isObject()) {
            return 5;
        }
        // not found
        return -1;
    }

    public String convertToString() {
        if (jsonElements.size() == 0) {
            return "";
        }
        String data = "{\n";
        ListIterator<Pair<String, JsonValue>> elemIter = jsonElements.listIterator();
        while (elemIter.hasNext()) {
            Pair<String, JsonValue> elem = elemIter.next();
            data = data + "\"" + elem.getKey() + "\" : " + elem.getValue().toString() + ",\n";
        }
        data = data.substring(0, data.length() - 2) + "\n}";

        jsonString = data;
        return data;
    }

    public void showJsonString() {
        System.out.println(jsonString);
    }

    public void showJsonElements() {
        ListIterator<Pair<String, JsonValue>> elemIter = jsonElements.listIterator();
        while (elemIter.hasNext()) {
            Pair<String, JsonValue> elem = elemIter.next();
            System.out.println(elem.getKey() + " : " + elem.getValue().asString());
        }
    }

    protected boolean loadFromFile(String fullpath) {
        boolean bLoaded = false;
        try {
            final Reader reader = new InputStreamReader(new BOMInputStream(new FileInputStream(fullpath)), "UTF-8");
            JsonObject obj = Json.parse(reader).asObject();

            List<String> names = obj.names();
            for (String name : names) {
                addElement(name, obj.get(name));
            }
            reader.close();
            bLoaded = true;
        } catch (FileNotFoundException fe) {
            System.out.println("[ERROR] file " + fullpath + " not found.");
            fe.printStackTrace();
        } catch (UnsupportedEncodingException ue) {
            System.out.println("[ERROR] file " + fullpath + " unsupported encoding.");
            ue.printStackTrace();
        } catch (IOException ie) {
            System.out.println("[ERROR] file " + fullpath + " IOException.");
            ie.printStackTrace();
        } finally {
            return bLoaded;
        }
    }
}
