package com.modirum.generic.utility;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CSVTestParams {
    /**
     * getJsonData()
     */
    public static JsonData getJsonData(String fullpath, String[] headers) {
        System.out.println("Default Json Msg PATH: " + fullpath);
        List<CSVRecord> records = getRecords(fullpath);
        int rowCount = records.size();

        if (rowCount == 0) {
            return null;
        }

        JsonData json = new JsonData();
        String[] row;

        for (CSVRecord rec : records) {
            row = getRow(rec, headers, 2);
            json.addElement(row[0], row[1]);
        }

        System.out.println("Default Json Msg end read");
        return json;
    }

    /**
     * getRecordsAsArrayByColHeaders() - Gets the Data params from the CSV file, given the column headers
     *
     * @param fullpath
     * @param headers
     * @return
     */
    public static Object[][] getRecordsAsArrayByColHeaders(String fullpath, String[] headers) {
        System.out.println("CSV Data PATH: " + fullpath);
        List<CSVRecord> records = getRecords(fullpath);
        int rowCount = records.size();
        int colCount = headers.length;

        if (rowCount == 0) {
            return null;
        }

        Object[][] dp = new Object[rowCount][colCount];
        int row = 0;

        for (CSVRecord rec : records) {
            dp[row] = getRow(rec, headers, colCount);
            row++;
        }

        System.out.println("CSV Data end read");
        return dp;
    }

    /**
     * getRecordsAsMapByColHeaders() - Gets the Expected Data mapping from CSV file
     *
     * @param fullpath
     * @param category
     * @param fields
     * @return
     */
    public static Map<String, String[]> getRecordsAsMapByColHeaders(Map<String, String[]> expData, String fullpath, String category, String[] fields) {
        System.out.println("CSV Expected Data PATH: " + fullpath);
        List<CSVRecord> records = getRecords(fullpath);
        int rowCount = records.size();
        int colCount = fields.length;

        if (rowCount == 0) {
            return null;
        }

        if (expData == null) {
            expData = new HashMap<String, String[]>();
        }

        for (CSVRecord record : records) {
            String resCategory = record.get(category);
            expData.put(resCategory, getRow(record, fields, colCount));
        }

        System.out.println("CSV Expected Data end read");
        return expData;
    }

    /**
     * getRecords() - Opens the CSV file and gets the list of CSVRecords
     *
     * @param fullpath
     * @return
     */
    public static List<CSVRecord> getRecords(String fullpath) {
        List<CSVRecord> records = null;
        try {
            final Reader reader = new InputStreamReader(new BOMInputStream(new FileInputStream(fullpath)), "UTF-8");

            CSVParser parser = new CSVParser(reader,
                                             CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreEmptyLines().withIgnoreSurroundingSpaces());
            records = parser.getRecords();
            parser.close();
            reader.close();
        } catch (FileNotFoundException fe) {
            System.out.println("[ERROR] file " + fullpath + " not found.");
            fe.printStackTrace();
        } catch (UnsupportedEncodingException ue) {
            System.out.println("[ERROR] file " + fullpath + " unsupported encoding.");
            ue.printStackTrace();
        } catch (IOException ie) {
            System.out.println("[ERROR] file " + fullpath + " IOException.");
            ie.printStackTrace();
        } finally {
            return records;
        }
    }

    /**
     * getRow() - converts 1 CSVRecord to a String[] using the given headers
     *
     * @param record
     * @param headers
     * @param colCount
     * @return
     */
    public static String[] getRow(CSVRecord record, String[] headers, int colCount) {
        String[] row = new String[colCount];

        for (int i = 0; i < colCount; i++) {
            row[i] = record.get(headers[i]);
            //System.out.println("record[" + i + "][" + row[i] + "]");
        }
        return row;
    }

    public static Object[][] createListDP(Object[][] dataSource, int maxRows, int numFieldsPerTest, String testCategory, String testCondition) {
        Object[][] temp = new Object[maxRows][1];
        int i = 0;
        int j = 0;

        List<String> testFields = new LinkedList<String>();

        for (Object[] field : dataSource) {
            String category = (String) field[1];
            String condition = "";
            String element = (String) field[0];

            if (!testCondition.equals("")) { // check CONDITIONAL GROUP column only if testCondition is specified
                condition = (String) field[2];
            }
            if (category.equals(testCategory) && condition.equals(testCondition)) {
                if (j == numFieldsPerTest) {
                    temp[i][0] = testFields;
                    testFields = new LinkedList<String>();
                    j = 0;
                    i += 1;
                }
                testFields.add((String) field[0]);
                j += 1;
            }
        }

        if (j != 0) {
            temp[i][0] = testFields;
        }
        Object[][] dp = new Object[i + 1][1];
        for (j = 0; j < i + 1; j++) {
            dp[j] = temp[j];
        }
        return dp;
    }

    public static Object[][] createStringDP(Object[][] dataSource, int maxRows, String testCategory, String testCondition) {
        Object[][] temp = new Object[maxRows][1];
        int i = 0;

        for (Object[] field : dataSource) {
            String category = (String) field[1];
            String condition = "";
            if (!testCondition.equals("")) { // check CONDITIONAL GROUP column only if testCondition is specified
                condition = (String) field[2];
            }
            if (category.equals(testCategory) && condition.equals(testCondition)) {
                temp[i][0] = field[0];
                i += 1;
            }
        }
        Object[][] dp = new Object[i][1];
        for (int j = 0; j < i; j++) {
            dp[j] = temp[j];
        }
        return dp;
    }

}
