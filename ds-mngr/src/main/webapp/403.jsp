<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page isErrorPage = "true"%>
<html>
<head>
    <title>Page forbidden</title>
</head>
<body>
<div align="center">
    Sorry, page not accessible.
    <br/><br/>
    <input type="button" value="Go to main page" onclick="window.location='<%=request.getContextPath() %>/main/main.html'; return false;"/>
</div>
</body>
</html>
