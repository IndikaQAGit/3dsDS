
function xfindPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft;
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function xfindPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop;
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
			
return curtop;
}

function xshowPopup(posobjid, refId)
{
	var obj=document.getElementById(posobjid);
	var x=0;
	var y=0;
	if (obj!=null)
	{ 	objwidth=obj.offsetWidth;
		x=xfindPosX(obj)+objwidth+1;
		y=xfindPosY(obj);
	}
	xshowPopupAt(refId, x, y)
}

function xshowPopupAt(refId, x, y)
{

	var popup=document.getElementById(refId);
	if (popup!=null)
	{
		popup.style.display = "block";
		popup.style.position="absolute";
		popup.style.left=x+"px";
		popup.style.top=y+"px";
		popup.style.zIndex=2;
		popup.style.visibility = "visible";
		//if (popup.offsetWidth>180)
		//	popup.style.width = 180+"px";

	var popupiebug1=document.getElementById("iebug1");
	
	if (popupiebug1!=null)
	{
	
		popupiebug1.style.width = popup.offsetWidth;
    	popupiebug1.style.height = popup.offsetHeight;
    	popupiebug1.style.top = popup.style.top;
    	popupiebug1.style.left = popup.style.left;
    	popupiebug1.style.zIndex = popup.style.zIndex - 1;
    	popupiebug1.style.display = "block";
    	popupiebug1.style.visibility = "visible";
	}

	}


}	

function xhidePopup(refId)
{

	var popup=document.getElementById(refId);
	if (popup!=null)
	{
		popup.style.visibility = "hidden";
		popup.style.display = "none";

	var popupiebug1=document.getElementById("iebug1");
	
		if (popupiebug1!=null)
		{
			popupiebug1.style.visibility = "hidden";
			popupiebug1.style.display = "none";
		}

	}


}	


//

function showWaitBox()
{
	var waitboxel=document.getElementById('waitbox');
	if (waitboxel!=null)
	{	
		waitboxel.style.display="block";
		waitboxel.style.visibility = "visible";
		waitboxel.style.zIndex=10;

/*		var popupiebug1=document.getElementById("iebug1");
		if (popupiebug1!=null)
		{
			popupiebug1.style.width = waitboxel.offsetWidth;
	    	popupiebug1.style.height = waitboxel.offsetHeight;
	    	popupiebug1.style.top = waitboxel.style.top;
	    	popupiebug1.style.left = waitboxel.style.left;
	    	popupiebug1.style.zIndex = waitboxel.style.zIndex - 1;
	    	popupiebug1.style.display = "block";
	    	popupiebug1.style.visibility = "visible";
		}
*/		
	}
	
	
return true;
}

function fillDatesToday(dateFormat)
{
 fillDates(new Date(), new Date(), dateFormat)
}

function fillDatesYesterday(dateFormat)
{
	var yesterDay=new Date();
	yesterDay.setTime(yesterDay.getTime()-24*3600*1000);

 fillDates(yesterDay, yesterDay, dateFormat);
}

function fillDatesThisMonth(dateFormat)
{
	 var start=new Date();
	 start.setDate(1);
	 var end=new Date();
	 end.setDate(getNumDaysInMonth(end));
	 fillDates(start, end, dateFormat);
}

function fillDatesPrevMonth(dateFormat)
{
	 var start=new Date();
	 start.setDate(1);
	 start.setHours(0);
	 start.setMinutes(0);
	 start.setSeconds(0);
	 start.setMilliseconds(0);
	 start.setTime(start.getTime()-1);
	 start.setDate(1);
	 var end=new Date(start.getTime());
	 end.setDate(getNumDaysInMonth(end));
	 fillDates(start, end, dateFormat);
}

function fillDates(from, to, dateFormat)
{
 var fromField=document.getElementById("dateFrom");
	if (fromField!=null)
		fromField.value=formatDate(from, dateFormat);

	var toField=document.getElementById("dateTo");
	if (toField!=null)
		toField.value=formatDate(to, dateFormat);
}

var monthNames=new Array();
monthNames['en']=new Array("Jan","Feb","Mar", "Apr","May","Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Det");
monthNames['et']=new Array("Jaanuar","Veebruar","M&auml;rts", "Aprill","Mai","Juuni","Juuli", "August", "September", "Oktoober", "November", "Detsember");
var lang="en";


function formatDate(iDate, ldateFormat)
{
	if (iDate==null) return "";
	var day=iDate.getDate();
	var month=iDate.getMonth();
	var year=iDate.getFullYear();
	var yearShort=iDate.getYear();
	
	var humanYear=""+year;
	while (humanYear.length<4) humanYear="0"+humanYear;
	
	var humanYearShort=""+yearShort;
	while (humanYearShort.length<2) humanYearShort="0"+humanYearShort;
	
	var humanMon=month+1;
	var humanMon2=humanMon;
	if (humanMon2<10) humanMon2="0"+humanMon2;
	
	var humanDay=day;
	var humanDay2=day;
	if (humanDay2<10) humanDay2="0"+humanDay2;

	var tmp = ldateFormat;
	tmp = tmp.replace("dd", humanDay2);
	tmp = tmp.replace("d",humanDay);
	tmp = tmp.replace("mmm",monthNames[lang][month]);
	tmp = tmp.replace("mm",humanMon2);
	tmp = tmp.replace("m",humanMon);
	tmp = tmp.replace("yyyy", humanYear);
	tmp = tmp.replace("yy", humanYearShort);
	
return tmp;
}	

function getNumDaysInMonth(date)
{

var mymonth=date.getUTCMonth();
var myyear=date.getUTCFullYear();
var day=1000*3600*24;

var nextMonth=(mymonth<11 ? mymonth+1 : 0);
var nextYear=(mymonth<11 ? myyear : myyear+1);

var currDate=new Date();
	currDate.setUTCFullYear(myyear);
	currDate.setUTCMonth(mymonth, 1);
	currDate.setUTCHours(0);
	currDate.setUTCMinutes(0);
	currDate.setUTCSeconds(0);
	currDate.setUTCMilliseconds(0);
	//currDate.setUTCDate(1);
	
var nextDate=new Date();
	nextDate.setUTCFullYear(nextYear);
	nextDate.setUTCMonth(nextMonth, 1);
	//nextDate.setUTCDate(1);
	nextDate.setUTCHours(0);
	nextDate.setUTCMinutes(0);
	nextDate.setUTCSeconds(0);
	nextDate.setUTCMilliseconds(0);
	
var days=(nextDate.getTime()-currDate.getTime())/day;

//	alert ("num of days="+days);

if (days>31) 
		days=31;
	
return days;
}

function blankCriterias(formid)
{
	var form=document.getElementById(formid);
	if (form!=null)
	{
		var formElements=form.elements;
		for(i=0; i<formElements.length; i++)
		{
			//alert("name "+formElements[i].name+ "type "+formElements[i].type);
			if (formElements[i].type=="select" || formElements[i].type=="select-one" || formElements[i].type=="select-multiple")
			{
				try	  {
					formElements[i].selectedIndex=0;
					
					if(formElements[i].id=="limit") {
						formElements[i].selectedIndex=1;
					}
					
				} catch(err) { }
			}
			else if (formElements[i].type=="text" || formElements[i].type=="textarea" )
			{
				try	  {	formElements[i].value="";} catch(err) { } 
				}
				else if (formElements[i].type=="checkbox")
			{
				try	  {	formElements[i].checked=false;} catch(err) { } 
				}
				
		}
	}
}

var symbolsRegExp = new RegExp('[!#%&$_:@-]');
var topBadPasswords=new Array();
topBadPasswords.push("123456", "password", "12345", "12345678", "qwerty", "123456789", "1234",
	"baseball", "dragon","football","1234567","monkey","letmein", "abc123","111111","mustang",
	"access", "shadow", "master","michael", "password123", "password1234");

// return 10+ absolute good 0 absolute weak bad
function calculatePasswordStrength(value) {
	var passwordStrength = 0;
	if (value!=null) 
	{

		var c1=0;
		var c2=0;
		var c3=0;
		
		if (value.length<1)
		{
			return -1;
		}	
		if (value.length<6)
		{
			return 0;
		}	
		
		for (i=0; i<topBadPasswords.length; i++)
		{
			if (value.toLowerCase().indexOf(topBadPasswords[i])==0)
			{
				return 1;
			}
		}
		
		var seqCount=0;
		var groupLowerCase = {};
		var groupUpperCase = {};
		var groupNumbers = {};
		var groupSymbols = {};
		for (var i = 0; i < value.length; i++) 
		{
			var char = value[i];
			if ('0' <= char && char <= '9') {
				groupNumbers[char] = 1;
			} else if (symbolsRegExp.test(char)) {
				groupSymbols[char] = 1;
			} else if (char === char.toLowerCase()) {
				groupLowerCase[char] = 1;
			} else if (char === char.toUpperCase()) {
				groupUpperCase[char] = 1;
			}
		
			if (i>=2)
			{
				c1=value.charCodeAt(i-2);
				c2=value.charCodeAt(i-1);
				c3=value.charCodeAt(i);
				if (Math.abs(c2-c1)==1 && Math.abs(c3-c2)==1)
				{
					seqCount++;
				}	
			}	
		}
		passwordStrength = calculateGroupStrength(groupNumbers)
			+ calculateGroupStrength(groupSymbols)
			+ calculateGroupStrength(groupLowerCase)
			+ calculateGroupStrength(groupUpperCase)
			+ (value.length > 8 ? 1 : 0)
			+ (value.length > 16 ? 2 : 0);
		
		if (passwordStrength>3)
		{	
			passwordStrength=Math.round((passwordStrength*10-(3*passwordStrength*seqCount/value.length))/10);
		}	
		
		
	}
	return passwordStrength;
}

function calculateGroupStrength(group) {
	return Math.min(Object.keys(group).length, 2);
}

function getPasswordStrengthFeedback(element) {
	
	var passwordStrengthFeedback = document.getElementById('passwordStrengthFeedback-'+element.id);
	if (passwordStrengthFeedback==null)
	{
		passwordStrengthFeedback = document.getElementById('passwordStrengthFeedback');
	}
	
	if (passwordStrengthFeedback!=null)
	{	
		var passwordStrength = calculatePasswordStrength(element.value);
		var txt="";
		var color='';
		var display = 'block';
		if (passwordStrength > 7) 
		{
			txt="Very strong"
			color="#00CA00";
		}
		else if (passwordStrength > 5) 
		{
			txt="Strong";
			color="#00FF00";
		}
		else if (passwordStrength >4) 
		{
			txt="Better";
			color="#DDDD00";
		} 
		else if (passwordStrength >2) 
		{
			txt="Fair";
			color="orange";
		}
		else if (passwordStrength < 3 && passwordStrength>-1) 
		{
			txt="Weak";
			color="red";
		} else 
		{
			display = 'none';
		}
		
		passwordStrengthFeedback.style.display = display;
		//passwordStrengthFeedback.style.width="120px";
		//passwordStrengthFeedback.style.backgroundColor=color;
		var bars="";
		for (i=0; i<8; i++)
		{
			var bcolor="#eeeeee"
			if (i<passwordStrength || i <1)
			{
				bcolor=color;
			}	
			bars=bars+'<span style="display: inline-block; border: solid 1px; margin-right: 1px; width:8px; background-color: '+bcolor+'">&nbsp;</span>';	
		}	
		passwordStrengthFeedback.innerHTML = bars+" "+txt;
	}
}



function submitFormWithCmd(button, formid, cmd)
{
		var cform=document.getElementById(formid);
		if (cform!=null)
		{	if (cmd!=null)
			{
				cform.cmd.value=cmd;
			}
			if (button!=null) 
			{	
				button.disabled=true;
			}
			//appControlUsed();
			cform.submit();
		}
}

function showPopupNat(refId)
{

	var popup=document.getElementById(refId);
	if (popup!=null)
	{
		popup.style.display = "block";
		popup.style.position="absolute";
//		popup.style.left=x+"px";
//		popup.style.top=y+"px";
//	popup.style.zIndex=2;
		popup.style.visibility = "visible";

	var popupiebug1=document.getElementById("iebug1");
	
	if (popupiebug1!=null)
	{
	
		popupiebug1.style.width = popup.offsetWidth;
    	popupiebug1.style.height = popup.offsetHeight;
    	popupiebug1.style.top = popup.style.top;
    	popupiebug1.style.left = popup.style.left;
    	popupiebug1.style.zIndex = popup.style.zIndex - 1;
    	popupiebug1.style.display = "block";
    	popupiebug1.style.visibility = "visible";
	}

	}
}	

function unDisplay(refId)
{
	var popup=document.getElementById(refId);
	if (popup!=null)
	{
		popup.style.display = "none";
	}	
}

function getPasswordStrengthFeedback(element) {
    //populateMessages(); unsafe here
    var passwordStrengthFeedback = document.getElementById('passwordStrengthFeedback-'+element.id);
    if (passwordStrengthFeedback==null)
    {
        passwordStrengthFeedback = document.getElementById('passwordStrengthFeedback');
    }

    if (passwordStrengthFeedback!=null)
    {
        var passwordStrength = calculatePasswordStrength(element.value);
        var txt="";
        var color='';
        var display = 'block';
        if (passwordStrength > 7)
        {
            txt=/*messages["password.very.strong"] != null ? messages["password.very.strong"]:*/ "Very Strong";
            color="#00CA00";
        }
        else if (passwordStrength > 5)
        {
            txt=/*messages["password.strong"] != null ? messages["password.strong"]:*/ "Strong";
            color="#00FF00";
        }
        else if (passwordStrength >4)
        {
            txt=/*messages["password.better"] != null ? messages["password.better"]:*/ "Better";
            color="#DDDD00";
        }
        else if (passwordStrength >2)
        {
            txt=/*messages["password.fair"] != null ? messages["password.fair"]:*/ "Fair";
            color="orange";
        }
        else if (passwordStrength < 3 && passwordStrength>-1)
        {
            txt=/*messages["password.weak"] != null ? messages["password.weak"]:*/ "Weak";
            color="red";
        } else
        {
            display = 'none';
        }

        passwordStrengthFeedback.style.display = display;
        //passwordStrengthFeedback.style.width="120px";
        //passwordStrengthFeedback.style.backgroundColor=color;
        var bars="";
        for (i=0; i<8; i++)
        {
            var bcolor="#eeeeee";
            if (i<passwordStrength || i <1)
            {
                bcolor=color;
            }
            bars=bars+'<span style="display: inline-block; border: solid 1px; margin-right: 1px; width:8px; background-color: '+bcolor+'">&nbsp;</span>';
        }
        passwordStrengthFeedback.innerHTML = bars+" "+txt;
    }
}

function showGenSecretPopUp(targetInputId, length, sha1pnrg)
{
    //populateMessages();
    var input=document.getElementById(targetInputId);
    if (input!=null)
    {
        var finalRandom=getValidRndFromSha1Pnrg(length, sha1pnrg);
        var useText = /*messages["auto.use"] != null ? messages["auto.use"] :*/ "Use";
        var rndPopUpContent="<div style=\"padding: 10px 10px 10px 10px;\"><form name=\"x\">";
        rndPopUpContent=rndPopUpContent+ useText +": <b><span style=\"font-family: Roman;\">"+finalRandom+"</span></b><br/>";
        rndPopUpContent=rndPopUpContent+"<input type=\"button\" value=\"Ok\" onclick=\"setValue('"+targetInputId+"','"+finalRandom+"');closeGenSecretPopUp('"+targetInputId+"');document.getElementById('"+targetInputId+"').oninput();\"/>";
        //rndPopUpContent=rndPopUpContent+"<input type='button' value='Cancel' onclick=\"closeGenSecretPopUp('"+targetInputId+"');\"/>";
        rndPopUpContent=rndPopUpContent+"</form></div>";

        var rndPopUpEl=document.getElementById('rndpopup'+targetInputId);
        if (rndPopUpEl!=null)
        {
            rndPopUpEl.innerHTML=rndPopUpContent;
            showPopupNat('rndpopup'+targetInputId);
        }

    }
}

function closeGenSecretPopUp(targetInputId)
{
    var rndPopUpEl=document.getElementById('rndpopup'+targetInputId);
    if (rndPopUpEl!=null)
    {
        rndPopUpEl.innerHTML="";
    }
    xhidePopup('rndpopup'+targetInputId);
}

function setValue(targetInputId, value)
{
    var input=document.getElementById(targetInputId);
    if (input!=null)
    {
        input.value=value;
    }
}

function getValidRndFromSha1Pnrg(length, sha1pnrg)
{
    var finalRandom="";
    var posArray=new Array();
    var charCount=0;
    var numCount=0;
    while(finalRandom.length<length)
    {
        var posTake=Math.random()*sha1pnrg.length;
        if (posArray.indexOf(posTake)<0)
        {
            var xchar=sha1pnrg.charAt(posTake);
            if (isNaN(xchar))
            {
                charCount++;
            }
            else
            {
                numCount++;
            }

            finalRandom=finalRandom+xchar;
            posArray.push(posTake);
        }
    }

    if (charCount<1 || numCount<1)
    {
        return getValidRndFromSha1Pnrg(length, sha1pnrg);
    }

    return finalRandom;
}

function confirmWarning(event, displayText) {
	if (!confirm(displayText)) {
		event.preventDefault();
		return false;
	}
	return true;
}

