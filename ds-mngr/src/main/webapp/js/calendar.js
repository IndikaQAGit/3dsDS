/*
Calendar (C) By AZS Services Ltd. 2005
http://www.a2zss.com

Donated as free to Modirum and Emporiki Bank


recognized format symbols are:
yyyy - year
yy - year
d - day of month
dd - day of month
m - month
mm - month
mmm - month name
*/

var foreground=null;
var background=null;

var mouseOverColor="rgb(150,210,245)";
var selectedDayColor="rgb(255,245,25)";
var sundayColor="#ff0000";

// the style of year and month links
/*
A.noul:link {  text-decoration:none; }
A.noul:visited { text-decoration:none; }
A.noul:hover {  text-decoration:underline; }
*/
var linkStyleClass1="noul";
// the style of dates links
/*A.nodec { text-decoration:none; }
A.nodec:link { text-decoration:none; }
A.nodec:visited { text-decoration:none; }
A.nodec:hover { text-decoration:none; }
*/
var linkStyleClass2="nodec";
var popupHeadStyleClass="calpopuphead";
var calContentStyleClass="calcontent";
var closeImg="../img/x.gif";


var minYear=0;
var maxYear=3000;

var weekDays=new Array();
weekDays['en']=new Array("Su","Mo","Tu", "We","Th","Fr","Sa");
weekDays['et']=new Array("P","E","T", "K","N","R","L");

var monthNames=new Array();
monthNames['en']=new Array("Jan","Feb","Mar", "Apr","May","Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Det");
monthNames['et']=new Array("Jaanuar","Veebruar","M&auml;rts", "Aprill","Mai","Juuni","Juuli", "August", "September", "Oktoober", "November", "Detsember");

var weekStartDay=new Array();
weekStartDay['en']=0;
weekStartDay['et']=1;

var calendarDate=new Date();
var dateFormat="yyyy-mm-dd";
var lang="en";
var srcDstObjId=null;


function drawCalendar(dateStr, ldateFormat, llang)
{
	var initDate=new Date();
	if (dateStr!=null && dateStr.length>0)
	{
	initDate=parseDate(dateStr, ldateFormat);
	}	
	
return 	drawCalendarImpl(initDate, ldateFormat, llang);
}

	
function drawCalendarImpl(date, ldateFormat, llang)
{
calendarDate=date;
dateFormat=ldateFormat;

	if (llang==null || (llang!='et' && llang!='en') )
		lang="en";
	else lang=llang;

	//var date=new Date(2004,2,05);
	var today=date.getUTCDate();
	var month=date.getUTCMonth();
	var year=date.getUTCFullYear();
	
	
	var fstdayOfMonth=new Date();
		fstdayOfMonth.setUTCFullYear(year);
		fstdayOfMonth.setUTCMonth(month, 1);
		//fstdayOfMonth.setUTCDate(1);
		
	var fstDayOfMonthDay=fstdayOfMonth.getUTCDay();
		
	var numOfDays=getNumDaysInMonth(date);
	

	var calHtml="<div class=\""+calContentStyleClass+"\" id=\"calcontent\">";
	calHtml+="<table width=\"100%\" border='0'><tr><td>";
	
	var yearPanel="<table width=\"100%\" border='0' cellspacing='0'>";
		yearPanel+="<tr><td align=\"left\">"+
			 "<a class=\""+linkStyleClass1+"\" href=\"javascript:changeYear(-10);\">-10</a> "+
			 "<a class=\""+linkStyleClass1+"\" href=\"javascript:changeYear(-1);\">&lt;&lt;</a>"+
			 "</td><td align=\"center\">"+year+"</td><td align=\"right\">"+
			  "<a class=\""+linkStyleClass1+"\" href=\"javascript:changeYear(1);\">&gt;&gt;</a> "+
			  "<a class=\""+linkStyleClass1+"\" href=\"javascript:changeYear(10);\">+10</a>"+
			  "</td></tr>";
	yearPanel+="</table>";
	calHtml+=yearPanel;

	var headerMonthPanel="<table width=\"100%\" border='0' cellspacing='0'>";
	var currentMonthName=monthNames[lang][month];
	headerMonthPanel+="<tr><td align=\"left\">"+
	 "<a class=\""+linkStyleClass1+"\" href=\"javascript:changeMonth(-1);\"> &lt;&lt; </a>"
	 +"</td><td align=\"center\">"+currentMonthName+"</td><td align=\"right\">"+
	  "<a class=\""+linkStyleClass1+"\" href=\"javascript:changeMonth(1);\"> &gt;&gt; </a>"+
	  "</td></tr>";
	headerMonthPanel+="</table>";
	
	calHtml+=headerMonthPanel;
	calHtml+="<table width=\"100%\" border='1' cellspacing='0'>";

	calHtml+="<tr>";
	for (i=weekStartDay[lang]; i<7+weekStartDay[lang]; i++)
	{
		var dayX=i%7;
		if (dayX==0)
		{	calHtml+="<th width='15' style='color: "+sundayColor+"'>"+weekDays[lang][dayX] +"</th>";}
				 
		else
		calHtml+="<th width='15'>"+weekDays[lang][dayX] +"</th>";
	}	

	calHtml+="</tr>";
	
	var ccday=1;
	var inrange=false;
	var testv=0;
	do 
	{
		testv++;
		calHtml+="<tr>";
		
		for (j=weekStartDay[lang]; j<7+weekStartDay[lang]; j++)
		{
			var dayj=j%7;
			
			if (fstDayOfMonthDay==dayj && ccday==1) { inrange=true; }
			if (ccday>numOfDays) { inrange=false; }

			calHtml+="<td align='center'";
			var selBgc=""; // default selected item color
			if (inrange && today==ccday) selBgc="style=\"background-color: "+selectedDayColor+";\"";
			calHtml+=selBgc+">"; // td

			var color=""; // nu,mber color
			if (inrange && dayj==0) color="style='color: "+sundayColor+"'"; // sundays
			

				if (inrange) 
				{ calHtml+="<a class=\""+linkStyleClass2+"\" "+color+" href=\"javascript:setFieldValue("+ccday+");\">"+
				"<span onMouseOver=\"style.background='"+mouseOverColor+"';\" onMouseOut=\"style.background='';\""+
				">"+(ccday)+"</span></a>";
				  ccday++;
				}
				else calHtml+="&nbsp;";
			
			calHtml+="</td>";
		}	
		calHtml+="</tr>";
	}  while(ccday<=numOfDays && testv<7);
	
	calHtml+="</table>";

	calHtml+="</td></tr></table>";
// debug	
//	calHtml+="numdays="+numOfDays+"<br>";
//	calHtml+="date="+date;
	calHtml+="</div>";

return calHtml;
}


function getNumDaysInMonth(date)
{

	var mymonth=date.getUTCMonth();
	var myyear=date.getUTCFullYear();
    var day=1000*3600*24;
    
	var nextMonth=(mymonth<11 ? mymonth+1 : 0);
	var nextYear=(mymonth<11 ? myyear : myyear+1);
	
	var currDate=new Date();
		currDate.setUTCFullYear(myyear);
		currDate.setUTCMonth(mymonth, 1);
		currDate.setUTCHours(0);
		currDate.setUTCMinutes(0);
		currDate.setUTCSeconds(0);
		currDate.setUTCMilliseconds(0);
		//currDate.setUTCDate(1);
		
	var nextDate=new Date();
		nextDate.setUTCFullYear(nextYear);
		nextDate.setUTCMonth(nextMonth, 1);
		//nextDate.setUTCDate(1);
		nextDate.setUTCHours(0);
		nextDate.setUTCMinutes(0);
		nextDate.setUTCSeconds(0);
		nextDate.setUTCMilliseconds(0);
		
	var days=(nextDate.getTime()-currDate.getTime())/day;

//	alert ("num of days="+days);
	
	if (days>31) days=31;
		
	return days;
}

function changeMonth(plusMinus)
{
	var dateOld=calendarDate;
	var month=dateOld.getUTCMonth();
	var year=dateOld.getUTCFullYear();
	
	var newDate=new Date();

//alert ("curr month="+month);	
		
		if (plusMinus==1)
		{
			if (month<11) 
				{ 
				month=month+1;	
				}
			else 
			{
			 month=0; 
			 if (year<maxYear) year=year+1;
			}

//alert ("month+="+month);
			newDate.setUTCFullYear(year);
			newDate.setUTCMonth(month, 1);
			//newDate.setUTCDate(1);
		}

		else if	(plusMinus==-1)
		{
			if (month>0) 
			{ 
				month=month-1;
			}
			else 
			{
			 month=11; 
			 if (year>minYear) year=year-1;
			}	
	
//alert ("month-="+month);
			newDate.setUTCFullYear(year);
			newDate.setUTCMonth(month, 1);
			//newDate.setUTCDate(1);
		}

	
//alert ("new date="+newDate);

	var calPopUpContent=createHeader();
	calPopUpContent+=drawCalendarImpl(newDate, dateFormat, lang);
	
	var calPopUpEl=document.getElementById('calpopup');
	if (calPopUpEl!=null)
	{	
	 calPopUpEl.innerHTML=calPopUpContent;
	}	

}

function changeYear(plusMinus)
{
var dateOld=calendarDate;
	var month=dateOld.getUTCMonth();
	var year=dateOld.getUTCFullYear();
	var newDate=new Date();
	
		if (plusMinus!=0)
		{
			if (year+plusMinus<=maxYear && year+plusMinus>=minYear) { year=year+plusMinus;}
			else if (year+plusMinus>maxYear) year=maxYear;
			else if (year+plusMinus<minYear) year=minYear;
			
			newDate.setUTCFullYear(year);
			newDate.setUTCMonth(month,1);
			//newDate.setUTCDate(1);
		}

	
	var calPopUpContent=createHeader();
	calPopUpContent+=drawCalendarImpl(newDate, dateFormat, lang);
	
	var calPopUpEl=document.getElementById('calpopup');
	if (calPopUpEl!=null)
	{	
	 calPopUpEl.innerHTML=calPopUpContent;
	}	

}



function createHeader()
{
return "<div class=\""+popupHeadStyleClass+"\"><a href=\"javascript:closeCalendar();\">"+
	"<img border=\"0\" src=\""+closeImg+"\" alt=\"X\"/></a></div>";
}
	
function showCalPopup(posobjid, dateSrcObjId, ldateFormat, llang)
{
	var calPopUpContent="";
	calPopUpContent+=createHeader();

	
	var obj=document.getElementById(posobjid);
	var x=0;
	var y=0;
	if (obj!=null)
	{ 	objwidth=obj.offsetWidth;
		x=findPosX(obj)+objwidth+1;
		y=findPosY(obj);
	}	
	
srcDstObjId=dateSrcObjId;
var dateValueStr="";
var srcDstObj=document.getElementById(dateSrcObjId);
if (srcDstObj!=null && srcDstObj.value.length>0)
{
	dateValueStr=srcDstObj.value;
	// dateValue=parseDate(srcDstObj.value, dateFormat);
}
//alert ("opening cal with init date:"+dateValueStr);

	var calHTML=drawCalendar(dateValueStr, ldateFormat, llang);
		calPopUpContent+=calHTML;
	
	var calPopUpEl=document.getElementById('calpopup');
	if (calPopUpEl!=null)
	{	
	 calPopUpEl.innerHTML=calPopUpContent;
	}	

	
	// make visible
	//var calpopup=document.getElementById("calpopup");
	showPopup("calpopup", x, y);
	
	
}


function setFieldValue(selDay)
{

if (calendarDate==null) calendarDate=new Date();
if (selDay!=null) calendarDate.setDate(selDay);
else calendarDate.setDate(1);

var srcDstObj=document.getElementById(srcDstObjId);
if (srcDstObj!=null)
{
	srcDstObj.value=formatDate(calendarDate, dateFormat);
}	
closeCalendar();

}	


function closeCalendar()
{
/*	var popuel=document.getElementById("calpopup");
	if (popuel!=null)
	{	popuel.style.visibility = "hidden";	}
*/	
	hidePopup("calpopup");
	
}


function formatDate(iDate, ldateFormat)
{
	if (iDate==null) return "";
	var day=iDate.getDate();
	var month=iDate.getMonth();
	var year=iDate.getFullYear();
	var yearShort=iDate.getYear();
	
	var humanYear=""+year;
	while (humanYear.length<4) humanYear="0"+humanYear;
	
	var humanYearShort=""+yearShort;
	while (humanYearShort.length<2) humanYearShort="0"+humanYearShort;
	
	var humanMon=month+1;
	var humanMon2=humanMon;
	if (humanMon2<10) humanMon2="0"+humanMon2;
	
	var humanDay=day;
	var humanDay2=day;
	if (humanDay2<10) humanDay2="0"+humanDay2;

	var tmp = ldateFormat;
	tmp = tmp.replace("dd", humanDay2);
	tmp = tmp.replace("d",humanDay);
	tmp = tmp.replace("mmm",monthNames[lang][month]);
	tmp = tmp.replace("mm",humanMon2);
	tmp = tmp.replace("m",humanMon);
	tmp = tmp.replace("yyyy", humanYear);
	tmp = tmp.replace("yy", humanYearShort);
	
return tmp;
}	

function parseDate(iDateStr, ldateFormat)
{
if (ldateFormat==null || ldateFormat.length<1)
ldateFormat=dateFormat;
	


// parse only ifiDate is not null
if (iDateStr==null || iDateStr.length<1) return null;

var tmpDate=new Date();


var day=0;
var mon=0;
var year=0;
var currYear=tmpDate.getFullYear();

// 1 get date components if separaotrs are used
var formatComps=null;
var dateComps=null;
var yearStr="";
	formatComps=ldateFormat.split(" ");
	dateComps=iDateStr.split(" ");

if (formatComps==null || formatComps.length<3)
{	formatComps=ldateFormat.split(".");
	dateComps=iDateStr.split(".");
}
if (formatComps==null || formatComps.length<3)
{	formatComps=ldateFormat.split("/");
	dateComps=iDateStr.split("/");
}	

if (formatComps==null || formatComps.length<3)
{	formatComps=ldateFormat.split("-");
	dateComps=iDateStr.split("-");
}	

	
if (formatComps!=null || formatComps.length>=3)
{
	for(i=0; i<formatComps.length && i<dateComps.length; i++)
	{
		if (("dd"==formatComps[i] || "d"==formatComps[i]) && dateComps[i]!=null)
		{
			day=parseInt(dateComps[i], 10);

		}	
		else if (("mm"==formatComps[i] || "m"==formatComps[i]) && dateComps[i]!=null)
		{
			mon=parseInt(dateComps[i], 10);
		}	
		else if (("yyyy"==formatComps[i] || "yy"==formatComps[i]) && dateComps[i]!=null)
		{
			year=parseInt(dateComps[i], 10);
			yearStr=dateComps[i];
		}	
	
		
	}

	if (!isNaN(year) && year<=maxYear && yearStr!=null)
	{	if (yearStr.length<=2 && year<30) year=Math.round(currYear/100)*100+year;
		else if (yearStr.length<=2 && year<99) year=(Math.round(currYear/100)-1)*100+year;
	
		tmpDate.setFullYear(year);
	}
	
	if (!isNaN(mon) && mon>0 && mon<13)
	{
		tmpDate.setMonth(mon-1);
	}
	if (!isNaN(day) && day>0 && day<32)
	{	
		tmpDate.setDate(day);
	}


	
}	
else {

// else if no separators used do only simpified
// day
//	var dayPos=ldateFormat.indexOf("dd");
//	var dayVal=iDateStr.substring(dayPos, dayPos+2);
	
	}

//alert("parsed date is: "+tmpDate);
		
return tmpDate;
}	

function findPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
			
return curtop;
}

function showPopup(refId, x, y)
{

	var popup=document.getElementById(refId);
	if (popup!=null)
	{
		popup.style.display = "block";
		popup.style.position="absolute";
		popup.style.left=x+"px";
		popup.style.top=y+"px";
		popup.style.zIndex=2;
		popup.style.visibility = "visible";
		//if (popup.offsetWidth>180)
		//	popup.style.width = 180+"px";

	var popupiebug1=document.getElementById("iebug1");
	
	if (popupiebug1!=null)
	{
	
		popupiebug1.style.width = popup.offsetWidth;
    	popupiebug1.style.height = popup.offsetHeight;
    	popupiebug1.style.top = popup.style.top;
    	popupiebug1.style.left = popup.style.left;
    	popupiebug1.style.zIndex = popup.style.zIndex - 1;
    	popupiebug1.style.display = "block";
    	popupiebug1.style.visibility = "visible";
	}

	}


}	

function hidePopup(refId)
{

	var popup=document.getElementById(refId);
	if (popup!=null)
	{
		popup.style.visibility = "hidden";
		popup.style.display = "none";

	var popupiebug1=document.getElementById("iebug1");
	
		if (popupiebug1!=null)
		{
			popupiebug1.style.visibility = "hidden";
			popupiebug1.style.display = "none";
		}

	}


}	


