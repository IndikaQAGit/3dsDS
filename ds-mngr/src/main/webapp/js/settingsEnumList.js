function switchAllSection() {
    var showAllToggleElement = document.getElementById("showAllSettingSections");
    if ( showAllToggleElement.innerHTML === "Show All + " ) {
        var settingSections = document.getElementsByClassName("setting-section-accordion");
        for (var i = 0; i < settingSections.length; i++) {
            showSection(settingSections.item(i));
        }
        showAllToggleElement.innerHTML = "Hide All -";
    } else {
        var settingSections = document.getElementsByClassName("setting-section-accordion");
        for (var i = 0; i < settingSections.length; i++) {
                hideSection(settingSections.item(i));
        }
        showAllToggleElement.innerHTML = "Show All + ";
    }
}

function switchSection(elementId) {
    var settingSection = document.getElementById(elementId);
    if ( settingSection.style.display != 'none' ) {
        hideSection(settingSection);
    } else {
        showSection(settingSection);
    }
}

function showSection(settingSection) {
    var settingSectionToggle = document.getElementById(settingSection.id + "-toggle");
    settingSection.style.display = '';
    settingSectionToggle.innerHTML = "Hide Section -";

    var hiddenInputSectionToggleFlag = document.getElementById(settingSection.id + "-active-flag");
    hiddenInputSectionToggleFlag.disabled = false;

}

function hideSection(settingSection) {
    var settingSectionToggle = document.getElementById(settingSection.id + "-toggle");
    settingSection.style.display = 'none';
    settingSectionToggle.innerHTML = "Show Section +";

    var hiddenInputSectionToggleFlag = document.getElementById(settingSection.id + "-active-flag");
    hiddenInputSectionToggleFlag.disabled = true;
}