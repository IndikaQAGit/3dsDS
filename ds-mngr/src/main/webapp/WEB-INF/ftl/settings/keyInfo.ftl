<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt;
        <span class="selected"><@global.text key='text.keyinfo'/></span></h1>
    <h2><@global.text key='text.keyinfo'/></h2>
    <#assign col1=110/>
    <form id="infoSearchForm" action="keyInfo.html" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.key.alias'/>:</span>
    <@global.spring.formInput path="searcher.keyword" attributes='size="30" maxlength="60"'/><br/>
    <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.key.status'/>:</span>
    <@global.spring.formSingleSelect path="searcher.value" options={"":"","working":"Working", "retired":"Retired"}/>
    <br/>
    <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
    <@global.spring.formSingleSelect path="searcher.order" options={"keyAlias":"Alias", "status":"Status"}/>
    <@global.spring.formSingleSelect path="searcher.orderDirection" options={"asc":"Ascending", "desc":"Descending"} />
    &nbsp;
    <span style="width: 106px; display: inline-block; text-align: right;"><@global.text key='text.results.per.page'/>:</span>
    <@global.spring.formSingleSelect path="searcher.limit" options={"50":"50", "100":"100"} />
    <br/>
    <input type="hidden" name="cmd" value="search"/>
    <input type="submit" name="submitbtn" value="<@global.text key='button.search'/>"/>
    <#if keyList?? && searcher?? && searcher.total &gt; 0 >
        <h2><@global.text key='text.found.total'/> ${(searcher.total!"")?html} keys,
            <@global.text key='text.showing'/> ${((searcher.start+1)!"")?html} <@global.text key='text.to'/>
            <#if searcher.total &gt; (searcher.start+searcher.limit)>
                ${(searcher.start+searcher.limit!"")?html}
            <#else>
                ${(searcher.total!"")?html}
            </#if>
        </h2>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="tdHeader" width="40"><@global.text key='text.key.id'/></td>
                <td class="tdHeader" width="160"><@global.text key='text.key.alias'/></td>
                <td class="tdHeader" width="65"><@global.text key='text.key.status'/></td>
                <td class="tdHeader" width="80"><@global.text key='text.key.checkvalue'/></td>
                <td class="tdHeader" width="100"><@global.text key='text.key.date'/></td>
                <td class="tdHeader" width="45"><@global.text key='text.key.days'/></td>
                <td class="tdHeader"><@global.text key='text.key.signedby'/>
                    / <@global.text key='text.key.x509info'/></td>
            </tr>
            <#list keyList as kx>
                <tr <#if kx_index%2==0> class="tr_even"<#else> class="tr_odd"</#if> >
                    <td>${(kx.getId()!"")?html}</td>
                    <td>${(kx.getKeyAlias()!"")?html}</td>
                    <td>${(kx.getStatus()!"")?html}</td>
                    <td>${(kx.getKeyCheckValue()!"")?html}</td>
                    <td><#if kx.getKeyDate()?exists>${kx.getKeyDate()?string(dateFormat+" HH:mm:ss")}</#if></td>
                    <td align="center">${((kx.getCryptoPeriodDays()?string)!"")?html}
                    <td>
                        <div style=" word-wrap:break-word; overflow-wrap: break-word;overflow:auto; font-size: smaller;">
                            ${(kx.getSignedBy1()!"")?html}<br/>
                            ${(kx.getSignedBy2()!"")?html}<br/>

                            <#if x509Info?? && x509Info["X509Subj."+kx.getId()?string]??>
                                <b><@global.text key='text.key.x509info'/></b>:
                                <br/>${(x509Info["X509Subj."+kx.getId()?string])?html}<br/></#if>
                            <#if x509Info?? && x509Info["X509Iss."+kx.getId()?string]??>${(x509Info["X509Iss."+kx.getId()?string])?html}
                                <br/></#if>
                            <#if x509Info?? && x509Info["X509Validity."+kx.getId()?string]??>${(x509Info["X509Validity."+kx.getId()?string])?html}
                                <br/></#if>
                            <#if x509Info?? && x509Info["X509KeyBits."+kx.getId()?string]??>${(x509Info["X509KeyBits."+kx.getId()?string])?html}</#if>
                        </div>
                    </td>
                </tr>
            </#list>
            <@global.pagingButtons searcher=searcher colspan=6/>
        </table>
        </form>
    <#else>
        <#if keyList??><h2>No matching data found</h2></#if>
    </#if>
</@global.page>