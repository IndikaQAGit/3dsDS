<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt;
        <span class="selected"><@global.text key='text.settings'/></span></h1>
    <h2><@global.text key='text.settings'/></h2>
    <@global.security.authorize access="hasAnyRole('adminSetup')">
        <a href="settingEdit.html">+<@global.text key='text.add.new'/></a>&nbsp;
    </@global.security.authorize>
    <!-- no shields avaiable at this time <a href="shields.html"><@global.text key='text.service.shields'/></a> -->
    <h2><@global.text key='text.search.settings'/></h2>
    <form id="textSearchForm" action="settingsList.html" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <span style="width: 120px; display: inline-block;"><@global.text key='text.keyword'/>:</span>
    <@global.spring.formInput path="s.keyword" attributes='size="30" maxlength="60"'/><br/>
    <span style="width: 120px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
    <@global.spring.formSingleSelect path="s.order" options={"key":"Key", "value":"Value","processorId":"Processor Id","lastModified":"Last modified"}/>
    <@global.spring.formSingleSelect path="s.orderDirection" options={"asc":"Ascending", "desc":"Descending"} />
    &nbsp;
    <span style="width: 109px; display: inline-block; text-align: right;"><@global.text key='text.results.per.page'/>:</span>
    <@global.spring.formSingleSelect path="s.limit" options={"25":"25", "50":"50", "100":"100"} />
    <br/>
    <input type="hidden" name="cmd" value="search"/>
    <input type="submit" name="submitbtn" value="<@global.text key='button.search'/>"/>

    <#if settingsList?exists && searcher?exists && searcher.total &gt; 0 >
        <h2><@global.text key='text.found.total'/> ${(searcher.total!"")?html} <@global.text key='text.settings'/>,
            <@global.text key='text.showing'/> ${((searcher.start+1)!"")?html} <@global.text key='text.to'/>
            <#if searcher.total &gt; (searcher.start+searcher.limit)>
                ${(searcher.start+searcher.limit!"")?html}
            <#else>
                ${(searcher.total!"")?html}
            </#if>
        </h2>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="tdHeader" width="270">Key</td>
                <td class="tdHeader" width="420">Value</td>
                <td class="tdHeader" width="170">Comment</td>
                <td class="tdHeader">Last modified</td>
            </tr>
            <#list settingsList as setting>
                <tr
                        <#if setting_index%2==0>
                            class="tr_even"
                        <#else>
                            class="tr_odd"
                        </#if>
                >
                    <td>
                        <div style=" word-wrap:break-word; width:270px; overflow-wrap: break-word;overflow:auto">
                            <#if settController?exists && settController.isSettingEdtiable(setting.getKey())>
                                <a href="settingEdit.html?sid=${(setting.getKey()!"")?html}&amp;procId=${(setting.getProcessorId()!"")?html}">${(setting.getKey()!"")?html}</a>
                            <#else>
                                ${(setting.getKey()!"")?html}
                            </#if>
                        </div>
                    </td>
                    <td>
                        <div style=" word-wrap:break-word; width:420px; overflow-wrap: break-word;overflow:auto; max-height:120px; height:auto !important; height:120px;">
                            ${(setting.getValue()!"")?html}
                        </div>
                    </td>
                    <td>
                        <div style=" word-wrap:break-word; width:160px; overflow-wrap: break-word;overflow:auto">
                            ${(setting.getComment()!"")?html}
                        </div>
                    </td>
                    <td>
                        <#if setting.lastModified?exists>${setting.lastModified?string(dateFormat+" HH:mm:ss")}</#if>
                        <#if setting.lastModifiedBy?exists> &nbsp; ${(setting.lastModifiedBy!"")?html}</#if>
                    </td>
                </tr>
            </#list>
            <@global.pagingButtons searcher=searcher colspan=4/>
        </table>
        </td>
        </tr>
        </table>
        </form>
    <#else>
        <#if settingsList?exists>
            <h2>No matching settings found</h2>
        </#if>
    </#if>

</@global.page>
