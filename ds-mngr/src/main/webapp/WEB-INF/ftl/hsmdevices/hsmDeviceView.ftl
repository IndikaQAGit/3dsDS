<#import "../common/global.ftl" as global/>
<@global.page>
    <#-- Page Headers -->
    <h1>
        <a href="../main/main.html"><@global.text key='text.home'/></a>
        &gt;
        <a href="hsmDeviceList.html"><@global.text key='text.HSMDevices'/></a>
        &gt;
        <span class="selected"><@global.text key='text.details'/>: ${(hsmDevice.name!"")?html}</span>
    </h1>
    <h2><@global.text key='text.HSMDevice'/></h2>

    <#-- HSM Device Details -->
    <#if hsmDevice??>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <form id="uForm" action="hsmDeviceEdit.html" method="post">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <input type="hidden" name="id" value="${(hsmDevice.id!"")?html}"/>
                        <input type="submit" name="button" value="<@global.text key='button.edit'/>"/>
                        <input type="button" name="delete" value="<@global.text key='button.delete'/>" onclick="deleteRecord();"/>
                    </form>
                </td>
                <td><br/></td>
            </tr>
            <tr>
                <td width="115" class="tdHeaderVertical"><@global.text key='text.HSMDevice.name'/>:</td>
                <td width="425">${(hsmDevice.name!"")?html}</td>
            </tr>
            <tr>
                <td width="115" class="tdHeaderVertical"><@global.text key='text.HSMDevice.className'/>:</td>
                <td width="425">${(hsmDevice.className!"")?html}</td>
            </tr>
            <tr>
                <td width="115" class="tdHeaderVertical"><@global.text key='text.HSMDeviceConf.config'/>:</td>
                <td width="425"><#if hsmDeviceConfig??>${(hsmDeviceConfig!"")?html}</#if></td>
            </tr>
            <tr>
                <td width="115" class="tdHeaderVertical"><@global.text key='text.status'/>:</td>
                <td width="425"><@global.displayStatus hsmDevice.status!""/></td>
            </tr>
        </table>
    </#if>
    <script type="text/javascript">
        function deleteRecord() {
            if (confirmWarning(event, '${deleteConfirmMsg}')) {
                document.getElementById('uForm').action = 'hsmDeviceDelete.html';
                document.getElementById('uForm').submit();
            }
        }
    </script>
</@global.page>