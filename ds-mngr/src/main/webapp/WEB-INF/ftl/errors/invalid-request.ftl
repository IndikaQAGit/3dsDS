<#-- @ftlvariable name="ex" type="Exception" -->
<#import "../common/global.ftl" as global/>
<@global.page hideLogout=false>
    <span class="error" style="text-align:center;">
    Error. Invalid request.
</span>
    <p>
        <#if ex?exists>
            ${ex?html}
        </#if>
    </p>
</@global.page>
