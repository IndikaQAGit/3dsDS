<#-- @ftlvariable name="ex" type="Exception" -->
<#import "../common/global.ftl" as global/>
<@global.page hideLogout=false>
    <span class="error" style="text-align:center;">
  Error. Service temporarily unavailable. Please try later.
</span>
    <p>
        <#if ex?exists>
            ${(ex.getMessage()!"")?html}
        </#if>
    </p>
</@global.page>
