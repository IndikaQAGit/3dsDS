<#import "../common/global.ftl" as global/>
<#import "paymentSystemSettingsTable.ftl" as settingsTable/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a>
        &gt;
        <a href="../paymentsystems/paymentsystemsList.html?x=${(.now?time)?html}"><@global.text key='text.paymentsystems'/></a>
        &gt;
        <a href="../paymentsystems/paymentsystemView.html?id=${(paymentSystem.getId()!"")?html}"><@global.text key='text.details'/>: ${(paymentSystem.name!"")?html}</a>
        &gt;
        <span class="selected">
            <@global.text key='text.settings'/>
        </span>
    </h1>
    <h2><@global.text key='text.paymentsystemssettings'/></h2>
    <#assign userSettingsTitle> <@global.text key='text.usersetting'/> </#assign>
    <@settingsTable.table title="${userSettingsTitle}" paymentSystemSettings=userPaymentSystemSettings viewRole="paymentsystemView" editRole="paymentsystemEdit" />
    <#assign adminSettingsTitle> <@global.text key='text.adminsettings'/> </#assign>
    <@settingsTable.table title="${adminSettingsTitle}" paymentSystemSettings=adminPaymentSystemSettings viewRole="adminSetup" editRole="adminSetup" />
    <#if eloPAPIPaymentSystemSettings??>
        <#assign eloPAPISettingsTitle> <@global.text key='text.elopapisetting'/> </#assign>
        <@settingsTable.table title="${eloPAPISettingsTitle}" paymentSystemSettings=eloPAPIPaymentSystemSettings viewRole="paymentsystemView" editRole="paymentsystemEdit" />
    </#if>

    <script type="text/javascript" src="../js/paymentSystemSettings.js"></script>
</@global.page>