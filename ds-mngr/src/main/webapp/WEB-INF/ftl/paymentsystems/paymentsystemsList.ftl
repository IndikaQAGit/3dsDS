<#-- @ftlvariable name="paymentsystemsList" type="java.util.List<com.modirum.ds.db.model.PaymentSystem>" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html">
            <@global.text key='text.mainpage'/>
        </a> &gt; <span class="selected"><@global.text key='text.paymentsystems'/></span>
    </h1>

    <#if hasAccessToAllPaymentSystems>
        <@global.security.authorize access="hasRole('adminSetup')">
            <a href="../paymentsystems/paymentsystemEdit.html?x=${(.now?time)?html}"><@global.text key='text.addNewPaymentSystem'/></a>
        </@global.security.authorize>
    </#if>

    <h2><@global.text key='text.paymentsystems'/></h2>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="20%" class="tdHeader">Logo</td>
            <td width="20%" class="tdHeader"><@global.text key='text.name'/></td>
            <td width="20%" class="tdHeader"><@global.text key='text.paymentsystem.type'/></td>
            <td width="20%" class="tdHeader"></td>
            <td width="20%" class="tdHeader"></td>
        </tr>
        <#list paymentsystemsList as paymentSystem>
            <tr <#if paymentSystem_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                <td width="20%">
                    <a href="paymentsystemView.html?id=${(paymentSystem.id!"")?html}">Logo</a>
                </td>
                <td width="20%">
                    <a href="paymentsystemView.html?id=${(paymentSystem.id!"")?html}">${(paymentSystem.name!"")?html}</a>
                </td>
                <td width="20%">${(paymentSystem.type!"")?html}</td>
                <td width="20%"></td>
                <td width="20%"></td>
            </tr>
        </#list>
    </table>
</@global.page>
