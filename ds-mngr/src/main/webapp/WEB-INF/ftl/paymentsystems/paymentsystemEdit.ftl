<#import "../common/global.ftl" as global/>

<@global.page>
    <#-- page headers -->
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a>
        &gt;
        <a href="paymentsystemsList.html?x=${(.now?time)?html}"><@global.text key='text.paymentsystems'/></a>
        &gt;
        <#if paymentSystem?? && paymentSystem.id??>
            <a href="paymentsystemView.html?id=${(paymentSystem.getId()!"")?html}">
            <@global.text key='text.details'/>: ${(paymentSystem.name!"")?html}</a>
            &gt;
        </#if>
        <span class="selected">
            <#if paymentSystem?? && paymentSystem.id??>
                <@global.text key='text.edit'/>
            <#else>
                <@global.text key='text.add.new'/>
            </#if>
        </span>
    </h1>
    <h2>
        <@global.text key='general.paymentSystem'/>
    </h2>

    <#-- payment system details -->
    <#if paymentSystem??>
        <form id="uForm" action="paymentsystemUpdate.html" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <@global.spring.formHiddenInput path='paymentSystem.id'/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="35" class="tdHeaderVertical"><@global.text key='text.paymentsystem.name'/>:*</td>
                    <td width="505">
                        <@global.spring.formInput path="paymentSystem.name" attributes='size="35"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                </tr>
                <tr>
                    <td width="35" class="tdHeaderVertical"><@global.text key='text.paymentsystem.type'/>:</td>
                    <td width="505">
                        <@global.spring.formInput path="paymentSystem.type" attributes='size="35"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                </tr>
                <tr>
                    <td width="35" class="tdHeaderVertical"><@global.text key='text.paymentsystem.port'/>:*</td>
                    <td width="505">
                        <input type="number" id="port" name="port" min="0" style="width: 70px" value="${(paymentSystem.port!"")?html}"/>
                        <@global.spring.bind path="paymentSystem.port"/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" name="save" value="<@global.text key='button.save'/>"/>
                        <#if paymentSystem.id??>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'paymentsystemView.html?id=${paymentSystem.id}'; return false;"/>
                        <#else>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'paymentsystemsList.html'; return false;"/>
                        </#if>
                    </td>
                </tr>
            </table>
        </form>
    </#if>
</@global.page>