<#-- @ftlvariable name="user" type="com.modirum.ds.db.model.User" -->
<#--<#import "/WEB-INF/ftl/common/spring.ftl" as spring/>-->
<#-- @ftlvariable name="processorsMap" type="java.util.Map<String, String>" -->
<#import "../common/global.ftl" as global/>
<@global.page>
<h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a href="usersList.html"><@global.text key='text.users'/></a> &gt; 
		<#if user?exists><span class="selected">Details: ${(user.getLoginname()!"")?html}</span></#if></h1>

<h2><@global.text key='text.user.details'/></h2>
<#if user?exists>
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td colspan="2">
	<@global.security.authorize access="hasAnyRole('userEdit')">
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<form action="userEdit.html" method="post">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<input type="hidden" name="id" value="${(user.getId()!"")?html}"/>
			<input type="submit" name="button" value="Edit"/>
			</form>
			</td>
			<td>
			<form action="userDelete.html" method="post">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<input type="hidden" name="id" value="${(user.getId()!"")?html}"/>
			<input type="submit" name="button" 
				onclick="if (confirm('Are you sure you want to delete this user?')) { return true; } return false;"
				value="Delete"/>
			</form>
			</td>
			<td>
				<#switch user.getStatus()!"">
				<#case "A">
					<form action="userDeactivate.html" method="post">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<input type="hidden" name="id" value="${(user.getId()!"")?html}"/>
					<input type="submit" name="button" value="Deactivate"/>
					</form>
					<#break/>
					<#case "B">
					<form action="userActivate.html" method="post">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<input type="hidden" name="id" value="${(user.getId()!"")?html}"/>
					<input type="submit" name="button" value="Activate"/>
					</form>
					<#break/>
				</#switch>
			</td>
			</tr>
			</table>
		</@global.security.authorize>
		</td>
	</tr>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.loginname'/>:</td>
		<td>${(user.loginname!"")?html}</td>
	</tr>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.firstName'/>:</td>
		<td>${(user.firstName!"")?html}</td>
	</tr>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.lastName'/>:</td>
		<td>${(user.lastName!"")?html}</td>
	</tr>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.email'/>:</td>
		<td>${(user.email!'')?html}</td>
	</tr>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.phone'/>:</td>
		<td>${(user.phone!'')?html}</td>
	</tr>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.status'/>:</td>
		<td>
			<@global.displayStatus user.getStatus()!""/>
		</td>
	</tr>
    <tr>
        <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
        <td>
            <#if user.paymentSystemId?exists>
                ${(user.paymentSystemName!'')?html}
            <#else>
                <@global.text key='general.unlimited'/>
            </#if>
        </td>
    </tr>
	 <tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.locale'/>:</td>
		<td>
   		<#if user?exists && user.locale?exists>
   			${(supportedLocales[user.locale]!"")?html}
		 </#if>
		</td>
	</tr>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.roles'/>:</td>
		<td>
			<#if user.roles?exists>
				<#list allroles?keys as role>
					<#if user.roles?seq_contains(role)>
						<span class="roleLabel" title="<@global.text key='text.tooltip.user.roles.${role}'/>">${(allroles[role]!"")?html} </span><br/>
					</#if>
				</#list>
			</#if>
		</td>
	</tr>
	<#if factor2Authentications?exists>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.factor2Authmethod'/>:</td>
		<td>
		<#if user?exists && user.factor2Authmethod?exists>
			${(factor2Authentications[user.factor2Authmethod]!"")?html}
		 </#if>
		</td>
	</tr>
	</#if>
	<#if factor2Authentications??>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.factor2AuthDeviceId'/>:</td>
		<td>
		<#if user?exists && user.factor2AuthDeviceId?exists>
			${(user.factor2AuthDeviceId!"")?html}
		 </#if>
		</td>
	</tr>
	</#if>
	<#if certificateAuthentications??>
	<tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.factor2CertCN'/>:</td>
		<td>
		<#if user?exists && user.factor2Data1??>
			${(user.factor2Data1!"")?html}
		 </#if>
		</td>
	</tr>
	</#if>
	<#if factor2Authentications?? && user.factor2Data2??>
	 <tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.factor2MDIDUserId'/>:</td>
		<td>${(user.factor2Data2!"")?html}
		</td>
	</tr>
	</#if>
		
 	 <tr>
		<td class="tdHeaderVertical"><@global.text key='text.user.modified'/>:</td>
		<td>
		<#if user?? && lastModified?? && lastModified.when??>
			${lastModified.when?string(dateFormat+" HH:mm:ss")}
			&nbsp; ${(lastModified.by!"")?html} ${(lastModified.byId!"")?html}
		 </#if>
		</td>
	</tr>
</table>
<#else>
<span style=" color: red;"><@global.text key='text.object.not.found'/></span>
</#if>
</@global.page>