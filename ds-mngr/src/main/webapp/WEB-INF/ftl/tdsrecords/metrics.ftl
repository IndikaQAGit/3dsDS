<#import "../common/global.ftl" as global/>
<#include "armacro.ftl"/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <span class="selected"
        ><@global.text key='text.metrics'/></span></h1>

    <@global.security.authorize access="hasRole('recordsView')">
        <#if xtimings??>
            <h2><@global.text key='text.metrics'/></h2>
            <br/><br/>
            <@global.text key='text.metrics.areq'/><br/>
            <canvas id="aReqTimingsCanvas" width="688" height="215" style="border:1px solid #000000;"></canvas>
            <br/><br/>
            <@global.text key='text.metrics.rreq'/><br/>
            <canvas id="rReqTimingsCanvas" width="688" height="215" style="border:1px solid #000000;"></canvas>
            <br/><br/>
            <@global.text key='text.metrics.preq'/><br/>
            <canvas id="pReqTimingsCanvas" width="688" height="215" style="border:1px solid #000000;"></canvas>

            <script type="text/javascript">

                var left = 17;
                var ymax = 200;
                var xmax = 600;
                var start =${start};
                var json90AReq = new Array();
                var json90PReq = new Array();
                var json90RReq = new Array();
                var dsinstances = new Array();
                var ctx1 = drawGraph("aReqTimings", start);
                var ctx2 = drawGraph("rReqTimings", start);
                var ctx3 = drawGraph("pReqTimings", start);

                function drawGraph(msgname, start) {
                    var c = document.getElementById(msgname + "Canvas");
                    if (c == null) {
                        return null;
                    }

                    var ctx = c.getContext("2d");

                    ctx.moveTo(left, ymax + 1);
                    ctx.lineTo(left + xmax, ymax + 1);
                    ctx.lineWidth = 1;
                    ctx.strokeStyle = '#0000cc';
                    ctx.stroke();

                    ctx.moveTo(left - 1, 0);
                    ctx.lineTo(left - 1, ymax + 1);
                    ctx.lineWidth = 1;
                    ctx.strokeStyle = '#0000cc';
                    ctx.stroke();

                    var i = 0;
                    for (i = 0; i <= xmax; i = i + 50) {
                        ctx.moveTo(i + left - 1, ymax + 1);
                        ctx.lineTo(i + left - 1, ymax + 3);
                        ctx.stroke();
                    }

                    ctx.font = "9px Courier New";
                    ctx.fillStyle = '#0000cc';

                    for (i = 40; i <= ymax; i = i + 40) {
                        ctx.beginPath();
                        ctx.moveTo(left - 1, i);
                        ctx.lineTo(left - 3, i);
                        ctx.stroke();

                        ctx.fillStyle = '#0000cc';
                        ctx.textAlign = "right";
                        ctx.font = "10px Courier New";
                        var secy = (ymax - i) / 20;
                        ctx.fillText(secy, left - 5, i + 3);
                    }
                    drawHLT(ctx);

                    left + xmax + 3
                    ctx.fillStyle = '#101010';
                    ctx.textAlign = "left";
                    ctx.font = "9.5px Arial";
                    ctx.fillText("n - msg per min", left + xmax + 3, 10);

                    ctx.beginPath();
                    setGraphLine(ctx)
                    ctx.moveTo(left + xmax + 5, 20);
                    ctx.lineTo(left + xmax + 45, 20);
                    ctx.stroke();

                    ctx.fillText("Timing bars", left + xmax + 8, 133);
                    ctx.fillText("seconds", left + xmax + 8, 143);
                    ctx.beginPath();
                    setBarReq(ctx)
                    ctx.moveTo(left + xmax + 5, 150);
                    ctx.lineTo(left + xmax + 5, 165);
                    ctx.stroke();
                    ctx.fillText("- request", left + xmax + 8, 160);

                    ctx.beginPath();
                    setBarWait(ctx)
                    ctx.moveTo(left + xmax + 5, 166);
                    ctx.lineTo(left + xmax + 5, 180);
                    ctx.stroke();
                    ctx.fillText("- wait", left + xmax + 8, 175);

                    ctx.beginPath();
                    setBarRes(ctx)
                    ctx.moveTo(left + xmax + 5, 181);
                    ctx.lineTo(left + xmax + 5, 195);
                    ctx.stroke();
                    ctx.fillText("- response", left + xmax + 8, 190);

                    return ctx;
                }

                function setGraphLine(ctx) {
                    ctx.lineWidth = "1";
                    ctx.strokeStyle = '#6c006c';
                }

                function setBarReq(ctx) {
                    ctx.lineWidth = "1";
                    ctx.strokeStyle = '#ff1000';
                }

                function setBarRes(ctx) {
                    ctx.lineWidth = "1";
                    ctx.strokeStyle = '#0c990c';
                }

                function setBarWait(ctx) {
                    ctx.lineWidth = "1";
                    ctx.strokeStyle = '#202040';
                }

                function drawHLT(ctx) {
                    ctx.font = "9px Courier New";
                    ctx.fillStyle = '#0000cc';
                    var xx = 50;
                    for (xx = 50; xx < xmax; xx = xx + 50) {
                        var d1 = formatTime(new Date(start + xx * 2 * 1000));
                        ctx.textAlign = "center";
                        ctx.fillText(d1, left + xx, ymax + 12);
                    }

                    ctx.lineWidth = 1;
                    for (i = 40; i < ymax; i = i + 40) {
                        var secy = (ymax - i) / 20;
                        ctx.beginPath();
                        if (secy >= 8) {
                            ctx.strokeStyle = '#ff5a5a';
                        } else if (secy >= 6) {
                            ctx.strokeStyle = '#fcee33';
                        } else {
                            ctx.strokeStyle = '#adfdad';
                        }
                        ctx.moveTo(left, i);
                        ctx.lineTo(left + xmax, i);
                        ctx.stroke();
                    }
                }

                function formatTime(date) {
                    var h = date.getHours();
                    if (h < 10) {
                        h = "0" + h;
                    }
                    var m = date.getMinutes();
                    if (m < 10) {
                        m = "0" + m;
                    }
                    var s = date.getSeconds();
                    if (s < 10) {
                        s = "0" + s;
                    }

                    return h + ":" + m + ":" + s;
                }

                function drawData(start, ctx, dataarray) {
                    if (dataarray == null || dataarray.length < 7) {
                        //alert("No data "+(dataarray!=null ?  ""+dataarray.length : "null"));
                        return timelinex;
                    }
                    //alert("Array "+dataarray.length);

                    var cnt = 0;
                    var reqMid = 0;
                    var reqEnd = 0;
                    var resStart = 0;
                    var resMid = 0;
                    var resEnd = 0;
                    var timeline = 0;
                    var timelinex = 0;
                    var timepos = 0;
                    var tcnt = 0;
                    var i = 0;

                    var timeposCount = 0;
                    var msgCount = 0;
                    var prevMsgCount = 0;
                    var prevPrevMsgCount = 0;
                    var prevPrevPrevMsgCount = 0;
                    var prevMsgCountTln = left;
                    var msminp = 0;
                    var msminpp = 0;
                    var txtcount = 1;

                    for (i = 0; i + 5 < dataarray.length; i = i + 7) {
                        timeline = dataarray[i];

                        // draw server reference position
                        timelinex = left + Math.round((timeline - start) / 2000);
                        if (timelinex > left && i + 8 > dataarray.length) {
                            ctx.beginPath();
                            ctx.lineWidth = "1";
                            ctx.strokeStyle = '#aaaaaa';
                            ctx.moveTo(timelinex, 0);
                            ctx.lineTo(timelinex, ymax);
                            ctx.stroke();
                        }

                        cnt += dataarray[i + 1];
                        msgCount += dataarray[i + 1];

                        reqMid = dataarray[i + 2];
                        reqEnd = dataarray[i + 3];
                        resStart = dataarray[i + 4];
                        resMid = dataarray[i + 5];
                        resEnd = dataarray[i + 6];
                        //alert("Draw "+timeline+"/"+timepos);

                        // 2 seconds per pixel window
                        if (timeline > timepos + 2000) {
                            reqMid = bounds(reqMid / cnt / 50);
                            reqEnd = bounds(reqEnd / cnt / 50);
                            resStart = bounds(resStart / cnt / 50);
                            resMid = bounds(resMid / cnt / 50);
                            resEnd = bounds(resEnd / cnt / 50);

                            //alert("Draw "+timelinex);
                            if (timelinex > left) {
                                ctx.beginPath();
                                setBarReq(ctx);
                                ctx.moveTo(timelinex, ymax - resEnd);
                                ctx.lineTo(timelinex, ymax - resEnd + reqEnd); //Mid);
                                ctx.stroke();
                                /*
                                ctx.beginPath();
                                ctx.lineWidth="1";
                                ctx.strokeStyle = '#00f100';
                                ctx.moveTo(timelinex, ymax-resEnd+reqMid+1);
                                ctx.lineTo(timelinex, ymax-resEnd+reqEnd);
                                ctx.stroke();
                                */

                                ctx.beginPath();
                                setBarWait(ctx);
                                ctx.moveTo(timelinex, ymax - resEnd + reqEnd + 1);
                                ctx.lineTo(timelinex, ymax - resEnd + resStart);
                                ctx.stroke();

                                ctx.beginPath();
                                setBarRes(ctx);
                                ctx.moveTo(timelinex, ymax - resEnd + resStart + 1);
                                ctx.lineTo(timelinex, ymax - resEnd + resEnd); //Mid);
                                ctx.stroke();
                            }

                            cnt = 0;
                            tcnt = 0;
                            reqMid = 0;
                            reqEnd = 0;
                            resStart = 0;
                            resMid = 0;
                            resEnd = 0;
                            timepos = timeline;
                        }

                        if (timeline >= timeposCount + 20000) {
                            var msmin = Math.round(msgCount + prevMsgCount + prevPrevMsgCount);
                            var h = hscale(msmin);
                            var hp = hscale(msminp);
                            if (timelinex > left) {
                                ctx.beginPath();
                                setGraphLine(ctx);
                                ctx.moveTo(prevMsgCountTln, ymax - hp);
                                ctx.lineTo(timelinex, ymax - h);
                                ctx.stroke();

                                if (msminp > 0 && msminp != msminpp && prevMsgCountTln > left) {
                                    var offs = 0;

                                    if (msminpp < msminp) {
                                        offs = -3;
                                    } else if (msminp > msmin) {
                                        offs = 3;
                                    }

                                    ctx.fillStyle = '#000000';
                                    ctx.textAlign = "center";
                                    ctx.font = "9px Arial";

                                    var offsy = 0;
                                    if (msminpp > 99) {
                                        txtcount++;
                                    }
                                    if (txtcount % 2 == 0 && msminpp > 99) {

                                    } else {
                                        ctx.fillText(msminp, prevMsgCountTln + offs, ymax - hp - 3 + offsy);
                                    }
                                }
                            }

                            msminpp = msminp;
                            msminp = msmin;
                            prevPrevPrevMsgCount = prevPrevMsgCount;
                            prevPrevMsgCount = prevMsgCount;
                            prevMsgCount = msgCount;
                            msgCount = 0;
                            prevMsgCountTln = timelinex;
                            timeposCount = timeline;
                        }
                    }

                    return timelinex;
                }

                function hscale(y) {
                    if (y > 0) {
                        y = 7 * Math.pow((y + 1), 0.63);
                    }

                    if (y > ymax - 10) {
                        return ymax - 11;
                    }

                    return Math.round(y);
                }

                function bounds(y) {
                    if (y > ymax) {
                        return ymax;
                    }
                    return Math.round(y);
                }

                function loadDoc(url, postcontent, ct) {

                    var xmlhttp = null;
                    var respcontent = null;
                    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else {// code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }

                    if (postcontent != null) {
                        xmlhttp.setRequestHeader("Content-type", ct != null ? ct : "application/xml");
                        xmlhttp.open("POST", url, false);
                        xmlHttp.send(postcontent);
                    } else {
                        xmlhttp.open("GET", url, false);
                        xmlhttp.send("");
                    }

                    if (xmlhttp.status != 200) {
                        return null;
                    }

                    return xmlhttp.responseText;
                }

                function renew() {
                    var jsonData = loadDoc("metricsLastData.html", null, null)
                    if (jsonData == null) {
                        alert("No data, session expired");
                    }

                    var jsonObj = JSON.parse(jsonData);
                    //json90.push(jsonObj[dsinstances[0]]);
                    var jsonData = jsonObj[dsinstances[0]];
                    json90AReq = json90AReq.concat(jsonData['aReqTimings']);
                    json90RReq = json90RReq.concat(jsonData['rReqTimings']);
                    json90PReq = json90PReq.concat(jsonData['pReqTimings']);

                    var maxseries = 165;
                    while (json90AReq > maxseries * 7) {
                        json90AReq.shift();
                        json90AReq.shift();
                        json90AReq.shift();
                        json90AReq.shift();
                        json90AReq.shift();
                        json90AReq.shift();
                    }
                    while (json90RReq > maxseries * 7) {
                        json90RReq.shift();
                        json90RReq.shift();
                        json90RReq.shift();
                        json90RReq.shift();
                        json90RReq.shift();
                        json90RReq.shift();
                    }
                    while (json90PReq > maxseries * 7) {
                        json90PReq.shift();
                        json90PReq.shift();
                        json90PReq.shift();
                        json90PReq.shift();
                        json90PReq.shift();
                        json90PReq.shift();
                    }

                    if (json90AReq.length > 7) {
                        start = json90AReq[json90AReq.length - 7] - 19 * 60000;
                    } else {
                        start = start + 1000;

                    }

                    drawAllData();
                    var timer = setTimeout("renew();", 10000);
                }

                function drawAllData() {
                    var xi = 0;
                    // graph data
                    ctx1.clearRect(left + 1, 0, xmax, ymax);
                    ctx2.clearRect(left + 1, 0, xmax, ymax);
                    ctx3.clearRect(left + 1, 0, xmax, ymax);

                    // timeline
                    ctx1.clearRect(left + 1, ymax + 3, xmax, ymax + 12);
                    ctx2.clearRect(left + 1, ymax + 3, xmax, ymax + 12);
                    ctx3.clearRect(left + 1, ymax + 3, xmax, ymax + 12);

                    drawHLT(ctx1);
                    drawHLT(ctx2);
                    drawHLT(ctx3);

                    drawData(start, ctx1, json90AReq);
                    drawData(start, ctx2, json90RReq);
                    drawData(start, ctx3, json90PReq);
                }

                // intial data set
                <#list xtimings?keys as xkey>
                dsinstances.push("${xkey?js_string}");

                <#assign instanceList = xtimings[xkey] >
                if (ctx1 != null) {
                    var intitialdata = null;
                    var jsonData = null;
                    <#list instanceList as jsondata>
                    intitialdata = "${jsondata?js_string}";
                    jsonData = JSON.parse(intitialdata);
                    json90AReq = json90AReq.concat(jsonData['aReqTimings']);
                    json90RReq = json90RReq.concat(jsonData['rReqTimings']);
                    json90PReq = json90PReq.concat(jsonData['pReqTimings']);
                    </#list>
                }
                </#list>

                if (json90AReq.length > 7) {
                    start = json90AReq[json90AReq.length - 7] - 19 * 60000;
                }
                drawAllData();
                var timer = setTimeout("renew();", 10000);

            </script>
        <#else>
            <@global.text key='text.object.not.found'/>
        </#if>
    </@global.security.authorize>
</@global.page>
