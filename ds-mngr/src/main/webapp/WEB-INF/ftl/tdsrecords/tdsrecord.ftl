<#-- @ftlvariable name="authrecord" type="com.modirum.ds.db.model.TDSRecord" -->
<#import "../common/global.ftl" as global/>
<#include "armacro.ftl"/>
<@global.page>
<script type="text/javascript">

	var timerget=null;
	var getcount=0;
	var loaded=false;

	function fload()
	{
		loaded=true;
	}

	function showPan()
	{
		loaded=false;
		getcount=0;
		submitFormWithCmd(null, 'viewpan', null);
		timerget=setTimeout("getPan();",200);
	}

	function objToTable(obj)
	{
		var table="<table>";
		for (var key in obj)
		{
			if (obj.hasOwnProperty(key))
			{
			var xval="";
			var xo=obj[key];
			if (xo!=null && xo instanceof Array)
			{
				var xi=0;
				for(xi=0;xi<xo.length; xi++)
				{
					if (xval.length>0)
					{
						xval=xval+", ";
					}

					if (key=='x' || key=='y' || key=='x5c')
					{
						xval=xval+mask(xo[xi]);
					}
					else
					{
						xval=xval+xo[xi];
					}
				}
			}
			else if (xo!=null && xo instanceof Object)
			{
				xval=objToTable(xo);
			}
			else
			{
				xval=xo;
							if (key=='x' || key=='y' || key=='x5c')
			{
				xval=mask(xval);
			}

			}

			table=table+'<tr><td width="20%" align="left" style="vertical-align:top;"><b>' + key + '</b>:</td> ' + '<td align="left">' + xval + '</td></tr>';
			} // if
		}// for

		table=table+"</table>";
		return table;
	}

	function getPan() {
		var frame = document.getElementById("panframe");
		var msg="";
		if (frame!=null)
		{
		getcount++;
		clearTimeout(timerget);
		var content=frame.contentDocument;

		if (content!=null && content.readyState == 'complete' && loaded || getcount>44 && content.documentElement!=null)
		{
			var i=0;
			var contentStr=content.documentElement.innerHTML;
			if (content.body)
			{
				contentStr=content.body.innerHTML;
			}
			//alert("contentStr "+new String(contentStr));

			if (content!=null && (i=contentStr.indexOf("VALUE"))>-1)
			{
				msg=contentStr.substring(i+5);
				var timer=setTimeout("terminatePan();",30000);
			}
			else if (content!=null && (i=contentStr.indexOf("ERROR"))>-1)
			{
				msg=contentStr.substring(i+5);
			}
			else if (contentStr.length>0)
			{
				msg="Invalid response";
			}
			else
			{
				msg="No response";
			}

			getcount=0;
		}
		else if (getcount<45)
		{
			var symb="|/-\\|".charAt(getcount%4);
			msg="Loading.."+symb;
			timerget=setTimeout("getPan();",330);
		}
		else
		{
			msg="No content";
			getcount=0;
		}
		var boxEl=document.getElementById('panboxpan');
		boxEl.innerHTML=msg;
		showPopupNat("panbox");
		}
	}

	function base64x(valueb64)
	{
		if (valueb64!=null)
		{	// support possible base64 url format
			valueb64=valueb64.replace("-", "+");
			valueb64=valueb64.replace("_", "/");
			valueb64=atob(valueb64);
		}

		return valueb64;
	}

	function terminatePan()
	{
		loaded=false;
		getcount=0;
		unDisplay('panbox');
		var frame = document.getElementById("panframe");
		if (frame!=null)
		{
			frame.contentWindow.document.open();
			frame.contentWindow.document.write("");
			frame.contentWindow.document.close();
		}
		var boxEl=document.getElementById('panboxpan');
		if (boxEl!=null)
		{
			boxEl.innerHTML="";
		}
		frame.contentDocument=null;
	}

</script>
<h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <span class="selected"
><@global.text key='text.authrecords'/>: <#if authrecord??>${(authrecord.id!"")?html}</#if></span></h1>

<@global.security.authorize access="hasRole('recordsView')">
<#if authrecord?exists>
<h2><@global.text key='text.authrecord'/></h2>
<iframe onload="fload();" src="" id="panframe" name="panframe" scrolling="no" frameborder="0"  style="position:absolute; top:0px; left:0px; display:none;"></iframe>
<form id="viewpan" name="viewpan" action="viewpan.html" method="post" target="panframe">
<input type="hidden" name="id" value="${(authrecord.id!"")?html}"/>
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td width="136" class="tdHeader"><@global.text key='text.ar.id'/></td>
	<td>${(authrecord.id!"")?html}</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.datetime'/></td>
	<td><#if authrecord.localDateStart??>
		${authrecord.localDateStart?string(dateFormat+" HH:mm:ssZ")}
		</#if>
	</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.completed'/></td>
	<td><#if authrecord.localDateEnd??>${authrecord.localDateEnd?string(dateFormat+" HH:mm:ssZ")}</#if></td>
</tr>
<tr>

	<td class="tdHeader"><@global.text key='text.ar.maskedpan'/></td>
	<td>${(authrecord.acctNumber!"")?html}
	<#if authrecord.acctNumber?? && authrecord.acctNumber?length &gt; 0 && viewpan?? && viewpan>
	&nbsp;<input type="button" name="showPanBtn" value="<@global.text key='text.ar.viewpan'/>" onclick="showPan();"/>
	<div id="panbox" style="height: 45px; width: 160px; display: none;" class="natpopup">
	<div style="text-align: right;"><a href="javascript:terminatePan();">close</a></div>
	<div id="panboxpan" style=" padding: 3px; text-align: center;"></div>
	</div>
	</#if>
	</td>
	</form>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.protocol'/></td>
	<td>${(authrecord.protocol!"")?html}</td>
</tr>

<tr>
	<td class="tdHeader"><@global.text key='text.ar.requestorid'/></td>
	<td>${(authrecord.requestorID!"")?html}</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.requestor.name'/></td>
	<td>${(authrecord.requestorName!"")?html}</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.merchantid'/></td>
	<td>${(authrecord.acquirerMerchantID!"")?html} / MCC ${(authrecord.MCC!"")?html}</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.merchantName'/></td>
	<td>${(authrecord.merchantName!"")?html}</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.merchantCountry'/></td>
	<td><#if authrecord.merchantCountry??>${(merchantCountries[authrecord.merchantCountry?string]!"")?html} (${authrecord.merchantCountry?html})</#if></td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.ar.issuer'/></td>
	<td><#if authrecord.localIssuerId??>${(issuersMap[authrecord.localIssuerId?string]!"")?html}</#if></td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.issuerbin'/></td>
	<td> ${(authrecord.issuerBIN!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.ar.acquirer'/></td>
	<td><#if authrecord.localAcquirerId??>${(acquirersMap[authrecord.localAcquirerId?string]!"")?html}</#if></td>
</tr>

<tr>
	<td  class="tdHeader"><@global.text key='text.acquirerbin'/></td>
	<td>${(authrecord.acquirerBIN!"")?html}</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.total'/></td>
	<td><@formatAmount authrecord.purchaseAmount!-1 authrecord.purchaseExponent!2 authrecord.purchaseCurrency!0/></td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.ar.status'/></td>
	<td><@arStatus authrecord.localStatus!""/></td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.authstatus'/></td>
	<td><@arAuthStatus authrecord.transStatus!""/></td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.statusreason'/></td>
	<td>${(authrecord.transStatusReason!"")?html}</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.authtype'/></td>
	<td>${(authrecord.authenticationType!"")?html}</td>
</tr>
<tr>
	<td class="tdHeader"><@global.text key='text.ar.resultsstatus'/></td>
	<td>${(authrecord.resultsStatus!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.ar.eci'/></td>
	<td>${(authrecord.ECI!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.sdktransid'/></td>
	<td>${(authrecord.SDKTransID!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.sdkappid'/></td>
	<td>${(authrecord.SDKAppID!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.tdsservertransid'/></td>
	<td>${(authrecord.tdsTransID!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.tdsserverref'/></td>
	<td>${(authrecord.MIReferenceNumber!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.dstransid'/></td>
	<td>${(authrecord.DSTransID!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.acstransid'/></td>
	<td>${(authrecord.ACSTransID!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.acsurl'/></td>
	<td>${(authrecord.ACSURL!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.acsopertorid'/></td>
	<td>${(authrecord.ACSOperatorID!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.acsrefno'/></td>
	<td>${(authrecord.ACSRefNo!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='text.browserip'/></td>
	<td>${(authrecord.browserIP!"")?html}</td>
</tr>
<tr>
	<td  class="tdHeader"><@global.text key='general.paymentSystem'/></td>
	<td>${(paymentSystemName!"")?html}</td>
</tr>

<#if authrecord.errorCode?? || authrecord.errorDetail??>
<tr>
	<td  class="tdHeader"><@global.text key='text.ar.errorDetail'/></td>
	<td><span class="error">${(authrecord.errorCode!"")?html} ${(authrecord.errorDetail!"")?html}</span></td>
</tr>
</#if>

<#if authrecord.errorDescription??>
    <tr>
        <td  class="tdHeader"><@global.text key='text.ar.errorDescription'/></td>
        <td><span class="error">${(authrecord.errorDescription!"")?html}</span></td>
    </tr>
</#if>

<#if authrecord.errorResolution??>
    <tr>
        <td  class="tdHeader"><@global.text key='text.ar.errorResolution'/></td>
        <td><span>${(authrecord.errorResolution!"")?html}</span></td>
    </tr>
</#if>
<#if authrecord.acquirerOptions??>
<tr>
	<td  class="tdHeader"><@global.text key='text.ar.acquirer'/> <@global.text key='text.ext.options'/></td>
	<td>${(authrecord.acquirerOptions!"")?html}</td>
</tr>
</#if>
<#if authrecord.issuerOptions??>
<tr>
	<td  class="tdHeader"><@global.text key='text.ar.issuer'/> <@global.text key='text.ext.options'/></td>
	<td>${(authrecord.issuerOptions!"")?html}</td>
</tr>
</#if>


<script type="text/javascript">

	var devInfoCount = 0;
	var devInfoTimeout = null;

	function getDeviceInfo() {
		devInfoCount++;
		clearTimeout(devInfoTimeout);
		var frame = document.getElementById('devInfoFrame');
		if (frame != null) {
			var frameDoc = frame.contentDocument;
			var msg = '';
			var deviceInfos;
			var obj;
			var targetSpan = document.getElementById('targetSpan');
			if ((frameDoc != null && frameDoc.readyState == 'complete' && frameDoc.documentElement != null && frameDoc.body.innerHTML != "") || devInfoCount > 40)
			{
				try {
					try {
						var dec = base64x(frameDoc.body.innerHTML);
					} catch (e)
					{
						console.log("Extract error: "+e);
						if (frameDoc.body.innerHTML.length>2000)
							msg = "Session has expired";
						else
							msg = "<b style='color:red'><@global.text key='err.failed.to.decode.data'/> :</b>" + "<br>" + frameDoc.body.innerHTML;
						throw e;
					}
					try {
						obj = JSON.parse(dec);
					} catch (e)
					{
						console.log("JSON parse error: "+e);
						msg = "<b style='color:red'><@global.text key='err.failed.to.parse.data'/></b>" + " Decoded value:<br>" + dec;
						throw  e;
					}

					frame.contentDocument.innerHTML = '';
					targetSpan.innerHTML = '';
					msg="";
					if (obj!=null)
					{
						msg=objToTable(obj);
					}

					if (msg.length<20)
					{
						msg = "<b style='color:red'>No DeviceData decodable. JSON object:</b><br>" + JSON.stringify(obj);
					}

				} catch (e) {
					console.log("Extract error: "+e);
					//unknown error
					if (msg == '' || msg.startsWith('<table>')) msg = '<b style="color:red"><@global.text key='err.failed.to.decode.data'/></b>';
					console.error(e, e.stack);
				}
				targetSpan.innerHTML = msg;
				frame.contentDocument.body.innerHTML='';
				devInfoCount = 0;
			}
			else if (devInfoCount < 40) {
				var symb = "|/-\\|".charAt(devInfoCount % 4);
				targetSpan.innerHTML = "Loading.." + symb;
				setTimeout("getDeviceInfo();", 330);
			} else if (devInfoCount >= 40) {
				targetSpan.innerHTML = 'No response';
			}
		}
	}

	function showDeviceInfo(){
		submitFormWithCmd(null, 'viewDevInfo', null);
		devInfoTimeout =setTimeout('getDeviceInfo();',200);
		document.getElementById('viewDevInfo').style.display='block';
		showPopupNat('devInfoBox');
		document.getElementById('devInfoBtn').disabled = true;
	}

	function hideDevInfo() {
		xhidePopup('devInfoBox');
		document.getElementById('viewDevInfo').style.display = 'none';
		document.getElementById('targetSpan').innerHTML = '';
		document.getElementById('devInfoBtn').disabled = false;

	}

</script>

</table>

<#if tdsrecordattributes??>
<h2><@global.text key='text.attr.recordattributes'/></h2>
<table style="width: 100%;">
	<tr style="border-bottom: 1px solid #ccc;;">
	<td  class="tdHeader"><@global.text key='text.ar.date'/></td><td  class="tdHeader"><@global.text key='text.attr.attributes'/></td><td  class="tdHeader"><@global.text key='text.attr.value'/></td>
	</tr>
		<#list tdsrecordattributes as tdsrecordattribute>
		<tr style="border-bottom: 1px solid #ccc;;">
			<td style="width: 30%;">${tdsrecordattribute.createdDate}</td>
			<td style="width: 30%;">${tdsrecordattribute.key}</td>
			<td style="width: 30%;">${tdsrecordattribute.value!} ${tdsrecordattribute.asciiValue!}</td>
		</tr>
		<#else>
		<tr><td>no record attributes</td></tr>
		</#list>
</table>
<#else>
</#if>

</form>
	<#if tdsmessages??>
	<h2><@global.text key='text.ar.messages'/></h2>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="stripe">
	<#list tdsmessages as mx>
	<tr>
	<td class="stripe" style="vertical-align: top;" class="tdHeader">
	<b>${(mx.messageType!"")?html}</b><br/>
	<#if mx.messageDate??>${mx.messageDate?string(dateFormat+" HH:mm:ss.SZ")}<br/></#if>
	<@global.text key='text.tdsmessage.sourceIP'/>: ${(mx.sourceIP!"")?html}<br/>
	<@global.text key='text.tdsmessage.destIP'/>: ${(mx.destIP!"")?html}<br/><br/>
	<#if mx.messageType?? && mx.messageType="ARes" && mx.contents?? && mx.contents?contains("acsSignedContent")>
			<script type="text/javascript">

				function mask(toMask){
					if (toMask.length>18)
					{
						return toMask.substr(0, 5) + "##....##" + toMask.substr(toMask.length - 5, 5);
					}
					return toMask;
				}

				function showAcsContentBox(messageIndex)
				{
					var contentHtml="";
					var payloadObject={};
					try
					{
						var fullContent = '${(mx.contents!"")?replace("\\n","")?replace("\\","")?js_string}';
						var sContentBeg = fullContent.indexOf('"acsSignedContent"');
						sContentBeg=fullContent.indexOf('"', sContentBeg+19)+1;
						var sContentEnd=fullContent.indexOf('"', sContentBeg+1);
						var sContent=fullContent.substring(sContentBeg, sContentEnd);
						//var payloadLength = acsContent.substr(payloadBeginning + 1).indexOf('.') + 1;
						//var rawPayloadJWS = acsContent.substr(payloadBeginning, payloadLength);
						var JWSCOmps = sContent.split(".");
						if (JWSCOmps!=null && JWSCOmps.length>1)
						{
							var i=0;
							//"_protected", "payload", "signatures
							for(i=0;i<JWSCOmps.length; i++)
							{
								if (i==0)
								{
									//alert("0="+ JWSCOmps[i]);
									var obj=JWSCOmps[i];
									try
									{
										obj=base64x(obj);
										obj=JSON.parse(obj);
									} catch (e1)
									{
										console.error(e1, e1.stack);
									}
									payloadObject["protected"]=obj;
								}
								if (i==1)
								{
									//alert("1="+JWSCOmps[i]);
									var obj=JWSCOmps[i];
									try
									{
										obj=base64x(obj);
										obj=JSON.parse(obj);
									} catch (e1)
									{
										console.error(e1, e1.stack);
									}
									payloadObject["payload"]=obj;
								}
								if (i==2)
								{
									payloadObject["signatures"]=JWSCOmps[i];
								}
							}
						}
						else
						{
							var decodedPayload = base64x(acsContent);
							//decodedPayload = decodedPayload.replace(new RegExp(String.fromCharCode(10), 'g'), '');// convert line feeds to spaces if present
							payloadObject = JSON.parse(decodedPayload);
						}
						contentHtml = objToTable(payloadObject);

					} catch (e)
					{
						console.log("Extract error: "+e);
						console.error(e, e.stack);
						contentHtml = "<@global.text key='err.failed.to.decode.data'/>";
					}
					document.getElementById('targetSpanAcs' + messageIndex).innerHTML = contentHtml;
					showPopupNat('acsContentBox' + messageIndex);
					document.getElementById('acsContentBtn'+ messageIndex).disabled = true;
				}
			</script>
		<input type="button" id="acsContentBtn${mx_index}" value="<@global.text key='text.ar.show.acscontent'/>" onclick="showAcsContentBox(${mx_index})"/>
		<div id="acsContentBox${mx_index}" class="natpopup" style="display:none; text-align:left; width: 650px;" >
		<div style="text-align: right;">
			<a href="javascript:document.getElementById('acsContentBtn'+${mx_index}).disabled = false;xhidePopup('acsContentBox'+${mx_index});"><@global.text key='text.ar.close'/></a>
			<div style="overflow:auto; text-overflow: clip; width: 650px;" id="targetSpanAcs${mx_index}"></div>
		</div>
		</div>
	</#if>  <#--show acsSignedContent -->

	<#assign hasDevInfo= mx.messageType?? && mx.messageType =="AReq" && mx.contents?contains("deviceInfo") && mx.destIP?starts_with("ACS") && mx.sourceIP?starts_with("DS") && viewDevInfo?? && viewDevInfo>
	<#if hasDevInfo>
		<input type="button" id="devInfoBtn" value="<@global.text key='text.ar.show.devinfo'/>" onclick="showDeviceInfo()"/>
		<form id="viewDevInfo" name="viewDevInfo" action="viewDeviceInfo.html" style="display:none; visibility: hidden" method="post" target="devInfoFrame">
		<div id="devInfoBox" class="natpopup" style="display:none; text-align:right; width: 450px;">
			<div style="text-align: right;"><a href="javascript:hideDevInfo();"><@global.text key='text.ar.close'/></a></div>
			<div style="overflow:auto; text-overflow: clip; text-align:right" id="targetSpan"></div>
			<input type="hidden" name="id" value="${(authrecord.id!"")?html}"/>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		</div>
		</form>
		<iframe id="devInfoFrame" name="devInfoFrame" style="display:none; visibility: hidden "></iframe>
		</#if>
	</td>
	<td class="stripe">
		<div style=" width: 790px; font-family: monospace; white-space: pre-wrap; word-wrap:break-word; overflow-wrap: break-word;overflow:auto;"
		>${(mx.contents!"")?html}</div>
	</td>
	</tr>
	</#list>
	</#if>
</table>
<br/>
<#else>
<@global.text key='text.object.not.found'/>
</#if>
</@global.security.authorize>
</@global.page>
