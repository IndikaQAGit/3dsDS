<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a
                href="certificateList.html?x=${(.now?time)?html}"><@global.text key='text.certificates'/></a> &gt;
        <@global.text key='text.text.upload.cert'/></h1>

    <#if msg?exists>
        <span class="msg">${msg?html}</span><br/>
    </#if>
    <#if duplicate??>
        <span class="warn"><@global.text key='text.wrn.certificate.sdn.exists'/></span><br/>
    </#if>
    <#if certificate??>
        <script type="text/javascript">

            function saveFile(filename, type, text) {
                var link = document.createElement("a");
                link.setAttribute("target", "_blank");
                if (Blob !== undefined) {
                    var blob = new Blob([text], {type: type});
                    link.setAttribute("href", URL.createObjectURL(blob));
                } else {
                    link.setAttribute("href", "data:text/plain," + encodeURIComponent(text));
                }
                link.setAttribute("download", filename);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        </script>
        <h2><@global.text key='text.text.cert.signed'/></h2>
        <form id="dummy" action="" method="post">
            <span style="width: 140px; display: inline-block;"><@global.text key='text.name'/>:</span>${(certificate.name!"")?html}
            <br/>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.expires'/>:</span><#if certificate.expires??>${certificate.expires?string(dateFormat+" HH:mm:ss")}</#if>
            <br/>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.subjectDN'/>:</span>${(certificate.subjectDN!"")?html}
            <br/>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.issuerDN'/>:</span>${(certificate.issuerDN!"")?html}
            <br/>
            <pre id="cert">${certificate.x509data?html}</pre>
            <input type="button" value="<@global.text key='button.downloadCert'/>"
                   onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}'; return false;"/>
            <#if chain??>
                <h2><@global.text key='text.text.cert.chain'/></h2>
                <pre id="chain">${chain?html}</pre>
            <#-- <input type="button" value="<@global.text key='button.saveChain'/>" onclick='saveFile("Chain.p7c", "application/pkcs7-mime", document.getElementById("chain").innerHTML);'/> -->
                <input type="button" value="<@global.text key='button.downloadchain'/>"
                       onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}&chain=true'; return false;"/>
                <input type="button" value="<@global.text key='button.downloadchainp7'/>"
                       onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}&chain=true&p7=true'; return false;"/>
            </#if>
        </form>
    <#elseif uploadCert??>
        <h2><@global.text key='text.text.upload.cert'/></h2>
        <form id="uForm" action="uploadCertificate.html" method="post" enctype="multipart/form-data">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.name'/>:*</span>
            <@global.spring.formInput path="uploadCert.name" attributes='size="50"'/>
            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
            <br/>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.select.cert.file'/>*</span>
            <#if uploadCert.uploadFileName?? && duplicate??>
                <@global.spring.formHiddenInput path='uploadCert.uploadData'/><#-- b64 backup of uploadfile after initial upload -->
                <@global.spring.formHiddenInput path='uploadCert.uploadFileName'/>
                ${uploadCert.uploadFileName?html}
                <@global.spring.bind path="uploadCert.uploadData"/>
                <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                <br/>
                <span style="width: 140px; display: inline-block;"><@global.text key='text.subjectDN'/>:</span>
                <span <#if duplicate??>class="warn"</#if>>${uploadCert.subjectDN?html}</span>
                <br/>
                <#if duplicate??>
                    <span style="width: 140px; display: inline-block;"><@global.text key='text.confirm.replace.cert'/>:</span>
                    <input type="checkbox" name="replace" value="true"
                           onchange="document.getElementById('uploadBtn').disabled = !this.checked;"">
                    <br/>
                </#if>
            <#else>
                <input type="file" name="uploadFile" value="<@global.text key='button.select.cert.file'/>"/>
                <@global.spring.bind path="uploadCert.uploadFile"/>
                <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
            </#if>

            <br/>
            <input type="submit" id="uploadBtn" name="uploadBtn" value="<@global.text key='button.upload'/>"
                   <#if duplicate??>disabled="true"</#if>/>
            <input type="submit" value="<@global.text key='button.cancel'/>"
                   onclick="document.location.href='certificateList.html?x=${(.now?time)?html}'; return false;"/>
        </form>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
