<#import "../common/global.ftl" as global/>
<@global.page>
    <script type="text/javascript" src="../js/calendar.js">
        // need for browsers...
    </script>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <span
                class="selected"><@global.text key='text.certificates'/></span></h1>
    <#if msg?exists>
        <span class="msg">${msg?html}</span><br/>
    </#if>
    <h2><@global.text key='text.search.certificates'/></h2>
    <form id="uSearchForm" action="certificateList.html" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <span style="width: 110px; display: inline-block;"><@global.text key='text.name'/>:</span>
        <@global.spring.formInput path="searcher.name" attributes='size="25" maxlength="50"'/>
        <br/>
        <span style="width: 110px; display: inline-block;"><@global.text key='text.issuerDN'/>:</span>
        <@global.spring.formInput path="searcher.issuerDN" attributes='size="25" maxlength="50"'/>
        <br/>
        <span style="width: 110px; display: inline-block;"><@global.text key='text.subjectDN'/>:</span>
        <@global.spring.formInput path="searcher.subjectDN" attributes='size="25" maxlength="50"'/>
        <br/>
        <span style="width: 110px; display: inline-block;"><@global.text key='text.expiresRange'/>:</span>
        <#if searcher?exists && searcher.expiresFrom?exists >
            <#assign dateFrom = searcher.expiresFrom?string(dateFormat)/>
        <#else>
            <#assign dateFrom = ""/>
        </#if>

        <input id="dateFrom" type="text" size="10" name="expiresFrom" value="${dateFrom}"/>
        <a href="javascript:showCalPopup('dfoico', 'dateFrom', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
            <img id="dfoico" align="baseline" border="0" src="../img/calicon.gif"/></a>
        <@global.text key='text.to'/>
        <#if searcher?exists && searcher.expiresTo?exists>
            <#assign dateTo= searcher.expiresTo?string(dateFormat)/>
        <#else>
            <#assign dateTo = ""/>
        </#if>
        <input id="dateTo" type="text" size="10" name="expiresTo" value="${dateTo}"/>
        <a href="javascript:showCalPopup('dtoico', 'dateTo', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
            <img id="dtoico" align="baseline" border="0" src="../img/calicon.gif"/></a>

        <input type="button" name="today" value="<@global.text key='text.today'/>"
               onClick="fillDatesToday('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="yesterDay" value="<@global.text key='text.yesterday'/>"
               onClick="fillDatesYesterday('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="thisMonth" value="<@global.text key='text.thismonth'/>"
               onClick="fillDatesThisMonth('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="prevMonth" value="<@global.text key='text.prevmonth'/>"
               onClick="fillDatesPrevMonth('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <br/>
        <span style="width: 110px; display: inline-block;"><@global.text key='text.status'/>:</span>
        <@global.spring.formSingleSelect path="searcher.status" options=certificateStatuses />
        <br/>
        <span style="width: 110px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
        <@global.spring.formSingleSelect path="searcher.order" options={"":"","id":"Id", "status":"Status", "name":"Name"} />
        <@global.spring.formSingleSelect path="searcher.orderDirection" options={"asc":"Ascending","desc":"Descending"} />
        <span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
        <@global.spring.formSingleSelect path="searcher.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
        <br/>
        <input type="hidden" name="search" value="Search"/>
        <input type="submit" name="submitbtn" value="<@global.text key='button.search'/>"/>
        <br/>
        <#if found?exists>
        <#if searcher?exists && searcher.total &gt; 0 >
        <h2><@global.text key='text.found.total'/> ${searcher.total!""?html} <@global.text key='text.certificates'/>
            , <@global.text key='text.showing'/> ${(searcher.start+1)!""?html} <@global.text key='text.to'/>
            <#if searcher.total &gt; (searcher.start+searcher.limit)>
                ${(searcher.start+searcher.limit)!""?html}
            <#else>
                ${(searcher.total)!""?html}
            </#if>
        </h2>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="5%" class="tdHeader">Id</td>
                <td width="15%" class="tdHeader"><@global.text key='text.name'/></td>
                <td width="15%" class="tdHeader"><@global.text key='text.subjectDN'/></td>
                <td width="15%" class="tdHeader"><@global.text key='text.issuerDN'/></td>
                <td align="center" class="tdHeader"><@global.text key='text.expires'/></td>
                <td align="center" class="tdHeader"><@global.text key='text.status'/></td>
            </tr>
            <#list found as acqx>
                <tr <#if acqx_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                    <@certificateDetails cd=acqx />
                </tr>
            </#list>
            <@global.pagingButtons searcher=searcher colspan=6/>
        </table>
    </form>
<#else>
    <@global.text key='text.no.matches.found'/>
</#if>
</#if>
</@global.page>

<#macro certificateDetails cd>
    <td width="5%"><a href="certificateView.html?id=${(cd.id!"")?html}">${(cd.id!"")?html}</a></td>
    <td width="15%"><a href="certificateView.html?id=${(cd.id!"")?html}">${(cd.name!"")?html}</a></td>
    <td width="15%">${(cd.subjectDN!"")?html}</td>
    <td width="15%">${(cd.issuerDN!"")?html}</td>
    <td width="10%"<#if cd.expires?? && cd.expires<.now> style="color: RED;" </#if>><#if cd.expires??>${cd.expires?string(dateFormat+" HH:mm:ss")}</#if></td>
    <td width="5%" align="center"><@global.objstatus certificateStatuses cd.status!""/></td>
</#macro>