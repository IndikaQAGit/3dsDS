<#-- @ftlvariable name="attemptsACS" type="com.modirum.ds.db.model.ACSProfile" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <a href="attemptsACSList.html?x=${(.now?time)?html}"><@global.text key='text.attempts.acs.profiles'/></a> &gt;
        <#if attemptsACS?exists>
            <span class="selected"><@global.text key='text.details'/>: ${(attemptsACS.getName()!"")?html}</span>
        </#if>
    </h1>
    <h2><@global.text key='text.attempts.acs.profile'/></h2>
    <#if attemptsACS??>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <@global.security.authorize access="hasAnyRole('issuerEdit')">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <form action="attemptsACSEdit.html" method="get">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="id" value="${(attemptsACS.getId()!"")?html}"/>
                                        <input type="submit" name="button" value="<@global.text key='button.edit'/>"/>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </@global.security.authorize>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical">Id:</td>
                <td>${(attemptsACS.id!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.name'/>:</td>
                <td>${(attemptsACS.name!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                <td>${(paymentSystemName!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.acsprofile.refNo'/>*:</td>
                <td>${(attemptsACS.refNo!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.acsprofile.url'/>*:</td>
                <td>${(attemptsACS.URL!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.attempts.acs.3dsmethodurl'/>:</td>
                <td>${(attemptsACS.threeDSMethodURL!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical">
                    <@global.text key='text.acsprofile.in.clientcertid'/>
                    :<#if sslCertsManagedExternally?? && sslCertsManagedExternally><#else>*</#if>
                </td>
                <td>${(attemptsACS.inClientCert!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.acsprofile.out.clientcertid'/>:</td>
                <td>${(attemptsACS.outClientCert!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.protoStart'/>:</td>
                <td>${(attemptsACS.startProtocolVersion!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.protoEnd'/>:</td>
                <td>${(attemptsACS.endProtocolVersion!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.status'/>:</td>
                <td><@global.objstatus acpStatuses attemptsACS.status!""/></td>
            </tr>
            <#if attemptsACS.lastMod??>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.lastmod'/>:</td>
                    <td>
                        ${attemptsACS.lastMod?string(dateFormat+" HH:mm:ss")}
                    </td>
                </tr>
            </#if>
        </table>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
