<#-- @ftlvariable name="tdsServerProfile" type="com.modirum.ds.db.model.TDSServerProfile" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a
                href="tdsServerList.html?x=${(.now?time)?html}"><@global.text key='text.tdsserverprofiles'/></a> &gt;
        <#if tdsServerProfile??><span class="selected">Details: ${(tdsServerProfile.getName()!"")?html}</span></#if></h1>
    <h2><@global.text key='text.tdsserverprofile'/></h2>
    <#if tdsServerProfile??>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <@global.security.authorize access="hasAnyRole('acquirerEdit')">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <form action="tdsServerEdit.html" method="post">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="id" value="${(tdsServerProfile.getId()!"")?html}"/>
                                        <input type="submit" name="button" value="<@global.text key='button.edit'/>"/>
                                        <input type="submit" name="delete" value="<@global.text key='button.delete'/>"
                                               onclick="confirmWarning(event,'${(loca.getText("text.wrn.are.you.sure.delete",loca.getText("text.tdsserverprofile",locale) locale)!"")?html}')"/>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </@global.security.authorize>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key="text.tdsserverprofile.id"/>:</td>
                <td>${(tdsServerProfile.id!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.name'/>:</td>
                <td>${(tdsServerProfile.name!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                <td>${(paymentSystemName!"")?html}</td>
            </tr>
            <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
            <#else>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.3ds.requestorId'/>:</td>
                    <td>${(tdsServerProfile.requestorID!"")?html}</td>
                </tr>
            </#if>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.3ds.operatorId'/>:</td>
                <td>${(tdsServerProfile.operatorID!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.3ds.referenceNumberList'/>:</td>
                <td><div style="word-wrap: break-word; width:350px">${(tdsServerProfile.tdsReferenceNumberList!"")?html}</div></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.tdsserver.url'/>:</td>
                <td>${(tdsServerProfile.URL!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.status'/>:</td>
                <td><@global.objstatus tdsServerStatuses tdsServerProfile.status!""/></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.tdsserverprofile.in.clientcertid'/>:*</td>
                <td><#if inCertificateList?? && tdsServerProfile.inClientCert??>${(inCertificateList[tdsServerProfile.inClientCert?string]!"")?html}</#if></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.tdsserverprofile.out.clientcertid'/>:</td>
                <td><#if outCertificateList?? && tdsServerProfile.outClientCert??>${(outCertificateList[tdsServerProfile.outClientCert?string]!"")?html}</#if></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.tdsserverprofile.merchantcount'/>:</td>
                <td class="bottom"><a
                            href="merchantList.html?TDSServerProfileId=${(tdsServerProfile.getId()!"")?html}&search=y">${(tdsServerMerchantCount!"")?html}</a>
                    &nbsp;
                    <@global.security.authorize access="hasAnyRole('acquirerEdit')">
                        <form action="merchantEdit.html" method="post" style="display: inline-block;">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <input type="hidden" name="TDSServerProfileId" value="${(tdsServerProfile.getId()!"")?html}"/>
                            <input type="submit" name="button" value="<@global.text key='button.addNew'/>"
                                   class="bottom" style="margin: 0px;"/>
                        </form>
                    </@global.security.authorize>
                </td>
            </tr>
        </table>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
