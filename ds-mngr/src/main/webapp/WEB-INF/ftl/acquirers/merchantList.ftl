<#-- @ftlvariable name="filter" type="com.modirum.ds.mngr.model.ui.filter.MerchantSearchFilter" -->
<#-- @ftlvariable name="merchantList" type="java.util.List<com.modirum.ds.db.model.Merchant>" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <span class="selected"><@global.text key='text.merchants'/></span>
    </h1>

    <@global.security.authorize access="hasAnyRole('acquirerEdit')">
        <a href="merchantEdit.html?x=${(.now?time)?html}"><@global.text key='text.addNewMerchant'/></a>    <a
            href="merchantsBatch.html">Batch Merchants</a>
    </@global.security.authorize>

    <h2><@global.text key='text.search.merchants'/></h2>
    <style> input[type="text"], input[type="password"], select {
            margin-bottom: 2pt;
        } </style>
    <form id="uSearchForm" action="merchantList.html" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="hidden" name="cmd" value=""/>
    <#assign col1_width=130/> <#-- this should be a table instead -->
    <span style="width: ${col1_width}px; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
    <@global.spring.formSingleSelect path="filter.paymentSystemId" options=paymentSystemSearchMap
        attributes='onchange="submitFormWithCmd(null, \'uSearchForm\' , \'refreshPaymentSystem\');"'/>
    <br/>
    <span style="width: ${col1_width}px; display: inline-block;"><@global.text key='text.name'/>:</span>
    <@global.spring.formInput path="filter.name" attributes='size="25" maxlength="50"'/>
    <br/>
    <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
        <span style="width: ${col1_width}px; display: inline-block;"><@global.text key='text.3ds.requestorId'/>:</span>
        <@global.spring.formInput path="filter.requestorID" attributes='size="25" maxlength="35"'/>
        <br/>
    </#if>
    <span style="width: ${col1_width}px; display: inline-block;"><@global.text key='text.merchant.acquirerMerchantID'/>:</span>
    <@global.spring.formInput path="filter.acquirerMerchantID" attributes='size="25" maxlength="24"'/>
    <br/>
    <span style="width: ${col1_width}px; display: inline-block;"><@global.text key='text.acquirer'/>:</span>
    <@global.spring.formSingleSelect path="filter.acquirerId" options=acquirersMap attributes='style="width: 200px;"'/>
    <br/>
    <span style="width: ${col1_width}px; display: inline-block;"><@global.text key='text.country'/>:</span>
    <@global.spring.formSingleSelect path="filter.country" options=countryMapA2 />
    <br/>
    <span style="width: ${col1_width}px; display: inline-block;"><@global.text key='text.status'/>:</span>
    <@global.spring.formSingleSelect path="filter.status" options=merchantStatuses />
    <br/>
    <span style="width: ${col1_width}px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
    <@global.spring.formSingleSelect path="filter.order" options={"":"","id":"Id", "status":"Status", "name":"Name"} />
    <@global.spring.formSingleSelect path="filter.orderDirection" options={"asc":"Ascending","desc":"Descending"} />
    <span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
    <@global.spring.formSingleSelect path="filter.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
    <br/>
    <input type="hidden" name="search" value="Search"/>
    <input type="submit" name="submitbtn" value="<@global.text key='button.search'/>"/>
    <br/>
    <#if merchantList?exists>
        <#if filter?exists && filter.total &gt; 0 >
            <h2><@global.text key='text.found.total'/> ${filter.total!""?html} <@global.text key='text.merchants'/>
                , <@global.text key='text.showing'/> ${(filter.start+1)!""?html} <@global.text key='text.to'/>
                <#if filter.total &gt; (filter.start+filter.limit)>
                    ${(filter.start+filter.limit)!""?html}
                <#else>
                    ${(filter.total)!""?html}
                </#if>
            </h2>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" class="tdHeader">Id</td>
                    <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
                        <td width="10%" class="tdHeader"><@global.text key='text.3ds.requestorId'/></td>
                    </#if>
                    <td width="10%" class="tdHeader"><@global.text key='text.merchant.acquirerMerchantID'/></td>
                    <td width="15%" class="tdHeader"><@global.text key='text.name'/></td>
                    <td width="15%" class="tdHeader"><@global.text key='text.tdsserverprofile'/></td>
                    <td width="15%" class="tdHeader"><@global.text key='text.acquirer'/></td>
                    <td width="10%" class="tdHeader"><@global.text key='text.country'/></td>
                    <td width="10%" class="tdHeader"><@global.text key='general.paymentSystem'/></td>
                    <td align="center" class="tdHeader"><@global.text key='text.status'/></td>
                </tr>
                <#list merchantList as merchant>
                    <tr <#if merchant_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                        <@MerchantDetails mer=merchant />
                    </tr>
                </#list>
                <@global.pagingButtons searcher=filter colspan=7/>
            </table>
            </form>
        <#else>
            <br/>
            <@global.text key='text.no.matches.found'/>
        </#if>
    </#if>
</@global.page>

<#macro MerchantDetails mer>
    <td><a href="merchantView.html?id=${(mer.id!"")?html}">${(mer.id!"")?html}</a></td>
    <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
        <td>${(mer.requestorID!"")?html}</td>
    </#if>
    <td>${(mer.identifier!"")?html}</td>
    <td>${(mer.name!"")?html}</td>
    <td><#if tdsServerList?? && mer.tdsServerProfileId??>${(tdsServerList[mer.tdsServerProfileId?string]!"")?html}</#if></td>
    <td><#if acquirersMap?? && mer.acquirerId??>${(acquirersMap[mer.acquirerId?string]!"")?html}</#if></td>
    <td><#if countryMapA2?? && mer.country??>${(countryMapA2[mer.country?string]!mer.country)?html}</#if></td>
    <td>
        <#if mer.paymentSystemId??>
            ${((paymentSystemComponentMap[mer.paymentSystemId?string])!mer.paymentSystemId)?html}
        </#if>
    </td>
    <td align="center"><@global.objstatus merchantStatuses mer.status!""/></td>
</#macro>
