<#-- @ftlvariable name="acquirer" type="com.modirum.ds.db.model.Acquirer" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <a href="acquirerList.html?x=${(.now?time)?html}"><@global.text key='text.acquirers'/></a> &gt;
        <#if acquirer?? && acquirer.id??>
            <a href="acquirerView.html?id=${(acquirer.getId()!"")?html}">
            <@global.text key='text.details'/>: ${(acquirer.name!"")?html}</a> &gt;
        </#if>
        <span class="selected">
            <#if acquirer?? && acquirer.id??>
                <@global.text key='text.edit'/>
            <#else>
                <@global.text key='text.add.new'/>
            </#if>
        </span>
    </h1>
    <h2><@global.text key='text.acquirer'/></h2>
    <#if acquirer??>
        <form id="uForm" action="acquirerUpdate.html" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <@global.spring.formHiddenInput path='acquirer.id'/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="130" class="tdHeaderVertical"><@global.text key='text.name'/>:*</td>
                    <td width="475">
                        <@global.spring.formInput path="acquirer.name" attributes='size="50"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>Known or business name</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.acquirer.bin'/>*:</td>
                    <td>
                        <@global.spring.formInput path="acquirer.BIN" attributes='size="10" maxlength="12"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>BIN acquirer is registered with Scheme</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                    <td>
                        <#if acquirer?? && acquirer.id??>
                            ${(paymentSystemName!"")?html}
                            <@global.spring.formHiddenInput path='acquirer.paymentSystemId'/>
                        <#else>
                            <@global.spring.formSingleSelect path="acquirer.paymentSystemId" options=paymentSystemComponentMap/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </#if>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.email'/>:</td>
                    <td>
                        <@global.spring.formInput path="acquirer.email" attributes='size="50" maxlength="128"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.address'/>:</td>
                    <td>
                        <@global.spring.formInput path="acquirer.address" attributes='size="50" maxlength="128"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.city'/>:</td>
                    <td>
                        <@global.spring.formInput path="acquirer.city" attributes='size="50" maxlength="128"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.country'/>:</td>
                    <td>
                        <@global.spring.formSingleSelect path="acquirer.country" options=countryMapA2 />
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.phone'/>:</td>
                    <td>
                        <@global.spring.formInput path="acquirer.phone" attributes='size="16" maxlength="16"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.status'/>:*</td>
                    <td>
                        <@global.spring.formSingleSelect path="acquirer.status" options=acquirerStatuses/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                <tr>
                    <td colspan="3">
                        <input type="submit" name="save" value="<@global.text key='button.save'/>"/>
                        <#if acquirer.id??>
                            <input type="submit" name="saveAsNew" value="<@global.text key='button.saveAsNew'/>">
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'acquirerView.html?id=${acquirer.id}'; return false;"/>
                        <#else>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'acquirerList.html'; return false;"/>
                        </#if>
                    </td>
                </tr>
            </table>
        </form>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
