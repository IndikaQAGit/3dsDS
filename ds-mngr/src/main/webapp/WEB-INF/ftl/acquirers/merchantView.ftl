<#-- @ftlvariable name="merchant" type="com.modirum.ds.db.model.Merchant" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a
                href="merchantList.html?x=${(.now?time)?html}"><@global.text key='text.merchants'/></a> &gt;
        <#if merchant??><span class="selected">Details: ${(merchant.getName()!"")?html}</span></#if></h1>
    <h2><@global.text key='text.merchant'/></h2>
    <#if merchant??>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <@global.security.authorize access="hasAnyRole('acquirerEdit')">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <form action="merchantEdit.html" method="post">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="id" value="${(merchant.getId()!"")?html}"/>
                                        <input type="submit" name="button" value="<@global.text key='button.edit'/>"
                                               <#if merchant.status=="E">disabled</#if>/>
                                        <input type="submit" name="delete" value="<@global.text key='button.delete'/>"
                                               onclick="confirmWarning(event,'${(loca.getText("text.wrn.are.you.sure.delete",loca.getText("text.merchant",locale) locale)!"")?html}');"/>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </@global.security.authorize>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key="text.merchant.id"/>:</td>
                <td>${(merchant.id!"")?html}</td>
            </tr>

            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.name'/>:</td>
                <td>${(merchant.name!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                <td>${(paymentSystemName!"")?html}</td>
            </tr>
            <#if requestorIDwithMerchant?? && requestorIDwithMerchant>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.3ds.requestorId'/>:</td>
                    <td>${(merchant.requestorID!"")?html}</td>
                </tr>
            </#if>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.merchant.acquirerMerchantID'/>:</td>
                <td>${(merchant.identifier!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.acquirer'/>:</td>
                <td><#if acquirer??>${(acquirer.name!"")?html}</#if></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.tdsserverprofile'/>:</td>
                <td><#if tdsServer??>${(tdsServer.id!"")?html}-${(tdsServer.name!"")?html}</#if></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.email'/>:</td>
                <td>${(merchant.email!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.phone'/>:</td>
                <td>${(merchant.phone!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.url'/>:</td>
                <td>${(merchant.URL!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeader"><@global.text key='text.country'/></td>
                <td><#if merchant.country??>${(countryMapA2[merchant.country?string]!"")?html}</#if></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.ext.options'/>:</td>
                <td>${(merchant.options!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.status'/>:</td>
                <td><@global.objstatus merchantStatuses merchant.status!""/></td>
            </tr>
        </table>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
