<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a
                href="merchantList.html?x=${(.now?time)?html}"><@global.text key='text.merchants'/></a> &gt;
        <@global.text key='text.import.csv'/></h1>

    <h2><@global.text key='text.import.csv'/></h2>
    <form id="form" action="merchantsBatch.html" method="post" enctype="multipart/form-data">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <br/>

        <#if data.uploadFileName??>
            <@global.spring.formHiddenInput path='data.uploadData'/><#-- b64 backup of uploadfile after initial upload -->
            <@global.spring.formHiddenInput path='data.uploadFileName'/>
            <b>${data.uploadFileName?html}</b>
            <@global.spring.bind path="data.uploadData"/>
            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
            <span> or Select new file </span>
            <input type="file" name="uploadFile" value="<@global.text key='button.select.file.csv'/>"/>
            <br/>
        <#else>
            <input type="file" name="uploadFile" value="<@global.text key='button.select.file.csv'/>"/>
            <@global.spring.bind path="data.uploadFile"/>
            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        </#if>

        <br/>
        <br/>
        <input type="submit" id="validate" name="actionButton" value="Validate"
               title="Validate CSV file without import data into database. All errors will be displayed."/>
        <input type="submit" id="uploadBtn" name="actionButton" value="Upload"
               title="Import CSV file. Only first error will be displayed during validation."/>
        <input type="submit" value="<@global.text key='button.cancel'/>"
               onclick="document.location.href = 'merchantList.html'; return false;"/>
    </form>

    <#if results??>
        <#if results.errors??>
            <#list results.errors as error>
                <p class="error">${error}</p>
            </#list>
        </#if>

        <#if results.status??>
            <#if results.status?size == 0>
                <p>CSV file with merchants is valid.</p>
            </#if>
            <#list results.status as key, value>
                <#switch key>
                    <#case "ADD">
                        <p>Added merchants: ${value}</p>
                        <#break>
                    <#case "DELETE">
                        <p>Removed merchants: ${value}</p>
                        <#break>
                    <#case "DISABLE">
                        <p>Disabled merchants: ${value}</p>
                        <#break>
                    <#case "ACTIVATE">
                        <p>Activated merchants: ${value}</p>
                        <#break>
                    <#default>
                        <#break>
                </#switch>
            </#list>
        </#if>
    </#if>
</@global.page>