package com.modirum.ds.mngr.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Report;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.model.filter.ReportListItem;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.PaymentSystemConstants;
import com.modirum.ds.enums.ReportStatus;
import com.modirum.ds.enums.ReportType;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.mngr.model.ui.filter.ReportsFilter;
import com.modirum.ds.model.ReportSearcher;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.model.util.ReportMapper;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/reports")
public class ReportController extends BaseController {

    PersistenceService persistenceService = ServiceLocator.getInstance().getPersistenceService();

    private final static Logger log = LoggerFactory.getLogger(ReportController.class);

    private final static List<String> timezones;
    private final static Map<String, ReportType> reportTypes;
    private final static Map<String, ReportStatus> reportStatuses;

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        timezones = Collections.unmodifiableList(Arrays.stream(TimeZone.getAvailableIDs()).sorted()
                        .collect(Collectors.toList()));

        Map<String, ReportType> reportTypeMap = new HashMap<String, ReportType>();
        for (ReportType reportType: ReportType.values()) {
            reportTypeMap.put(reportType.getLabel(), reportType);
        }
        reportTypes = Collections.unmodifiableMap(reportTypeMap);

        Map<String, ReportStatus> reportStatusMap = new HashMap<String, ReportStatus>();
        for (ReportStatus reportStatus: ReportStatus.values()) {
            reportStatusMap.put(reportStatus.getLabel(), reportStatus);
        }
        reportStatuses = Collections.unmodifiableMap(reportStatusMap);
    }

    @RequestMapping("/reportsList.html")
    public ModelAndView listReports(HttpServletRequest request,
                                    @ModelAttribute ReportSearcher searcher) throws Exception {
        if (!AuthorityUtil.hasAnyRole(Roles.reports)) {
            return unauthorized(request);
        }

        if (searcher.getPaymentSystemId() == null && !PaymentSystemConstants.UNLIMITED.equals(currentUser().getPaymentSystemId())) {
            searcher.setPaymentSystemId(currentUser().getPaymentSystemId());
        }

        if (searcher.getStart() == null) {
            searcher.setStart(0);
        }
        if (searcher.getLimit() == null) {
            searcher.setLimit(25);
        }

        ReportListItem filter = ReportMapper.mapToDbModel(searcher);
        List<Report> reports = persistenceService.listReports(filter);

        ModelAndView mav = new ModelAndView("reports/reportsList");
        mav.addObject("reportTypes", reportTypes);
        mav.addObject("reportStatuses", reportStatuses);
        mav.addObject("searcher", ReportMapper.mapFromDbModel(filter));
        mav.addObject("found", reports);
        mav.addObject("categorizedReportsTypes", getCategorizedReportsTypes());

        Setting autoRefresh = cnfs.getSettingByKey(DsSetting.REPORTS_AUTO_REFRESH_SECONDS.getKey());
        if (autoRefresh != null) {
            Integer value = Misc.parseInt(autoRefresh.getValue());
            if (value > 0) {
                mav.addObject("autoRefresh", value);
            }
        }
        return mav;
    }

    @RequestMapping({"/reportsTable.html"})
    public ModelAndView generateReportTable(HttpServletRequest request,
                                            @ModelAttribute ReportSearcher searcher) {
        if (!AuthorityUtil.hasAnyRole(Roles.reports)) {
            return unauthorized(request);
        }

        if (searcher.getPaymentSystemId() == null && !PaymentSystemConstants.UNLIMITED.equals(currentUser().getPaymentSystemId())) {
            searcher.setPaymentSystemId(currentUser().getPaymentSystemId());
        }

        ReportListItem filter = ReportMapper.mapToDbModel(searcher);
        List<Report> reports = persistenceService.listReports(filter);

        ModelAndView mav = new ModelAndView("reports/reportsTable");
        mav.addObject("reportTypes", reportTypes);
        mav.addObject("reportStatuses", reportStatuses);
        mav.addObject("searcher", ReportMapper.mapFromDbModel(filter));
        mav.addObject("found", reports);
        return mav;
    }

    private ModelAndView reportsEditView(ReportsFilter reportsFilter) {
        return reportsEditView(reportsFilter, null);
    }

    private ModelAndView reportsEditView(ReportsFilter reportsFilter, String error) {
        String reportType = reportsFilter.getType();
        ReportType reportsType = ReportType.getByReportLabel(reportType);
        ModelAndView mav = new ModelAndView("reports/reportsEdit");
        if (Misc.isNotNullOrEmpty(error)) {
            mav.addObject("error", error);
        }
        mav.addObject("typeHeader", reportsType.getDescription());
        mav.addObject("reportType", reportType);
        mav.addObject("finder", reportsFilter);
        mav.addObject("timezones", timezones);
        return mav;
    }

    @RequestMapping({"/reportsEdit.html"})
    public ModelAndView editReport(HttpServletRequest request,
                                    @RequestParam(required = true, value = "type") String reportType,
                                    @ModelAttribute(value = "filter") ReportsFilter reportsFilter) {

        if (!AuthorityUtil.hasAnyRole(Roles.reports)) {
            return unauthorized(request);
        }

        ReportType reportsType = ReportType.getByReportLabel(reportType);
        if (reportsType == null) {
            return invalidRequest();
        }

        if (!PaymentSystemConstants.UNLIMITED.equals(currentUser().getPaymentSystemId())) {
            reportsFilter.setPaymentSystemId(currentUser().getPaymentSystemId());
        }

        if (Misc.isNullOrEmpty(reportsFilter.getLocale())) {
            reportsFilter.setLocale(Locale.US.toString());
        }

        if (Misc.isNullOrEmpty(reportsFilter.getTimezone())) {
            reportsFilter.setTimezone("UTC");
        }

        reportsFilter.setType(reportType);

        return reportsEditView(reportsFilter);
    }

    private String createJson(Map<String, Object> map) throws JsonProcessingException {
        return mapper.writeValueAsString(map);
    }

    @RequestMapping({"/reportsSave.html"})
    public ModelAndView saveReport(HttpServletRequest request,
                                  @ModelAttribute(value = "filter") ReportsFilter reportsFilter,
                                  BindingResult result) throws Exception {

        if (!AuthorityUtil.hasAnyRole(Roles.reports)) {
            return unauthorized(request);
        }

        ReportType reportType = ReportType.getByReportLabel(reportsFilter.getType());
        if (reportType == null) {
            return invalidRequest();
        }

        Map<String, Object> json = new HashMap<String, Object>();
        Report report = new Report();
        String error = null;
        report.setName(reportType.getLabel());

        switch(reportType) {
            case TRANSACTION:
                StringBuilder startDateBuilder = new StringBuilder();
                Date startDate = reportsFilter.getStartDate();
                if (startDate != null) {
                    startDateBuilder.append(DateUtil.formatDate(startDate, "yyyy-MM-dd"));
                    String hourString = reportsFilter.getStartDateHour();
                    if (Misc.isNotNullOrEmpty(hourString)) {
                        boolean validHour = false;
                        if (Misc.isNumber(hourString)) {
                            int hour = Misc.parseInt(hourString);
                            if (hour >= 0 && hour <= 23) {
                                validHour = true;
                                startDateBuilder.append(" ")
                                         .append(Misc.padCutStringLeft(hourString, '0', 2))
                                         .append(":");
                            }
                        }
                        if (!validHour) {
                            error = "err.invalid.startdatetime";
                        }
                    } else {
                        error = "err.invalid.startdatetime";
                    }

                    String minuteString = reportsFilter.getStartDateMinute();
                    if (Misc.isNotNullOrEmpty(minuteString)) {
                        boolean validMinute = false;
                        if (Misc.isNumber(minuteString)) {
                            int hour = Misc.parseInt(minuteString);
                            if (hour >= 0 && hour <= 59) {
                                validMinute = true;
                                startDateBuilder.append(Misc.padCutStringLeft(minuteString, '0', 2))
                                         .append(":00");
                                report.setDateFrom(startDateBuilder.toString());
                            }
                        }
                        if (!validMinute) {
                            error = "err.invalid.startdatetime";
                        }
                    } else {
                        error = "err.invalid.startdatetime";
                    }
                } else {
                    error = "err.invalid.startdatetime";
                }

                if (error == null) {
                    Date endDate = reportsFilter.getEndDate();
                    if (endDate != null) {
                        report.setDateTo(DateUtil.formatDate(DateUtil.normalizeDateDayEnd(endDate),
                                "yyyy-MM-dd HH:mm:ss"));
                    } else {
                        error = "err.invalid.enddate";
                    }
                }

                if (error == null) {
                    if (reportsFilter.getEndDate().before(reportsFilter.getStartDate())) {
                        error = "err.startdate.after.enddate";
                    }
                }
                if (Misc.isNotNullOrEmpty(reportsFilter.getLocale())) {
                    json.put("locale", reportsFilter.getLocale());
                }

                if (Misc.isNotNullOrEmpty(reportsFilter.getTimezone())) {
                    json.put("timezone", reportsFilter.getTimezone());
                }
                break;
            case USER_AUDIT:
                if (Misc.isNotNullOrEmpty(reportsFilter.getLocale())) {
                    json.put("locale", reportsFilter.getLocale());
                }

                if (Misc.isNotNullOrEmpty(reportsFilter.getTimezone())) {
                    json.put("timezone", reportsFilter.getTimezone());
                }
                break;
        }

        if (error != null) {
            return reportsEditView(reportsFilter, getLocalizedMessage(error));
        }

        report.setPaymentSystemId(reportsFilter.getPaymentSystemId());
        report.setStatus(ReportStatus.QUEUED.getLabel());
        report.setCreatedDate(new Date());
        report.setCreatedUser(currentUser().getLoginname());
        report.setLastModifiedDate(new Date());
        report.setLastModifiedBy(currentUser().getLoginname());
        if (!json.isEmpty()) {
            report.setCriteria(createJson(json));
        }

        if (!AuthorityUtil.isAccessAllowed(report)) {
            return unauthorized(request);
        }

        persistenceService.saveOrUpdate(report);
        auditLogService.logAction(AuthorityUtil.getUser(), report, DSModel.AuditLog.Action.INSERT,
                "Added report " + report.getId());
        return redirect("/reports/reportsList.html");
    }

    @RequestMapping({"/reportsDelete.html"})
    public ModelAndView deleteReport(HttpServletRequest request,
                                      @RequestParam(value = "id", required = true) String id) throws Exception {

        if (!AuthorityUtil.hasAnyRole(Roles.reports)) {
            return unauthorized(request);
        }

        Report report = persistenceService.getPersistableById(Misc.parseLong(id), Report.class);

        if (!AuthorityUtil.isAccessAllowed(report)) {
            return unauthorized(request);
        }

        if (report != null) {
            report.setLastModifiedDate(new Date());
            report.setLastModifiedBy(currentUser().getLoginname());
            report.setStatus(ReportStatus.DELETED.getLabel());
            persistenceService.saveOrUpdate(report);
        }
        return redirect("/reports/reportsList.html");
    }

    @RequestMapping({"/reportsDownload.html"})
    public Object downloadReport(HttpServletRequest request, HttpServletResponse response,
                                      @RequestParam(value = "id", required = true) String id) throws Exception {

        String reportsDownloadUrlPrefix = cnfs.getStringSetting(DsSetting.REPORTS_DOWNLOAD_URL_PREFIX.getKey());
        Report report = persistenceService.getPersistableById(Misc.parseLong(id), Report.class);

        if (report == null || Misc.isNullOrEmpty(reportsDownloadUrlPrefix)) {
            return invalidRequest();
        }

        if (!(AuthorityUtil.hasAnyRole(Roles.reports) && AuthorityUtil.isAccessAllowed(report))) {
            return unauthorized(request);
        }

        File file = new File(reportsDownloadUrlPrefix + report.getSecret() + "/" + report.getFilename());
        if (!file.exists()) {
            return invalidRequest();
        }

        response.addHeader(HttpHeaders.CONTENT_TYPE, "text/csv;charset=UTF-8");
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + report.getFilename() + "\"");
        response.addHeader(HttpHeaders.EXPIRES, "0");

        writeFileToResponse(file, response);

        auditLogService.logAction(AuthorityUtil.getUser(), report, DSModel.AuditLog.Action.VIEW,
                "Downloaded report " + report.getId());

        return ResponseEntity.ok().build();
    }

    private void writeFileToResponse(File file, HttpServletResponse response) {
        try (FileInputStream fis = new FileInputStream(file);
             ReadableByteChannel inputChannel = Channels.newChannel(fis);
             WritableByteChannel outputChannel = Channels.newChannel(response.getOutputStream());) {

            ByteBuffer buffer = ByteBuffer.allocateDirect(10240);
            while (inputChannel.read(buffer) != -1) {
                buffer.flip();
                outputChannel.write(buffer);
                buffer.clear();
            }
        } catch (IOException e) {
            log.error("Failed to download report", e);
        }
    }

    private Map<String, List<ReportType>> getCategorizedReportsTypes() {
        Comparator<ReportType> comp = Comparator.comparing(ReportType::getLabel);
        Map<String, List<ReportType>> categoriesMap = new LinkedHashMap<>();
        addCategory(categoriesMap, getLocalizedMessage("text.report.type.transactions"), comp, ReportType.TRANSACTION_REPORTS);
        addCategory(categoriesMap, getLocalizedMessage("text.report.type.users"), comp, ReportType.USER_REPORTS);
        return categoriesMap;
    }

    private void addCategory(Map<String, List<ReportType>> categoriesMap, String categoryName,
                             Comparator<ReportType> comp, List<ReportType> categoryReportTypes) {
        if (Misc.isNotNullOrEmpty(categoryReportTypes)) {
            List<ReportType> reportType = new ArrayList<>(categoryReportTypes);
            reportType.sort(comp);
            categoriesMap.put(categoryName, reportType);
        }
    }

}
