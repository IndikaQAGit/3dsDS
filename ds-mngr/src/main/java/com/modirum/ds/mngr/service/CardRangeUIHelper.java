package com.modirum.ds.mngr.service;

import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.services.AuditLogService;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.LocalizationService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Misc;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service
public class CardRangeUIHelper {

    private PersistenceService persistenceService = ServiceLocator.getInstance().getPersistenceService();
    private LocalizationService localizationService = ServiceLocator.getInstance().getLocalizationService();
    private AuditLogService auditLogService = ServiceLocator.getInstance().getAuditLogService();
    private IssuerService issuerService = ServiceLocator.getInstance().getIssuerService();

    //TODO should be refactored to use it without BindingResult
    public void saveOrUpdateCardRange(CardRange cardRange, String issuerIdString, BindingResult result, HttpServletRequest request, Locale locale) throws Exception {
        Integer index = Misc.parseInt(request.getParameter("index"));
        Integer issuerId = Misc.parseInt(issuerIdString);
        if (issuerService.isBINRangeMode()) {
            cardRange.setRangeMode();
        }

        cardRange.setIssuerId(issuerId);
        cardRange.setIncludeThreeDSMethodURL(true);


        int binerrs = 0;
        if (issuerService.isBINRangeMode()) {

            String[] values = {cardRange.getStart(), cardRange.getEnd()};
            String[] fields = {"start", "end"};
            String[] names = {"Start", "End"};

            for (int ib = 0; ib < values.length; ib++) {
                if (Misc.isNullOrEmpty(values[ib])) {
                    result.addError(
                            new FieldError("lists.list1[" + index + "]", fields[ib], values[ib], false, null,
                                    null, localizationService.getText("err.required", names[ib],
                                    locale)));
                    binerrs++;
                } else if (!Misc.isNumber(values[ib])) {
                    result.addError(
                            new FieldError("lists.list1[" + index + "]", fields[ib], values[ib], false, null,
                                    null, localizationService.getText("err.invalid",
                                    localizationService.getText(
                                            "err.must.be.number",
                                            names[ib], locale),
                                    locale)));
                    binerrs++;
                } else if (values[ib].length() > IssuerService.cMaxRangeLength ||
                        values[ib].length() < IssuerService.cMinRangeLength) {
                    result.addError(
                            new FieldError("lists.list1[" + index + "]", fields[ib], values[ib], false, null,
                                    null, localizationService.getText("err.invalid.length",
                                    localizationService.getText(
                                            "err.must.be.length",
                                            IssuerService.cMinRangeLength +
                                                    ".." +
                                                    IssuerService.cMaxRangeLength,
                                            locale), locale)));
                    binerrs++;
                }
            }

            if (Misc.isNumber(cardRange.getStart()) && Misc.isNumber(cardRange.getEnd())) {
                BigInteger s = new BigInteger(cardRange.getStart());
                BigInteger e = new BigInteger(cardRange.getEnd());
                if (e.subtract(s).compareTo(BigInteger.valueOf(issuerService.getMinRangeSize())) < 0) {
                    BigInteger min = s.add(BigInteger.valueOf(issuerService.getMinRangeSize()));
                    result.addError(
                            new FieldError("lists.list1[" + index + "]", fields[1], values[1], false, null,
                                    null, localizationService.getText("err.min.allowed",
                                    String.valueOf(min),
                                    locale)));
                }
                if (cardRange.getStart().length() != cardRange.getEnd().length()) {
                    result.addError(
                            new FieldError("lists.list1[" + index + "]", fields[1], values[1], false, null,
                                    null, localizationService.getText("err.invalid.length",
                                    localizationService.getText(
                                            "err.must.be.length",
                                            cardRange.getStart().length(),
                                            locale), locale)));
                }
            }
        } else {
            if (Misc.isNullOrEmpty(cardRange.getBin())) {
                result.addError(
                        new FieldError("lists.list1[" + index + "]", "bin", cardRange.getBin(), false, null,
                                null, localizationService.getText("err.required", "BIN", locale)));

                binerrs++;
            } else if (!Misc.isNumber(cardRange.getBin())) {
                result.addError(
                        new FieldError("lists.list1[" + index + "]", "bin", cardRange.getBin(), false, null,
                                null, localizationService.getText("err.invalid",
                                localizationService.getText(
                                        "err.must.be.number",
                                        "BIN", locale),
                                locale)));

                binerrs++;
            } else if (cardRange.getBin().length() > issuerService.getMaxBINLength() ||
                    cardRange.getBin().length() < issuerService.getMinBINLength()) {
                result.addError(
                        new FieldError("lists.list1[" + index + "]", "bin", cardRange.getBin(), false, null,
                                null, localizationService.getText("err.invalid.length",
                                localizationService.getText(
                                        "err.must.be.length",
                                        issuerService.getMinBINLength() +
                                                ".." +
                                                issuerService.getMaxBINLength(),
                                        locale), locale)));

                binerrs++;
            }
        }

        if (Misc.isNullOrEmpty(cardRange.getType())) {
            result.addError(
                    new FieldError("lists.list1[" + index + "]", "type", cardRange.getType(), false, null, null,
                            localizationService.getText("err.required",
                                    localizationService.getText("text.cardbrand",
                                            locale),
                                    locale)));

            binerrs++;
        }

        if (Misc.isNullOrEmpty(cardRange.getStatus())) {
            result.addError(
                    new FieldError("lists.list1[" + index + "]", "status", cardRange.getStatus(), false, null,
                            null, localizationService.getText("err.required",
                            localizationService.getText(
                                    "text.ar.status", locale),
                            locale)));

            binerrs++;
        }

        if (Misc.isNullOrEmpty(cardRange.getName())) {
            result.addError(
                    new FieldError("lists.list1[" + index + "]", "name", cardRange.getName(), false, null, null,
                            localizationService.getText("err.required",
                                    localizationService.getText("text.cardname",
                                            locale),
                                    locale)));

            binerrs++;
        } else if (cardRange.getName().length() > 32) {
            result.addError(
                    new FieldError("lists.list1[" + index + "]", "name", cardRange.getName(), false, null, null,
                            localizationService.getText("err.invalid.length",
                                    localizationService.getText(
                                            "err.must.be.length", "1..32",
                                            locale), locale)));
        }

        String fieldName = issuerService.isBINRangeMode() ? "start" : "bin";
        if (binerrs == 0) {
            Issuer issuer = issuerService.getIssuerById(issuerId);
            List<CardRange> existingDuplicateRanges = issuerService
                    .getExistingCardRangesPerPaymentSystem(cardRange.getId(), cardRange.getBinSt(), issuer.getPaymentSystemId());
            if (CollectionUtils.isNotEmpty(existingDuplicateRanges)) {
                String duplicateRangeErrKey = "err.duplicate.range";
                for (CardRange range : existingDuplicateRanges) {
                    if (!range.getIssuerId().equals(issuer.getId())) {
                        duplicateRangeErrKey = "err.other.issuer.overlaping.value";
                        break;
                    }
                }
                result.addError(new FieldError("lists.list1[" + index + "]", fieldName, cardRange.getBinSt(), false,
                                null, null, localizationService.getText(duplicateRangeErrKey, locale)));
            }

            if (StringUtils.isNotEmpty(cardRange.getName()) && issuer.getPaymentSystemId() != null &&
                    issuerService.isCardRangeNameExistingUnderPaymentSystem(cardRange.getName(), cardRange.getId(),
                            issuer.getPaymentSystemId())) {
                result.addError(new FieldError("lists.list1[" + index + "]", "name", cardRange.getName(), false, null,
                        null, localizationService.getText("err.duplicate.value", locale)));
            }
        }

        if (!result.hasErrors()) {
            cardRange.setLastMod(new Date());
            persistenceService.saveOrUpdate(cardRange);
        }
    }

    public void deleteCardRange(CardRange cardRange, int issuerId) throws Exception {
        cardRange.setIssuerId(issuerId);
        persistenceService.delete(cardRange);
        auditLogService.logDelete(AuthorityUtil.getUser(), cardRange);
    }
}
