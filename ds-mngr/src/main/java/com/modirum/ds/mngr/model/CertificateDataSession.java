package com.modirum.ds.mngr.model;

import com.modirum.ds.db.model.CertificateData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CertificateDataSession {
    private CertificateData certificateData;
    private CSR csr;
}
