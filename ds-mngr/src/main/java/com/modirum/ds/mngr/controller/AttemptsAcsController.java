package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.PaymentSystemConstants;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.model.AcsSearcher;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.mngr.support.Helpers;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/issuers")
public class AttemptsAcsController extends BaseController {
    private IssuerService issuerService = ServiceLocator.getInstance().getIssuerService();

    @GetMapping(value = "/attemptsACSList.html")
    public ModelAndView getAacsListPage(HttpServletRequest request,
            @ModelAttribute(value = "searcher") AcsSearcher acsSearcher) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.issuerView)) {
            return unauthorized(request);
        }
        setDefaultSearchPaginationValues(acsSearcher);
        ModelAndView modelAndView = new ModelAndView("issuers/attemptsACSList");
        modelAndView.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));
        return modelAndView;
    }

    private void setDefaultSearchPaginationValues(AcsSearcher acsSearcher) {
        if (acsSearcher.getStart() == null) {
            acsSearcher.setStart(0);
        }
        if (acsSearcher.getLimit() == null) {
            acsSearcher.setLimit(25);
        }
        if (StringUtils.isEmpty(acsSearcher.getOrderDirection())) {
            acsSearcher.setOrderDirection("desc");
        }
    }

    @PostMapping(value = "/attemptsACSList.html")
    public ModelAndView searchAACS(HttpServletRequest request,
            @ModelAttribute(value = "searcher") AcsSearcher acsSearcher, BindingResult result) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.issuerView)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("issuers/attemptsACSList");
        modelAndView.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));

        if (acsSearcher.getPaymentSystemId() != null &&
            (paymentSystemSearchMap() == null || paymentSystemSearchMap().get(acsSearcher.getPaymentSystemId().toString()) == null)) {
            result.addError(new FieldError("searcher", "paymentSystemId", getLocalizedMessage("err.invalid.value", "within the dropdown selection only")));
        }

        if (result.hasErrors()) {
            return modelAndView;
        }

        if (PaymentSystemConstants.UNLIMITED.equals(acsSearcher.getPaymentSystemId())) {
            acsSearcher.setPaymentSystemId(null);
        }
        setDefaultSearchPaginationValues(acsSearcher);
        modelAndView.addObject("foundAacs", issuerService.searchAttemptsACS(acsSearcher));
        return modelAndView;
    }

    @GetMapping(value = "/attemptsACSEdit.html")
    public ModelAndView getEditPage(HttpServletRequest request,
            @RequestParam(value = "id", required = false) String id) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.issuerEdit)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("issuers/attemptsACSEdit");
        ACSProfile attemptsAcs;
        if (id != null) {
            attemptsAcs = issuerService.getAttemptsACSById(Integer.valueOf(id));
            if (!AuthorityUtil.isAccessAllowed(attemptsAcs, Roles.issuerView)) {
                return unauthorized(request);
            }
            modelAndView.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(attemptsAcs.getPaymentSystemId())));
        } else {
            attemptsAcs = new ACSProfile();
        }
        modelAndView.addObject("attemptsACS", attemptsAcs);
        modelAndView.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));
        modelAndView.addObject("inCertificateList", inCertificateMap());
        modelAndView.addObject("protoVersionList", protocolVersionsMap(false));
        return modelAndView;
    }

    private void trimACSAttempt(ACSProfile attemptAcs) {
        attemptAcs.setName(StringUtils.trim(attemptAcs.getName()));
        attemptAcs.setRefNo(StringUtils.trim(attemptAcs.getRefNo()));
        attemptAcs.setURL(StringUtils.trim(attemptAcs.getURL()));
        attemptAcs.setThreeDSMethodURL(StringUtils.trim(attemptAcs.getThreeDSMethodURL()));
    }

    @PostMapping(value = "/attemptsACSEdit.html")
    public ModelAndView saveAttemptsAcs(HttpServletRequest request,
            @ModelAttribute("attemptsACS") ACSProfile attemptsAcs,
            BindingResult result) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.issuerEdit)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("issuers/attemptsACSEdit");
        modelAndView.addObject("attemptsACS", attemptsAcs);
        modelAndView.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));
        modelAndView.addObject("inCertificateList", inCertificateMap());
        modelAndView.addObject("protoVersionList", protocolVersionsMap(false));

        boolean isUpdateAttemptsAcs = attemptsAcs.getId() != null;
        trimACSAttempt(attemptsAcs);

        if (StringUtils.isNotEmpty(attemptsAcs.getURL())) {
            if (!Helpers.isValidUrl(attemptsAcs.getURL())) {
                result.addError(new FieldError("attemptsAcs", "URL", attemptsAcs.getURL(), false, null, null,
                        getLocalizedMessage("err.invalid.url")));
            }
        }

        if (StringUtils.isNotEmpty(attemptsAcs.getThreeDSMethodURL())) {
            if (!Helpers.isValidUrl(attemptsAcs.getThreeDSMethodURL())) {
                result.addError(new FieldError("attemptsAcs", "threeDSMethodURL", attemptsAcs
                        .getThreeDSMethodURL(), false, null, null, getLocalizedMessage("err.invalid.url")));
            }
        }

        if (isUpdateAttemptsAcs) {
            ACSProfile oldAacs = issuerService.getAttemptsACSById(attemptsAcs.getId());
            if (oldAacs == null) {
                if (!AuthorityUtil.isAccessAllowed(oldAacs)) {
                    return unauthorized(request);
                }
            }
            attemptsAcs.setPaymentSystemId(oldAacs.getPaymentSystemId());
            modelAndView.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(attemptsAcs.getPaymentSystemId())));
        } else {
            if (attemptsAcs.getPaymentSystemId() == null) {
                result.addError(
                        new FieldError("attemptsACS", "paymentSystemId", null, false, null, null,
                                       getLocalizedMessage("err.required", "general.paymentSystem")));
            }
        }

        if (attemptsAcs.getPaymentSystemId() != null &&
            !paymentSystemComponentMap().containsKey(String.valueOf(attemptsAcs.getPaymentSystemId()))) {
            result.addError(new FieldError("attemptsAcs", "paymentSystemId", getLocalizedMessage("err.invalid.value")));
        }

        if (StringUtils.isEmpty(attemptsAcs.getName())) {
            result.addError(
                    new FieldError("attemptsACS", "name", null, false, null, null,
                                   getLocalizedMessage("err.required", "text.name")));
        } else if (attemptsAcs.getPaymentSystemId() != null &&
            StringUtils.isNotEmpty(attemptsAcs.getName())) {
            List<ACSProfile> acsProfilesList = issuerService.getAcsProfileByNameAndPaymentSystem(attemptsAcs.getName(), attemptsAcs.getPaymentSystemId());
            if (CollectionUtils.isNotEmpty(acsProfilesList)) {
                for (ACSProfile aacsProfile : acsProfilesList) {
                    if (!aacsProfile.getId().equals(attemptsAcs.getId())) {
                        result.addError(new FieldError("attemptsAcs", "name", attemptsAcs.getName(), false, null, null,
                                                       getLocalizedMessage("err.duplicate.value", "text.name")));
                    }
                }
            }
        }

        if (StringUtils.isEmpty(attemptsAcs.getRefNo())) {
            result.addError(
                    new FieldError("attemptsACS", "refNo", null, false, null, null,
                                   getLocalizedMessage("err.required", "text.acsprofile.refNo")));
        } else if (attemptsAcs.getRefNo().length() > 150) {
            result.addError(
                    new FieldError("attemptsAcs", "refNo", attemptsAcs.getRefNo(), false, null, null,
                                   getLocalizedMessage("err.invalid.length", getLocalizedMessage("err.must.be.length", "<=150"))));
        }

        if (StringUtils.isEmpty(attemptsAcs.getURL())) {
            result.addError(new FieldError("attemptsACS", "URL", null, false, null, null,
                                           getLocalizedMessage("err.required", "text.acsprofile.url")));
        } else if (attemptsAcs.getURL().length() > 1024) {
            result.addError(new FieldError("attemptsACS", "URL", attemptsAcs.getURL(), false, null, null,
                                           getLocalizedMessage("err.must.be.length", "<=1024")));
        }

        if (StringUtils.isNotEmpty(attemptsAcs.getThreeDSMethodURL()) &&
            attemptsAcs.getThreeDSMethodURL().length() > 1024) {
            result.addError(new FieldError("attemptsACS", "threeDSMethodURL", attemptsAcs.getThreeDSMethodURL(), false,
                    null, null,
                                           getLocalizedMessage("err.must.be.length", "<=1024")));
        }
        
        if (!sslCertsManagedExternally() && Misc.isNullOrEmpty(attemptsAcs.getInClientCert())) {
            result.addError(
                    new FieldError("attemptsAcs", "inClientCert", attemptsAcs.getOutClientCert(), false, null, null,
                                   getLocalizedMessage("err.required", getLocalizedMessage("text.acsprofile.in.clientcertid"))));
        }

        if (StringUtils.isEmpty(attemptsAcs.getStatus())) {
            result.addError(new FieldError("attemptsAcs", "status",
                                           getLocalizedMessage("err.required", "text.status")));
        }

        if (result.hasErrors()) {
            return modelAndView;
        } else {
            Date now = new Date();
            attemptsAcs.setLastMod(now);
            persistenceService.saveOrUpdate(attemptsAcs);
            if (isUpdateAttemptsAcs) {
                ACSProfile oldAttemptsAcs = persistenceService.getPersistableById(attemptsAcs.getId(), ACSProfile.class);
                auditLogService.logUpdate(AuthorityUtil.getUser(), oldAttemptsAcs, attemptsAcs, null);
            } else {
                auditLogService.logInsert(AuthorityUtil.getUser(), attemptsAcs, null);
            }
        }

        return super.redirect("attemptsACSView.html?id=" + attemptsAcs.getId());
    }

    @GetMapping(value = "/attemptsACSView.html")
    public ModelAndView viewPage(HttpServletRequest request,
            @RequestParam(value = "id", required = false) String id) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.issuerView)) {
            return unauthorized(request);
        }

        ACSProfile attemptsAcs = issuerService.getAttemptsACSById(Integer.valueOf(id));
        if (!AuthorityUtil.isAccessAllowed(attemptsAcs, Roles.issuerView)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("issuers/attemptsACSView");
        modelAndView.addObject("attemptsACS", attemptsAcs);
        modelAndView.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));
        modelAndView.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(attemptsAcs.getPaymentSystemId())));
        return modelAndView;
    }
}