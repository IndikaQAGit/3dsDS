package com.modirum.ds.mngr.support.csv;

import java.io.Writer;
import java.util.List;

public class CsvParams<T> {
    private Writer writer;
    private String[] headers;
    private List<T> records;
    private CsvMapper<T> csvMapper;
    private char delimiter;

    public Writer getWriter() {
        return writer;
    }

    public CsvParams<T> setWriter(Writer writer) {
        this.writer = writer;
        return this;
    }

    public String[] getHeaders() {
        return headers;
    }

    public CsvParams<T> setHeaders(String[] headers) {
        this.headers = headers;
        return this;
    }

    public List<T> getRecords() {
        return records;
    }

    public CsvParams<T> setRecords(List<T> records) {
        this.records = records;
        return this;
    }

    public CsvMapper<T> getCsvMapper() {
        return csvMapper;
    }

    public CsvParams<T> setCsvMapper(CsvMapper<T> csvMapper) {
        this.csvMapper = csvMapper;
        return this;
    }

    public char getDelimiter() {
        return delimiter;
    }

    public CsvParams<T> setDelimiter(char delimiter) {
        this.delimiter = delimiter;
        return this;
    }
}
