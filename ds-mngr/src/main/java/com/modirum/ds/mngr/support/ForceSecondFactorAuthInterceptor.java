package com.modirum.ds.mngr.support;

import com.modirum.ds.db.model.User;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.access.WebInvocationPrivilegeEvaluator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ForceSecondFactorAuthInterceptor extends org.springframework.web.servlet.handler.HandlerInterceptorAdapter {

    transient static Logger log = LoggerFactory.getLogger(ForceSecondFactorAuthInterceptor.class);

    private String secondFactorUri = "/secondFactor/secondFactor.html";
    private String allowPath = "/secondFactor/";

    @Autowired
    WebInvocationPrivilegeEvaluator webInvocationPrivilegeEvaluator;
    Authentication noOne;

    @Autowired
    public void setWebInvocationPrivilegeEvaluator(WebInvocationPrivilegeEvaluator webInvocationPrivilegeEvaluator) {
        this.webInvocationPrivilegeEvaluator = webInvocationPrivilegeEvaluator;
    }

    public ForceSecondFactorAuthInterceptor() {
        List<GrantedAuthority> empty = new java.util.ArrayList<GrantedAuthority>();
        empty.add(new SimpleGrantedAuthority("IS_AUTHENTICATED_ANONYMOUSLY"));
        org.springframework.security.core.userdetails.User nobody = new org.springframework.security.core.userdetails.User(
                "anon", "anon", false, false, false, false, empty);
        noOne = new AnonymousAuthenticationToken("anon", nobody, empty);
		/*accessEvaluator=
				new org.springframework.security.web.access.DefaultWebInvocationPrivilegeEvaluator(
						new org.springframework.security.web.access.intercept.FilterSecurityInterceptor());
		
		SecurityContextHolder.getContextHolderStrategy().getContext().
		*/
    }

    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws IOException, ServletException {

		/*		if (handler!=null && 
					(handler.getClass().getCanonicalName().indexOf("com.modirum.authserver.")>=0  // dont protect static stuff only controllers
					// seems this has been changed in sprin lately that the object no longer controller by HandlerMethod
					|| 
					handler instanceof org.springframework.web.method.HandlerMethod &&
					   ((org.springframework.web.method.HandlerMethod)handler).getBean().getClass()
					   .getCanonicalName().indexOf("com.modirum.authserver.")>=0
					) 
					&& req.getRequestURI().indexOf(req.getContextPath()+allowPath)!=0)
			*/
        String ctxPath = req.getContextPath();
        String uri = req.getRequestURI();
        if (ctxPath != null && ctxPath.length() > 0) {
            uri = uri.substring(ctxPath.length());
        }

        if (!webInvocationPrivilegeEvaluator.isAllowed(uri, noOne) &&
            req.getRequestURI().indexOf(req.getContextPath() + allowPath) != 0) {

            User user = AuthorityUtil.getUser();
            if (user != null && Misc.isNotNullOrEmpty(user.getFactor2Authmethod()) && !user.isFactor2Passed()) {
                res.sendRedirect(req.getContextPath() + secondFactorUri);
                return false;
            }
        }

        return true;
    }

    public String getSecondFactorUri() {
        return secondFactorUri;
    }

    public void setSecondFactorUri(String secondFactorUri) {
        this.secondFactorUri = secondFactorUri;
    }

    public String getAllowPath() {
        return allowPath;
    }

    public void setAllowPath(String allowPath) {
        this.allowPath = allowPath;
    }

}
