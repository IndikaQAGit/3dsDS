package com.modirum.ds.mngr.controller;

import com.modirum.ds.jdbc.core.model.DSInit;
import com.modirum.ds.model.ProductInfo;
import com.modirum.ds.enums.DSInitKey;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.mngr.support.CustomAuthenticationProvider;
import com.modirum.ds.mngr.support.CustomUsernamePasswordAuthenticationFilter;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.ConfigService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.security.auth.x500.X500Principal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.Date;

@Controller
public class MainController extends BaseController {

    private final transient Logger log = LoggerFactory.getLogger(MainController.class);

    public final static String cRemoteCertificateAttr = "remoteCertificate";
    public final static String cRemoteCertificateSDNAttr = "remoteCertificateSDN";

    @Autowired
    CustomUsernamePasswordAuthenticationFilter upaf;

    @Autowired
    CustomAuthenticationProvider ap;

    @RequestMapping({"/login/", "/login/*"})
    public ModelAndView getLoginPage(@RequestParam(value = "loginError", required = false) Integer loginError,
                                     @RequestParam(value = "initAdmin", required = false) String initAdmin,
                                     @RequestParam(value = "initAdminPwd", required = false) String initAdminPwd,
                                     @RequestParam(value = "initAdminVerifyPwd", required = false) String initAdminVerifyPwd) {

        ModelAndView mav = new ModelAndView();

        // initialize necessary attributes
        Exception error = (Exception) getSessionAttribute("SPRING_SECURITY_LAST_EXCEPTION");
        if (error != null) {
            mav.addObject("errorMessage", error.getMessage());
            removeSessionAttribute("SPRING_SECURITY_LAST_EXCEPTION");
        }
        if (application.getCertAuthUri() != null) {
            mav.addObject("certAuthUri", application.getCertAuthUri());
        }

        // do creation of initial admin account
        if (initAdmin != null) {
            return createAdminAccount(mav, initAdmin, initAdminPwd, initAdminVerifyPwd);
        }

        // return login view
        if (!isEnableInitAdmin()) {
            mav.setViewName("login/login");
            mav.addObject("loginError", loginError);
            return mav;
        }

        // return create initial admin account view
        return initCreateAdmin(mav, null);
    }

    @RequestMapping({"/ssllogin.html", "/ssllogin2.html"})
    public ModelAndView sslLogin(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String userName = req.getParameter("username");
        log.info("SSL login executing, username " + userName);
        ConfigService settingService = cnfs;
        boolean viaProxy = "true".equals(settingService.getStringSetting(DsSetting.MNGR_SSL_AUTHPROXY_ENABLED.getKey()));

        String sdn = req.getRemoteUser();
        String at = req.getAuthType();
        Object o = req.getAttribute("javax.servlet.request.X509Certificate");
        X509Certificate cert = null;

        if (o != null) {
            X509Certificate[] certs = (X509Certificate[]) o;
            cert = certs[0];
            sdn = cert.getSubjectX500Principal().toString();
            log.info("Remote client cert SDN (tomacat): '" + sdn + "'");
            if (log.isDebugEnabled()) {
                log.debug("Remote client cert (tomacat): " + Base64.encode(certs[0].getEncoded()) + "");
            }

            req.setAttribute(cRemoteCertificateAttr, cert);
        } else if (viaProxy) { // trusting proxy variables
            String proxyType = settingService.getStringSetting(DsSetting.MNGR_SSL_AUTHPROXY_TYPE.getKey());
            String cd = null;
            if ("pound".equals(proxyType)) {
                cd = req.getHeader("X-SSL-certificate");
                if (cd != null) {
                    at = "CLIENT_CERT";
                }
            } else if ("nginx".equals(proxyType)) {
                cd = req.getHeader("X-SSL-Client-Cert");
                if (cd != null) {
                    at = "CLIENT_CERT";
                }
            } else { // apache
                cd = req.getHeader("SSL_CLIENT_CERT");
                if (sdn != null && "SUCCESS".equals(req.getHeader("SSL_CLIENT_VERIFY"))) {
                    at = "CLIENT_CERT";
                }
            }

            if (cd != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Remote client cert: '" + cd + "'");
                }
                cd = Misc.replace(cd, "-----BEGIN CERTIFICATE-----", "");
                cd = Misc.replace(cd, "-----END CERTIFICATE-----", "").trim();

                X509Certificate[] certs = KeyService.parseX509Certificates(Base64.decode(cd), "X509");
                if (certs != null) {
                    cert = certs[0];
                    req.setAttribute(cRemoteCertificateAttr, cert);
                    sdn = certs[0].getSubjectX500Principal().getName(X500Principal.RFC2253);
                    log.info("Remote client cert SDN (proxy " + Misc.defaultString(proxyType) + ") '" + sdn + "'");
                }
            } else {
                log.warn("Remote client cert data was not present (via proxy " + Misc.defaultString(proxyType) + ")");
            }
        }

        req.setAttribute(cRemoteCertificateSDNAttr, sdn);

        if (cert != null) {
            UsernamePasswordAuthenticationToken upt = new UsernamePasswordAuthenticationToken(userName, cert);
            try {
                Authentication auth = ap.authenticate(upt);
                upaf.successfulAuthentication(req, resp, null, auth);
                return null;
            } catch (AuthenticationException e) {
                req.setAttribute("SPRING_SECURITY_LAST_EXCEPTION", e);
                if (req.getSession(false) != null) {
                    req.getSession(false).setAttribute("SPRING_SECURITY_LAST_EXCEPTION", e);
                }
                log.warn("Cert authentication failed ", e);
                upaf.unsuccessfulAuthentication(req, resp, e);
                return null;
            }
        }
        log.warn("Cert authentication failed sdn is missing sdn=" + sdn + " or not cert authenticated at=" + at);
        Exception bce = new BadCredentialsException("No certificate supplied (try restart browser if Smart Card is used)");
        req.setAttribute("SPRING_SECURITY_LAST_EXCEPTION", bce);
        if (req.getSession(false) != null) {
            req.getSession(false).setAttribute("SPRING_SECURITY_LAST_EXCEPTION", bce);
        }

        if (resp.isCommitted()) {
            return null;
        }

        return super.redirect("login/login.html");
    }

    @RequestMapping({"/unauthorized/", "/unauthorized/*"})
    public ModelAndView getUnauthorizedPage(HttpServletRequest request, @RequestParam(value = "error", required = false) String error) {
        if (error != null) {
            request.setAttribute("error", error);
        }
        return super.unauthorized(request);
    }

    @RequestMapping({"/errors/acces-denied.html"})
    public ModelAndView getAccesDeniedPage(HttpServletRequest request, @RequestParam(value = "error", required = false) String error) {
        ModelAndView mav = new ModelAndView("errors/access-denied");
        String orig = (String) request.getAttribute("javax.servlet.forward.request_uri");
        if (request.getSession(false) == null || orig != null && orig.endsWith("/login")) {
            error = "Access denied, check if You have session cookies enabled";
        }
        if (orig == null) {
            orig = request.getRequestURI();
        }
        if (error == null) {
            error = "Access denied, Your privileges/invalid data did not allowed to You to access the this page";
        }

        User user = AuthorityUtil.getUser();
        auditLogService.logAction(user, null, DSModel.AuditLog.Action.UNAUTH,
                                  request.getMethod() + " URI=" + orig + "; IP=" + request.getRemoteAddr() +
                                  (user != null ? " user: " + user.getLoginname() + "/" + user.getId() : "") + " " +
                                  error);

        mav.addObject("error", error);
        return mav;
    }


    @RequestMapping({"/errors/invalid-request.html"})
    public ModelAndView getInvalidRequestPage(@RequestParam(value = "error", required = false) String error) {
        ModelAndView mav = new ModelAndView("errors/invalid-request");
        if (error == null) {
            error = "The invalid request was received";
        }

        mav.addObject("error", error);
        return mav;
    }

    @RequestMapping(value = {"/main/", "/main/main.html"})
    public ModelAndView getMainPage(HttpServletRequest request, @RequestParam(value = "cmd", required = false) String cmd) {
        long start = System.currentTimeMillis();

        if (request.getSession(false) == null) {
            return super.redirect("/login/login.html");
        }

        User user = AuthorityUtil.getUser();
        if (user == null) {
            return super.redirect("/login/login.html");
        }


        ModelAndView mav = new ModelAndView("main/main");

        if (request.getParameter("tz") != null) {
            super.setTimeZone(request.getParameter("tz"));
            mav.addObject("timeZone", super.getTimeZone());
        }

        if (log.isDebugEnabled()) {
            log.debug("Main controller getMainPage executed in " + (System.currentTimeMillis() - start) + " in ms");
        }


        return mav;
    }


    @RequestMapping("/main/licenseinfo.html")
    public ModelAndView licenseinfo(@ModelAttribute("li") @Valid ProductInfo li, BindingResult result, HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "cmd", required = false) String cmd) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.userView)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("main/licenseinfo", result.getModel());

        ProductInfo licensedb = super.productInfoService.loadProductInfo();
        String buildVersion = super.persistenceService.getStringSetting(DsSetting.DS_BUILD_VERSION.getKey());
        java.util.List<Setting> runtimeInfo = super.persistenceService.getSettingsByIdStart("DS.instanceinfo.%", null,
                                                                                            null);
        mav.addObject("runtimeInfo", runtimeInfo);
        mav.addObject("helpText", localizationService.getText("general.help", locale()));
        mav.addObject("issuedHelpTooltip", getLocalizedMessage("text.tooltip.licenseinfo.issued"));

        if (licensedb == null) {
            mav.addObject("error", "Error: License data does not exist");
        } else if ("edit".equals(cmd)) {
            if (!AuthorityUtil.hasRole(Roles.userEdit)) {
                return unauthorized(request);
            }
            mav.addObject("cmd", "edit");
            // mav.addObject("li", licensedb);
            li.setLicensee(licensedb.getLicensee());
            li.setLicenseKey(licensedb.getLicenseKey());
            li.setExpiration(licensedb.getExpiration());
            li.setIssued(licensedb.getIssued());
            li.setProduct(licensedb.getProduct());
            li.setVersion(licensedb.getVersion());
            li.setMaxMerchants(licensedb.getMaxMerchants());
            li.setExtensions(licensedb.getExtensions());
        } else if ("save".equals(cmd)) {
            if (!AuthorityUtil.hasRole(Roles.userEdit)) {
                return unauthorized(request);
            }

            if (Misc.isNullOrEmpty(li.getExpiration())) {
                result.addError(new FieldError("li", "expiration", li.getExpiration(), false, null, null,
                                               "Must be date yyyy-MM-dd or 'Never' or 'never'."));
            } else if (!"Never".equalsIgnoreCase(li.getExpiration())) {
                try {
                    DateUtil.parseDate(li.getExpiration(), "yyyy-MM-dd");
                } catch (Exception e) {
                    result.addError(new FieldError("li", "expiration", li.getExpiration(), false, null, null,
                                                   "Must be date yyyy-MM-dd or 'Never'."));
                }
            }
            if (Misc.isNullOrEmpty(li.getIssued())) {
                result.addError(new FieldError("li", "issued", li.getIssued(), false, null, null,
                        getLocalizedMessage("err.required", getLocalizedMessage("text.li.issued"))));
            } else {
                try {
                    DateUtil.parseDate(li.getIssued(), "yyyy-MM-dd");
                } catch (Exception e) {
                    result.addError(new FieldError("li", "issued", li.getIssued(), false, null, null,
                        getLocalizedMessage("err.invalid.date.format", "yyyy-MM-dd")));
                }
            }

            if (Misc.isNullOrEmpty(li.getLicensee())) {
                result.addError(
                        new FieldError("li", "licensee", li.getLicensee(), false, null, null, "Licensee is required"));
            }

            if (Misc.isNullOrEmpty(li.getLicenseKey())) {
                result.addError(new FieldError("li", "licenseKey", li.getLicenseKey(), false, null, null,
                                               "License key is required"));
            }

            if (Misc.isNullOrEmpty(li.getProduct())) {
                result.addError(new FieldError("li", "product", li.getProduct(), false, null, null,
                                               "Product name is required"));
            }
            if (Misc.isNullOrEmpty(li.getVersion())) {
                result.addError(new FieldError("li", "version", li.getVersion(), false, null, null,
                                               "Product version is required"));
            }
            if (Misc.isNullOrEmpty(li.getMaxMerchants().trim())) {
                result.addError(new FieldError("li", "maxMerchants", li.getMaxMerchants(), false, null, null,
                                               "Max merchants must be Integer or 'Unlimited' or 'unlimited' as in your license."));
            } else if (!Misc.isInt(li.getMaxMerchants()) && !"unlimited".equalsIgnoreCase(li.getMaxMerchants())) {
                result.addError(new FieldError("li", "maxMerchants", li.getMaxMerchants(), false, null, null,
                                               "Max merchants must be Integer or 'Unlimited' or 'unlimited' as in your license."));
            }

            if (!result.hasErrors()) {
                licensedb.setLicensee(li.getLicensee());
                licensedb.setLicenseKey(li.getLicenseKey());
                licensedb.setExpiration(li.getExpiration());
                licensedb.setIssued(li.getIssued());
                licensedb.setProduct(li.getProduct());
                licensedb.setVersion(li.getVersion());
                licensedb.setMaxMerchants(li.getMaxMerchants());
                licensedb.setExtensions(li.getExtensions());

                try {
                    super.productInfoService.updateProductInfo(licensedb);
                    li = licensedb;
                    mav.addObject("cmd", "view");
                    mav.addObject("msg", "Changes saved sucessfully");
                } catch (Exception e) {
                    mav.addObject("cmd", "edit");
                    log.error("License info save failed", e);
                    mav.addObject("errormsg", "Save failed, retry " + e);
                }

                mav.addObject("li", li);
            } else {
                mav.addObject("cmd", "edit");
                mav.addObject("errormsg", "Errors found in form data");
                mav.addObject("li", li);
            }

        } // save
        else {
            mav.addObject("li", licensedb);
        }

        return mav;
    }

    private ModelAndView initCreateAdmin(ModelAndView mav, String errorMsg) {
        mav.setViewName("login/init");
        mav.addObject("initError", errorMsg);
        mav.addObject("initialLoginInfo", localizationService.getText("text.initial.admin.login.info", locale()));
        return mav;
    }

    private ModelAndView createAdminAccount(ModelAndView mav, String initAdmin, String initAdminPwd, String initAdminVerifyPwd) {

        if (StringUtils.isEmpty(initAdmin) || StringUtils.isEmpty(initAdminPwd) || StringUtils.isEmpty(initAdminVerifyPwd)) {
            return initCreateAdmin(mav, localizationService.getText("text.initial.admin.missing", locale()));
        }

        if (!initAdminPwd.equals(initAdminVerifyPwd)) {
            return initCreateAdmin(mav, localizationService.getText("err.password.mismatch", locale()));
        }

        User user = new User();
        user.setLoginname(initAdmin);
        user.setPassword(CryptoService.sha256WithUniquePredictable(initAdminPwd));
        user.setLastPassChangeDate(new Date());
        user.setStatus(DSModel.User.Status.ACTIVE);
        user.setRoles(Roles.getAllRoles());

        DSInit enableInitAdmin = DSInit.builder()
            .initKey(DSInitKey.ENABLE_INIT_ADMIN.getKey())
            .initValue(Boolean.FALSE.toString())
            .lastModifiedBy(user.getLoginname())
            .build();

        try {
            // save new user
            persistenceService.saveOrUpdate(user);
            auditLogService.logInsert(user, user, "Initial admin account " + user.getLoginname());

            // save enableInitAdmin setting to indicate that initialize admin page should be disabled
            dsInitService.saveOrUpdate(enableInitAdmin);
            auditLogJdbcService.logUpdate(enableInitAdmin, user);
        } catch (Exception e) {
            log.error("Error occurred when trying to save user and setting: ", e);
            return initCreateAdmin(mav, localizationService.getText("text.initial.admin.error", locale()));
        }

        // account successfully created
        mav.addObject("initAdminDone", localizationService.getText("text.initial.admin.success", locale()));
        mav.setViewName("login/login");
        return mav;
    }

    private boolean isEnableInitAdmin() {
        DSInit enableInitAdmin = dsInitService.getByInitKey(DSInitKey.ENABLE_INIT_ADMIN.getKey());
        return enableInitAdmin != null && enableInitAdmin.getInitValue().equalsIgnoreCase(Boolean.TRUE.toString());
    }
}
