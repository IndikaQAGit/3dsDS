package com.modirum.ds.mngr.model.ui.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DS-Admin 3DS Server profile search-list filter. Filled from search panel, used to lookup 3DS Servers from DB.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TDSServerSearchFilter {
    private String name;
    private Integer acquirerId;
    private String requestorID;
    private String operatorID;
    private Long inClientCert;
    private String status;
    private Integer paymentSystemId;
    private Integer start;
    private Integer limit;
    private Integer total;
    private String order;
    private String orderDirection;
}