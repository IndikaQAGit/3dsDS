package com.modirum.ds.mngr.support;

/**
 * Contains Levenshtein's string distance algorithm.
 * <p>
 * The distance algorithm was copied from http://rosettacode.org/wiki/Levenshtein_distance#Java Created by vir on 11.9.2015.
 */
public class Levenshtein {
    /**
     * The standard Levenshtein distance algorithm.
     *
     * @param a
     * @param b
     * @return distance between given strings
     */
    public static int distance(String a, String b, boolean bycase) {
        if (a == null || b == null) {
            return Integer.MAX_VALUE;
        }

        if (!bycase) {
            a = a.toLowerCase();
            b = b.toLowerCase();
        }
        // i == 0
        int[] costs = new int[b.length() + 1];
        for (int j = 0; j < costs.length; j++) {
            costs[j] = j;
        }
        for (int i = 1; i <= a.length(); i++) {
            // j == 0; nw = lev(i - 1, j)
            costs[0] = i;
            int nw = i - 1;
            for (int j = 1; j <= b.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]),
                                  a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }
        return costs[b.length()];
    }

    /**
     * Calculate Levenshtein distance between given strings, limiting comparison to the shortest common length of given strings.
     *
     * @param a
     * @param b
     * @return distance between given strings compared with shortest common length only
     */
    public static int sameLengthDistance(String a, String b, boolean bycase) {
        if (a == null || b == null) {
            return Integer.MAX_VALUE;
        }
        return distance(b.length() < a.length() ? a.substring(0, b.length()) : a, b, bycase);
    }

}
