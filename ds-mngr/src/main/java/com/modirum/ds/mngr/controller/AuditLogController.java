package com.modirum.ds.mngr.controller;

import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.enums.PaymentSystemConstants;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.AuditLog;
import com.modirum.ds.db.model.AuditLogSearcher;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.model.Processor;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.model.TDSMessageData;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.db.model.User;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/auditlogs")
public class AuditLogController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(AuditLogController.class);

    private static final Class<?>[] oClasses = {TDSRecord.class, TDSMessageData.class, User.class, Issuer.class,
            Acquirer.class, Merchant.class, TDSServerProfile.class, Processor.class, Setting.class};

    @ModelAttribute("events")
    public Map<String, String> events() {
        Map<String, String> events = new LinkedHashMap<>();
        Field[] sFields = DSModel.AuditLog.Action.class.getFields();
        events.put("", "");
        for (int i = 0; i < sFields.length; i++) {
            try {
                if (Modifier.isPublic(sFields[i].getModifiers())) {
                    String st = (String) sFields[i].get(null);
                    events.put(st, localizationService.getText("al.action." + st, locale()));
                }
            } catch (Exception e) {
                LOG.error("al events read error", e);
            }
        }

        return events;
    }

    @ModelAttribute("objectClasses")
    public Map<String, String> objectClasses() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("", "");
        for (int i = 0; i < oClasses.length; i++) {
            try {
                String st = oClasses[i].getSimpleName();
                map.put(st, localizationService.getText("objectClass." + st, locale()));
            } catch (Exception e) {
                LOG.error("au objecttypes read error", e);
            }
        }
        return map;
    }

    @RequestMapping("/auditlogs.html")
    public ModelAndView audidlogs(HttpServletRequest request,
                                  @ModelAttribute(value = "s") AuditLogSearcher searcher,
                                  BindingResult result) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.auditLogView)) {
            return unauthorized(request);
        }
        long start = System.currentTimeMillis();

        ModelAndView mav = new ModelAndView("auditlogs/auditlogs");

        if (Misc.isNotNullOrEmpty(request.getParameter("search"))) {

            if (searcher.getLogDateFrom() != null && Misc.isInt(request.getParameter("logDateFromHH")) &&
                Misc.parseInt(request.getParameter("logDateFromHH")) >= 0 &&
                Misc.parseInt(request.getParameter("logDateFromHH")) < 24) {
                searcher.setLogDateFrom(new java.util.Date(searcher.getLogDateFrom().getTime() + 3600000L *
                                                                                                 Misc.parseInt(
                                                                                                         request.getParameter(
                                                                                                                 "logDateFromHH"))));
                mav.addObject("logDateFromHH", request.getParameter("logDateFromHH"));
            }

            if (!paymentSystemSearchMap().containsKey(searcher.getPaymentSystemId().toString())) {
                searcher.setPaymentSystemId(currentUser().getPaymentSystemId());
            }

            if (PaymentSystemConstants.UNLIMITED.equals(searcher.getPaymentSystemId())) {
                searcher.setPaymentSystemId(null);
            }

            if (searcher.getLogDateFrom() != null && Misc.isInt(request.getParameter("logDateFrommm")) &&
                Misc.parseInt(request.getParameter("logDateFrommm")) >= 0 &&
                Misc.parseInt(request.getParameter("logDateFrommm")) < 60) {
                searcher.setLogDateFrom(new java.util.Date(searcher.getLogDateFrom().getTime() + 60000L * Misc.parseInt(
                        request.getParameter("logDateFrommm"))));
                mav.addObject("logDateFrommm", request.getParameter("logDateFrommm"));
            }
            if (searcher.getLogDateTo() != null) {
                searcher.setLogDateTo(DateUtil.normalizeDateDayEnd(searcher.getLogDateTo()));
            }

            if (searcher.getStart() == null) {
                searcher.setStart(0);
            }
            if (searcher.getLimit() == null) {
                searcher.setLimit(25);
            }

            if (!result.hasErrors()) {

                List<AuditLog> found = auditLogService.getLogRecordsByQuery(searcher);
                mav.addObject("found", found);

            }

            mav.addObject("searcher", searcher);
        } else {
            if (Misc.isNullOrEmpty(searcher.getLimit())) {
                searcher.setLimit(25);
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("AuditLog records list done in " + (System.currentTimeMillis() - start) + " ms");
        }

        return mav;
    }

}
