package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.PaymentSystemConstants;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.imports.AbstractBatchHandlerFacade;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.DataBatchResultStatus;
import com.modirum.ds.imports.merchant.MerchantBatchHandlerFacade;
import com.modirum.ds.mngr.model.ui.filter.AcquirerSearchFilter;
import com.modirum.ds.mngr.model.ui.filter.MerchantSearchFilter;
import com.modirum.ds.mngr.model.ui.filter.TDSServerSearchFilter;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.mngr.support.Helpers;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.services.AcquirerService;
import com.modirum.ds.services.MerchantService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.ThreeDSProfileService;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.ObjectDiff;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.util.StringUtils.trimWhitespace;

/**
 * Controller for managing acquiring 3DS Components' side: Merchants, Acquirers, 3DS Servers
 */
@Controller
@RequestMapping("/acquirers")
public class AcquirerController extends BaseController {

    private final transient Logger log = LoggerFactory.getLogger(AcquirerController.class);

    private AcquirerService acquirerService = ServiceLocator.getInstance().getAcquirerService();

    private MerchantService merchantService = ServiceLocator.getInstance().getMerchantService();

    private ThreeDSProfileService tdsProfileService = ServiceLocator.getInstance().getThreeDSProfileService();

    @ModelAttribute("countryMapA2")
    public Map<String, String> countryMap() {
        return super.countriesMapA2();
    }

    @ModelAttribute("acquirerStatuses")
    public Map<String, String> acquirerStatusesMap() {
        return statusMap(DSModel.Acquirer.Status.class.getName());
    }

    @ModelAttribute("tdsServerStatuses")
    public Map<String, String> tdsServerStatusesMap() {
        return statusMap(DSModel.TDSServerProfile.Status.class.getName());
    }

    @ModelAttribute("merchantStatuses")
    public Map<String, String> MerchantStatusesMap() {
        return statusMap(DSModel.Merchant.Status.class.getName());
    }

    @ModelAttribute("requestorIDwithMerchant")
    public Boolean getRequestorIDwithMerchant() {
        boolean singleMerchantPer33DSS = "true".equals(cnfs.getStringSetting(DsSetting.SINGLE_MERCHANT_PER_3DSS.getKey()));
        //force requestor id on merchant since merchants will not be looked up by acquirer id
        if (singleMerchantPer33DSS) {
            return true;
        }
        return !"false".equals(cnfs.getStringSetting(DsSetting.REQUESTOR_ID_WITH_MERCHANT.getKey())) ? Boolean.TRUE : Boolean.FALSE;
    }

    @ModelAttribute("enableTrustAllMerchants")
    public Boolean isTrustAllMerchnatEnabled() {
        return getWebContext().isTrueSetting(DsSetting.ENABLE_TRUST_ALL_MERCHANTS.getKey());
    }

    @RequestMapping("/acquirerList.html")
    public ModelAndView acquirerList(HttpServletRequest request,
                                     @ModelAttribute(value = "filter") AcquirerSearchFilter filter) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.acquirerView)) {
            return unauthorized(request);
        }
        long start = System.currentTimeMillis();

        ModelAndView mav = new ModelAndView("acquirers/acquirerList");
        // if one merchant user then he can only manage his merchant
        mav.addObject("binLength", getBinLength());
        if (Misc.isNotNullOrEmpty(request.getParameter("search"))) {
            // validate search form
            if (!paymentSystemSearchMap().containsKey(filter.getPaymentSystemId().toString())) {
                filter.setPaymentSystemId(currentUser().getPaymentSystemId());
            }

            if (filter.getStart() == null) {
                filter.setStart(0);
            }
            if (filter.getLimit() == null) {
                filter.setLimit(25);
            }
            if (PaymentSystemConstants.UNLIMITED.equals(filter.getPaymentSystemId())) {
                filter.setPaymentSystemId(null);
            }

            Map<String, Object> query = new HashMap<>();
            if (Misc.isNotNullOrEmpty(filter.getBIN())) {
                query.put("BIN", filter.getBIN() + "%");
            }
            if (Misc.isNotNullOrEmpty(filter.getName())) {
                query.put("name", filter.getName() + "%");
            }
            if (Misc.isNotNullOrEmpty(filter.getCountry())) {
                query.put("country", filter.getCountry());
            }
            if (Misc.isNotNullOrEmpty(filter.getStatus())) {
                query.put("status", filter.getStatus());
            }
            if (Misc.isNotNullOrEmpty(filter.getPaymentSystemId())) {
                query.put("paymentSystemId", filter.getPaymentSystemId());
            }

            List<Acquirer> acquirerList = persistenceService.getPersitableList(Acquirer.class, filter.getOrder(),
                    filter.getOrderDirection(), true,
                    filter.getStart(), filter.getLimit(),
                    query);

            filter.setTotal(((Long) query.get("total")).intValue());

            mav.addObject("filter", filter);
            mav.addObject("acquirerList", acquirerList);

            if (log.isDebugEnabled()) {
                log.debug("Acquirerlist done in " + (System.currentTimeMillis() - start) + " ms");
            }

            if ("true".equals(cnfs.getStringSetting(DsSetting.AUDIT_LOG_SEARCHES.getKey()))) {
                String detail = "" + ObjectDiff.properties(filter, true);
                auditLogService.logAction(AuthorityUtil.getUser(), new Acquirer(), DSModel.AuditLog.Action.SEARCH,
                                          detail);
            }
        } else {
            if (Misc.isNullOrEmpty(filter.getLimit())) {
                filter.setLimit(25);
            }
            if (Misc.isNullOrEmpty(filter.getOrder())) {
                filter.setOrder("id");
                filter.setOrderDirection("desc");
            }
        }

        return mav;
    }

    @RequestMapping("/acquirerView.html")
    public ModelAndView viewAcquirer(@RequestParam("id") Integer id, HttpServletRequest request) throws Exception {
        Acquirer acquirer = persistenceService.getPersistableById(id, Acquirer.class);
        if (!AuthorityUtil.isAccessAllowed(acquirer, Roles.acquirerView)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("acquirers/acquirerView");
        mav.addObject("acquirer", acquirer);

        if (Misc.isNotNullOrEmpty(acquirer.getPaymentSystemId())) {
            PaymentSystem paymentSystem = persistenceService.getPersistableById(acquirer.getPaymentSystemId(), PaymentSystem.class);
            if (paymentSystem != null) {
                mav.addObject("paymentSystemName", paymentSystem.getName());
            }
        }

        if (acquirer.getId() != null) {
            Map<String, Object> query = new HashMap<>();
            query.put("acquirerId", acquirer.getId());
            persistenceService.getPersitableList(Merchant.class, null, null, true, 0, 0, query);
            mav.addObject("acquirerMerCount", query.get("total"));
        }

        auditLogService.logAction(AuthorityUtil.getUser(), acquirer, DSModel.AuditLog.Action.VIEW,
                "View acquirer " + acquirer.getId());

        return mav;
    }

    @RequestMapping("/acquirerEdit.html")
    public ModelAndView editAcquirer(@RequestParam(value = "id", required = false) String id,
                                     HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.acquirerEdit)) {
            return unauthorized(request);
        }

        Acquirer acquirer = null;
        if (id != null && Misc.isInt(id)) {
            acquirer = persistenceService.getPersistableById(Misc.parseInt(id), Acquirer.class);
            if (!AuthorityUtil.isAccessAllowed(acquirer)) {
                return unauthorized(request);
            }
        }

        if (acquirer == null) {
            acquirer = new Acquirer();
        }

        ModelAndView mav = new ModelAndView("acquirers/acquirerEdit");
        mav.addObject("acquirer", acquirer);

        if (acquirer.getId() != null) {
            mav.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(acquirer.getPaymentSystemId())));
            auditLogService.logAction(AuthorityUtil.getUser(), acquirer, DSModel.AuditLog.Action.VIEW,
                                           "View acquirer " + acquirer.getId());
        }
        return mav;
    }

    private Map<String, String> getMerchantListTDSServerMap(List<Merchant> mList) throws Exception {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("", "");
        if (mList != null) {
            Object[] mIds = new Object[mList.size()];
            int i = 0;
            for (Merchant mc : mList) {
                mIds[i++] = mc.getTdsServerProfileId();
            }

            Map<String, Object> query = new HashMap<>();
            query.put("id:", mIds);
            List<TDSServerProfile> found = persistenceService.getPersitableList(TDSServerProfile.class, "name", null,
                                                                                true, 0, null, query);
            for (TDSServerProfile tdsServerProfile : found) {
                map.put(String.valueOf(tdsServerProfile.getId()), "" + tdsServerProfile.getId() + "-" + tdsServerProfile.getName());
            }
        }
        return map;
    }

    private void trimAcquirerFields(Acquirer acquirer) {
        acquirer.setName(StringUtils.trim(acquirer.getName()));
        acquirer.setEmail(StringUtils.trim(acquirer.getEmail()));
        acquirer.setAddress(StringUtils.trim(acquirer.getAddress()));
        acquirer.setCity(StringUtils.trim(acquirer.getCity()));
        acquirer.setPhone(StringUtils.trim(acquirer.getPhone()));
    }

    @RequestMapping(value = "/acquirerUpdate.html", method = RequestMethod.POST)
    public ModelAndView updateAcquirer(@ModelAttribute("acquirer") Acquirer acquirer,
                                       BindingResult result,
                                       HttpServletRequest request) throws Exception {

        if (!AuthorityUtil.isAccessAllowed(acquirer, Roles.acquirerEdit)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("acquirers/acquirerEdit");
        mav.addObject("acquirer", acquirer);
        mav.addObject("acquirerStatuses", statusMap(DSModel.Acquirer.Status.class.getName()));
        if (acquirer.getId() != null) {
            mav.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(acquirer.getPaymentSystemId())));
        }

        boolean saveAsNew = request.getParameter("saveAsNew") != null;
        Integer acquirerId = saveAsNew ? null : acquirer.getId();
        trimAcquirerFields(acquirer);

        Integer paymentSystemId = acquirer.getPaymentSystemId();
        if (!paymentSystemComponentMap().containsKey(paymentSystemId.toString())) {
            result.addError(new FieldError("acquirer", "paymentSystemId", getLocalizedMessage("err.invalid.value")));
        }

        if (Misc.isNullOrEmpty(acquirer.getName())) {
            result.addError(new FieldError("acquirer", "name", acquirer.getName(), false, null, null,
                getLocalizedMessage("err.required", getLocalizedMessage("text.name"))));
        } else if (acquirerService.isExistingDuplicateAcquirerName(acquirerId, acquirer.getName(), paymentSystemId)) {
            result.addError(new FieldError("acquirer", "name", acquirer.getName(), false, null, null,
                    getLocalizedMessage("err.duplicate.value")));
        }

        if (Misc.isNullOrEmpty(acquirer.getStatus())) {
            result.addError(new FieldError("acquirer", "status", acquirer.getStatus(), false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.status",
                                                                                                   locale()),
                                                                       locale())));
        }

        if (Misc.isNullOrEmpty(acquirer.getBIN())) {
            result.addError(new FieldError("acquirer", "BIN", acquirer.getBIN(), false, null, null,
                                           localizationService.getText("err.required", "BIN", locale())));
        } else if (!Misc.isNumber(acquirer.getBIN())) {
            result.addError(new FieldError("acquirer", "BIN", acquirer.getBIN(), false, null, null,
                                           localizationService.getText("err.invalid",
                                                                       localizationService.getText("err.must.be.number",
                                                                                                   "BIN", locale()),
                                                                       locale())));
        } else if (acquirerService.isExistingDuplicateAcquirerBIN(acquirerId, acquirer.getBIN(), paymentSystemId)) {
            result.addError(new FieldError("acquirer", "BIN", getLocalizedMessage("err.duplicate.value")));
        }

        if (!result.hasErrors()) {
            if (acquirerId == null) {
                if (saveAsNew) {
                    acquirer.setId(null);
                }
                persistenceService.save(acquirer);
                auditLogService.logInsert(AuthorityUtil.getUser(), acquirer, null);
            } else {
                Acquirer old = persistenceService.getPersistableById(acquirerId, Acquirer.class);
                //To be safe so paymentSystemId can't be hacked and changed
                acquirer.setPaymentSystemId(old.getPaymentSystemId());
                persistenceService.saveOrUpdate(acquirer);
                auditLogService.logUpdate(AuthorityUtil.getUser(), old, acquirer, null);
            }

            setContextAttribute("acquirers_cached", null);
            return super.redirect("acquirerView.html?id=" + acquirer.getId());
        }

        return mav;
    }

    @RequestMapping("/tdsServerList.html")
    public ModelAndView tdsServerList(HttpServletRequest request,
                               @ModelAttribute(value = "filter") TDSServerSearchFilter filter) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.acquirerView)) {
            return unauthorized(request);
        }
        long start = System.currentTimeMillis();
        ModelAndView mav = new ModelAndView("acquirers/tdsServerList");
        mav.addObject("inCertificateList", inCertificateMap());

        if (filter.getPaymentSystemId() == null) {
            filter.setPaymentSystemId(currentUser().getPaymentSystemId());
        } else if (PaymentSystemConstants.UNLIMITED.equals(filter.getPaymentSystemId()) ||
            !paymentSystemSearchMap().containsKey(filter.getPaymentSystemId().toString())) {
            filter.setPaymentSystemId(currentUser().getPaymentSystemId());
        }

        if ("refreshPaymentSystem".equals(request.getParameter("cmd"))) {
            return mav;
        }

        if (Misc.isNotNullOrEmpty(request.getParameter("search"))) {
            if (filter.getStart() == null) {
                filter.setStart(0);
            }
            if (filter.getLimit() == null) {
                filter.setLimit(25);
            }

            Map<String, Object> query = new HashMap<>();
            if (Misc.isNotNullOrEmpty(filter.getName())) {
                query.put("name", filter.getName() + "%");
            }
            if (Misc.isNotNullOrEmpty(filter.getRequestorID())) {
                query.put("requestorID", filter.getRequestorID() + "%");
            }
            if (Misc.isNotNullOrEmpty(filter.getOperatorID())) {
                query.put("operatorID", filter.getOperatorID() + "%");
            }
            if (Misc.isNotNullOrEmpty(filter.getStatus())) {
                query.put("status", filter.getStatus());
            }
            if (filter.getInClientCert() != null) {
                query.put("inClientCert", filter.getInClientCert());
            }
            if (Misc.isNotNullOrEmpty(filter.getPaymentSystemId())) {
                query.put("paymentSystemId", filter.getPaymentSystemId());
            }

            List<TDSServerProfile> tdsServerList = persistenceService.getPersitableList(TDSServerProfile.class,
                                                                                        filter.getOrder(),
                    filter.getOrderDirection(), true, filter.getStart(), filter.getLimit(), query);

            filter.setTotal(((Long) query.get("total")).intValue());

            mav.addObject("filter", filter);
            mav.addObject("tdsServerList", tdsServerList);

            if (log.isDebugEnabled()) {
                log.debug("MIlist done in " + (System.currentTimeMillis() - start) + " ms");
            }

            if ("true".equals(cnfs.getStringSetting(DsSetting.AUDIT_LOG_SEARCHES.getKey()))) {
                String detail = "" + ObjectDiff.properties(filter, true);
                auditLogService.logAction(AuthorityUtil.getUser(), new TDSServerProfile(), DSModel.AuditLog.Action.SEARCH,
                                          detail);
            }
        } else {
            if (Misc.isNullOrEmpty(filter.getLimit())) {
                filter.setLimit(25);
            }
            if (Misc.isNullOrEmpty(filter.getOrder())) {
                filter.setOrder("id");
                filter.setOrderDirection("desc");
            }
        }

        return mav;
    }

    @RequestMapping("/tdsServerView.html")
    public ModelAndView viewTdsServer(@RequestParam("id") Long id, HttpServletRequest request) throws Exception {
        TDSServerProfile tdsServerProfile = persistenceService.getPersistableById(id, TDSServerProfile.class);
        if (!AuthorityUtil.isAccessAllowed(tdsServerProfile, Roles.acquirerView)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("acquirers/tdsServerView");
        mav.addObject("tdsServerProfile", tdsServerProfile);
        mav.addObject("inCertificateList", inCertificateMap());
        mav.addObject("outCertificateList", outCertificateMap());
        mav.addObject("tdsServerStatuses", statusMap(DSModel.TDSServerProfile.Status.class.getName()));

        if (Misc.isNotNullOrEmpty(tdsServerProfile.getPaymentSystemId())) {
            PaymentSystem paymentSystem =
                    persistenceService.getPersistableById(tdsServerProfile.getPaymentSystemId(), PaymentSystem.class);
            if (paymentSystem != null) {
                mav.addObject("paymentSystemName", paymentSystem.getName());
            }
        }

        Map<String, Object> query = new HashMap<>();
        query.put("tdsServerProfileId", tdsServerProfile.getId());
        persistenceService.getPersitableList(Merchant.class, null, null, true, 0, 0, query);
        mav.addObject("tdsServerMerchantCount", query.get("total"));

        this.auditLogService.logAction(AuthorityUtil.getUser(), tdsServerProfile, DSModel.AuditLog.Action.VIEW,
                "View acquirer " + tdsServerProfile.getId());

        return mav;
    }


    @RequestMapping("/tdsServerEdit.html")
    public ModelAndView editTdsServer(@RequestParam(value = "id", required = false) String id,
                               HttpServletRequest request) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.acquirerEdit)) {
            return unauthorized(request);
        }

        TDSServerProfile tdsServerProfile = null;
        if (id != null && Misc.isLong(id)) {
            tdsServerProfile = persistenceService.getPersistableById(Misc.parseLong(id), TDSServerProfile.class);
            if (!AuthorityUtil.isAccessAllowed(tdsServerProfile)) {
                return unauthorized(request);
            }

            if (request.getParameter("delete") != null) {
                persistenceService.delete(tdsServerProfile);
                auditLogService.logDelete(AuthorityUtil.getUser(), tdsServerProfile);
                return redirect("tdsServerList.html");
            }
        }

        if (tdsServerProfile == null) {
            tdsServerProfile = new TDSServerProfile();
        }

        ModelAndView mav = new ModelAndView("acquirers/tdsServerEdit");
        mav.addObject("tdsServerProfile", tdsServerProfile);
        mav.addObject("tdsServerStatuses", statusMap(DSModel.TDSServerProfile.Status.class.getName()));
        mav.addObject("inCertificateList", inCertificateMap());
        mav.addObject("outCertificateList", outCertificateMap());

        if (tdsServerProfile.getId() != null) {
            mav.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(tdsServerProfile.getPaymentSystemId())));
            this.auditLogService.logAction(AuthorityUtil.getUser(), tdsServerProfile, DSModel.AuditLog.Action.VIEW,
                                           "View TDSServerProfile " + tdsServerProfile.getId());
        }
        return mav;
    }

    private void trimTDSServerProfileFields(TDSServerProfile tdsServerProfile) {
        tdsServerProfile.setName(StringUtils.trim(tdsServerProfile.getName()));
        tdsServerProfile.setRequestorID(StringUtils.trim(tdsServerProfile.getRequestorID()));
        tdsServerProfile.setOperatorID(StringUtils.trim(tdsServerProfile.getOperatorID()));
        tdsServerProfile.setTdsReferenceNumberList(StringUtils.trim(tdsServerProfile.getTdsReferenceNumberList()));
        tdsServerProfile.setURL(StringUtils.trim(tdsServerProfile.getURL()));
    }

    @RequestMapping(value = "/tdsServerSave.html", method = RequestMethod.POST)
    public ModelAndView saveTdsServer(@ModelAttribute("tdsServerProfile") TDSServerProfile tdsServerProfile,
                               BindingResult result,
                               HttpServletRequest request) throws Exception {

        if (!AuthorityUtil.isAccessAllowed(tdsServerProfile, Roles.acquirerEdit)) {
            return unauthorized(request);
        }

        boolean saveAsNew = request.getParameter("saveAsNew") != null;

        ModelAndView mav = new ModelAndView("acquirers/tdsServerEdit");
        mav.addObject("tdsServerProfile", tdsServerProfile);
        mav.addObject("tdsServerStatuses", statusMap(DSModel.TDSServerProfile.Status.class.getName()));
        mav.addObject("inCertificateList", inCertificateMap());
        mav.addObject("outCertificateList", outCertificateMap());

        Boolean enableTrustAllMerchants = isTrustAllMerchnatEnabled();
        trimTDSServerProfileFields(tdsServerProfile);

        if ("refreshPaymentSystem".equals(request.getParameter("cmd"))) {
            return mav;
        }

        if (Misc.isNullOrEmpty(tdsServerProfile.getName())) {
            result.addError(new FieldError("tdsServerProfile", "name", tdsServerProfile.getName(), false, null, null,
                                           getLocalizedMessage("err.required", getLocalizedMessage("text.name"))));
        } else if (tdsServerProfile.getName().length() > 40) {
            result.addError(new FieldError("tdsServerProfile", "name", tdsServerProfile.getName(), false, null, null,
                                           getLocalizedMessage("err.invalid.length", getLocalizedMessage("err.must.be.length","<=40"))));
        }

        if (Misc.isNotNullOrEmpty(tdsServerProfile.getName()) && tdsServerProfile.getPaymentSystemId() != null) {
            Map<String, Object> queryByName = new HashMap<>();
            queryByName.put("name", tdsServerProfile.getName());
            queryByName.put("paymentSystemId", tdsServerProfile.getPaymentSystemId());
            List<TDSServerProfile> profilesByName = persistenceService.getPersitableList(
                    TDSServerProfile.class, null, null, true,
                    null, null, queryByName);
            if (profilesByName != null && profilesByName.size() > 0) {
                TDSServerProfile sameNameProfile = profilesByName.get(0);
                if (sameNameProfile != null && (!sameNameProfile.getId().equals(tdsServerProfile.getId()) || saveAsNew)) {
                    result.addError(new FieldError("tdsServerProfile", "name", tdsServerProfile.getName(), false, null, null,
                                                   localizationService.getText("err.duplicate.value", locale())));
                }
            }
        }

        if (StringUtils.isNotEmpty(tdsServerProfile.getRequestorID()) && tdsServerProfile.getRequestorID().length() > 35) {
            result.addError(new FieldError("tdsServerProfile", "requestorID", tdsServerProfile.getRequestorID(), false, null, null,
                getLocalizedMessage("err.invalid.length", getLocalizedMessage("err.must.be.length", "<=35"))));
        }

        if (StringUtils.isNotEmpty(tdsServerProfile.getRequestorID())) {
            Long profileId = saveAsNew ? null : tdsServerProfile.getId();
            if (tdsProfileService.isExistingDuplicateTdsRequestorId(tdsServerProfile.getRequestorID(), tdsServerProfile.getPaymentSystemId(), profileId)) {
                result.addError(new FieldError("tdsServerProfile", "requestorID", tdsServerProfile.getRequestorID(), false, null, null,
                    getLocalizedMessage("err.duplicate.value")));
            }
        }

        if (tdsServerProfile.getOperatorID() != null && tdsServerProfile.getOperatorID().length() > 35) {
            result.addError(new FieldError("tdsServerProfile", "operatorID", tdsServerProfile.getOperatorID(), false, null, null,
                                           localizationService.getText("err.invalid.length",
                                                                       localizationService.getText("err.must.be.length",
                                                                                                   "<=35", locale()),
                                                                       locale())));
        }

        if (StringUtils.isNotEmpty(tdsServerProfile.getURL())) {
            if (!Helpers.isValidUrl(tdsServerProfile.getURL())) {
                result.addError(new FieldError("tdsServerProfile", "URL", tdsServerProfile.getURL(), false, null, null,
                        getLocalizedMessage("err.invalid.url")));
            }
        }

        if (StringUtils.isNotEmpty(tdsServerProfile.getOperatorID()) && tdsServerProfile.getPaymentSystemId() != null) {
            if (acquirerService.isExistingDuplicateOperatorId(tdsServerProfile.getOperatorID(), tdsServerProfile
                    .getPaymentSystemId(), tdsServerProfile.getId())) {
                result.addError(new FieldError("tdsServerProfile", "operatorID", tdsServerProfile.getOperatorID(),
                        false, null, null, getLocalizedMessage("err.duplicate.value")));
            }
        } else {
            tdsServerProfile.setOperatorID(null); // null out becuse of constraint, '' qualifies for constraint
        }

        if (Misc.isNullOrEmpty(tdsServerProfile.getStatus())) {
            result.addError(new FieldError("tdsServerProfile", "status", tdsServerProfile.getStatus(), false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.status",
                                                                                                   locale()),
                                                                       locale())));
        }

        if (!sslCertsManagedExternally() && Misc.isNullOrEmpty(tdsServerProfile.getInClientCert())) {
            result.addError(new FieldError("tdsServerProfile", "inClientCert", tdsServerProfile.getInClientCert(), false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.inClientCert",
                                                                                                   locale()),
                                                                       locale())));
        }

        if (StringUtils.isNotEmpty(tdsServerProfile.getTdsReferenceNumberList()) && tdsServerProfile.getTdsReferenceNumberList().length() > 255) {
            result.addError(new FieldError("tdsServerProfile", "tdsReferenceNumberList", tdsServerProfile.getTdsReferenceNumberList(), false, null, null,
                                           localizationService.getText("err.invalid.length",
                                                                       localizationService.getText("err.must.be.length",
                                                                                                   "<=255", locale()),
                                                                       locale())));
        }

        if (tdsServerProfile.getId() == null) {
            List<TDSServerProfile> tdsServerProfileList = acquirerService.getTdsServerProfilesByNameAndPaymentSystem(tdsServerProfile.getName(),
                                                                                                tdsServerProfile.getPaymentSystemId());
            if (CollectionUtils.isNotEmpty(tdsServerProfileList)) {
                result.addError(new FieldError("tdsServerProfile", "paymentSystemId", getLocalizedMessage(
                        "err.duplicate.value")));
            }
        }

        // validate payment system
        String userInputPaymentSystemId = tdsServerProfile.getPaymentSystemId().toString();
        if (!paymentSystemComponentMap().containsKey(userInputPaymentSystemId)) {
            result.addError(new FieldError("tdsServerProfile", "paymentSystemId", getLocalizedMessage("err.invalid.value")));
        }

        if (!result.hasErrors()) {

            if (saveAsNew || tdsServerProfile.getId() == null) {
                tdsServerProfile.setId(null);
                persistenceService.save(tdsServerProfile);
                auditLogService.logInsert(AuthorityUtil.getUser(), tdsServerProfile, null);
            } else {
                TDSServerProfile old = persistenceService.getPersistableById(tdsServerProfile.getId(), TDSServerProfile.class);
                //To be safe so paymentSystemId can't be hacked and changed
                tdsServerProfile.setPaymentSystemId(old.getPaymentSystemId());
                persistenceService.saveOrUpdate(tdsServerProfile);
                auditLogService.logUpdate(AuthorityUtil.getUser(), old, tdsServerProfile, null);
            }

            return super.redirect("tdsServerView.html?id=" + tdsServerProfile.getId());
        }
        if (tdsServerProfile.getId() != null) {
            mav.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(tdsServerProfile.getPaymentSystemId())));
        }
        return mav;
    }

    @RequestMapping("/merchantList.html")
    public ModelAndView MerchantList(HttpServletRequest request,
                                     @ModelAttribute(value = "filter") MerchantSearchFilter filter,
                                     @RequestParam(value = "TDSServerProfileId", required = false) Long TDSServerProfileId) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.acquirerView)) {
            return unauthorized(request);
        }
        
        long start = System.currentTimeMillis();
        ModelAndView mav = new ModelAndView("acquirers/merchantList");

        if (filter.getPaymentSystemId() == null) {
            if (Misc.isNotNullOrEmpty(filter.getAcquirerId())) {
                Acquirer acquirer = acquirerService.getAcquirerById(filter.getAcquirerId());
                filter.setPaymentSystemId(acquirer.getPaymentSystemId());
            } else {
                filter.setPaymentSystemId(currentUser().getPaymentSystemId());
            }
        } else if (PaymentSystemConstants.UNLIMITED.equals(filter.getPaymentSystemId()) ||
                !paymentSystemSearchMap().containsKey(filter.getPaymentSystemId().toString())) {
            filter.setPaymentSystemId(currentUser().getPaymentSystemId());
        }

        mav.addObject("acquirersMap", acquirerService.getAcquirerDropdownMap(locale(), filter.getPaymentSystemId()));

        if ("refreshPaymentSystem".equals(request.getParameter("cmd"))) {
            filter.setAcquirerId(null);
            return mav;
        }

        if (Misc.isNotNullOrEmpty(request.getParameter("search"))) {
            if (filter.getStart() == null) {
                filter.setStart(0);
            }
            if (filter.getLimit() == null) {
                filter.setLimit(25);
            }

            Map<String, Object> query = new HashMap<>();
            if (Misc.isNotNullOrEmpty(filter.getName())) {
                query.put("name", filter.getName() + "%");
            }
            if (Misc.isNotNullOrEmpty(filter.getAcquirerId())) {
                query.put("acquirerId", filter.getAcquirerId());
            }
            if (Misc.isNotNullOrEmpty(filter.getStatus())) {
                query.put("status", filter.getStatus());
            }
            if (Misc.isNotNullOrEmpty(filter.getCountry())) {
                query.put("country", filter.getCountry());
            }
            if (Misc.isNotNullOrEmpty(filter.getAcquirerMerchantID())) {
                query.put("identifier", filter.getAcquirerMerchantID() + "%");
            }
            if (Misc.isNotNullOrEmpty(filter.getRequestorID())) {
                query.put("requestorID", filter.getRequestorID() + "%");
            }
            if (Misc.isNotNullOrEmpty(filter.getPaymentSystemId())) {
                query.put("paymentSystemId", filter.getPaymentSystemId());
            }

            if (TDSServerProfileId != null) {
                query.put("tdsServerProfileId", TDSServerProfileId);
            }

            List<Merchant> merchantList = persistenceService.getPersitableList(Merchant.class, filter.getOrder(),
                    filter.getOrderDirection(), true, filter.getStart(), filter.getLimit(), query);

            filter.setTotal(((Long) query.get("total")).intValue());

            mav.addObject("tdsServerList", getMerchantListTDSServerMap(merchantList));
            mav.addObject("merchantList", merchantList);
            mav.addObject("filter", filter);

            if (log.isDebugEnabled()) {
                log.debug("Merchant list done in " + (System.currentTimeMillis() - start) + " ms");
            }

            if ("true".equals(cnfs.getStringSetting(DsSetting.AUDIT_LOG_SEARCHES.getKey()))) {
                String detail = "" + ObjectDiff.properties(filter, true);
                auditLogService.logAction(AuthorityUtil.getUser(), new Merchant(), DSModel.AuditLog.Action.SEARCH,
                                          detail);
            }
        } else {
            if (Misc.isNullOrEmpty(filter.getLimit())) {
                filter.setLimit(25);
            }
            if (Misc.isNullOrEmpty(filter.getOrder())) {
                filter.setOrder("id");
                filter.setOrderDirection("desc");
            }
        }

        return mav;
    }

    @RequestMapping("/merchantView.html")
    public ModelAndView viewMerchant(@RequestParam("id") Long id, HttpServletRequest request) throws Exception {
        Merchant merchant = persistenceService.getPersistableById(id, Merchant.class);
        if (!AuthorityUtil.isAccessAllowed(merchant, Roles.acquirerView)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("acquirers/merchantView");
        mav.addObject("merchant", merchant);
        if (merchant.getAcquirerId() != null) {
            mav.addObject("acquirer", acquirerService.getAcquirerById(merchant.getAcquirerId()));
        }

        if (merchant.getTdsServerProfileId() != null) {
            mav.addObject("tdsServer", acquirerService.getTdsServerProfileById(merchant.getTdsServerProfileId()));
        }

        if (merchant.getPaymentSystemId() != null) {
            PaymentSystem paymentSystem =
                    persistenceService.getPersistableById(merchant.getPaymentSystemId(), PaymentSystem.class);
            if (paymentSystem != null) {
                mav.addObject("paymentSystemName", paymentSystem.getName());
            }
        }

        auditLogService.logAction(AuthorityUtil.getUser(), merchant, DSModel.AuditLog.Action.VIEW,
                                           "View merchant " + merchant.getId());
        return mav;
    }

    @RequestMapping("/merchantsBatch.html")
    public ModelAndView merchantsBatch(@ModelAttribute(value = "data") MerchantsBatchRequestData data,
                                       HttpServletRequest request) throws Exception {
        ModelAndView mav = new ModelAndView("acquirers/merchantsBatch");
        mav.addObject("data", data);
        if (request.getParameter("actionButton") != null) {
            String actionButton = request.getParameter("actionButton");
            if (!data.getUploadFile().isEmpty() || data.getUploadData() != null) {
                if (!data.getUploadFile().isEmpty()) {
                    // Return selected file to a user so he can use it again without the need to open file
                    data.setUploadData(Base64.encode(data.getUploadFile().getBytes(), 0));
                    data.setUploadFileName(data.getUploadFile().getOriginalFilename());
                }

                String fileContent = new String(!data.getUploadFile().isEmpty()
                        ? data.getUploadFile().getBytes() : Base64.decode(data.getUploadData()));
                DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(currentUser());
                AbstractBatchHandlerFacade importer = new MerchantBatchHandlerFacade(ServiceLocator.getInstance().getPersistenceService(), configuration);
                DataBatchResultStatus results = null;
                switch (actionButton) {
                    case "Upload":
                        if (!AuthorityUtil.hasRole(Roles.merchantsEdit)) {
                            return unauthorized(request);
                        }
                        results = importer.validateAndProcess(fileContent);
                        break;
                    case "Validate":
                        configuration.setContinueOnError(true);
                        results = importer.validate(fileContent);
                        break;
                    default:
                        mav.addObject("error", "Not supported action");
                }
                mav.addObject("results", results);
            }
        }

        return mav;
    }

    @RequestMapping("/merchantEdit.html")
    public ModelAndView editMerchant(@RequestParam(value = "id", required = false) String id,
                                     HttpServletRequest request) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.acquirerEdit)) {
            return unauthorized(request);
        }

        Merchant merchant = null;
        if (id != null && Misc.isLong(id)) {
            merchant = persistenceService.getPersistableById(Misc.parseLong(id), Merchant.class);
            if (!AuthorityUtil.isAccessAllowed(merchant)) {
                return unauthorized(request);
            }

            if (request.getParameter("delete") != null) {
                persistenceService.delete(merchant);
                auditLogService.logDelete(AuthorityUtil.getUser(), merchant);
                return redirect("merchantList.html");
            }
        }

        if (merchant == null) {
            merchant = new Merchant();
            if (Misc.isLong(request.getParameter("TDSServerProfileId"))) {
                merchant.setTdsServerProfileId(Misc.parseLongBoxed(request.getParameter("TDSServerProfileId")));
            } else if (Misc.isInt(request.getParameter("acqid"))) {
                merchant.setAcquirerId(Misc.parseInt(request.getParameter("acqid")));
            }
        }

        ModelAndView mav = new ModelAndView("acquirers/merchantEdit");
        mav.addObject("merchant", merchant);
        int paymentSystemId = merchant.getPaymentSystemId() != null ? merchant.getPaymentSystemId() :
            Integer.parseInt(paymentSystemComponentMap().keySet().iterator().next());
        mav.addObject("acquirersMap", acquirerService.getAcquirerDropdownMap(locale(), paymentSystemId));
        mav.addObject("tdsServerMap", acquirerService.getTDSServerDropdown(paymentSystemId));

        if (merchant.getId() != null) {
            mav.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(paymentSystemId)));
            this.auditLogService.logAction(AuthorityUtil.getUser(), merchant, DSModel.AuditLog.Action.VIEW,
                                           "View acquirer " + merchant.getId());
        }
        return mav;
    }

    @RequestMapping(value = "/merchantSave.html", method = RequestMethod.POST)
    public ModelAndView saveMerchant(@ModelAttribute("merchant") Merchant merchant,
                                     BindingResult result,
                                     HttpServletRequest request) throws Exception {

        if (!AuthorityUtil.isAccessAllowed(merchant, Roles.acquirerEdit)) {
            return unauthorized(request);
        }

        formatMerchantUserInput(merchant);

        boolean saveAsNew = request.getParameter("saveAsNew") != null;

        ModelAndView mav = new ModelAndView("acquirers/merchantEdit");
        mav.addObject("merchant", merchant);
        mav.addObject("merchantStatuses", statusMap(DSModel.Merchant.Status.class.getName()));
        mav.addObject("acquirersMap", acquirerService.getAcquirerDropdownMap(locale(), merchant.getPaymentSystemId()));
        mav.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(merchant.getPaymentSystemId())));
        mav.addObject("tdsServerMap", acquirerService.getTDSServerDropdown(merchant.getPaymentSystemId()));

        if ("refreshPaymentSystem".equals(request.getParameter("cmd"))) {
            return mav;
        }

        validateMerchant(merchant, result, saveAsNew);

        if (!result.hasErrors()) {
            if (saveAsNew || merchant.getId() == null) {
                merchant.setId(null);
                persistenceService.save(merchant);
                auditLogService.logInsert(AuthorityUtil.getUser(), merchant, null);
            } else {
                Merchant oldMerchant = persistenceService.getPersistableById(merchant.getId(), Merchant.class);
                //To be safe so paymentSystemId can't be hacked and changed
                merchant.setPaymentSystemId(oldMerchant.getPaymentSystemId());
                persistenceService.saveOrUpdate(merchant);
                auditLogService.logUpdate(AuthorityUtil.getUser(), oldMerchant, merchant, null);
            }

            return super.redirect("merchantView.html?id=" + merchant.getId());
        }

        return mav;
    }

    private void validateMerchant(Merchant merchant, BindingResult result, boolean saveAsNew) throws Exception {
        if (Misc.isNullOrEmpty(merchant.getName())) {
            result.addError(new FieldError("merchant", "name", merchant.getName(), false, null, null,
                                           getLocalizedMessage("err.required", "text.name")));

        } else if (merchant.getAcquirerId() != null) {
            Map<String, Object> queryByNameAndAcqId = new HashMap<>();
            queryByNameAndAcqId.put("name", merchant.getName());
            queryByNameAndAcqId.put("acquirerId", merchant.getAcquirerId());
            List<Merchant> sameNameAndAcqIdMerchants = persistenceService.getPersitableList(Merchant.class, null, null,
                                                                                            true, null, null,
                                                                                            queryByNameAndAcqId);
            if (sameNameAndAcqIdMerchants != null && sameNameAndAcqIdMerchants.size() > 0) {
                Merchant sameNameAndAcqIdMerchant = sameNameAndAcqIdMerchants.get(0);
                if (sameNameAndAcqIdMerchant != null &&
                    (!sameNameAndAcqIdMerchant.getId().equals(merchant.getId()) || saveAsNew)) {
                    result.addError(new FieldError("merchant", "name", merchant.getName(), false, null, null,
                                                   getLocalizedMessage("err.name.already.in.use.for.acquirer")));
                }
            }

        }
        if (Misc.isNullOrEmpty(merchant.getStatus())) {
            result.addError(new FieldError("merchant", "status", merchant.getStatus(), false, null, null,
                                           getLocalizedMessage("err.required", "text.status")));
        }

        if (Misc.isNullOrEmpty(merchant.getTdsServerProfileId())) {
            result.addError(
                    new FieldError("merchant", "tdsServerProfileId", merchant.getTdsServerProfileId(), false, null,
                                   null, getLocalizedMessage("err.required", "text.tdsserverprofile")));
        }

        if (merchant.getTdsServerProfileId() != null) {
            TDSServerProfile selectedTdsServerProfile = acquirerService.getTdsServerProfileById(merchant.getTdsServerProfileId());
            if (selectedTdsServerProfile == null) {
                result.addError(new FieldError("merchant", "tdsServerProfileId",
                                               getLocalizedMessage("err.tdsServerProfile.invalid")));
            }
        }

        if (merchant.getAcquirerId() != null && merchant.getPaymentSystemId() != null) {
            Acquirer selectedAcquirer = acquirerService.getAcquirerById(merchant.getAcquirerId());
            if (selectedAcquirer == null ||
                !merchant.getPaymentSystemId().equals(selectedAcquirer.getPaymentSystemId())) {
                result.addError(new FieldError("merchant", "acquirerId", getLocalizedMessage("err.acquirer.invalid")));
            }
        }
        if (merchant.getAcquirerId() != null && Misc.isNullOrEmpty(merchant.getIdentifier())) {
            result.addError(new FieldError("merchant", "identifier", merchant.getIdentifier(), false, null, null,
                                           getLocalizedMessage("err.required", "text.merchant.acquirerMerchantID")));
        }

        if (merchant.getAcquirerId() != null && Misc.isNotNullOrEmpty(merchant.getIdentifier())) {
            Merchant duplicateMerchant = persistenceService.getAquirerMerchant(merchant.getAcquirerId(),
                                                                               merchant.getIdentifier());
            if (duplicateMerchant != null && (!duplicateMerchant.getId().equals(merchant.getId()) || saveAsNew)) {
                result.addError(new FieldError("merchant", "identifier", merchant.getIdentifier(), false, null, null,
                                               getLocalizedMessage("err.registration.id.is.already.in.use")));
            }
        }
        if (getRequestorIDwithMerchant()) {
            if (Misc.isNullOrEmpty(merchant.getRequestorID())) {
                result.addError(new FieldError("merchant", "requestorID", merchant.getRequestorID(), false, null, null,
                                               getLocalizedMessage("err.required", "text.3ds.requestorId")));
            }

            if (Misc.isNotNullOrEmpty(merchant.getRequestorID())) {
                Merchant duplicateMerchant = persistenceService.getRequestorMerchant(merchant.getRequestorID());
                if (duplicateMerchant != null && (!duplicateMerchant.getId().equals(merchant.getId()) || saveAsNew)) {
                    result.addError(
                            new FieldError("merchant", "requestorID", merchant.getRequestorID(), false, null, null,
                                           getLocalizedMessage("err.requestor.id.is.already.in.use")));
                }
            }
        }

        if (merchant.getId() == null) {
            Merchant m = persistenceService.getMerchantByNameAndPaymentSystemId(merchant.getName(),
                                                                                merchant.getPaymentSystemId());
            if (m != null) {
                result.addError(
                        new FieldError("merchant", "paymentSystemId", getLocalizedMessage("err.duplicate.value")));
            }
        }

        // validate payment system
        String userInputPaymentSystemId = merchant.getPaymentSystemId().toString();
        if (!paymentSystemComponentMap().containsKey(userInputPaymentSystemId)) {
            result.addError(new FieldError("merchant", "paymentSystemId", getLocalizedMessage("err.invalid.value")));
        }

        if (StringUtils.isNotEmpty(merchant.getURL()) && !Helpers.isValidUrl(merchant.getURL())) {
            result.addError(new FieldError("merchant", "URL", merchant.getURL(), false, null, null,
                                           getLocalizedMessage("err.invalid.url")));
        }
    }

    /**
     * Formats Merchant model user input before saving to the database.
     */
    private void formatMerchantUserInput(Merchant merchant) {
        if (merchant != null) {
            merchant.setIdentifier(trimWhitespace(merchant.getIdentifier()));
            merchant.setCountry(trimWhitespace(merchant.getCountry()));
            merchant.setRequestorID(trimWhitespace(merchant.getRequestorID()));
            merchant.setName(trimWhitespace(merchant.getName()));
            merchant.setPhone(trimWhitespace(merchant.getPhone()));
            merchant.setStatus(trimWhitespace(merchant.getStatus()));
            merchant.setURL(trimWhitespace(merchant.getURL()));
            merchant.setEmail(trimWhitespace(merchant.getEmail()));
            merchant.setOptions(trimWhitespace(merchant.getOptions()));
        }
    }

    public static class MerchantsBatchRequestData {

        private String uploadData;
        private String uploadFileName;
        private MultipartFile uploadFile;

        public MultipartFile getUploadFile() {
            return uploadFile;
        }

        public void setUploadFile(MultipartFile uploadFile) {
            this.uploadFile = uploadFile;
        }

        public String getUploadData() {
            return uploadData;
        }

        public void setUploadData(String uploadData) {
            this.uploadData = uploadData;
        }

        public String getUploadFileName() {
            return uploadFileName;
        }

        public void setUploadFileName(String uploadFileName) {
            this.uploadFileName = uploadFileName;
        }
    }
}
