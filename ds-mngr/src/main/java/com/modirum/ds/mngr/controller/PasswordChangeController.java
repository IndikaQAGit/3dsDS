package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.mngr.support.Helpers;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.utils.Misc;
import me.legrange.haveibeenpwned.HaveIBeenPwndApi;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
@RequestMapping("/passwordChange")
public class PasswordChangeController extends BaseController {

    @RequestMapping("/view.html")
    public ModelAndView viewPassword() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ModelAndView mav = new ModelAndView("changePassword/passwordChange");
        User userFormData = new User();
        userFormData.setId(user.getId());
        userFormData.setLoginname(user.getLoginname());
        userFormData.setForcedPasswordChange(user.isForcedPasswordChange());
        mav.addObject("user", userFormData);
        return mav;
    }

    @RequestMapping(value = "/change.html", method = RequestMethod.POST)
    public ModelAndView changePassword(final User userDataRequest, final BindingResult result) throws Exception {
        int encPassLength = 44;

        User authUser = AuthorityUtil.getUser();
        Date now = new Date(System.currentTimeMillis());

        // user can only change his own password
        userDataRequest.setId(authUser.getId());
        userDataRequest.setLoginname(authUser.getLoginname());

        User dbUser = this.persistenceService.getPersistableById(authUser.getId(), User.class);

        if (dbUser == null) {
            result.addError(new FieldError("user", "currentPass", "Error: Current user not found"));
        } else if (Misc.isNullOrEmpty(userDataRequest.getPassword())) {
            result.addError(new FieldError("user", "currentPass", localizationService.getText("mn.current.password.is.required", locale())));
        } else if (!checkPassword(userDataRequest.getPassword(), dbUser.getPassword())) {
            result.addError(new FieldError("user", "password", localizationService.getText("mn.current.password.entered.is.not.correct", locale())));
        }

        if (!result.hasErrors()) {
            if (Misc.isNullOrEmpty(userDataRequest.getNewPassword())) {
                result.addError(new FieldError("user", "newPassword", localizationService.getText("mn.password.may.not.be.blank", locale())));
            }
        }

        if (!result.hasErrors()) {
            if (Misc.isNullOrEmpty(userDataRequest.getNewPasswordRepeat())) {
                result.addError(new FieldError("user", "newPasswordRepeat", localizationService.getText("mn.password.verify.may.not.be.blank", locale())));
            } else if (!userDataRequest.getNewPasswordRepeat().equals(userDataRequest.getNewPassword())) {
                result.addError(new FieldError("user", "newPasswordRepeat", localizationService.getText("mn.password.entries.dont.match", locale())));
            }
        }

        if (!result.hasErrors()) {
            // deep semantics checks
            Helpers.isPasswordAsRequired(userDataRequest.getNewPassword(), "newPassword", cnfs, localizationService, locale(), result);
            Helpers.isPasswordDiffOk(userDataRequest.getPassword(), userDataRequest.getNewPassword(), "newPassword", cnfs, localizationService, locale(), result);
        }

        // repeat checks
        if (!result.hasErrors()) {
            // current password check
            if (checkPassword(userDataRequest.getNewPassword(), dbUser.getPassword())) {
                result.addError(new FieldError("user", "newPassword", localizationService.getText("mn.new.password.is.one.of.the.last.used.please.choose.another.password", locale())));
            } else {
                // old passwords check
                String oldPass = dbUser.getOldpasswords();
                if (oldPass != null && oldPass.length() > 0) {
                    for (int i = 0; i < oldPass.length() / encPassLength; i++) {
                        String pass = oldPass.substring(encPassLength * i, encPassLength * (i + 1));
                        if (checkPassword(userDataRequest.getNewPassword(), pass)) {
                            result.addError(new FieldError("user", "newPassword", localizationService.getText(
                                    "mn.new.password.is.one.of.the.last.used.please.choose.another.password",
                                    locale())));
                            break;
                        }
                    }
                }
            }
        }

        if (!result.hasErrors()) {
            boolean useStrongPassword = "true".equals(cnfs.getStringSetting(DsSetting.MNGR_USE_STRONG_PASSWORD.getKey()));
            if (useStrongPassword && new HaveIBeenPwndApi().isPlainPasswordPwned(userDataRequest.getNewPassword())) {
                result.addError(new FieldError("user", "newPassword", localizationService.getText("mn.new.password.has.been.compromised", locale())));
            }
        }

        if (result.hasErrors()) {
            ModelAndView mav = new ModelAndView("changePassword/passwordChange");
            mav.addObject("user", userDataRequest);
            return mav;
        }

        // update users last modified password date and oldPasswords field
        // cut old passwords to fit into table
        String old = "";
        if (dbUser.getOldpasswords() != null) {
            old = dbUser.getOldpasswords().substring(0, dbUser.getOldpasswords().length() <
                    encPassLength * 3 ? dbUser.getOldpasswords().length() :
                    encPassLength * 3);
            dbUser.setOldpasswords(dbUser.getPassword() + old.substring(0, (old.length() < encPassLength *
                    2 ? dbUser.getOldpasswords().length() :
                    encPassLength * 2)));
        } else {
            dbUser.setOldpasswords(dbUser.getPassword());
        }

        dbUser.setPassword(CryptoService.sha256WithUniquePredictable(userDataRequest.getNewPassword()));
        dbUser.setLastPassChangeDate(now);
        auditLogService.logUpdate(AuthorityUtil.getUser(), dbUser, dbUser.getLoginname() + " self changed password");
        persistenceService.saveOrUpdate(dbUser);

        // update user in session for correct further work
        // may be also from certificate strong so in any case valid auth
        dbUser.setFactor2Passed(authUser.isFactor2Passed());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(dbUser, "", AuthorityUtil.getAuthorities(dbUser)));

        return super.redirect("/main/main.html");
    }

    boolean checkPassword(final String rawPassword, final String hash) {
        return CryptoService.sha256WithUniquePredictable(rawPassword).equals(hash);
    }
}
