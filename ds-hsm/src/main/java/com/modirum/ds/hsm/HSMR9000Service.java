package com.modirum.ds.hsm;

import com.modirum.ds.hsm.support.HSMDataKey;
import com.modirum.ds.hsm.support.HsmException;
import com.modirum.ds.hsm.support.RSADecryptPadMode;
import com.modirum.ds.hsm.support.SignAlgIdentifier;
import com.modirum.ds.jce.MdRSAPrivateKey;
import com.modirum.ds.jce.MdRSAPublicKey;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;

/**
 * Implements parts of Thales Payshield 9000 HSM interface for DS.
 * - Allows HSM usage with 'Keyblock LMK' only.
 * - Uses AES key for data encryption.
 */
public class HSMR9000Service extends HSMR8000Service implements HSMDevice {

    private transient static final Logger LOG = LoggerFactory.getLogger(HSMR9000Service.class);
    public static final int BLOCK_SIZE_AES = 16;

    public HSMR9000Service(String host, int port) {
        super(host, port);
    }

    public HSMR9000Service() {
        super();
    }

    /**
     * Generates AES-256 data encryption key.
     * @return generated key.
     * @throws Exception in case of any HSM issues.
     */
    @Override
    public Key generateHSMDataEncryptionKey() throws Exception {
        byte[][] keyAncCv = generateAESKey("A3"); // 'A3' = 256-bit AES key
        return new HSMDataKey(keyAncCv[0], HexUtils.fromString(new String(keyAncCv[1], StandardCharsets.ISO_8859_1)));
    }

    @Override
    public byte[] encryptData(byte[] hsmDataEncryptionKey, byte[] data) throws Exception {
        return aesEncrypt(hsmDataEncryptionKey, usePad ? pkcs5pad(data, BLOCK_SIZE_AES) : data, false);
    }

    @Override
    public byte[] decryptData(byte[] hsmDataEncryptionKey, byte[] data) throws Exception {
        return usePad ? pkcs5unpad(aesDecrypt(hsmDataEncryptionKey, data, false), BLOCK_SIZE_AES) :
                aesDecrypt(hsmDataEncryptionKey, data, false);
    }

    @Override
    public KeyPair generateRSAdecryptAndSign(int keysize)
        throws HsmException {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        String command = "EI";
        int keyTypeIndicator = 4; // Data encryption/decryption (e.g. TLS/SSL premaster secret)

        StringBuilder paramsBuilder = new StringBuilder();
        paramsBuilder.append(keyTypeIndicator);
        paramsBuilder.append(zeroPad(4, keysize)); // key length
        paramsBuilder.append("01"); // public key encoding = DER encoding, INTEGER unsigned
        if (lmkKB != null) {
            paramsBuilder.append('%'); // delimiter
            paramsBuilder.append(lmkKB); // LMK Identifier
        }
        paramsBuilder.append("#00"); // key version number
        paramsBuilder.append("00"); // number of optional blocks
        paramsBuilder.append("&S"); // exportability

        try {
            if (LOG.isDebugEnabled()) {
                LOG.debug("generateRSAdecryptAndSign, type {}, keysize {}", keyTypeIndicator, keysize);
            }

            byte[] params = paramsBuilder.toString().getBytes();
            byte[] result = sendCommand(command, params);
            if (LOG.isDebugEnabled()) {
                LOG.debug(toHexString(result));
            }

            checkResponseCode(result, "EJ");
            checkErrorCode(result);
            byte[][] keys = cutKeys(result, keysize);

            PrivateKey privateKey = new MdRSAPrivateKey(keys[2]);
            PublicKey publicKey = new MdRSAPublicKey(keys[0]);
            return new KeyPair(publicKey, privateKey);
        } catch (Exception e) {
            throw new HsmException("Error generating RSA keypair.", e);
        }
    }

    /**
     * Generate an RSA Signature
     *
     * @param key key
     * @param data message data
     * @param signatureAlgorithm algorithm
     * @return byte array
     * @throws Exception
     */
    private byte[] sign(byte[] key, byte[] data, String signatureAlgorithm) throws Exception {
        String command = "EW";
        SignAlgIdentifier sigAlgIdentifier = SignAlgIdentifier.getBySignatureAlgorithm(signatureAlgorithm);
        String hashIdentifier = sigAlgIdentifier.getHashIdentifier();
        String padModeIdentifier = sigAlgIdentifier.getPadModeIdentifier();

        byte[] p1 = concat((hashIdentifier + "01" + padModeIdentifier + zeroPad(4, data.length)).getBytes(), data);
        byte[] p2 = concat((";99FFFF").getBytes(), key);
        byte[] p3 = concat(p2, lmkKB != null ? ("%" + lmkKB).getBytes() : EMPTY);
        byte[] params = concat(p1, p3);

        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "EX");
        checkErrorCode(res);
        return dropFirst(8, res);
    }

    @Override
    public ContentSigner getContentSigner(final byte[] rsaPrivateKeyBytes, final String signatureAlgorithm) {
        // A more convenient MDjce provider could be used for seamless signing. But that needs to be implemented in DS
        return new ContentSigner() {
            private ByteArrayOutputStream dataToSign = new ByteArrayOutputStream();

            @Override
            public AlgorithmIdentifier getAlgorithmIdentifier() {
                return new DefaultSignatureAlgorithmIdentifierFinder().find(signatureAlgorithm);
            }

            @Override
            public OutputStream getOutputStream() {
                return dataToSign;
            }

            @Override
            public byte[] getSignature() {
                try {
                    return sign(rsaPrivateKeyBytes, dataToSign.toByteArray(), signatureAlgorithm);
                } catch (Exception e) {
                    LOG.error("Error generating RSA signature.", e);
                    return null;
                }
            }
        };
    }

    @Override
    public byte[] rsaPrivateDecrypt(byte rsaPrivateKey[], byte encryptedData[], RSADecryptPadMode padMode)
            throws Exception {

        if (LOG.isDebugEnabled()) {
            LOG.debug("key length: {}; rsa decrypt data length: {}", rsaPrivateKey.length, encryptedData.length);
        }

        String commandCode = "GI";
        String commandPrefix;
        switch (padMode) {
            case PKCS1:
                commandPrefix = "0101FFFF"; // encryption identifier + pad mode identifier + key type (KEY BLOCK LMK)
                break;
            case OAEP_SHA1_MGF1:
                // encryption identifier + pad mode identifier + mask generation function + MGF Hash function
                // + OAEP params length + OAEP params delimiter + key type (KEY BLOCK LMK)
                commandPrefix = "0102010100;FFFF";
                break;
            case OAEP_SHA256_MGF1:
                // encryption identifier + pad mode identifier + mask generation function + MGF Hash function
                // + OAEP params length + OAEP params delimiter + key type (KEY BLOCK LMK)
                commandPrefix = "0102010600;FFFF";
                break;
            default:
                throw new HsmException("Unknown padding mode for RSA decrypt: " + padMode.name());
        }

        byte[] cmdBlock1 = concat((commandPrefix + zeroPad(4, encryptedData.length)).getBytes(), encryptedData);
        byte[] cmdBlock2 = concat(";99FFFF".getBytes(), rsaPrivateKey);
        byte[] cmdBlock3 = concat(cmdBlock1, cmdBlock2);
        byte[] cmdBlock4 = concat(cmdBlock3, lmkKB != null ? ("%" + lmkKB).getBytes() : EMPTY);

        byte[] response = sendCommand(commandCode, cmdBlock4);
        checkResponseCode(response, "GJ");
        checkErrorCode(response);

        return dropFirst(8, response);
    }

    protected byte[] aesEncrypt(byte[] keyRaw, byte[] data, boolean cbc) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        String command = "M0";
        StringBuilder params = new StringBuilder();
        params.append(cbc ? "01" : "00"); // mode ECB
        params.append("0"); // input format binary
        params.append("0"); // out format binary
        params.append("FFF"); // keytype
        params.append("S");
        params.append(new String(keyRaw, StandardCharsets.ISO_8859_1));
        if (cbc) {
            params.append(IV_HEX_STR);
            params.append(IV_HEX_STR);
        }
        params.append(len2hex4(data.length));

        byte[] paramsAndLenAndData = concat(params.toString().getBytes(StandardCharsets.ISO_8859_1), data,
                lmkKB != null && lmkKB.length() == 2 ? ("%" + lmkKB).getBytes(StandardCharsets.ISO_8859_1) : EMPTY);

        byte[] res = sendCommand(command, paramsAndLenAndData);
        // System.out.println("Res="+new String(res));
        checkResponseCode(res, "M1");
        checkErrorCode(res);
        byte[] out = dropFirst(cbc ? 8 + 32 : 8, res);
        return out;
    }

    protected byte[] aesDecrypt(byte[] keyRaw, byte[] data, boolean cbc) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        String command = "M2";
        StringBuilder params = new StringBuilder();
        params.append(cbc ? "01" : "00"); // mode ECB
        params.append("0"); // input format binary
        params.append("0"); // out format binary
        params.append("FFF"); // keytype
        params.append("S");
        params.append(new String(keyRaw, StandardCharsets.ISO_8859_1));
        if (cbc) {
            params.append(IV_HEX_STR);
            params.append(IV_HEX_STR);
        }
        params.append(len2hex4(data.length));
        byte[] paramsAndLenAndData = concat(params.toString().getBytes(StandardCharsets.ISO_8859_1), data,
                lmkKB != null && lmkKB.length() == 2 ? ("%" + lmkKB).getBytes(StandardCharsets.ISO_8859_1) : EMPTY);
        byte[] res = sendCommand(command, paramsAndLenAndData);
        checkResponseCode(res, "M3");
        checkErrorCode(res);
        byte[] out = dropFirst(cbc ? 8 + 32 : 8, res);
        return out;
    }

    protected byte[][] generateAESKey(String alg) throws Exception {

        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("generateAESKey: " + alg);
        }

        String command = "A0";
        StringBuilder params = new StringBuilder();
        params.append("0"); // Mode
        params.append("FFF"); // keyblock
        params.append("S"); // keyblock
        if (lmkKB != null) {
            params.append('%'); // del
            params.append(lmkKB); // LMK number
        }

        params.append("#"); // delim
        params.append("D0"); // usage D0
        params.append(alg); // alg. 'A1' = 128-bit AES key; 'A2' = 192-bit AES key; 'A3' = 256-bit AES key
        params.append("B"); // mode both
        params.append("00"); // key version
        params.append("S"); // exportability
        params.append("00"); // number of optional blocks

        byte[] res = sendCommand(command, params.toString().getBytes(StandardCharsets.ISO_8859_1));
        checkResponseCode(res, "A1");
        checkErrorCode(res);
        if (LOG.isDebugEnabled()) {
            LOG.debug("res length: " + res.length);
        }

        byte[][] keyAndCv = new byte[2][];
        byte[] keylen = sub(6, 4, res);
        int keyln = Misc.parseInt(new String(keylen, StandardCharsets.ISO_8859_1));
        keyAndCv[0] = sub(5, keyln, res);
        keyAndCv[1] = sub(5 + keyln, 6, res);

        return keyAndCv;
    }

    @Override
    public String getMasterKeyCheckValue() throws Exception {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getMasterKeyCheckValue");
        }
        String command = "NC";
        byte[] params = (this.lmkKB != null ? "%" + lmkKB : "%00").getBytes();

        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "ND");
        checkErrorCode(res);
        byte[] lmkcheckvalue = sub(4, 16, res);
        String fullhexkcv = new String(lmkcheckvalue, StandardCharsets.ISO_8859_1);

        return fullhexkcv.substring(0, 6);
    }


    public static void main(String args[]) {
        try {
            String host = args != null && args.length > 0 ? args[0] : "185.35.202.86";
            String port = args != null && args.length > 1 ? args[1] : "1500";
            HSMR9000Service r9000 = new HSMR9000Service(host, Integer.parseInt(port));

            r9000.initializeHSM("tcp=true;lmkkcv=9D04A0");
            String hsmInitializationResult = r9000.alive() ? "success" : "fail";
            System.out.println("Initialization of HSM9000: " + hsmInitializationResult);
            KeyPair keyPair = r9000.generateRSAdecryptAndSign(2048);

            String clearTextData = "test string";
            Security.addProvider(new BouncyCastleProvider());
            KeyFactory keyFactory = KeyFactory.getInstance(SoftwareJCEService.RSA, SoftwareJCEService.BC);
            PublicKey rsaKeyResultPublic = keyFactory.generatePublic(new X509EncodedKeySpec(keyPair.getPublic()
                    .getEncoded()));
            Cipher rsaEncryptCipher = Cipher.getInstance(SoftwareJCEService.RSA_NONE_OAEP_SHA256_MGF1);
            rsaEncryptCipher.init(Cipher.ENCRYPT_MODE, rsaKeyResultPublic);
            byte[] rsaEncryptedData = rsaEncryptCipher.doFinal(clearTextData.getBytes());
            byte[] decrypted = r9000.rsaPrivateDecrypt(keyPair.getPrivate().getEncoded(), rsaEncryptedData,
                    RSADecryptPadMode.OAEP_SHA256_MGF1);
            String testResult = (clearTextData.equalsIgnoreCase(new String(decrypted))) ? "success" : "failed";
            System.out.println("encryption and decryption test: " + testResult);

            String signingData = "sign test";
            byte[] signature = r9000.sign(keyPair.getPrivate().getEncoded(), signingData.getBytes(), "SHA256withRSA");
            Signature sign = Signature.getInstance("SHA256withRSA");
            sign.initVerify(keyPair.getPublic());
            sign.update(signingData.getBytes());
            String signTestResult = sign.verify(signature) ? "success" : "failed";
            System.out.println("Signature test: " + signTestResult);

            // STATIC CHECK //
            String base64privateKey = "UzEwNzA0MDZSTjAwUzAwMDHGM8Gpjb0V1wGcM3aTOuN95/+azOeYZSjNaS8HSEJWTg4TPP8l+55F\n"
                + "UVHuw3pVrCaDoVciBou9rPfmxlRpSM0GhPu2sOxheZEzTdi9aVWSQMLyyqooZFy8YScf8XdvfH7N\n"
                + "+EnhQr1vwvrTXFPcs5axT3WaAaCd6w6f3m4hhKKMea/ggZQ7wLmuwwgpkOYGBZDTBWXqVqiUCjrE\n"
                + "P8TDrpB2KI71A0mNqdRpfjgZ0W3cO3uJCaMXXlUGPkd+6sF9APu+R7v5CEwTZ2bdwVpH32Ziu5CF\n"
                + "ApH1jS2EMRl+L86F4/wX2xnCoaTHz9z0jAuVU3jTjJwbrasqx4VobQFV0YX/TDmEm0vw7P5UmsqC\n"
                + "+SNHQ2ChNKvzvRDI8i1uCpgVE9M8I8635uYDjCqIcwbXjfZm5S0+My47kl+Urv7JkCT/u9CeMEBC\n"
                + "q6nvRm4yR+UMmnaLse6Lpa1cokV1p8q8PtmsQ5Om0AM42SNWUuAAlRgFah8iUxxZfPnapBBIWWEf\n"
                + "NarhX3PLjgo8rpLPMWbUg9SZxotjpBWztIZ1KGG+SrIrD/Hrz/503F6y/VIo/R4EROppjo/8cDGw\n"
                + "d/skE8/YPh1+2ZCiFQ8E6LUtaZP74VRB5GvZhMEQl0kVjJIVfp2ogchdyg2v+E7Y8QYwVUpA6qTV\n"
                + "iJFSyjRxBUOjLkHFnofDkM5DnuDv7N0k7P+06qlsYS0bAvo6Q9zzKWreLFXul93D7l3PPuU4nuzK\n"
                + "4S8IPMIewXRmEvt/QGmNSKHynRvoe+DrSc3JN1p6q8NoyWuQjSaCg/9LNxKRe8w0NlpVE57ya/5h\n"
                + "IJbmKB0ELb4ll56w6w6uo5zTdtlHQiHX3dZnwT8UWx5VKtmRJPU+TD2GIK02aDD/n929d/cRXcNr\n"
                + "qC/2ptw5MDVEQzVBREE5MEJBMDlG";

            String base64csr = "MIICmjCCAYICAQAwVTEZMBcGA1UEAwwQVGVzdCBTREsgQ1NSIGtleTEMMAoGA1UECwwDRGV2MQsw\n"
                + "CQYDVQQKDAJNRDEQMA4GA1UEBwwHVGFsbGlubjELMAkGA1UEBhMCRUUwggEiMA0GCSqGSIb3DQEB\n"
                + "AQUAA4IBDwAwggEKAoIBAQCkoR06v1vR0YrYjUmjumQbvYgmV2kUwmk8RZb8aip27JOCKxxRzNVg\n"
                + "B2ehn9d3lph4Ply5vUiAQ048X6h+OC6/QmdVvjXmTa8opUf8sU2AdyURgZXcCEpvortr+WywyVwm\n"
                + "HzPN9ZA1DKKfLKvRmFkA2AUA8y3AW+iiLnWFHyN1zkqWM+eU7xcuQ9PeR4vnjH40PUZ1sInO76o1\n"
                + "eHLAkO5fA8g+CGtWRWwpCIxMVE3wWA0G70PrlE5VElO6KW6lqcQRGWmvgELy9zjmFOxnlreBm8/F\n"
                + "cJw4PkWewVLMa43Jo6+yO0Pm698Tu959G5mkpQDWHcmvjYfiQhOokwDhw4TdAgMBAAGgADANBgkq\n"
                + "hkiG9w0BAQsFAAOCAQEAHwvKc2ZOspTF4XNgHhkvC50g15+5krfBV7D6ewu30W0H6mudVnu83w/1\n"
                + "9aWLGmlE7kV8uuKtHv+w1KgNo8r5MWtCE4rHLJoYTDj6GiEgJjJTDU2bguwGrta1CxXRgv7/pr1n\n"
                + "wKx1cO5O0uJfQjEkt3ZGBHOJA36u5/ipL3svV24ObAE6fuMw9wj45TR0Ur6j+IB0TJXBhN5CRd+i\n"
                + "yvr2lRnwXz0O3CJ4R5AWOPCIPnlHfHrfGuv0+jrxNU/RhzUoNeCA0JCVTKtyca/++m02wBE2GbIF\n"
                + "Abhjtd3JGfFSltA8vFVQuxcN12g3coHO+fHAW2JuHPvXDTDwXLT29eKUpQ==\n";

            PKCS10CertificationRequest csr = new PKCS10CertificationRequest(Base64.decode(base64csr));
            String clearTextForStaticTest = "test string for static test";
            org.bouncycastle.asn1.pkcs.RSAPublicKey pkcs1PublicKey = org.bouncycastle.asn1.pkcs.RSAPublicKey
                    .getInstance(csr.getSubjectPublicKeyInfo().parsePublicKey().getEncoded());
            BigInteger modulus = pkcs1PublicKey.getModulus();
            BigInteger publicExponent = pkcs1PublicKey.getPublicExponent();
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(modulus, publicExponent);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey publicKeyFromCSR = kf.generatePublic(keySpec);
            rsaEncryptCipher.init(Cipher.ENCRYPT_MODE, publicKeyFromCSR);
            byte[] rsaEncryptedDataStatic = rsaEncryptCipher.doFinal(clearTextForStaticTest.getBytes());
            byte[] staticRSAprivateKey = Base64.decode(base64privateKey);
            byte[] decryptedStatic = r9000.rsaPrivateDecrypt(staticRSAprivateKey, rsaEncryptedDataStatic,
                    RSADecryptPadMode.OAEP_SHA256_MGF1);
            String initialStaticCSRTest = clearTextForStaticTest.equals(new String(decryptedStatic)) ? "success"
                    : "failed";
            System.out.println("initial CSR static test: " + initialStaticCSRTest);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
