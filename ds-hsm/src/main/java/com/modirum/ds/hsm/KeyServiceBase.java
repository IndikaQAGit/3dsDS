/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 28.04.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.hsm;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;

public class KeyServiceBase {

    final static byte[] ffblock8 = {127, 127, 127, 127, 127, 127, 127, 127};
    final static byte[] zeroblock16 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    final static int minSymKeyLenBytes = 16;
    final static int minUniqueBytesInKey = 8;

    public final static String BEGIN_CERTIFICATE_REQUEST = "-----BEGIN CERTIFICATE REQUEST-----";
    public final static String END_CERTIFICATE_REQUEST = "-----END CERTIFICATE REQUEST-----";
    public final static String BEGIN_CERTIFICATE = "-----BEGIN CERTIFICATE-----";
    public final static String END_CERTIFICATE = "-----END CERTIFICATE-----";
    public final static String BEGIN_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----";
    public final static String END_PUBLIC_KEY = "-----END PUBLIC KEY-----";
    public final static String BEGIN_PRIVATE_KEY = "-----BEGIN PRIVATE KEY-----";
    public final static String END_PRIVATE_KEY = "-----END PRIVATE KEY-----";
    public final static String BEGIN_ENCRYPTED_PRIVATE_KEY = "-----BEGIN ENCRYPTED PRIVATE KEY-----";
    public final static String END_ENCRYPTED_PRIVATE_KEY = "-----END ENCRYPTED PRIVATE KEY-----";
    public final static char[] ccrlf = {'\r', '\n'};
    public final static byte[] c5dashes = "-----".getBytes(StandardCharsets.ISO_8859_1);

    protected transient static Logger log = LoggerFactory.getLogger(KeyServiceBase.class);

    // initialize safe default
    static java.security.SecureRandom sr = new java.security.SecureRandom(
            ("QQuu6343636ds9r4h" + new java.util.Date() + "s5NLHWM" + Runtime.getRuntime().totalMemory() + "Zd9Bsl" +
                    System.currentTimeMillis() + " " + Math.random()).getBytes(StandardCharsets.UTF_8)
    );

    public KeyServiceBase() {

    }

    public static byte[] genKey(int length) throws Exception {
        byte[] keyMat = new byte[length];
        int test = 0;
        while (true) {
            sr.nextBytes(keyMat);
            test++;
            if (test > 5 || validateKey(keyMat)) {
                break;
            }
        }

        return keyMat;
    }

    public static boolean validateKey(byte[] km) {
        if (km == null || km.length < minSymKeyLenBytes) {
            throw new RuntimeException("Too weak key.. length " + (km != null ? km.length * 8 : 0) + " bits, min allowed "
                    + (minSymKeyLenBytes * 8) + " bits");
        }

        java.util.Map<Integer, Integer> numMap = new java.util.TreeMap<Integer, Integer>();

        for (int i = 0; i < km.length; i++) {
            Integer cx = (int) km[i];
            numMap.put(cx, cx);
        }

        return (numMap.size() >= minUniqueBytesInKey || numMap.size() >= km.length / 4);
    }

    /**
     * Parse <code>certData</code> into X509Certificate array using <code>X509</code> factory type.
     * @param certData
     * @return
     * @throws Exception
     */
    public static X509Certificate[] parseX509Certificates(byte[] certData) throws Exception {
        return parseX509Certificates(certData, "X509");
    }

    public static X509Certificate[] parseX509Certificates(byte[] certData, String factoryType) throws Exception {
        if (factoryType == null) {
            factoryType = "X509";
        }

        CertificateFactory cf = CertificateFactory.getInstance(factoryType);
        ByteArrayInputStream bis = new ByteArrayInputStream(certData);
        Collection<? extends Certificate> cc = cf.generateCertificates(bis);
        bis.close();
        List<X509Certificate> clX509 = new java.util.LinkedList<X509Certificate>();
        Iterator<? extends Certificate> it = cc.iterator();
        while (it.hasNext()) {
            Certificate cx = it.next();
            if (cx instanceof X509Certificate) {
                clX509.add((X509Certificate) cx);
            }
        }
        if (clX509.size() > 0) {
            return clX509.toArray(new X509Certificate[clX509.size()]);
        }
        return null;
    }

    public static X509Certificate[] parseX509PemHeadersSafeCertificates(byte[] certData, String factoryType) throws Exception {
        X509Certificate[] clX509 = null;
        if (factoryType == null) {
            factoryType = "X509"; //KeyManagerFactory.getDefaultAlgorithm();
        }
        int dashes = Misc.indexOf(certData, c5dashes);
        // pem case
        if (dashes > -1 || Misc.isBase64(certData)) {
            List<X509Certificate> clX509lst = new java.util.LinkedList<X509Certificate>();
            if (dashes < 0) {
                byte[] xcertData = Base64.decode(certData);
                X509Certificate[] xca = parseX509Certificates(xcertData, factoryType);
                for (X509Certificate xc : xca) {
                    clX509lst.add(xc);
                }
            } else {
                String[] pemDataLines = Misc.split(new String(certData, StandardCharsets.ISO_8859_1), '\n');
                StringBuilder singlePem = new StringBuilder(4096);
                boolean started = false;
                for (String x : pemDataLines) {
                    if (x.contains("-----")) {
                        started = !started;
                    } else if (started && Misc.isNotNullOrEmptyTrimmed(x)) {
                        singlePem.append(x);
                        singlePem.append('\n');
                    }
                    if (!started && singlePem.length() > 0) {
                        byte[] xcertData = Base64.decode(singlePem.toString());
                        X509Certificate[] xca = parseX509Certificates(xcertData, factoryType);
                        for (X509Certificate xc : xca) {
                            clX509lst.add(xc);
                        }
                        singlePem.setLength(0);
                    }

                }
            }
            clX509 = clX509lst.toArray(new X509Certificate[clX509lst.size()]);

        } else // DER binary
        {
            clX509 = parseX509Certificates(certData, factoryType);
        }

        return clX509;
    }

    public static String parsePEMCSR(String mixedContent) {
        return parsePEMCSR(mixedContent, false);
    }

    public static String parsePEMCSR(String mixedContent, boolean includeTags) {
        int start = -1;
        if (mixedContent != null && (start = mixedContent.indexOf(BEGIN_CERTIFICATE_REQUEST)) > -1) {
            start = start + (includeTags ? 0 : BEGIN_CERTIFICATE_REQUEST.length());
            int end = mixedContent.indexOf(END_CERTIFICATE_REQUEST, start);
            if (end > start) {
                return mixedContent.substring(start, end - 1 + (includeTags ? END_CERTIFICATE_REQUEST.length() + 1 : 0));
            }
            throw new RuntimeException("End certificate request block not found");
        }

        return null;
    }

    public static String parsePEMCert(String mixed) {
        return parsePEMCert(mixed, false);
    }

    public static String parsePEMCert(String mixed, boolean includeTags) {
        int start = -1;
        if (mixed != null && (start = mixed.indexOf(BEGIN_CERTIFICATE)) > -1) {
            start = start + (includeTags ? 0 : BEGIN_CERTIFICATE.length());
            int end = mixed.indexOf(END_CERTIFICATE, start);
            if (end > start) {
                return mixed.substring(start, end - 1 + (includeTags ? END_CERTIFICATE.length() + 1 : 0));
            }
            throw new RuntimeException("End certificate block not found");
        }

        return null;
    }

    public static String parsePEMChain(String mixed) {
        int start = -1;
        if (mixed != null && (start = mixed.indexOf(BEGIN_CERTIFICATE)) > -1) {
            return mixed.substring(start);
        }

        return null;
    }

    /**
     * Extracts Private key (not encrypted) from a mixed data PEM formatted.
     *
     * @param mixed Mixed pem content (e.g. concatenated Private Key and (Certificate or public key))
     * @return Extracted private key part.
     */
    public static String parsePemPrivateKey(String mixed) {
        int start = -1;
        if (mixed != null && (start = mixed.indexOf(BEGIN_PRIVATE_KEY)) > -1) {
            start = start + BEGIN_PRIVATE_KEY.length();
            int end = mixed.indexOf(END_PRIVATE_KEY, start);
            if (end > start) {
                return mixed.substring(start, end - 1);
            }
            throw new RuntimeException("End private key block not found");
        }

        return null;
    }

    public static String parsePemEncryptedPrivateKey(String mixed) {
        int start = -1;
        if (mixed != null && (start = mixed.indexOf(BEGIN_ENCRYPTED_PRIVATE_KEY)) > -1) {
            start = start + BEGIN_ENCRYPTED_PRIVATE_KEY.length();
            int end = mixed.indexOf(END_ENCRYPTED_PRIVATE_KEY, start);
            if (end > start) {
                return mixed.substring(start, end - 1);
            }
            throw new RuntimeException("End encrypted private key block not found");
        }

        return null;
    }

    public static String getKeyHashCheckValue(byte[] key) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(key);
        String hash = HexUtils.toString(digest);

        return hash.substring(hash.length() - 8);
    }

    public static String calculateAESKeyCheckValue(byte[] key) throws Exception {
        Cipher aes = Cipher.getInstance("AES/CBC/NoPadding"); // PKCS5Padding
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        aes.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
        byte[] kcvfull = aes.doFinal(zeroblock16);
        String kcvffullHex = HexUtils.toString(kcvfull);

        return kcvffullHex.substring(kcvffullHex.length() - 6);
    }

    public static String calculateRSAPublicKeyCheckValue(PublicKey bobPubKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, bobPubKey);
        byte[] kcvfull = cipher.doFinal(ffblock8);
        String kcvffullHex = HexUtils.toString(kcvfull);

        return kcvffullHex.substring(kcvffullHex.length() - 6);
    }

    public static String calculateRSAPrivateKeyCheckValue(PrivateKey priKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, priKey);
        byte[] kcvfull = cipher.doFinal(ffblock8);
        String kcvffullHex = HexUtils.toString(kcvfull);

        return kcvffullHex.substring(kcvffullHex.length() - 6);
    }

    public static String calculateECIESPublicKeyCheckValue(PublicKey pubKey) throws Exception {
        return getKeyHashCheckValue(pubKey.getEncoded());
    }

    public static String calculateECIESPrivateKeyCheckValue(PrivateKey priKey) throws Exception {
        return getKeyHashCheckValue(priKey.getEncoded());
    }

    public static String combineEncryptedPrivateKeyAndCert(byte[] newPkDataAesHSM, byte[] newCertData) {
        StringBuilder bos = new StringBuilder(4096);
        if (newPkDataAesHSM != null) {
            bos.append(KeyServiceBase.BEGIN_ENCRYPTED_PRIVATE_KEY);
            bos.append(KeyServiceBase.ccrlf);
            bos.append(Base64.encode(newPkDataAesHSM));
            bos.append(KeyServiceBase.ccrlf);
            bos.append(KeyServiceBase.END_ENCRYPTED_PRIVATE_KEY);
        }
        if (newCertData != null) {
            if (bos.length() > 0) {
                bos.append(KeyServiceBase.ccrlf);
            }
            bos.append(KeyServiceBase.BEGIN_CERTIFICATE);
            bos.append(KeyServiceBase.ccrlf);
            bos.append(Base64.encode(newCertData));
            bos.append(KeyServiceBase.ccrlf);
            bos.append(KeyServiceBase.END_CERTIFICATE);
        }

        return bos.toString();
    }

    /**
     * Combines private key and PKCS10 certificate request (CSR) into pem format.
     *
     * @param privateKeyBytes Private key bytes. Can be either a clear-text software or HSM private key.
     * @param certificateRequestBytes PKCS10 certificate request (CSR) bytes
     * @return Combined private key and CSR in PEM format.
     */
    public static String combinePrivateKeyAndCSR(byte[] privateKeyBytes, byte[] certificateRequestBytes) {
        StringBuilder bos = new StringBuilder(4096);
        if (privateKeyBytes != null) {
            bos.append(KeyServiceBase.BEGIN_PRIVATE_KEY);
            bos.append(KeyServiceBase.ccrlf);
            bos.append(Base64.encode(privateKeyBytes));
            bos.append(KeyServiceBase.ccrlf);
            bos.append(KeyServiceBase.END_PRIVATE_KEY);
        }
        if (certificateRequestBytes != null) {
            if (bos.length() > 0) {
                bos.append(KeyServiceBase.ccrlf);
            }
            bos.append(KeyServiceBase.BEGIN_CERTIFICATE_REQUEST);
            bos.append(KeyServiceBase.ccrlf);
            bos.append(Base64.encode(certificateRequestBytes));
            bos.append(KeyServiceBase.ccrlf);
            bos.append(KeyServiceBase.END_CERTIFICATE_REQUEST);
        }
        return bos.toString();
    }

    /**
     * Combines private key and X509 certificate into pem format.
     *
     * @param privateKeyBytes Private key bytes. Can be either a clear-text software or HSM private key.
     * @param certificateBytes X509 certificate bytes
     * @return
     */
    public static String combinePrivateKeyAndCertificate(byte[] privateKeyBytes, byte[] certificateBytes) {
        StringBuilder bos = new StringBuilder(4096);
        if (privateKeyBytes != null) {
            bos.append(KeyServiceBase.BEGIN_PRIVATE_KEY);
            bos.append(KeyServiceBase.ccrlf);
            bos.append(Base64.encode(privateKeyBytes));
            bos.append(KeyServiceBase.ccrlf);
            bos.append(KeyServiceBase.END_PRIVATE_KEY);
        }
        if (certificateBytes != null) {
            if (bos.length() > 0) {
                bos.append(KeyServiceBase.ccrlf);
            }
            bos.append(KeyServiceBase.BEGIN_CERTIFICATE);
            bos.append(KeyServiceBase.ccrlf);
            bos.append(Base64.encode(certificateBytes));
            bos.append(KeyServiceBase.ccrlf);
            bos.append(KeyServiceBase.END_CERTIFICATE);
        }
        return bos.toString();
    }

    /**
     * Combines private key and X509 certificate chain into PEM format.
     *
     * @param privateKeyBytes Private key bytes. Can be either a clear-text software or HSM private key.
     * @param chain X509 certificates
     * @return Private key and certificate chain as combined pem string.
     */
    public static String combinePrivateKeyAndCertChain(byte[] privateKeyBytes, Certificate[] chain)
            throws CertificateEncodingException {

        StringBuilder bos = new StringBuilder(4096);

        if (privateKeyBytes != null) {
            bos.append(KeyServiceBase.BEGIN_PRIVATE_KEY);
            bos.append(KeyServiceBase.ccrlf);
            bos.append(Base64.encode(privateKeyBytes));
            bos.append(KeyServiceBase.ccrlf);
            bos.append(KeyServiceBase.END_PRIVATE_KEY);
        }

        if (chain != null) {
            for (int i = 0; i < chain.length; i++) {
                if (bos.length() > 0) {
                    bos.append(KeyServiceBase.ccrlf);
                }
                bos.append(KeyServiceBase.BEGIN_CERTIFICATE);
                bos.append(KeyServiceBase.ccrlf);
                bos.append(Base64.encode(chain[i].getEncoded()));
                bos.append(KeyServiceBase.ccrlf);
                bos.append(KeyServiceBase.END_CERTIFICATE);
            }
        }

        return bos.toString();
    }

    /**
     * Combines private key and ASN.1 public key into PEM format.
     *
     * @param privateKeyBytes Private key bytes. Can be either a clear-text software or HSM private key.
     * @param publicKeyBytes Public key bytes that represent ASN.1 structure.
     * @return concatenated keys in PEM format.
     */
    public static String combinePrivateKeyAndPublicKey(byte[] privateKeyBytes, byte[] publicKeyBytes) {
        StringBuilder sb = new StringBuilder();
        if (privateKeyBytes != null) {
            sb.append(KeyServiceBase.BEGIN_PRIVATE_KEY);
            sb.append(KeyServiceBase.ccrlf);
            sb.append(Base64.encode(privateKeyBytes));
            sb.append(KeyServiceBase.ccrlf);
            sb.append(KeyServiceBase.END_PRIVATE_KEY);
        }
        if (publicKeyBytes != null) {
            if (sb.length() > 0) {
                sb.append(KeyServiceBase.ccrlf);
            }
            sb.append(KeyServiceBase.BEGIN_PUBLIC_KEY);
            sb.append(KeyServiceBase.ccrlf);
            sb.append(Base64.encode(publicKeyBytes));
            sb.append(KeyServiceBase.ccrlf);
            sb.append(KeyServiceBase.END_PUBLIC_KEY);
        }

        return sb.toString();
    }

    public static byte[] parsePEMCSR(byte[] csrd) {
        String csr = new String(csrd, StandardCharsets.US_ASCII);
        if (csr.indexOf("-----BEGIN") >= 0 && csr.indexOf("CERTIFICATE REQUEST-----") > 0) {
            int start = csr.indexOf("CERTIFICATE REQUEST-----") + "CERTIFICATE REQUEST-----".length();
            int end = csr.indexOf("-----END", start);

            if (start > -1 && end > start) {
                String b64 = csr.substring(start, end);
                return Base64.decode(b64);
            }
        }
        return csrd;
    }

    /**
     * Accepts csr in pem format. This method accepts stringified csr considering different header starting word.
     * @param csr
     * @return
     */
    public static byte[] parseCSR(String csr) {
        if (csr.indexOf("-----BEGIN") >= 0 && csr.indexOf("CERTIFICATE REQUEST-----") > 0) {
            int start = csr.indexOf("CERTIFICATE REQUEST-----") + "CERTIFICATE REQUEST-----".length();
            int end = csr.indexOf("-----END", start);

            if (start > -1 && end > start) {
                String b64 = csr.substring(start, end);
                return Base64.decode(b64);
            }
        }
        return Base64.decode(csr);
    }

    public static int getPublicKeyBitLength(PublicKey pk) {
        if (pk instanceof RSAPublicKey) {
            return ((RSAPublicKey) pk).getModulus().bitLength();
        }
        if (pk instanceof DSAPublicKey) {
            return ((DSAPublicKey) pk).getParams().getP().bitCount();
        }
        if (pk instanceof ECPublicKey) {
            return ((ECPublicKey) pk).getParams().getCurve().getA().bitLength();
        }

        return -1;
    }

    public static void main(String[] arg) {
        try {
            java.security.KeyPairGenerator kpg = java.security.KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048, new java.security.SecureRandom());

            java.security.KeyPair keyPair = kpg.genKeyPair();

            System.out.println("RSAPrik CheckValue1:" + calculateRSAPrivateKeyCheckValue(keyPair.getPrivate()));
            System.out.println("RSAPrik CheckValue2:" + calculateRSAPrivateKeyCheckValue(keyPair.getPrivate()));
            System.out.println("RSAPrik CheckValue3:" + calculateRSAPrivateKeyCheckValue(keyPair.getPrivate()));
            System.out.println();
            System.out.println("RSAPubk CheckValue1:" + calculateRSAPublicKeyCheckValue(keyPair.getPublic()));
            System.out.println("RSAPubk CheckValue2:" + calculateRSAPublicKeyCheckValue(keyPair.getPublic()));
            System.out.println("RSAPubk CheckValue3:" + calculateRSAPublicKeyCheckValue(keyPair.getPublic()));

        } catch (Exception ee) {
            ee.printStackTrace(System.out);
        }


        try {
			/*
			java.security.Provider bcpe=java.security.Security.getProvider("BC");
			if (bcpe==null)
			{
				bcpe= (java.security.Provider)Class.forName("org.bouncycastle.jce.provider.BouncyCastleProvider").newInstance();
				java.security.Security.addProvider(bcpe);
			}
		*/
            java.security.KeyPairGenerator kpg = java.security.KeyPairGenerator.getInstance("EC");
            kpg.initialize(256, new java.security.SecureRandom());

            java.security.KeyPair keyPair = kpg.genKeyPair();
            System.out.println();
            System.out.println();
            System.out.println("ECPrik CheckValue1:" + calculateECIESPrivateKeyCheckValue(keyPair.getPrivate()));
            System.out.println("ECPrik CheckValue2:" + calculateECIESPrivateKeyCheckValue(keyPair.getPrivate()));
            System.out.println("ECPrik CheckValue3:" + calculateECIESPrivateKeyCheckValue(keyPair.getPrivate()));
            System.out.println();
            System.out.println("ECPubk CheckValue1:" + calculateECIESPublicKeyCheckValue(keyPair.getPublic()));
            System.out.println("ECPubk CheckValue2:" + calculateECIESPublicKeyCheckValue(keyPair.getPublic()));
            System.out.println("ECPubk CheckValue3:" + calculateECIESPublicKeyCheckValue(keyPair.getPublic()));

        } catch (Exception ee) {
            ee.printStackTrace(System.out);
        }
    }
}
