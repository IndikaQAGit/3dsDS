package com.modirum.ds.hsm;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;

public class CryptoServiceBase {

    final protected static Charset utf8 = StandardCharsets.UTF_8;
    protected final static long someNumber = 1234567891123456L; // dont change it

    public static String decryptConfigPassword(final CharSequence encPassword, final HSMDevice hsms) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        String[] parts = Misc.split(encPassword, ';');
        byte[] passwordKey = Base64.decode(parts[0]);
        byte[] passwordData = Base64.decode(parts[1]);
        byte[] password = hsms.decryptData(passwordKey, passwordData);

        String passWithPredictable = new String(password, utf8);
        String passwordStr = passWithPredictable.substring(0, passWithPredictable.length() - 12);
        return passwordStr;
    }

    public static String encryptConfigPassword(final String password, final HSMDevice hsms) throws Exception {
        if (Misc.isNullOrEmpty(password)) {
            return password;
        }

        return encryptConfigPassword(password.toCharArray(), hsms);
    }

    public static String encryptConfigPassword(final char[] password, final HSMDevice hsms) throws Exception {
        StringBuilder sb = new StringBuilder(password.length + 18).append(password);
        long uniPred1 = 0;
        for (int i = 0; i < password.length; i++) {
            uniPred1 += (i + 1) * password[i] + someNumber;
        }
        sb.append(Base64.encode(HexUtils.fromString(Long.toHexString(uniPred1))));

        HSMDevice.Key passwordKey = hsms.generateHSMDataEncryptionKey();
        byte[] encPassword = hsms.encryptData(passwordKey.getValue(), Misc.toBytes(utf8, sb));
        return Base64.encode(passwordKey.getValue()) + ";" + Base64.encode(encPassword);
    }

    public static String sha256WithUniquePredictable(final CharSequence password) {
        return sha256WithUniquePredictable(password.toString().toCharArray());
    }

    public static String sha256WithUniquePredictable(final char[] password) {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        try {
            StringBuilder sb = new StringBuilder(password.length * 2).append(password);
            long uniPred1 = 0;
            for (int i = 0; i < password.length; i++) {
                uniPred1 += (i + 1) * password[i] + someNumber;
            }
            sb.append(Base64.encode(HexUtils.fromString(Long.toHexString(uniPred1))));

            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] digestResult = messageDigest.digest(sb.toString().getBytes(utf8));
            return Base64.encode(digestResult, 128);
        } catch (Exception e) {
            throw new RuntimeException("sha256WithUniquePredictable error", e);
        }
    }
}
