package com.modirum.ds.hsm.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Contains helper methods for HSM layer functionality.
 */
public class HSMUtil {

    private HSMUtil(){
        // util
    }

    /**
     * Returns first <code>count</code> number of bytes from <code>source</code> byte array.
     * @param count Number of head bytes to return.
     * @param source Array to take the bytes from.
     * @return First <code>count</code> bytes from array.
     */
    public static byte[] head(int count, byte[] source) {
        byte[] out = new byte[count];
        System.arraycopy(source, 0, out, 0, count);
        return out;
    }

    /**
     * Split {@code source} into partitioned arrays using delimiter {@code c}.
     *
     * @param source the source array.
     * @param c delimiter.
     * @return the partitioned arrays.
     */
    public static byte[][] split(byte[] source, int c) {
        if (source == null || source.length == 0) {
            return new byte[][] {};
        }

        List<byte[]> bytes = new ArrayList<>();
        int offset = 0;
        for (int i = 0; i <= source.length; i++) {

            if (i == source.length) {

                bytes.add(Arrays.copyOfRange(source, offset, i));
                break;
            }

            if (source[i] == c) {
                bytes.add(Arrays.copyOfRange(source, offset, i));
                offset = i + 1;
            }
        }
        return bytes.toArray(new byte[bytes.size()][]);
    }
}
