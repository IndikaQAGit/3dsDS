package com.modirum.ds.hsm.support;

/**
 * HSM-level enumerator for RSA Decryption pad mode option.
 */
public enum RSADecryptPadMode {
    PKCS1,
    OAEP_SHA1_MGF1,
    OAEP_SHA256_MGF1;
}
