INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy)
VALUES ('DSPublicURL', 'https://$SERVER_AUTH_HOST/ds/DServer', '', '2020-09-28 17:42:25.000', 'admin (1000)'),
  ('DSURL', 'https://$MUTUAL_AUTH_HOST/ds/DServer', '', '2020-09-28 17:42:25.000', 'admin (1000)');

INSERT INTO ds_acsprofiles (id, issuerId, name, URL, threeDSMethodURL, inClientCert, operatorID, outClientCert, refNo, setv, status, SSLProto, startProtocolVersion, endProtocolVersion, lastMod, isid, paymentSystemId)
VALUES (10000, 10000, 'Azure Modirum ACS', 'https://$MUTUAL_AUTH_HOST/mdpayacs/areq', 'https://$SERVER_AUTH_HOST/mdpayacs/3ds-method', NULL, '3DS_LOA_ACS_MOMD_020100_00061', 4, '3DS_LOA_ACS_MOMD_020100_00061', 1, 'A', NULL, '2.1.0', '2.2.0', '2020-08-04 04:02:51.324000', NULL, 0),
  (11000, 11000, 'DS-3DS-EMULATOR', 'http://$SERVER_AUTH_HOST/ds-3ds-emulator/acsAreqStub.jsp', '', NULL, 'ACS-EMULATOR-OPER-ID', NULL, 'ACS-EMULATOR-REF-NO', 1, 'A', NULL, '2.1.0', '2.1.0', '2020-07-24 14:50:51.010000', NULL, 0),
  (12040, 12110, 'Azure Modirum ACS', 'https://$MUTUAL_AUTH_HOST/mdpayacs/areq', 'https://$SERVER_AUTH_HOST/mdpayacs/3ds-method ', '10030', '3DS_LOA_ACS_MOMD_020100_00061', 4, '3DS_LOA_ACS_MOMD_020100_00061', '1', 'A', NULL, '2.1.0', '2.2.0', '2020-11-05 15:37:57.856000', NULL, 0);
