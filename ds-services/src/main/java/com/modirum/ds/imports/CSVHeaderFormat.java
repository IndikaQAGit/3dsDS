package com.modirum.ds.imports;

public enum CSVHeaderFormat {

    // Merchants
    ADD_MERCHANT,
    CHANGE_MERCHANT_STATE,

    // Issuers
    ADD_ISSUER,
    CHANGE_ISSUER_STATE,
    UNKNOWN,

    // Card ranges
    ADD_CARD_RANGE,
    CHANGE_CARD_RANGE_STATE,
}
