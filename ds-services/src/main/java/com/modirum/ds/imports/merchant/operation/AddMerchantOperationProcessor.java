package com.modirum.ds.imports.merchant.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.dao.PersistenceService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AddMerchantOperationProcessor implements IDataOperationProcessor<BatchMerchantModel> {

    private static final Logger LOG = LoggerFactory.getLogger(AddMerchantOperationProcessor.class);

    private final PersistenceService persistenceService;

    private int processedCount;

    public AddMerchantOperationProcessor(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public void process(final List<BatchMerchantModel> merchants) {
        processedCount = 0;
        merchants.stream()
                 .map(this::mapToMerchant)
                 .forEach(this::saveMerchant);
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }

    private Merchant mapToMerchant(final BatchMerchantModel importModel) {
        Merchant merchant = new Merchant();
        merchant.setAcquirerId(Integer.parseInt(importModel.getAcquirerId()));
        merchant.setTdsServerProfileId(Long.parseLong(importModel.getThreeDSProfileId()));
        merchant.setIdentifier(importModel.getRegistrationId());
        merchant.setName(importModel.getName());
        merchant.setURL(importModel.getURL());
        merchant.setPhone(importModel.getPhone());
        merchant.setEmail(importModel.getEmail());
        merchant.setCountry(StringUtils.isNotBlank(importModel.getCountry()) ? importModel.getCountry() : null);
        merchant.setRequestorID(importModel.getRequestorID());
        merchant.setStatus(mapStatus(importModel.getStatus()));
        merchant.setOptions(importModel.getOptions());
        merchant.setPaymentSystemId(Integer.parseInt(importModel.getPaymentSystemId()));
        return merchant;
    }

    private String mapStatus(final String status) {
        switch (status) {
            case "active":
                return DSModel.Merchant.Status.ACTIVE;
            case "disabled":
                return DSModel.Merchant.Status.DISABLED;
        }
        return DSModel.Merchant.Status.DISABLED;
    }

    private void saveMerchant(final Merchant merchant) {
        try {
            persistenceService.save(merchant);
            processedCount++;
        } catch (Exception e) {
            LOG.error("Error while saving merchant in database: " + e.getMessage());
        }
    }
}
