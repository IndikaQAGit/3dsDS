package com.modirum.ds.imports.merchant.parser;

import com.modirum.ds.imports.BaseBatchModel;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class BatchMerchantModel extends BaseBatchModel {

    private String acquirerId;
    private String name;
    private String URL;
    private String phone;
    private String email;
    private String country;
    private String requestorID;
    private String threeDSProfileId;
    private String options;
    private String registrationId;
    private String externalOptions;
    private String status;
    private String acquirerBin;
    private String threeDSOperatorId;
}
