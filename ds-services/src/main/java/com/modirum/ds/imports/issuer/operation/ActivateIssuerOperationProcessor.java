package com.modirum.ds.imports.issuer.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.services.IssuerService;

import java.util.List;

public class ActivateIssuerOperationProcessor implements IDataOperationProcessor<BatchIssuerModel> {

    private final IssuerService issuerService;

    private int processedCount;

    public ActivateIssuerOperationProcessor(final IssuerService issuerService) {
        this.issuerService = issuerService;
    }

    @Override
    public void process(final List<BatchIssuerModel> issuers) {
        issuers.stream()
               .peek(e -> processedCount++)
               .forEach(model -> issuerService.setIssuerActive(model.getName(), Integer.parseInt(model.getPaymentSystemId())));
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }
}
