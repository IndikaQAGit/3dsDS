package com.modirum.ds.imports.merchant.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import com.modirum.ds.services.MerchantService;

import java.util.List;

public class DisableMerchantOperationProcessor implements IDataOperationProcessor<BatchMerchantModel> {

    private final MerchantService merchantService;

    private int processedCount;

    public DisableMerchantOperationProcessor(final MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @Override
    public void process(final List<BatchMerchantModel> merchants) {
        merchants.stream()
                 .peek(e -> processedCount++)
                 .forEach(model -> merchantService.setMerchantDisabled(model.getName(), Integer.parseInt(model.getPaymentSystemId())));
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }
}
