package com.modirum.ds.imports.cardrange.parser;

import com.modirum.ds.imports.BaseBatchModel;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class BatchCardRangeModel extends BaseBatchModel {

    private String issuerName;
    private String binStart;
    private String binEnd;
    private String status;
    private String alias;
    private String cardType;
    private String set;

    private String acsName;
    private String acsRefNumber;
    private String acsUrl;
    private String acsProtocolStart;
    private String acsProtocolEnd;
    private String acsOperatorId;
    private String acs3DSMethodUrl;
    private String acsProfileStatus;
    private String acsSet;
}
