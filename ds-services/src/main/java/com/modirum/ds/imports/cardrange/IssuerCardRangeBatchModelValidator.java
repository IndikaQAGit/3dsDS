package com.modirum.ds.imports.cardrange;

import com.modirum.ds.imports.AbstractBatchDataValidator;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.imports.ValidationsList;
import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.PaymentSystemsService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import com.modirum.ds.util.DSUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Collections;
import java.util.stream.Collectors;

import static com.modirum.ds.imports.cardrange.parser.IssuerCardRangeCSVParser.CSV_HEADER_ACS_PROFILE_STATUS;
import static com.modirum.ds.imports.cardrange.parser.IssuerCardRangeCSVParser.CSV_HEADER_ACS_REF_NUMBER;

public class IssuerCardRangeBatchModelValidator extends AbstractBatchDataValidator<BatchCardRangeModel> {

    private static final List<String> SUPPORTED_STATUSES = Arrays.asList("participating", "notparticipating");

    private final IssuerService issuerService;

    protected IssuerCardRangeBatchModelValidator(final DataBatchHandlerConfiguration configuration,
                                                 final IssuerService issuerService,
                                                 final PaymentSystemsService paymentSystemsService) {
        super(configuration, paymentSystemsService);
        this.issuerService = issuerService;
    }

    @Override
    protected Map<CSVHeaderFormat, ValidationsList<BatchCardRangeModel>> initValidators() {
        final Map<CSVHeaderFormat, ValidationsList<BatchCardRangeModel>> map = new HashMap<>();

        ValidationsList<BatchCardRangeModel> addValidations = new ValidationsList<>();
        addValidations.add(this::isValidPaymentSystemId);
        addValidations.add(this::isValidBinStartEnd);
        addValidations.add(this::isValidCardType);
        addValidations.add(this::isValidStatus);
        addValidations.add(this::isValidACSFields);
        addValidations.add(this::isIssuerExists);
        addValidations.add(this::isCardRangeUniqueInCSV);
        addValidations.add(this::isCardRangeWithoutOverlap);
        map.put(CSVHeaderFormat.ADD_CARD_RANGE, addValidations);

        ValidationsList<BatchCardRangeModel> changeValidations = new ValidationsList<>();
        changeValidations.add(this::isValidPaymentSystemId);
        changeValidations.add(this::isValidBinStartEnd);
        changeValidations.add(this::isCardRangeUniqueInCSV);
        map.put(CSVHeaderFormat.CHANGE_CARD_RANGE_STATE, changeValidations);
        return map;
    }

    private Boolean isValidACSFields(FilterParams<BatchCardRangeModel> params) {
        boolean result = true;
        for (final BatchCardRangeModel model: params.importModels) {
            final String acsUrl = model.getAcsUrl();
            if (acsUrl != null) {
                if (model.getAcsRefNumber() == null) {
                    result = false;
                    addError(acsMissingRefNumberError());
                    if (!params.continueOnError) {
                        return result;
                    }
                } else if (model.getAcsProfileStatus() == null) {
                    result = false;
                    addError(acsAcsProfileStatusError());
                    if (!params.continueOnError) {
                        return result;
                    }
                }
            }
            if (model.getAcsProtocolStart() != null) {
                TDSModel.MessageVersion version = TDSModel.MessageVersion.fromValue(model.getAcsProtocolStart());
                if (version == null) {
                    result = false;
                    addError(notSupportedMessageVersionError(model.getAcsProtocolStart()));
                    if (!params.continueOnError) {
                        return result;
                    }
                }
            }
            if (model.getAcsProtocolEnd() != null) {
                TDSModel.MessageVersion version = TDSModel.MessageVersion.fromValue(model.getAcsProtocolEnd());
                if (version == null) {
                    result = false;
                    addError(notSupportedMessageVersionError(model.getAcsProtocolEnd()));
                    if (!params.continueOnError) {
                        return result;
                    }
                }
            }
        }
        return result;
    }

    private Boolean isValidStatus(final FilterParams<BatchCardRangeModel> params) {
        boolean result = true;
        Set<String> statuses = DSUtils.toSet(params.importModels.stream()
                .map(BatchCardRangeModel::getStatus)
                .filter(Objects::nonNull));
        statuses.removeAll(SUPPORTED_STATUSES);
        if (!statuses.isEmpty()) {
            result = false;
            addError(invalidStatuses(statuses));
            if (!params.continueOnError) {
                return result;
            }
        }
        return result;
    }

    private Boolean isValidCardType(final FilterParams<BatchCardRangeModel> params) {
        boolean result = true;
        for (final BatchCardRangeModel model: params.importModels) {
            try {
                Integer.valueOf(model.getCardType());
            } catch (NumberFormatException e) {
                result = false;
                addError(invalidCardTypeFormatError(model.getCardType()));
                if (!params.continueOnError) {
                    return result;
                }
            }
        }

        return result;
    }

    private boolean isIssuerExists(final FilterParams<BatchCardRangeModel> params) {
        // proceed only when payment system ID is valid
        if (!isValidPaymentSystemId(params)) {
            return false;
        }

        boolean result = true;
        for (final BatchCardRangeModel model: params.importModels) {

            if (StringUtils.isNotEmpty(model.getIssuerName()) &&
                NumberUtils.isDigits(model.getPaymentSystemId()) &&
                !issuerService.isIssuerExists(model.getIssuerName(), Integer.parseInt(model.getPaymentSystemId()), null)) {

                result = false;
                addError(issuerDoesntExistsForPaymentSystemError(model.getIssuerName(), model.getPaymentSystemId()));
                if (!params.continueOnError) {
                    return result;
                }
            }
        }

        return result;
    }

    private boolean isCardRangeWithoutOverlap(final FilterParams<BatchCardRangeModel> params) {
        // proceed only when payment system ID and BIN are valid
        if (!isValidPaymentSystemId(params) || !isValidBinStartEnd(params)) {
            return false;
        }

        boolean result = true;
        for (final BatchCardRangeModel model: params.importModels) {
            Integer paymentSystemId = Integer.parseInt(model.getPaymentSystemId());
            try {
                CardRange overlappingCardRange = issuerService.getBestMatchingIssuerCardBinByRange(model.getBinStart(), paymentSystemId);
                if (overlappingCardRange == null) {
                    overlappingCardRange = issuerService.getBestMatchingIssuerCardBinByRange(model.getBinEnd(), paymentSystemId);
                }
                if (overlappingCardRange != null) {
                    result = false;
                    addError(cardRangeWithOverlapError(model.getBinStart(), model.getBinEnd(), paymentSystemId));
                    if (!params.continueOnError) {
                        return result;
                    }
                }
            } catch (Exception e) {
                result = false;
                addError(validationError());
                if (!params.continueOnError) {
                    return result;
                }
            }
        }

        return result;
    }

    private boolean isValidBinStartEnd(final FilterParams<BatchCardRangeModel> params) {
        boolean result = true;
        for (BatchCardRangeModel model : params.importModels) {
            long end = 0;
            try {
                end = Long.parseLong(model.getBinEnd());
            } catch (NumberFormatException e) {
                result = false;
                addError(invalidBinFormatError(model.getBinEnd()));
                if (!params.continueOnError) {
                    return result;
                }
            }

            long start = 0;
            try {
                start = Long.parseLong(model.getBinStart());
            } catch (NumberFormatException e) {
                result = false;
                addError(invalidBinFormatError(model.getBinEnd()));
                if (!params.continueOnError) {
                    return result;
                }
            }

            if (end - start < 0) {
                result = false;
                addError(invalidBinErrors(start, end));
                if (!params.continueOnError) {
                    return result;
                }
            }
        }

        return result;
    }

    private boolean isValidPaymentSystemId(final FilterParams<BatchCardRangeModel> params) {
        List<String> paymentSystemIds = params.importModels
            .stream()
            .map(BatchCardRangeModel::getPaymentSystemId)
            .collect(Collectors.toList());
        return isValidPaymentSystemsIDs(params.continueOnError, paymentSystemIds);
    }

    private boolean isCardRangeUniqueInCSV(final FilterParams<BatchCardRangeModel> params) {
        // proceed only when payment system ID and BIN are valid
        if (!isValidPaymentSystemId(params) || !isValidBinStartEnd(params)) {
            return false;
        }

        boolean result = true;
        List<String> rangePaymentSystemList = params.importModels.stream()
                .map(model -> model.getBinStart() + "," + model.getBinEnd() + "," + model.getPaymentSystemId())
                .collect(Collectors.toList());
        for (BatchCardRangeModel model : params.importModels) {
            String rangePaymentSystem = model.getBinStart() + "," + model.getBinEnd() + "," + model.getPaymentSystemId();
            int frequency = Collections.frequency(rangePaymentSystemList, rangePaymentSystem);
            if (frequency > 1) {
                addError(cardRangeNotUniqueInCSVError(frequency, model.getBinStart(), model.getBinEnd(), Integer.parseInt(model.getPaymentSystemId())));
                result = false;
                if (!params.continueOnError) {
                    return false;
                }
            }
        }
        return result;
    }

    private String validationError() {
        return "Error occurred while validating CSV file";
    }

    private String issuerDoesntExistsForPaymentSystemError(final String issuerName, final String paymentSystemId) {
        return String.format("Issuer doesn't exist for payment system ID (%s): %s", paymentSystemId, issuerName);
    }

    private String invalidBinFormatError(final String str) {
        return String.format("Invalid numeric BIN: %s", str);
    }

    private String invalidCardTypeFormatError(final String cardType) {
        return String.format("Invalid numeric card type: %s", cardType);
    }

    private String invalidBinErrors(final long start, final long end) {
        return String.format("BIN start should be less then end: start - %s, end - %s", start, end);
    }

    private String cardRangeWithOverlapError(final String start, final String end, final Integer paymentSystemId) {
        return String.format("Found BIN range overlapping with another range in the system under the same payment system ID (%d): %s - %s", paymentSystemId, start, end);
    }

    private String cardRangeNotUniqueInCSVError(final Integer frequency, final String start, final String end, final Integer paymentSystemId) {
        return String.format("BIN range %s - %s under payment system ID %d exists %d times in the batch upload file", start, end, paymentSystemId, frequency);
    }

    private String acsAcsProfileStatusError() {
        return String.format("If ACS URL is set, ACS Profile status has to be set as well. (Field name - %s)", CSV_HEADER_ACS_PROFILE_STATUS);
    }

    private String acsMissingRefNumberError() {
        return String.format("If ACS URL is set, ACS Reference number has to be set as well. (Field name - %s)", CSV_HEADER_ACS_REF_NUMBER);
    }

    private String notSupportedMessageVersionError(final String version) {
        return String.format("Message version '%s' is not supported", version);
    }

    private String invalidStatuses(final Set<String> statuses) {
        return String.format("Invalid card statuses: %s", statuses);
    }
}
