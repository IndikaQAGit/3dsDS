package com.modirum.ds.imports;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public abstract class AbstractCsvParser<M extends BaseBatchModel> {

    /**
     * Provides a map of supported headers types of CSV file.
     */
    protected abstract Map<CSVHeaderFormat, HeaderFormat> getHeaderFormats();

    /**
     * Handles header.
     */
    protected abstract DataBatchBundle<M> handleHeaderFormat(final CSVHeaderFormat headerDataFormat,
                                                             final List<String> headerNames,
                                                             final List<CSVRecord> records) throws CSVParserException, IOException;

    public DataBatchBundle<M> parse(final String fileContent) throws CSVParserException {
        try {
            CSVParser csvParser = initCSVParser(fileContent);
            CSVHeaderFormat headerDataFormat = getHeaderFormat(csvParser);
            if (CSVHeaderFormat.UNKNOWN.equals(headerDataFormat)) {
                throw new CSVParserException("Provided CSV file doesn't have required header elements.");
            }
            return handleHeaderFormat(headerDataFormat, csvParser.getHeaderNames(), csvParser.getRecords());
        } catch (IOException e) {
            throw new CSVParserException("Error during paring CVS file: " + e.getMessage(), e);
        }
    }

    protected CSVParser initCSVParser(final String fileContent) throws IOException {
        return CSVParser.parse(fileContent, CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .withIgnoreEmptyLines()
                .withIgnoreSurroundingSpaces()
                .withDelimiter(',')
                .withIgnoreHeaderCase()
                .withTrim());
    }

    public CSVHeaderFormat getHeaderFormat(final CSVParser csvParser) {
        Map<CSVHeaderFormat, HeaderFormat> headerFormats = getHeaderFormats();
        final List<String> headerNames = csvParser.getHeaderNames();
        for (final CSVHeaderFormat type : headerFormats.keySet()) {
            HeaderFormat headerFormat = headerFormats.get(type);
            if (headerNames.containsAll(headerFormat.getRequiredFields())) {
                if (!headerFormat.hasOptionalFields()) {
                    if (headerFormat.getRequiredFields().size() == headerNames.size()) {
                        return type;
                    }
                } else {
                    return type;
                }
            }
        }
        return CSVHeaderFormat.UNKNOWN;
    }

    public void setField(final List<String> headerNames, final String fieldName, final CSVRecord record, Consumer<String> consumer) {
        if (headerNames.contains(fieldName)) {
            consumer.accept(record.get(fieldName));
        }
    }

    public static class HeaderFormat {

        private final List<String> requiredFields;
        private final boolean hasOptionalFields;

        public HeaderFormat(final List<String> requiredFields, final boolean hasOptionalFields) {
            this.requiredFields = requiredFields;
            this.hasOptionalFields = hasOptionalFields;
        }

        public List<String> getRequiredFields() {
            return requiredFields;
        }

        public boolean hasOptionalFields() {
            return hasOptionalFields;
        }
    }
}
