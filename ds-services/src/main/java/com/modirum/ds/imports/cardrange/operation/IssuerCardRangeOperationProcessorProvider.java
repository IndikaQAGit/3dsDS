package com.modirum.ds.imports.cardrange.operation;

import com.modirum.ds.imports.AbstractOperationProcessorProvider;
import com.modirum.ds.imports.BaseBatchModel;
import com.modirum.ds.imports.DataChangeStateOperation;
import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;

import java.util.HashMap;
import java.util.Map;

public class IssuerCardRangeOperationProcessorProvider extends AbstractOperationProcessorProvider {

    public IssuerCardRangeOperationProcessorProvider(final PersistenceService persistenceService) {
        super(persistenceService);
    }

    @Override
    public Map<DataChangeStateOperation, IDataOperationProcessor<? extends BaseBatchModel>> initProviders() {
        Map<DataChangeStateOperation, IDataOperationProcessor<? extends BaseBatchModel>> processors = new HashMap<>();
        processors.put(DataChangeStateOperation.ADD, new AddIssuerCardRangeOperation(this.persistenceService, ServiceLocator.getInstance().getIssuerService()));
        processors.put(DataChangeStateOperation.DELETE, new DeleteIssuerCardRangeOperation(ServiceLocator.getInstance().getCardRangeService()));
        processors.put(DataChangeStateOperation.ACTIVATE, new ActivateIssuerCardRangeOperation(ServiceLocator.getInstance().getCardRangeService()));
        processors.put(DataChangeStateOperation.DISABLE, new DisableIssuerCardRangeOperation(ServiceLocator.getInstance().getCardRangeService()));
        return processors;
    }
}
