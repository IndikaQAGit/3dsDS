package com.modirum.ds.imports.cardrange.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;
import com.modirum.ds.services.CardRangeService;

import java.util.List;

public class DisableIssuerCardRangeOperation implements IDataOperationProcessor<BatchCardRangeModel> {

    private final CardRangeService cardRangeService;

    private int processedCount;

    public DisableIssuerCardRangeOperation(final CardRangeService cardRangeService) {
        this.cardRangeService = cardRangeService;
    }

    @Override
    public void process(final List<BatchCardRangeModel> cardRangeModels) {
        cardRangeModels.stream()
                       .peek(e -> processedCount++)
                       .forEach(model -> cardRangeService.setCardRangeNotParticipating(model.getBinStart(), model.getBinEnd(), model.getPaymentSystemId()));
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }
}
