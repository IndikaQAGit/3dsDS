package com.modirum.ds.imports.cardrange.parser;

import com.modirum.ds.imports.AbstractCsvParser;
import com.modirum.ds.imports.CSVParserException;
import com.modirum.ds.imports.DataChangeStateOperation;
import com.modirum.ds.imports.DataBatchBundle;
import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.util.DSUtils;
import org.apache.commons.csv.CSVRecord;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IssuerCardRangeCSVParser extends AbstractCsvParser<BatchCardRangeModel> {

    public static final String CSV_HEADER_ACTION = "action";

    public static final String CSV_HEADER_ISSUER_NAME        = "issuername";
    public static final String CSV_HEADER_START              = "start";
    public static final String CSV_HEADER_END                = "end";
    public static final String CSV_HEADER_CARD_TYPE          = "cardtype";
    public static final String CSV_HEADER_STATUS             = "status";
    public static final String CSV_HEADER_PAYMENT_SYSTEM_ID  = "paymentSystemId";
    public static final String CSV_HEADER_ALIAS              = "alias";
    public static final String CSV_HEADER_SET                = "set";

    public static final String CSV_HEADER_ACS_NAME           = "acsname";
    public static final String CSV_HEADER_ACS_REF_NUMBER     = "acsrefnumber";
    public static final String CSV_HEADER_ACS_URL            = "acsurl";
    public static final String CSV_HEADER_ACS_PROTOCOL_START = "acsprotocolstart";
    public static final String CSV_HEADER_ACS_PROTOCOL_END   = "acsprotocolend";
    public static final String CSV_HEADER_ACS_OPERATOR_ID    = "acsoperatorid";
    public static final String CSV_HEADER_ACS_3DS_METHOD_URL = "acsmethodurl";
    public static final String CSV_HEADER_ACS_PROFILE_STATUS = "acsstatus";
    public static final String CSV_HEADER_ACS_SET            = "acsset";

    @Override
    protected Map<CSVHeaderFormat, HeaderFormat> getHeaderFormats() {
        Map<CSVHeaderFormat, HeaderFormat> headerFormats = new HashMap<>();
        headerFormats.put(CSVHeaderFormat.CHANGE_CARD_RANGE_STATE, new HeaderFormat(Arrays.asList(CSV_HEADER_ACTION, CSV_HEADER_PAYMENT_SYSTEM_ID, CSV_HEADER_START, CSV_HEADER_END), true));
        headerFormats.put(CSVHeaderFormat.ADD_CARD_RANGE, new HeaderFormat(Arrays.asList(CSV_HEADER_ISSUER_NAME, CSV_HEADER_START, CSV_HEADER_END, CSV_HEADER_CARD_TYPE, CSV_HEADER_STATUS, CSV_HEADER_PAYMENT_SYSTEM_ID), true));
        return headerFormats;
    }

    @Override
    protected DataBatchBundle<BatchCardRangeModel> handleHeaderFormat(final CSVHeaderFormat headerDataFormat,
                                                                      final List<String> headerNames,
                                                                      final List<CSVRecord> records) throws CSVParserException {
        List<BatchCardRangeModel> importModels;
        switch (headerDataFormat) {
            case ADD_CARD_RANGE:
                importModels = DSUtils.toList(records.stream().map(record -> mapRecordToIssuerCardRangeImportModel(headerNames, record)));
                break;
            case CHANGE_CARD_RANGE_STATE:
                importModels = DSUtils.toList(records.stream().map(record -> mapRecordToIssuerChangeStateCardRange(headerNames, record)));
                break;
            default:
                throw new CSVParserException("Unsupported header data format: " + headerDataFormat);
        }
        return new DataBatchBundle<>(headerDataFormat, importModels);
    }

    private BatchCardRangeModel mapRecordToIssuerCardRangeImportModel(final List<String> headerNames, final CSVRecord record) {
        BatchCardRangeModel.BatchCardRangeModelBuilder builder = BatchCardRangeModel.builder();
        builder.operation(DataChangeStateOperation.ADD);

        // Required fields
        setField(headerNames, CSV_HEADER_ISSUER_NAME, record, builder::issuerName);
        setField(headerNames, CSV_HEADER_START, record, builder::binStart);
        setField(headerNames, CSV_HEADER_END, record, builder::binEnd);
        setField(headerNames, CSV_HEADER_CARD_TYPE, record, builder::cardType);
        setField(headerNames, CSV_HEADER_STATUS, record, builder::status);
        setField(headerNames, CSV_HEADER_PAYMENT_SYSTEM_ID, record, builder::paymentSystemId);

        // Optional fields
        setField(headerNames, CSV_HEADER_ALIAS, record, builder::alias);
        setField(headerNames, CSV_HEADER_SET, record, builder::set);
        setField(headerNames, CSV_HEADER_ACS_NAME, record, builder::acsName);
        setField(headerNames, CSV_HEADER_ACS_REF_NUMBER, record, builder::acsRefNumber);
        setField(headerNames, CSV_HEADER_ACS_URL, record, builder::acsUrl);
        setField(headerNames, CSV_HEADER_ACS_PROTOCOL_START, record, builder::acsProtocolStart);
        setField(headerNames, CSV_HEADER_ACS_PROTOCOL_END, record, builder::acsProtocolEnd);
        setField(headerNames, CSV_HEADER_ACS_OPERATOR_ID, record, builder::acsOperatorId);
        setField(headerNames, CSV_HEADER_ACS_3DS_METHOD_URL, record, builder::acs3DSMethodUrl);
        setField(headerNames, CSV_HEADER_ACS_PROFILE_STATUS, record, builder::acsProfileStatus);
        setField(headerNames, CSV_HEADER_ACS_SET, record, builder::acsSet);
        return builder.build();
    }

    private BatchCardRangeModel mapRecordToIssuerChangeStateCardRange(final List<String> headerNames, final CSVRecord record) {
        BatchCardRangeModel.BatchCardRangeModelBuilder builder = BatchCardRangeModel.builder();

        // Required fields
        setField(headerNames, CSV_HEADER_PAYMENT_SYSTEM_ID, record, builder::paymentSystemId);
        setField(headerNames, CSV_HEADER_START, record, builder::binStart);
        setField(headerNames, CSV_HEADER_END, record, builder::binEnd);
        DataChangeStateOperation operation = DataChangeStateOperation.getValue(record.get(CSV_HEADER_ACTION).toUpperCase());
        if (DataChangeStateOperation.ADD.equals(operation)) {
            throw new IllegalStateException("'add' is not a valid action for this CSV file format.");
        }
        builder.operation(operation);
        return builder.build();
    }
}
