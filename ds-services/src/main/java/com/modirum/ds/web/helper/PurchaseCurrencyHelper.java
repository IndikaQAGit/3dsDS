package com.modirum.ds.web.helper;

import com.modirum.ds.utils.ISO4217;
import com.modirum.ds.utils.Misc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PurchaseCurrencyHelper {

    private static final String DISALLOW_ALL = "-*";
    private static final String DISALLOWED = "-";

    private boolean limitedCurrencySupported;

    private List<String> allowed = new ArrayList<>();
    private List<String> disallowed = new ArrayList<>();

    private final Map<String, String> excludedCurrencies;

    public PurchaseCurrencyHelper(final String purchaseCurrencySetup, final Map<String, String> excludedCurrencies) {
        this.excludedCurrencies = excludedCurrencies;
        if (purchaseCurrencySetup != null) {
            init(purchaseCurrencySetup);
        }
    }

    private void init(final String purchaseCurrencySetup) {
        for (String currency : purchaseCurrencySetup.split(",")) {
            if (currency.equals(DISALLOW_ALL)) {
                limitedCurrencySupported = true;
            } else if (currency.startsWith(DISALLOWED)) {
                disallowed.add(currency.substring(1));
            } else {
                allowed.add(currency);
            }
        }
    }

    public boolean isExcluded(String currency) {
        if (excludedCurrencies.get(currency) != null) {
            return true;
        } else {
            return disallowed.contains(currency);
        }
    }

    public boolean isLimitedNotSupported(String currency) {
        return limitedCurrencySupported && !allowed.contains(currency);
    }

    public String getAllowedAsString() {
        return String.join(", ", this.allowed);
    }

    public List<String> getAllowed() {
        return Collections.unmodifiableList(this.allowed);
    }

    public boolean isSupported(String currency) {
        ISO4217 isoCurrency = ISO4217.findFromNumeric((short) Misc.parseInt(currency));
        if (isoCurrency == null) {
            return allowed.contains(currency);
        } else if (limitedCurrencySupported) {
            return excludedCurrencies.get(currency) == null && allowed.contains(currency);
        } else {
            return excludedCurrencies.get(currency) == null && !disallowed.contains(currency);
        }
    }
}