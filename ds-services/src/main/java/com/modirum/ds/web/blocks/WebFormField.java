/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.blocks;

import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "##default")
public class WebFormField implements java.io.Serializable {

    private static final long serialVersionUID = -1271034588756814461L;

    public final static String TYPE_TEXT = "text";
    public final static String TYPE_PASSWORD = "password";
    public final static String TYPE_BUTTON = "button";
    public final static String TYPE_SUBMIT = "submit";
    public final static String TYPE_HIDDEN = "hidden";
    public final static String TYPE_IMAGE = "image";


    String type = null;
    String name = null;
    String value = null;
    String id = null;
    String src = null; // for image input..

    public WebFormField() {
        this(null, null);
    }

    public WebFormField(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Method getName
     *
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     *
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method getType
     *
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }

    /**
     * Method setType
     *
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Method getValue
     *
     * @return Returns the value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Method setValue
     *
     * @param value The value to set.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Method getId
     *
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }

    /**
     * Method setId
     *
     * @param id The id to set.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Method getSrc
     *
     * @return Returns the src.
     */
    public String getSrc() {
        return src;
    }

    /**
     * Method setSrc
     *
     * @param src The src to set.
     */
    public void setSrc(String src) {
        this.src = src;
    }
}
