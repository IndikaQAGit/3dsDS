/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.blocks;

import javax.xml.bind.annotation.XmlType;

/**
 * @author andri
 */
@XmlType(namespace = "##default")
public class Error implements java.io.Serializable {

    private static final long serialVersionUID = -5259089303860673396L;
    String err = null;
    String msg = null;
    String errorCode = null;

    public Error() {
    }

    public Error(String err, String msg) {
        this.err = err;
        this.msg = msg;
    }

    /**
     * Method getErr
     *
     * @return Returns the err.
     */
    public String getErr() {
        return err;
    }

    /**
     * Method setErr
     *
     * @param err The err to set.
     */
    public void setErr(String err) {
        this.err = err;
    }

    /**
     * Method getMsg
     *
     * @return Returns the msg.
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Method setMsg
     *
     * @param msg The msg to set.
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
