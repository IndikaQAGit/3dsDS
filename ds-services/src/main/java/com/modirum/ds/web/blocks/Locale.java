/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 06.12.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.blocks;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
@XmlType(namespace = "##default")
public class Locale {
    private java.util.Locale l;

    public Locale() {
    }

    public Locale(java.util.Locale l) {
        this.l = l;
    }

    public String getISO3Language() {
        return l != null ? l.getISO3Language() : null;
    }

    public String getLanguage() {
        return l != null ? l.getLanguage() : null;
    }

    public void setLanguage(String s) {

    }


    public String getDisplayName() {
        return l != null ? l.getDisplayName() : null;
    }

    public String getCountry() {
        return l != null ? l.getCountry() : null;
    }

    public void setISO3Language(String v) {

    }

    public void setDdisplayName(String v) {

    }

    public void setCountry(String c) {

    }
}
