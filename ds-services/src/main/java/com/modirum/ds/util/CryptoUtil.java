package com.modirum.ds.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * Utility class for common crypto related operations.
 */
public class CryptoUtil {

    public static final byte[] ZERO_BLOCK_8 = {0, 0, 0, 0, 0, 0, 0, 0};
    public static final byte[] ZERO_BLOCK_16 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    private CryptoUtil() {
        // utility class
    }

    /**
     * GZIP deflates input bytes.
     * @param bytes Input data to compress.
     * @return Compressed data.
     */
    public static byte[] gzip(final byte[] bytes) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DeflaterOutputStream deflaterOutput = new DeflaterOutputStream(bos);
            deflaterOutput.write(bytes);
            deflaterOutput.close();
            bos.flush();
            return bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * GZIP inflates input bytes.
     * @param bytes Input data to decompress.
     * @return Decompressed data.
     */
    public static byte[] gunzip(final byte[] bytes) {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            InflaterInputStream inflaterInput = new InflaterInputStream(bis);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int read = inflaterInput.read(buffer);
                if (read == -1) {
                    break;
                }
                bos.write(buffer, 0, read);
            }
            bos.flush();
            return bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Produces sha-512 of the input <code>value</code>.
     * @param value Original value to calculate sha-512 for.
     * @return sha-512 of the input.
     */
    public static byte[] SHA512(byte[] value) {
        try {
            MessageDigest mdigest = MessageDigest.getInstance("SHA-512");
            byte[] digestResult = mdigest.digest(value);
            return digestResult;
        } catch (Exception e) {
            throw new RuntimeException("SHA512 error", e);
        }
    }

    /**
     * Produces sha-256 of the input <code>value</code>.
     * @param value Original value to calculate sha-256 for.
     * @return sha-256 of the input.
     */
    public static byte[] SHA256(byte[] value) {
        try {
            MessageDigest mdigest = MessageDigest.getInstance("SHA-256");
            byte[] digestResult = mdigest.digest(value);
            return digestResult;
        } catch (Exception e) {
            throw new RuntimeException("SHA256 error", e);
        }
    }
}
