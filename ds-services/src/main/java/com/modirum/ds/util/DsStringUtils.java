package com.modirum.ds.util;

import com.modirum.ds.utils.Misc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DsStringUtils {
    /**
     * This extracts substring starting from the end backwards with the size provided by @param lastCharactersSize.
     * @param lastCharactersSize
     * @return
     */
    public static String extractLast(int lastCharactersSize, String originalValue) {
        if (Misc.isNotNullOrEmpty(originalValue)) {
            int length = originalValue.length();
            if (length > lastCharactersSize) {
              return originalValue.substring(length - lastCharactersSize, length);
            } else {
                return originalValue;
            }
        }

        return originalValue;
    }

    /**
     * This extracts substring from the start with the size provided by @param firstCharactersSize.
     * @param firstCharactersSize
     * @return
     */
    public static String extractFirst(int firstCharactersSize, String originalValue) {
        if (Misc.isNotNullOrEmpty(originalValue)) {
            int length = originalValue.length();
            if (length > firstCharactersSize) {
                return originalValue.substring(0, firstCharactersSize);
            } else {
                return originalValue;
            }
        }

        return originalValue;
    }

    public static List<String> splitToList(String value, String delimiter) {
        return splitToList(value, delimiter, true);
    }

    /**
     * Split a String value by the given delimiter and build the result into ArrayList
     * @param value The String value to be splitted
     * @param delimiter The delimiter regex pattern
     * @param trim if true, will trim leading and trailing spaces on the splitted values
     * @return
     */
    public static List<String> splitToList(String value, String delimiter, boolean trim) {
        if (Misc.isNotNullOrEmpty(value)) {
            String[] multipleValues = value.split(delimiter);
            for (int index = 0; index < multipleValues.length; index++) {
                multipleValues[index] = multipleValues[index].trim();
            }
            return Arrays.asList(multipleValues);
        } else {
            return Collections.emptyList();
        }
    }
}
