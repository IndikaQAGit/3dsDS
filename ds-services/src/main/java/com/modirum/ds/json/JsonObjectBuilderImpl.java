/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 19. jaan 2017
 *
 */
package com.modirum.ds.json;

import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class JsonObjectBuilderImpl implements JsonObjectBuilder {

    static Logger log = Logger.getLogger("com.modirum.ds.json.JsonObjectBuilderImpl");

    static {
        log.info(
                "Loading duplicate field processing json object builder com.modirum.ds.json.JsonObjectBuilderImpl by AKruus");
    }

    public static final ThreadLocal<Map<String, AtomicInteger>> duplicateFieldCountsTL = new ThreadLocal<>();

    private Map<String, Object> valueMapForCount;
    private Map<String, AtomicInteger> duplicateFieldCounts;
    javax.json.JsonObjectBuilder gob;

    public JsonObjectBuilderImpl(javax.json.JsonObjectBuilder gob) {
        this.gob = gob;
    }

    public JsonObjectBuilder add(String name, JsonValue value) {
        gob.add(name, value);
        putValueMap(name, value);
        return this;
    }

    public JsonObjectBuilder add(String name, String value) {
        gob.add(name, value);
        putValueMap(name, value);
        return this;
    }

    public JsonObjectBuilder add(String name, BigInteger value) {
        gob.add(name, value);
        putValueMap(name, value);
        return this;
    }

    public JsonObjectBuilder add(String name, BigDecimal value) {
        gob.add(name, value);
        putValueMap(name, value);
        return this;
    }

    public JsonObjectBuilder add(String name, int value) {
        gob.add(name, value);
        putValueMap(name, value);
        return this;
    }

    public JsonObjectBuilder add(String name, long value) {
        gob.add(name, value);
        putValueMap(name, value);
        return this;
    }

    public JsonObjectBuilder add(String name, double value) {
        gob.add(name, value);
        putValueMap(name, value);
        return this;
    }

    public JsonObjectBuilder add(String name, boolean value) {
        gob.add(name, value);
        putValueMap(name, value);
        return this;
    }

    public JsonObjectBuilder addNull(String name) {
        gob.addNull(name);
        putValueMap(name, "NULL");
        return this;
    }

    public JsonObjectBuilder add(String name, JsonObjectBuilder builder) {
        gob.add(name, builder);
        putValueMap(name, ""); //builder.build());
        return this;
    }

    public JsonObjectBuilder add(String name, JsonArrayBuilder builder) {
        gob.add(name, builder);
        putValueMap(name, "");//builder.build());
        return this;
    }

    public JsonObject build() {
        duplicateFieldCountsTL.remove();
        return gob.build();
    }

    private void putValueMap(String name, Object value) {
        if (this.duplicateFieldCounts == null) {
            this.valueMapForCount = new LinkedHashMap<String, Object>();
            this.duplicateFieldCounts = duplicateFieldCountsTL.get();
        }

        if (valueMapForCount.get(name) != null) {
            if (duplicateFieldCounts != null) {
                AtomicInteger count = duplicateFieldCounts.get(name);
                if (count == null) {
                    count = new AtomicInteger(2);
                    duplicateFieldCounts.put(name, count);
                } else {
                    count.incrementAndGet();
                }
            }
        }
        valueMapForCount.put(name, value);
    }

    @Override
    public JsonObjectBuilder remove(String name) {
        gob.remove(name);
        putValueMap(name, null);
        return this;
    }
}
