/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 21. aug 2017
 *
 */
package com.modirum.ds.json;

import javax.json.spi.JsonProvider;
import java.io.InputStream;
import java.io.Reader;
import java.util.Map;
import java.util.logging.Logger;


public class JsonProviderImpl extends org.glassfish.json.JsonProviderImpl {

    static Logger log = Logger.getLogger("com.modirum.ds.json.JsonProviderImpl");

    static {
        log.info("Loading duplicate field detection supporting com.modirum.ds.json.JsonProviderImpl by AKruus");
    }

    static JsonProviderImpl inst = null;

    public JsonProviderImpl() {
        super();
    }

    @Override
    public javax.json.JsonObjectBuilder createObjectBuilder() {
        return new com.modirum.ds.json.JsonObjectBuilderImpl(super.createObjectBuilder());
    }

    @Override
    public javax.json.JsonBuilderFactory createBuilderFactory(Map<String, ?> config) {
        return new com.modirum.ds.json.JsonBuilderFactoryImpl(super.createBuilderFactory(config));
    }

    @Override
    public javax.json.JsonReader createReader(Reader reader) {
        return new com.modirum.ds.json.JsonReaderImpl(reader);
    }

    @Override
    public javax.json.JsonReader createReader(InputStream in) {
        return new com.modirum.ds.json.JsonReaderImpl(in);
    }

    public static JsonProvider provider() {
        if (inst == null) {
            inst = new JsonProviderImpl();
        }

        return inst;
    }
}