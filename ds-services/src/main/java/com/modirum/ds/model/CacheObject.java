/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 2. veebr 2016
 *
 */
package com.modirum.ds.model;

public class CacheObject<T> {

    public long loaded;

    public T obj;
}
