package com.modirum.ds.model.threeds;

/**
 * Predefined 01-06 values for threeDSRequestorAuthenticationInd starting on EMVco version 2.1.0.
 */
public enum ThreeDSRequestorAuthenticationIndicator {
    PAYMENT_TRANSACTION("01"),
    RECURRING_TRANSACTION("02"),
    INSTALMENT_TRANSACTION("03"),
    ADD_CARD("04"),
    MAINTAIN_CARD("05"),
    CARDHOLDER_VERIFICATION("06");

    private final String value;

    ThreeDSRequestorAuthenticationIndicator(String value) {
        this.value = value;
    }

    public boolean isEqual(String value) {
        return this.value.equals(value);
    }
}