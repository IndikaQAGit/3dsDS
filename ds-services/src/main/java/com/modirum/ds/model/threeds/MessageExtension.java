package com.modirum.ds.model.threeds;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Java Model representing a messageExtension object in 3DS Message.
 */
@JsonInclude(JsonInclude.Include. NON_NULL)
public class MessageExtension {

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private String id;

    @JsonProperty("criticalityIndicator")
    private boolean criticalityIndicator;

    @JsonProperty("data")
    private Map<String, Object> data;

    public MessageExtension() {
    }

    public MessageExtension(String name, String id, boolean criticalityIndicator, Map<String, Object> data) {
        this.name = name;
        this.id = id;
        this.criticalityIndicator = criticalityIndicator;
        this.data = data;
    }

    public boolean isCriticalityIndicator() {
        return criticalityIndicator;
    }

    public MessageExtension setCriticalityIndicator(boolean criticalityIndicator) {
        this.criticalityIndicator = criticalityIndicator;
        return this;
    }

    public String getId() {
        return id;
    }

    public MessageExtension setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public MessageExtension setName(String name) {
        this.name = name;
        return this;
    }

    public MessageExtension addData(String fieldName, Object fieldValue) {
        if (this.data == null) {
            this.data = new HashMap<>();
        }
        this.data.put(fieldName, fieldValue);
        return this;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Map<String, Object> getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MessageExtension that = (MessageExtension) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
