/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 10. nov 2017
 *
 */
package com.modirum.ds.model;

import com.modirum.ds.services.JsonMessage;

import javax.json.JsonObject;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public interface TDSMessageBase {

    String getMessageType();

    void setMessageType(String value);

    String getMessageVersion();

    void setMessageVersion(String value);

    String getMessageCategory();

    void setMessageCategory(String value);

    String getAcquirerBIN();

    void setAcquirerBIN(String value);

    String getAcquirerMerchantID();

    void setAcquirerMerchantID(String value);

    String getAcsOperatorID();

    void setAcsOperatorID(String value);

    String getAcsReferenceNumber();

    void setAcsReferenceNumber(String value);

    String getAcsTransID();

    void setAcsTransID(String value);

    String getAcsURL();

    void setAcsURL(String value);

    String getAuthenticationType();

    void setAuthenticationType(String value);

    String getAuthenticationValue();

    void setAuthenticationValue(String value);

    String getBrowserLanguage();

    void setBrowserLanguage(String value);

    String getBrowserIP();

    void setBrowserIP(String value);

    Boolean getBrowserJavaEnabled();

    void setBrowserJavaEnabled(Boolean value);

    String getBrowserUserAgent();

    void setBrowserUserAgent(String value);

    String getCardExpiryDate();

    void setCardExpiryDate(String value);

    String getPurchaseInstalData();

    void setPurchaseInstalData(String value);

    String getMcc();

    void setMcc(String value);

    String getTdsRequestorURL();

    void setTdsRequestorURL(String value);

    String getTdsServerRefNumber();

    void setTdsServerRefNumber(String value);

    String getTdsServerURL();

    void setTdsServerURL(String value);

    String getPurchaseAmount();

    void setPurchaseAmount(String value);

    String getPurchaseDate();

    void setPurchaseDate(String value);

    String getPurchaseExponent();

    void setPurchaseExponent(String value);

    String getSdkReferenceNumber();

    void setSdkReferenceNumber(String value);

    String getSdkAppID();

    void setSdkAppID(String value);

    String getRecurringExpiry();

    void setRecurringExpiry(String value);

    String getRecurringFrequency();

    void setRecurringFrequency(String value);

    String getTransType();

    void setTransType(String value);


    String getTdsRequestorID();

    void setTdsRequestorID(String value);

    String getThreeDSServerOperatorID();

    void setThreeDSServerOperatorID(String value);

    String getTdsServerTransID();

    void setTdsServerTransID(String value);

    String getDsTransID();

    void setDsTransID(String value);

    String getDsReferenceNumber();

    void setDsReferenceNumber(String value);

    String getDsURL();

    void setDsURL(String value);

    String getDeviceChannel();

    void setDeviceChannel(String value);

    String getPurchaseCurrency();

    void setPurchaseCurrency(String value);

    String getBillAddrCountry();

    void setBillAddrCountry(String value);

    String getBillAddrState();

    void setBillAddrState(String value);

    String getShipAddrCountry();

    void setShipAddrCountry(String value);

    String getShipAddrState();

    void setShipAddrState(String value);

    String getMerchantCountryCode();

    void setMerchantCountryCode(String value);

    String getSdkTransID();

    void setSdkTransID(String value);

    String getAcctNumber();

    void setAcctNumber(String value);

    String getEci();

    void setEci(String value);

    String getErrorDetail();

    void setErrorDetail(String value);

    String getErrorDescription();

    void setErrorDescription(String value);

    String getErrorComponent();

    void setErrorComponent(String value);

    String getErrorMessageType();

    void setErrorMessageType(String value);

    String getTransStatus();

    void setTransStatus(String value);

    String getTransStatusReason();

    void setTransStatusReason(String value);

    String getMerchantName();

    void setMerchantName(String value);

    String getInteractionCounter();

    void setInteractionCounter(String value);

    String getNotificationURL();

    void setNotificationURL(String value);

    String getErrorCode();

    void setErrorCode(String value);

    Map<String, AtomicInteger> getDuplicateCounts();

    void setDuplicateCounts(Map<String, AtomicInteger> value);

    void setJsonObject(JsonObject jsonObject);

    JsonObject getJsonObject();

    String getTdsRequestorName();

    void setTdsRequestorName(String value);

    JsonMessage getIncomingJsonMessage();

    void setIncomingJsonMessage(JsonMessage incomingJsonMessage);
}
