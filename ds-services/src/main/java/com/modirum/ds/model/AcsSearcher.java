package com.modirum.ds.model;

import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.Searcher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AcsSearcher extends ACSProfile implements Serializable, Searcher {
    private static final long serialVersionUID = 1L;
    private Integer start;
    private Integer limit;
    private Integer total;
    private String order;
    private String orderDirection;
}
