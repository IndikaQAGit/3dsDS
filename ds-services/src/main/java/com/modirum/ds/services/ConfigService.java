/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 22.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.utils.SettingService;

import java.util.List;

public class ConfigService implements SettingService {
    private PersistenceService persistenceService;

    public ConfigService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public static synchronized long getErrorId() {
        try {
            Thread.sleep(1);
        } catch (Exception dc) {
        }
        return System.currentTimeMillis();
    }

    /**
     * @param id setting key
     * @return setting if found
     */
    @SuppressWarnings("rawtypes")
    public Setting getSettingByKey(String id) {
        if (id != null) {
            return persistenceService.getSettingById(id);
        }

        return null;
    }

    public String getStringSetting(String key) {
        Setting s = getSettingByKey(key);

        return s != null ? s.getValue() : null;
    }

    public List<Setting> getSettingsByIdStart(String idStart) throws Exception {
        List<Setting> settings;
        if (idStart != null && idStart.length() > 0) {
            try {
                settings = persistenceService.getSettingsByIdStart(idStart + "%", null, null);
                return settings;
            } catch (Exception e) {
                throw e;
            }
        }

        return null;
    }

    @Override
    public com.modirum.ds.utils.Setting getSettingByKey(String arg0, Short arg1) {
        return getSettingByKey(arg0);
    }
}