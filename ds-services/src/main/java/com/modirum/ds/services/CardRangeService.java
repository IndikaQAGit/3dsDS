package com.modirum.ds.services;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.dao.PersistenceService;

public class CardRangeService {

    private final PersistenceService persistenceService;

    public CardRangeService(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public void setCardRangeParticipating(final String start, final String end, final String paymentSystemId) {
        persistenceService.setCardRangeStatus(start, end, paymentSystemId, DSModel.CardRange.Status.PARTICIPATING);
    }

    public void setCardRangeNotParticipating(final String start, final String end, final String paymentSystemId) {
        persistenceService.setCardRangeStatus(start, end, paymentSystemId, DSModel.CardRange.Status.NOTPARTICIPATING);
    }

    /**
     * Returns true if an object was removed.
     */
    public boolean deleteCardRange(final String start, final String end, final String paymentSystemId) {
        return persistenceService.deleteCardRange(start, end, paymentSystemId);
    }
}
