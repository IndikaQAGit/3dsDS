package com.modirum.ds.services;

import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.tds21msgs.TDSMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageService230 <T extends TDSMessageBase> extends MessageService220<T> {

    protected static Logger log = LoggerFactory.getLogger(MessageService230.class);

    private volatile static MessageService230<TDSMessage> instance;

    public static MessageService230<TDSMessage> getInstance() {
        if (instance == null) {
            instance = new MessageService230<>();
        }
        return instance;
    }

    protected MessageService230() {
        super("/tds230.xsd");
        log.info("MessageService230 initialized");
    }

    public String getSupportedVersion() {
        return TDSModel.MessageVersion.V2_3_0.value();
    }

}
