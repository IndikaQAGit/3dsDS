package com.modirum.ds.services;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.dao.PersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MerchantService {

    private final PersistenceService persistenceService;
    protected transient static Logger log = LoggerFactory.getLogger(MerchantService.class);

    public MerchantService(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public void setMerchantActive(final String name, final Integer paymentSystemId) {
        this.persistenceService.setMerchantStatus(name, paymentSystemId, DSModel.Merchant.Status.ACTIVE);
    }

    public void setMerchantDisabled(final String name, final Integer paymentSystemId) {
        this.persistenceService.setMerchantStatus(name, paymentSystemId, DSModel.Merchant.Status.DISABLED);
    }
}
