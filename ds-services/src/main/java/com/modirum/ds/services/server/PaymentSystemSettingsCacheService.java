package com.modirum.ds.services.server;

import com.modirum.ds.enums.DSId;
import com.modirum.ds.enums.PsSetting;
import com.modirum.ds.jdbc.core.model.PaymentSystemConfig;
import com.modirum.ds.services.PaymentSystemConfigService;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * DS-Server specific Payment System Settings caching service. Is used within "ds-server" application module to
 * access Payment System settings. Settings are cached for a period of time to decrease DB network interaction overhead.
 */
@Service
public class PaymentSystemSettingsCacheService {

    @Autowired
    private PaymentSystemConfigService paymentSystemConfigService;

    public String getPaymentSystemSetting(Integer paymentSystemId, PsSetting psSetting) {
        PaymentSystemConfig paymentSystemConfig =
                paymentSystemConfigService.getPaymentSystemConfig(DSId.DEFAULT, paymentSystemId, psSetting.getKey());

        return paymentSystemConfig == null ? null : paymentSystemConfig.getValue();
    }

    /**
     * Returns integer-value settings.
     */
    public Optional<Integer> getIntegerSetting(Integer paymentSystemId, PsSetting psSetting) {
        String psSettingValue = getPaymentSystemSetting(paymentSystemId, psSetting);
        if (!NumberUtils.isDigits(psSettingValue)) {
            return Optional.empty();
        }
        return Optional.ofNullable(NumberUtils.createInteger(psSettingValue));
    }

    /**
     * Returns integer-value settings.
     */
    public Optional<Long> getLongSetting(Integer paymentSystemId, PsSetting psSetting) {
        String psSettingValue = getPaymentSystemSetting(paymentSystemId, psSetting);
        if (!NumberUtils.isDigits(psSettingValue)) {
            return Optional.empty();
        }
        return Optional.ofNullable(NumberUtils.createLong(psSettingValue));
    }

    /**
     * Returns <code>true</code> if the setting exists and is 'true' for the given payment system. Otherwise, false.
     */
    public boolean isTrueSetting(Integer paymentSystemId, PsSetting psSetting) {
        String psSettingValue = getPaymentSystemSetting(paymentSystemId, psSetting);
        return Boolean.parseBoolean(psSettingValue);
    }
}