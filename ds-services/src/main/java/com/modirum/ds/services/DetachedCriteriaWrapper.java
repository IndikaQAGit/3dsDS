package com.modirum.ds.services;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.internal.CriteriaImpl;

/**
 * DetachedCriteria that reuses Criteria. This is useful when using the main Criteria object
 * as subquery for other secondary batch queries.
 */
public class DetachedCriteriaWrapper extends DetachedCriteria {
    public DetachedCriteriaWrapper(Criteria criteria) {
        super((CriteriaImpl) criteria, criteria);
        ((CriteriaImpl) criteria).setSession(null);
    }
}