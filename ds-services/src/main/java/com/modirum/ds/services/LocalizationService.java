/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 13.12.2010
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.db.model.Text;
import com.modirum.ds.db.model.TextSearcher;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.utils.GenericPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class LocalizationService {

    static Map<String, GenericPool<MessageFormat>> fmtMap = new TreeMap<>();
    static Map<String, Text> textCache = new TreeMap<>();
    static volatile LocalizationService instance = null;
    private PersistenceService persistenceService;

    protected static Logger log = LoggerFactory.getLogger(LocalizationService.class);

    protected LocalizationService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public static void resetTextCache() {
        fmtMap.clear();
        textCache.clear();
    }

    public String getText(String lkey, java.util.Locale loc) {
        return getText(lkey, null, loc);
    }

    public String getText(String lkey, Object arg, java.util.Locale loc) {
        return getText(lkey, new Object[]{arg}, loc);
    }

    public void putText(Text tx) {
        textCache.put(tx.getKey() + "_" + tx.getLocale(), tx);
    }

    public String getText(String lkey, Object[] args, java.util.Locale loc) {
        try {
            Text tx = textCache.get(lkey + "_" + loc);
            if (tx == null) {
                tx = textCache.get(lkey + "_" + loc.getLanguage());
            }

            // cached non existing text retry load after 10 minutes
            if (tx != null && tx.getKey() == null && tx.getId() != null &&
                tx.getId() < (System.currentTimeMillis() / 60000) - 10) {
                tx = null;
            }

            boolean langOnly = false;
            if (tx == null) {
                tx = loadText(lkey, loc.toString());
                if (tx == null) {
                    tx = loadText(lkey, loc.getLanguage());
                    langOnly = true;
                }

                if (tx != null) {
                    synchronized (textCache) {
                        textCache.put(lkey + "_" + (langOnly ? loc.getLanguage() : loc.toString()), tx);
                    }
                } else {    // check fallback from cache
                    tx = loadText(lkey, "en");
                    if (tx != null) {
                        textCache.put(lkey + "_en", tx);
                        // cache also in locale to avoid repating reloading
                        if (!"en".equals(loc.getLanguage())) {
                            synchronized (textCache) {
                                textCache.put(lkey + "_" + loc.getLanguage(), tx);
                            }
                        }
                    }
                }
            }

            if (tx == null) {
                tx = new Text();
                tx.setId((int) (System.currentTimeMillis() / 60000));
                tx.setMessage(lkey);
                synchronized (textCache) {
                    textCache.put(lkey + "_" + loc.getLanguage(), tx);
                }
            } else {
                return formatMessage(tx, args, loc);
            }

        } catch (Exception ee) {
            log.warn("Bundle/format errror", ee);
        }

        return lkey;
    }

    public String formatMessage(Text tx, Object[] args, java.util.Locale loc) {
        if (tx.getMessage() != null && tx.getMessage().contains("{") && args != null && args.length > 0) {
            GenericPool<MessageFormat> fmtPool = fmtMap.get("mdm.messageRespource_" + loc);
            if (fmtPool == null) {
                fmtPool = new GenericPool<MessageFormat>();
                synchronized (fmtMap) {
                    fmtMap.put("mdm.messageRespource_" + loc, fmtPool);
                }
            }

            MessageFormat format = fmtPool.reuse();
            if (format == null) {
                format = new java.text.MessageFormat("", loc);
            }

            StringBuffer result = new StringBuffer(tx.getMessage().length() * 10 / 8);
            format.applyPattern(tx.getMessage());
            format.format(args, result, null);
            fmtPool.recycle(format);
            return result.toString();
        }

        return tx.getMessage();
    }

    @SuppressWarnings("rawtypes")
    public Text loadText(String key, String language) throws Exception {

        Text foundText = null;

        if (key != null && language != null) {
            foundText = persistenceService.loadText(key, language);
        }
        return foundText;
    }

    public List<Text> findTexts(TextSearcher searcher) {
        return persistenceService.findTexts(searcher);
    }
}
