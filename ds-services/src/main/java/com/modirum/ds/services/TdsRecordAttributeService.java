package com.modirum.ds.services;

import com.modirum.ds.enums.TransactionAttribute;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.jdbc.core.dao.TdsRecordAttributeDAO;
import com.modirum.ds.jdbc.core.model.TdsRecordAttribute;
import com.modirum.ds.model.ProductInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * service class for tdsRecordAttributeService manages data on table ds_tdsrecord_attributes or persistence
 * tdsrecordattribute
 */
@Service("tdsRecordAttributeService")
public class TdsRecordAttributeService {

    private final static Logger log = LoggerFactory.getLogger(TdsRecordAttributeService.class);

    @Autowired
    private TdsRecordAttributeDAO tdsRecordAttributeDAO;

    private static final String DS_USER = "DS";

    /**
     * saves Authentication value in ds_tdsrecord_attributes table
     * 
     * @param value
     * @param record
     */
    public void saveAuthenticationValue(String value, TDSRecord record) {
        saveAttribute(TransactionAttribute.AUTHENTICATION_VALUE, record.getId(), value);
    }

    /**
     * saves Card range in ds_tdsrecord_attributes table
     * 
     * @param record
     * @param cardRange
     */
    public void saveCardRangeAttribute(TDSRecord record, CardRange cardRange) {
        TransactionAttribute transactionAttribute;
        String value = StringUtils.EMPTY;
        if (cardRange.isRangeMode()) {
            transactionAttribute = TransactionAttribute.CARD_RANGE;
            value = cardRange.getStart() + " - " + cardRange.getEnd();
        } else {
            transactionAttribute = TransactionAttribute.BIN;
            value = cardRange.getBin();
        }
        saveAttribute(transactionAttribute, record.getId(), value);
    }


    /**
     * saves attributes related to tdsrecord for Areq flow in ds_tdsrecord_attributes table
     *
     * @param record
     * @param incomingPortNumber
     */
    public void saveTDSRecordAttributesAreq(TDSRecord record, Integer incomingPortNumber) {
        saveAttribute(TransactionAttribute.TDS_SERVER_URL, record.getId(), record.getTdsServerURL());
        saveIncomingPortNumber(record, incomingPortNumber);
        saveDSVersion(record);
    }

    /**
     * saves attributes related to tdsrecord for Preq flow in ds_tdsrecord_attributes table
     *
     * @param record
     * @param incomingPortNumber
     */
    public void saveTDSRecordAttributesPreq(TDSRecord record, Integer incomingPortNumber) {
        saveIncomingPortNumber(record, incomingPortNumber);
        saveDSVersion(record);
    }

    public void saveIncomingPortNumber(TDSRecord record, Integer incomingPortNumber){
        saveAttribute(TransactionAttribute.INCOMING_PORT_NUMBER, record.getId(), incomingPortNumber.toString());
    }

    /**
     * saves the version of the DS the record
     *
     * @param record
     */
    public void saveDSVersion(TDSRecord record) {
        saveAttribute(TransactionAttribute.DS_VERSION, record.getId(), ProductInfo.getBuildVersion());
    }

    /**
     * saves a recordattribute in persistence
     * saves its value either utf or ascii for now
     * 
     * 
     * @param transactionAttribute
     * @param tdsRecordId
     * @param value
     */
    public void saveAttribute(TransactionAttribute transactionAttribute, Long tdsRecordId, String value) {
        try {
            TdsRecordAttribute tdsRecordAttribute = new TdsRecordAttribute();
            tdsRecordAttribute.setTdsRecordId(tdsRecordId);
            tdsRecordAttribute.setCreatedUser(DS_USER);
            tdsRecordAttribute.setId(UUID.randomUUID().toString());
            tdsRecordAttribute.setKey(transactionAttribute.getKey());
            tdsRecordAttribute.setCreatedDate(new Date());
            if (transactionAttribute.getCharSet().equals(StandardCharsets.US_ASCII)) {
                tdsRecordAttribute.setAsciiValue(value);
            } else {
                tdsRecordAttribute.setValue(value);
            }
            tdsRecordAttributeDAO.insert(tdsRecordAttribute);
        } catch (Exception e) {
            log.error("Failed to save record attribute", e);
        }
    }

    /**
     * set attributes related to tdsrecord
     *
     * @param record
     */
    public void setTDSRecordAttributes(final TDSRecord record) {
        setTDSServerURL(record);
    }

    /**
     * returns a list of tdsrecordattribute rows from the db
     *
     * @param tdsRecordId
     * @return
     * @throws Exception
     */
    public List<TdsRecordAttribute> getRecordAttributesByTDSRecordId(Long tdsRecordId) {
        return tdsRecordAttributeDAO.getRecordAttributesByTDSRecordId(tdsRecordId);
    }

    private void setTDSServerURL(final TDSRecord record) {
        TdsRecordAttribute tdsServerURLAttribute = getAttributeByTDSRecordIDAndKey(record.getId(),
            TransactionAttribute.TDS_SERVER_URL.getKey());
        if (tdsServerURLAttribute != null) {
            record.setTdsServerURL(tdsServerURLAttribute.getAsciiValue());
        } else {
            log.error("Failed to get TDS Server URL.");
        }
    }

    public TdsRecordAttribute getAttributeByTDSRecordIDAndKey(Long tdsRecordId, String key) {
        return tdsRecordAttributeDAO.getTDSSRecordAttributeByRecordIDAndKey(tdsRecordId, key);
    }
}
