package com.modirum.ds.services;

import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.model.DS;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.model.Error;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.model.IssuerSearcher;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.model.Persistable;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.utils.Misc;
import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;

public class ImportService {

    protected static Logger log = LoggerFactory.getLogger(ImportService.class);

    final static int DEFAULT_MAX_TX_SIZE = 1000;

    static Object syno = new Object();

    static final String[] cAcqImportTypes = {Merchant.class.getSimpleName(), TDSServerProfile.class.getSimpleName(), Acquirer.class.getSimpleName()};
    static final String[] cIssImportTypes = {ACSProfile.class.getSimpleName(), CardRange.class.getSimpleName(), Issuer.class.getSimpleName()};
    final static String cLastModifier = "ImportSerice";
    private Schema schema;
    private JAXBContext jc;

    private final ImportPersistenceService persistenceService;
    private final AcquirerService acquirerService;
    private final List<String> errors;
    private final IssuerService issuerService;

    public interface Mode {
        String FULL = "full";
        String UPDATE = "update";
        String EXPORT = "export";
        String EXPORT_TEST = "exporttest";
    }

    public  ImportService() {
        persistenceService = new ImportPersistenceService();
        errors = new ArrayList<>();
        acquirerService = ServiceLocator.getInstance().getAcquirerService();
        issuerService = ServiceLocator.getInstance().getIssuerService();
    }

    public List<String> getErrors() {
        return errors;
    }

    static class FinishedRunnable implements Runnable {

        boolean finished;
        Runnable runnable;

        FinishedRunnable(Runnable runnable) {
            this.runnable = runnable;
        }

        public void run() {
            try {
                runnable.run();
            } catch (Throwable t) {
                log.error("Unexpected Error in run", t);
            } finally {
                finished = true;
                try {
                    Thread.sleep(5);
                } catch (Exception dc) {
                }
                synchronized (syno) {
                    syno.notifyAll();
                }
            }
        }
    }

    public DS unmarshal(InputStream stream, List<Error> errors, boolean validate) throws JAXBException, SAXException {
        if (jc == null) {
            jc = JAXBContext.newInstance(DS.class);
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schema = factory.newSchema(new StreamSource(ImportService.class.getResourceAsStream("/dsxmodel.xsd")));
        }
        Unmarshaller u = jc.createUnmarshaller();
        if (validate) {
            u.setSchema(schema);
            u.setEventHandler(new MyValidationEventHandler(errors));
        }

        return (DS) u.unmarshal(stream);
    }

    public Map<String, Integer> doImport(final DS importSource, final String mode, final boolean delete, final boolean withEnded, String user) throws Exception {
        long start = System.currentTimeMillis();
        ImportPersistenceService pse = this.persistenceService;
        final Map<String, Integer> results = new LinkedHashMap<>();

        Setting sise = pse.getSettingById(DsSetting.IMPORT_SESSION_ID.getKey());
        int v = sise != null && Misc.isInt(sise.getValue()) ? Misc.parseInt(sise.getValue()) : 1;
        if (v >= 127) {
            v = 1;
        } else {
            v++;
        }

        if (sise == null) {
            sise = new Setting();
            sise.setKey(DsSetting.IMPORT_SESSION_ID.getKey());
            sise.setComment("Internal counter, do not touch!");
        }
        sise.setLastModified(new java.util.Date());
        sise.setLastModifiedBy(cLastModifier);
        sise.setValue(String.valueOf(v));
        pse.saveOrUpdate(sise);

        final byte importSessionId = (byte) v;
        String txs = pse.getStringSetting(DsSetting.IMPORT_MAX_TX_SIZE.getKey());
        final int maxTxSize;
        if (Misc.isInt(txs) && Misc.parseInt(txs) > 0) {
            maxTxSize = Misc.parseInt(txs);
        } else {
            maxTxSize = DEFAULT_MAX_TX_SIZE;
        }

        log.info("Import session " + importSessionId + ", tx size " + maxTxSize + " starting..");
        int impAcquirers = Misc.nullSafe(importSource.getAcquirers()).size();
        int impIssuers = Misc.nullSafe(importSource.getIssuers()).size();
        int impMIs = Misc.nullSafe(importSource.getTdsServerProfiles()).size();
        int impCRs = Misc.nullSafe(importSource.getCardRanges()).size();
        int impACSs = Misc.nullSafe(importSource.getACSProfiles()).size();
        int impMEs = Misc.nullSafe(importSource.getMerchants()).size();
        final int commitStep = 100;

        log.info("Import source contains:\n" + "   " + impAcquirers + " Acquirers\n" + "   " + impIssuers +
                 " Issuers\n" + "   " + impMIs + " TDSServerProfiles\n" + "   " + impCRs + " CardRanges\n" + "   " + impACSs +
                 " ACSProfiles\n" + "   " + impMEs + " Merchants");

        // merchants with new MI mi needs stored before and then those need to saved or resaved
        //final List<Merchant> existingAcquirerEntitiesMEMIC = new ArrayList<>();

        FinishedRunnable issuerRunnable = new FinishedRunnable(new Runnable() {
            @Override
            public void run() {
                final Map<Integer, Issuer> issuerMap = new HashMap<>();
                final Map<Integer, Integer> issuerReverseMappings = new HashMap<>();
                final Map<Integer, Integer> issuerForwardMappings = new HashMap<>();
                final Map<Integer, ACSProfile> acpForwardMappings = new HashMap<>();
                final Map<Integer, Integer> attemptsAcpForwardMappings = new HashMap<>();
                final Map<Integer, Integer> acpReverseMappings = new HashMap<>();
                final Map<Issuer, List<Persistable>> issuersAndSubEntities = new LinkedHashMap<>();
                final Map<Issuer, List<Number>> issuersAcpEquals = new HashMap<>();
                final Map<Issuer, List<Number>> issuersCrEquals = new HashMap<>();
                final Map<Issuer, List<Persistable>> newIssuersAndSubEntities = new LinkedHashMap<>();
                final Map<Integer, List<CardRange>> ownerChangedCR = new HashMap<>();
                //final Map<Issuer, List<Number>> ownerChangedACP = new HashMap<>();


                int crDupIgnored = 0;
                int crErrIgnored = 0;
                int crEquals = 0;
                int crAdd = 0;
                int crUpdate = 0;
                int crDelete = 0;
                int orphanCRIgnored = 0;

                int acpEquals = 0;
                int acpAdd = 0;
                int acpUpdate = 0;
                int acpDelete = 0;
                int acpDupIgnored = 0;
                int orphanACPIgnored = 0;

                int issuerEquals = 0;
                int issuerAdd = 0;
                int issuerUpdate = 0;
                int issuerDupIgnored = 0;

                Session session = persistenceService.getSessionFactory().openSession();
                session.setFlushMode(FlushMode.MANUAL);
                Transaction xt = null;
                try {
                    //check if issuer already exists in db. match by issuer bin first then  name.Don't import if status = "E".
                    //if so, set its id equal to the one of the corresponding db entity.
                    //keep track of its file id to compare with issuerId of card ranges & issuerId of acsProfiles
                    //If they match then take the generated Id//and assign it to the acsProfiles and cardRanges that had matching file ids.
                    //If there is no match set its id to null
                    log.info("Processing issuers and subentities..");
                    Issuer dummy = new Issuer();
                    dummy.setName("Dummy");
                    dummy.setBIN("999999999");
                    dummy.setCountry("XX");
                    dummy.setId(null);
                    dummy.setIsId(null);

                    int attemptsACPCount = 0;
                    if (importSource.getACSProfiles() != null) {
                        log.info("Pre-Processing Attemps ACPs for issuers.. ");
                    }
                    for (ACSProfile profile : Misc.nullSafe(importSource.getACSProfiles())) {
                        // ATTEMPTS ACP case
                        if (profile.getIssuerId() == null) {
                            attemptsACPCount++;
                            ACSProfile match = findACSProfileMatch(profile.getRefNo(), null);
                            if (match != null) {
                                attemptsAcpForwardMappings.put(profile.getId(), match.getId());
                            }
                        }
                    }

                    if (importSource.getACSProfiles() != null) {
                        log.info("Attempts ACPs (wo issuer) found " + attemptsACPCount);
                    }

                    //
                    {
                        log.info("Pre-Processing issuers " + Misc.nullSafe(importSource.getIssuers()).size() + "..");
                        long s = System.currentTimeMillis();
                        long sk = s;
                        int icnt = 0;
                        issuersAndSubEntities.put(dummy, new LinkedList<Persistable>());
                        issuersAcpEquals.put(dummy, new LinkedList<Number>());
                        issuersCrEquals.put(dummy, new LinkedList<Number>());

                        for (Issuer issuer : Misc.nullSafe(importSource.getIssuers())) {
                            icnt++;
                            if (icnt % 1000 == 0) {
                                log.info("Pre-Processed Issuers " + (icnt - 999) + "-" + icnt + "/" +
                                         importSource.getIssuers().size() + " in " + (System.currentTimeMillis() - sk) +
                                         " ms");
                                sk = System.currentTimeMillis();
                            }

                            if (!withEnded && DSModel.Issuer.Status.ENDED.equals(issuer.getStatus())) {
                                continue;
                            }

                            if (issuerMap.get(issuer.getId()) != null) {
                                issuerDupIgnored++;
                                log.warn("Issuer " + issuer.getId() + ":" + issuer.getName() +
                                         " is conflicts with earlier file Issuer same id  and is ignored");
                                continue;
                            }

                            issuerMap.put(issuer.getId(), issuer);
                            issuer.setIsId(importSessionId);
                            Issuer match = findIssuerMatch(issuer);
                            if (match != null)//this issuer already exists in the db
                            {
                                issuerForwardMappings.put(issuer.getId(), match.getId());
                                issuerReverseMappings.put(match.getId(), issuer.getId());
                                issuer.setId(match.getId());
                                Integer issuerAAbackup = issuer.getAttemptsACSProId();
                                Integer acpId = issuer.getAttemptsACSProId() != null ? attemptsAcpForwardMappings.get(
                                        issuer.getAttemptsACSProId()) : null;
                                issuer.setAttemptsACSProId(acpId);
                                if (issuer.equals(match)) {
                                    issuerEquals++;
                                    if (!Mode.FULL.equals(mode)) {
                                        issuer.setIsId(null); // no need to update
                                    }
                                } else if (acpId == null) {
                                    issuer.setAttemptsACSProId(issuerAAbackup);
                                }
                                issuersAndSubEntities.put(issuer, new LinkedList<Persistable>());
                                issuersAcpEquals.put(issuer, new LinkedList<Number>());
                                issuersCrEquals.put(issuer, new LinkedList<Number>());
                            } else {
                                newIssuersAndSubEntities.put(issuer, new LinkedList<Persistable>());
                            }
                            ownerChangedCR.put(issuer.getId(), new LinkedList<CardRange>());
                            //ownerChangedACP.put(issuer, new LinkedList<Number>());

                        }
                        long e = System.currentTimeMillis();
                        log.info("Pre-Processing of " + Misc.nullSafe(importSource.getIssuers()).size() +
                                 " Issuers completed in " + (e - s) + " ms");
                    }

                    if (importSource.getCardRanges() != null && importSource.getCardRanges().size() > 0) {
                        log.info("Pre-Processing CardRanges " + importSource.getCardRanges().size() + "..");
                        Map<String, CardRange> crMap = new TreeMap<>();
                        int ccnt = 0;
                        long s = System.currentTimeMillis();
                        long sk = s;

                        int step = 100;
                        List<CardRange> subList = new ArrayList<>(step);
                        List<String> subListCheck = new ArrayList<>(step);
                        Map<String, CardRange> matchMap = new HashMap<String, CardRange>();
                        for (int i1 = 0; i1 < importSource.getCardRanges().size(); i1 += step) {
                            subList.clear();
                            matchMap.clear();
                            subListCheck.clear();

                            for (int i2 = 0; i2 < step && i1 + i2 < importSource.getCardRanges().size(); i2++) {
                                ccnt++;
                                if (ccnt % 1000 == 0) {
                                    log.info("Pre-Processed CardRanges " + (ccnt - 999) + "-" + ccnt + "/" +
                                             importSource.getCardRanges().size() + " in " +
                                             (System.currentTimeMillis() - sk) + " ms");
                                    sk = System.currentTimeMillis();
                                }

                                CardRange cr = importSource.getCardRanges().get(i1 + i2);

                                CardRange dup = crMap.get(cr.getBinSt());
                                if (dup != null) {
                                    log.warn("CardRange " + cr.getId() + ":" + cr.getBinSt() + " " + cr.getName() +
                                             " is conflicts with earlier file CardRange " + dup.getId() + ":" +
                                             dup.getBinSt() + " " + dup.getName() + " and is ignored");
                                    crDupIgnored++;
                                    continue;
                                }
                                crMap.put(cr.getBinSt(), cr);

                                Issuer issuer = cr.getIssuerId() != null ? issuerMap.get(cr.getIssuerId()) : null;
                                if (issuer == null) {
                                    log.warn("CardRange " + cr.getId() + " file issid=" + cr.getIssuerId() + " " +
                                             Misc.defaultString(cr.getBinSt()) + " has no existing or new issuer, ignored");
                                    orphanCRIgnored++;
                                    continue;
                                }
                                if (cr.getType() == null) {
                                    log.warn("CardRange " + cr.getId() + " file issid=" + cr.getIssuerId() + " " +
                                             Misc.defaultString(cr.getBinSt()) +
                                             " required property type is not set (null), ignored");
                                    crErrIgnored++;
                                    continue;
                                }

                                subListCheck.add(cr.getBinSt());
                                subList.add(cr);
                            }

                            issuerService.getIssuerCardBins(subListCheck, matchMap);

                            for (CardRange cr : subList) {
                                Issuer crIssuer = issuerMap.get(cr.getIssuerId());
                                CardRange match = matchMap.get(cr.getBinSt());
                                // no issuer may have move do not also check issuer belonging match
                                if (match != null) {
                                    Integer newIssuerId = issuerForwardMappings.get(cr.getIssuerId());
                                    cr.setIssuerId(newIssuerId);
                                    cr.setId(match.getId());
                                    if (cr.equals(match)) {
                                        crEquals++;
                                        if (Mode.FULL.equals(mode)) {
                                            issuersAndSubEntities.get(crIssuer).add(cr);
                                        } else {
                                            issuersCrEquals.get(crIssuer).add(cr.getId());
                                        }
                                    } else // minor changes or issuer change
                                    {
                                        // owner changed must get special treatment id null out if deleted before re added
                                        if (newIssuerId == null ||
                                            match.getIssuerId() != null && !newIssuerId.equals(match.getIssuerId())) {
                                            // old issuer id
                                            List<CardRange> ocl = ownerChangedCR.get(match.getIssuerId());
                                            // if such issuer no longer exist in file then special treatment also not needed.
                                            if (ocl != null) {
                                                ocl.add(cr);
                                            }
                                        }

                                        List<Persistable> mxl = issuersAndSubEntities.get(crIssuer);
                                        if (mxl == null) {
                                            mxl = newIssuersAndSubEntities.get(crIssuer);
                                        }

                                        if (newIssuerId == null) // restore original
                                        {
                                            cr.setIssuerId(crIssuer.getId());
                                        }
                                        mxl.add(cr);
                                    }
                                    cr.setIsId(importSessionId);
                                } // match
                                else { // new to existing or new issuers
                                    List<Persistable> mxl = issuersAndSubEntities.get(crIssuer);
                                    if (mxl == null) {
                                        mxl = newIssuersAndSubEntities.get(crIssuer);
                                    }

                                    if (mxl == null) {
                                        log.warn("CardRange " + cr.getId() + " file issid=" + cr.getIssuerId() + " " +
                                                 Misc.defaultString(cr.getBinSt()) +
                                                 " has no existing or new issuer subentity list (anomaly), ignored");
                                        orphanCRIgnored++;
                                        continue;
                                    }

                                    mxl.add(cr);
                                }
                            } // sublist
                        }
                        long e = System.currentTimeMillis();
                        log.info("Pre-Processing of " + importSource.getCardRanges().size() +
                                 " Card Ranges completed in " + (e - s) + " ms");
                    }

                    if (importSource.getACSProfiles() != null) {
                        int acpcnt = 0;
                        log.info("Pre-Processing ACSProfiles " + importSource.getACSProfiles().size() + "..");
                        long s = System.currentTimeMillis();
                        long sk = s;
                        java.util.Map<Integer, ACSProfile> dupCheck = new java.util.TreeMap<>();
                        java.util.Map<Integer, ACSProfile> dupCheckHashCode = new java.util.TreeMap<>();
                        for (ACSProfile profile : importSource.getACSProfiles()) {
                            acpcnt++;
                            if (acpcnt % 1000 == 0) {
                                log.info("Pre-Processed ACP " + (acpcnt - 999) + "-" + acpcnt + "/" +
                                         importSource.getACSProfiles().size() + " in " +
                                         (System.currentTimeMillis() - sk) + " ms");
                                sk = System.currentTimeMillis();
                            }

                            int hashCode = profile.hashCode();
                            if (profile.getId() != null) {
                                if (dupCheck.get(profile.getId()) != null) {
                                    log.warn("ACSProfile " + profile.getId() + ":" + profile.getName() +
                                             " is duplicate, conflicts with earlier file ACSProfile same id and is ignored");
                                    acpDupIgnored++;
                                    continue;
                                }

                                dupCheck.put(profile.getId(), profile);
                            } else {
                                ACSProfile dx = dupCheckHashCode.get(hashCode);
                                if (dx != null && acpSame(dx, profile)) {
                                    log.warn("ACSProfile " + profile.getId() + ":" + profile.getName() + " url " +
                                             profile.getURL() + " ref " + profile.getRefNo() + " set " +
                                             profile.getSet() + " stat " + profile.getStatus() +
                                             " is duplicate, conflicts with earlier file ACSProfile exact same values and is ignored");
                                    acpDupIgnored++;
                                    continue;
                                }
                            }
                            dupCheckHashCode.put(hashCode, profile);


                            // ATTEMPTS ACP case
                            Issuer issuer = null;
                            if (profile.getIssuerId() != null) {
                                issuer = issuerMap.get(profile.getIssuerId());
                                if (issuer == null) {
                                    log.warn("ACSProfile " + profile.getId() + " " + profile.getRefNo() + " " +
                                             profile.getName() + " has no existing or new issuer, ignored");
                                    orphanACPIgnored++;
                                    continue;
                                }
                            } else {
                                issuer = dummy;
                            }

                            if (issuer == null) {
                                log.warn("Issuer must not be null at this point acp in file id=" + profile.getId() +
                                         " ref=" + profile.getRefNo());
                                log.warn("ACSProfile " + profile.getId() + " " + profile.getRefNo() + " " +
                                         profile.getName() + " has no existing or new issuer, ignored");
                                orphanACPIgnored++;
                                continue;
                            }

                            if (profile.getId() != null) {
                                acpForwardMappings.put(profile.getId(), profile);
                            }

                            Integer issuerDBId = profile.getIssuerId() != null ? issuerForwardMappings.get(
                                    profile.getIssuerId()) : null;
                            Integer oldId = profile.getId();
                            ACSProfile match = findACSProfileMatch(profile.getRefNo(), issuerDBId);
                            if (match != null) {
                                profile.setId(match.getId());
                                profile.setIssuerId(match.getIssuerId());
                                if (profile.equals(match)) {
                                    acpEquals++;
                                    if (oldId != null) {
                                        acpReverseMappings.put(match.getId(), oldId);
                                    }

                                    if (Mode.FULL.equals(mode)) {
                                        issuersAndSubEntities.get(issuer).add(profile);
                                    } else {
                                        issuersAcpEquals.get(issuer).add(profile.getId());
                                    }
                                } else {
                                    profile.setIssuerId(issuer.getId());
                                    List<Persistable> lx = issuersAndSubEntities.get(issuer);
                                    // an old issuer acp was moved to a new issuer
                                    if (lx == null) {
                                        lx = newIssuersAndSubEntities.get(issuer);
                                        if (lx != null) {
                                            log.info("Detected ACSProfile " + profile.getId() + " " +
                                                     profile.getRefNo() + " " + profile.getName() +
                                                     " move to brand new issuer " + issuer.getId() + " " +
                                                     issuer.getBIN() + " " + issuer.getName());
                                        }
                                    }
                                    if (lx != null) {
                                        lx.add(profile);
                                    } else {
                                        log.error("No entity list for existing or new issuer " + issuer.getId() + " " +
                                                  issuer.getBIN() + " " + issuer.getName());
                                        log.warn("ACSProfile " + profile.getId() + " " + profile.getRefNo() + " " +
                                                 profile.getName() + " has no existing or new issuer, ignored");
                                        orphanACPIgnored++;
                                    }
                                }
                                profile.setIsId(importSessionId);
                            } else {
                                profile.setId(null);
                                profile.setIsId(null);
                                List<Persistable> isl = issuersAndSubEntities.get(issuer);
                                if (isl == null) {
                                    isl = newIssuersAndSubEntities.get(issuer);
                                } else {
                                    profile.setIssuerId(issuer.getId());
                                }
                                isl.add(profile);
                            }
                        }
                        long e = System.currentTimeMillis();
                        log.info("Pre-Processing of " + importSource.getACSProfiles().size() +
                                 " ACSProfiles completed in " + (e - s) + " ms");
                    } else {
                        log.info("No ACSProfiles in input");
                    }

                    log.info("Updating existing issuers " + issuersAndSubEntities.size() + " with sub entities..");
                    long s = System.currentTimeMillis();
                    long sk = s;
                    xt = null;
                    final Map<Integer, Boolean> issuerDoneMap = new TreeMap<>();
                    java.util.Date now = new java.util.Date();

                    for (Map.Entry<Issuer, List<Persistable>> entry : issuersAndSubEntities.entrySet()) {
                        if (xt == null) {
                            xt = session.getTransaction();
                            xt.begin();
                        }
                        Issuer is = entry.getKey();

                        List<Number> crMatch = issuersCrEquals.get(is);
                        List<Number> acpMatch = issuersAcpEquals.get(is);

                        if (!dummy.equals(is) && is.getAttemptsACSProId() != null) {
                            ACSProfile attemptsACP = acpForwardMappings.get(is.getAttemptsACSProId());
                            if (attemptsACP != null && is.getIsId() != null) {
                                if (attemptsACP.getId() != null &&
                                    !attemptsACP.getId().equals(is.getAttemptsACSProId())) {
                                    is.setAttemptsACSProId(attemptsACP.getId());
                                    if (issuerEquals > 0) {
                                        issuerEquals--;
                                    }
                                    is.setIsId(importSessionId);
                                }
                            }
                        }

                        if (is.getIsId() != null) {
                            session.saveOrUpdate(is);
                            session.flush();
                            issuerUpdate++;
                        }

                        int sbcnt = 0;
                        for (Persistable per : entry.getValue()) {
                            if (per instanceof CardRange) {
                                if (((CardRange) per).getIsId() == null) {
                                    ((CardRange) per).setId(null);
                                    ((CardRange) per).setIsId(importSessionId);
                                }
                                //log.info("CR "+((CardRange)per).getId()+" "+((CardRange)per).getBinSt()+" "+((CardRange)per).getName());
                                int x = per.getId() == null ? crAdd++ : crUpdate++;
                                ((CardRange) per).setIssuerId(is.getId());
                                if (((CardRange) per).getLastMod() == null) {
                                    ((CardRange) per).setLastMod(now);
                                }

                            } else if (per instanceof ACSProfile) {
                                if (((ACSProfile) per).getIsId() == null) {
                                    ((ACSProfile) per).setId(null);
                                    ((ACSProfile) per).setIsId(importSessionId);

                                }
                                //log.info("ACP "+((ACSProfile)per).getId()+" "+((ACSProfile)per).getName());
                                int x = per.getId() == null ? acpAdd++ : acpUpdate++;
                                ((ACSProfile) per).setIssuerId(is.getId());
                            }
                            session.saveOrUpdate(per);
                            sbcnt++;

                            if (sbcnt % 100 == 0) {
                                session.flush();
                            }
                        }
                        if (sbcnt > 0) {
                            session.flush();
                        }

                        // delete leftovers also in same tx except dummy
                        if (is.getId() != null) {
                            crDelete += deleteType(session, delete, importSessionId, "CardRange", "issuerId",
                                                   is.getId(), crMatch);
                            acpDelete += deleteType(session, delete, importSessionId, "ACSProfile", "issuerId",
                                                    is.getId(), acpMatch);
                            session.flush();
                        }

                        // the if owner changed if still in some list must get new id now
                        for (CardRange occr : Misc.nullSafe(ownerChangedCR.get(is.getId()))) {
                            // issuer not dont yet id need to null out bcause upper deletes
                            if (occr.getIssuerId() != null && issuerDoneMap.get(occr.getIssuerId()) == null) {
                                //session.evict(occr);
                                occr.setId(null);
                            }
                        }

                        if (issuerUpdate % commitStep == 0) {
                            // must be commited one by one as otherwise hibenrate somehow developes wirdnesses even with proper flush
                            //Batch update returned unexpected row count from update [0]; actual row count: 0; expected: 1
                            // even id null care is taken above
                            xt.commit();
                            xt = null;
                            try {
                                session.clear();
                            } catch (Exception dc) {
                            }
                        }

                        // skip dummy
                        if (is.getId() != null) {
                            issuerDoneMap.put(is.getId(), Boolean.TRUE);
                        }

                        if (issuerUpdate > 0 && issuerUpdate % 100 == 0) {
                            log.info("Update issuers " + (issuerUpdate - 99) + "-" + issuerUpdate + "/" +
                                     issuersAndSubEntities.size() + " in " + (System.currentTimeMillis() - sk) +
                                     " ms " + " (ucr " + crUpdate + " newcr " + crAdd + " delcr " + crDelete + ")" +
                                     " (uacs " + acpUpdate + " newacs " + acpAdd + " delacs " + acpDelete + ")");
                            sk = System.currentTimeMillis();
                        }

                    } // for issuersAndSubEntities

                    if (xt != null) {
                        xt.commit();
                        xt = null;
                        try {
                            session.clear();
                        } catch (Exception dc) {
                        }
                    }

                    long e = System.currentTimeMillis();
                    log.info("Updating issuers done " + issuerUpdate + " (ucr " + crUpdate + " newcr " + crAdd +
                             " delcr " + crDelete + ")" + " (uacs " + acpUpdate + " newacs " + acpAdd + " delacs " +
                             acpDelete + ")" + " in " + (e - s) + " ms");

                    if (!Mode.FULL.equals(mode)) {
                        log.info("Not Updated " + issuerEquals + " exact matched existing issuer entities");
                        log.info("Not Updated " + acpEquals + " exact matched existing acs profile entities");
                        log.info("Not Updated " + crEquals + " exact matched existing cr entities");
                    }

                    // clear no longer needed
                    issuersAndSubEntities.clear();

                    log.info("Adding new issuers " + newIssuersAndSubEntities.size() + " and sub entities..");
                    s = System.currentTimeMillis();
                    sk = s;
                    int icdone = 0;
                    int subedone = 0;

                    xt = null;
                    for (Map.Entry<Issuer, List<Persistable>> entry : newIssuersAndSubEntities.entrySet()) {
                        if (xt == null) {
                            xt = session.getTransaction();
                            xt.begin();
                        }

                        Issuer issuer = entry.getKey();
                        if (issuer.getAttemptsACSProId() != null) {
                            ACSProfile attemptsACP = acpForwardMappings.get(issuer.getAttemptsACSProId());
                            if (attemptsACP != null) {
                                if (attemptsACP.getId() != null &&
                                    !attemptsACP.getId().equals(issuer.getAttemptsACSProId())) {
                                    issuer.setAttemptsACSProId(attemptsACP.getId());
                                }
                            }
                        }

                        issuer.setId(null);
                        session.saveOrUpdate(issuer);
                        session.flush();
                        issuerAdd++;

                        int uc = 0;
                        List<Persistable> subEntities = entry.getValue();
                        Integer newIssuerId = issuer.getId();
                        for (Persistable subent : subEntities) {
                            if (subent instanceof CardRange) {
                                if (((CardRange) subent).getIsId() == null) {
                                    ((CardRange) subent).setIsId(importSessionId);
                                    ((CardRange) subent).setId(null);
                                    crAdd++;
                                } else {
                                    crUpdate++;
                                }
                                ((CardRange) subent).setIssuerId(newIssuerId);

                                if (((CardRange) subent).getLastMod() == null) {
                                    ((CardRange) subent).setLastMod(now);
                                }
                            } else if (subent instanceof ACSProfile) {
                                if (((ACSProfile) subent).getIsId() == null) {
                                    ((ACSProfile) subent).setIsId(importSessionId);
                                    ((ACSProfile) subent).setId(null);
                                    acpAdd++;
                                } else {
                                    acpUpdate++;
                                }
                                ((ACSProfile) subent).setIssuerId(newIssuerId);
                            }

                            session.saveOrUpdate(subent);
                            uc++;
                            if (uc % 200 == 0) {
                                session.flush();
                            }
                        }
                        session.flush();

                        subedone += uc;

                        icdone++;
                        if (icdone % commitStep == 0) {
                            xt.commit();
                            xt = null;
                            try {
                                session.clear();
                            } catch (Exception dc) {
                            }
                        }
                        if (icdone % 100 == 0) {
                            log.info("New Issuers done " + (icdone - 99) + "-" + icdone + "/" +
                                     newIssuersAndSubEntities.size() + "(+sub " + subedone + ") in " +
                                     (System.currentTimeMillis() - sk) + " ms");
                            sk = System.currentTimeMillis();
                        }
                    } // for

                    if (xt != null) {
                        xt.commit();
                        xt = null;
                    }

                    e = System.currentTimeMillis();
                    log.info("Added " + issuerAdd + " new issuers and " + subedone + " associated sub entities " +
                             subedone + " in " + (e - s) + " ms");
                    // clear no longer needed
                    newIssuersAndSubEntities.clear();

                    results.put("crOrphanIgnored", orphanCRIgnored);
                    results.put("crErrIgnored", crErrIgnored);
                    results.put("crEquals", crEquals);
                    results.put("crAdd", crAdd);
                    results.put("crUpdate", crUpdate);
                    results.put("crDelete", crDelete);
                    results.put("crDupIgnored", crDupIgnored);

                    results.put("acpOrphanIgnored", orphanACPIgnored);
                    results.put("acpEquals", acpEquals);
                    results.put("acpAdd", acpAdd);
                    results.put("acpUpdate", acpUpdate);
                    results.put("acpDelete", acpDelete);
                    results.put("acpDupIgnored", acpDupIgnored);

                    results.put("issuerEquals", issuerEquals);
                    results.put("issuerAdd", issuerAdd);
                    results.put("issuerUpdate", issuerUpdate);
                    results.put("issuerDupIgnored", issuerDupIgnored);

                    if (Mode.FULL.equals(mode)) {
                        log.info("FULL mode: " + (delete ? "deleting" : "ending") +
                                 " issuer entities not prsent in this session (" + importSessionId + ")");
                        try {
                            for (String type : cIssImportTypes) {
                                xt = session.beginTransaction();
                                xt.begin();
                                int deletedCount = deleteType(session, delete, importSessionId, type, null, null, null);
                                xt.commit();
                                xt = null;

                                log.info((delete ? "Deleted " : "Ended (set status to 'E') ") + deletedCount + " " +
                                         type + " entities from the database");
                                results.put((delete ? "fullDeleted" : "fullEnded") + type, deletedCount);
                            }
                        } catch (Exception ee) {
                            log.warn("Error during full load mode entity ending/deletion", ee);
                            if (xt != null) {
                                try {
                                    xt.rollback();
                                } catch (Exception eee) {
                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    log.error("Error updating issuer entities", e);
                    if (xt != null) {
                        try {
                            xt.rollback();
                        } catch (Exception dc) {
                        }
                    }
                    results.put("issuersImportError", 1);

                } finally {
                    try {
                        session.close();
                    } catch (Exception dc) {
                    }

                    results.put("crOrphanIgnored", orphanCRIgnored);
                    results.put("crErrIgnored", crErrIgnored);
                    results.put("crEquals", crEquals);
                    results.put("crAdd", crAdd);
                    results.put("crUpdate", crUpdate);
                    results.put("crDelete", crDelete);
                    results.put("crDupIgnored", crDupIgnored);

                    results.put("acpOrphanIgnored", orphanACPIgnored);
                    results.put("acpEquals", acpEquals);
                    results.put("acpAdd", acpAdd);
                    results.put("acpUpdate", acpUpdate);
                    results.put("acpDelete", acpDelete);
                    results.put("acpDupIgnored", acpDupIgnored);

                    results.put("issuerEquals", issuerEquals);
                    results.put("issuerAdd", issuerAdd);
                    results.put("issuerUpdate", issuerUpdate);
                    results.put("issuerDupIgnored", issuerDupIgnored);
                }

            }
        });

        FinishedRunnable acquirerRunnable = new FinishedRunnable(new Runnable() {
            @Override
            public void run() {
                //final Map<Acquirer, List<Merchant>> newAcquirersAndSubEntitiesME = new TreeMap<>();
                //final Map<Acquirer, List<Merchant>> acquirersAndSubEntitiesMEnewMI = new TreeMap<>();
                final Map<Integer, Acquirer> existingAcquirers = new LinkedHashMap<>();
                final Map<Integer, List<Number>> acquierMIEquals = new HashMap<>();
                final Map<Integer, List<Number>> acquierMEEquals = new HashMap<>();
                final Map<Integer, Acquirer> newAcquirers = new LinkedHashMap<>();
                //final List<Merchant> existingAcquirerEntitiesME = new ArrayList<>();

                final Map<String, Acquirer> acquirerBINMap = new TreeMap<>();
                final Map<Integer, Acquirer> acquirerMap = new TreeMap<>();
                final Map<Integer, Integer> acquirersForwardMappings = new HashMap<>();
                final Map<Integer, Integer> acquirersReverseMappings = new HashMap<>();
                final Map<Integer, List<TDSServerProfile>> acquirerTDSServerMap = new TreeMap<>();
                final Map<Integer, List<Merchant>> acquirerMEMap = new TreeMap<>();

                final Map<Long, List<Merchant>> newMIMEMap = new TreeMap<>();
                //final Map<Long, List<Acquirer>> newMIAMap = new TreeMap<>();
                final Map<Long, TDSServerProfile> tdsServerProfileMap = new HashMap<>();
                Map<Long, Long> tdsServerForwardMap = new TreeMap<>();
                Map<Long, Long> tdsServerBackwardMap = new TreeMap<>();

                int acquirersDupIgnored = 0;
                int acquirerEquals = 0;
                int orphanMIIgnored = 0;
                int miEquals = 0;
                int orphanMEIgnored = 0;
                int meEquals = 0;
                int acquirerUpdate = 0;
                int acquirerAdd = 0;
                int miAdd = 0;
                int miUpdate = 0;
                int miDelete = 0;
                int meAdd = 0;
                int meUpdate = 0;
                int meDelete = 0;
                int miDupIgnored = 0;
                int meDupIgnored = 0;

                log.info("Processing acquirers and subentities..");

                Session session = persistenceService.getSessionFactory().openSession();
                session.setFlushMode(FlushMode.MANUAL);
                Transaction tx = null;
                try {
                    boolean requestorIDwithMerchant = "true".equals(
                            persistenceService.getStringSetting(DsSetting.REQUESTOR_ID_WITH_MERCHANT.getKey()));
                    //check if acquirer already exists in db. match by issuer bin first then  name. Don't import if status = "E".
                    //if so, set its id equal to the one of the corresponding db entity.
                    //keep track of its file id to compare with acquirerId of merchants & acquirerId of tdsServerProfiles. If they match then take the generated Id
                    //and assign it to the merchants and tdsServerProfiles that had matching file ids.
                    //If there is no match set its id to null
                    if (importSource.getAcquirers() != null) {
                        for (TDSServerProfile tdsServerProfile : Misc.nullSafe(importSource.getTdsServerProfiles())) {
                            tdsServerProfileMap.put(tdsServerProfile.getId(), tdsServerProfile);
                        }

                        int accnt = 0;
                        log.info("Pre-Processing Acquirers " + importSource.getAcquirers().size() + "..");
                        long s = System.currentTimeMillis();
                        long sk = s;
                        for (Acquirer acquirer : importSource.getAcquirers()) {
                            accnt++;
                            if (!withEnded && DSModel.Acquirer.Status.ENDED.equals(acquirer.getStatus())) {
                                continue;
                            }
                            Acquirer dup = acquirerBINMap.get(acquirer.getBIN());
                            if (dup != null) {
                                log.warn("File Acquirer " + acquirer.getId() + " " + acquirer.getName() + " " +
                                         acquirer.getBIN() + " bin is conflicting with earlier file acquirer" +
                                         dup.getId() + " " + dup.getName() + " " + dup.getBIN() + " and is ignored");
                                acquirersDupIgnored++;
                                continue;
                            }

                            if (accnt % 1000 == 0) {
                                log.info("Pre-Processed ACQ " + (accnt - 999) + "-" + accnt + "/" +
                                         importSource.getAcquirers().size() + " in " +
                                         (System.currentTimeMillis() - sk) + " ms");
                                sk = System.currentTimeMillis();
                            }

                            acquirerBINMap.put(acquirer.getBIN(), acquirer);
                            acquirer.setIsId(importSessionId);
                            Integer acquirerIdFile = acquirer.getId();
                            acquirerMap.put(acquirer.getId(), acquirer);

                            Acquirer acqMatch = findAcquirerMatch(acquirer);
                            if (acqMatch != null) {
                                acquirersForwardMappings.put(acquirer.getId(), acqMatch.getId());
                                acquirersReverseMappings.put(acqMatch.getId(), acquirer.getId());

                                Long miidback = acquirer.getTdsServerProfileId();
                                if (miidback != null) {
                                    TDSServerProfile tdsServerMatchFile = tdsServerProfileMap.get(miidback);
                                    TDSServerProfile tdsServerMatch = tdsServerMatchFile != null ? findTdsServerProfileMatch(
                                            requestorIDwithMerchant ? null : tdsServerMatchFile.getRequestorID(),
                                            tdsServerMatchFile.getOperatorID(), acqMatch.getId()) : null;
                                    if (tdsServerMatch != null) {
                                        acquirer.setTdsServerProfileId(tdsServerMatch.getId());
                                    } else {
                                        acquirer.setTdsServerProfileId(null);
                                    }
                                }

                                acquirer.setId(acqMatch.getId());
                                if (acquirer.equals(acqMatch)) {
                                    acquirerEquals++;
                                    if (!Mode.FULL.equals(mode)) {
                                        acquirer.setIsId(null); // no need to update
                                    }
                                } else {
                                    acquirer.setTdsServerProfileId(miidback);
                                }

                                existingAcquirers.put(acquirer.getId(), acquirer);
                                acquierMIEquals.put(acquirer.getId(), new LinkedList<Number>());
                                acquierMEEquals.put(acquirer.getId(), new LinkedList<Number>());
                            } else {
                                newAcquirers.put(acquirer.getId(), acquirer);
                            }
                            acquirerMEMap.put(acquirerIdFile, new LinkedList<Merchant>());
                            acquirerTDSServerMap.put(acquirerIdFile, new LinkedList<TDSServerProfile>());
                        }
                        long e = System.currentTimeMillis();
                        log.info(
                                "Pre-Processing of " + importSource.getAcquirers().size() + " Acquirers completed in " +
                                (e - s) + "ms");
                    }
                    if (importSource.getTdsServerProfiles() != null) {
                        int micnt = 0;
                        log.info("Pre-Processing TDSServerProfiles " + importSource.getTdsServerProfiles().size() + "..");
                        long s = System.currentTimeMillis();
                        long sk = s;
                        java.util.Map<Long, TDSServerProfile> dupCheck = new java.util.TreeMap<>();
                        for (TDSServerProfile tdsServerProfile : importSource.getTdsServerProfiles()) {
                            micnt++;
                            if (micnt % 1000 == 0) {
                                log.info("Pre-Processed MI " + (micnt - 999) + "-" + micnt + "/" +
                                         importSource.getTdsServerProfiles().size() + " in " +
                                         (System.currentTimeMillis() - sk) + " ms");
                                sk = System.currentTimeMillis();
                            }

                            if (!withEnded && DSModel.TDSServerProfile.Status.ENDED.equals(tdsServerProfile.getStatus())) {
                                continue;
                            }

                            if (tdsServerProfile.getId() != null) {
                                if (dupCheck.get(tdsServerProfile.getId()) != null) {
                                    log.warn("Duplicate same id TDSServerProfile " + tdsServerProfile.getId() + " " + tdsServerProfile.getName() +
                                             " detected in input and will be ignored");
                                    miDupIgnored++;
                                    continue;
                                }
                                dupCheck.put(tdsServerProfile.getId(), tdsServerProfile);
                            }

                            Acquirer acquirer =
                                    tdsServerProfile.getAcquirerId() != null ? acquirerMap.get(tdsServerProfile.getAcquirerId()) : null;
                            if (acquirer == null) {
                                log.warn("TDSServerProfile " + tdsServerProfile.getId() + " acq " + tdsServerProfile.getAcquirerId() + " re " +
                                         tdsServerProfile.getRequestorID() + " has no existing or new acquirer, ignored");
                                orphanMIIgnored++;
                                continue;
                            }

                            // acquirer may have transferred so acquirer matching still needed

                            //Integer aid1;
                            Integer aidm = tdsServerProfile.getAcquirerId() != null ? acquirersForwardMappings.get(
                                    tdsServerProfile.getAcquirerId()) : null;
                            TDSServerProfile match = aidm != null ? findTdsServerProfileMatch(
                                    requestorIDwithMerchant ? null : tdsServerProfile.getRequestorID(), tdsServerProfile.getOperatorID(),
                                    aidm) : null;
                            List<TDSServerProfile> tdsServerProfiles = acquirerTDSServerMap.get(tdsServerProfile.getAcquirerId());
                            if (match != null) {
                                tdsServerForwardMap.put(tdsServerProfile.getId(), match.getId());
                                tdsServerBackwardMap.put(match.getId(), tdsServerProfile.getId());

                                tdsServerProfile.setId(match.getId());
                                tdsServerProfile.setAcquirerId(match.getAcquirerId());
                                if (tdsServerProfile.equals(match)) {
                                    miEquals++;
                                    if (Mode.FULL.equals(mode)) {
                                        tdsServerProfiles.add(tdsServerProfile);
                                    } else {
                                        acquierMIEquals.get(tdsServerProfile.getAcquirerId()).add(tdsServerProfile.getId());
                                    }
                                } else {
                                    tdsServerProfiles.add(tdsServerProfile);
                                }
                                tdsServerProfile.setIsId(importSessionId);
                            } else {
                                newMIMEMap.put(tdsServerProfile.getId(), new LinkedList<Merchant>());
                                tdsServerProfiles.add(tdsServerProfile);
                            }
                        }

                        long e = System.currentTimeMillis();
                        log.info("Pre-Processing of " + importSource.getTdsServerProfiles().size() +
                                 " TDSServerProfiles completed in " + (e - s) + " ms");
                    }

                    Map<String, Merchant> meMap = new TreeMap<>();
                    if (importSource.getMerchants() != null) {
                        log.info("Pre-Processing Merchants " + importSource.getMerchants().size() + "..");
                        int step = 100;
                        List<Merchant> subList = new ArrayList<>(step);
                        List<String> subListId = new ArrayList<>(step);
                        List<Integer> subListAId = new ArrayList<>(step);

                        long s = System.currentTimeMillis();
                        long sk = s;
                        int counter = 0;
                        for (int i1 = 0; i1 < importSource.getMerchants().size(); i1 = i1 + step) {
                            subList.clear();
                            subListId.clear();
                            subListAId.clear();

                            for (int i2 = 0; i2 < step && i1 + i2 < importSource.getMerchants().size(); i2++) {
                                Merchant mer = importSource.getMerchants().get(i1 + i2);
                                counter++;
                                if (counter % 1000 == 0) {
                                    long ex = System.currentTimeMillis();
                                    log.info("Pre-Processed mer " + (counter - 999) + "-" + counter + "/" +
                                             importSource.getMerchants().size() + " in " + (ex - sk) + " ms");
                                    sk = System.currentTimeMillis();
                                }
                                if (!withEnded && DSModel.Merchant.Status.ENDED.equals(mer.getStatus())) {
                                    continue;
                                }

                                Acquirer acquirer = acquirerMap.get(mer.getAcquirerId());
                                if (acquirer == null) {
                                    log.warn("Merchant " + mer.getId() + "/" + mer.getIdentifier() + "/" +
                                             mer.getName() + " has mapping to not existing or new Acquirer id:" +
                                             mer.getAcquirerId() + " (in file), ignored");
                                    orphanMEIgnored++;
                                    continue;
                                }

                                if (mer.getTdsServerProfileId() != null && tdsServerForwardMap.get(mer.getTdsServerProfileId()) == null &&
                                    newMIMEMap.get(mer.getTdsServerProfileId()) == null) {
                                    log.warn("Merchant " + mer.getId() + "/" + mer.getIdentifier() + "/" +
                                             mer.getName() + " ACQ id:" + mer.getAcquirerId() + " " +
                                             " has mapping to not existing or new MI id: " + mer.getTdsServerProfileId() +
                                             " (in file), ignored");
                                    orphanMEIgnored++;
                                    continue;
                                }

                                String key = mer.getAcquirerId() + "." + mer.getIdentifier();

                                Merchant dup = meMap.get(key);
                                if (dup != null) {
                                    log.warn("Merchant " + mer.getId() + " " + mer.getIdentifier() + " " +
                                             mer.getName() + " " + mer.getAcquirerId() +
                                             " is conflicting with earlier file Merchant with same identifier and acquirer id " +
                                             dup.getId() + " " + dup.getIdentifier() + " " + dup.getName() + " " +
                                             dup.getAcquirerId() + " and is ignored");
                                    meDupIgnored++;
                                    continue;
                                }

                                meMap.put(key, mer);
                                subList.add(mer);

                                Integer aidm = mer.getAcquirerId() != null ? acquirersForwardMappings.get(
                                        mer.getAcquirerId()) : null;
                                if (mer.getIdentifier() != null && aidm != null) {
                                    subListId.add(mer.getIdentifier());
                                    subListAId.add(aidm);
                                }
                            }

                            Map<String, Merchant> matchMap = acquirerService.getMerchantsByIdentifierAndAcquirerId(subListId, subListAId);

                            for (Merchant mer : subList) {
                                mer.setIsId(importSessionId);
                                // must check acquirer matching as well
                                Integer aidm = mer.getAcquirerId() != null ? acquirersForwardMappings.get(mer.getAcquirerId()) : null;
                                Merchant match = matchMap.get(mer.getIdentifier() + "-" + aidm);
                                List<Merchant> mel = acquirerMEMap.get(mer.getAcquirerId());
                                Long mappedMIId = mer.getTdsServerProfileId() != null ? tdsServerForwardMap.get(mer.getTdsServerProfileId()) : null;
                                if (mappedMIId != null) {
                                    mer.setTdsServerProfileId(mappedMIId);
                                }

                                if (match != null) {
                                    Acquirer acquirer = acquirerMap.get(mer.getAcquirerId());
                                    mer.setId(match.getId());
                                    mer.setAcquirerId(match.getAcquirerId());
                                    if (mer.equals(match)) { //if the merchants tdsServerprofile is removed then this will not match (and will not increment the counter)
                                        meEquals++;
                                        if (Mode.FULL.equals(mode)) {
                                            mel.add(mer);
                                        } else {
                                            acquierMEEquals.get(mer.getAcquirerId()).add(mer.getId());
                                        }
                                    } else {
                                        mel.add(mer);
                                        // merchants with new MI
                                        if (mappedMIId == null) {

                                            if (mer.getTdsServerProfileId() == null && acquirer.getTdsServerProfileId() == null) {
                                                log.warn("Merchant " + mer.getId() + " " + mer.getIdentifier() + " " +
                                                         mer.getName() + " " + mer.getAcquirerId() +
                                                         " does not map to any TDS Server profile and has TDSServerprofiled id null and itsd acquierer has" +
                                                         "no default TDSServer profile set, this merchant may start to fail processing");
                                            } else {
                                                List<Merchant> melmi = newMIMEMap.get(mer.getTdsServerProfileId());
                                                if (melmi != null) {
                                                    melmi.add(mer);
                                                }
                                            }
                                        } else {
                                            mer.setTdsServerProfileId(mappedMIId);
                                        }
                                    }
                                } else {
                                    mer.setIsId(null);
                                    mel.add(mer);
                                    // merchants with new MI
                                    if (mappedMIId == null) {
                                        List<Merchant> melmi = mer.getTdsServerProfileId() != null ? newMIMEMap.get(
                                                mer.getTdsServerProfileId()) : null;
                                        if (melmi != null) {
                                            melmi.add(mer);
                                        }
                                    } else {
                                        mer.setTdsServerProfileId(mappedMIId);
                                    }
                                }
                            }
                        }
                        long e = System.currentTimeMillis();
                        log.info(
                                "Pre-Processing of " + importSource.getMerchants().size() + " Merchants completed in " +
                                (e - s) + " ms");
                    }

                    log.info("Updating existing Acquirers " + existingAcquirers.size() + " start tx");
                    long s = System.currentTimeMillis();
                    long sk = s;
                    for (Acquirer ax : existingAcquirers.values()) {
                        if (tx == null) {
                            tx = session.getTransaction();
                            tx.begin();
                        }

                        List<Number> meEqualsLst = acquierMEEquals.get(ax.getId());
                        List<Number> miEqualsLst = acquierMIEquals.get(ax.getId());

                        Integer aidFile = acquirersReverseMappings.get(ax.getId());
                        List<TDSServerProfile> tdsServerProfiles = aidFile != null ? acquirerTDSServerMap.get(aidFile) : null;
                        for (TDSServerProfile tdsServerProfile : Misc.nullSafe(tdsServerProfiles)) {
                            List<Merchant> mel = tdsServerProfile.getId() != null ? newMIMEMap.get(tdsServerProfile.getId()) : null;
                            tdsServerProfile.setAcquirerId(ax.getId());
                            if (tdsServerProfile.getIsId() == null) {
                                Long miIdBak = tdsServerProfile.getId();
                                tdsServerProfile.setId(null);
                                tdsServerProfile.setIsId(importSessionId);
                                miAdd++;
                                session.saveOrUpdate(tdsServerProfile);

                                if (ax.getIsId() == null && ax.getTdsServerProfileId() != null &&
                                    ax.getTdsServerProfileId().equals(miIdBak)) {
                                    ax.setTdsServerProfileId(tdsServerProfile.getId());
                                }

                                for (Merchant mx : Misc.nullSafe(mel)) {
                                    mx.setTdsServerProfileId(tdsServerProfile.getId());
                                }
                            } else {
                                miUpdate++;
                                session.saveOrUpdate(tdsServerProfile);
                            }
                        } // for mi

                        // new mi merchants are here already up to date
                        List<Merchant> mel = aidFile != null ? acquirerMEMap.get(aidFile) : null;
                        for (Merchant mix : Misc.nullSafe(mel)) {
                            if (mix.getIsId() == null) {
                                mix.setId(null);
                                mix.setIsId(importSessionId);
                                meAdd++;
                            } else {
                                meUpdate++;
                            }
                            mix.setAcquirerId(ax.getId());

                            if (mix.getId() != null && session.contains(mix)) {
                                log.warn("Duplicate same id Merchant " + mix.getId() + " " + mix.getName() +
                                         " detected in input and will be ignored");
                                meDupIgnored++;
                                continue;
                            }

                            session.saveOrUpdate(mix);

                        } // for me

                        if (ax.getIsId() != null) {
                            session.saveOrUpdate(ax);
                            acquirerUpdate++;
                            session.flush();
                        }

                        // delte
                        miDelete += deleteType(session, delete, importSessionId, "TDSServerProfile", "acquirerId", ax.getId(),
                                               miEqualsLst);
                        meDelete += deleteType(session, delete, importSessionId, "Merchant", "acquirerId", ax.getId(),
                                               meEqualsLst);
                        session.flush();

                        if (acquirerUpdate % commitStep == 0) {
                            tx.commit();
                            tx = null;
                            try {
                                session.clear();
                            } catch (Exception dc) {
                            }
                        }

                        if (acquirerUpdate > 0 && acquirerUpdate % 100 == 0) {
                            log.info("Existing Acquirers done " + (acquirerUpdate - 99) + "-" + acquirerUpdate + "/" +
                                     existingAcquirers.size() + " in " + (System.currentTimeMillis() - sk) + " ms " +
                                     " (miu " + miUpdate + " mi add " + miAdd + " del mi " + miDelete + ", meu " +
                                     meUpdate + " me add " + meAdd + " del me " + meDelete + ")");
                            sk = System.currentTimeMillis();
                        }
                    } // for

                    if (tx != null) {
                        tx.commit();
                        tx = null;
                        try {
                            session.clear();
                        } catch (Exception dc) {
                        }
                    }

                    long e = System.currentTimeMillis();
                    log.info("Update Acquirers done " + existingAcquirers.size() + " (miu " + miUpdate + " mi add " +
                             miAdd + " del mi " + miDelete + ", meu " + meUpdate + " me add " + meAdd + " del me " +
                             meDelete + ") in " + (e - s) + " ms");

                    if (!Mode.FULL.equals(mode)) {
                        log.info("Not Updated " + acquirerEquals + " exact matched existing Acquirer entities");
                    }
                    existingAcquirers.clear();

                    log.info("Adding new Acquirers " + newAcquirers.size() + " and sub entities.. ");
                    s = System.currentTimeMillis();
                    sk = s;
                    for (Acquirer ax : newAcquirers.values()) {
                        if (tx == null) {
                            tx = session.getTransaction();
                            tx.begin();
                        }
                        Integer aidFile = ax.getId();
                        ax.setId(null);
                        session.saveOrUpdate(ax);
                        session.flush();
                        acquirerAdd++;

                        //int sub = miAdd + miUpdate;
                        List<TDSServerProfile> tdsServerProfiles = aidFile != null ? acquirerTDSServerMap.get(aidFile) : null;
                        for (TDSServerProfile tdsServerProfile : Misc.nullSafe(tdsServerProfiles)) {
                            List<Merchant> mel = tdsServerProfile.getId() != null ? newMIMEMap.get(tdsServerProfile.getId()) : null;
                            tdsServerProfile.setAcquirerId(ax.getId());
                            //if (mix.getIsId() == null)
                            //{
                            Long miIdBak = tdsServerProfile.getId();
                            tdsServerProfile.setId(null);
                            tdsServerProfile.setIsId(importSessionId);
                            miAdd++;
                            session.saveOrUpdate(tdsServerProfile);

                            // update acq to new mi id
                            if (ax.getTdsServerProfileId() != null && ax.getTdsServerProfileId().equals(miIdBak)) {
                                ax.setTdsServerProfileId(tdsServerProfile.getId());
                                session.saveOrUpdate(ax);
                                session.flush();
                            }

                            for (Merchant mex : Misc.nullSafe(mel)) {
                                mex.setTdsServerProfileId(tdsServerProfile.getId());
                            }

                            //}
                            //else
                            //{
                            //	miUpdate++;
                            //	session.saveOrUpdate(mix);
                            //}
                        }

                        session.flush();
                        // new mi merchants are here already up to date
                        List<Merchant> mel = aidFile != null ? acquirerMEMap.get(aidFile) : null;
                        for (Merchant mix : Misc.nullSafe(mel)) {
                            mix.setAcquirerId(ax.getId());
                            if (mix.getIsId() == null) {
                                mix.setId(null);
                                mix.setIsId(importSessionId);
                                meAdd++;
                                session.saveOrUpdate(mix);
                            } else // should not happen
                            {
                                meUpdate++;
                                session.saveOrUpdate(mix);
                            }
                        }

                        session.flush();
                        if (acquirerAdd % commitStep == 0) {
                            tx.commit();
                            tx = null;
                            try {
                                session.clear();
                            } catch (Exception dc) {
                            }
                        }

                        if (acquirerAdd % 100 == 0) {
                            log.info("New Acquirers done " + (acquirerAdd - 99) + "-" + acquirerAdd + "/" +
                                     newAcquirers.size() + " in " + (System.currentTimeMillis() - sk) + " ms" +
                                     " (mi add " + miAdd + " me add " + meAdd + ")");
                            sk = System.currentTimeMillis();
                        }
                    } // for acquirers

                    if (tx != null) {
                        tx.commit();
                        tx = null;
                        try {
                            session.clear();
                        } catch (Exception dc) {
                        }
                    }

                    //session.flush();
                    e = System.currentTimeMillis();

                    log.info("Inserted " + acquirerAdd + " new acquirers and associated miadd " + miAdd + " meadd " +
                             meAdd + " in " + (e - s) + " ms");

                    if (!Mode.FULL.equals(mode)) {
                        log.info("Not Updated " + acquirerEquals + " exact matched existing acquirer entities");
                        log.info("Not Updated " + meEquals + " exact matched existing merchant entities");
                        log.info("Not Updated " + miEquals + " exact matched existing mi entities");
                    }

                    results.put("acquirersDupIgnored", acquirersDupIgnored);
                    results.put("orphanMEIgnored", orphanMEIgnored);
                    results.put("orphanMIIgnored", orphanMIIgnored);

                    results.put("acquirerEquals", acquirerEquals);
                    results.put("miEquals", miEquals);
                    results.put("meEquals", meEquals);

                    results.put("acquirerAdd", acquirerAdd);
                    results.put("acquirerUpdate", acquirerUpdate);

                    results.put("miAdd", miAdd);
                    results.put("miUpdate", miUpdate);
                    results.put("miDelete", miDelete);
                    results.put("miDupIgnored", miDupIgnored);

                    results.put("meAdd", meAdd);
                    results.put("meUpdate", meUpdate);
                    results.put("meDelete", meDelete);
                    results.put("meDupIgnored", meDupIgnored);

                    if (Mode.FULL.equals(mode)) {
                        log.info("FULL mode: " + (delete ? "deleting" : "ending") +
                                 " acquirer entities not prsent in this session (" + importSessionId + ")");
                        try {
                            for (String type : cAcqImportTypes) {
                                tx = session.beginTransaction();
                                tx.begin();
                                int deletedCount = deleteType(session, delete, importSessionId, type, null, null, null);
                                tx.commit();
                                tx = null;

                                log.info((delete ? "Deleted " : "Ended (set status to 'E') ") + deletedCount + " " +
                                         type + " entities from the database");
                                results.put((delete ? "fullDeleted" : "fullEnded") + type, deletedCount);
                            }
                        } catch (Exception ee) {
                            log.warn("Error during full load mode entity ending/deletion", ee);
                            if (tx != null) {
                                try {
                                    tx.rollback();
                                } catch (Exception eee) {
                                }
                            }
                        }
                        try {
                            session.close();
                        } catch (Exception dc) {
                        }
                    }

                } catch (Exception e) {
                    log.error("Error updating acquirer entities", e);
                    if (tx != null) {
                        try {
                            tx.rollback();
                        } catch (Exception dc) {
                        }
                    }
                    results.put("acquirersImportError", 1);
                } finally {
                    try {
                        session.close();
                    } catch (Exception dc) {
                    }

                    results.put("acquirersDupIgnored", acquirersDupIgnored);
                    results.put("orphanMEIgnored", orphanMEIgnored);
                    results.put("orphanMIIgnored", orphanMIIgnored);

                    results.put("acquirerEquals", acquirerEquals);
                    results.put("miEquals", miEquals);
                    results.put("meEquals", meEquals);

                    results.put("acquirerAdd", acquirerAdd);
                    results.put("acquirerUpdate", acquirerUpdate);

                    results.put("miAdd", miAdd);
                    results.put("miUpdate", miUpdate);
                    results.put("miDelete", miDelete);

                    results.put("meAdd", meAdd);
                    results.put("meUpdate", meUpdate);
                    results.put("meDelete", meDelete);

                }

            }
        });

        log.info("Starting issuer and acquirer flow parallel processing..");
        // run issuer and acquirer side in parallel in 2 separated tx
        ServiceLocator.getInstance().getScheduledExecutorService().execute(issuerRunnable);
        ServiceLocator.getInstance().getScheduledExecutorService().execute(acquirerRunnable);

        while (!(issuerRunnable.finished && acquirerRunnable.finished)) {
            if (!issuerRunnable.finished && !acquirerRunnable.finished) {
                log.info("Waiting for processing threads to finish..");
            } else if (!issuerRunnable.finished) {
                log.info("Waiting for issuer thread to finish..");
            } else if (!acquirerRunnable.finished) {
                log.info("Waiting for acquirer thread to finish..");
            }

            synchronized (syno) {
                syno.wait(30000);
            }
        }
        log.info("Import processing completed, set reload flags");

        try {
            Setting s = new Setting();
            s.setKey(DsSetting.DS_RELOAD_RANGES.getKey());
            s.setValue("100");
            s.setLastModified(new java.util.Date());
            s.setLastModifiedBy(cLastModifier);
            pse.saveOrUpdate(s);
        } catch (Exception e) {
            log.warn("Error during update reload settings", e);
        }

        log.info("Import complete, execution time: " + (System.currentTimeMillis() - start) + " ms\n" +
                 resultsToString(results));

        return results;
    }

    boolean acpSame(ACSProfile a1, ACSProfile a2) {
        if (a1 != null && a2 != null) {
            return Misc.defaultString(a1.getThreeDSMethodURL()).equals(Misc.defaultString(a2.getThreeDSMethodURL())) &&
                   Misc.defaultString(a1.getInClientCert()).equals(Misc.defaultString(a2.getInClientCert())) &&
                   Misc.defaultString(a1.getName()).equals(Misc.defaultString(a2.getName())) &&
                   Misc.defaultString(a1.getRefNo()).equals(Misc.defaultString(a2.getRefNo())) &&
                   Misc.defaultString(a1.getSet()).equals(Misc.defaultString(a2.getSet())) &&
                   Misc.defaultString(a1.getSSLProto()).equals(Misc.defaultString(a2.getSSLProto())) &&
                   Misc.defaultString(a1.getStatus()).equals(Misc.defaultString(a2.getStatus())) &&
                   Misc.defaultString(a1.getURL()).equals(Misc.defaultString(a2.getURL()));
        }


        return false;
    }

    int deleteType(Session session, boolean delete, byte importSessionId, String type, String idName, Serializable id, List<Number> exludeIds) {
        int deletedCount = 0;

        StringBuilder exl = new StringBuilder(exludeIds != null ? exludeIds.size() * 10 : 0);
        if (exludeIds != null && exludeIds.size() > 0) {
            for (Number s : exludeIds) {
                if (exl.length() == 0) {
                    exl.append(" and id not in (");
                }
                exl.append(s);
                exl.append(',');

            }
            if (exl.length() > 0) {
                exl.setCharAt(exl.length() - 1, ')');
            }
        }

        Query query = delete ? session.createQuery("delete from " + type + " where isid!=:isx " +
                                                   (idName != null && id != null ? " and " + idName + "=:idx" : "") +
                                                   exl) : session.createQuery(
                "update " + type + " set status='E' where isid!=:isx and status!='E'" + exl +
                (idName != null && id != null ? " and " + idName + "=:idx" : ""));

        query.setParameter("isx", importSessionId);
        if (idName != null && id != null) {
            query.setParameter("idx", id);
        }

        deletedCount = query.executeUpdate();

        return deletedCount;
    }

    public String resultsToString(Map<String, Integer> results) {
        StringBuilder rsb = new StringBuilder();
        for (Map.Entry<String, Integer> e : results.entrySet()) {
            rsb.append("	" + e.getKey() + ": " + e.getValue() + "\n");
        }

        return rsb.toString();
    }

    private Issuer findIssuerMatch(Issuer issuer) throws Exception {
        IssuerSearcher is = new IssuerSearcher();
        is.setBIN(issuer.getBIN());
        List<Issuer> results = issuerService.getIssuersByQuery(is);
        if (results.size() == 1) {
            return results.get(0);
        } else if (results.size() > 1) {
            for (Issuer i : results) {
                if (issuer.getName().equals(i.getName())) {
                    return i;
                }
            }
        }

        return null;
    }

    private Acquirer findAcquirerMatch(Acquirer acquirer) throws Exception {
        IssuerSearcher is = new IssuerSearcher();
        is.setBIN(acquirer.getBIN());
        List<Acquirer> results = acquirerService.getAcquirersByQuery(is);
        if (results.size() == 1) {
            return results.get(0);
        } else if (results.size() > 1) {
            for (Acquirer a : results) {
                if (acquirer.getName().equals(a.getName())) {
                    return a;
                }
            }
        }
        return null;
    }

    private ACSProfile findACSProfileMatch(String refNo, Integer issid) {
        List<ACSProfile> profilesByRefNo = issuerService.getACSProfilesByRefNoAndIssId(refNo, issid);
        if (profilesByRefNo != null && profilesByRefNo.size() > 0) {
            return profilesByRefNo.get(0);
        }
        return null;
    }

    private TDSServerProfile findTdsServerProfileMatch(String requestorId, String operatorId, Integer acqId) {
        List<TDSServerProfile> results = acquirerService.getTdsServerProfilesByRequestorIdOrOperatorId(requestorId,
                                                                                                       operatorId);
        if (results.size() > 0) {
            return results.get(0);
        }
        return null;
    }

    private static class MyValidationEventHandler implements ValidationEventHandler {
        List<Error> errors;

        private MyValidationEventHandler(List<Error> errors) {
            this.errors = errors;
        }

        @Override
        public boolean handleEvent(ValidationEvent event) {
            ValidationEventLocator vel = event.getLocator();
            if (event.getSeverity() != ValidationEvent.ERROR) {
                Error error = new Error();
                error.setDescription(
                        "Line:Col[" + vel.getLineNumber() + ":" + vel.getColumnNumber() + "]:" + event.getMessage());
                errors.add(error);
                return true;
            } else {
                Error error = new Error();
                error.setDescription("Error:Line:Col[" + vel.getLineNumber() + ":" + vel.getColumnNumber() + "]:" +
                                     event.getMessage());
                errors.add(error);
                return false;
            }

        }
    }

    public int exportEntities(File file, boolean validate) throws Exception {
        Integer idStart = 0;
        int totalEntities = 0;
        DS entities = new DS();

        List<Issuer> issuers;
        List<Acquirer> acquirers;
        List<TDSServerProfile> tdsServerProfiles;
        List<ACSProfile> acsProfiles;
        List<CardRange> cardRanges;
        List<Merchant> merchants;

        final Integer limit = 250; // what should the limit be?

        entities.setTdsServerProfiles(new ArrayList<TDSServerProfile>());
        entities.setACSProfiles(new ArrayList<ACSProfile>());
        entities.setCardRanges(new ArrayList<CardRange>());
        entities.setAcquirers(new ArrayList<Acquirer>());
        entities.setIssuers(new ArrayList<Issuer>());
        entities.setMerchants(new ArrayList<Merchant>());

        do {
            tdsServerProfiles = persistenceService.getPersitableList(TDSServerProfile.class, idStart, limit);
            idStart += tdsServerProfiles.size();

            if (tdsServerProfiles.size() > 0) {
                entities.getTdsServerProfiles().addAll(tdsServerProfiles);
            } else {
                break;
            }

        } while (true);

        log.info("Found " + idStart + " TDSServerProfiles");
        totalEntities += idStart;
        idStart = 0;

        do {
            issuers = persistenceService.getPersitableList(Issuer.class, idStart, limit);
            idStart += issuers.size();

            if (issuers.size() > 0) {
                entities.getIssuers().addAll(issuers);
            } else {
                break;
            }

        } while (true);

        log.info("Found " + idStart + " Issuers");
        totalEntities += idStart;
        idStart = 0;

        do {
            acquirers = persistenceService.getPersitableList(Acquirer.class, idStart, limit);
            idStart += acquirers.size();

            if (acquirers.size() > 0) {
                entities.getAcquirers().addAll(acquirers);
            } else {
                break;
            }

        } while (true);

        log.info("Found " + idStart + " acquirers");
        totalEntities += idStart;
        idStart = 0;

        do {
            acsProfiles = persistenceService.getPersitableList(ACSProfile.class, idStart, limit);
            idStart += acsProfiles.size();

            if (acsProfiles.size() > 0) {
                entities.getACSProfiles().addAll(acsProfiles);
            } else {
                break;
            }

        } while (true);

        log.info("Found " + idStart + " ACSProfiles");
        totalEntities += idStart;
        idStart = 0;

        do {
            merchants = persistenceService.getPersitableList(Merchant.class, idStart, limit);
            idStart += merchants.size();

            if (merchants.size() > 0) {
                entities.getMerchants().addAll(merchants);
            } else {
                break;
            }

        } while (true);

        log.info("Found " + idStart + " Merchants");
        totalEntities += idStart;
        idStart = 0;

        do {
            cardRanges = persistenceService.getPersitableList(CardRange.class, idStart, limit);
            idStart += cardRanges.size();

            if (cardRanges.size() > 0) {
                entities.getCardRanges().addAll(cardRanges);
            } else {
                break;
            }

        } while (true);

        log.info("Found " + idStart + " CardRanges");
        totalEntities += idStart;

        log.info("Found " + totalEntities + " entities, initiating marshalling");

        OutputStream os = null;
        try {
            boolean gzip = file.getName().toLowerCase().endsWith(".gz");

            os = gzip ? new java.util.zip.GZIPOutputStream(new java.io.FileOutputStream(file), 32 * 1024,
                                                           false) : new java.io.FileOutputStream(file);
            marshall(entities, os, validate);
        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }

        return totalEntities;

    }

    public void marshall(DS toExport, OutputStream os, boolean validate) throws JAXBException, SAXException {
        if (jc == null) {
            jc = JAXBContext.newInstance(DS.class);
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schema = factory.newSchema(new StreamSource(ImportService.class.getResourceAsStream("/dsxmodel.xsd")));
        }
        Marshaller m = jc.createMarshaller();
        if (validate) {
            m.setSchema(schema);
        }
        m.setProperty("jaxb.formatted.output", true);
        m.marshal(toExport, os);

    }

    public DS generateTestEntities(int issuerCount, int acquirerCount, int merchantCount, int acsProfileCount, int tdsServerProfileCount, int cardRangeCount) {
        DS result = new DS();
        ArrayList<Merchant> ml = new ArrayList<Merchant>(merchantCount);
        result.setMerchants(ml);
        ArrayList<Issuer> il = new ArrayList<Issuer>(issuerCount);
        result.setIssuers(il);
        ArrayList<CardRange> crl = new ArrayList<CardRange>(cardRangeCount);
        result.setCardRanges(crl);
        ArrayList<ACSProfile> acpl = new ArrayList<ACSProfile>(acsProfileCount);
        result.setACSProfiles(acpl);
        ArrayList<TDSServerProfile> tdsServerProfiles = new ArrayList<TDSServerProfile>(tdsServerProfileCount);
        result.setTdsServerProfiles(tdsServerProfiles);
        ArrayList<Acquirer> acql = new ArrayList<Acquirer>(acquirerCount);
        result.setAcquirers(acql);

        for (int i = 1; i <= acquirerCount; i++) {
            Acquirer acquirer = new Acquirer();
            acquirer.setId(i);
            acquirer.setName("Bank ACQ" + i);
            acquirer.setStatus(DSModel.Acquirer.Status.ACTIVE);
            acquirer.setAddress("Street " + ((i / 10) + 1) + " House " + i);
            acquirer.setCity("City " + i);
            acquirer.setBIN("5" + Misc.padCutStringLeft("" + i, '0', 5));
            acquirer.setCountry(i % 2 == 0 ? "EE" : "RU");
            acquirer.setPhone("+3721234" + i);
            acquirer.setEmail("acqemail@" + i + "somedomain.ru");
            acql.add(acquirer);
        }

        for (long i = 1; i <= tdsServerProfileCount; i++) {
            TDSServerProfile profile = new TDSServerProfile();
            profile.setId(i);
            int acqId = ThreadLocalRandom.current().nextInt(0, acquirerCount);
            Acquirer acq = acql.get(acqId);
            profile.setAcquirerId(acq != null ? acq.getId() : 1);
            if (acq != null) {
                acq.setTdsServerProfileId(profile.getId());
            }

            profile.setName("3DS Server " + i);
            profile.setInClientCert(i);
            profile.setOutClientCert(i);
            profile.setSSLProto("TLSv1.2");
            profile.setStatus(i % 99 == 0 ? DSModel.TDSServerProfile.Status.DISABLED : DSModel.TDSServerProfile.Status.ACTIVE);
            profile.setRequestorID("88" + 10 * i);
            tdsServerProfiles.add(profile);
        }

        for (long i = 1; i <= merchantCount; i++) {
            Merchant merchant = new Merchant();
            merchant.setId(i);
            int mii = ThreadLocalRandom.current().nextInt(0, tdsServerProfileCount);

            TDSServerProfile tdsServerProfile = tdsServerProfiles.get(mii);
            if (tdsServerProfile != null) {
                merchant.setAcquirerId(tdsServerProfile.getAcquirerId());
                merchant.setTdsServerProfileId(tdsServerProfile.getId());
            }

            merchant.setPhone("+3721234" + i);
            merchant.setStatus(i % 99 == 0 ? DSModel.Merchant.Status.DISABLED : DSModel.Merchant.Status.ACTIVE);
            merchant.setCountry(i % 2 == 0 ? "EE" : "RU");
            merchant.setIdentifier(Misc.padCutStringLeft("" + i, '0', 8));
            merchant.setURL("https://soememerchant" + i + ".com/page1.html");
            merchant.setName("Merchant" + i);

            ml.add(merchant);
        }

        for (int i = 1; i <= issuerCount; i++) {
            Issuer issuer = new Issuer();
            issuer.setId(i);
            issuer.setBIN("6" + Misc.padCutStringLeft("" + i, '0', 5));
            issuer.setName("Bank " + i);
            issuer.setPhone("+3721231234" + i);
            issuer.setEmail("user" + i + "@domain.com");
            issuer.setAddress("Street " + i);
            issuer.setCity("IssuerCity" + i);
            issuer.setCountry(i % 2 == 0 ? "EE" : "RU");
            issuer.setStatus(DSModel.Issuer.Status.ACTIVE);
            issuer.setAttemptsACSProId(i);
            il.add(issuer);
        }

        Map<Integer, Integer> icnt = new HashMap<Integer, Integer>();
        for (int i = 1; i <= acsProfileCount; i++) {
            ACSProfile profile = new ACSProfile();
            profile.setId(i);

            int iid = ThreadLocalRandom.current().nextInt(1, issuerCount);

            int cnt = icnt.get(iid) == null ? 0 : icnt.get(iid);
            cnt++;
            icnt.put(iid, cnt);

            if (cnt > 5) {
                iid = ThreadLocalRandom.current().nextInt(1, issuerCount);
            }

            profile.setIssuerId(iid);

            profile.setStatus(i % 87 == 0 ? DSModel.ACSProfile.Status.DISABLED : DSModel.ACSProfile.Status.ACTIVE);
            profile.setSSLProto("TLSv1.2");
            profile.setThreeDSMethodURL("https://bank" + i + ".com/acsprehandle");
            profile.setInClientCert((long) i);
            profile.setName("ACS Profile" + i);
            profile.setRefNo("99" + i);
            profile.setSet((short) 1);
            profile.setURL("https://bank" + i + ".com/acs");
            acpl.add(profile);
        }

        Map<Integer, Integer> binMap = new TreeMap<Integer, Integer>();
        for (long i = 1; i <= cardRangeCount; i++) {
            CardRange cr = new CardRange();
            cr.setId(i);
            cr.setIssuerId(ThreadLocalRandom.current().nextInt(1, issuerCount));
            cr.setName("CR Name" + i);
            cr.setType((short) (i % 6 + 1));
            cr.setSet((short) 1);
            cr.setRangeMode();

            int bin = ThreadLocalRandom.current().nextInt(100000, 999999);

            if (binMap.get(bin) != null) {
                bin = ThreadLocalRandom.current().nextInt(100000, 999999);
            }
            if (binMap.get(bin) != null) {
                bin = ThreadLocalRandom.current().nextInt(100000, 999999);
            }
            binMap.put(bin, bin);

            cr.setStart(Misc.padCutStringRight("" + cr.getType() + "" + bin, '0', 16));
            cr.setEnd(Misc.padCutStringRight("" + cr.getType() + "" + bin, '9', 16));
            cr.setStatus(DSModel.CardRange.Status.PARTICIPATING);
            crl.add(cr);
        }

        return result;
    }

    private class ImportPersistenceService {
        private final PersistenceService persistenceService;
        private SessionFactory sessionFactory;
        private transient Configuration cfg = null;
        private transient  Logger log = LoggerFactory.getLogger(ImportPersistenceService.class);
        private final String[] mappings = {"AuditLog.hbm.xml", "CardRange.hbm.xml", "CertificateData.hbm.xml",
                "Id.hbm.xml", "Issuer.hbm.xml", "Acquirer.hbm.xml", "KeyData.hbm.xml", "Setting.hbm.xml",
                "TDSRecord.hbm.xml", "TDSMessageData.hbm.xml", "Text.hbm.xml", "User.hbm.xml", "TDSServerProfile.hbm.xml",
                "ACSProfile.hbm.xml", "Merchant.hbm.xml", "HsmDevice.hbm.xml", "HsmDeviceConf.hbm.xml", "PaymentSystem.hbm.xml"};

        private ImportPersistenceService() {
            persistenceService = ServiceLocator.getInstance().getPersistenceService();
        }

        private Setting getSettingById(String key) {
            return persistenceService.getSettingById(key);
        }

        private void saveOrUpdate(Setting sise) throws Exception {
            persistenceService.saveOrUpdate(sise);
        }

        private String getStringSetting(String key) throws Exception {
            return persistenceService.getStringSetting(key);
        }

        private <T extends Persistable> List<T> getPersitableList(Class<T> clazz, Integer idStart, Integer limit) {
            return persistenceService.getPersitableList(clazz, idStart, limit);
        }

        private SessionFactory getSessionFactory() {
            if (sessionFactory == null) {
                sessionFactory = buildSessionFactory();
            }
            return sessionFactory;
        }

        private SessionFactory buildSessionFactory() {
            try {
                // Create the SessionFactory from hibernate.cfg.xml
                configureFromProperties(null);
                return cfg.buildSessionFactory();
            } catch (Throwable ex) {
                // Make sure you log the exception, as it might be swallowed
                log.error("Initial SessionFactory creation failed.", ex);
                throw new ExceptionInInitializerError(ex);
            }
        }

        private void configureFromProperties(Properties properties) {
            try {
                if (cfg == null) {
                    Configuration cfgtemp = new Configuration();
                    for (String res : mappings) {
                        cfgtemp.addResource(res);
                    }

                    if (properties != null) {
                        cfgtemp.addProperties(properties);
                    }

                    cfgtemp.configure();
                    cfg = cfgtemp;
                } else if (properties != null) {
                    cfg.addProperties(properties);
                    cfg.configure();
                }
            } catch (Throwable ex) {
                log.error("Exception during configuring hibernate" + (properties != null ? " from properties" : ""), ex);
                throw new ExceptionInInitializerError(ex);
            }
        }
    }
}
