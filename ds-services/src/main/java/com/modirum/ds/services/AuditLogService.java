/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 09.04.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.db.model.AuditLog;
import com.modirum.ds.db.model.AuditLogSearcher;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Persistable;
import com.modirum.ds.db.model.User;
import com.modirum.ds.db.model.ui.PaymentSystemResource;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.ObjectDiff;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

public class AuditLogService {

    protected transient static Logger log = LoggerFactory.getLogger(AuditLogService.class);
    private PersistenceService persistenceService;
    int detailMaxLen = 512;

    public AuditLogService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public void logInsert(User u, Persistable o, String details) {
        AuditLog al = new AuditLog();
        al.setWhen(new java.util.Date());
        al.setById(u != null ? u.getId() : 0L);
        al.setBy(u != null ? u.getLoginname() : null);
        al.setAction(DSModel.AuditLog.Action.INSERT);
        al.setObjectId(getId(o));
        al.setDetails(details);

        if (o != null) {
            al.setObjectClass(o.getClass().getSimpleName());
            setAuditLogPaymentSystemId(o, al);
        }

        try {
            persistenceService.save(al);
        } catch (Exception e) {
            log.info(al.getWhen() + " " + al.getAction() + " " + al.getBy() + " " + al.getObjectClass() + " " +
                     al.getObjectId() + " " + al.getDetails());
        }
    }

    public void logUpdate(User u, Persistable o, Persistable on, String details) {
        AuditLog al = new AuditLog();
        al.setById(u != null ? u.getId() : 0L);
        al.setWhen(new java.util.Date());
        al.setBy(u != null ? u.getLoginname() : null);
        al.setAction(DSModel.AuditLog.Action.UPDATE);
        al.setObjectId(getId(o));
        if (o != null) {
            al.setObjectClass(o.getClass().getSimpleName());
            setAuditLogPaymentSystemId(o, al);
        }

        try {
            if (o != null && on != null) {
                String d = ObjectDiff.createSimpleDiffString(ObjectDiff.diff(o, on));
                d = (details != null ? details : "") + (d != null ? d : "");
                al.setDetails(d != null && d.length() > detailMaxLen ? d.substring(0, detailMaxLen - 2) + ".." : d);
            }

            persistenceService.save(al);

        } catch (Exception e) {
            log.error("AuditLog error ", e);
            log.info(al.getWhen() + " " + al.getAction() + " " + al.getBy() + " " + al.getObjectClass() + " " +
                     al.getObjectId() + " " + al.getDetails());
        }
    }

    public void logUpdate(User u, Persistable o, String detail) {
        AuditLog al = new AuditLog();
        al.setById(u != null ? u.getId() : 0L);
        al.setWhen(new java.util.Date());
        al.setBy(u != null ? u.getLoginname() : null);
        al.setAction(DSModel.AuditLog.Action.UPDATE);
        al.setObjectId(getId(o));

        if (o != null) {
            al.setObjectClass(o.getClass().getSimpleName());
            setAuditLogPaymentSystemId(o, al);
        }

        al.setDetails(Misc.trunc(detail, detailMaxLen));
        try {
            persistenceService.save(al);

        } catch (Exception e) {
            log.error("AuditLog error ", e);
            log.info(al.getWhen() + " " + al.getAction() + " " + al.getBy() + " " + al.getObjectClass() + " " +
                     al.getObjectId() + " " + al.getDetails());
        }
    }

    public void logDelete(User u, Persistable o) {
        logDelete(u, o, null);
    }

    public void logDelete(User u, Persistable o, String detail) {
        AuditLog al = new AuditLog();
        al.setById(u != null ? u.getId() : 0L);
        al.setWhen(new java.util.Date());
        al.setBy(u != null ? u.getLoginname() : null);
        al.setAction(DSModel.AuditLog.Action.DELETE);
        al.setObjectId(getId(o));
        al.setDetails(Misc.trunc(detail, detailMaxLen));
        if (al.getObjectId() == null) {
            al.setObjectId(Long.valueOf(0));
        }

        if (o != null) {
            al.setObjectClass(o.getClass().getSimpleName());
            setAuditLogPaymentSystemId(o, al);
        }

        try {
            persistenceService.save(al);

        } catch (Exception e) {
            log.error("AuditLog error ", e);
            log.info(al.getWhen() + " " + al.getAction() + " " + al.getBy() + " " + al.getObjectClass() + " " +
                     al.getObjectId() + " " + al.getDetails());
        }
    }

    public Long getId(Persistable o) {
        Serializable v1 = o != null ? o.getId() : null;
        if (v1 != null && v1 instanceof Number) {
            return Long.valueOf(((Number) v1).longValue());
        } else {
            return Long.valueOf(0);
        }
    }

    public Long getId(Object o) {
        if (o != null) {
            Class<?> oclass = o.getClass();
            try {
                Method method = oclass.getMethod("getId", null);//.getDeclaredMethods();
                if (method != null) {
                    Object v1 = method.invoke(o, (Object[]) null);
                    if (v1 instanceof Number) {
                        return Long.valueOf(((Number) v1).longValue());
                    } else {
                        return Long.valueOf(0);
                    }
                }
            } catch (Exception e) {
                return Long.valueOf(0);
            }
        }
        return Long.valueOf(-1);
    }

    public void logAction(User u, Persistable o, String action, String detail) {
        AuditLog al = new AuditLog();
        al.setById(u == null || u.getId() == null ? 0L : u.getId());
        al.setWhen(new java.util.Date());
        al.setBy(u != null ? u.getLoginname() : null);
        al.setAction(action);
        if (o != null) {
            al.setObjectId(getId(o));
            al.setObjectClass(o.getClass().getSimpleName());
        }
        if (al.getObjectId() == null) {
            al.setObjectId(Long.valueOf(0));
            setAuditLogPaymentSystemId(o, al);
        }

        al.setDetails(Misc.trunc(detail, detailMaxLen));
        try {
            persistenceService.save(al);

        } catch (Exception e) {
            log.error("AuditLog error ", e);
        }
        log.info(al.getWhen() + " " + al.getAction() + " " + al.getBy() + " " + al.getObjectClass() + " " +
                 al.getObjectId() + " " + al.getDetails());
    }

    public AuditLog getLastModidified(Persistable o) throws Exception {
        AuditLogSearcher searcher = new AuditLogSearcher();
        if (o.getId() != null && o.getId() instanceof Number) {
            searcher.setObjectId(((Number) o.getId()).longValue());
            searcher.setObjectClass(o.getClass().getSimpleName());
            searcher.setActions(new String[]{DSModel.AuditLog.Action.INSERT, DSModel.AuditLog.Action.UPDATE});
            searcher.setLimit(2);
            searcher.setOrder("when");
            searcher.setOrderDirection("desc");
            List<AuditLog> al = getLogRecordsByQuery(searcher);
            if (al != null && al.size() > 0) {
                return al.get(0);
            }
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<AuditLog> getLogRecordsByQuery(AuditLogSearcher searcher) throws Exception {
        return persistenceService.getLogRecordsByQuery(searcher);
    }

    public AuditLog getLastLoginRecord(Long userId) throws Exception {
        AuditLogSearcher searcher = new AuditLogSearcher();
        searcher.setAction(DSModel.AuditLog.Action.LOGIN);
        searcher.setById(userId);
        searcher.setLimit(2);
        searcher.setOrder("when");
        searcher.setOrderDirection("desc");
        List<AuditLog> found = this.getLogRecordsByQuery(searcher);

        return found.size() > 0 ? found.get(0) : null;
    }

    private void setAuditLogPaymentSystemId(Persistable o, AuditLog al) {
        if (o instanceof PaymentSystemResource) {
            al.setPaymentSystemId(((PaymentSystemResource) o).getPaymentSystemId());
        }
    }
}
