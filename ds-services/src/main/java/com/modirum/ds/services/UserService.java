package com.modirum.ds.services;

import com.modirum.ds.db.model.User;
import com.modirum.ds.db.model.UserSearchFilter;
import com.modirum.ds.db.dao.PersistenceService;

import java.util.List;

public class UserService {

    private PersistenceService persistenceService;

    public UserService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public List<User> listUsers(UserSearchFilter filter) {
        return persistenceService.listUsers(filter);
    }

    public User getUserByLoginname(String loginname) {

        User found = null;
        if (loginname != null) {
            found = persistenceService.getUserByLoginName(loginname);
        }
        return found;
    }
}
