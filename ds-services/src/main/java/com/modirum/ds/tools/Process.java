/*
 * Copyright (C) 2018 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on Sep 20, 2018
 *
 */
package com.modirum.ds.tools;

import com.modirum.ds.services.CryptoService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class Process {

    protected static Logger log = LoggerFactory.getLogger(Process.class);

    public static void configHibernate(Properties settingProps, PersistenceService pse) {

        String password = settingProps.getProperty("db.password");
        Properties hibProps = new Properties(); //Can I reuse hiberanteProps?
        if (password != null && (password.startsWith("vep://") || password.startsWith("ep://"))) {
            try {
                HSMDevice se = (HSMDevice) Class.forName(
                        (String) settingProps.get("hsmDevice")).getDeclaredConstructor().newInstance();
                se.initializeHSM((String) settingProps.get("hsmConfig"));
                password = password.substring(password.indexOf("ep://") + 5);
                password = CryptoService.decryptConfigPassword(password, se);
            } catch (Exception e) {
                log.warn("Exception during password decryption: ", e);
                throw new RuntimeException("Failed to decrypt password ", e);
            }
        }

        if (Misc.isNotNullOrEmpty(password)) {
            hibProps.put("connection.password", password);
            hibProps.put("hibernate.connection.password", password);
        }

        String user = settingProps.getProperty("db.user");
        if (Misc.isNotNullOrEmpty(user)) {
            hibProps.put("connection.username", user);
            hibProps.put("hibernate.connection.username", user);
        }

        String url = settingProps.getProperty("db.url");
        if (Misc.isNotNullOrEmpty(url)) {
            hibProps.put("connection.url", url);
            hibProps.put("hibernate.connection.url", url);
        }
        String driver = settingProps.getProperty("db.driver");
        if (Misc.isNotNullOrEmpty(driver)) {
            hibProps.put("connection.driver_class", driver);
            hibProps.put("hibernate.connection.driver_class", driver);
        }

        //some hardcoded properties can be moved here
        hibProps.put("current_session_context_class", "thread");

        //here more properties are applied on hibernate config
        pse.configureFromProperties(hibProps);
    }
}
