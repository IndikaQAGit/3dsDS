package com.modirum.ds.messageprocessor;

import com.modirum.ds.model.Error;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.services.JsonMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * Abstraction for custom 3DS message processing per PaymentSystem.
 */
public interface PaymentSystemMessageProcessor {

    Logger logger = LoggerFactory.getLogger(PaymentSystemMessageProcessor.class);

    /**
     * This method allows custom AReq message validation.
     */
    default List<Error> validatePReq(JsonMessage pReqJsonMessage) {
        logger.info("Default PaymentSystemMessageProcessor.validatePReq is called.");
        return Collections.emptyList();
    }

    /**
     * This method allows custom AReq message validation.
     */
    default List<Error> validateAReqIncoming(JsonMessage aReqJsonMessage) {
        logger.info("Default PaymentSystemMessageProcessor.validateAReqIncoming is called.");
        return Collections.emptyList();
    }

    /**
     * This method allows custom ARes message validation.
     */
    default List<Error> validateAResIncoming(JsonMessage aResJsonMessage, JsonMessage aReqJsonMessage) {
        logger.info("Default PaymentSystemMessageProcessor.validateAResIncoming is called.");
        return Collections.emptyList();
    }

    /**
     * This method allows custom RReq message validation.
     */
    default List<Error> validateRReqIncoming(JsonMessage rReqJsonMessage, TDSRecord record) {
        logger.info("Default PaymentSystemMessageProcessor.validateRReqIncoming is called.");
        return Collections.emptyList();
    }

    /**
     * This method allows custom RRes message validation.
     */
    default List<Error> validateRResIncoming(JsonMessage rResJsonMessage, TDSRecord record) {
        logger.info("Default PaymentSystemMessageProcessor.validateRResIncoming is called.");
        return Collections.emptyList();
    }

    /**
     * Custom filtering logic of the outgoing AReq message represented by the JsonMessage.
     * This may include removing, adding, modifying of fields/objects.
     */
    default JsonMessage processAReqOutgoing(JsonMessage aReqJsonMessage) {
        logger.info("Default PaymentSystemMessageProcessor.processAReqOutgoing is called.");
        return aReqJsonMessage;
    }

    /**
     * Custom filtering logic of the outgoing ARes message represented by the JsonMessage.
     * This may include removing, adding, modifying of fields/objects.
     */
    default JsonMessage processAResOutgoing(JsonMessage aResJsonMessage, JsonMessage aReqJsonMessage) {
        logger.info("Default PaymentSystemMessageProcessor.processAResOutgoing is called.");
        return aResJsonMessage;
    }

    /**
     * Custom filtering logic of the outgoing RReq message represented by the JsonMessage.
     * This may include removing, adding, modifying of fields/objects.
     */
    default JsonMessage processRReqOutgoing(JsonMessage rReqJsonMessage) {
        logger.info("Default PaymentSystemMessageProcessor.rReqJsonMessage is called.");
        return rReqJsonMessage;
    }

    /**
     * Custom filtering logic of the outgoing RRes message represented by the JsonMessage.
     * This may include removing, adding, modifying of fields/objects.
     */
    default JsonMessage processRResOutgoing(JsonMessage rResJsonMessage) {
        logger.info("Default PaymentSystemMessageProcessor.rResJsonMessage is called.");
        return rResJsonMessage;
    }
}
