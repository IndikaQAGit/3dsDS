package com.modirum.ds.messageprocessor;

/**
 * Default implementation for PaymentSystemMessageProcessor that has no
 * any additional emv processing behavior. This is used if no specific
 * PaymentSystem processor is provided to prevent null checking in
 * many places.
 *
 * A single instance may be shared between multiple Payment Systems as a null-safe default processor.
 */
public class DefaultPaymentSystemMessageProcessor implements PaymentSystemMessageProcessor {
}
