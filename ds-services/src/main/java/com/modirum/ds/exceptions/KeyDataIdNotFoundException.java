package com.modirum.ds.exceptions;

public class KeyDataIdNotFoundException extends Exception {
    public KeyDataIdNotFoundException(String message) {
        super(message);
    }
}