package com.modirum.ds.enums;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * this documents all the transaction attributes
 */
public enum TransactionAttribute {

    /**
     * this is for transaction card range if it's range mode
     */
    CARD_RANGE("cardRange", StandardCharsets.US_ASCII),

    /**
     * this is for transaction bin if it's in bin mode
     */
    BIN("BIN", StandardCharsets.US_ASCII),
    
    /**
     * this is authentitication value to be save when STORE_AUTHENTICATION_VALUE is true
     */
    AUTHENTICATION_VALUE("authenticationValue", StandardCharsets.US_ASCII),

    /**
     * this is for TDS Server URL for multi tenancy.
     */
    TDS_SERVER_URL("threeDSServerURL", StandardCharsets.US_ASCII),

    INCOMING_PORT_NUMBER("incomingPortNumber", StandardCharsets.US_ASCII),

    /**
     * Human readable error description when transaction exception occurs
     */
    ERROR_DESCRIPTION("errorDescription", StandardCharsets.US_ASCII),
    /**
     * Human readable error resolution for transaction exception
     */
    ERROR_RESOLUTION("errorResolution", StandardCharsets.US_ASCII),

    /**
     * DS Version Hash that processed the transaction
     */
    DS_VERSION("DSVersion", StandardCharsets.US_ASCII);

    private String key;

    private Charset charSet;

    public String getKey() {
        return key;
    }

    public Charset getCharSet() {
        return charSet;
    }

    TransactionAttribute(String key, Charset charSet) {
        this.key = key;
        this.charSet = charSet;
    }
}
