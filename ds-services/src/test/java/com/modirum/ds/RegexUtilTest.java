package com.modirum.ds;

import com.modirum.ds.util.RegexUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;

public class RegexUtilTest {

    private static final String allowedCharacters = " ='!\"#$%&()*+,-./0123456789:;<>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

    @Test
    public void validPattern() {
        Assertions.assertTrue(RegexUtil.isValidPattern(".*"));
    }

    @Test
    public void invalidPattern() {
        Assertions.assertFalse(RegexUtil.isValidPattern("*{*{*{*"));
    }

    @Test
    public void emailMaskPattern() {
        String emailMask = "(?<=^.{1}).+(?=.{1}@)";
        Assertions.assertTrue(RegexUtil.isValidPattern(emailMask));
    }

    @Test
    public void validateAllowedCharacters() {
        RegexUtilTest.allowedCharacters.chars()
                                       .mapToObj(value -> (char) value)
                                       .collect(Collectors.toList())
                                       .forEach(invalidCharacter -> Assertions.assertTrue(RegexUtil.validAllowedCharacters(invalidCharacter.toString(), RegexUtilTest.allowedCharacters), "'" + invalidCharacter + "' is not allowed"));
    }

    @Test
    public void validateNotAllowedCharacters() {
        String invalidCharacters = "⁅⁆‹›⁽⁾«»©®℗™×’¬–—„‟”“°℃℉⁕§†‡′‵•·⁞⁛⁑″‶‣‥⁚⁖⁙¡‼⁔⁀¶¿€ä∑⨘α";

        invalidCharacters.chars()
                         .mapToObj(value -> (char) value)
                         .collect(Collectors.toList())
                         .forEach(invalidCharacter -> Assertions.assertFalse(RegexUtil.validAllowedCharacters(invalidCharacter.toString(), RegexUtilTest.allowedCharacters), "'" + invalidCharacter + "' is allowed"));

    }

}
