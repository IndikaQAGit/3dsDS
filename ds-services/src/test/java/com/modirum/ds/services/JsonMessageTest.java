package com.modirum.ds.services;

import com.modirum.ds.enums.FieldName;
import com.modirum.ds.model.threeds.MessageExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JsonMessageTest {
    private final JsonMessageService jsonMessageService = new JsonMessageService();

    @Test
    public void removeAuthenticationValueField_withValue() throws Exception {
        String json = "{\"authenticationValue\":\"av\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessage.setAuthenticationValue(null);
        Assertions.assertEquals("{}", jsonMessage.toString());
    }

    @Test
    public void removeAuthenticationValue_nonExistingField() throws Exception {
        String json = "{\"emailAddress\":\"mike.rustia@modirum.com\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessage.setAuthenticationValue(null);
        Assertions.assertEquals(json, jsonMessage.toString());
    }

    @Test
    public void removeEmptyField() throws Exception {
        String json = "{\"authenticationValue\":\"\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessage.setAuthenticationValue(null);
        Assertions.assertEquals("{}", jsonMessage.toString());
    }

    @Test
    public void removeNullField() throws Exception {
        String json = "{\"authenticationValue\":null}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessage.setAuthenticationValue(null);
        Assertions.assertEquals("{}", jsonMessage.toString());
    }

    @Test
    public void addMessageExtension() throws Exception {
        String json = "{}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        MessageExtension messageExtension = new MessageExtension()
                .setId("1")
                .setName("ext1")
                .addData("name", "John");
        List<MessageExtension> messageExtensions = Optional.ofNullable(jsonMessage.getMessageExtension())
                                                           .orElse(new ArrayList<>());
        messageExtensions.add(messageExtension);
        jsonMessage.setMessageExtension(messageExtensions);
        Assertions.assertEquals("{\"messageExtension\":[{\"name\":\"ext1\",\"id\":\"1\",\"criticalityIndicator\":false,\"data\":{\"name\":\"John\"}}]}", jsonMessage.toString());
    }

    @Test
    public void addMessageExtensions() throws Exception {
        String json = "{}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        MessageExtension ext1 = new MessageExtension()
                .setId("1")
                .setName("ext1")
                .addData("name", "John");
        MessageExtension ext2 = new MessageExtension()
                .setId("2")
                .setName("ext2")
                .setCriticalityIndicator(true)
                .addData("name", "Peter");
        List<MessageExtension> messageExtensions = Optional.ofNullable(jsonMessage.getMessageExtension())
                                                           .orElse(new ArrayList<>());
        messageExtensions.add(ext1);
        messageExtensions.add(ext2);
        jsonMessage.setMessageExtension(messageExtensions);
        Assertions.assertEquals("{\"messageExtension\":[{\"name\":\"ext1\",\"id\":\"1\",\"criticalityIndicator\":false,\"data\":{\"name\":\"John\"}},{\"name\":\"ext2\",\"id\":\"2\",\"criticalityIndicator\":true,\"data\":{\"name\":\"Peter\"}}]}", jsonMessage.toString());
    }

    @Test
    public void addMessageExtensionToExisting() throws Exception {
        String json = "{\"messageExtension\":[{\"name\":\"ext1\",\"id\":\"1\",\"criticalityIndicator\":false,\"data\":{\"name\":\"John\"}}]}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        MessageExtension ext2 = new MessageExtension()
                .setId("2")
                .setName("ext2")
                .setCriticalityIndicator(true)
                .addData("name", "Peter");
        List<MessageExtension> messageExtensions = jsonMessage.getMessageExtension();
        messageExtensions.add(ext2);
        jsonMessage.setMessageExtension(messageExtensions);
        Assertions.assertEquals("{\"messageExtension\":[{\"name\":\"ext1\",\"id\":\"1\",\"criticalityIndicator\":false,\"data\":{\"name\":\"John\"}},{\"name\":\"ext2\",\"id\":\"2\",\"criticalityIndicator\":true,\"data\":{\"name\":\"Peter\"}}]}", jsonMessage.toString());
    }

    @Test
    public void emptyObject() throws IOException {
        String json = "{\"merchantRiskIndicator\": {}}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessage.isEmptyOrNullField(FieldName.merchantRiskIndicator));
    }

    @Test
    public void emptyMessageExtension() throws IOException {
        String json = "{\"messageExtension\": []}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessage.isEmptyOrNullField(FieldName.messageExtension));
    }

    @Test
    public void nullMessageExtension() throws IOException {
        String json = "{\"messageExtension\": null}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessage.isEmptyOrNullField(FieldName.messageExtension));
    }

    @Test
    public void eciFieldExistButNull() throws Exception {
        String json = "{\"eci\":null}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertNull(jsonMessage.getEci());
    }

    @Test
    public void eciFieldExistButEmpty() throws Exception {
        String json = "{\"eci\":\"\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertEquals("", jsonMessage.getEci());
    }

    @Test
    public void eciFieldExistWithValue() throws Exception {
        String json = "{\"eci\":\"05\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertEquals("05", jsonMessage.getEci());
    }

    @Test
    public void eciFieldNotExisting() throws Exception {
        String json = "{}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertNull(jsonMessage.getEci());
    }

    @Test
    public void setEciValue_whenNotExisting() throws Exception {
        String json = "{}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessage.setEci("05");
        Assertions.assertEquals("05", jsonMessage.getEci());
        Assertions.assertEquals("{\"eci\":\"05\"}", jsonMessage.toString());
    }

    @Test
    public void setEciValue_whenExisting() throws Exception {
        String json = "{\"eci\":\"06\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessage.setEci("05");
        Assertions.assertEquals("05", jsonMessage.getEci());
        Assertions.assertEquals("{\"eci\":\"05\"}", jsonMessage.toString());
    }

    @Test
    public void setEciValue_whenEmpty() throws Exception {
        String json = "{\"eci\":\"\"}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessage.setEci("05");
        Assertions.assertEquals("05", jsonMessage.getEci());
        Assertions.assertEquals("{\"eci\":\"05\"}", jsonMessage.toString());
    }

    @Test
    public void setEciValue_whenNull() throws Exception {
        String json = "{\"eci\":null}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        jsonMessage.setEci("05");
        Assertions.assertEquals("05", jsonMessage.getEci());
        Assertions.assertEquals("{\"eci\":\"05\"}", jsonMessage.toString());
    }

    @Test
    public void nullField() throws Exception {
        String json = "{\"authenticationValue\":null}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessage.isEmptyOrNullField(FieldName.authenticationValue));
    }

    @Test
    public void multiTransaction_NotEmptyMerchantList() throws Exception {
        String json = "{\"multiTransaction\" : {\n" +
                "    \"merchantList\" : [\n" +
                "        {\n" +
                "            \"merchantNameListed\" : \"Merchant Name Listed\",\n" +
                "            \"acquirerMerchantIdListed\" : \"\",\n" +
                "            \"merchantAmount\" : null,\n" +
                "            \"merchantCurrency\" : \"036\",\n" +
                "            \"merchantExponent\" : \"2\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"avValidityTime\" : \"555\",\n" +
                "    \"avNumberUse\" : \"1\"\n" +
                "   } " +
                "}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessage.isObject("multiTransaction"));
        Assertions.assertFalse(jsonMessage.isEmptyOrNullField("multiTransaction.merchantList"));
        Assertions.assertTrue(jsonMessage.isArray("multiTransaction.merchantList"));
        Assertions.assertFalse(jsonMessage.isEmptyOrNullField("multiTransaction.merchantList.0.merchantNameListed"));
        Assertions.assertTrue(jsonMessage.isEmptyOrNullField("multiTransaction.merchantList.0.merchantAmount"));
        Assertions.assertTrue(jsonMessage.isEmptyOrNullField("multiTransaction.merchantList.0.acquirerMerchantIdListed"));
    }

    @Test
    public void multiTransaction_NullMerchantList() throws Exception {
        String json = "{ \"multiTransaction\" : {\n" +
                "    \"merchantList\" : null,\n" +
                "    \"avValidityTime\" : \"555\",\n" +
                "    \"avNumberUse\" : \"1\"\n" +
                "   }" +
                "}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessage.isEmptyOrNullField("multiTransaction.merchantList"));
    }

    @Test
    public void multiTransaction_EmptyMerchantList() throws Exception {
        String json = "{ \"multiTransaction\" : {\n" +
                "    \"merchantList\" : [],\n" +
                "    \"avValidityTime\" : \"555\",\n" +
                "    \"avNumberUse\" : \"1\"\n" +
                "   }" +
                "}";
        JsonMessage jsonMessage = jsonMessageService.parse(json);
        Assertions.assertTrue(jsonMessage.isEmptyOrNullField("multiTransaction.merchantList"));
    }

}