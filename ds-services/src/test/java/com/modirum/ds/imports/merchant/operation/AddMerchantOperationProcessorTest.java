package com.modirum.ds.imports.merchant.operation;

import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import com.modirum.ds.db.dao.PersistenceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AddMerchantOperationProcessorTest {

    private ImportMerchantModelProvider modelProvider = new ImportMerchantModelProvider();

    @Mock
    private PersistenceService persistenceService;

    @Test
    void testProcess() throws Exception {
        BatchMerchantModel model = modelProvider.getValidImportModel();
        AddMerchantOperationProcessor processor = new AddMerchantOperationProcessor(persistenceService);
        processor.process(Collections.singletonList(model));
        verify(persistenceService).save(any());
    }
}