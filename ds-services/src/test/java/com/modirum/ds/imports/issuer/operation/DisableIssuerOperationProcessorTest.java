package com.modirum.ds.imports.issuer.operation;

import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.services.IssuerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DisableIssuerOperationProcessorTest {

    private ImportIssuerModelProvider modelProvider = new ImportIssuerModelProvider();

    @Mock
    private IssuerService issuerService;

    @Test
    void testProcess() {
        BatchIssuerModel model = modelProvider.getValidModel();
        DisableIssuerOperationProcessor processor = new DisableIssuerOperationProcessor(issuerService);
        processor.process(Collections.singletonList(model));
        verify(issuerService).setIssuerDisabled(model.getName(), Integer.parseInt(model.getPaymentSystemId()));
    }
}