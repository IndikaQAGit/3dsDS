package com.modirum.ds.imports.merchant.operation;

import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;

public class ImportMerchantModelProvider {

    public BatchMerchantModel getInvalidImportModel() {
        return BatchMerchantModel.builder()
            .acquirerId("")
            .name("Name")
            .URL("url")
            .phone("phone")
            .email("email")
            .country("country")
            .requestorID("requestorID")
            .threeDSProfileId("")
            .options("Options")
            .registrationId("RegistrationId")
            .externalOptions("sdfasdf")
            .status("Active")
            .paymentSystemId("1")
            .build();
    }

    public BatchMerchantModel getValidImportModel() {
        return BatchMerchantModel.builder()
            .acquirerId("10000")
            .name("Name")
            .URL("http://wdfsdf.com:8090")
            .phone("2342351235")
            .email("test@mail.com")
            .country("AU")
            .requestorID("RequestorId")
            .threeDSProfileId("123")
            .options("Options")
            .registrationId("RegistrationId")
            .externalOptions("")
            .status("Active")
            .paymentSystemId("1")
            .build();
    }

    public BatchMerchantModel getChangeModel() {
        return BatchMerchantModel.builder()
            .name("Name")
            .paymentSystemId("1").build();
    }
}
