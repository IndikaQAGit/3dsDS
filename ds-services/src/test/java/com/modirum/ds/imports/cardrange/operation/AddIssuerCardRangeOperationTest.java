package com.modirum.ds.imports.cardrange.operation;

import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.db.dao.PersistenceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AddIssuerCardRangeOperationTest {

    private ImportCardRangeModelProvider modelProvider = new ImportCardRangeModelProvider();

    @Mock
    private IssuerService issuerService;

    @Mock
    private PersistenceService persistenceService;

    @Test
    void testProcess() throws Exception {
        BatchCardRangeModel model = modelProvider.getValidModel();
        Issuer issuer = new Issuer();
        issuer.setId(123);
        when(persistenceService.getIssuerByNameAndPaymentSystem(model.getIssuerName(), Integer.parseInt(model.getPaymentSystemId()))).thenReturn(issuer);
        AddIssuerCardRangeOperation processor = new AddIssuerCardRangeOperation(persistenceService, issuerService);
        processor.process(Collections.singletonList(model));
        verify(persistenceService).save(any());
    }
}




