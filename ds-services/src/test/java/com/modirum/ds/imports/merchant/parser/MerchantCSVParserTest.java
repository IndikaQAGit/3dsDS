package com.modirum.ds.imports.merchant.parser;

import com.modirum.ds.imports.CSVParserException;
import com.modirum.ds.imports.DataBatchBundle;
import com.modirum.ds.imports.DataChangeStateOperation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.modirum.ds.TestResourceFileReader.MERCHANTS_ADD;
import static com.modirum.ds.TestResourceFileReader.MERCHANTS_ADD_MISSING_ELEMENTS;
import static com.modirum.ds.TestResourceFileReader.MERCHANTS_CHANGE_STATE;
import static com.modirum.ds.TestResourceFileReader.readFileAsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class MerchantCSVParserTest {

    private MerchantCSVParser parser = new MerchantCSVParser();

    @Test
    public void testParseThrowsError() throws Exception {
        String fileContent = readFileAsString(MERCHANTS_ADD_MISSING_ELEMENTS);
        assertThrows(CSVParserException.class, () -> parser.parse(fileContent));
    }

    @Test
    public void testParseWithChangeStateHeader() throws Exception {
        String fileContent = readFileAsString(MERCHANTS_CHANGE_STATE);
        DataBatchBundle<BatchMerchantModel> model = parser.parse(fileContent);

        List<BatchMerchantModel> batchMerchantModels = model.getImportModels();
        assertEquals(model.getImportModels().size(), 3);

        assertEquals("merch1", batchMerchantModels.get(0).getName());
        assertEquals("merch2", batchMerchantModels.get(1).getName());
        assertEquals("merch3", batchMerchantModels.get(2).getName());

        assertEquals(DataChangeStateOperation.ACTIVATE, batchMerchantModels.get(0).getOperation());
        assertEquals(DataChangeStateOperation.DISABLE, batchMerchantModels.get(1).getOperation());
        assertEquals(DataChangeStateOperation.DELETE, batchMerchantModels.get(2).getOperation());

        assertEquals("1", batchMerchantModels.get(0).getPaymentSystemId());
        assertEquals("2", batchMerchantModels.get(1).getPaymentSystemId());
        assertEquals("3", batchMerchantModels.get(2).getPaymentSystemId());
    }

    @Test
    public void testParseWithAddStateHeader() throws Exception {
        String fileContent = readFileAsString(MERCHANTS_ADD);
        DataBatchBundle<BatchMerchantModel> model = parser.parse(fileContent);
        List<BatchMerchantModel> batchMerchantModels = model.getImportModels();
        assertEquals(batchMerchantModels.size(), 3);

        assertEquals("merch1", batchMerchantModels.get(0).getName());
        assertEquals("merch2", batchMerchantModels.get(1).getName());
        assertEquals("merch3", batchMerchantModels.get(2).getName());

        verifyMerchant(batchMerchantModels.get(0), "merch1", "MPI1", "18000", "18000", "active", "1234", "test@mail.com", "US", "registration1", "1");
        verifyMerchant(batchMerchantModels.get(1), "merch2", "MPI2", "17000", "17000", "disabled", "123412", "test@email.com", "US", "registration2", "2");
        verifyMerchant(batchMerchantModels.get(2), "merch3", "MPI3", "16000", "16000", "disabled", "12312", "test@com.email", "US", "registration3", "3");
    }

    private void verifyMerchant(BatchMerchantModel model, String name, String threeDSRequestorId, String acquirerBin, String threeDSProfileOperatorId,
                                String status, String phone, String email, String country, String registrationId, String paymentSystemId) {
        assertEquals(name, model.getName());
        assertEquals(threeDSRequestorId, model.getRequestorID());
        assertEquals(acquirerBin, model.getAcquirerBin());
        assertEquals(threeDSProfileOperatorId, model.getThreeDSOperatorId());
        assertEquals(status, model.getStatus());
        assertEquals(phone, model.getPhone());
        assertEquals(email, model.getEmail());
        assertEquals(country, model.getCountry());
        assertEquals(registrationId, model.getRegistrationId());
        assertEquals(paymentSystemId, model.getPaymentSystemId());
        assertNull(model.getAcquirerId());
        assertNull(model.getThreeDSProfileId());
    }
}