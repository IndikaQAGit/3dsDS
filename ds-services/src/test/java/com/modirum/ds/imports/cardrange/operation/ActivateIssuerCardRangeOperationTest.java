package com.modirum.ds.imports.cardrange.operation;

import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;
import com.modirum.ds.services.CardRangeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ActivateIssuerCardRangeOperationTest {

    private ImportCardRangeModelProvider modelProvider = new ImportCardRangeModelProvider();

    @Mock
    private CardRangeService cardRangeService;

    @Test
    void testProcess() {
        BatchCardRangeModel model = modelProvider.getValidModel();
        ActivateIssuerCardRangeOperation processor = new ActivateIssuerCardRangeOperation(cardRangeService);
        processor.process(Collections.singletonList(model));
        verify(cardRangeService).setCardRangeParticipating(model.getBinStart(), model.getBinEnd(), model.getPaymentSystemId());
    }
}