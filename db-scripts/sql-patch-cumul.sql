-- changes 2017/5 additional fields for 2.0.1
ALTER TABLE ds_merchants ADD COLUMN requestorID VARCHAR(40);
ALTER TABLE ds_miprofiles ADD COLUMN operatorID VARCHAR(40);
ALTER TABLE ds_miprofiles ADD CONSTRAINT ds_mi_operid UNIQUE (status, operatorID);

ALTER TABLE ds_tdsrecords MODIFY resultsStatus CHAR(2);

-- support for optional acs operator id 2017/8
ALTER TABLE ds_acsprofiles ADD COLUMN operatorID VARCHAR(40);
ALTER TABLE ds_tdsrecords ADD COLUMN ACSOperatorID VARCHAR(40);

-- DS 1.0.6 2.1 support
-- support for acs start and end protocol versions 2.1
ALTER TABLE ds_acsprofiles ADD COLUMN startProtocolVersion VARCHAR(12);
ALTER TABLE ds_acsprofiles ADD COLUMN endProtocolVersion VARCHAR(12);
ALTER TABLE ds_card_ranges ADD COLUMN lastMod DATETIME(0);
ALTER TABLE ds_acsprofiles ADD COLUMN lastMod DATETIME(0);
ALTER TABLE ds_tdsrecords ADD COLUMN protocol VARCHAR(32);
ALTER TABLE ds_tdsmessages MODIFY messageVersion VARCHAR(12);

-- DS 1.0.8 extensions
ALTER TABLE ds_merchants ADD COLUMN options VARCHAR(40);
ALTER TABLE ds_card_ranges ADD COLUMN options VARCHAR(40);

ALTER TABLE ds_tdsrecords ADD COLUMN issuerOptions VARCHAR(40);
ALTER TABLE ds_tdsrecords ADD COLUMN acquirerOptions VARCHAR(40);

-- changes 2019/12 additional fields for 2.1.0
ALTER TABLE ds_card_ranges ADD COLUMN acsInfoInd VARCHAR(20);
ALTER TABLE ds_card_ranges ADD COLUMN includeThreeDSMethodURL TINYINT(1) NULL;

-- #444 (DS key management redesign) Add separate table for HSM service definition
CREATE TABLE ds_hsmservice (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL,
  className VARCHAR(254) NOT NULL,
  PRIMARY KEY (id)
);

-- #444 (DS key management redesign) Add separate table to define DS per-instance HSM configurations
CREATE TABLE ds_hsmservice_conf (
  hsmservice_id BIGINT NOT NULL,
  dsId TINYINT DEFAULT 0,
  config VARCHAR(254) DEFAULT NULL,
  PRIMARY KEY (hsmservice_id, dsId)
);

-- #444 (DS key management redesign) To keydata add references to key-specific HSM service and optional wrapping key
ALTER TABLE ds_keydata ADD COLUMN hsmservice_id BIGINT NOT NULL;
ALTER TABLE ds_keydata ADD COLUMN wrapkey_id BIGINT DEFAULT NULL;

-- #444 (DS key management redesign) Disable mandatory use of dual-control via keysigners.
ALTER TABLE ds_keydata MODIFY cryptoPeriodDays INTEGER DEFAULT NULL;
ALTER TABLE ds_keydata MODIFY signedby1 VARCHAR(128) DEFAULT NULL;
ALTER TABLE ds_keydata MODIFY signedby2 VARCHAR(128) DEFAULT NULL;
ALTER TABLE ds_keydata MODIFY signature1 VARCHAR(1000) DEFAULT NULL;
ALTER TABLE ds_keydata MODIFY signature2 VARCHAR(1000) DEFAULT NULL;

-- #444 (DS key management redesign) Adds software "hsm" as explicit component of previous key-management processes
INSERT INTO ds_hsmservice(id, name, className) VALUES (1, 'Software engine', 'com.modirum.ds.hsm.SoftwareJCEService');
-- #444 (DS key management redesign) Adds previously used HSM engine as explicit component
INSERT INTO ds_hsmservice(id, name, className)
  (SELECT 2, 'Thales HSM', svalue FROM ds_settings
    WHERE skey='com.modirum.ds.services.HSMService' AND svalue != 'com.modirum.ds.hsm.SoftwareJCEService');

-- #444 (DS key management redesign) Transfers HSM configuration as default config to new HSM configuration holder
INSERT INTO ds_hsmservice_conf(hsmservice_id, dsId, config)
  (SELECT 2, 0, svalue FROM ds_settings
    WHERE skey LIKE 'com.modirum.ds.services.HSMService.config' AND (svalue IS NOT NULL AND svalue != '')
      AND (SELECT 1 FROM ds_settings WHERE skey='com.modirum.ds.services.HSMService' AND svalue != 'com.modirum.ds.hsm.SoftwareJCEService'));

-- #444 (DS key management redesign) Redefine DS per-instance hsm configurations
-- NB! Manual insertions should be performed, if the following statement returns result
SELECT SUBSTRING(skey, 43) AS dsId, svalue FROM ds_settings WHERE skey LIKE 'com.modirum.ds.services.HSMService.config.%';
-- INSERT INTO ds_hsmservice_conf(hsmservice_id, dsId, config) VALUES (2, dsId, svalue);

-- #444 (DS key management redesign) Recommended to (Backup) and delete unused data from settings table.
-- DELETE FROM ds_settings WHERE skey = 'com.modirum.ds.services.HSMService' OR skey LIKE 'com.modirum.ds.services.HSMService.config%';

-- #444 (DS key management redesign) Assign correct HSM Service IDs to keys based on their previous types
UPDATE ds_keydata SET hsmservice_id=2 WHERE keyAlias LIKE 'keysKey%';
UPDATE ds_keydata SET hsmservice_id=2 WHERE keyAlias LIKE 'masterkeyInfo%';
UPDATE ds_keydata SET hsmservice_id=2 WHERE keyAlias LIKE 'sdkRSAhsmKey%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'trustedRootCert%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'signerKeystore%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'hmacKey%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'hmacKeyExt%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'dataKey%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'schemeRootCert%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'schemeIntermedCert%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'clientAuhtCert%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'sharedSecret%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'sdkECKey%';
UPDATE ds_keydata SET hsmservice_id=1 WHERE keyAlias LIKE 'sdkRSAKey%';

-- #444 (DS key management redesign) Assign wrapper kes based on previous keytypes.
SET @temp_const_keysKeyId := (SELECT id AS keysKeyId FROM ds_keydata WHERE keyAlias='keysKey');
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'hmacKey%';
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'hmacKeyExt%';
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'dataKey%';
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'schemeRootCert%';
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'schemeIntermedCert%';
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'clientAuhtCert%';
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'sharedSecret%';
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'sdkECKey%';
UPDATE ds_keydata SET wrapkey_id=@temp_const_keysKeyId WHERE keyAlias LIKE 'sdkRSAKey%';

ALTER TABLE ds_miprofiles ADD COLUMN tdsReferenceNumberList VARCHAR(150) NULL DEFAULT NULL AFTER isid;

-- #594 added PaymentSystem for custom message processing
CREATE TABLE ds_paymentsystems (
  id INTEGER NOT NULL,
  name VARCHAR(100) NOT NULL,
  type VARCHAR(32) NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

ALTER TABLE ds_issuers ADD COLUMN paymentSystemId INTEGER NULL DEFAULT NULL;
ALTER TABLE ds_tdsrecords ADD COLUMN paymentSystemId INTEGER NULL DEFAULT NULL;
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.paymentSystem', 'en', 'Payment System');

-- #356 multiple acs ref numbers
ALTER TABLE ds_acsprofiles  CHANGE COLUMN refNo refNo VARCHAR(150) NULL DEFAULT NULL;
UPDATE ds_texts SET message = 'ACS Ref. Nos.' WHERE tkey = 'text.acsprofile.refno' and locale = 'en';

-- #646 added card-range duplicate error text
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.duplicate.range', 'en', 'Duplicate range');

-- #660 PaymentSystem type is tagged on 3DS Profile
ALTER TABLE ds_miprofiles ADD COLUMN paymentSystemId INTEGER NULL DEFAULT NULL;
UPDATE ds_texts SET tkey = 'text.select.paymentSystem' WHERE tkey = 'text.issuer.paymentSystem' and locale = 'en';

-- #685 utf8mb4 charset for fields that needs it
ALTER TABLE ds_tdsmessages MODIFY COLUMN contents longtext CHARSET utf8mb4;
ALTER TABLE ds_tdsrecords
  MODIFY COLUMN acquirerMerchantID VARCHAR(35) CHARSET utf8mb4,
  MODIFY COLUMN ACSOperatorID VARCHAR(40) CHARSET utf8mb4,
  MODIFY COLUMN ACSRef VARCHAR(32) CHARSET utf8mb4,
  MODIFY COLUMN browserUserAgent VARCHAR(2048) CHARSET utf8mb4,
  MODIFY COLUMN dsRef VARCHAR(32) CHARSET utf8mb4,
  MODIFY COLUMN requestorID VARCHAR(40) CHARSET utf8mb4,
  MODIFY COLUMN requestorName VARCHAR(40) CHARSET utf8mb4,
  MODIFY COLUMN merchantName VARCHAR(40) CHARSET utf8mb4,
  MODIFY COLUMN MIReferenceNumber VARCHAR(32) CHARSET utf8mb4,
  MODIFY COLUMN SDKReferenceNumber VARCHAR(32) CHARSET utf8mb4,
  MODIFY COLUMN requestorURL VARCHAR(2048) CHARSET utf8mb4,
  MODIFY COLUMN MIURL VARCHAR(2048) CHARSET utf8mb4,
  MODIFY COLUMN errorDetail VARCHAR(2048) CHARSET utf8mb4;
ALTER TABLE ds_paymentsystems MODIFY COLUMN name VARCHAR(100) CHARSET utf8mb4 NOT NULL;
ALTER TABLE ds_acquirers
  MODIFY COLUMN name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  MODIFY COLUMN address VARCHAR(255) CHARSET utf8mb4,
  MODIFY COLUMN city VARCHAR(128) CHARSET utf8mb4;
ALTER TABLE ds_acsprofiles
  MODIFY COLUMN name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  MODIFY COLUMN operatorID VARCHAR(40) CHARSET utf8mb4,
  MODIFY COLUMN refNo VARCHAR(150) CHARSET utf8mb4;
ALTER TABLE ds_card_ranges MODIFY COLUMN name VARCHAR(32) CHARSET utf8mb4;
ALTER TABLE ds_certdata MODIFY COLUMN name VARCHAR(50) CHARSET utf8mb4 NOT NULL;
ALTER TABLE ds_issuers
  MODIFY COLUMN name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  MODIFY COLUMN address VARCHAR(255) CHARSET utf8mb4,
  MODIFY COLUMN city VARCHAR(128) CHARSET utf8mb4;
ALTER TABLE ds_merchants
  MODIFY COLUMN identifier VARCHAR(35) CHARSET utf8mb4,
  MODIFY COLUMN name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  MODIFY COLUMN requestorID VARCHAR(40) CHARSET utf8mb4;
ALTER TABLE ds_miprofiles
  MODIFY COLUMN name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  MODIFY COLUMN requestorID VARCHAR(40) CHARSET utf8mb4,
  MODIFY COLUMN operatorID VARCHAR(40) CHARSET utf8mb4,
  MODIFY COLUMN tdsReferenceNumberList VARCHAR(150) CHARSET utf8mb4;
ALTER TABLE ds_texts MODIFY COLUMN message VARCHAR(10000) CHARSET utf8mb4;
ALTER TABLE ds_users
  MODIFY COLUMN loginname VARCHAR(32) CHARSET utf8mb4,
  MODIFY COLUMN firstName VARCHAR(64) CHARSET utf8mb4,
  MODIFY COLUMN lastName VARCHAR(64) CHARSET utf8mb4;
ALTER TABLE ds_auditlog MODIFY COLUMN details VARCHAR(512) CHARSET utf8mb4;

-- #697
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.D', 'en', 'D-Decoupled Authentication confirmed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.I', 'en', 'I-Challenge preference acknowledged');

-- #708
CREATE TABLE ds_tdsrecord_attributes (
  id VARCHAR(36) NOT NULL,
  tdsRecordId BIGINT NOT NULL,
  attr VARCHAR(64) NOT NULL,
  value VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  asciiValue VARCHAR(256) CHARACTER SET ascii DEFAULT NULL,
  createdDate DATETIME(6) NOT NULL,
  createdUser VARCHAR(64) NOT NULL,
  PRIMARY KEY(id)
);
CREATE INDEX ds_tdsr_attr_tdsrid_attr ON ds_tdsrecord_attributes (tdsRecordId, attr);
CREATE INDEX ds_tdsr_attr_created ON ds_tdsrecord_attributes (createdDate);
CREATE INDEX ds_tdsr_attr_asciiValue ON ds_tdsrecord_attributes (asciiValue);
CREATE INDEX ds_tdsr_attr_attr_value ON ds_tdsrecord_attributes (attr, value(36));

-- #670 Added Payment System id to a user entry, part of multi-tenancy implementation
ALTER TABLE ds_users ADD COLUMN paymentSystemId INTEGER NULL DEFAULT NULL;
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'general.unlimited', 'en', 'Unlimited');
UPDATE ds_texts SET tkey = 'general.paymentSystem' WHERE tkey = 'text.select.paymentSystem' and locale = 'en';

-- #754
ALTER TABLE ds_miprofiles MODIFY COLUMN tdsReferenceNumberList VARCHAR(255) CHARSET utf8mb4;

-- #805 Making acs-profile status mandatory to set in DS-Admin. Marking missing status as Disabled.
UPDATE ds_acsprofiles SET status='D' WHERE status='' OR status IS NULL;

-- #829 Adding multitenancy support to DS
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.paymentsystemView', 'en', 'View payment systems');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.paymentsystemEdit', 'en', 'Edit payment systems');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystems', 'en', 'Payment Systems');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.info', 'en', 'Payment System Information');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.name', 'en', 'Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.type', 'en', 'Type');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.components', 'en', '3DS Components');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'general.all', 'en', 'ALL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.port', 'en', 'Port');
UPDATE ds_texts SET tkey='text.merchant.acquirerMerchantID', message='Acquirer Merchant ID' WHERE tkey = 'text.merchant.identifier' and locale = 'en';

ALTER TABLE ds_paymentsystems ADD COLUMN port SMALLINT UNSIGNED NOT NULL;
-- If unique constraint fails - manually update ports for each PS entry, and re-run the constraint.
-- port=0 should be assigned to a "Default PS", if such has to exist. Payment System with port=0 is a fallback, if no entry is found for actual port number.
ALTER TABLE ds_paymentsystems ADD CONSTRAINT ds_ps_port UNIQUE (port);

-- Creates default Payment System entry. All unassigned 3DS Components will be assigned to this payment system.
-- Optionally, use any existing Payment System instead to set all the 3DS components to it
SET @temp_new_ds_psID = (SELECT COALESCE(MAX(id),0)+1 FROM ds_paymentsystems);
INSERT INTO ds_paymentsystems (id, name, type, port) VALUES (@temp_new_ds_psID, 'Default PS', null, 0);

UPDATE ds_issuers SET paymentSystemId=@temp_new_ds_psID WHERE paymentSystemId IS NULL;
UPDATE ds_miprofiles SET paymentSystemId=@temp_new_ds_psID WHERE paymentSystemId IS NULL;
ALTER TABLE ds_issuers MODIFY COLUMN paymentSystemId INTEGER NOT NULL;
ALTER TABLE ds_miprofiles MODIFY COLUMN paymentSystemId INTEGER NOT NULL;

ALTER TABLE ds_acsprofiles ADD COLUMN paymentSystemId INTEGER NOT NULL;
ALTER TABLE ds_acquirers ADD COLUMN paymentSystemId INTEGER NOT NULL;
ALTER TABLE ds_merchants ADD COLUMN paymentSystemId INTEGER NOT NULL;
ALTER TABLE ds_auditlog ADD COLUMN paymentSystemId INTEGER NULL DEFAULT NULL;
UPDATE ds_acsprofiles SET paymentSystemId=@temp_new_ds_psID;
UPDATE ds_acquirers SET paymentSystemId=@temp_new_ds_psID;
UPDATE ds_merchants SET paymentSystemId=@temp_new_ds_psID;

-- Sets all users to belong to a particular payment system to avoid having accidental superusers.
-- In production this may need to be done manually per user.
UPDATE ds_users SET paymentSystemId=@temp_new_ds_psID WHERE paymentSystemId IS NULL;

SET @temp_new_ds_psID = NULL;

-- #609 Remove typo in message
UPDATE ds_texts SET message = 'You must change password now as the system set password change interval has been passed.' where tkey = 'text.msg.focedpassword.change';

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'general.help', 'en', 'Help');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.userEdit.roles.edit', 'en', 'Add or remove user granted roles (View PAN role has effect only with 2nd factor authentication performed)');
UPDATE ds_texts SET message = 'View users' where tkey = 'role.userView';
UPDATE ds_texts SET message = 'View merchants' where tkey = 'role.merchantsView';
UPDATE ds_texts SET message = 'Edit users' where tkey = 'role.userEdit';
UPDATE ds_texts SET message = 'Edit merchants' where tkey = 'role.merchantsEdit';

-- #886 card range is unique only per payment system
ALTER TABLE ds_card_ranges DROP INDEX ds_cr_bin ;
ALTER TABLE ds_card_ranges ADD INDEX ds_cr_bin_index (bin ASC);
UPDATE ds_texts SET message = 'Overlapping with other Issuer card range' where tkey = 'err.other.issuer.overlaping.value';

-- #881 add info for create initial admin account feature
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.initial.admin.header', 'en', 'Initial Admin Account');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.initial.admin.login.info', 'en', 'Create initial admin account for login.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.initial.admin.missing', 'en', 'Missing required details for account.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.initial.admin.error', 'en', 'Error occurred when creating initial admin account.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.initial.admin.success', 'en', 'Initial admin account successfully created.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.password.mismatch', 'en', 'Passwords do not match.');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('enableInitAdmin', 'false', null, NOW(), 'admin (1000)');

-- #938 search/view/edit for AACS
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.aacs', 'en', 'Search Attempts ACS Profiles');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewAACS', 'en', 'Add new attempt acs');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.attempts.acs.browserHsUrl', 'en', 'HandShake URL');

-- #950 add/edit for payment systems
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.adminSetup', 'en', 'Administration Setup');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewPaymentSystem', 'en', 'Add new payment system');

-- #960 non-unique issuer bin
ALTER TABLE ds_issuers DROP INDEX ds_iss_bin;
CREATE INDEX ds_iss_bin ON ds_issuers (BIN);

-- #841 Save card range/card bin used for a transaction to recordattributes
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.attr.attributes', 'en', 'Attribute');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.attr.value', 'en', 'Value');

-- #941 rename MI to 3dsServer
RENAME TABLE ds_miprofiles TO ds_tdsserver_profiles;
ALTER TABLE ds_tdsserver_profiles DROP KEY ds_mi_operid;
ALTER TABLE ds_tdsserver_profiles ADD CONSTRAINT ds_tdsserver_operid UNIQUE (status, operatorID);

ALTER TABLE ds_tdsserver_profiles DROP KEY ds_mi_reqacqid;
ALTER TABLE ds_tdsserver_profiles ADD CONSTRAINT ds_tdsserver_reqacqid UNIQUE (acquirerId, requestorID);

ALTER TABLE ds_acquirers CHANGE MIProfileId tdsServerProfileId BIGINT NULL;
ALTER TABLE ds_merchants CHANGE MIProfileId tdsServerProfileId BIGINT NULL;

UPDATE ds_settings  SET skey = 'tdsserver.ssl.client.certId' WHERE skey = 'mi.ssl.client.certId';
UPDATE ds_settings  SET skey = 'tdsserver.ssl.client.proto' WHERE skey = 'mi.ssl.client.proto';
UPDATE ds_settings  SET skey = 'tdsserver.ssl.client.ciphers' WHERE skey = 'mi.ssl.client.ciphers';
UPDATE ds_settings  SET skey = 'tdsserver.ssl.client.trustany' WHERE skey = 'mi.ssl.client.trustany';
UPDATE ds_settings  SET skey = 'tdsserver.ssl.client.verifyhostname' WHERE skey = 'mi.ssl.client.verifyhostname';
UPDATE ds_settings  SET skey = 'tdsServerTimeout' WHERE skey = 'miTimeout';
UPDATE ds_settings  SET skey = 'acceptedTDSServerList' WHERE skey = 'acceptedMIList';
UPDATE ds_settings  SET skey = 'superTDSServerInClientCertId' WHERE skey = 'superMIInClientCertId';
UPDATE ds_settings  SET skey = 'superTDSServerOutClientCertId' WHERE skey = 'superMIOutClientCertId';

UPDATE ds_texts  SET tkey = 'text.default.tdsServerProfile' WHERE tkey = 'text.default.MIProfile';
UPDATE ds_texts  SET tkey = 'objectClass.tdsServerProfile' WHERE tkey = 'objectClass.MIProfile';
UPDATE ds_texts  SET tkey = 'text.addNewTDSServer' WHERE tkey = 'text.addNewMI';
UPDATE ds_texts  SET tkey = 'text.tdsserver.url' WHERE tkey = 'text.mi.url';
UPDATE ds_texts  SET tkey = 'text.acquirer.tdsservercount' WHERE tkey = 'text.acquirer.micount';
UPDATE ds_texts  SET tkey = 'text.cert.tdsservercount' WHERE tkey='text.cert.micount';
UPDATE ds_texts  SET tkey = 'tdsserverprofile.status.A' WHERE tkey='miprofile.status.A';
UPDATE ds_texts  SET tkey = 'text.tdsserverprofile.in.clientcertid' WHERE tkey='text.miprofile.in.clientcertid';
UPDATE ds_texts  SET tkey = 'text.tdsserverprofile.out.clientcertid' WHERE tkey='text.miprofile.out.clientcertid';
UPDATE ds_texts  SET tkey = 'text.tdsserverprofile.merchantcount' WHERE tkey='text.miprofile.merchantcount';
UPDATE ds_texts  SET tkey = 'text.tdsserverprofiles' WHERE tkey='text.miprofiles';
UPDATE ds_texts  SET tkey = 'text.tdsserverprofile' WHERE tkey='text.miprofile';
UPDATE ds_texts  SET tkey = 'tdsserverprofile.status.D' WHERE tkey='miprofile.status.D';
UPDATE ds_texts  SET tkey = 'tdsserverprofile.status.E' WHERE tkey='miprofile.status.E';
UPDATE ds_texts  SET tkey = 'text.tdsserverprofile.id' WHERE tkey='text.miprofile.id';
UPDATE ds_texts  SET tkey = 'text.tdsserverref' WHERE tkey='text.miref';
UPDATE ds_texts  SET tkey = 'text.tdsserverservers' WHERE tkey='text.miservers';
UPDATE ds_texts  SET tkey = 'text.search.tdsserverservers' WHERE tkey='text.search.miservers';
UPDATE ds_texts  SET tkey = 'text.tdsservertransid' WHERE tkey='text.mitransid';
UPDATE ds_texts  SET tkey = 'text.tdsserverurl' WHERE tkey='text.miurl';

UPDATE ds_ids  SET name = 'tdsServerProfileId' WHERE name ='miProfileId';

-- #950 add validation message for adminSetup role
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.adminsetup.access', 'en', 'User does not have access to Administrative Setup.');

-- #957 error messages for acquirer
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.acquirer.invalid', 'en', 'Invalid selected acquirer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.tdsServerProfile.invalid', 'en', 'Invalid selected 3DS Profile');

-- #841 Save card range/card bin used for a transaction to recordattributes
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.attr.recordattributes', 'en', 'Transaction Attributes');
UPDATE ds_texts SET message = 'Record Attributes' WHERE tkey = 'text.attr.recordattributes' and locale = 'en';

-- #1009 added created and last modified dates to issuer table.
ALTER TABLE ds_issuers ADD createdDate DATETIME(6) DEFAULT '2021-01-01' NOT NULL;
ALTER TABLE ds_issuers ADD lastModified DATETIME(6) DEFAULT '2021-01-01' NOT NULL;

-- #992 DS-Manager: Issuer's ACS URL field allows saving untrimmed values
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.url', 'en', 'Invalid URL format');

-- #1012 change acquirer bin index from unique to non-unique
ALTER TABLE ds_acquirers DROP INDEX ds_acq_bin;
CREATE INDEX ds_acq_bin ON ds_acquirers (BIN);

-- #950 add error message for adminsetup role with missing payment system roles
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.adminsetup.no.paymentsystem.roles', 'en', 'Administration Setup role requires both View and Edit Payment System roles.');

-- #997 update browser handshake to Three DS Method URL
UPDATE ds_texts SET tkey = 'text.attempts.acs.3dsmethodurl', message = 'Three DS Method URL' WHERE tkey = 'text.attempts.acs.browserHsUrl' AND locale = 'en';
ALTER TABLE ds_acsprofiles CHANGE COLUMN browserHandshakeURL threeDSMethodURL VARCHAR(1024) NULL DEFAULT NULL ;

-- #1068 rename add new attempts ACS button
UPDATE ds_texts SET message = 'Add new Attempts ACS' where tkey = 'text.addNewAACS' and locale = 'en';

-- #997  Three DS Method URL to 3DS Method URL
UPDATE ds_texts SET message = '3DS Method URL' WHERE tkey = 'text.attempts.acs.3dsmethodurl' AND locale = 'en';

-- 1048 rename lastModified to lastModifiedDate
ALTER TABLE ds_issuers CHANGE lastModified lastModifiedDate DATETIME(6) DEFAULT '2021-01-01' NOT NULL;

-- 1115 fix grammar errors in user edit page error message
UPDATE ds_texts SET message = 'A user with this name already exists. Please choose a different name.' WHERE tkey = 'err.user.with.this.loginname.exists' AND locale = 'en';

-- #881 new table for DS initial settings and remove obsolete DS setting
CREATE TABLE ds_init (
  initKey VARCHAR(128) NOT NULL,
  initValue VARCHAR(128) NOT NULL,
  lastModifiedBy VARCHAR(64) NOT NULL DEFAULT 'admin (10000)',
  lastModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (initKey)
);
INSERT INTO ds_init (initKey, initValue) VALUES ('enableInitAdmin', 'false');
DELETE FROM ds_settings WHERE skey = 'enableInitAdmin';

-- #1148 DS Manager: 3DS Operator ID detects "duplicate" value in a different payment system
ALTER TABLE ds_tdsserver_profiles DROP INDEX ds_tdsserver_operid;
CREATE INDEX ds_tdsserver_operid ON ds_tdsserver_profiles (operatorID);

-- #1154 add entry for view/edit attempts ACS profile page header
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.attempts.acs.profile', 'en', 'Attempts ACS profile');

-- #1155 DS Manager: Grammatical error in unauthorized access page
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.privilege.noaccess', 'en', 'Access denied. Your privileges do not allow you to access this page.');

-- #1191 remove obsolete error message for admin setup privilege
DELETE FROM ds_texts WHERE tkey = 'err.adminsetup.no.paymentsystem.roles' AND locale = 'en';

-- #567
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.errordetail', 'en', 'Error Details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.settings', 'en', 'Search settings');

-- #1211 Add payment system configuration database table 
CREATE TABLE ds_paymentsystem_conf (
  paymentSystemId INT(11) NOT NULL,
  dsId TINYINT(4) NOT NULL DEFAULT 0,
  `key` VARCHAR(64) NOT NULL,
  `value` VARCHAR(256) NULL,
  comment VARCHAR(256) NULL,
  createdDate DATETIME(6) NOT NULL,
  createdUser VARCHAR(64) NOT NULL,
  lastModifiedDate DATETIME(6) NOT NULL,
  lastModifiedBy VARCHAR(64) NOT NULL,
  PRIMARY KEY (`paymentSystemId`, `key`, `dsId`));

-- #1215 fix typo in password validation message
UPDATE ds_texts SET message = 'New password must be at least {0}% different from old' WHERE tkey = 'mn.err.password.mindiff' AND locale = 'en';

-- #1218 add 'adminSetup' privilege for all users who have 'settingEdit' privilege AND have access to all payment systems
INSERT INTO ds_userroles (user_id, role)
SELECT e.user_id, 'adminSetup'
FROM ds_userroles e
WHERE e.role = 'settingEdit'
AND NOT EXISTS (SELECT 1 FROM ds_userroles a WHERE a.user_id = e.user_id AND a.role = 'adminSetup')
AND EXISTS (SELECT 1 FROM ds_users u WHERE u.id = e.user_id AND u.paymentSystemId is null);

-- #1218 remove code usage of settingEdit and settingView
DELETE FROM ds_texts WHERE tkey in ('role.settingEdit', 'role.settingView') AND locale = 'en';

-- #1232 remove obsolete validation message
DELETE FROM ds_texts WHERE tkey = 'err.adminsetup.access' AND locale = 'en';

-- #769 error description and resolution for invalidClientCertId
UPDATE ds_texts SET tkey='text.ar.errorDetail', message='Error Detail' WHERE tkey='text.ar.errorcode';
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.errorDescription', 'en', 'Error Description');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.errorResolution', 'en', 'Error Resolution');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.invalidClientCertId', 'en', 'The value of the DS Setting "{0}" is non-existing.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.invalidClientCertId', 'en', 'Please update the DS setting "{0}". The value should be the Id of the KeyInfo with alias="{1}"');

-- #1286 Move DS license information from ds_settings table to a dedicated ds_license table
CREATE TABLE ds_license (
  extension VARCHAR(32) DEFAULT NULL,
  licensee VARCHAR(32) DEFAULT NULL,
  expiration VARCHAR(32) NOT NULL,
  issuedDate DATETIME NOT NULL,
  licenseKey VARCHAR(20480) DEFAULT NULL,
  maxMerchants VARCHAR(32) DEFAULT NULL,
  name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
  version VARCHAR(32) DEFAULT NULL,
  id bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id));

INSERT INTO ds_license (extension, licensee, expiration, issuedDate, licensekey, maxMerchants, name, version, id)
VALUES
	((select svalue from ds_settings where skey = 'product.extensions' LIMIT 1),
	(select svalue from ds_settings where skey = 'product.licensee' LIMIT 1),
	(select svalue from ds_settings where skey = 'product.licenseexp' LIMIT 1),
	(select svalue from ds_settings where skey = 'product.licenseissued' LIMIT 1),
	(select svalue from ds_settings where skey = 'product.licensekey' LIMIT 1),
	(select svalue from ds_settings where skey = 'product.maxmerchants' LIMIT 1),
	(select svalue from ds_settings where skey = 'product.name' LIMIT 1),
	(select svalue from ds_settings where skey = 'product.version' LIMIT 1),
    default);

-- #1274 add status column for HSM Service
ALTER TABLE ds_hsmservice ADD COLUMN status CHAR(1) NOT NULL DEFAULT 'A';

-- #1274 add entries for Key Administration HSM Service/Device
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.keyManage', 'en', 'Key management');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.keyAdmin', 'en', 'Key Administration');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMServices', 'en', 'HSM Devices');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMService', 'en', 'HSM Device');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMService.name', 'en', 'Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMService.className', 'en', 'Class Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMServiceConf.config', 'en', 'Configuration');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addHSMService', 'en', 'Add new HSM device');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.hsmServiceEdit.config', 'en', 'A string of key-value pairs delimited by a semicolon (ex. host=127.0.0.1;timeout=30)');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.hsmserviceconf.config.invalid.format', 'en',
                                                         'Configuration has invalid format. Value should be a string of key-value pairs delimited by a semicolon.');

-- #1256 Reporting functionality
CREATE TABLE ds_reports (
  id BIGINT NOT NULL,
  paymentSystemId INT(11) NOT NULL,
  status VARCHAR(1) DEFAULT 'U' NOT NULL,
  secret VARCHAR(1024) DEFAULT NULL,
  filename VARCHAR(1024) DEFAULT NULL,
  name VARCHAR(254) DEFAULT 'Unknown' NOT NULL,
  dateFrom VARCHAR(23) DEFAULT '2003-09-09 00:00:00',
  dateTo VARCHAR(23) DEFAULT '2003-09-09 00:00:00',
  createdDate DATETIME(6) NOT NULL,
  createdUser VARCHAR(64) NOT NULL,
  lastModifiedDate DATETIME(6) NOT NULL,
  lastModifiedBy VARCHAR(64) NOT NULL,
  criteria VARCHAR(1024) DEFAULT NULL,
  PRIMARY KEY (id));

CREATE INDEX ds_mdar_rptstatus_modified ON ds_reports(status,lastModifiedDate);
CREATE INDEX ds_mdar_rptstatus_psid_modified ON ds_reports(status,paymentSystemId,lastModifiedDate);

INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('reports.download.url.prefix', '../../reports/', NULL, NOW(), 'admin (1000)');
INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy) VALUES ('reports.autorefresh.seconds', '4', NULL, NOW(), 'admin (1000)');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.reports', 'en', 'Reports');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.reports', 'en', 'Reports');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.type.transactions', 'en', 'Transactions');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.type.users', 'en', 'Users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.transaction', 'en', 'Transaction Report');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.useraudit', 'en', 'User Audit Report');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.list.show.reports', 'en', 'Show reports +');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.list.hide.reports', 'en', 'Hide reports -');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.locale', 'en', 'Locale');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.timezone', 'en', 'Timezone');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.run.report', 'en', 'Run Report');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.queued', 'en', 'Queued');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.processing', 'en', 'Processing');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.completed', 'en', 'Completed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.error', 'en', 'Error');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.deleted', 'en', 'Deleted');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.unknown', 'en', 'Unknown');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.name', 'en', 'Report Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.from', 'en', 'Report From Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.to', 'en', 'Report To Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.created', 'en', 'Report Created');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user', 'en', 'User');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.startdatetime', 'en', 'Missing/Invalid Start Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.enddate', 'en', 'Missing/Invalid End Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.startdate.after.enddate', 'en', 'Start Date cannot be after End Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.confirm.delete', 'en', 'Confirm delete report?');

-- #1309 ds_texts entries for license info issued field
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.li.issued', 'en', 'Issued');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.date.format', 'en', 'Invalid date format. Format must be {0}.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.licenseinfo.issued', 'en', 'Exact value from provided license data, Date yyyy-MM-dd');

-- #1222 Add Payment System Settings page
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings', 'en', 'Payment System Settings');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.comment', 'en', 'Comment');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.modified', 'en', 'Last Modified');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.DSURL', 'en', 'URL of the DS to which the ACS will send the RReq if a challenge occurs. The ACS is responsible for storing this value for later use in the transaction for sending the RReq to the DS.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.DSURL', 'en', 'AReq DS URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.usersetting', 'en', 'User Settings');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.adminsettings', 'en', 'Admin Settings');

ALTER TABLE ds_paymentsystem_conf CHANGE `key` skey VARCHAR(64) NOT NULL;

INSERT  ds_paymentsystem_conf (paymentSystemId, dsId, skey, `value`, comment, createdDate, createdUser, lastModifiedDate, lastModifiedBy)
SELECT p.id,0 as dsId,s.skey, s.svalue, s.comments, s.lastModified,s.lastModifiedBy, s.lastModified, s.lastModifiedBy  FROM ds_settings s, ds_paymentsystems p WHERE s.skey='DSURL';
-- DELETE FROM ds_settings WHERE skey='DSURL';

-- #1304 Support DS 2.3.0
ALTER TABLE ds_acsprofiles MODIFY threeDSMethodURL VARCHAR(2048);
ALTER TABLE ds_acsprofiles ADD COLUMN acsInfoInd VARCHAR(20);

INSERT INTO ds_texts (id, tkey, locale, message) VALUES	(default, 'text.ext.acsInfoInd.2_2_0', 'en', '2.2.0 ACS InfoInd');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES	(default, 'text.ext.acsInfoInd.2_3_0', 'en', '2.3.0 ACS InfoInd');

-- #966 move acceptedSDKList to ds_paymentsystem_conf
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.acceptedSDKList', 'en', 'List of SDK Reference Numbers that DS would accept as valid in AReq. Value format: CSV.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.acceptedSDKList', 'en', 'Accepted SDK List');

INSERT  ds_paymentsystem_conf (paymentSystemId, dsId, skey, `value`, comment, createdDate, createdUser, lastModifiedDate, lastModifiedBy)
SELECT p.id,0 as dsId,s.skey, s.svalue, s.comments, s.lastModified,s.lastModifiedBy, s.lastModified, s.lastModifiedBy  FROM ds_settings s, ds_paymentsystems p WHERE s.skey='acceptedSDKList';
-- DELETE FROM ds_settings WHERE skey='acceptedSDKList';

-- #1274 Rename all instances of HSM Service to HSM Device
ALTER TABLE ds_hsmservice RENAME ds_hsmdevice;
ALTER TABLE ds_keydata CHANGE hsmservice_id hsmdevice_id BIGINT NOT NULL;
ALTER TABLE ds_hsmservice_conf CHANGE hsmservice_id hsmdevice_id BIGINT NOT NULL;
UPDATE ds_texts SET tkey = 'text.HSMDevices' WHERE tkey = 'text.HSMServices' AND locale = 'en';
UPDATE ds_texts SET tkey = 'text.HSMDevice' WHERE tkey = 'text.HSMService' AND locale = 'en';
UPDATE ds_texts SET tkey = 'text.HSMDevice.name' WHERE tkey = 'text.HSMService.name' AND locale = 'en';
UPDATE ds_texts SET tkey = 'text.HSMDevice.className' WHERE tkey = 'text.HSMService.className' AND locale = 'en';
UPDATE ds_texts SET tkey = 'text.addHSMDevice' WHERE tkey = 'text.addHSMService' AND locale = 'en';
UPDATE ds_texts SET tkey = 'text.tooltip.hsmDeviceEdit.config' WHERE tkey = 'text.tooltip.hsmServiceEdit.config' AND locale = 'en';

-- #1274 Rename all instances of HSM Service Conf to HSM Device Conf
ALTER TABLE ds_hsmservice_conf RENAME ds_hsmdevice_conf;
UPDATE ds_texts SET tkey = 'text.HSMDeviceConf.config' WHERE tkey = 'text.HSMServiceConf.config' AND locale = 'en';
UPDATE ds_texts SET tkey = 'err.hsmdeviceconf.config.invalid.format' WHERE tkey = 'err.hsmserviceconf.config.invalid.format' AND locale = 'en';
UPDATE ds_settings SET skey = 'com.modirum.ds.services.HSMDevice' WHERE skey = 'com.modirum.ds.services.HSMService';
UPDATE ds_settings SET skey = 'com.modirum.ds.services.HSMDevice.config' WHERE skey = 'com.modirum.ds.services.HSMService.config';

-- #1256 Add reports role to users with adminSetup
INSERT INTO ds_userroles (user_id, role)
SELECT e.user_id, 'reports'
FROM ds_userroles e
WHERE e.role = 'adminSetup'
AND NOT EXISTS (SELECT 1 FROM ds_userroles a WHERE a.user_id = e.user_id AND a.role = 'reports')
AND EXISTS (SELECT 1 FROM ds_users u WHERE u.id = e.user_id AND u.paymentSystemId is null);

-- #1274 Add Key Management role to users with adminSetup and unlimited access to payment systems
INSERT INTO ds_userroles (user_id, role)
SELECT e.user_id, 'keyManage'
FROM ds_userroles e
WHERE e.role = 'adminSetup'
AND NOT EXISTS (SELECT 1 FROM ds_userroles a WHERE a.user_id = e.user_id AND a.role = 'keyManage')
AND EXISTS (SELECT 1 FROM ds_users u WHERE u.id = e.user_id AND u.paymentSystemId is null);

-- #1369 Enhance error messages
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.AReqConnectTimeout', 'en', 'The ACS URL "{0}" is unreachable.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.AReqConnectTimeout', 'en', 'Please check if the URL "{0}" is correct and reachable.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.AReqReadTimeout', 'en', 'The ACS URL "{0}" did not respond in time.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.AReqReadTimeout', 'en', 'Please check if the URL "{0}" is responsive. You can also increase the allowed time to respond by updating the value for setting "acsTimeout".');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.RReqConnectTimeout', 'en', 'The 3DSS URL "{0}" is unreachable.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.RReqConnectTimeout', 'en', 'Please check if the URL "{0}" is correct and reachable.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.RReqReadTimeout', 'en', 'The 3DSS URL "{0}" did not respond in time.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.RReqReadTimeout', 'en', 'Please check if the URL "{0}" is responsive. You can also increase the allowed time to respond by updating the value for setting "tdsServerTimeout".');

-- #882 Add tooltip to user roles
DELETE FROM ds_texts WHERE tkey='text.tooltip.userEdit.roles.edit';

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.adminSetup', 'en', 'Admin or superuser role.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.paymentsystemView', 'en', 'View payment systems.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.paymentsystemEdit', 'en', 'Edit payment system details.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.merchantsView', 'en', 'Vew merchants.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.merchantsEdit', 'en', 'Edit merchant details.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.userView', 'en', 'View other users.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.userEdit', 'en', 'Edit other user roles.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.issuerView', 'en', 'View issuers.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.issuerEdit', 'en', 'Edit issuers details.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.acquirerView', 'en', 'View acquirers.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.acquirerEdit', 'en', 'Edit acquirer details.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.recordsView', 'en', 'View Authentication records.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.panView', 'en', 'View Primary Account Numbers.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.textView', 'en', 'View texts.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.textEdit', 'en', 'Edit texts.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.caView', 'en', 'View CA.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.caEdit', 'en', 'Edit CA.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.auditLogView', 'en', 'View audit logs.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.keyManage', 'en', 'View and management of HSM devices.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.reports', 'en', 'View and generation of reports.');

-- #769 change the error message for invalid cert id
UPDATE ds_texts SET message='The value of the DS Setting "{0}" does not exist.' WHERE tkey='text.err.desc.invalidClientCertId' and locale = 'en';

-- #882  Remove period from description and update view pan description
UPDATE ds_texts SET message = REPLACE(message,  '.', '') WHERE tkey LIKE 'text.tooltip.user.roles%' AND locale = 'en';
UPDATE ds_texts SET message = 'View Primary Account Numbers (View PAN role has effect only with 2nd factor authentication performed)' WHERE tkey = 'text.tooltip.user.roles.panView' AND locale = 'en';

-- #769 rename clientAuhtCert to clientAuthCert
UPDATE ds_keydata SET keyAlias= REPLACE(keyAlias,  'clientAuhtCert', 'clientAuthCert') WHERE keyAlias LIKE 'clientAuhtCert%';

-- #1409 ds_texts for elo papi settings
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.elopapisetting', 'en', 'ELO PAPI Settings');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiEnabled', 'en', 'Enable Dekotenization Service');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiEnabled', 'en', 'Enables the ELO Detokenization in 3ds message processing');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiDetokenUrl', 'en', 'Detokenization Endpoint URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiDetokenUrl', 'en', ' ELO PAPI Detokenization Service Endpoint URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiDetokenClientId', 'en', 'Client ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiDetokenClientId', 'en', 'The clientId used in authorized access of ELO API');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiDetokenClientSecret', 'en', 'Client Secret Value');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiDetokenClientSecret', 'en', 'The clientSecret used as initial authorization until the successful authentication against ELO API');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiConnectTimeoutMillis', 'en', 'Connect Timeout in ms');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiConnectTimeoutMillis', 'en', 'ELO PAPI Detokenization Service HTTP connect timeout in milliseconds');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiReadTimeoutMillis', 'en', 'Read Timeout in ms');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiReadTimeoutMillis', 'en', 'ELO PAPI Detokenization Service HTTP read timeout in milliseconds');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiSkipTLSHostnameVerify', 'en', 'Toggle TLS Server Cert');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiSkipTLSHostnameVerify', 'en', 'FOR TESTING PURPOSES ONLY. Toggles ELO PAPI Detokenization Service TLS server cert CN/SAN check.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiAuthUrl', 'en', 'Authentication Endpoint URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiAuthUrl', 'en', 'ELO Authentication Service endpoint URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiUsername', 'en', 'Username');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiUsername', 'en', 'The username used in authenticating against ELO API');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiPassword', 'en', 'Password');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.eloPapiPassword', 'en', 'The password used in authenticating against ELO API');

-- #1404
SET @temp_const_eloPsId := (select id from ds_paymentsystems where type='elo');
-- Fill out the value for @elo_papi_graphql_endpoint
-- for ci/test environments elo-papi-emulator would be enough
-- but for staging external ELO test server is needed and of course for discover prod it requires ELO production server as well
SET @elo_papi_graphql_endpoint := 'http://3ds2-ds.ci.modirum.com:8180/elo-papi-emulator/graphql.jsp';
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiAuthUrl',@elo_papi_graphql_endpoint,NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiConnectTimeoutMillis','2000',NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiReadTimeoutMillis','4000',NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
-- Fill out the value for @elo_papi_areq_endpoint
-- for ci/test environments elo-papi-emulator would be enough
-- but for staging external ELO test server is needed and of course for discover prod it requires ELO production server as well
SET @elo_papi_areq_endpoint := 'http://3ds2-ds.ci.modirum.com:8180/elo-papi-emulator/areq-messages.jsp';
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiDetokenUrl',@elo_papi_areq_endpoint,NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiEnabled','true',NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
-- these scripts for clientId, clientSecret, username, password and eloPapiSkipTLSHostnameVerify here are only used for test environments that uses elo emulator
-- for staging and production actual credentials from ELO server should be used
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiDetokenClientId','dsClientId',NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiDetokenClientSecret','dsClientSecret',NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiUsername','dsUser',NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiPassword','password',NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');
INSERT INTO ds_paymentsystem_conf (paymentSystemId,dsId,skey,value,comment,createdDate,createdUser,lastModifiedDate,lastModifiedBy) VALUES (@temp_const_eloPsId,0,'eloPapiSkipTLSHostnameVerify','true',NULL,'2025-08-04 06:56:05.827000','admin','2025-08-04 06:56:05.827000','admin');

-- #1409 fix wrong spelling
UPDATE ds_texts SET message = 'Enable Detokenization Service' WHERE tkey = 'text.paymentsystemssettings.presentablename.eloPapiEnabled' and locale = 'en';

-- #1438 remove relationship between acquirer and tdsServer
ALTER TABLE ds_acquirers DROP COLUMN tdsServerProfileId;
ALTER TABLE ds_tdsserver_profiles
  DROP COLUMN acquirerId,
  DROP INDEX ds_tdsserver_reqacqid;

-- #1432 add new payment system config for Elo PAPI
INSERT INTO ds_texts (id, tkey, locale, message)
VALUES (default, 'text.paymentsystemssettings.presentablename.eloPapiTLSClientCertId', 'en', 'TLS Client Certificate ID');
INSERT INTO ds_texts (id, tkey, locale, message)
VALUES (default, 'text.paymentsystemssettings.help.eloPapiTLSClientCertId', 'en', 'Key data ID of the client certificate for TLS connections with ELO server');
SET @eloPsId := (SELECT id FROM ds_paymentsystems WHERE TYPE = 'elo');
-- Note that eloPapiTLSClientCertId setting value is initially null
-- When we have the client certificate based on Elo server's CA, this setting should be updated for all environments
INSERT INTO ds_paymentsystem_conf (paymentSystemId, dsId, skey, value, comment, createdDate, createdUser, lastModifiedDate, lastModifiedBy)
VALUES (@eloPsId, 0, 'eloPapiTLSClientCertId', NULL, NULL, '2025-08-04 06:56:05.827000', 'admin', '2025-08-04 06:56:05.827000','admin');
