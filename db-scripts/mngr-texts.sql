INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'general.all', 'en', 'ALL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'merchant.name', 'en', 'Merchant');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'order.total', 'en', 'Order total');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'purchase.title', 'en', 'Your order');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'user.password', 'en', 'Password');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.cancel', 'en', 'Cancel');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.authenticate', 'en', 'Authenticate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'choose.card', 'en', 'Select card for payment');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.complete.order', 'en', 'Complete order');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.password.required', 'en', 'Password required');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'add.new.address', 'en', 'Add new address');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'dear.user', 'en', 'Dear');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.authentication.failed.retry', 'en', 'Authentication failed, please remember passwords are case sensitive. Please try again.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'user.id', 'en', 'User id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'invalid.request', 'en', 'Invalid request (errId: {0})');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'address.edit', 'en', 'Edit');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'address.del', 'en', 'Del');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'addr.street', 'en', 'Street');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'addr.city', 'en', 'City');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'addr.zip', 'en', 'Postcode');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'addr.state', 'en', 'County');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'addr.country', 'en', 'Country');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'system.error', 'en', 'System failure errid={0}');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'user.email', 'en', 'Your email');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.continue', 'en', 'Continue');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.userView', 'en', 'View users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.merchantsView', 'en', 'View merchants');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.userEdit', 'en', 'Edit users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.merchantsEdit', 'en', 'Edit merchants');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.saved.ok', 'en', 'Text saved');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.deleted.ok', 'en', 'Text deleted');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.data.management', 'en', 'Data management');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.data.management', 'et', 'Haldus');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.authrecords', 'en', 'Authentication records');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.authrecords', 'et', 'Autentimise kirjed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.please.wait', 'en', 'Please wait..');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.localization', 'en', 'Localization');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.manage.issuers', 'en', 'Issuers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.manage.users', 'en', 'User management');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.system.setup', 'en', 'System setup');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.system.setup', 'et', 'Seadistused');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.localization', 'et', 'Keele tekstid');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.manage.issuers', 'et', 'Kaardi väljastajad');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.change.password', 'en', 'Change password');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.logout', 'en', 'Logout');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.logout', 'et', 'Logi välja');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.back', 'en', 'Back');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.back', 'et', 'Tagasi');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.mainpage', 'en', 'Main');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.mainpage', 'et', 'Pealeht');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.change.password', 'et', 'Muuda parool');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.texts', 'en', 'Search texts');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.texts', 'et', 'Otsi tekste');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNew', 'en', 'Add new');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNew', 'et', 'Lisa uus');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.save', 'en', 'Save');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.save', 'et', 'Salvesta');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.keyword', 'en', 'Keyword');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.language', 'en', 'Language');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.text.id(', 'en', 'Object id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.select.language', 'en', 'Select language for text');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.text.key.desc', 'en', 'Text match key');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.enter.text.value', 'en', 'Enter text value');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.text.value', 'en', 'Text value');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.text.key', 'en', 'Text key');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.cancel', 'et', 'Tühista');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.textedit', 'en', 'Text edit');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.search', 'en', 'Search');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.keyword', 'et', 'Märksõna');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.delete', 'en', 'Delete');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.page', 'en', 'Page');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.page', 'et', 'leht');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.saveAsNew', 'en', 'Save as new');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.saveAsNew', 'et', 'Salvesta uuena');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.nextpage', 'en', 'Next');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.lastpage', 'en', 'Last');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.firstpage', 'en', 'First');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.prevpage', 'en', 'Prev');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.textedit', 'et', 'Muuda tekst');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.text.id', 'en', 'Object id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.key', 'en', 'Text key');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.message', 'en', 'Text content');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.language', 'et', 'Keel');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.select.language', 'et', 'Vali teksti keel');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.found.total', 'en', 'Found total');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.texts', 'en', 'texts');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.showing', 'en', 'showing');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.to', 'en', 'to');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.msg.focedpassword.change', 'en', 'You must change password now as the system set password change interval has been passed.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.users', 'en', 'Users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.currentPassword', 'en', 'Current password');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.newPassword', 'en', 'New password');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.newPasswordRepeat', 'en', 'New password confirm');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.manage.users', 'et', 'Kasutajate haldus');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.active', 'en', 'Active');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystems', 'en', 'Payment Systems');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.info', 'en', 'Payment System Information');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.name', 'en', 'Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.type', 'en', 'Type');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.components', 'en', '3DS Components');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystem.port', 'en', 'Port');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.authrecordsView', 'en', 'View auth. records');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.userView', 'en', 'View users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.cardView', 'en', 'View cards');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.textView', 'en', 'View texts');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.cardEdit', 'en', 'Edit cards');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.userEdit', 'en', 'Edit users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.textEdit', 'en', 'Edit texts');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.master', 'en', 'All issuers (master)');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.issuerView', 'en', 'View issuers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.issuerEdit', 'en', 'Edit issuers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.paymentsystemView', 'en', 'View payment systems');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.paymentsystemEdit', 'en', 'Edit payment systems');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewUser', 'en', 'Add new user');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.users', 'en', 'Search users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.loginname', 'en', 'Login name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.status', 'en', 'User status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.user.with.this.loginname.exists', 'en', 'A user with this name already exists. Please choose a different name.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.email.invalid', 'en', 'Email is incorrect');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.password.security', 'en', 'Password must be at least 7 characters and include numbers and letters');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.password.security.num.let', 'en', 'Password must include both letters and numbers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.password.repat.not.matching', 'en', 'Password entries do not match');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.password.security.history', 'en', 'New password is one of the last used. Please choose another password.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.blocked', 'en', 'Blocked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.locked', 'en', 'Temp. locked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.template', 'en', 'Template');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer', 'en', 'Issuer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.orderby', 'en', 'Order by');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuers', 'en', 'Issuers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewIssuer', 'en', 'Add new issuer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewAACS', 'en', 'Add new Attempts ACS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.name', 'en', 'Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.status', 'en', 'Status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.issuers', 'en', 'Search issuers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.aacs', 'en', 'Search Attempts ACS Profiles');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.results.per.page', 'en', 'Result per page');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.visa', 'en', 'VISA');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.mastercard', 'en', 'MasterCard');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.length', 'en', 'Invalid length {0}');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.must.be.length', 'en', 'required length {0}');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.required', 'en', '{0} is required ');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardname', 'en', 'Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.duplicate.value', 'en', 'Duplicate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.amex', 'en', 'American Express');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.diners', 'en', 'Diners Club');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.jcb', 'en', 'JCB');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'am.com.modirum.authserver.services.AuthenticationServicePartPassword', 'en', 'Partial password');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'am.com.modirum.authserver.services.AuthenticationServicePassword', 'en', 'Password');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.other.issuer.overlaping.value', 'en', 'Overlapping with other Issuer card range');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardbrand', 'et', 'Card brand');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.id', 'en', 'Id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.datetime', 'en', 'Date time');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.completed', 'en', 'Completed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.merchantid', 'en', 'Merchant ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.merchantName', 'en', 'Merchant name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.issuer', 'en', 'Issuer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.total', 'en', 'Total');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.status', 'en', 'Status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.authstatus', 'en', 'Auth. Status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.ipaddress', 'en', 'IP Address');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.date', 'en', 'Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.NOCARDHOLDER', 'en', 'No Cardholder');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.SUCCESS', 'en', 'Success');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.INPROCESS', 'en', 'In Process');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.authrecords', 'en', 'Search authentication records');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.SUCCESS', 'en', 'Success');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.found.total', 'et', 'Kokku leitud');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.showing', 'et', 'näitan');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.to', 'et', 'kuni');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.INPROCESS-TIMEDOUT', 'en', 'In process time out');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.PREPROCESS-TIMEDOUT', 'en', 'Pre process time out');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.ERROR', 'en', 'Error');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.CANCELED', 'en', 'Canceled');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.NOSUPPORT', 'en', 'Not supported');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.ISSUERCANCELED', 'en', 'Issuer canceled');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.AUTHFAIL', 'en', 'Auth. failed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.NOCARD', 'en', 'No card');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.CARDHOLDERBLCK', 'en', 'Cardholder blocked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.ERROR', 'en', 'Error');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.FAILED', 'en', 'Failed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.ATTEMPT', 'en', 'Attempt');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.service', 'en', 'Service');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.service.SEAMLESS', 'en', 'Seamless');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.service.PAYPASS', 'en', 'MasterPass');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.service.ACS', 'en', 'ACS 3DS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.no.matches.found', 'en', 'No matches found');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.TIMEDOUT', 'en', 'Timed out');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.status.PREPROCESS', 'en', 'Pre process');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.min.length', 'en', 'Min {0} chars required');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.name', 'en', 'Card holder name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.authntication.failed.retry', 'en', 'Authentication failure, retry');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.message', 'en', 'Errors/Message');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.usedCard', 'en', 'Used card');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.selectedCard', 'en', 'Selected card');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.selectedAddress', 'en', 'Selected address');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.authrecord', 'en', 'Authentication record');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.results.per.page', 'et', 'Kirjeid lehel');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.orderby', 'et', 'Järjestus');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.today', 'en', 'Today');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.yesterday', 'en', 'Yesterday');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.thismonth', 'en', 'This month');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.prevmonth', 'en', 'Prev. month');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.thismonth', 'et', 'See kuu');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.prevmonth', 'et', 'Eelm. kuu');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.today', 'et', 'Täna');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.yesterday', 'et', 'Eile');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.authrecords', 'et', 'Otsi authentimise kirjeid');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.authrecords', 'et', 'Otsi autentimise kirjeid');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cho.createddate', 'en', 'Created date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cho.modifieddate', 'en', 'Modified date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.status', 'en', 'Card holder status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.brand', 'en', 'Card brand');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.bin', 'en', 'Has card BIN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.suffix', 'en', 'Last 4');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.id', 'en', 'Id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.modifiedDate', 'en', 'Modified');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cho.status.A', 'en', 'Active');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cho.status.B', 'en', 'Blocked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cho.status.T', 'en', 'Temp. locked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.cardholders', 'en', 'Search card holders');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.email', 'en', 'E-mail');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.object.not.found', 'en', 'Object is not found');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.modified', 'en', 'Modified');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.created', 'en', 'Created');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.firstName', 'en', 'First name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.lastName', 'en', 'Last name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardholder', 'en', 'Card holder');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.idCode', 'en', 'Id code');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.phone', 'en', 'Phone');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.authmethod', 'en', 'Authentication Method');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.cards', 'en', 'Cards');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.addresses', 'en', 'Addresses');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.bin.and.last4', 'en', 'BIN and last4');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.status', 'en', 'Status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.threeDstatus', 'en', '3-D Secure status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.expiration', 'en', 'Expiration');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'card.status.A', 'en', 'Active');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'card.status.B', 'en', 'Blocked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'card.3dstatus.Y', 'en', 'Enrolled (Y)');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'card.3dstatus.N', 'en', 'Not enrolled (N)');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'card.3dstatus.U', 'en', 'Unable auth. (U)');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.address.lines', 'en', 'Address');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.address.city', 'en', 'City');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.address.state', 'en', 'County');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.address.zip', 'en', 'Postcode');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.address.country', 'en', 'Country');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.address.type', 'en', 'Type');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'address.type.B', 'en', 'Billing');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'address.type.S', 'en', 'Shipping');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.searchbutton', 'en', 'Search');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.resetbutton', 'en', 'Reset');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.has.brand', 'en', 'Has card type');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.bins.list', 'en', 'BINs Operated by this issuer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.bin', 'en', 'BIN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardbrand', 'en', 'Card brand');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.delete', 'en', 'Delete');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.email', 'en', 'Email');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.phone', 'en', 'Phone');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.def.auth.method', 'en', 'Default auth. method');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.secret', 'en', 'Mac secret');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.modified', 'en', 'Last modified');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.bins', 'en', 'BINs');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.temp.locked', 'en', 'Temp. locked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.editCardHolder', 'en', 'Edit');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.editCard', 'en', 'Edit');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.addCard', 'en', 'Add new');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.editAddress', 'en', 'Edit');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.deleteAddress', 'en', 'Delete');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.addAddress', 'en', 'Add new');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.addNewBin', 'en', 'Add new');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cho.status.L', 'en', 'Temp. Locked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cho.status.D', 'en', 'Deleted');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.saveCardHolder', 'en', 'Save');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.order.asc', 'en', 'Ascending');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.order.desc', 'en', 'Descending');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'card.status.D', 'en', 'Deleted');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.saveCard', 'en', 'Save');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.saveAddress', 'en', 'Save');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.duplicate.pan', 'en', 'Duplicate pan');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.pan.luhn.check,mismatch', 'en', 'Luhn mismatch');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.bin.belongs.to.other.issuer', 'en', 'Other issuer range');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.bin.undefined', 'en', 'Undefined range');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.value', 'en', 'Invalid value, allowed {0}');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.only.one.billing.addr.allowed', 'en', 'One billing adress allowed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.object.mismatch', 'en', 'Mismatched object');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.authdata', 'en', 'Authentication Data');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.add.new.cardHolder', 'en', 'Add new cardholder/wallet');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.no.cards.yet ', 'en', 'No cards yet');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.no.addresses.yet', 'en', 'No addresses Yet');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.save.cardholder.first.to add.adresses', 'en', 'Save cardholder, to add addresses');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.save.cardholder.first.to add.cards', 'en', 'Save cardholder to add cards');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.add.new.cardHolder', 'et', 'Lisa uus kaardiomanik');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.firstName', 'en', 'First name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.lastName', 'en', 'Last name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.roles', 'en', 'Roles');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.email', 'en', 'Email');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.modified', 'en', 'Last modified');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.setNewPass', 'en', 'Set New password');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.newPasswordRepeat', 'en', 'Password confirm');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.saveAsNewTemplate', 'en', 'Save as new template');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.details', 'en', 'User details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.edit', 'en', 'Edit');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.expiring', 'en', 'Has card expiring');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'card.pan', 'en', 'Pan');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.status', 'et', 'Kasutaja staatus');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'page.title.paypass', 'en', 'Sign to MasterPass');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.mp.unknown.userid', 'en', 'Unrecognized user id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.mp.invalid.userid', 'en', 'Invalid user id, enter phone or email');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'mp.privacy.policy', 'en', 'Please read and accept privacy policy');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'not.me', 'en', 'Not me?');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'mp.remember.me', 'en', 'Remember me');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.pam', 'en', 'Personal assurance msg.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.policyaccepted', 'en', 'Policy accepted');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.policyaccepted.yes', 'en', 'Yes');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.factor2Authentication', 'en', 'Send factor auth');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.factor2Authmethod', 'en', '2nd factor auth method');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.factor2AuthDeviceId', 'en', '2nd factor device id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'sms.otp', 'en', 'Verification password {0} for session {1}');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'msg.otp', 'en', 'Please enter Your verification password for session {0}');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.sending.otp', 'en', 'Error sending session password, retry');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.did.not.received.otp', 'en', 'Click <Resend OTP> button, if You did not received one ');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'msg.securid', 'en', 'Enter Your secureID code');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'msg.pincalc', 'en', 'Enter pin from pin calculator');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.secondFactor.mismatch', 'en', 'Invalid value, authentication failed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.secondFactorPassword', 'en', 'Verification code');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.secondFactor.login', 'en', 'Second Level Authentication');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.smsotp.wait', 'en', 'Wait few seconds to receive Your OTP SMS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.newsmsotp', 'en', 'Resend OTP');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardHolder.countryResidence', 'en', 'Country of residence');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.walletname', 'en', 'MasterPass Wallet');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.auditLogView', 'en', 'View audit logs');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.auditlogs', 'en', 'Audit logs');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.al.date', 'en', 'Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.al.action', 'en', 'Action');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.ins', 'en', 'Add new');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.upd', 'en', 'Change');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.del', 'en', 'Delete');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.view', 'en', 'View');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.view.login', 'en', 'Login');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.loginfail', 'en', 'Login fail');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.lgoff', 'en', 'Log off');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.unauthorized', 'en', 'Unauthorized');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.view.search', 'en', 'Search');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.auditlogs', 'en', 'Sear for audit logs');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.al.objectClass', 'en', 'Object type');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.al.by', 'en', 'By user');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.al.objectId', 'en', 'Object id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.al.when', 'en', 'Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.al.details', 'en', 'Details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.al.issuer', 'en', 'Issuer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.search', 'en', 'Search');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.login', 'en', 'Login');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'al.action.logoff', 'en', 'Logoff');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.User', 'en', 'User');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.Issuer', 'en', 'Issuer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.CardHolder', 'en', 'Card holder');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.Acquirer', 'en', 'Acquirer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.Merchant', 'en', 'Merchant');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.tdsServerProfile', 'en', '3DSServer(MI)');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.Processor', 'en', 'Processor');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.Setting', 'en', 'Setting');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.CardInfo', 'en', 'Card');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'objectClass.Address', 'en', 'Address');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.enter.pan', 'en', 'Enter PAN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.card.expiring', 'en', 'Card expiring');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.email.notification.failed', 'en', 'Unable to send email notifications, error occurred');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.settings', 'en', 'Settings');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.setting.edit', 'en', 'Edit setting');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.licenseinfo', 'en', 'License info');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.licenseinfo.detail', 'en', 'License details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'locale.en', 'en', 'English');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'locale.en_GB', 'en', 'English - GB');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'locale.en_US', 'en', 'English - US');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'locale.en_IE', 'en', 'English - IE');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'locale.et', 'en', 'Estonian');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'locale.et_EE', 'en', 'Estonian - EE');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.manage.acquirers ', 'en', 'Acquirers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.loggedinas', 'en', 'Logged in as');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.keyinfo', 'en', 'Key info');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.acquirerEdit', 'en', 'Edit acquirers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.acquirerView', 'en', 'View acquirers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.recordsView', 'en', 'View auth records');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.locale', 'en', 'Preferred locale');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user.phone', 'en', 'Phone');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.merchant.name', 'en', 'Merchant name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.merchant.country', 'en', 'Merchant country');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuerbin', 'en', 'Issuer BIN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acquirer', 'en', 'Acquirer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acquirerbin', 'en', 'Acquirer BIN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.dstransid', 'en', 'DS Trans ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.sdktransid', 'en', 'SDK Trans ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsservertransid', 'en', '3DS Trans ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsaccountid', 'en', 'ACS Trans ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.errorcode', 'en', 'Error code');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.errordetail', 'en', 'Error Details');
-- INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ireqcode', 'en', 'Ireq code');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.merchant.acqid', 'en', 'Merchant number');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.acquirer', 'en', 'Acquirer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.eci', 'en', 'ECI');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.100', 'en', 'Error');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.20', 'en', 'In challenge');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.25', 'en', 'Challenge complete');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.50', 'en', 'Completed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.110', 'en', 'Error ACS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.112', 'en', 'Error 3DS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.120', 'en', 'Ireq ACS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.122', 'en', 'Ireq 3DS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.130', 'en', 'Error Comm ACS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.132', 'en', 'Error Com 3DS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.140', 'en', 'Error ACS late');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.localstatus.10', 'en', 'In process');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.maskedpan', 'en', 'Masked PAN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.key.id', 'en', 'Id');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.key.alias', 'en', 'Alias');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.key.checkvalue', 'en', 'Check value');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.key.date', 'en', 'Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.key.days', 'en', 'Days');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.key.signedby', 'en', 'Signers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsmessage.sourceIP', 'en', 'Source');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.messages', 'en', 'Messages');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.sdkappid', 'en', 'SDK App');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserverref', 'en', '3DS App ref');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsaccountuid', 'en', 'ACS Account ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsmessage.destIP', 'en', 'Dest');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.resultsstatus', 'en', 'Results status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.U', 'en', 'U-Could not perform');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.A', 'en', 'A-Attempts processing');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.N', 'en', 'N-Not authenticated');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.R', 'en', 'R-Rejected');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.C', 'en', 'C-Challenge');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.Y', 'en', 'Y-Authenticated');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.D', 'en', 'D-Decoupled Authentication confirmed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'ar.authstatus.I', 'en', 'I-Challenge preference acknowledged');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.merchantCountry', 'en', 'Merchant country');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsurl', 'en', 'ACS URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.authtype', 'en', 'Auth type');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.authvalue', 'en', 'Auth value');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserverurl', 'en', '3DS URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.ireccode', 'en', 'Irec code');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.edit', 'en', 'Edit');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cardtype', 'en', 'Card type');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.1', 'en', 'VISA');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.2', 'en', 'MasterCard');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.3', 'en', 'American Express');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.4', 'en', 'JCB');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.5', 'en', 'Discover');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.6', 'en', 'Diners');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.7', 'en', 'Bancontact Mister Cash');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardType.8', 'en', 'NSPK MIR');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.country', 'en', 'Country');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.address', 'en', 'Address');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.city', 'en', 'City');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.acsprofiles', 'en', 'ACS profiles');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsprofile.name', 'en', 'ACS Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsprofile.url', 'en', 'ACS URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsprofile.status', 'en', 'Status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardrange.status.P', 'en', 'Participating');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.bin.status', 'en', 'Status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'acsprofile.status.A', 'en', 'Active');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuer.bin', 'en', 'Issuer BIN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'issuer.status.A', 'en', 'Active');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsprofile.in.clientcertid', 'en', 'In Certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.addNewACSP', 'en', 'Add new ACS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'acsprofile.status.D', 'en', 'Disabled');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'acsprofile.status.T', 'en', 'Template');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'acsprofile.status.E', 'en', 'Ended');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'sertificatedata.status.V', 'en', 'Valid');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'sertificatedata.status.R', 'en', 'Revoked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsprofile.out.clientcertid', 'en', 'Out certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardrange.status.N', 'en', 'Not participating');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'cardrange.status.U', 'en', 'Unknown');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acquirers', 'en', 'Acquirers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.name', 'en', 'Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.email', 'en', 'Email');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.country', 'en', 'Country');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.status', 'en', 'Status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewAcquirer', 'en', 'Add new');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'acquirer.status.A', 'en', 'Active');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.acquirers', 'en', 'Search acquirers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acquirer.merchantcount', 'en', 'Merchants');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acquirer.tdsservercount', 'en', '3DSs configured');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.addNew', 'en', 'Add new');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.address', 'en', 'Address');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.city', 'en', 'City');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.phone', 'en', 'Phone');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acquirer.bin', 'en', 'Acquirer BIN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'acquirer.status.D', 'en', 'Disabled');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'acquirer.status.T', 'en', 'Template');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'acquirer.status.E', 'en', 'Ended');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.default.tdsServerProfile', 'en', 'Default 3DS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewTDSServer', 'en', 'Add new 3DS');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserverservers', 'en', '3DS servers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'tdsserverprofile.status.A', 'en', 'Active');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserver.url', 'en', '3DS URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserverprofile.in.clientcertid', 'en', 'In auth certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserverprofile.out.clientcertid', 'en', 'To auth certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserverprofile.merchantcount', 'en', 'Merchants');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserverprofiles', 'en', '3DS profiles');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.merchants', 'en', 'Merchants');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.URL', 'en', 'URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.administration', 'en', 'Administration');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ds.setup', 'en', 'DS Setup');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.caView', 'en', 'CA View');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.caEdit', 'en', 'CA Edit');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.certificates', 'en', 'Certificates');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tdsserverprofile', 'en', '3DS profile');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.tdsserverservers', 'en', 'Search 3DS servers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewMerchant', 'en', 'Add new merchant');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.merchant.acquirerMerchantID', 'en', 'Acquirer Merchant ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'merchant.status.A', 'en', 'Active');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.merchants', 'en', 'Search Merchants');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'merchant.status.D', 'en', 'Disabled');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'merchant.status.T', 'en', 'Template');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'merchant.status.E', 'en', 'Ended');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.merchant', 'en', 'Merchant');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'issuer.status.D', 'en', 'Disabled');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'issuer.status.T', 'en', 'Template');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'issuer.status.E', 'en', 'Ended');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.sign.csr', 'en', 'Sign a Certificate request');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.manage.certificates', 'en', 'Manage certificates');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.issuerDN', 'en', 'Issuer DN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.subjectDN', 'en', 'Subject DN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.subjAltNames', 'en', 'Subject alt names');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.expiresRange', 'en', 'Expires range');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'certificatedata.status.V', 'en', 'Valid');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'certificatedata.status.R', 'en', 'Revoked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.expires', 'en', 'Expires');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.validity', 'en', 'Validity');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.x509data', 'en', 'X509 Data');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cert.acscount', 'en', 'ACS count');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cert.tdsservercount', 'en', '3DS count');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'tdsserverprofile.status.D', 'en', 'Disabled');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'tdsserverprofile.status.E', 'en', 'Ended');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.revoke', 'en', 'Revoke');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.details', 'en', 'Details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'certificate.revoked', 'en', 'Certificate revoked');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.in.certificate', 'en', 'In SSL certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.certificates', 'en', 'Search certificates');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.menu.sign.csr', 'en', 'Sign a Certificate req.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.select.csr.file', 'en', 'Select CSR file');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.upload', 'en', 'Upload');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.validate', 'en', 'Validate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid ', 'en', 'Invalid data {0}');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.must.be.valid.pkcs10', 'en', 'must be valid PKCS10');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.sig.alg', 'en', 'Signature algorithm');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.sig.cert', 'en', 'Signing key/certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'sigalg.SHA256withRSA', 'en', 'SHA2-256 with RSA');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'sigalg.SHA384withRSA', 'en', 'SHA2-384 with RSA');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'sigalg.SHA512withRSA', 'en', 'SHA2-512 with RSA');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.text.sign.csr', 'en', 'Sign a certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.sign', 'en', 'Sign');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.sigalg', 'en', 'Signature algorithm');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.cakey', 'en', 'CA signing key');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.uploadfile', 'en', 'Upload file');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.text.cert.signed', 'en', 'Signed certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.text.cert.chain', 'en', 'Certificate chain');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.downloadCert', 'en', 'Download certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.downloadchain', 'en', 'Download chain');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.downloadchainp7', 'en', 'Download P7 chain');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.certificate', 'en', 'Certificate');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.unauthorized', 'en', 'Unauthorized');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.errorDetail', 'en', 'Error Detail');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.home', 'en', 'Home');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.key.status', 'en', 'Status');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.browserip', 'en', 'Browser IP');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.max.allowed', 'en', 'Max value {0}');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.show.decoded.devinfo', 'en', 'Show decoded deviceInfo');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.hide.decoded.devinfo', 'en', 'Hide decoded deviceInfo');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.viewpan', 'en', 'View PAN');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.close', 'en', 'Close');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.show.devinfo', 'en', 'Show deviceInfo');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.2ndfactor.required.for.devinfoview', 'en', 'Access denied 2nd factor required for deviceInfo View');



-- 1.0.1
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.by.requestorid', 'en', 'By requestorID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.requestor.id', 'en', 'Requestor ID');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.requestor.name', 'en', 'Requestor Name');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'text.msg.nocafunc.noschemeroot.privatekeys', 'en', 'CA functionality unavailable, due no scheme root private keys installed..'),
	(default, 'button.select.cert.file', 'en', 'Select certificate file'),
	(default, 'button.select.file.csv', 'en', 'Select CSV file'),
	(default, 'text.select.cert.file', 'en', 'Select certificate file'),


	(default, 'text.text.upload.cert', 'en', 'Upload certificate'),
	(default, 'text.import.csv', 'en', 'Import CSV file'),
	(default, 'text.text.cert.uploaded', 'en', 'Uploaded certificate'),
	(default, 'err.must.be.valid.x509', 'en', 'Invalid X509 data'),
	(default, 'err.must.contain.one.certificate', 'en', 'Must contain one certificate'),
	(default, 'err.signature.mismatch', 'en', 'Invalid certificate signature'),
	(default, 'err.signature.validation.error', 'en', 'Signature validation error'),
	(default, 'err.signature.validation.failed.noroot', 'en', 'Signature validation failed, no root found with subject "{0}"'),
	(default, 'text.menu.upload.cert', 'en', 'Upload certificate'),

	(default, 'certificate.upload.success', 'en', 'Certificate uploaded successfully');


INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'text.confirm.replace.cert', 'en', 'Confirm replace'),
	(default, 'text.wrn.certificate.sdn.exists', 'en', 'Warning: certificate with same SDN already exists'),
	(default, 'text.issuer.attemptsacs', 'en', 'Attempts ACS'),
	(default, 'text.acsprofile.refno', 'en', 'ACS Ref. Nos.'),
	(default, 'value.alphanum', 'en', 'alphanumeric'),
	(default, 'text.data.saved.ok', 'en', 'Data saved OK'),
	(default, 'text.3ds.requestorId', 'en', '3DS requestor Id'),
	(default, 'text.add.new', 'en', 'Add new'),
	(default, 'text.service.shields', 'en', 'Service shields'),
	(default, 'text.attempts.acs', 'en', 'Attempts ACS'),
	(default, 'text.attempts.acs.profiles', 'en', 'Attempts ACS profiles'),
	(default, 'text.attempts.acs.profile', 'en', 'Attempts ACS profile'),
	(default, 'text.attempts.acs.3dsmethodurl', 'en', '3DS Method URL');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.acsrefno', 'en', 'ACS Ref No');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'role.panView', 'en', 'View PAN'),
	(default, 'err.2ndfactor.required.for.panview', 'en', 'Access denied 2nd factor required for PAN View'),
	(default, 'err.min.allowed', 'en', 'Min value {0}'),
	(default, 'text.set', 'en', 'Set'),
	(default, 'button.addNewSet', 'en', 'Add new Set'),
	(default, 'text.binrange', 'en', 'BIN range');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'text.metrics', 'en', 'DS metrics'),
	(default, 'text.metrics.areq', 'en', 'AReq metrics'),
	(default, 'text.metrics.rreq', 'en', 'RReq metrics'),
	(default, 'text.metrics.preq', 'en', 'PReq metrics'),
	(default, 'text.searchbtn', 'en', 'Search'),
	(default, 'text.resetbtn', 'en', 'Reset'),
	(default, 'text.ar.deviceChannel', 'en', 'Device channel');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'ar.deviceChannel.00', 'en', 'Unknown'),
	(default, 'ar.deviceChannel.01', 'en', 'App-based'),
	(default, 'ar.deviceChannel.02', 'en', 'Browser'),
	(default, 'ar.deviceChannel.03', 'en', '3RI');

-- New texts 2017-02-22
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'text.ar.show.acscontent', 'en', 'Signed cont. detail'),
	(default, 'text.ar.could.not.retrieve.value', 'en', 'Could not retrieve value'),
	(default, 'err.registration.id.is.already.in.use', 'en', 'Registration ID already in use for this acquirer');

-- Missing order by texts for users 27/2/17
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'user.order.loginname', 'en', 'Login name'),
	(default, 'user.order.firstName', 'en', 'First name'),
	(default, 'user.order.lastName', 'en', 'Last name'),
	(default, 'user.order.id', 'en', 'Id'),
	(default, 'user.order.status', 'en', 'Status'),
	(default, 'user.order.email', 'en', 'Email'),
	(default, 'text.inClientCert', 'en', 'In auth certificate'),
	(default, 'factor2method.pincalc', 'en', 'Pin calculator'),
	(default, 'factor2method.smsotp', 'en', 'SMS OTP');

-- New texts 2017-03-08
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'factor2method.mdidotp', 'en', 'Modirum ID OTP'),
	(default, 'err.verify.secondfactor', 'en', 'Unexpected second factor verification error ref {0}, contact administrator'),
	(default, 'err.invalid.secondfactor.config', 'en', 'Invalid second factor configuration, contact administrator'),
	(default, 'text.user.factor2CertCN', 'en', 'Certificate CN'),
	(default, 'text.user.factor2MDIDUserId', 'en', 'Modirum ID user id'),
	(default, 'msg.mdidotpenter', 'en', 'Please open Modirum ID select registration {0} {1} and select One-Time password and enter value'),
	(default, 'msg.mdidapprorotpenter', 'en', 'Please open Modirum ID approve login or select registration {0} {1} and select One-Time password and enter value'),
	(default, 'msg.mdidotpactivate', 'en', 'Please open Modirum ID and select Create new registration and enter this {0} code'),
	(default, 'msg.mdidapprmobile', 'en', '{2} You are logging to {0} as {1}.
Do You approve?'),
	(default, 'err.mdidotp.activation', 'en', 'Unexpected error id {0} activating Modirum ID service, please contact administrator');


-- New texts for auto password generation 15/3/17
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'password.auto', 'en', 'Auto'),
	(default, 'password.reset', 'en', 'Reset');

-- fix of texts 2017-03-16
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'text.deleted', 'en', 'Deleted'),
	(default, 'text.disabled', 'en', 'Disabled'),
	(default, 'al.action.appStart', 'en', 'App Started'),
	(default, 'al.action.appStop', 'en', 'App Stoped');

-- texts 2017-04-07
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'text.key.x509info', 'en', 'X509 Info'),
	(default, 'text.cert.extUse', 'en', 'Extended use');


-- texts 2017-06-06
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.3ds.operatorId', 'en', 'Operator ID');

-- texts 2017-06-27
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'super.acquirer', 'en', 'Super Acquirer');

-- texts 2017-07-25
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
(default, 'text.instance.id', 'en', 'Instance id'),
(default, 'text.version', 'en', 'Version');

-- texts 2017-08-07
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
(default, 'text.acsprofile.operatorid', 'en', 'Operator ID'),
(default, 'text.acsopertorid', 'en', 'ACS Operator ID'),
(default, 'text.version', 'en', 'Version');


-- texts 2017-10-03
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
(default, 'mn.new.password.is.one.of.the.last.used.please.choose.another.password', 'en', 'New password is one of the last used. Please choose another password.'),
(default, 'mn.password.required.for.new.user', 'en', 'Password required for new user'),
(default, 'mn.password.entries.dont.match', 'en', 'Password entries do not match'),
(default, 'mn.new.password.is.one.of.the.last.used.please.choose.another.password', 'en', 'New password is one of the last four used. Please choose a new password.'),
(default, 'mn.current.password.is.required', 'en', 'Current password is required'),
(default, 'mn.current.password.entered.is.not.correct', 'en', 'Current password entered is not correct'),
(default, 'mn.password.may.not.be.blank', 'en', 'Password may not be blank'),
(default, 'mn.password.verify.may.not.be.blank', 'en', 'Password verify may not be blank'),
(default, 'mn.password.entries.dont.match', 'en', 'Password entries do not match'),

(default, 'mn.err.password.length', 'en', 'Password must be at least {0} characters'),
(default, 'mn.err.password.content', 'en', 'Password must include both letters and numbers'),
(default, 'mn.err.password.complexities', 'en', 'Password must contain at least {0} of the following categories: {1}'),
(default, 'mn.err.password.consecutive.sequence1', 'en', 'Maximum consecutive sequence'),
(default, 'mn.err.password.consecutive.lowercase.uppercase.and.numbers.is', 'en', ' for lowercase, uppercase and numbers is {0} characters'),
(default, 'mn.err.password.consecutive.lowercase.uppercase.is', 'en', ' for lowercase, uppercase  is {0} characters'),
(default, 'mn.err.password.consecutive.and.for.numbers.is', 'en', ' and for numbers is {0} characters'),
(default, 'mn.err.password.consecutive.for.lowercase.numbers.is', 'en', ' for lowercase, numbers is {0} characters'),
(default, 'mn.err.password.consecutive.and.for.uppercase.is', 'en', ' and for uppercase  is {0} characters'),

(default, 'mn.err.password.consecutive.and.for.uppercase.is', 'en', ' for uppercase, numbers  is {0} characters'),
(default, 'mn.err.password.consecutive.and.for.lowercase.is', 'en', ' and for lowercase  is {0} characters'),
(default, 'mn.err.password.consecutive.for.lowercase.is', 'en', ' for lowercase is {0}'),
(default, 'mn.err.password.consecutive.for.uppercase.is', 'en', ' and for uppercase  is {0} characters'),
(default, 'mn.err.password.consecutive.and.for.uppercase.is', 'en', ' and for uppercase  is {0} characters'),

(default, 'mn.err.password.consecutive.for.numbers.is', 'en', ' for numbers is {0}'),
(default, 'mn.err.password.characters', 'en', ' characters'),
(default, 'mn.err.password.and', 'en', ' and'),
(default, 'mn.err.password.consecutive.for.symbols.is', 'en', ' for symbols is {0} characters'),

(default, 'mn.err.password.mindiff', 'en', 'New password must be at least {0}% different from old'),

(default, 'mn.user.account.invalid.data', 'en', 'User account {0} invalid, contact administrator'),
(default, 'mn.user.account.temp.locked.due.bad.attempts', 'en', 'Too many bad login attempts for user {0} (temp locked)'),
(default, 'mn.user.account.disabled.due.long.inactivity', 'en', 'Account {0} disabled due long inactivity'),
(default, 'mn.user.account.invalid.template', 'en', 'Account {0} not a valid account (template)'),
(default, 'mn.user.account.invalid.terminated', 'en', 'Account {0} not valid, an ended/terminated account'),
(default, 'mn.user.account.blocked', 'en', 'Account {0} blocked/disabled'),
(default, 'mn.user.password.mismatch', 'en', 'Username/Password does not match for {0}'),
(default, 'mn.user.system.reserved.login.name.not.allowed.choose.other.name', 'en', 'System reserved login name not allowed, choose other name.'),
(default, 'err.leading.whitespaces', 'en', '{0} cannot begin with whitespaces');


-- texts 2017-11-29
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
(default, 'text.protoStart', 'en', 'Protocol start'),
(default, 'text.protoEnd', 'en', 'Protocol end'),
(default, 'text.lastmod', 'en', 'Last mod.'),
(default, 'text.ar.requestorid', 'en', 'Requestor ID'),
(default, 'text.ar.statusreason', 'en', 'Status reason'),
(default, 'text.requestor.name', 'en', 'Requestor name'),
(default, 'err.must.be.number', 'en', '{0} must be number'),
(default, 'text.ar.protocol', 'en', 'Protocol');

-- New texts 2018-04-09
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'err.requestor.id.is.already.in.use', 'en', 'Requestor ID already in use, must be unique'),
	(default, 'text.purchasecurrency', 'en', 'Purchase amount'),
	(default, 'text.purchaseamount.range', 'en', 'Purchase amount'),
	(default, 'err.failed.to.parse.data', 'en', 'Error, failed to parse object'),
	(default, 'err.failed.to.decode.data', 'en', 'Error, failed to decode data');


-- to clean up ducplciates if proves necessary
-- delete a from ds_texts a left JOIN (select min(id) as id, tkey from ds_texts where locale='en' GROUP BY tkey) b on a.tkey = b.tkey  and a.id = b.id where b.id is null;

-- New texts 2018-11-01
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'text.ext.options', 'en', 'Options (ext)');
	
-- 2018-12-11	
INSERT INTO ds_texts (id, tkey, locale, message)
VALUES (default, 'text.acstransid', 'en', 'ACS Trans ID'),
       (default, 'err.name.already.in.use.for.acquirer', 'en', 'Name already in use for this acquirer'),
       (default, 'text.wrn.are.you.sure.delete', 'en', 'Are you sure you want to delete this {0}?'),
       (default, 'text.merchant.id', 'en', 'Id'),
       (default, 'text.tdsserverprofile.id', 'en', 'Id');

-- 2019-07-31
INSERT INTO ds_texts (id, tkey, locale, message)
VALUES (default, 'text.issuer.binMatch', 'en', 'Ranges');

-- New texts 2019-11-05
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
	(default, 'text.ext.acsInfoInd', 'en', 'ACS InfoInd');


-- 2020-01-12
INSERT INTO ds_texts (id, tkey, locale, message)
VALUES (default, 'text.issuer.accountnumber', 'en', 'Account number');

-- 2020-05-18
INSERT INTO ds_texts (id, tkey, locale, message) VALUES
(default, 'text.exportbtn', 'en', 'Export (CSV file)');

-- 2020-06-03
INSERT INTO ds_texts (id, tkey, locale, message)
VALUES (default, 'mn.new.password.has.been.compromised', 'en', 'Entered password has been compromised by data breaches');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.settingsEnum', 'en', 'Settings');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.search.settings', 'en', 'Search settings');

-- 2020-08-10
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.csrPem', 'en', 'CSR');

-- texts 2020-09-08
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.3ds.referenceNumberList', 'en', '3DS Ref Numbers');

-- #594 added PaymentSystem for custom message processing
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'general.paymentSystem', 'en', 'Payment System');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'general.unlimited', 'en', 'Unlimited');

-- #646 added card-range duplicate error text
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.duplicate.range', 'en', 'Duplicate range');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'general.help', 'en', 'Help');

INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.adminSetup', 'en', 'Administration Setup');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addNewPaymentSystem', 'en', 'Add new payment system');

-- #841 Save card range/card bin used for a transaction to recordattributes
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.attr.attributes', 'en', 'Attribute');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.attr.value', 'en', 'Value');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.attr.recordattributes', 'en', 'Record Attributes');

-- #957 error messages for acquirer
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.acquirer.invalid', 'en', 'Invalid selected acquirer');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.tdsServerProfile.invalid', 'en', 'Invalid selected 3DS Profile');

-- #992 DS-Manager: Issuer's ACS URL field allows saving untrimmed values
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.url', 'en', 'Invalid URL format');

-- #1155 DS Manager: Grammatical error in unauthorized access page
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.privilege.noaccess', 'en', 'Access denied. Your privileges do not allow you to access this page.');

-- #769 error description and resolution for invalidClientCertId
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.errorDescription', 'en', 'Error Description');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.ar.errorResolution', 'en', 'Error Resolution');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.invalidClientCertId', 'en', 'The value of the DS Setting "{0}" does not exist.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.invalidClientCertId', 'en', 'Please update the DS setting "{0}". The value should be the Id of the KeyInfo with alias="{1}"');

-- #1274 add entries for Key Administration and HSM Service/Device
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.keyManage', 'en', 'Key management');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.keyAdmin', 'en', 'Key Administration');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMDevices', 'en', 'HSM Devices');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMDevice', 'en', 'HSM Device');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMDevice.name', 'en', 'Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMDevice.className', 'en', 'Class Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.HSMDeviceConf.config', 'en', 'Configuration');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.addHSMDevice', 'en', 'Add new HSM device');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.hsmDeviceEdit.config', 'en', 'A string of key-value pairs delimited by a semicolon (ex. host=127.0.0.1;timeout=30)');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.hsmdeviceconf.config.invalid.format', 'en',
                                                         'Configuration has invalid format. Value should be a string of key-value pairs delimited by a semicolon.');

-- #1256 Implement reporting functionality
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.reports', 'en', 'Reports');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'role.reports', 'en', 'Reports');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.type.transactions', 'en', 'Transactions');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.type.users', 'en', 'Users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.transaction', 'en', 'Transaction Report');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.useraudit', 'en', 'User Audit Report');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.list.show.reports', 'en', 'Show reports +');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.list.hide.reports', 'en', 'Hide reports -');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.locale', 'en', 'Locale');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.timezone', 'en', 'Timezone');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'button.run.report', 'en', 'Run Report');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.queued', 'en', 'Queued');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.processing', 'en', 'Processing');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.completed', 'en', 'Completed');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.error', 'en', 'Error');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.deleted', 'en', 'Deleted');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.status.unknown', 'en', 'Unknown');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.name', 'en', 'Report Name');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.from', 'en', 'Report From Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.to', 'en', 'Report To Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.created', 'en', 'Report Created');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.user', 'en', 'User');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.startdatetime', 'en', 'Missing/Invalid Start Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.enddate', 'en', 'Missing/Invalid End Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.startdate.after.enddate', 'en', 'Start Date cannot be after End Date');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.report.confirm.delete', 'en', 'Confirm delete report?');

-- #1309 ds_texts entries for license info issued field
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.li.issued', 'en', 'Issued');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'err.invalid.date.format', 'en', 'Invalid date format. Format must be {0}.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.licenseinfo.issued', 'en', 'Exact value from provided license data, Date yyyy-MM-dd');

-- #1222 Add Payment System Settings page
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings', 'en', 'Payment System Settings');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.comment', 'en', 'Comment');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.modified', 'en', 'Last Modified');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.help.DSURL', 'en', 'URL of the DS to which the ACS will send the RReq if a challenge occurs. The ACS is responsible for storing this value for later use in the transaction for sending the RReq to the DS.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.paymentsystemssettings.presentablename.DSURL', 'en', 'AReq DS URL');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.usersetting', 'en', 'User Settings');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.adminsettings', 'en', 'Admin Settings');

-- #1304 Support DS 2.3.0
INSERT INTO ds_texts (id, tkey, locale, message) VALUES	(default, 'text.ext.acsInfoInd.2_2_0', 'en', '2.2.0 ACS InfoInd');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES	(default, 'text.ext.acsInfoInd.2_3_0', 'en', '2.3.0 ACS InfoInd');

-- #1369 Enhance error messages
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.AReqConnectTimeout', 'en', 'The ACS URL "{0}" is unreachable.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.AReqConnectTimeout', 'en', 'Please check if the URL "{0}" is correct and reachable.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.AReqReadTimeout', 'en', 'The ACS URL "{0}" did not respond in time.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.AReqReadTimeout', 'en', 'Please check if the URL "{0}" is responsive. You can also increase the allowed time to respond by updating the value for setting "acsTimeout".');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.RReqConnectTimeout', 'en', 'The 3DSS URL "{0}" is unreachable.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.RReqConnectTimeout', 'en', 'Please check if the URL "{0}" is correct and reachable.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.desc.RReqReadTimeout', 'en', 'The 3DSS URL "{0}" did not respond in time.');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.err.resolution.RReqReadTimeout', 'en', 'Please check if the URL "{0}" is responsive. You can also increase the allowed time to respond by updating the value for setting "tdsServerTimeout".');

-- #882 Add tooltip to user roles
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.adminSetup', 'en', 'Admin or superuser role');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.paymentsystemView', 'en', 'View payment systems');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.paymentsystemEdit', 'en', 'Edit payment system details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.merchantsView', 'en', 'Vew merchants');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.merchantsEdit', 'en', 'Edit merchant details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.userView', 'en', 'View other users');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.userEdit', 'en', 'Edit other user roles');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.issuerView', 'en', 'View issuers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.issuerEdit', 'en', 'Edit issuers details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.acquirerView', 'en', 'View acquirers');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.acquirerEdit', 'en', 'Edit acquirer details');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.recordsView', 'en', 'View Authentication records');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.panView', 'en', 'View Primary Account Numbers (View PAN role has effect only with 2nd factor authentication performed)');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.textView', 'en', 'View texts');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.textEdit', 'en', 'Edit texts');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.caView', 'en', 'View CA');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.caEdit', 'en', 'Edit CA');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.auditLogView', 'en', 'View audit logs');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.keyManage', 'en', 'View and management of HSM devices');
INSERT INTO ds_texts (id, tkey, locale, message) VALUES (default, 'text.tooltip.user.roles.reports', 'en', 'View and generation of reports');
